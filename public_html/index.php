<?php
  require_once "functies/website_usage.php";
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html lang="nl"><!-- InstanceBegin template="/Templates/templaterootphp.dwt.php" codeOutsideHTMLIsLocked="false" -->
<?php
   require_once "functies/badm_db.inc.php";
   $badm_db = badm_conn_db();
?>
<head>
<META NAME="robots"                  CONTENT="index,follow">
<META NAME="keywords"                CONTENT="badminton,batminton,w&amp;l,badmintonvereniging,club,badmintonclub,sport,Belgium,Veltem,Herent,Leuven,Winksele,Belgi�">
<META NAME="description"             CONTENT="W&amp;L bv : De grootste en leukste badmintonclub van Brabant">
<META NAME="abstract"                CONTENT="badminton, club">
<META NAME="Area"                    CONTENT="General">
<META NAME="Author"                  CONTENT="W&amp;L bv">
<META NAME="Copyright"               CONTENT="(c) 2003-2004 W&amp;L bv">
<META NAME="Creator"                 CONTENT="W&amp;L bv">
<META NAME="Generator"               CONTENT="W&amp;L bv">
<META NAME="Owner"                   CONTENT="W&amp;L bv">
<META NAME="Publisher"               CONTENT="W&amp;L bv">
<META NAME="Rating"                  CONTENT="General">
<META NAME="revisit-after"           CONTENT="7 Days">
<META NAME="revisit"                 CONTENT="7 Days">
<META NAME="document-classification" CONTENT="Internet">
<META NAME="document-type"           CONTENT="Public">
<META NAME="document-rating"         CONTENT="Safe for Kids">
<META NAME="document-distribution"   CONTENT="Global">
<META HTTP-EQUIV="Content-Type"      CONTENT="text/html; charset=iso-8859-1">
<META HTTP-EQUIV="Audience"          CONTENT="General">
<META HTTP-EQUIV="content-language"  CONTENT="NL">
<META HTTP-EQUIV="Pragma"            CONTENT="no-cache">
<META HTTP-EQUIV="Expires"           CONTENT="now">
<!-- InstanceBeginEditable name="doctitle" -->
<title>W&amp;L badmintonvereniging vzw - Homepage</title>
<!-- disable image toolbar -->
<meta http-equiv="imagetoolbar" content="no">
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="head" -->
<script type="text/javascript" language="JavaScript">
<!--
// have people break free from frames automatically when they come to our page
if (parent.frames.length >= 1)
{
  window.top.location.href="index.php";
}
// show or hide content on a page
function hide(id)
{
  document.getElementById(id).style.display = 'none';
}
function show(id)
{
  document.getElementById(id).style.display = 'block';
}
// -->
</script>

<link href="css/badminton.css" rel="stylesheet" type="text/css">
<link rel="shortcut icon" href="images/icoontje.ico">
<link rel="alternate" type="application/rss+xml" title="RSS" href="http://www.badmintonsport.be/rss/main.rss">
<link rel="alternate" type="application/rss+xml" title="RSS Forum" href="http://www.badmintonsport.be/rss/gb.rss">

<style type="text/css">
<!--
li.gold {
    font-family: Verdana, Arial, Helvetica, sans-serif;
    font-size: 11px;
    list-style-image: url(images/gouden_beker.png);
}
li.silver {
    font-family: Verdana, Arial, Helvetica, sans-serif;
    font-size: 11px;
    list-style-image: url(images/zilveren_beker.png);
}
-->
</style><!-- InstanceEndEditable -->
<link href="http://www.badmintonsport.be/css/badminton.css" rel="stylesheet" type="text/css">
<script type="text/javascript" language="JavaScript1.2" src="http://www.badmintonsport.be/scripts/stm31.js"></script>
<script type="text/javascript" language="JavaScript" src="http://www.badmintonsport.be/scripts/dw_rotator.js"></script>
<script type="text/javascript" language="JavaScript">
<!--
function initRotator()
{
  var rotator1 = new dw_Rotator("sponsors_banner", 4000, "http://www.badmintonsport.be/images/", "_blank");
  rotator1.addImages("Veltem_Motors_banner.png", "banner_verhoeven.jpg");
  //rotator1.addImages("Veltem_Motors_banner.png");
  rotator1.addActions("http://www.veltem-motors.be", "http://www.isolatieverhoeven.be");
  //rotator1.addActions("http://www.veltem-motors.be");
  dw_Rotator.start();
}
function doLoad()
{
  // initialize the menu
  st_onload();
  // initialize the banner rotator
  initRotator();
}
//-->
</script>
</head>
<body class="badminton" onload="doLoad();">
<table width="100%" border="0" cellpadding="0" cellspacing="0">
 <tr>
  <td colspan="2">
   <table width="100%" border="0" cellpadding="0" cellspacing="0">
    <!--DWLayoutTable-->
    <tr> 
     <td width="100%" style="background-color: #006600;"><img src="http://www.badmintonsport.be/images/pt_transp.gif" width="1px" height="4px" alt=""></td>
    </tr>
    <tr> 
     <td align="center" valign="top" bgcolor="#CCCCCC">
      <table width="100%" border="0" cellspacing="0" cellpadding="3">
       <tr align="center" valign="middle"> 
        <td rowspan="2" align="left">
	     <img src="http://www.badmintonsport.be/images/logow&amp;l-100.gif" width="100" height="80" alt="logo">
	    </td>
        <td>
	     <h4>W&amp;L Badmintonvereniging vzw<br>
	         Mechelsesteenweg 387<br>
             3020 Herent</h4>
		</td>
        <td align="right"><a href="javascript:void();" onclick="return dw_Rotator.doClick(0)"  onmouseover="dw_Rotator.pause(0)" onmouseout="dw_Rotator.resume(0)" onfocus="this.blur()"><img id="sponsors_banner" name="sponsors_banner" src="images/Veltem_Motors_banner.png" alt="sponsors" height="100" width="320" border="0"></a></td>
       </tr>
      </table>
	 </td>
    </tr>
    <tr> 
     <td bgcolor="#336600"><img src="http://www.badmintonsport.be/images/pt_transp.gif" width="1px" height="4px" alt=""></td>
    </tr>
   </table>
  </td>
 </tr>
 <tr> 
    <td width="150" valign="top">
<!-- InstanceBeginEditable name="Floating Menu" -->

<!-- InstanceEndEditable --> 
      <script type="text/javascript" language="JavaScript1.2">
<!--
stm_bm(["W&L menu",400,"","http://www.badmintonsport.be/scripts/blank.gif",0,"","",0,0,250,0,1000,1,0,0]);
stm_bp("p0",[1,4,0,0,1,4,0,7,100,"progid:DXImageTransform.Microsoft.Fade(overlap=.5,enabled=0,Duration=0.30)",-2,"progid:DXImageTransform.Microsoft.Pixelate(MaxSquare=15,enabled=0,Duration=0.50)",-2,50,0,0,"#669999","#006633","",3,0,0,"#ffffff"]);
stm_ai("p0i0",[0,"Home","","",-1,-1,0,"index.php","_self","Startpagina","","","",0,0,0,"","",0,0,0,0,1,"#fffff7",0,"#b5bed6",0,"","",3,3,0,0,"#fffff7","#000000","#000000","#000000","10pt Verdana","10pt Verdana",0,0]);
stm_aix("p0i1","p0i0",[0,"Algemeen","","",-1,-1,0,"","_self","informatie over de club","","","",0,0,0,"http://www.badmintonsport.be/scripts/arrow_r.gif","http://www.badmintonsport.be/scripts/arrow_r.gif",7,7]);
stm_bpx("p1","p0",[1,2,0,0,1,4,0]);
stm_aix("p1i0","p0i0",[0,"Bestuur","","",-1,-1,0,"/algemeen/bestuur.php","_self","Bestuursleden","","","",0,0,0,"","",0,0,0,1]);
stm_bpx("p1","p0",[1,2,0,0,1,4,0,0]);
stm_aix("p2i0","p1i0",[0,"Mededelingen","","",-1,-1,0,"/algemeen/Bestuur/bestuursmededelingen.php","_self","Bestuursmededelingen"]);
stm_ep();
stm_aix("p1i1","p0i0",[0,"Medewerkers","","",-1,-1,0,"/algemeen/medewerkers.php","_self","Medewerkers","","","",0,0,0,"","",0,0,0,1]);
stm_aix("p1i2","p0i0",[0,"Speeluren","","",-1,-1,0,"/algemeen/speeluren.php","_self","Speeluren","","","",0,0,0,"","",0,0,0,1]);
stm_aix("p1i3","p0i1",[0,"Geschiedenis","","",-1,-1,0,"","_self","Geschiedenis","","","",0,0,0,"","",0,0,0,1]);
stm_bpx("p2","p0",[1,2,0,0,1,4,0,0]);
stm_aix("p2i0","p1i0",[0,"Clubkampioenen","","",-1,-1,0,"/algemeen/Geschiedenis/clubkampioenen.php","_self","Winnaars Clubkampioenschap"]);
stm_aix("p2i1","p1i0",[0,"Nationale Kampioenen","","",-1,-1,0,"/algemeen/Geschiedenis/nationalekampioenen.php","_self","Nationale Kampioenen W&L"]);
stm_ep();
stm_aix("p1i4","p0i0",[0,"Sponsors","","",-1,-1,0,"/algemeen/sponsors.php","_self","Sponsors","","","",0,0,0,"","",0,0,0,1]);
stm_aix("p1i5","p0i0",[0,"Artikels","","",-1,-1,0,"/algemeen/artikels.php","_self","Artikels","","","",0,0,0,"","",0,0,0,1]);
stm_ep();
stm_aix("p0i2","p0i0",[0,"Competitie","","",-1,-1,0,"/competitie.php","_self","Competitie: ploegen en uitslagen"]);
stm_aix("p0i3","p0i1",[0,"Tornooien","","",-1,-1,0,"/tornooien/tornooioverzicht.php","_self","Tornooien"]);
stm_bpx("p1","p0",[1,2,0,0,1,4,0]);
stm_aix("p1i0","p0i1",[0,"Inschrijvingen","","",-1,-1,0,"","_self","Inschrijvingen","Schrijf je hier in voor tornooien"]);
stm_bpx("p1","p0",[1,2,0,0,1,4,0,0]);
//-->
</script>
<?php
// create SQL statement 
   $sql = "SELECT id
                , organisatie
                , doelgroep
                , DATE_FORMAT(start_dt, '%d/%m') AS start_dt
                , verzonden
           FROM inschr_torn
		   WHERE DATE_ADD(dt_inschr, INTERVAL 21 DAY ) > NOW()
		   ORDER BY dt_inschr DESC";
   $result = mysql_query ($sql, $badm_db) or badm_mysql_die();
// format results by row
   $teller = 0;
   echo"<script type=\"text/javascript\" language=\"JavaScript1.2\">";
   while ($badm = mysql_fetch_object($result))
   {
      echo "stm_aix(\"p2i";
      echo $teller;
      echo "\",\"p1i0\",[1,\"";
      echo $badm->organisatie;
      if ($badm->doelgroep == 'J' || $badm->doelgroep == 'V')
      {
        echo $badm->doelgroep == 'J' ? " (Jeugd)" : " (Veteranen)";
        if ($badm->verzonden == 0) //inschrijvingsperiode nog niet afgelopen
        {
          echo " <B>".$badm->start_dt."</B>";
        }
        else
        {
          echo "  <IMG src=/images/closed.png>";
        }
      }
      else
      {
       if ($badm->verzonden == 0) //inschrijvingsperiode nog niet afgelopen
        {
          echo " <B>".$badm->start_dt."</B>";
        }
        else
        {
          echo "  <IMG src=/images/closed.png>";
        }
      }
      echo "\",\"\",\"\",-1,-1,0,\"/tornooien/inschrijvingen.php?id=";
      echo $badm->id;
      echo "\",\"_self\",\"Inschrijving ";
      echo $badm->organisatie;
      echo "\"]);\n";
      $teller = $teller + 1;
   }
   echo"</script>\n";
?>
<script type="text/javascript" language="JavaScript1.2">
<!--
stm_ep();
stm_aix("p1i1","p0i0",[0,"Aanvangsuren","","",-1,-1,0,"/tornooien/aanvangsuren.php","_self","Aanvangsuren tornooien"]);
stm_aix("p1i2","p0i0",[0,"Kalender","","",-1,-1,0,"http://www.badmintonsport.be/docs/tornooikalender.pdf","_blank","Tornooikalender"]);
stm_aix("p1i3","p0i1",[0,"Resultaten","","",-1,-1,0,"","_self","Tornooiresultaten W&L spelers"]);
stm_bpx("p2","p0",[1,2,0,0,1,4,0,0]);
//-->
</script>
<?php
// create SQL statement   
 $sql = "SELECT  DISTINCT 
IF(DATE_FORMAT(dt_van, '%m') < 8, DATE_FORMAT(dt_van, '%Y') - 1, DATE_FORMAT(dt_van, '%Y')) AS start
,IF(DATE_FORMAT(dt_van, '%m') < 8, DATE_FORMAT(dt_van, '%Y'), DATE_FORMAT(dt_van, '%Y') + 1) AS einde
           FROM bad_tornooien
           ORDER  BY start DESC ";
   $result = mysql_query ($sql, $badm_db) or badm_mysql_die();
// format results by row
   $teller = 0;
   echo"<script type=\"text/javascript\" language=\"JavaScript1.2\">";
   while ($badm = mysql_fetch_object($result))
   {
       echo "stm_aix(\"p2i";
      echo $teller;
      echo "\",\"p1i0\",[0,\"Seizoen ";
      echo $badm->start.'-'.$badm->einde;
      echo "\",\"\",\"\",-1,-1,0,\"/tornooien/resultaten.php?s=";
      echo $badm->einde;
      echo "&view_id=22\",\"_self\",\"Seizoen ";
      echo $badm->start.'-'.$badm->einde;
      echo "\"]);\n";
      $teller = $teller + 1;
   }
   echo"</script>\n";
   //mysql_free_result($result);
   mysql_close($badm_db);
?>
<script type="text/javascript" language="JavaScript1.2">
<!--
stm_ep();
stm_ep();
stm_aix("p0i4","p0i0",[0,"Spelers","","",-1,-1,0,"/spelers.php","_self","Spelersprofielen"]);
stm_aix("p0i5","p0i1",[0,"Jeugd","","",-1,-1,0,"","_self","Informatie over en voor de jeugdspelers"]);
stm_bpx("p1","p0",[1,2,0,0,1,4,0]);
stm_aix("p1i0","p0i0",[0,"Algemeen","","",-1,-1,0,"/jeugd/jeugd.php","_self","Jeugd trainers"]);
stm_aix("p1i1","p0i0",[0,"Teams","","",-1,-1,0,"/jeugd/jeugdteams.php","_self","Jeugdteams"]);
stm_aix("p1i2","p0i0",[0,"J-team","","",-1,-1,0,"/jeugd/J-team.php","_self","J-team"]);
stm_aix("p1i3","p0i0",[0,"Resultaten","","",-1,-1,0,"/jeugd/resultaten_jeugd.php?s=2008","_self","Resultaten jeugdtornooien W&L spelers"]);
stm_aix("p1i4","p0i0",[0,"Jeugdtornooi","","",-1,-1,0,"/jeugd/jeugdtornooi.php","_self","Jeugdtornooi W&L"]);
stm_ep();
stm_aix("p0i6","p0i0",[0,"Recreanten","","",-1,-1,0,"/recreant.php","_self","Recreanten"]);
stm_aix("p0i7","p0i1",[0,"Foto's","","",-1,-1,0,"http://users.pandora.be/ikkegg","_blank","badminton foto's"]);
//-->
</script>
<?php
/*************************************************************************************************
 * Create a link to every photo directory that exists
 ************************************************************************************************/
/* Get the root dirctory of the website */
  $rootdir = $_SERVER['DOCUMENT_ROOT'];
/* directory where all the photo's are located in subdirectories */
  $dir = $rootdir."/pictures";
/* Get all the subdirectories of the pictures directory */
  include $rootdir."/functies/getDirectoryListing.php";
  $subdirs = getDirectoryListing($dir, "a", 1, 0, "none");
/* Number of subdirs in directory */
  $subdir_cnt = count($subdirs);
/* Display a link to every subdirectory */
  $i = 0;
  foreach ($subdirs as $subdir)
  {
    echo "<script type=\"text/javascript\" language=\"JavaScript1.2\">\n";
    if ($i == 0) // first subdirectory
    {
       echo "stm_bpx(\"p1\",\"p0\",[1,2,0,0,1,4,0]);\n";
    }
	echo "stm_aix(\"p0i".$i."\",\"p0i0\",[0,\"".$subdir."\",\"\",\"\",-1,-1,0,\"/pictures/showThumbnails.php?dir=".$subdir."\",\"_self\",\"".$subdir."\"]);\n";
    if ($i == $subdir_cnt - 1) // last subdirectory
    {
      echo "stm_ep();\n";
    }
    echo"</script>\n";
	$i++;
  }
?>
      <script type="text/javascript" language="JavaScript1.2">
<!--
stm_ai("p0i8",[6,4,"#006633","",0,0,0]);
stm_aix("p0i9","p0i0",[0,"Links","","",-1,-1,0,"/links.php","_self","Links"]);
stm_aix("p0i10","p0i0",[0,"Message board","","",-1,-1,0,"/messageboard/messageboard.php?p=1&m=n&id=0","_blank","Message Board"]);
stm_aix("p0i11","p0i0",[0,"Laatste Nieuws","","",-1,-1,0,"/laatste_nieuws.php","_self","De laatste wijzigingen aan de W&L website"]);
stm_aix("p0i12","p0i0",[0,"Team Veltem Motors","","",-1,-1,0,"http://www.everyoneweb.com/teamveltemmotors","_self","De fansite van Team Veltem Motors"]);
stm_ep();
stm_em();
//-->
</script>
<!-- InstanceBeginEditable name="Floating Menu end" --><!-- InstanceEndEditable --> 
      &nbsp;&nbsp; <!-- InstanceBeginEditable name="counter" --><!-- Begin Nedstat Basic code -->

<!-- Title: WL Badmintonvereniging vzw -->

<!-- URL: http://www.badmintonsport.be/ -->
<script language="JavaScript" type="text/javascript" src="http://m1.nedstatbasic.net/basic.js">
<!--
  nedstatbasic("AADtlQDAouCV4ZUt/EjCf8L+yHvQ", 0);
// -->
</script>
      <a target="_blank" href="http://v1.nedstatbasic.net/stats?AADtlQDAouCV4ZUt/EjCf8L+yHvQ">
      <img src="http://m1.nedstatbasic.net/n?id=AADtlQDAouCV4ZUt/EjCf8L+yHvQ"
border="0" width="18" height="18" alt="Nedstat Basic - Free web site statistics"></a>
      &nbsp; <a href="http://www.badmintonsport.be/rss/main.rss"> <img src="rss/rss.gif" width="36" height="14" alt="XML/RSS feed" style="border: none"><img src="rss/xml.gif" width="36" height="14" alt="XML/RSS feed" style="border: none"></a>
      <!-- End Nedstat Basic code -->
      <!-- InstanceEndEditable --> 
      <!-- InstanceBeginEditable name="Poll" -->
      <?php
/*include_once "/home/badmin/public_html/poll/booth.php";
echo $php_poll->poll_process(6);*/
?>
      <!-- InstanceEndEditable --> <!-- InstanceBeginEditable name="wiens verjaardag is het" -->
      <?php
   require_once "functies/Tabel.php";
   require_once "functies/badm_db.inc.php";
   $badm_db = badm_conn_db();

  /*********************************************************************************/
  /* Checken of er een tornooi is waarvan de inschrijvingsperiode vandaag vervalt. */
  /* De lijst met ingeschrevenen verzenden naar de organisatie                     */
  /* Tabel inschr_torn aanpassen zodat deze mailing slechts 1 keer gebeurt.        */
  /*********************************************************************************/

  // We gaan ervan uit dat er elke dag voor 9u minstens 1 bezoeker is van de homepage
  if (date("G") < 9)
  {
    require_once("/home/badmin/public_html/functies/send_inschr_torn.php");
    $status = send_all_subscriptions($badm_db);
    if ($status == false)
    {
      mail("freek.ceymeulen@pandora.be", "Probleem bij verzenden definitieve inschrijvingen", $debug, "From: webmaster@badmintonsport.be");
    }
    // Elke maandag verzenden we alle nieuwe en/of gewijzigde inschrijvingen
    // Dit om te voorkomen dat het tornooi al volzet is wanneer we de definitieve inschrijvingen versturen
    if (date("l") == "Monday")
    {
      $status = send_new_and_changed_subscriptions($badm_db);
      if ($status == false)
      {
        mail("freek.ceymeulen@pandora.be", "Probleem bij verzenden definitieve inschrijvingen", $debug, "From: webmaster@badmintonsport.be");
      }
    } // end dag = maandag
  } // end voor 9u

  /*******************************************************************************************************/
  /* Einde mailing                                                                                       */
  /*******************************************************************************************************/

// SQL statement voor de jarige(n)
   $sql = "SELECT id
                , naam
           FROM bad_spelers
           WHERE eind_dt IS NULL
           AND DATE_FORMAT(geb_dt, '%d-%m') = DATE_FORMAT(NOW(), '%d-%m')";
   $result = mysql_query ($sql, $badm_db) or badm_mysql_die();
   if (mysql_num_rows($result) != 0)
   {
      echo "<br><table border=\"1\" align=\"left\" cellpadding=\"2\" cellspacing=\"0\"><tr><td colspan=\"2\" bgcolor=\"#666699\" class=\"kleingeel\">";
      if (mysql_num_rows($result) == 1)
      {
         echo "Jarige van de dag</td></tr>";
      }
      else // meer dan 1 jarige
      {
         echo "Jarigen van de dag</td></tr>";
      }
   // format results by row
      echo "<tr><td valign=\"top\" class=\"kleinetekst\">";
      while ($jarig = mysql_fetch_object($result))
      {
         echo "<br><a href=\"spelers/spelersinfo.php?id=";
         echo $jarig->id;
         echo "&amp;a=n\" target=\"_blank\">";
         echo $jarig->naam;
         echo "</a><br>";
      }
      echo "</td></tr></table>";
   }
?>
      <br clear="all"><br><img src="images/Logo_Badminton_Plus.png" alt="Onze club heeft het Badminton Plus kwaliteitslabel categorie A behaald!" width="130" height="81">
    <p>&nbsp;<a class="nws" href="http://www.badmintonsport.be/algemeen/speeluren.php" title="Algemeen -> Speeluren" target="_self">Sluitingsdagen</a></p>
    <p>&nbsp;<a class="nws" href="http://www.badmintonsport.be/plan1.htm" title="Wegbeschrijvingen en plannetjes sporthallen" target="_blank">sporthallen</a></p>
      <!-- InstanceEndEditable --> 
    </td>
    <td width="100%" valign="top">
	  <!-- InstanceBeginEditable name="general" -->
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr align="left" valign="top">
          <td> </td>
          <td><img src="images/spacer.gif" width="15" height="1" alt=""></td>
          <td align="right" class="kleinetekst">

<?php
   /* Het aantal bezoekers online tellen */
   include "functies/usersOnline.class.php";
   $visitors_online = new usersOnline;
   if ($visitors_online->count_users() == 1)
   {
     echo "Er is " . $visitors_online->count_users() . " bezoeker online &nbsp;";
   }
   else
   {
     echo "Er zijn " . $visitors_online->count_users() . " bezoekers online &nbsp;";
   }
?>

            <img src="http://www.badmintonsport.be/images/spacer.gif" width="350" height="1" alt=""></td>
          <td width="5" height="0" align="left"> <img src="http://www.badmintonsport.be/images/spacer.gif" width="5" height="1" alt="">
          </td>
        </tr>
        <tr align="left" valign="top">
          <td width="100%">

            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td align="center" valign="top" class="kleinetekst">

<?php

  // Get the homepage content

  $sql = "
SELECT id
     , title
     , message
     , DATE_FORMAT(from_date, '%d/%m/%y') AS from_dt
     , DATE_FORMAT(thru_date, '%d/%m/%y') AS thru_dt
     , DATE_FORMAT(dt_crea, '%d/%m/%y') AS creation_date
     , usid_crea
     , DATE_FORMAT(dt_wijz, '%d/%m/%y') AS last_modified
     , usid_wijz
  FROM homepage_content
 WHERE from_date <= NOW()
   AND (thru_date >= NOW() OR thru_date IS NULL)
ORDER BY from_date DESC, thru_date";

  $result = mysql_query($sql);

  while ($row = mysql_fetch_assoc($result))
  {
    echo "<table id=\"".$row["id"]."\" width=\"100%\" border=\"1\" cellspacing=\"0\" cellpadding=\"2\">";
    echo "<tr bordercolor=\"#CCCCCC\" bgcolor=\"#006633\">";
    echo "<td class=\"kleingeel\"><span style=\"float:right;font-size:smaller;font-weight:normal\">&nbsp;".$row["from_dt"]."&nbsp;</span>&nbsp;".$row["title"]."&nbsp;";
    echo "</tr><tr bordercolor=\"#CCCCCC\"><td align=\"left\" valign=\"top\" class=\"kleinetekst\">";
    echo $row["message"];
    echo "<!-- Created: ".$row["creation_date"]." - ".$row["usid_crea"]." -->";
    echo "<!-- Last modified: ".$row["last_modified"]." - ".$row["usid_wijz"]." -->";
    echo "<span style=\"float:right\"><a href=\"admin/addHomepageContent.php?id=".$row["id"]."\" target=\"_blank\" title=\"Wijzigen\"><img src=\"images/pt_transp.gif\" width=\"16px\" height=\"16px\" alt=\"Wijzigen (enkel voor bevoegden)\" border=\"0\" onMouseOver=\"this.src='images/edit_16.png';\" onMouseOut=\"this.src='images/pt_transp.gif';\"></a></span>";
    echo "</tr></table><br>";
  }
?>

            </table>
           </td>
          <td align="center">&nbsp;</td>
          <td align="left">
            <table width="100%" border="1" cellspacing="0" cellpadding="2">
              <tr>
                <td align="left" valign="top" style="border-color:#006600" bgcolor="#006600" class="kleingeel">Nieuws</td>
              </tr>
              <tr>
                <td align="left" valign="top" style="border-color:#006600" bgcolor="#EFEFEF" class="kleinetekst">
                <table width="100%" border="0" cellspacing="0" cellpadding="2">
                   <tr bordercolor="#CCCCCC">
                     <td align="left" valign="top" class="kleinetekst">
                       <p class="nws"></td>
                   </tr>

<tr bordercolor="#CCCCCC">
                     <td align="left" valign="top" class="nws"><abbrev>Sluitingsdagen</abbrev></td>
                   </tr>
                   <tr bordercolor="#CCCCCC">
                     <td align="left" valign="top" class="kleinetekst"><ul class="kleinetekst">
                                            
                      <li>Er is geen badminton op offici�le feestdagen tenzij anders afgesproken.</li>
                     </ul>
                     </td>
                   </tr>

                   <tr bordercolor="#CCCCCC">
                     <td align="left" valign="top" class="nws"><abbrev>Activiteitenkalender</abbrev></td>
                   </tr>
                   
                   <tr bordercolor="#CCCCCC">
                     <td align="left" valign="top"><?php
  /*
  || Toon de competitiematchen van vandaag - 4 dagen tot vandaag + 4 dagen
  */

  $query = "
SELECT m.id
     , CASE DAYOFWEEK(m.datum)
       WHEN 1 THEN 'zon'
       WHEN 2 THEN 'ma'
       WHEN 3 THEN 'di'
       WHEN 4 THEN 'wo'
       WHEN 5 THEN 'do'
       WHEN 6 THEN 'vr'
       WHEN 7 THEN 'zat'
       END AS dag
     , DATE_FORMAT(m.datum, '%e/%c') AS datum
     , m.uur
     , m.wedstrijd
     , m.score
     , c.id AS ploeg_id
     , c.ploegnummer
     , c.afdeling
     , c.type
  FROM competitiematchen m
     , bad_competitieploegen c
 WHERE m.competitieploegen_id = c.id
   AND m.datum BETWEEN ADDDATE(CURDATE(), INTERVAL - 4 DAY) AND ADDDATE(CURDATE(), INTERVAL 4 DAY)
 ORDER BY m.datum
        , m.uur";

  $result = mysql_query($query, $badm_db) or badm_mysql_die();
  $totaal = mysql_num_rows($result);

  if ($totaal > 0)
  {
    echo "                 <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\">\n";
    echo "                    <tr bordercolor=\"#CCCCCC\">\n";
    echo "                      <td class=\"nws\">Competitienieuws</td>\n";
    echo "                    </tr>\n";
    echo "                    <tr bordercolor=\"#CCCCCC\">\n";
    echo "                      <td valign=\"top\" bordercolor=\"#CCCCCC\">\n";
    echo "                        <table border=\"0\" cellspacing=\"0\" cellpadding=\"4\">\n";
  }

  while ($row = mysql_fetch_object($result))
  {
    echo "                          <tr class=\"kleinetekst\">\n";
    echo "                            <td align=\"left\" valign=\"top\">".$row->dag."</td>\n";
    echo "                            <td align=\"right\" valign=\"top\">".$row->datum."</td>\n";
    echo "                            <td align=\"left\" valign=\"top\">".$row->uur."</td>\n";
    // Maak een link naar deze ploeg op de competitiepagina
    $ontmoeting = str_replace('W&amp;L', '<a href="competitie.php#'.$row->ploeg_id.'">W&amp;L '.substr($row->type, 0, 1).$row->ploegnummer.'</a>', htmlspecialchars($row->wedstrijd));
    echo "                            <td align=\"left\" valign=\"top\">".$ontmoeting."</td>\n";
    echo "                            <td align=\"center\"><b><nobr>".$row->score."</nobr></b></td>\n";
    echo "                          </tr>\n";
  }
  if ($totaal > 0)
  {
    echo "                        </table>\n";
    echo "                      </td>\n";
    echo "                    </tr>\n";
    echo "                  </table>\n";
  }
  /*
  || Toon de eerstvolgende tornooien en de winnaars van de eerstvorige tornooien
  */
  // Selecteer alle tornooien in de toekomst waarvoor W&L-ers zich hebben ingeschreven
  $query = "SELECT DISTINCT it.id
                 , it.organisatie
                 , it.datum
                 , it.start_dt
              FROM inschr_torn it
                 , inschr_spel s
             WHERE s.inschr_torn_id = it.id
               AND start_dt >= NOW()
             ORDER BY start_dt";

  $result = mysql_query($query, $badm_db) or badm_mysql_die();

  // Selecteer alle winnaars van een tornooi van max 14 dagen geleden
  $query = "SELECT CONCAT(DATE_FORMAT(tor.dt_van, '%d/%m/%y'), IF(tor.dt_tot IS NULL, ' ' , ' - '), IFNULL(DATE_FORMAT(tor.dt_tot, '%d/%m/%y'), '')) as datum
                 , plaats
                 , IF (tor.reeks = 'G', 'GD', CONCAT(CASE sp.geslacht WHEN 'M' THEN 'H' ELSE 'D' END, tor.reeks)) as discipline
                 , tor.klassement
                 , tor.categorie
                 , sp.naam
                 , IF (tor.klassement = 'openB', 'B12', IF (tor.klassement = 'openC', 'C12', tor.klassement)) as orderbyklassement
         , sp.id AS spelers_id
             FROM bad_spelers as sp
               ,bad_tornooien as tor
           WHERE tor.spelers_id = sp.id
             AND ADDDATE(dt_van, INTERVAL 14 DAY) > NOW()
           ORDER BY tor.dt_van DESC, 2, 7, 5, 3";
  $result2 = mysql_query($query, $badm_db) or badm_mysql_die();
  $totaal1 = mysql_num_rows($result);
  $totaal2 = mysql_num_rows($result2);

  if ($totaal1 + $totaal2 > 0)
  {
    echo "                 <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\">\n";
    echo "                   <tr bordercolor=\"#CCCCCC\">\n";
    echo "                     <td class=\"nws\">Tornooinieuws</td>\n";
    echo "                   </tr>\n";
  }

  $prev_date = "dummy";
  $counter = 0;
  // toon de volgende tornooien
  $tornooi = "";
  while ($row = mysql_fetch_object($result))
  {
    if ($counter == 0 || $prev_date == $row->start_dt)
    {
      $tornooi .= "                        <li class=\"kleinetekst\"><a href=\"http://www.badmintonsport.be/tornooien/inschrijvingen.php?id=".$row->id."\">".$row->organisatie."</a> (".$row->datum.")</li>\n";
    }
    else
    {
      break; // exit the loop
    }
    $counter++;
    $prev_date = $row->start_dt;
  }

  if ($counter > 0)
  {
    echo "                    <tr bordercolor=\"#CCCCCC\">\n";
    if ($counter > 1)
    {
      echo "                      <td class=\"kleinetekst\"><b>Volgende tornooien</b></td>\n";
    }
    else
    {
      echo "                      <td class=\"kleinetekst\"><b>Volgend tornooi</b></td>\n";
    }
    echo "                    </tr>\n";
    echo "                    <tr bordercolor=\"#CCCCCC\"><td><ul>\n";
    echo $tornooi;
    echo "                    </ul></td></tr>\n";
  }
  if ($totaal2 > 0)
  {
    echo "                    <tr bordercolor=\"#CCCCCC\">\n";
    echo "                      <td class=\"kleinetekst\"><b>Overwinningen</b></td>\n";
    echo "                    </tr>\n";
  }

  $tornooi = "dummy";
  // toon de resultaten
  while ($row = mysql_fetch_object($result2))
  {
    if ($tornooi != $row->plaats)
    {
      if ($tornooi != "dummy")
      {
        echo "                    </ul></td></tr>\n";
      }
      echo "                    <tr bordercolor=\"#CCCCCC\">\n";
      echo "                      <td align=\"left\" valign=\"top\">\n";
      echo "                        <p class=\"kleinetekst\">- ".$row->plaats." (".$row->datum.")</p>\n";
      echo "                      </td>\n";
      echo "                    </tr>\n";
      echo "                    <tr bordercolor=\"#CCCCCC\"><td><ul>\n";
    }
    if (strlen($row->categorie) == 0) // officieel (volwassen) tornooi
    {
      $winnaar = $row->discipline." ".$row->klassement.": <a href=\"http://www.badmintonsport.be/spelers/spelersinfo.php?id=".$row->spelers_id."&amp;a=n\" title=\"Spelersprofiel\">".$row->naam."</a>";
    }
    else // jeugd of veteranen tornooi
    {
      $winnaar = $row->discipline." ".$row->categorie.": <a href=\"http://www.badmintonsport.be/spelers/spelersinfo.php?id=".$row->spelers_id."&amp;a=n\" title=\"Spelersprofiel\">".$row->naam."</a>";
    }
    echo "                      <li class=\"kleinetekst\">".$winnaar."</li>\n";
    $tornooi = $row->plaats;
  }
  if ($totaal2 > 0)
  {
    echo "                    </ul></td></tr>\n";
  }

  if (($totaal1 + $totaal2) > 0)
  {
    echo "                  </table>\n";
  }
?></td>
                   </tr>
                   
                   <tr bordercolor="#CCCCCC">
                     <td align="left" valign="top" class="kleinetekst">&nbsp;</td>
                   </tr>
                   <tr bordercolor="#CCCCCC">
                     <td align="left" valign="top" class="nws"><abbr lang="nl" title="Vlaamse Badminton Liga">VBL</abbr> nieuwsbrief</td>
                   </tr>
                   <tr bordercolor="#CCCCCC">
                     <td align="left" valign="top" class="kleinetekst"><ul>
                       <li class="kleinetekst">zie website <a href="http://www.badmintonvlaanderen.be/index.php?option=com_acajoom&Itemid=423" target="_blank">Badminton Vlaanderen</a></li>
                      </ul></td>
                   </tr>
<!--                   <tr bordercolor="#CCCCCC">
                    <td align="left" valign="top" class="nws"><abbr lang="nl" title="Vlaamse Badminton Liga">VBL</abbr>
                        Nieuwsbrief</td>
                   </tr>
                   <tr bordercolor="#CCCCCC">
                    <td align="left" valign="top">
                     <ul>
              <li class="kleinetekst"><a href="http://vbl.badmintonliga.be/index.php?option=com_acajoom&amp;act=mailing&amp;task=view&amp;listid=1&amp;mailingid=10&amp;Itemid=423" target="_blank">april 2008</a><a href="http://www.badmintonliga.be/badmintonliga/nieuwsbrief/vblnieuwsbriefmei07.html" target="_blank"></a></li>
                      </ul>                    </td>
                   </tr>-->
                </table>                </td>
              </tr>
            </table>
          </td>
          <td width="10">&nbsp;</td>
        </tr>
      </table>

      <table width="98%" border="0" align="left" cellpadding="0" cellspacing="0">
        <!--DWLayoutTable-->
        <tr>
          <td width="100%" valign="top" bgcolor="#FFFFFF">
            <!--
            <table width="100%" border="1" cellspacing="0" cellpadding="2">
              <tr bordercolor="#CCCCCC" bgcolor="#666699">
                <td colspan="2" class="kleingeel">06/02/2005 - zomerkamp badminton</td>
              </tr>
              <tr bordercolor="#CCCCCC">
                <td width="29" align="center" valign="top">
                 <img src="images/bericht.gif" width="32" height="32"></td>
                <td width="701" align="left" valign="top" class="kleinetekst">
                  <p class="kleinetekst">
Van <b>27</b> tem <b>31 augustus</b> gaat de vierde editie van het "<a href="docs/Zomerkamp Kluisbos 2005.doc" title="Voor meer info én info omtrent de promotie (tot 15 april!): klik hier" target="_blank">Internationaal Badminton Zomerkamp</a>" door in het KLUISBOS (nabij Oudenaarde).<br/>
Een ideale gelegenheid om je optimaal voor te bereiden op het nieuwe badmintonseizoen!
                  </p>
                  </td>
              </tr>
            </table>
            <br/>
-->
          </td>
        </tr>
      </table>
      <!-- InstanceEndEditable -->
	</td>
  </tr>
  <tr> 
    <td height="0"></td>
    <td></td>
  </tr>
  <tr> 
    <td height="1"><img src="http://www.badmintonsport.be/images/spacer.gif" alt="" width="150" height="1"></td>
    <td></td>
  </tr>
</table>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <!--DWLayoutTable-->
  <tr> 
    <td width="100%" height="50" valign="top" bgcolor="#FFFFFF">
	  <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#CCCCCC">
        <!--DWLayoutTable-->
        <tr> 
          <td align="center" bordercolor="#CCCCCC" bgcolor="#006600"><img src="http://www.badmintonsport.be/images/spacer.gif" width="1" height="4" alt=""></td>
        </tr>
        <tr> 
          <td width="100%" align="center" valign="middle" bordercolor="#CCCCCC" bgcolor="#CCCCCC" class="gmenu">
		    Last change: <!-- InstanceBeginEditable name="datum" -->

        <!-- #BeginDate format:Sp1 -->12/1/13<!-- #EndDate -->

            &nbsp;<!-- InstanceEndEditable --> 
            E-mail: <a href="mailto:webmaster@badmintonsport.be">webmaster@badmintonsport.be</a></td>
        </tr>
        <tr>
          <td align="center" bordercolor="#CCCCCC" bgcolor="#006600" class="gmenu"><img src="http://www.badmintonsport.be/images/spacer.gif" width="1" height="4" alt=""></td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</body>
<!-- InstanceEnd --></html>
<?php
  log_website_usage($start_time, null, $badm_db);
?>
