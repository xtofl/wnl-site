<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html><!-- InstanceBegin template="/Templates/templaterootphp.dwt.php" codeOutsideHTMLIsLocked="false" -->
<?php
   require_once "functies/badm_db.inc.php";
   $badm_db = badm_conn_db();
?>
<head>
<META NAME="robots"                  CONTENT="index,follow">
<META NAME="keywords"                CONTENT="badminton,batminton,w&l,badmintonvereniging,club,badmintonclub,sport,Belgium,Veltem,Herent,Leuven,Winksele,Belgi�" />
<META NAME="description"             CONTENT="W&L bv : De grootste en leukste badmintonclub van Brabant" />
<META NAME="abstract"                CONTENT="badminton, club">
<META NAME="Area"                    CONTENT="General">
<META NAME="Author"                  CONTENT="W&L bv">
<META NAME="Copyright"               CONTENT="(c) 2003-2004 W&L bv">
<META NAME="Creator"                 CONTENT="W&L bv">
<META NAME="Generator"               CONTENT="W&L bv">
<META NAME="Owner"                   CONTENT="W&L bv">
<META NAME="Publisher"               CONTENT="W&L bv">
<META NAME="Rating"                  CONTENT="General">
<META NAME="revisit-after"           CONTENT="7 Days">
<META NAME="revisit"                 CONTENT="7 Days">
<META NAME="document-classification" CONTENT="Internet">
<META NAME="document-type"           CONTENT="Public">
<META NAME="document-rating"         CONTENT="Safe for Kids">
<META NAME="document-distribution"   CONTENT="Global">
<META HTTP-EQUIV="Content-Type"      CONTENT="text/html; charset=iso-8859-1" />
<META HTTP-EQUIV="Audience"          CONTENT="General">
<META HTTP-EQUIV="content-language"  CONTENT="NL">
<META HTTP-EQUIV="Pragma"            CONTENT="no-cache">
<META HTTP-EQUIV="Expires"           CONTENT="now">
<!-- InstanceBeginEditable name="doctitle" -->
<title>W&amp;L - Clubkampioenschap en clubfeest</title>
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="head" --><!-- InstanceEndEditable -->
<link href="css/badminton.css" rel="stylesheet" type="text/css">
<script type="text/javascript" language="JavaScript1.2" src="http://www.badmintonsport.be/scripts/stm31.js"></script>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" class="badminton">
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td height="95" colspan="2" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
        <!--DWLayoutTable-->
        <tr> 
          <td width="100%" height="95"><table width="100%" border="0" cellpadding="0" cellspacing="0" bordercolor="#CCCCCC" bgcolor="#FFFFFF">
              <!--DWLayoutTable-->
              <tr> 
                <td width="100%" align="center" valign="middle" bgcolor="#006600"><img src="images/spacer.gif" width="200" height="4"></td>
              </tr>
              <tr> 
                <td align="center" valign="middle" bgcolor="#CCCCCC"><img src="images/pt_transp.gif" width="500" height="5"></td>
              </tr>
              <tr> 
                <td align="center" valign="top" bgcolor="#CCCCCC"><table width="100%" border="0" cellspacing="0" cellpadding="3">
                    <tr align="center" valign="middle"> 
                      <td rowspan="2" align="left"><img src="images/logow&l-100.gif" width="100" height="80"></td>
                      <td>&nbsp;</td>
                      <td><h4>W&amp;L Badmintonvereniging vzw, Mechelsesteenweg 
                          387, 3020 Herent</h4></td>
                    </tr>
                    <tr align="center" valign="middle"> 
                      <td>&nbsp;</td>
                      <td><img src="images/chinas.gif" width="400" height="60"></td>
                    </tr>
                  </table></td>
              </tr>
              <tr> 
                <td align="center" valign="middle" bgcolor="#CCCCCC"><img src="images/pt_transp.gif" width="500" height="5"></td>
              </tr>
              <tr> 
                <td align="center" valign="middle" bgcolor="#336600"><img src="images/spacer.gif" width="200" height="4"></td>
              </tr>
            </table></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr> 
    <td width="150" valign="top">
<!-- InstanceBeginEditable name="Floating Menu" -->
<!-- InstanceEndEditable --> 
      <script type="text/javascript" language="JavaScript1.2">
<!--
stm_bm(["W&L menu",400,"","scripts/blank.gif",0,"","",0,0,250,0,1000,1,0,0]);
stm_bp("p0",[1,4,0,0,1,4,0,7,100,"progid:DXImageTransform.Microsoft.Fade(overlap=.5,enabled=0,Duration=0.30)",-2,"progid:DXImageTransform.Microsoft.Pixelate(MaxSquare=15,enabled=0,Duration=0.50)",-2,50,0,0,"#669999","#006633","",3,0,0,"#ffffff"]);
stm_ai("p0i0",[0,"Home","","",-1,-1,0,"index.php","_self","Startpagina","","","",0,0,0,"","",0,0,0,0,1,"#fffff7",0,"#b5bed6",0,"","",3,3,0,0,"#fffff7","#000000","#000000","#000000","10pt Verdana","10pt Verdana",0,0]);
stm_aix("p0i1","p0i0",[0,"Algemeen","","",-1,-1,0,"","_self","informatie over de club","","","",0,0,0,"scripts/arrow_r.gif","scripts/arrow_r.gif",7,7]);
stm_bpx("p1","p0",[1,2,0,0,1,4,0]);
stm_aix("p1i0","p0i0",[0,"Bestuur","","",-1,-1,0,"/algemeen/bestuur.php","_self","Bestuursleden","","","",0,0,0,"","",0,0,0,1]);
stm_bpx("p1","p0",[1,2,0,0,1,4,0,0]);
stm_aix("p2i0","p1i0",[0,"Mededelingen","","",-1,-1,0,"/algemeen/Bestuur/bestuursmededelingen.php","_self","Bestuursmededelingen"]);
stm_ep();
stm_aix("p1i1","p0i0",[0,"Medewerkers","","",-1,-1,0,"/algemeen/medewerkers.php","_self","Medewerkers","","","",0,0,0,"","",0,0,0,1]);
stm_aix("p1i2","p0i0",[0,"Speeluren","","",-1,-1,0,"/algemeen/speeluren.php","_self","Speeluren","","","",0,0,0,"","",0,0,0,1]);
stm_aix("p1i3","p0i1",[0,"Geschiedenis","","",-1,-1,0,"","_self","Geschiedenis","","","",0,0,0,"","",0,0,0,1]);
stm_bpx("p2","p0",[1,2,0,0,1,4,0,0]);
stm_aix("p2i0","p1i0",[0,"Clubkampioenen","","",-1,-1,0,"/algemeen/Geschiedenis/clubkampioenen.php","_self","Winnaars Clubkampioenschap"]);
stm_aix("p2i1","p1i0",[0,"Nationale Kampioenen","","",-1,-1,0,"/algemeen/Geschiedenis/nationalekampioenen.php","_self","Nationale Kampioenen W&L"]);
stm_ep();
stm_aix("p1i4","p0i0",[0,"Sponsors","","",-1,-1,0,"/algemeen/sponsors.php","_self","Sponsors","","","",0,0,0,"","",0,0,0,1]);
stm_aix("p1i5","p0i0",[0,"Artikels","","",-1,-1,0,"/algemeen/artikels.php","_self","Artikels","","","",0,0,0,"","",0,0,0,1]);
stm_ep();
stm_aix("p0i2","p0i0",[0,"Competitie","","",-1,-1,0,"/competitie.php","_self","Competitie: ploegen en uitslagen"]);
stm_aix("p0i3","p0i1",[0,"Tornooien","","",-1,-1,0,"","","Tornooien"]);
stm_bpx("p1","p0",[1,2,0,0,1,4,0]);
stm_aix("p1i0","p0i1",[0,"Inschrijvingen","","",-1,-1,0,"","_self","Inschrijvingen","Schrijf je hier in voor tornooien"]);
stm_bpx("p1","p0",[1,2,0,0,1,4,0,0]);
//-->
</script>
<?php
// create SQL statement 
   $sql = "SELECT id
                , organisatie
                , doelgroep
                , DATE_FORMAT(start_dt, '%d/%m') AS start_dt
                , verzonden
           FROM inschr_torn
		   WHERE DATE_ADD(dt_inschr, INTERVAL 21 DAY ) > NOW()
		   ORDER BY dt_inschr DESC";
   $result = mysql_query ($sql, $badm_db) or badm_mysql_die();
// format results by row
   $teller = 0;
   echo"<script type=\"text/javascript\" language=\"JavaScript1.2\">";
   while ($badm = mysql_fetch_object($result))
   {
      echo "stm_aix(\"p2i";
      echo $teller;
      echo "\",\"p1i0\",[1,\"";
      echo $badm->organisatie;
      if ($badm->doelgroep == 'J' || $badm->doelgroep == 'V')
      {
        echo $badm->doelgroep == 'J' ? " (Jeugd)" : " (Veteranen)";
        if ($badm->verzonden == 0) //inschrijvingsperiode nog niet afgelopen
        {
          echo " <B>".$badm->start_dt."</B>";
        }
        else
        {
          echo "  <IMG src=/images/closed.png>";
        }
        echo "\",\"\",\"\",-1,-1,0,\"/tornooien/inschrijvingen_jeugd.php?id=";
      }
      else
      {
       if ($badm->verzonden == 0) //inschrijvingsperiode nog niet afgelopen
        {
          echo " <B>".$badm->start_dt."</B>";
        }
        else
        {
          echo "  <IMG src=/images/closed.png>";
        }
        echo "\",\"\",\"\",-1,-1,0,\"/tornooien/inschrijvingen.php?id=";
      }
      echo $badm->id;
      echo "\",\"_self\",\"Inschrijving ";
      echo $badm->organisatie;
      echo "\"]);";
      $teller = $teller + 1;
   }
   echo"</script>";
?>
<script type="text/javascript" language="JavaScript1.2">
<!--
stm_ep();
stm_aix("p1i1","p0i0",[0,"Aanvangsuren","","",-1,-1,0,"/tornooien/aanvangsuren.php","_self","Aanvangsuren tornooien"]);
stm_aix("p1i2","p0i0",[0,"Kalender","","",-1,-1,0,"/docs/tornooikalender20052006.pdf","_blank","Tornooikalender seizoen 2005 - 2006"]);
stm_aix("p1i3","p0i1",[0,"Resultaten","","",-1,-1,0,"","_self","Tornooiresultaten W&L spelers"]);
stm_bpx("p2","p0",[1,2,0,0,1,4,0,0]);
//-->
</script>
<?php
// create SQL statement   
 $sql = "SELECT  DISTINCT 
IF(DATE_FORMAT(dt_van, '%m') < 8, DATE_FORMAT(dt_van, '%Y') - 1, DATE_FORMAT(dt_van, '%Y')) AS start
,IF(DATE_FORMAT(dt_van, '%m') < 8, DATE_FORMAT(dt_van, '%Y'), DATE_FORMAT(dt_van, '%Y') + 1) AS einde
           FROM bad_tornooien
           ORDER  BY start DESC ";
   $result = mysql_query ($sql, $badm_db) or badm_mysql_die();
// format results by row
   $teller = 0;
   echo"<script type=\"text/javascript\" language=\"JavaScript1.2\">";
   while ($badm = mysql_fetch_object($result))
   {
       echo "stm_aix(\"p2i";
      echo $teller;
      echo "\",\"p1i0\",[0,\"Seizoen ";
      echo $badm->start.'-'.$badm->einde;
      echo "\",\"\",\"\",-1,-1,0,\"/tornooien/resultaten.php?s=";
      echo $badm->einde;
      echo "&view_id=22\",\"_self\",\"Seizoen ";
      echo $badm->start.'-'.$badm->einde;
      echo "\"]);";
      $teller = $teller + 1;
   }
   echo"</script>";
   //mysql_free_result($result);
   mysql_close($badm_db);
?>
<script type="text/javascript" language="JavaScript1.2">
<!--
stm_ep();
stm_ep();
stm_aix("p0i4","p0i0",[0,"Spelers","","",-1,-1,0,"/spelers.php","_self","Spelersprofielen"]);
stm_aix("p0i5","p0i1",[0,"Jeugd","","",-1,-1,0,"","_self","Informatie over en voor de jeugdspelers"]);
stm_bpx("p1","p0",[1,2,0,0,1,4,0]);
stm_aix("p1i0","p0i0",[0,"Algemeen","","",-1,-1,0,"/jeugd/jeugd.php","_self","Jeugd trainers"]);
stm_aix("p1i1","p0i0",[0,"Teams","","",-1,-1,0,"/jeugd/jeugdteams.php","_self","Jeugdteams"]);
stm_aix("p1i2","p0i0",[0,"J-team","","",-1,-1,0,"/jeugd/J-team.php","_self","J-team"]);
stm_aix("p1i3","p0i0",[0,"Resultaten","","",-1,-1,0,"/jeugd/resultaten_jeugd.php?s=2006","_self","Resultaten jeugdtornooien W&L spelers"]);
stm_aix("p1i4","p0i0",[0,"Jeugdtornooi","","",-1,-1,0,"/jeugd/jeugdtornooi.php","_self","Jeugdtornooi W&L"]);
stm_ep();
stm_aix("p0i6","p0i0",[0,"Recreanten","","",-1,-1,0,"/recreant.php","_self","Recreanten"]);
stm_aix("p0i7","p0i1",[0,"Foto's","","",-1,-1,0,"http://users.pandora.be/ikkegg","_blank","badminton foto's"]);
//-->
</script>
<?php
/*************************************************************************************************
 * Create a link to every photo directory that exists
 ************************************************************************************************/
/* Get the root dirctory of the website */
  $rootdir = $_SERVER['DOCUMENT_ROOT'];
/* directory where all the photo's are located in subdirectories */
  $dir = $rootdir."/pictures";
/* Get all the subdirectories of the pictures directory */
  include $rootdir."/functies/getDirectoryListing.php";
  $subdirs = getDirectoryListing($dir, "a", 1, 0, "none");
/* Number of subdirs in directory */
  $subdir_cnt = count($subdirs);
/* Display a link to every subdirectory */
  for ($i = 0; $i < $subdir_cnt; $i++)
  {
    echo"<script type=\"text/javascript\" language=\"JavaScript1.2\">";
    if ($i == 0) // first subdirectory
    {
       echo "stm_bpx(\"p1\",\"p0\",[1,2,0,0,1,4,0]);\n";
    }
		echo "stm_aix(\"p0i".$i."\",\"p0i0\",[0,\"".$subdirs[$i]."\",\"\",\"\",-1,-1,0,\"/pictures/showThumbnails.php?dir=".$subdirs[$i]."\",\"_self\",\"".$subdirs[$i]."\"]);\n";
    if ($i == $subdir_cnt - 1) // last subdirectory
    {
      echo "stm_ep();\n";
    }
    echo"</script>";
  }
?>
      <script type="text/javascript" language="JavaScript1.2">
<!--
stm_ai("p0i8",[6,4,"#006633","",0,0,0]);
stm_aix("p0i9","p0i0",[0,"Links","","",-1,-1,0,"/links.php","_self","Links"]);
stm_aix("p0i10","p0i0",[0,"Message board","","",-1,-1,0,"/messageboard/messageboard.php?p=1&m=n&id=0","_blank","Message Board"]);
stm_ep();
stm_em();
//-->
</script>
<!-- InstanceBeginEditable name="Floating Menu end" -->
<!-- InstanceEndEditable --> 
      &nbsp;&nbsp; <!-- InstanceBeginEditable name="counter" --><!-- InstanceEndEditable --> 
      <!-- InstanceBeginEditable name="Poll" --><!-- InstanceEndEditable --> <!-- InstanceBeginEditable name="wiens verjaardag is het" --><!-- InstanceEndEditable --> 
    </td>
    <td width="100%" valign="top">
	  <!-- InstanceBeginEditable name="general" -->
      <table width="100%" border="0" align="left" cellpadding="0" cellspacing="0">
        <!--DWLayoutTable-->
        <tr> 
          <td width="100%" height="213" valign="top" bgcolor="#FFFFFF"> <h1>              Clubkampioenschap 17 april 2010</h1>
            <p>De winnaars en finalisten zijn:</p>
            <table width="80%" border="1">
              <tr>
                <td><p class="subtitel">Dubbels</p></td>
                <td><p><strong>winnaar</strong></p></td>
                <td><p><strong>finalist</strong></p></td>
              </tr>
              <tr>
                <td><p>Dames A -C1</p></td>
                <td><p>Janne Elst</p></td>
                <td><p>Chlo&euml; De Pauw</p></td>
              </tr>
              <tr>
                <td><p>Dames Jeugd</p></td>
                <td><p>Eva Roose</p></td>
                <td><p>Maaike Tyberghein</p></td>
              </tr>
              <tr>
                <td><p>Jeugd -11 (jongens en meisjes)</p></td>
                <td><p>Catoo Van Driessche</p></td>
                <td><p>Nele Collet</p></td>
              </tr>
              <tr>
                <td><p>Heren A-B1</p></td>
                <td><p>Freek Golinski</p></td>
                <td><p>Rapha&euml;l Van Wel</p></td>
              </tr>
              <tr>
                <td><p>Heren B2-C1</p></td>
                <td><p>Hendrik Mommaerts</p></td>
                <td><p>Wim Petes</p></td>
              </tr>
              <tr>
                <td><p>Heren C2-D</p></td>
                <td><p>Philippe Geerts</p></td>
                <td><p>Rudi Vogeleer</p></td>
              </tr>
              <tr>
                <td><p>Heren J0</p></td>
                <td><p>Nicolas De Raeymaeker</p></td>
                <td><p>Alexander Brumagne</p></td>
              </tr>
              <tr>
                <td><p>Heren J1</p></td>
                <td><p>Jonathan Van Der Beek</p></td>
                <td><p>Elias Brack&eacute;</p></td>
              </tr>
              <tr>
                <td><p>&nbsp;</p></td>
                <td><p>&nbsp;</p></td>
                <td><p>&nbsp;</p></td>
              </tr>
              <tr>
                <td><p class="subtitel">Enkel</p></td>
                <td><p><strong>winnaar</strong></p></td>
                <td><p><strong>finalist</strong></p></td>
              </tr>
              <tr>
                <td><p>DE A-B1</p></td>
                <td><p>Heleen Neerinckx</p></td>
                <td><p>Nele Vervloet</p></td>
              </tr>
              <tr>
                <td><p>DE B2-C2</p></td>
                <td><p>Chlo&euml; De Pauw</p></td>
                <td><p>Kim Van Espen</p></td>
              </tr>
              <tr>
                <td><p>HE A-B2</p></td>
                <td><p>Freek Golinski</p></td>
                <td><p>Birger Abts</p></td>
              </tr>
              <tr>
                <td><p>HE C1-C2</p></td>
                <td><p>Stijn Lenaerts</p></td>
                <td><p>Glenn Huegaerts</p></td>
              </tr>
              <tr>
                <td><p>HE D</p></td>
                <td><p>Maxime De Raeymaeker</p></td>
                <td><p>Nicolas De Raeymaeker</p></td>
              </tr>
              <tr>
                <td><p>HE Vets</p></td>
                <td><p>Peter Ringoet</p></td>
                <td><p>Luc Van Driessche</p></td>
              </tr>
              <tr>
                <td><p>ME -11</p></td>
                <td><p>Arizona Binst</p></td>
                <td><p>Nele Collet</p></td>
              </tr>
              <tr>
                <td><p>ME -19</p></td>
                <td><p>Wencke Hublou</p></td>
                <td><p>Justine Lambrechts</p></td>
              </tr>
              <tr>
                <td><p>JE -11</p></td>
                <td><p>Christophe Van Enis</p></td>
                <td><p>Ruben Tyberghein</p></td>
              </tr>
              <tr>
                <td><p>JE -13</p></td>
                <td><p>Elias Brack&eacute;</p></td>
                <td><p>Maarten Lenaerts</p></td>
              </tr>
              <tr>
                <td><p>E -9 (minibad)</p></td>
                <td><p>Milan Brumagne</p></td>
                <td><p>Matthias van Der Beek</p></td>
              </tr>
            </table>            
            <h4>&nbsp;</h4>
          </td>
        </tr>
      </table>
      <!-- InstanceEndEditable -->
	</td>
  </tr>
  <tr> 
    <td height="0"></td>
    <td></td>
  </tr>
  <tr> 
    <td height="1"><img src="images/spacer.gif" alt="" width="150" height="1"></td>
    <td></td>
  </tr>
</table>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <!--DWLayoutTable-->
  <tr> 
    <td width="100%" height="50" valign="top" bgcolor="#FFFFFF"><table width="100%" border="0" cellpadding="0" cellspacing="0" bordercolor="#CCCCCC" bgcolor="#CCCCCC">
        <!--DWLayoutTable-->
        <tr> 
          <td align="center" valign="middle" bordercolor="#CCCCCC" bgcolor="#006600"><img src="images/spacer.gif" width="200" height="4"></td>
        </tr>
        <tr> 
          <td width="100%" align="center" valign="middle" bordercolor="#CCCCCC" bgcolor="#CCCCCC" class="gmenu">Last 
            change: <!-- InstanceBeginEditable name="datum" -->
            <!-- #BeginDate format:It1 -->18-04-2010<!-- #EndDate -->
            <!-- InstanceEndEditable --> 
            E-mail: <a href="mailto:webmaster@badmintonsport.be">webmaster@badmintonsport.be</a></td>
        </tr>
        <tr>
          <td align="center" valign="middle" bordercolor="#CCCCCC" bgcolor="#006600" class="gmenu"><img src="images/spacer.gif" width="200" height="4"></td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</body>
<!-- InstanceEnd --></html>
