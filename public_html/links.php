<?php
  require_once "functies/website_usage.php";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><!-- InstanceBegin template="/Templates/cssLayout.dwt.php" codeOutsideHTMLIsLocked="false" -->
<?php
   /* Build the relative path to the root directory of the website */
   $rootdir = "";
   for ($i=1; $i<substr_count(dirname($_SERVER['PHP_SELF']), "/"); $i++)
   {
     $rootdir = $rootdir."../";
   }
?>
<head>
  <meta http-equiv="Content-Type"      content="text/html charset=iso-8859-1">
  <meta name="robots"                  content="index,follow">
<!-- InstanceBeginEditable name="meta" -->
  <meta name="keywords"                content="badminton,batminton,w&l,W&amp;L bv,badmintonvereniging,club,badmintonclub,sport,Belgium,Veltem,Herent,Leuven,Winksele,Vlaanderen,Belgi�,links,url,clubs">
  <meta name="description"             content="W&amp;L bv : Links">
  <meta name="abstract"                content="badminton, clubs, urls">
<!-- InstanceEndEditable -->
  <meta name="Area"                    content="General">
  <meta name="Author"                  content="W&amp;L bv">
  <meta name="Copyright"               content="(c) 2004-2005 W&amp;L bv">
  <meta name="Creator"                 content="W&amp;L bv">
  <meta name="Generator"               content="W&amp;L bv">
  <meta name="Owner"                   content="W&amp;L bv">
  <meta name="Publisher"               content="W&amp;L bv">
  <meta name="Rating"                  content="General">
  <meta name="revisit-after"           content="7 Days">
  <meta name="revisit"                 content="7 Days">
  <meta name="document-classification" content="Internet">
  <meta name="document-type"           content="Public">
  <meta name="document-rating"         content="Safe for Kids">
  <meta name="document-distribution"   content="Global">
  <meta http-equiv="Audience"          content="General">
  <meta http-equiv="content-language"  content="NL">
  <meta http-equiv="Pragma"            content="no-cache">
  <meta http-equiv="Expires"           content="now">
<!-- InstanceBeginEditable name="doctitle" -->
<title>W&amp;L badmintonvereniging vzw - Links</title>
<!-- InstanceEndEditable --> 
<link rel="stylesheet" href="<?=$rootdir ?>css/W&amp;L_layout.css" type="text/css">

<script type="text/javascript" language="JavaScript1.2" src="<?php echo $rootdir ?>scripts/stm31.js"></script>
<script type="text/javascript" src="<?php echo $rootdir ?>scripts/get_window_height.js"></script>
<script type="text/javascript" src="<?php echo $rootdir ?>scripts/set_footer.js"></script>
<script type="text/javascript">
<!-- have people break free from frames automatically when they come to our page
if (parent.frames.length >= 1)
{
  window.top.location.href="<?php echo basename(__FILE__);  ?>";
}
// -->
</script>
<!-- InstanceBeginEditable name="script" -->

<script type="text/javascript" language="JavaScript" src="http://www.badmintonsport.be/scripts/floating.js"></script>
<script type="text/javascript" language="JavaScript" src="http://www.badmintonsport.be/scripts/sort_links.js"></script>
<script type="text/javascript" language="JavaScript">
// Array to store the table data
var linkArray = new Array();

<?php
   require_once $rootdir."functies/badm_db.inc.php";
   $badm_db = badm_conn_db();
/*************************************************************************************************
 * Initialize an Array with the clubs data
 ************************************************************************************************/
  $sql = "SELECT id
               , naam
               , plaats
               , url
            FROM bad_clubs
           WHERE url IS NOT NULL";
  $result = mysql_query ($sql, $badm_db) or badm_mysql_die();
  for ($i = 0; $i < mysql_num_rows($result); $i++)
  {
    $link = mysql_fetch_object($result);
    echo "linkArray[".$i."] = new linkRecord(\"".$link->plaats."\",\"".$link->naam."\",\"".$link->url."\");\n";
  }
  mysql_close($badm_db);
?>

</script>
<script type="text/javascript" language="JavaScript1.2" src="http://www.badmintonsport.be/scripts/stm31.js"></script>
<!-- InstanceEndEditable -->
</head>

<body>
<!-- ------------------------------------------- HEADER --------------------------------------- -->
<div id="header">

 <table width="85%" border="0" cellspacing="0" cellpadding="3">
  <tr>
   <td rowspan="2" align="left">
    <img src="<?php echo $rootdir ?>images/logow&amp;l-100.gif" width="100" height="80" alt="W&amp;L logo">
   </td>
   <td align="center">
    <h4>W&amp;L Badmintonvereniging vzw, Mechelsesteenweg 387, 3020 Herent</h4>
   </td>
  </tr>
  <tr>
   <td align="center">
    <img src="<?php echo $rootdir ?>images/chinas.gif" width="400" height="60" alt="China's Capital">
   </td>
  </tr>
 </table>

</div>
  <!-- ------------------------------------------ LEFTCONTENT --------------------------------- -->
<div id="leftcontent">

  <!-- InstanceBeginEditable name="LeftContent" -->

  <!-- ------------------------------------------ MENU ---------------------------------------- -->
  <DIV id="divStayTopLeft" style="position:absolute; visibility:inherit;">

<?php
  require_once $rootdir."functies/show_menu.php";
  show_menu();
?>

  </DIV>

<script type="text/javascript">
<!--
JSFX_FloatTopDiv();
-->
</script>

   <!-- InstanceEndEditable --> 
</div>

<!-- ------------------------------------------- MAIN ----------------------------------------- -->
<div id="centercontent">

  <!-- InstanceBeginEditable name="CenterContent" -->
<table width="100%" border="0" cellpadding="0" cellspacing="0">
 <tr> 
  <td width="100%" valign="top">
   <table width="100%" cellpadding="0" cellspacing="0">
    <tr> 
     <td width="100%" valign="top" bgcolor="#FFFFFF">
     <br/>
      <table width="85%" align="center" cellpadding="4" cellspacing="1">
       <tr align="center" bgcolor="#666699">
        <td colspan="2" class="kleingeel">Internationaal</td>
       </tr>
       <tr bgcolor="#AEBBCA"> 
        <td width="35%" bgcolor="#f1f1f1" align="left"><p> Badminton World Federation</p></td>
        <td width="50%" bgcolor="#DFDFDF" align="left"> 
         <p><a href="http://www.internationalbadminton.org/" target="_blank" title="International Badminton Federation">BWF</a></p>
        </td>
       </tr>
      </table>
      <br/>                              
      <table width="85%" border="0" align="center" cellpadding="4" cellspacing="1">
       <tr align="center" bgcolor="#666699"> 
        <td colspan="2" class="kleingeel">Nationaal</td>
       </tr>
       <tr bgcolor="#AEBBCA" align="center">
         <td bgcolor="#f1f1f1" align="left"><p>B.B.F.</p></td>
         <td bgcolor="#DFDFDF" align="left"><p><a href="http://www.belgian-badminton.be/" target="_blank">Belgian Badminton Federation</a></p></td>
       </tr>
       <tr bgcolor="#AEBBCA" align="center"> 
        <td width="35%" bgcolor="#f1f1f1" align="left"><p>LFBB</p></td>
        <td width="50%" bgcolor="#DFDFDF" align="left"> 
         <p><a href="http://www.lfbb.be" target="_blank" title="Ligue Francophone Belge de Badminton">Ligue Francophone Belge de Badminton</a></p>        </td>
       </tr>
       <tr> 
        <td width="35%" bgcolor="#f1f1f1" align="left"><p>VBL</p></td>
        <td width="50%" bgcolor="#DFDFDF">
         <p><a href="http://www.badmintonliga.be" target="_blank" title="Vlaamse Badminton Liga">Vlaamse Badminton Liga</a></p></td>
       </tr>
      </table>
      </BR>                             
      <table width="85%" border="0" align="center" cellpadding="4" cellspacing="1">
       <tr align="center" bgcolor="#666699"> 
        <td colspan="2" class="kleingeel">Provinciaal</td>
       </tr>
       <tr bgcolor="#AEBBCA" align="left"> 
        <td width="35%" bgcolor="#f1f1f1"> 
         <p>Antwerpen</p></td>
        <td width="50%" bgcolor="#dfdfdf"> 
         <p><a href="http://www.badminton-pba.be" target="_blank" title="Provinciale Badmintonraad Antwerpen">
         Provinciale Badmintonraad Antwerpen</a></p></td>
       </tr>
       <tr bgcolor="white"> 
        <td width="35%" bgcolor="#f1f1f1"><p>Limburg</p></td>
        <td width="50%" bgcolor="#dfdfdf">
         <p><a href="http://www.badminton-pbl.be" target="_blank">Provinciaal Badmintonverbond Limburg</a></p></td>
       </tr>
       <tr bgcolor="white"> 
        <td width="35%" bgcolor="#f1f1f1"><p>Oost-Vlaanderen</p></td>
        <td width="50%" class="maroonlink" bgcolor="#dfdfdf">
         <p><a href="http://www.badminton-pbo.be" class="maroonlink" target="_blank" title="Provinciale Badmintonraad Oost-Vlaanderen v.z.w.">Provinciale 
                    Badmintonraad Oost-Vlaanderen v.z.w.</a></p></td>
       </tr>
       <tr bgcolor="#AEBBCA"> 
        <td width="35%" bgcolor="#f1f1f1"><p>Vlaams-Brabant</p></td>
        <td width="50%" bgcolor="#dfdfdf">
         <p><a href="http://www.vvbbc.be" target="_blank" title="Vereniging voor Vlaams-Brabantse Badmintonclubs">Vereniging 
            voor Vlaams-Brabantse Badmintonclubs</a></p></td>
       </tr>
       <tr bgcolor="#AEBBCA"> 
        <td width="35%" bgcolor="#f1f1f1"><p>West-Vlaanderen</p></td>
        <td width="50%" bgcolor="#dfdfdf">
         <p><a href="http://www.badminton-wvbf.yucom.be/" target="_blank" title="West-Vlaamse Badminton Federatie">West-Vlaamse 
                    Badminton Federatie</a></p></td>
       </tr>
      </table>
      <br/>                 
      <table width="85%" border="0" align="center" cellpadding="0" cellspacing="0">
       <tr>
        <td>
         <TABLE cellspacing="1" cellpadding="4" width="100%" align="center">
          <tr bgcolor="#666699">
           <td class="kleingeel" width="35%" onMouseOver="this.style.cursor='hand';" onClick="sortBy('plaats');" title="Klik hier om te sorteren volgens plaats">
            Plaats</td>
           <td class="kleingeel" width="50%" onMouseOver="this.style.cursor='hand';" onClick="sortBy('naam');" title="Klik hier om te sorteren volgens clubnaam">
            Club</td>
          </tr> 
         </table>
        </td> 
       </tr>
       <tr>
        <td id="sortData">
         <TABLE cellspacing="1" cellpadding="4" width="100%" align="center">
<?php
   require_once $rootdir."functies/badm_db.inc.php";
   $badm_db = badm_conn_db();
/*************************************************************************************************
 * Get data from every club
 ************************************************************************************************/
  $sql = "SELECT id
               , naam
               , plaats
               , url
            FROM bad_clubs
           WHERE url IS NOT NULL
           ORDER BY SUBSTRING(postcode, 1, 1)
                  , plaats";
  $result = mysql_query ($sql, $badm_db) or badm_mysql_die();
  while ($badm = mysql_fetch_object($result))
  {
    echo "          <tr>\n";
    echo "           <td width=\"35%\" bgcolor=\"#f1f1f1\"> <p>".$badm->plaats."</p></td>\n";
    echo "           <td width=\"50%\" bgcolor=\"#dfdfdf\"> <p>\n";
    echo "            <a href=\"".$badm->url."\" target=\"_blank\" title=\"".$badm->naam."\">".$badm->naam."</a></p></td>\n";
    echo "          </tr>\n";
  }
?>
         </table>
        </td>
       </tr>
      </table>
      <br/>
      <table width="85%" align="center" cellpadding="4" cellspacing="1">
       <tr align="center" bgcolor="#666699"> 
        <td colspan="2" class="kleingeel">Special Links</td>
       </tr>
       <tr bgcolor="white"> 
        <td width="35%" bgcolor="#f1f1f1"><p>Goodies</p></td>
        <td width="50%" bgcolor="#DFDFDF"><p><a href="http://www.badmintonart.coolfreepage.com" target="_blank">
         http://www.badmintonart.coolfreepage.com</a></p></td>
       </tr>
       <tr bgcolor="white"> 
        <td width="35%" bgcolor="#f1f1f1"><p>Badmintonlife</p></td>
        <td width="50%" bgcolor="#DFDFDF"><p><a href="http://users.skynet.be/BuddyM/badminton/" target="_blank">
         http://users.skynet.be/BuddyM/badminton/</a></p></td>
       </tr>
       <tr bgcolor="white"> 
        <td width="35%" bgcolor="#f1f1f1"><p>Badmintonsites</p></td>
        <td width="50%" bgcolor="#DFDFDF"><p><a href="http://www.sportsites.be/badminton" target="_blank">
         http://www.sportsites.be/badminton</a></p></td>
       </tr>
       <tr bgcolor="white"> 
        <td width="35%" bgcolor="#f1f1f1"><p>Sportclubs</p></td>
        <td width="50%" bgcolor="#DFDFDF"><p><a href="http://www.sportsites.be/sportclubs" target="_blank">
         http://www.sportsites.be/sportclubs</a></p></td>
       </tr>
      </table>
      <br/>
     </td>
    </tr>
   </table>
  </td>
 </tr>
</table>

<!-- InstanceEndEditable -->

</div>
<!-- ------------------------------------------- LEEG ----------------------------------------- -->
<div id="rightcontent">

  <!-- InstanceBeginEditable name="RightContent" -->

  <!-- InstanceEndEditable -->

</div>
<!-- ------------------------------------------ FOOTER ---------------------------------------- -->
<div id="footer">

 <center class="kleinetekst">
    Last change: 
    <!-- #BeginDate format:Fr1 -->30/11/08<!-- #EndDate -->&nbsp;&nbsp;
    E-mail: <a href="mailto:webmaster@badmintonsport.be">webmaster@badmintonsport.be</a> 
 </center>

</div>

</body>
<!-- InstanceEnd --></html>
<?php
  log_website_usage($start_time, null, $badm_db);
  mysql_close($badm_db);
?>