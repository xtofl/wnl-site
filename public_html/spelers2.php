<?php
  require_once "functies/website_usage.php";
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html lang="nl"><!-- InstanceBegin template="/Templates/templaterootphp.dwt.php" codeOutsideHTMLIsLocked="false" -->
<?php
   require_once "functies/badm_db.inc.php";
   $badm_db = badm_conn_db();
?>
<head>
<META NAME="robots"                  CONTENT="index,follow">
<META NAME="keywords"                CONTENT="badminton,batminton,w&amp;l,badmintonvereniging,club,badmintonclub,sport,Belgium,Veltem,Herent,Leuven,Winksele,Belgi�">
<META NAME="description"             CONTENT="W&amp;L bv : De grootste en leukste badmintonclub van Brabant">
<META NAME="abstract"                CONTENT="badminton, club">
<META NAME="Area"                    CONTENT="General">
<META NAME="Author"                  CONTENT="W&amp;L bv">
<META NAME="Copyright"               CONTENT="(c) 2003-2004 W&amp;L bv">
<META NAME="Creator"                 CONTENT="W&amp;L bv">
<META NAME="Generator"               CONTENT="W&amp;L bv">
<META NAME="Owner"                   CONTENT="W&amp;L bv">
<META NAME="Publisher"               CONTENT="W&amp;L bv">
<META NAME="Rating"                  CONTENT="General">
<META NAME="revisit-after"           CONTENT="7 Days">
<META NAME="revisit"                 CONTENT="7 Days">
<META NAME="document-classification" CONTENT="Internet">
<META NAME="document-type"           CONTENT="Public">
<META NAME="document-rating"         CONTENT="Safe for Kids">
<META NAME="document-distribution"   CONTENT="Global">
<META HTTP-EQUIV="Content-Type"      CONTENT="text/html; charset=iso-8859-1">
<META HTTP-EQUIV="Audience"          CONTENT="General">
<META HTTP-EQUIV="content-language"  CONTENT="NL">
<META HTTP-EQUIV="Pragma"            CONTENT="no-cache">
<META HTTP-EQUIV="Expires"           CONTENT="now">
<!-- InstanceBeginEditable name="doctitle" -->
<title>W&amp;L badmintonvereniging vzw - Spelers</title>
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="head" -->
<script type="text/javascript" src="http://www.badmintonsport.be/scripts/floating.js"></script>
<!-- InstanceEndEditable -->
<link href="http://www.badmintonsport.be/css/badminton.css" rel="stylesheet" type="text/css">
<script type="text/javascript" language="JavaScript1.2" src="http://www.badmintonsport.be/scripts/stm31.js"></script>
<script type="text/javascript" language="JavaScript" src="http://www.badmintonsport.be/scripts/dw_rotator.js"></script>
<script type="text/javascript" language="JavaScript">
<!--
function initRotator()
{
  var rotator1 = new dw_Rotator("sponsors_banner", 4000, "http://www.badmintonsport.be/images/", "_blank");
  rotator1.addImages("Veltem_Motors_banner.png", "china.jpg");
  rotator1.addActions("http://www.veltem-motors.be", "http://www.chinacapital.be");
  dw_Rotator.start();
}
function doLoad()
{
  // initialize the menu
  st_onload();
  // initialize the banner rotator
  initRotator();
}
//-->
</script>
</head>
<body class="badminton" onload="doLoad();">
<table width="100%" border="0" cellpadding="0" cellspacing="0">
 <tr>
  <td colspan="2">
   <table width="100%" border="0" cellpadding="0" cellspacing="0">
    <!--DWLayoutTable-->
    <tr> 
     <td width="100%" style="background-color: #006600;"><img src="http://www.badmintonsport.be/images/pt_transp.gif" width="1px" height="4px" alt=""></td>
    </tr>
    <tr> 
     <td align="center" valign="top" bgcolor="#CCCCCC">
      <table width="100%" border="0" cellspacing="0" cellpadding="3">
       <tr align="center" valign="middle"> 
        <td rowspan="2" align="left">
	     <img src="http://www.badmintonsport.be/images/logow&amp;l-100.gif" width="100" height="80" alt="logo">
	    </td>
        <td>
	     <h4>W&amp;L Badmintonvereniging vzw<br>
	         Mechelsesteenweg 387<br>
             3020 Herent</h4>
		</td>
        <td align="right"><a href="javascript:void();" onclick="return dw_Rotator.doClick(0)"  onmouseover="dw_Rotator.pause(0)" onmouseout="dw_Rotator.resume(0)" onfocus="this.blur()"><img id="sponsors_banner" name="sponsors_banner" src="images/Veltem_Motors_banner.png" alt="sponsors" height="100" width="320" border="0"></a></td>
       </tr>
      </table>
	 </td>
    </tr>
    <tr> 
     <td bgcolor="#336600"><img src="http://www.badmintonsport.be/images/pt_transp.gif" width="1px" height="4px" alt=""></td>
    </tr>
   </table>
  </td>
 </tr>
 <tr> 
    <td width="150" valign="top">
<!-- InstanceBeginEditable name="Floating Menu" -->
<DIV id="divStayTopLeft" style="position:absolute; visibility:inherit;">
<!-- InstanceEndEditable --> 
      <script type="text/javascript" language="JavaScript1.2">
<!--
stm_bm(["W&L menu",400,"","http://www.badmintonsport.be/scripts/blank.gif",0,"","",0,0,250,0,1000,1,0,0]);
stm_bp("p0",[1,4,0,0,1,4,0,7,100,"progid:DXImageTransform.Microsoft.Fade(overlap=.5,enabled=0,Duration=0.30)",-2,"progid:DXImageTransform.Microsoft.Pixelate(MaxSquare=15,enabled=0,Duration=0.50)",-2,50,0,0,"#669999","#006633","",3,0,0,"#ffffff"]);
stm_ai("p0i0",[0,"Home","","",-1,-1,0,"index.php","_self","Startpagina","","","",0,0,0,"","",0,0,0,0,1,"#fffff7",0,"#b5bed6",0,"","",3,3,0,0,"#fffff7","#000000","#000000","#000000","10pt Verdana","10pt Verdana",0,0]);
stm_aix("p0i1","p0i0",[0,"Algemeen","","",-1,-1,0,"","_self","informatie over de club","","","",0,0,0,"http://www.badmintonsport.be/scripts/arrow_r.gif","http://www.badmintonsport.be/scripts/arrow_r.gif",7,7]);
stm_bpx("p1","p0",[1,2,0,0,1,4,0]);
stm_aix("p1i0","p0i0",[0,"Bestuur","","",-1,-1,0,"/algemeen/bestuur.php","_self","Bestuursleden","","","",0,0,0,"","",0,0,0,1]);
stm_bpx("p1","p0",[1,2,0,0,1,4,0,0]);
stm_aix("p2i0","p1i0",[0,"Mededelingen","","",-1,-1,0,"/algemeen/Bestuur/bestuursmededelingen.php","_self","Bestuursmededelingen"]);
stm_ep();
stm_aix("p1i1","p0i0",[0,"Medewerkers","","",-1,-1,0,"/algemeen/medewerkers.php","_self","Medewerkers","","","",0,0,0,"","",0,0,0,1]);
stm_aix("p1i2","p0i0",[0,"Speeluren","","",-1,-1,0,"/algemeen/speeluren.php","_self","Speeluren","","","",0,0,0,"","",0,0,0,1]);
stm_aix("p1i3","p0i1",[0,"Geschiedenis","","",-1,-1,0,"","_self","Geschiedenis","","","",0,0,0,"","",0,0,0,1]);
stm_bpx("p2","p0",[1,2,0,0,1,4,0,0]);
stm_aix("p2i0","p1i0",[0,"Clubkampioenen","","",-1,-1,0,"/algemeen/Geschiedenis/clubkampioenen.php","_self","Winnaars Clubkampioenschap"]);
stm_aix("p2i1","p1i0",[0,"Nationale Kampioenen","","",-1,-1,0,"/algemeen/Geschiedenis/nationalekampioenen.php","_self","Nationale Kampioenen W&L"]);
stm_ep();
stm_aix("p1i4","p0i0",[0,"Sponsors","","",-1,-1,0,"/algemeen/sponsors.php","_self","Sponsors","","","",0,0,0,"","",0,0,0,1]);
stm_aix("p1i5","p0i0",[0,"Artikels","","",-1,-1,0,"/algemeen/artikels.php","_self","Artikels","","","",0,0,0,"","",0,0,0,1]);
stm_ep();
stm_aix("p0i2","p0i0",[0,"Competitie","","",-1,-1,0,"/competitie.php","_self","Competitie: ploegen en uitslagen"]);
stm_aix("p0i3","p0i1",[0,"Tornooien","","",-1,-1,0,"/tornooien/tornooioverzicht.php","_self","Tornooien"]);
stm_bpx("p1","p0",[1,2,0,0,1,4,0]);
stm_aix("p1i0","p0i1",[0,"Inschrijvingen","","",-1,-1,0,"","_self","Inschrijvingen","Schrijf je hier in voor tornooien"]);
stm_bpx("p1","p0",[1,2,0,0,1,4,0,0]);
//-->
</script>
<?php
// create SQL statement 
   $sql = "SELECT id
                , organisatie
                , doelgroep
                , DATE_FORMAT(start_dt, '%d/%m') AS start_dt
                , verzonden
           FROM inschr_torn
		   WHERE DATE_ADD(dt_inschr, INTERVAL 21 DAY ) > NOW()
		   ORDER BY dt_inschr DESC";
   $result = mysql_query ($sql, $badm_db) or badm_mysql_die();
// format results by row
   $teller = 0;
   echo"<script type=\"text/javascript\" language=\"JavaScript1.2\">";
   while ($badm = mysql_fetch_object($result))
   {
      echo "stm_aix(\"p2i";
      echo $teller;
      echo "\",\"p1i0\",[1,\"";
      echo $badm->organisatie;
      if ($badm->doelgroep == 'J' || $badm->doelgroep == 'V')
      {
        echo $badm->doelgroep == 'J' ? " (Jeugd)" : " (Veteranen)";
        if ($badm->verzonden == 0) //inschrijvingsperiode nog niet afgelopen
        {
          echo " <B>".$badm->start_dt."</B>";
        }
        else
        {
          echo "  <IMG src=/images/closed.png>";
        }
      }
      else
      {
       if ($badm->verzonden == 0) //inschrijvingsperiode nog niet afgelopen
        {
          echo " <B>".$badm->start_dt."</B>";
        }
        else
        {
          echo "  <IMG src=/images/closed.png>";
        }
      }
      echo "\",\"\",\"\",-1,-1,0,\"/tornooien/inschrijvingen.php?id=";
      echo $badm->id;
      echo "\",\"_self\",\"Inschrijving ";
      echo $badm->organisatie;
      echo "\"]);\n";
      $teller = $teller + 1;
   }
   echo"</script>\n";
?>
<script type="text/javascript" language="JavaScript1.2">
<!--
stm_ep();
stm_aix("p1i1","p0i0",[0,"Aanvangsuren","","",-1,-1,0,"/tornooien/aanvangsuren.php","_self","Aanvangsuren tornooien"]);
stm_aix("p1i2","p0i0",[0,"Kalender","","",-1,-1,0,"http://www.badmintonsport.be/docs/tornooikalender.pdf","_blank","Tornooikalender"]);
stm_aix("p1i3","p0i1",[0,"Resultaten","","",-1,-1,0,"","_self","Tornooiresultaten W&L spelers"]);
stm_bpx("p2","p0",[1,2,0,0,1,4,0,0]);
//-->
</script>
<?php
// create SQL statement   
 $sql = "SELECT  DISTINCT 
IF(DATE_FORMAT(dt_van, '%m') < 8, DATE_FORMAT(dt_van, '%Y') - 1, DATE_FORMAT(dt_van, '%Y')) AS start
,IF(DATE_FORMAT(dt_van, '%m') < 8, DATE_FORMAT(dt_van, '%Y'), DATE_FORMAT(dt_van, '%Y') + 1) AS einde
           FROM bad_tornooien
           ORDER  BY start DESC ";
   $result = mysql_query ($sql, $badm_db) or badm_mysql_die();
// format results by row
   $teller = 0;
   echo"<script type=\"text/javascript\" language=\"JavaScript1.2\">";
   while ($badm = mysql_fetch_object($result))
   {
       echo "stm_aix(\"p2i";
      echo $teller;
      echo "\",\"p1i0\",[0,\"Seizoen ";
      echo $badm->start.'-'.$badm->einde;
      echo "\",\"\",\"\",-1,-1,0,\"/tornooien/resultaten.php?s=";
      echo $badm->einde;
      echo "&view_id=22\",\"_self\",\"Seizoen ";
      echo $badm->start.'-'.$badm->einde;
      echo "\"]);\n";
      $teller = $teller + 1;
   }
   echo"</script>\n";
   //mysql_free_result($result);
   mysql_close($badm_db);
?>
<script type="text/javascript" language="JavaScript1.2">
<!--
stm_ep();
stm_ep();
stm_aix("p0i4","p0i0",[0,"Spelers","","",-1,-1,0,"/spelers.php","_self","Spelersprofielen"]);
stm_aix("p0i5","p0i1",[0,"Jeugd","","",-1,-1,0,"","_self","Informatie over en voor de jeugdspelers"]);
stm_bpx("p1","p0",[1,2,0,0,1,4,0]);
stm_aix("p1i0","p0i0",[0,"Algemeen","","",-1,-1,0,"/jeugd/jeugd.php","_self","Jeugd trainers"]);
stm_aix("p1i1","p0i0",[0,"Teams","","",-1,-1,0,"/jeugd/jeugdteams.php","_self","Jeugdteams"]);
stm_aix("p1i2","p0i0",[0,"J-team","","",-1,-1,0,"/jeugd/J-team.php","_self","J-team"]);
stm_aix("p1i3","p0i0",[0,"Resultaten","","",-1,-1,0,"/jeugd/resultaten_jeugd.php?s=2008","_self","Resultaten jeugdtornooien W&L spelers"]);
stm_aix("p1i4","p0i0",[0,"Jeugdtornooi","","",-1,-1,0,"/jeugd/jeugdtornooi.php","_self","Jeugdtornooi W&L"]);
stm_ep();
stm_aix("p0i6","p0i0",[0,"Recreanten","","",-1,-1,0,"/recreant.php","_self","Recreanten"]);
stm_aix("p0i7","p0i1",[0,"Foto's","","",-1,-1,0,"http://users.pandora.be/ikkegg","_blank","badminton foto's"]);
//-->
</script>
<?php
/*************************************************************************************************
 * Create a link to every photo directory that exists
 ************************************************************************************************/
/* Get the root dirctory of the website */
  $rootdir = $_SERVER['DOCUMENT_ROOT'];
/* directory where all the photo's are located in subdirectories */
  $dir = $rootdir."/pictures";
/* Get all the subdirectories of the pictures directory */
  include $rootdir."/functies/getDirectoryListing.php";
  $subdirs = getDirectoryListing($dir, "a", 1, 0, "none");
/* Number of subdirs in directory */
  $subdir_cnt = count($subdirs);
/* Display a link to every subdirectory */
  $i = 0;
  foreach ($subdirs as $subdir)
  {
    echo "<script type=\"text/javascript\" language=\"JavaScript1.2\">\n";
    if ($i == 0) // first subdirectory
    {
       echo "stm_bpx(\"p1\",\"p0\",[1,2,0,0,1,4,0]);\n";
    }
	echo "stm_aix(\"p0i".$i."\",\"p0i0\",[0,\"".$subdir."\",\"\",\"\",-1,-1,0,\"/pictures/showThumbnails.php?dir=".$subdir."\",\"_self\",\"".$subdir."\"]);\n";
    if ($i == $subdir_cnt - 1) // last subdirectory
    {
      echo "stm_ep();\n";
    }
    echo"</script>\n";
	$i++;
  }
?>
      <script type="text/javascript" language="JavaScript1.2">
<!--
stm_ai("p0i8",[6,4,"#006633","",0,0,0]);
stm_aix("p0i9","p0i0",[0,"Links","","",-1,-1,0,"/links.php","_self","Links"]);
stm_aix("p0i10","p0i0",[0,"Message board","","",-1,-1,0,"/messageboard/messageboard.php?p=1&m=n&id=0","_blank","Message Board"]);
stm_aix("p0i11","p0i0",[0,"Laatste Nieuws","","",-1,-1,0,"/laatste_nieuws.php","_self","De laatste wijzigingen aan de W&L website"]);
stm_ep();
stm_em();
//-->
</script>
<!-- InstanceBeginEditable name="Floating Menu end" -->
<script>
<!--
JSFX_FloatTopDiv();
-->
</script>
<!-- InstanceEndEditable --> 
      &nbsp;&nbsp; <!-- InstanceBeginEditable name="counter" --><!-- InstanceEndEditable --> 
      <!-- InstanceBeginEditable name="Poll" --><!-- InstanceEndEditable --> <!-- InstanceBeginEditable name="wiens verjaardag is het" --><!-- InstanceEndEditable --> 
    </td>
    <td width="100%" valign="top">
	  <!-- InstanceBeginEditable name="general" -->
      <h1>Spelers</h1>
      <p>Op 
        <?php
   require_once "functies/badm_db.inc.php";
   $badm_db = badm_conn_db();
// DATUM 
   $sql = "SELECT DATE_FORMAT(NOW(), '%d-%m-%Y') as nu
           FROM bad_spelers";
   $result = mysql_query ($sql, $badm_db) or badm_mysql_die();
// format results by row
      $datum = mysql_fetch_object($result);
   echo $datum->nu;
?>
        telt W&amp;L welgeteld 
        <?php
// TOTAAL AANTAL ACTIEVE LEDEN
   $sql2 = "SELECT COUNT(*) as aantal
           FROM bad_spelers
           WHERE eind_dt IS NULL";
   $result2 = mysql_query ($sql2, $badm_db) or badm_mysql_die();
// format results by row
      $totaal = mysql_fetch_object($result2);
   echo $totaal->aantal;
   $tot = $totaal->aantal;
?>
        spelers die allemaal zijn aangesloten bij de VBL. <br>
        De club bestaat uit 
        <?php
// AANTAL A-SPELERS 
   $sql3 = "SELECT COUNT(*) as aantal
           FROM bad_spelers
           WHERE klassement = 'A'
           AND eind_dt IS NULL";
   $result3 = mysql_query ($sql3, $badm_db) or badm_mysql_die();
// format results by row
      $totaal = mysql_fetch_object($result3);
   echo $totaal->aantal;
?>
        A-spelers, 
        <?php
// AANTAL B1-SPELERS
   $sql3 = "SELECT COUNT(*) as aantal
           FROM bad_spelers
           WHERE klassement = 'B1'
           AND eind_dt IS NULL";
   $result3 = mysql_query ($sql3, $badm_db) or badm_mysql_die();
// format results by row
      $totaal = mysql_fetch_object($result3);
   echo $totaal->aantal;
?>
        B1-spelers, 
        <?php
// AANTAL B2-SPELERS
   $sql3 = "SELECT COUNT(*) as aantal
           FROM bad_spelers
           WHERE klassement = 'B2'
           AND eind_dt IS NULL";
   $result3 = mysql_query ($sql3, $badm_db) or badm_mysql_die();
// format results by row
      $totaal = mysql_fetch_object($result3);
   echo $totaal->aantal;
?>
        B2-spelers, 
        <?php
// AANTAL C1-SPELERS
   $sql3 = "SELECT COUNT(*) as aantal
           FROM bad_spelers
           WHERE klassement = 'C1'
           AND eind_dt IS NULL";
   $result3 = mysql_query ($sql3, $badm_db) or badm_mysql_die();
// format results by row
      $totaal = mysql_fetch_object($result3);
   echo $totaal->aantal;
?>
        C1-spelers, 
        <?php
// AANTAL C2-SPELERS
   $sql3 = "SELECT COUNT(*) as aantal
           FROM bad_spelers
           WHERE klassement = 'C2'
           AND eind_dt IS NULL";
   $result3 = mysql_query ($sql3, $badm_db) or badm_mysql_die();
// format results by row
      $totaal = mysql_fetch_object($result3);
   echo $totaal->aantal;
?>
        C2-spelers en 
        <?php
// AANTAL D-SPELERS
   $sql3 = "SELECT COUNT(*) as aantal
           FROM bad_spelers
           WHERE klassement = 'D'
           AND eind_dt IS NULL";
   $result3 = mysql_query ($sql3, $badm_db) or badm_mysql_die();
// format results by row
      $totaal = mysql_fetch_object($result3);
   echo $totaal->aantal;
?>
        D-spelers. <br>
        Als we dit per geslacht gaan bekijken, tellen we 
        <?php
// AANTAL HEREN EN DAMES
   $sql3 = "SELECT COUNT(*) as aantal
           FROM bad_spelers
           WHERE eind_dt IS NULL
           GROUP BY geslacht
           ORDER BY geslacht";
   $result3 = mysql_query ($sql3, $badm_db) or badm_mysql_die();
// format results by row
   $mannen  = mysql_fetch_object($result3);
   $vrouwen = mysql_fetch_object($result3);
   echo $mannen->aantal ." heren en ";
   echo $vrouwen->aantal ." dames."
?>
        <br>
        In totaal hebben we 
        <?php
// create SQL statement 
   $sql3 = "SELECT COUNT(*) as aantal
            FROM bad_spelers
            WHERE (YEAR(NOW()) - YEAR(geb_dt)) < 19
            AND eind_dt IS NULL";
   $result3 = mysql_query ($sql3, $badm_db) or badm_mysql_die();
// format results by row
   $jongeren  = mysql_fetch_object($result3);
   echo $jongeren->aantal;
?>
        jongeren, wat dus betekent dat 
        <?php
   echo round(($jongeren->aantal / $tot) * 100);
?>
        % van onze club uit -19 jarigen bestaat. <br>
        Je vindt hier alle leden van W&amp;L terug met eventueel hun profiel, 
        <a href="/click.php?url=www.badmintonsport.be/spelers/statistieken.html&view_id=25">statistieken</a> en prijzenkast.<br>
        Indien je bepaalde gegevens over jezelf (jawel!) wenst te wijzigen en/of 
        aan te vullen, aarzel dan niet en vraag je username en paswoord aan de 
        <a href="mailto:webmaster@badmintonsport.be">webmaster</a>. </p>
      <table width="80%" border="1" cellspacing="0" cellpadding="0">
        <tr align="center"> 
          <td> <p> <a href="#a">A spelers</a> </p></td>
          <td> <p> <a href="#b1">B1 spelers</a> </p></td>
          <td> <p> <a href="#b2">B2 spelers</a> </p></td>
        </tr>
        <tr align="center"> 
          <td> <p> <a href="#c1">C1 spelers</a> </p></td>
          <td> <p> <a href="#c2">C2 spelers</a> </p></td>
          <td> <p> <a href="#d">D spelers</a> </p></td>
        </tr>
      </table>
      <p>&nbsp; </p>
      <table width="60%" border="1" cellspacing="0" cellpadding="5" ondragstart="return false" onselectstart="return false">
        <tr align="left" valign="top"> 
          <td width="10%"> <p> <a name="a"></a>A spelers </p></td>
          <td width="50%"> <p> 
              <?php
// A-SPELERS
   $sql = "SELECT id
                 ,naam
           FROM bad_spelers
           WHERE klassement = 'A'
           AND eind_dt IS NULL
           ORDER BY achternaam";
   $result = mysql_query ($sql, $badm_db) or badm_mysql_die();
// format results by row
   while ($badm = mysql_fetch_object($result))
   {
      echo "<a href=\"spelers/spelersinfo.php?id=";
      echo $badm->id;
      echo "&a=n\" target=\"_self\">";
      echo $badm->naam;
      echo "</a>";
      //echo "<img src=\"images/info.gif\" width=\"15\" height=\"15\">";
      echo "<br>";
   }
?>
              <br>
            </p></td>
        </tr>
        <tr align="left" valign="top"> 
          <td width="10%"> <p> <a name="b1"></a>B1 spelers </p></td>
          <td width="50%"> <p> 
              <?php
// B1-SPELERS
   $sql = "SELECT id
                 ,naam
           FROM bad_spelers
           WHERE klassement = 'B1'
           AND eind_dt IS NULL
           ORDER BY achternaam";
   $result = mysql_query ($sql, $badm_db) or badm_mysql_die();
// format results by row
   while ($badm = mysql_fetch_object($result))
   {
      echo "<a href=\"spelers/spelersinfo.php?id=";
      echo $badm->id;
      echo "&a=n\" target=\"_self\">";
      echo $badm->naam;
      echo "</a>";
      //echo "<img src=\"images/info.gif\" width=\"15\" height=\"15\">";
      echo "<br>";
   }
?>
              <br>
            </p></td>
        </tr>
        <tr align="left" valign="top"> 
          <td width="10%"> <p> <a name="b2"></a>B2 spelers </p></td>
          <td width="50%"> <p> 
              <?php
// B2-SPELERS
   $sql = "SELECT id
                 ,naam
           FROM bad_spelers
           WHERE klassement = 'B2'
           AND eind_dt IS NULL
           ORDER BY achternaam";
   $result = mysql_query ($sql, $badm_db) or badm_mysql_die();
// format results by row
   while ($badm = mysql_fetch_object($result))
   {
      echo "<a href=\"spelers/spelersinfo.php?id=";
      echo $badm->id;
      echo "&a=n\" target=\"_self\">";
      echo $badm->naam;
      echo "</a>";
      //echo "<img src=\"images/info.gif\" width=\"15\" height=\"15\">";
      echo "<br>";
   }
?>
              <br>
            </p></td>
        </tr>
        <tr align="left" valign="top"> 
          <td width="10%"> <p> <a name="c1"></a>C1 spelers </p></td>
          <td width="50%"> <p>
              <?php
// C1-SPELERS
   $sql = "SELECT id
                 ,naam
           FROM bad_spelers
           WHERE klassement = 'C1'
           AND eind_dt IS NULL
           ORDER BY achternaam";
   $result = mysql_query ($sql, $badm_db) or badm_mysql_die();
// format results by row
   while ($badm = mysql_fetch_object($result))
   {
      echo "<a href=\"spelers/spelersinfo.php?id=";
      echo $badm->id;
      echo "&a=n\" target=\"_self\">";
      echo $badm->naam;
      echo "</a>";
      //echo "<img src=\"images/info.gif\" width=\"15\" height=\"15\">
      echo "<br>";
   }
?>
              <br>
            </p></td>
        </tr>
        <tr align="left" valign="top"> 
          <td width="10%"> <p> <a name="c2"></a>C2 spelers </p></td>
          <td width="50%"> <p>
              <?php
// C2-SPELERS
   $sql = "SELECT id
                 ,naam
           FROM bad_spelers
           WHERE klassement = 'C2'
           AND eind_dt IS NULL
           ORDER BY achternaam";
   $result = mysql_query ($sql, $badm_db) or badm_mysql_die();
// format results by row
   while ($badm = mysql_fetch_object($result))
   {
      echo "<a href=\"spelers/spelersinfo.php?id=";
      echo $badm->id;
      echo "&a=n\" target=\"_self\">";
      echo $badm->naam;
      echo "</a>";
      //echo "<img src=\"images/info.gif\" width=\"15\" height=\"15\">";
      echo "<br>";
   }
?>
              <br>
            </p></td>
        </tr>
        <tr align="left" valign="top"> 
          <td width="10%"> <p> <a name="d"></a>D spelers </p></td>
          <td width="50%"> <p>
<?php
// D-SPELERS
   $sql = "SELECT id
                 ,naam
           FROM bad_spelers
           WHERE klassement = 'D'
           AND eind_dt IS NULL
           ORDER BY achternaam";
   $result = mysql_query ($sql, $badm_db) or badm_mysql_die();
// format results by row
   while ($badm = mysql_fetch_object($result))
   {
      echo "<a href=\"spelers/spelersinfo.php?id=";
      echo $badm->id;
      echo "&a=n\" target=\"_self\">";
      echo $badm->naam;
      //echo "</a><img src=\"images/info.gif\" width=\"15\" height=\"15\">";
      echo "<br>";
   }
?>
              <br>
            </p></td>
        </tr>
      </table>
      <p>&nbsp; </p>
      <!-- InstanceEndEditable -->
	</td>
  </tr>
  <tr> 
    <td height="0"></td>
    <td></td>
  </tr>
  <tr> 
    <td height="1"><img src="http://www.badmintonsport.be/images/spacer.gif" alt="" width="150" height="1"></td>
    <td></td>
  </tr>
</table>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <!--DWLayoutTable-->
  <tr> 
    <td width="100%" height="50" valign="top" bgcolor="#FFFFFF">
	  <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#CCCCCC">
        <!--DWLayoutTable-->
        <tr> 
          <td align="center" bordercolor="#CCCCCC" bgcolor="#006600"><img src="http://www.badmintonsport.be/images/spacer.gif" width="1" height="4" alt=""></td>
        </tr>
        <tr> 
          <td width="100%" align="center" valign="middle" bordercolor="#CCCCCC" bgcolor="#CCCCCC" class="gmenu">
		    Last change: <!-- InstanceBeginEditable name="datum" --> <!-- InstanceEndEditable --> 
            E-mail: <a href="mailto:webmaster@badmintonsport.be">webmaster@badmintonsport.be</a></td>
        </tr>
        <tr>
          <td align="center" bordercolor="#CCCCCC" bgcolor="#006600" class="gmenu"><img src="http://www.badmintonsport.be/images/spacer.gif" width="1" height="4" alt=""></td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</body>
<!-- InstanceEnd --></html>
<?php
  log_website_usage($start_time, null, $badm_db);
  mysql_close($badm_db);
?>