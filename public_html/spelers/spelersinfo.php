<?php

  /* 03/12/2013 Freek Ceymeulen Woonplaats en e-mail adres wordt niet langer getoond na klachten ivm privacy */

  require_once "../functies/website_usage.php";
?>
<!DOCTYPE HTML PUBLIC "-//W3C//Dtd html 4.0 Transitional//EN">
<html lang="nl">
 <head>
  <title>W&amp;L badmintonvereniging vzw - Spelersinfo</title>
  <meta http-equiv=Content-Type content="text/html; charset=iso-8859-1">
  <link href="http://www.badmintonsport.be/css/badminton.css" rel="stylesheet" type="text/css">
  <script type="text/javascript" language="JavaScript" src="http://www.badmintonsport.be/scripts/stm31.js"></script>
 </head>

<body>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
 <tr>
  <td width="150" height="213" valign="top">
   <img src="http://www.badmintonsport.be/images/logow&l-120.gif" alt="W&L logo">
<?php
   // need to call this before doing any other db action because a connection is made in this file
   require "../templates/template.php";
   invoegen_template(); //menu

// Connect to database
   require_once "../functies/badm_db.inc.php";
   $badm_db = badm_conn_db();

  function nvl ($p_in, $p_ifnull)
  {
    if (is_null($p_in) || $p_in == '')
    {
      return $p_ifnull;
    }
    else
    {
      return $p_in;
    }
  }

   if ($_GET['a'] == y)
   // coming from profile maintenance page: apply changes
   {
      $update = " UPDATE bad_spelers
                  SET usid_wijz = '".$_POST["usid"]
                  ."',geb_plaats = '" .$_POST["geb_plaats"]
                  ."',woonplaats = '" .$_POST["woonplaats"]
                  ."',beroep = '".$_POST["beroep"]
                  ."',handig = '".$_POST["handig"]
                  ."',racket = '".$_POST["racket"]
                  ."',email = '".$_POST["email"]
                  ."',website = '".$_POST["website"]."'";
      if ($_POST["geb_dt"] != '')
      {
         $update .= ",geb_dt = '" .$_POST["geb_dt"] ."' ";
      }
      if ($_POST["lengte"] != '')
      {
         $update .= ",lengte = ".$_POST["lengte"];
      }
      if ($_POST["gewicht"] != '')
      {
         $update .= ",gewicht = ".$_POST["gewicht"];
      }
      if ($_POST["start_dt"] != '')
      {
         $update .= ",start_dt = '".$_POST["start_dt"] ."' ";
      }
      if ($_POST["club_dt"] != '')
      {
         $update .= ",club_dt = '".$_POST["club_dt"] ."' ";
      }
      if ($_POST["lidnr"] != '')
      {
         $update .= ",lidnr = ".$_POST["lidnr"];
      }
      if ($_POST["zend_inschrijving"] != '')
      {
         $update .= ",zend_inschrijving = '" .$_POST["zend_inschrijving"] ."' ";
      }
    else
    {
      $update .= ",zend_inschrijving = NULL";
      }
      $update .= " WHERE id = " .$_GET['id'];
      $result = mysql_query($update, $badm_db) or die("Invalid query: " . mysql_error());
      //INSERT
      $dt_van = $_POST["dt_van"];
      $dt_tot = $_POST["dt_tot"];
      if ($dt_van != '')
      // a new tournament victory was entered
      {
         // convert date from DD-MM-YYYY to YYYY-MM-DD if necessary
         if (ereg ("([0-9]{1,2})-([0-9]{1,2})-([0-9]{4})", $dt_van, $regs))
         {
           $dt_van = "$regs[3]-$regs[2]-$regs[1]";
         }
         elseif (ereg ("([0-9]{1,2})/([0-9]{1,2})/([0-9]{4})", $dt_van, $regs))
         {
           $dt_van = "$regs[3]-$regs[2]-$regs[1]";
         }
         if (ereg ("([0-9]{1,2})-([0-9]{1,2})-([0-9]{4})", $dt_tot, $regs))
         {
           $dt_tot = "$regs[3]-$regs[2]-$regs[1]";
         }
         elseif (ereg ("([0-9]{1,2})/([0-9]{1,2})/([0-9]{4})", $dt_tot, $regs))
         {
           $dt_tot = "$regs[3]-$regs[2]-$regs[1]";
         }
         // build insert statement
         $insert = "INSERT INTO bad_tornooien
           (reeks
           ,klassement
           ,categorie
           ,dt_van
           ,dt_tot
           ,plaats
           ,partner
           ,spelers_id
           ,usid_wijz)
                   VALUES ('"
              .$_POST["reeks"]
              ."', '" .$_POST["klassement2"]."', ";
         if ($_POST["categorie"] == 'NVT')
           $insert .= "NULL, ";
         else
           $insert .= "'" .$_POST["categorie"]."', ";
         $insert .= "'" .$dt_van."', ";
         if ($_POST["dt_tot"] == '')
           $insert .= "NULL, ";
         else
           $insert .= "'" .$dt_tot."', ";
         $insert .= "'" .$_POST["plaats"]."', ";
         if ($_POST["partner"] == '')
           $insert .= "NULL, ";
         else
           $insert .= "'" .$_POST["partner"]."', ";
         $insert .= "'" .$_REQUEST["id"]
              ."', '" .$_POST["usid"]
              ."') ";
         // execute insert
         $result2 = mysql_query($insert, $badm_db) or die("Invalid query: " . mysql_error());

         if ($_POST["partner"] != '' && !is_null($_POST["partner"]))
         //het gaat om dubbel of gemengd
         {
            $sql = "SELECT id FROM bad_spelers WHERE naam = '".$_POST["partner"]."' AND eind_dt IS NULL";
            $result = mysql_query($sql, $badm_db) or badm_mysql_die();
            $partner = mysql_fetch_object($result);
            if (mysql_num_rows($result) == 1)
            //als de partner bestaat
            {
               $partner_id = $partner->id;
               $sql = "SELECT id FROM bad_tornooien WHERE plaats = '".$_POST["plaats"]
                    ."' AND dt_van = '".$dt_van
                    ."' AND dt_tot = '".$dt_tot
                    ."' AND reeks = '".$_POST["reeks"]
                    ."' AND klassement = '".$_POST["klassement2"]
                    ."' AND categorie = '".$_POST["categorie"]
                    ."' AND partner = '".$_POST["naam"]."'";
               $result = mysql_query($sql, $badm_db) or badm_mysql_die();
               if (mysql_num_rows($result) == 0)
               //en betreffend record bestaat nog niet
               {
                  $insert = "INSERT INTO bad_tornooien
                            ( reeks
                            , klassement
                            , categorie
                            , dt_van
                            , dt_tot
                            , plaats
                            , partner
                            , spelers_id
                            , usid_wijz)
                            VALUES ('"
              .$_POST["reeks"]
              ."', '" .$_POST["klassement2"]."', ";
         if ($_POST["categorie"] == 'NVT')
           $insert .= "NULL, ";
         else
           $insert .= "'" .$_POST["categorie"]."', ";
         $insert .= "'" .$dt_van."', ";
         if ($_POST["dt_tot"] == '')
           $insert .= "NULL, ";
         else
           $insert .= "'" .$dt_tot."', ";
         $insert .= "'" .$_POST["plaats"]."', ";
         $insert .= "'" .$_POST["naam"]."', ";
         $insert .= "'" .$partner_id
              ."', '" .$_POST["usid"]
              ."') ";
                  // insert victory in partner profile too
                  $result3 = mysql_query($insert, $badm_db) or die("Invalid query: " . mysql_error());
               }
            }
         }
      }
      // PASWOORD EN GEBRUIKERSNAAM WIJZIGEN
      $update_stmt = "";
      if ($_POST["nieuwe_gebruikersnaam"] != '')
      {
        $update_stmt = ",usid = '".$_POST["nieuwe_gebruikersnaam"]."'";
      }
      if ($_POST["nieuw_paswoord1"] != '')
      {
        $update_stmt = $update_stmt.",paswoord = '".$_POST["nieuw_paswoord1"]."'";
      }
      if ($update_stmt != "")
      {
        $update_stmt .= ",usid_wijz = '".nvl($_POST["nieuwe_gebruikersnaam"], $_POST["usid"])."'";
        $update_users = "UPDATE users SET ".substr($update_stmt, 1)." WHERE usid = '".$_POST["usid"]."'";
        $result4 = mysql_query($update_users, $badm_db) or die("Invalid query: " . mysql_error());
      }
   }  // end applying changes

   // Get speler data
   $spelers_id = $_GET['id'];
   if (!is_numeric($spelers_id))
   // Check to avoid people passing sql statements in the url
   {
     echo "Er bestaat geen speler met id ".$spelers_id;
     exit;
   }
   else
   {
     $sql = "SELECT s.naam
                   ,s.geb_plaats
                   ,DATE_FORMAT(s.geb_dt, '%d-%m-%Y') as geb_dt
                   ,s.woonplaats
                   ,s.lengte
                   ,s.gewicht
                   ,s.beroep
                   ,s.handig
                   ,YEAR(s.start_dt) as start_dt
                   ,YEAR(s.club_dt) as club_dt
                   ,s.racket
                   ,s.klassement
                   ,s.lidnr
                   ,s.email
                   ,s.website
                   ,s.usid_wijz
                   ,u.usid
             FROM bad_spelers AS s
             LEFT JOIN users  AS u
             ON s.naam = u.naam
             WHERE s.id = ".$spelers_id;
     $result = mysql_query ($sql, $badm_db) or badm_mysql_die();
   }
   // format results by row
   $speler = mysql_fetch_object($result);
?>

   <BR>
   Gegevens toevoegen<BR>
   of wijzigen?
   <table cellspacing="2">
   <form name="login" action="update.php" method="post">
    <tr class="kleinetekst">
     <td>Username :</td>
     <td><input type="text" name="username" size="7" maxlength="25"></td>
     </tr>
    <tr class="kleinetekst">
     <td>Paswoord :</td>
     <td><input type="password" name="password" size="7" maxlength="25"></td>
    </tr>
    <tr>
     <td><input type="hidden" name="naam" value="<?php echo $speler->naam; ?>">
         <input type="hidden" name="spelers_id" value="<?php echo $_GET['id']; ?>"></td>
     <td><input type="submit" value="login"></td>
    </tr>
   </form>
<?php
mysql_free_result($result);
?>
   </table>
  </td>
  <td>
   <table cellPadding="4" width="98%" border="1" cellspacing="0">
    <tr align="left" class="geel">
     <td colspan="2" bgColor=#666699><p><?php echo $speler->naam;?></p></td>
    </tr>
    <tr class="kleinetekst">
     <td align="right">Geboorteplaats</td>
     <td><?php echo $speler->geb_plaats ?></td>
    </tr>
    <tr class="kleinetekst">
     <td align="right">Geboortedatum</td>
     <td>
<?php
   if ($speler->usid_wijz == $speler->usid)
     // We display date of birth only if the user has updated his personalia at least once
      echo $speler->geb_dt;
?>
     </td>
    </tr>
    <!--<tr class="kleinetekst">
     <td align="right">Woonplaats</td>
     <td><?php echo $speler->woonplaats ?></td>
    </tr>-->
    <tr class="kleinetekst">
     <td align="right">Lengte</td>
     <td><?php echo $speler->lengte ?></td>
    </tr>
    <tr class="kleinetekst">
     <td align="right">Gewicht</td>
     <td><?php
   if ($speler->gewicht != "" AND $speler->gewicht != NULL)
      echo $speler->gewicht ." kg";
?>
     </td>
    </tr>
    <tr class="kleinetekst">
     <td align="right">Beroep/studies</td>
     <td><?php echo $speler->beroep ?></td>
    </tr>
    <tr class="kleinetekst">
     <td align="right">Links-/rechtshandig</td>
     <td>
<?php
   if ($speler->handig == 'R')
   {
      echo "rechtshandig";
   }
   else if ($speler->handig == 'L')
   {
      echo "linkshandig";
   }
?>
     </td>
    </tr>
    <tr class="kleinetekst">
     <td align="right">Speelt badminton sinds</td>
     <td width="71%"><?php echo $speler->start_dt ?></td>
    </tr>
    <tr class="kleinetekst">
     <td align="right">Lid van W&amp;L sinds</td>
     <td width="71%"><?php echo $speler->club_dt ?></td>
    </tr>
    <tr class="kleinetekst">
     <td align="right">Racket</td>
     <td><?php echo $speler->racket; ?></td>
    </tr>
    <tr class="kleinetekst">
     <td align="right">Klassement</td>
     <td><?php echo $speler->klassement ?></td>
    </tr>
    <tr class="kleinetekst">
     <td align="right">Lidnummer</td>
     <td><?php echo $speler->lidnr; ?></td>
    </tr>
<?php
   // Get number of victories in single
   $sql2 = "SELECT COUNT(*) as aantal
            FROM bad_tornooien
            WHERE spelers_id = ".$_GET['id']
          ." AND reeks = 'E'";
   $result2 = mysql_query ($sql2, $badm_db) or badm_mysql_die();
   // format results by row
   $aant = mysql_fetch_object($result2);
   $enkel   = $aant->aantal;
   // Get number of victories in doubles
   $sql2 = "SELECT COUNT(*) as aantal
            FROM bad_tornooien
            WHERE spelers_id = ".$_GET['id']
          ." AND reeks = 'D'";
   $result2 = mysql_query ($sql2, $badm_db) or badm_mysql_die();
   $aant = mysql_fetch_object($result2);
   $dubbel  = $aant->aantal;
   // Get number of victories in mixed
   $sql2 = "SELECT COUNT(*) as aantal
            FROM bad_tornooien
            WHERE spelers_id = ".$_GET['id']
          ." AND reeks = 'G'";
   $result2 = mysql_query ($sql2, $badm_db) or badm_mysql_die();
   $aant = mysql_fetch_object($result2);
   $gemengd = $aant->aantal;
   mysql_free_result($result2);
   // gewonnen tornooien enkel
   $sql3 = "SELECT DATE_FORMAT(dt_van, '%d-%m-%Y') as datum_van
                 , DATE_FORMAT(dt_tot, '%d-%m-%Y') as datum_tot
                 , plaats
                 , klassement
                 , categorie
                 , partner
            FROM bad_tornooien
            WHERE spelers_id = ".$_GET['id']
         ." AND reeks = 'E'
            ORDER BY dt_van DESC";
   $result3 = mysql_query ($sql3,$badm_db) or badm_mysql_die();
?>
    <tr class="kleinetekst">
     <td align="right" valign="top">Aantal gewonnen tornooien enkel</td>
     <td>
<?php
   echo $enkel;
   echo "<br>";
// format results by row
   while ($badm = mysql_fetch_object($result3))
   {
       echo "<br>";
       echo $badm->datum_van;
       if ($badm->datum_tot != '')
          echo " - ".$badm->datum_tot;
       else
          echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
       echo " - ";
       echo $badm->plaats;
       if (strlen($badm->categorie) == 0)
         echo " (".$badm->klassement.")";
       else
         echo " (".$badm->categorie.")";
   }
   mysql_free_result($result3);
?>
    </tr>
     <tr class="kleinetekst">
     <td align="right" valign="top">Aantal gewonnen tornooien dubbel</td>
     <td>
<?php
   echo $dubbel;
   echo "<br>";
// gewonnen tornooien dubbel
   $sql4 = "SELECT DATE_FORMAT(dt_van, '%d-%m-%Y') as datum_van
                  ,DATE_FORMAT(dt_tot, '%d-%m-%Y') as datum_tot
                  ,plaats
                  ,klassement
                  ,categorie
                  ,partner
            FROM bad_tornooien
            WHERE spelers_id = ".$_GET['id']
         ." AND reeks = 'D'
            ORDER BY dt_van DESC";
   $result4 = mysql_query ($sql4,$badm_db) or badm_mysql_die();
// format results by row
   while ($badm = mysql_fetch_object($result4))
   {
       echo "<br>";
       echo $badm->datum_van;
       if ($badm->datum_tot != '')
          echo " - ".$badm->datum_tot;
       else
          echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
       echo " - ".$badm->plaats;
       if (strlen($badm->categorie) == 0)
         echo " (".$badm->klassement.")";
       else
         echo " (".$badm->categorie.")";
       echo " - met ";
       echo $badm->partner;
   }
   mysql_free_result($result4);
?>
     </td>
    </tr>
    <tr class="kleinetekst">
     <td align="right" valign="top">Aantal gewonnen tornooien gemengd</td>
     <td>
<?php
   echo $gemengd;
   echo "<br>";
// gewonnen tornooien gemengd
   $sql5 = "SELECT DATE_FORMAT(dt_van, '%d-%m-%Y') as datum_van
                  ,DATE_FORMAT(dt_tot, '%d-%m-%Y') as datum_tot
                  ,plaats
                  ,klassement
                  ,categorie
                  ,partner
            FROM bad_tornooien
            WHERE spelers_id = ".$_GET['id']
         ." AND reeks = 'G'
            ORDER BY dt_van DESC";
   $result5 = mysql_query ($sql5,$badm_db) or badm_mysql_die();
// format results by row
   while ($badm = mysql_fetch_object($result5))
   {
       echo "<br>";
       echo $badm->datum_van;
       if ($badm->datum_tot != '')
          echo " - ".$badm->datum_tot;
       else
          echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
       echo " - ".$badm->plaats;
       if (strlen($badm->categorie) == 0)
         echo " (".$badm->klassement.")";
       else
         echo " (".$badm->categorie.")";
       echo " - met ";
       echo $badm->partner;
    }
   mysql_free_result($result5);
?>
     </td>
    </tr>
    <!--<tr class="kleinetekst">
     <td align="right">E-mail</td>
     <td>
<?php
if (strlen($speler->email) > 0)
   {
      // Ik bouw het e-mail adres op de client via Javascript, op die manier kunnen robots het niet herkennen en vermijden we spam.
      //echo "<script type=\"text/javascript\" language=\"JavaScript\">\n";
      //echo "<!--\n";
      //echo " naam = '".htmlspecialchars(substr($speler->email, 0, strpos($speler->email, "@")))."';\n";
      //echo " akrol = '@';\n";
      //echo " domein = '".htmlspecialchars(substr($speler->email, strpos($speler->email, "@") + 1))."';\n";
      //echo " document.write('<a href=\"mailto:'+naam+akrol+domein+'\">'+naam+akrol+domein+'</a>');\n";
      //echo "-->\n";
      //echo "</script>\n";
      //echo "<noscript>\n";
      //echo "Email adres afgeschermd mbv Javascript.<br>Laat Javascript toe voor contact.\n";
      //echo "</noscript>\n";
   }
?>
     </td>
    </tr>-->
    <tr class="kleinetekst">
     <td align="right">Website</td>
     <td><a href="<?php echo $speler->website; ?>" target="_blank"><?php echo $speler->website;?></A>
     </td>
    </tr>
   </table>
  </td>
 </tr>
</table>
</body>
</html>
<?php
  log_website_usage($start_time, $spelers_id, $badm_db);
?>