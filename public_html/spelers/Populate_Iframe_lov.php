<html>
<head>
<style>
 input    { border : none}
 td.label { border-bottom-style : solid;
            border-bottom-width : thin;
            border-bottom-color : black }
</style>
<script language="JavaScript" type="text/javascript">
// original background color
var orgBColor;
// original text color
var orgTColor;
// original font weight
var orgFWeight;
// Highlight an element when clicked on it
// and restore the previously highlighted item to its original settings
function highlight(element)
{
  //preEl is a variable defined in  the parent window, so we call it by specifying 'parent'
  if(typeof(parent.preEl)!= 'undefined') //some element is already highlighted
  {
    // restore this element to its original settings
    parent.preEl.style.backgroundColor = orgBColor;
    parent.preEl.style.color = orgTColor;
    //parent.preEl.style.fontWeight = orgFWeight;
  }
  // store settings of new element
  orgBColor = element.style.backgroundColor;
  orgTColor = element.style.color;
  orgFWeight = element.style.fontWeight;
  // highlight new element
  element.style.backgroundColor = 'blue';
  element.style.color = 'white';
  //element.style.fontWeight = 'bolder';
  // set previous element to new element
  parent.preEl = element;
}

</script>
</head>
<BODY bgcolor="#BBBBBB">
<!--
|| This form is submitted when the 'Find' button in the parent window is clicked.
|| On submission a select statement is conducted that will get all the values that correspond to the string
|| entered in the Find field in the parent window, and these values will be displayed here in the iFrame
-->
<FORM name="data" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
 <TABLE width="100%">
  <TR>
   <TD class=label valign="top">Tornooi</TD>
  </TR>
  <TR>
   <TD>
<?php
/* Display the requested values. */
if (!isset($_POST['p_search_string']))
{
  $search_string = "%";
}
else
{
  $search_string = $_POST['p_search_string'];
}
    require_once "../functies/badm_db.inc.php";
    $badm_db = badm_conn_db();
// Create SQL statement
 $sql = 'SELECT DISTINCT plaats
           FROM bad_tornooien
          WHERE plaats LIKE "'.$search_string.'"
          ORDER BY plaats';
 $result = mysql_query ($sql, $badm_db) or badm_mysql_die();
// format results by row
 $found = FALSE;
 while ($tornooi = mysql_fetch_object($result))
 {
    echo "    <INPUT type=\"text\" size=\"40\" value=\"".$tornooi->plaats."\" onClick=\"highlight(this);\" onMouseOver=\"this.style.cursor='pointer'\"></BR>";
    $found = TRUE;
 }
 if (!$found)
 {
   echo "No data found";
 }
?>
   </TD>
  </TR>
 </TABLE>
<!--
|| These hidden fields are used to save the parameter values received from the parent window
|| and to transmit these values to the populate_iframe procedure on submit.
-->
 <INPUT type="hidden" name="p_search_string">
</FORM>
</BODY>
</HTML>