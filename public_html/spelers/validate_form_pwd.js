function validateForm()
{

//check of dt_van leeg is
   if (document.update_spelers.elements['dt_van'].value != "")
   {
   //check of plaats leeg is
      if (document.update_spelers.elements['plaats'].value == "")
      {
         alert("Vul een waarde in voor het veld Naam en plaats");
         document.update_spelers.elements['plaats'].focus();
         return(false);
      }
   }

//check of Email juist is
   if (document.update_spelers.elements['email'].value != "")
   {
      if (!checkEmail(document.update_spelers.elements['email'].value))
      {
         alert("Het e-mail adres dat je ingevuld hebt is niet correct");
         document.update_spelers.elements['email'].focus();
         return(false);
      }
   }

//check of nieuw paswoord correct is
  if (document.update_spelers.elements['nieuw_paswoord1'].value != "")
  {
    if (document.update_spelers.elements['nieuw_paswoord1'].value != document.update_spelers.elements['nieuw_paswoord2'].value)
    {
      alert("De nieuwe paswoorden zijn niet gelijk");
      document.update_spelers.elements['nieuw_paswoord1'].focus();
      return(false);
    }
  }
  return true;
}

// deze functie controleert de juistheid van het e-mailadres op meerdere punten
function checkEmail(emailAddress)
{
   var foundAtSymbol = 0;
   var foundDot = 0;
   var md;

// Go through each character in the email address.
   for (var x=0; x<emailAddress.length - 1; x++)
   {
      md = emailAddress.substr(x, 1);

   // Is the character an @ symbol?
      if (md == '@')
         foundAtSymbol++;

   // Count how many dots there are after the @ symbol.
      if (md == '.' && foundAtSymbol == 1)
         foundDot++;
   }

// Is there only one @ symbol, and are there more than one dots?
   if (foundDot > 0 && foundAtSymbol == 1)
   {
      return true;
   }
   else
   {
      return false;
   }
}
var submitcount = 0;
function checkSubmitCount()
{
  if (validateForm())
  {
    submitcount = submitcount + 1;
    if (submitcount == 1)
    {
      document.update_spelers.submit();
    }
    else
    {
      if (submitcount == 2)
      {
        alert("Je hebt deze gegevens al verzonden");
      }
      else
      {
        alert("Je hebt deze gegevens nu al" + submitcount.toString() + " keer verzonden");
      }
    }
  }
}