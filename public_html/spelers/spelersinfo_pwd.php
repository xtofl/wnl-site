<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
 <HEAD>
  <TITLE>W&amp;L - spelersinfo</TITLE>
  <META http-equiv=Content-Type content="text/html; charset=iso-8859-1">
  <link href="../css/badminton.css" rel="stylesheet" type="text/css">
  <script src="../scripts/stm31.js"></script>
 </HEAD>

<BODY text=#000000 bgColor=#ffffff>
<TABLE width="100%" border="0" cellpadding="0" cellspacing="0">
 <TR>
  <TD width="150" height="213" valign="top">
   <IMG src="../images/logow&l-120.gif">
<?php
   // need to call this before doing any other db action because a connection is made in this file
   require "../templates/template.php";
   invoegen_template(); //menu

// Connect to database
   //require "../badm_db.inc.php";
   $badm_db = badm_conn_db();

   if ($_GET['a'] == y)
   // coming from profile maintenance page: apply changes
   {
      $update = " UPDATE bad_spelers 
                  SET usid_wijz = '".$_POST["usid"]
                  ."',geb_plaats = '" .$_POST["geb_plaats"]
                  ."',woonplaats = '" .$_POST["woonplaats"]
                  ."',beroep = '".$_POST["beroep"]
                  ."',handig = '".$_POST["handig"]
                  ."',racket = '".$_POST["racket"]
                  ."',email = '".$_POST["email"]
                  ."',website = '".$_POST["website"]."'";
      if ($_POST["geb_dt"] != '')
      {
         $update .= ",geb_dt = '" .$_POST["geb_dt"] ."' ";
      }
      if ($_POST["lengte"] != '')
      {
         $update .= ",lengte = ".$_POST["lengte"];
      }
      if ($_POST["gewicht"] != '')
      {
         $update .= ",gewicht = ".$_POST["gewicht"];
      }
      if ($_POST["start_dt"] != '')
      {
         $update .= ",start_dt = '".$_POST["start_dt"] ."' ";
      }
      if ($_POST["club_dt"] != '')
      {
         $update .= ",club_dt = '".$_POST["club_dt"] ."' ";
      }
      if ($_POST["lidnr"] != '')
      {
         $update .= ",lidnr = ".$_POST["lidnr"];
      }
      $update .= " WHERE id = " .$_GET['id'];
      $result = mysql_query($update, $badm_db) or die("Invalid query: " . mysql_error());
      //INSERT
      $dt_van = $_POST["dt_van"];
      $dt_tot = $_POST["dt_tot"];
      if ($dt_van != '')
      // a new tournament victory was entered
      {
         // convert date from DD-MM-YYYY to YYYY-MM-DD if necessary
         if (ereg ("([0-9]{1,2})-([0-9]{1,2})-([0-9]{4})", $dt_van, $regs))
         {
           $dt_van = "$regs[3]-$regs[2]-$regs[1]";
         }
         elseif (ereg ("([0-9]{1,2})/([0-9]{1,2})/([0-9]{4})", $dt_van, $regs))
         {
           $dt_van = "$regs[3]-$regs[2]-$regs[1]";
         }
         if (ereg ("([0-9]{1,2})-([0-9]{1,2})-([0-9]{4})", $dt_tot, $regs))
         {
           $dt_tot = "$regs[3]-$regs[2]-$regs[1]";
         }
         elseif (ereg ("([0-9]{1,2})/([0-9]{1,2})/([0-9]{4})", $dt_tot, $regs))
         {
           $dt_tot = "$regs[3]-$regs[2]-$regs[1]";
         }
         // build insert statement
         $insert = "INSERT INTO bad_tornooien
           (reeks
           ,klassement
           ,categorie
           ,dt_van
           ,dt_tot
           ,plaats
           ,partner
           ,spelers_id
           ,usid_wijz) 
                   VALUES ('"
              .$_POST["reeks"]
              ."', '" .$_POST["klassement2"]."', ";
         if ($_POST["categorie"] == 'NVT')
           $insert .= "NULL, ";
         else
           $insert .= "'" .$_POST["categorie"]."', ";
         $insert .= "'" .$dt_van."', ";
         if ($_POST["dt_tot"] == '')
           $insert .= "NULL, ";
         else
           $insert .= "'" .$dt_tot."', ";
         $insert .= "'" .$_POST["plaats"]."', ";
         if ($_POST["partner"] == '')
           $insert .= "NULL, ";
         else
           $insert .= "'" .$_POST["partner"]."', ";
         $insert .= "'" .$id
              ."', '" .$_POST["usid"]
              ."') ";
         // execute insert
         $result2 = mysql_query($insert, $badm_db) or die("Invalid query: " . mysql_error());

         if ($_POST["partner"] != '' && !is_null($_POST["partner"]))
         //het gaat om dubbel of gemengd
         {
            $sql = "SELECT id FROM bad_spelers WHERE naam = '".$_POST["partner"]."'";
            $result = mysql_query($sql, $badm_db) or badm_mysql_die();
            $partner = mysql_fetch_object($result);
            if (mysql_num_rows($result) == 1)
            //als de partner bestaat
            {
               $partner_id = $partner->id;
               $sql = "SELECT id FROM bad_tornooien WHERE plaats = '".$_POST["plaats"]
                    ."' AND dt_van = '".$dt_van
                    ."' AND dt_tot = '".$dt_tot
                    ."' AND reeks = '".$_POST["reeks"]
                    ."' AND klassement = '".$_POST["klassement2"]
                    ."' AND categorie = '".$_POST["categorie"]
                    ."' AND partner = '".$_POST["naam"]."'";
               $result = mysql_query($sql, $badm_db) or badm_mysql_die();
               if (mysql_num_rows($result) == 0)
               //en betreffend record bestaat nog niet
               {
                  $insert = "INSERT INTO bad_tornooien
                            ( reeks
                            , klassement
                            , categorie
                            , dt_van
                            , dt_tot
                            , plaats
                            , partner
                            , spelers_id
                            , usid_wijz) 
                            VALUES ('"
              .$_POST["reeks"]
              ."', '" .$_POST["klassement2"]."', ";
         if ($_POST["categorie"] == 'NVT')
           $insert .= "NULL, ";
         else
           $insert .= "'" .$_POST["categorie"]."', ";
         $insert .= "'" .$dt_van."', ";
         if ($_POST["dt_tot"] == '')
           $insert .= "NULL, ";
         else
           $insert .= "'" .$dt_tot."', ";
         $insert .= "'" .$_POST["plaats"]."', ";
         $insert .= "'" .$_POST["naam"]."', ";
         $insert .= "'" .$partner_id
              ."', '" .$_POST["usid"]
              ."') ";
                  // insert victory in partner profile too
                  $result3 = mysql_query($insert, $badm_db) or die("Invalid query: " . mysql_error());
               }
            }
         }
      }
      // PASWOORD EN GEBRUIKERSNAAM WIJZIGEN
      $update_stmt = "";
      if ($_POST["nieuwe_gebruikersnaam"] != '')
      {
        $update_stmt = ",usid = '".$_POST["nieuwe_gebruikersnaam"]."'";
      }
      if ($_POST["nieuw_paswoord1"] != '')
      {
        $update_stmt = $update_stmt.",paswoord = '".$_POST["nieuw_paswoord1"]."'";
      }
      if ($update_stmt != "")
      {
        $update_users = "UPDATE users SET ".substr($update_stmt, 1)." WHERE usid = '".$_POST["usid"]."'";
        $result4 = mysql_query($update_users, $badm_db) or die("Invalid query: " . mysql_error());
      }
   }  // end applying changes

   // Get speler data
   $spelers_id = $_GET['id'];
   if (!is_numeric($spelers_id))
   // Check to avoid people passing sql statements in the url
   {
     echo "Er bestaat geen speler met id ".$spelers_id;
     exit;
   }
   else
   {
     $sql = "SELECT s.naam
                   ,s.geb_plaats
                   ,DATE_FORMAT(s.geb_dt, '%d-%m-%Y') as geb_dt
                   ,s.woonplaats
                   ,s.lengte
                   ,s.gewicht
                   ,s.beroep
                   ,s.handig
                   ,YEAR(s.start_dt) as start_dt
                   ,YEAR(s.club_dt) as club_dt
                   ,s.racket
                   ,s.klassement
                   ,s.lidnr
                   ,s.email
                   ,s.website
                   ,u.naam AS usid
             FROM bad_spelers AS s
                , users       AS u
             WHERE s.id = ".$spelers_id."
             AND   s.usid_wijz = u.usid";
     $result = mysql_query ($sql, $badm_db) or badm_mysql_die();
   }
   // format results by row
   $speler = mysql_fetch_object($result);
?>

   <BR>
   Gegevens toevoegen<BR>
   of wijzigen?
   <table cellspacing="2">
   <form name="login" action="update_pwd.php" method="post">
    <tr class="kleinetekst">
     <td>Username :</td>
     <td><input type="text" name="username" size="7" maxlength="25"></td>
     </tr>
    <tr class="kleinetekst">
     <td>Paswoord :</td>
     <td><input type="password" name="password" size="7" maxlength="25"></td>
    </tr>
    <tr>
     <td><input type="hidden" name="naam" value="<?php echo $speler->naam; ?>">
         <input type="hidden" name="spelers_id" value="<?php echo $_GET['id']; ?>"></td>
     <td><input type="submit" value="login"></td>
    </tr>
   </form>
<?php
mysql_free_result($result);
?>
   </table>
  </TD>
  <TD>
   <TABLE cellPadding="4" width="98%" border="1" cellspacing="0">
    <TR align="left" class="geel">
     <TD colspan="2" bgColor=#666699><p><?php echo $speler->naam;?></p></TD>
    </TR>
    <TR class="kleinetekst">
     <TD align="right">Geboorteplaats</TD>
     <TD><?php echo $speler->geb_plaats ?></TD>
    </TR>
    <TR class="kleinetekst">
     <TD align="right">Geboortedatum</TD>
     <TD>
<?php
   if ($speler->naam == $speler->usid)
     // We display date of birth only if the user has updated his personalia at least once
      echo $speler->geb_dt;
?>
     </TD>
    </TR>
    <TR class="kleinetekst">
     <TD align="right">Woonplaats</TD>
     <TD><?php echo $speler->woonplaats ?></TD>
    </TR>
    <TR class="kleinetekst">
     <TD align="right">Lengte</TD>
     <TD><?php echo $speler->lengte ?></TD>
    </TR>
    <TR class="kleinetekst">
     <TD align="right">Gewicht</TD>
     <TD><?php
   if ($speler->gewicht != "" AND $speler->gewicht != NULL)
      echo $speler->gewicht ." kg";
?>
     </TD>
    </TR>
    <TR class="kleinetekst">
     <TD align="right">Beroep/studies</TD>
     <TD><?php echo $speler->beroep ?></TD>
    </TR>
    <TR class="kleinetekst">
     <TD align="right">Links-/rechtshandig</TD>
     <TD>
<?php
   if ($speler->handig == 'R')
   {
      echo "rechtshandig";
   }
   else if ($speler->handig == 'L')
   {
      echo "linkshandig";
   }
?>
     </TD>
    </TR>
    <TR class="kleinetekst">
     <TD align="right">Speelt badminton sinds</TD>
     <TD width="71%"><?php echo $speler->start_dt ?></TD>
    </TR>
    <TR class="kleinetekst">
     <TD align="right">Lid van W&amp;L sinds</TD>
     <TD width="71%"><?php echo $speler->club_dt ?></TD>
    </TR>
    <TR class="kleinetekst">
     <TD align="right">Racket</TD>
     <TD><?php echo $speler->racket; ?></TD>
    </TR>
    <TR class="kleinetekst">
     <TD align="right">Klassement</TD>
     <TD><?php echo $speler->klassement ?></TD>
    </TR>
    <TR class="kleinetekst">
     <TD align="right">Lidnummer</TD>
     <TD><?php echo $speler->lidnr; ?></TD>
    </TR>
<?php
   // Get number of victories in single
   $sql2 = "SELECT COUNT(*) as aantal
            FROM bad_tornooien
            WHERE spelers_id = ".$_GET['id']
          ." AND reeks = 'E'";
   $result2 = mysql_query ($sql2, $badm_db) or badm_mysql_die();
   // format results by row
   $aant = mysql_fetch_object($result2);
   $enkel   = $aant->aantal;
   // Get number of victories in doubles
   $sql2 = "SELECT COUNT(*) as aantal
            FROM bad_tornooien
            WHERE spelers_id = ".$_GET['id']
          ." AND reeks = 'D'";
   $result2 = mysql_query ($sql2, $badm_db) or badm_mysql_die();
   $aant = mysql_fetch_object($result2);
   $dubbel  = $aant->aantal;
   // Get number of victories in mixed
   $sql2 = "SELECT COUNT(*) as aantal
            FROM bad_tornooien
            WHERE spelers_id = ".$_GET['id']
          ." AND reeks = 'G'";
   $result2 = mysql_query ($sql2, $badm_db) or badm_mysql_die();
   $aant = mysql_fetch_object($result2);
   $gemengd = $aant->aantal;
   mysql_free_result($result2);
   // gewonnen tornooien enkel
   $sql3 = "SELECT DATE_FORMAT(dt_van, '%d-%m-%Y') as datum_van
                 , DATE_FORMAT(dt_tot, '%d-%m-%Y') as datum_tot
                 , plaats
                 , klassement
                 , categorie
                 , partner
            FROM bad_tornooien
            WHERE spelers_id = ".$_GET['id']
         ." AND reeks = 'E'
            ORDER BY dt_van DESC";
   $result3 = mysql_query ($sql3,$badm_db) or badm_mysql_die();
?>
    <TR class="kleinetekst">
     <TD align="right" valign="top">Aantal gewonnen tornooien enkel</TD>
     <TD>
<?php
   echo $enkel;
   echo "<br>";
// format results by row
   while ($badm = mysql_fetch_object($result3))
   { 
       echo "<br>";
       echo $badm->datum_van;
       if ($badm->datum_tot != '')
          echo " - ".$badm->datum_tot;
       else
          echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
       echo " - ";
       echo $badm->plaats;
       if (strlen($badm->categorie) == 0)
         echo " (".$badm->klassement.")";
       else
         echo " (".$badm->categorie.")";
   }
   mysql_free_result($result3);
?>
    </TR>
     <TR class="kleinetekst">
     <TD align="right" valign="top">Aantal gewonnen tornooien dubbel</TD>
     <TD>
<?php
   echo $dubbel;
   echo "<br>";
// gewonnen tornooien dubbel
   $sql4 = "SELECT DATE_FORMAT(dt_van, '%d-%m-%Y') as datum_van
                  ,DATE_FORMAT(dt_tot, '%d-%m-%Y') as datum_tot
                  ,plaats
                  ,klassement
                  ,categorie
                  ,partner
            FROM bad_tornooien
            WHERE spelers_id = ".$_GET['id']
         ." AND reeks = 'D'
            ORDER BY dt_van DESC";
   $result4 = mysql_query ($sql4,$badm_db) or badm_mysql_die();
// format results by row
   while ($badm = mysql_fetch_object($result4))
   { 
       echo "<br>";
       echo $badm->datum_van;
       if ($badm->datum_tot != '')
          echo " - ".$badm->datum_tot;
       else
          echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
       echo " - ".$badm->plaats;
       if (strlen($badm->categorie) == 0)
         echo " (".$badm->klassement.")";
       else
         echo " (".$badm->categorie.")";
       echo " - met ";
       echo $badm->partner;
   }
   mysql_free_result($result4);
?>
     </TD>
    </TR>
    <TR class="kleinetekst">
     <TD align="right" valign="top">Aantal gewonnen tornooien gemengd</TD>
     <TD>
<?php
   echo $gemengd;
   echo "<br>";
// gewonnen tornooien gemengd
   $sql5 = "SELECT DATE_FORMAT(dt_van, '%d-%m-%Y') as datum_van
                  ,DATE_FORMAT(dt_tot, '%d-%m-%Y') as datum_tot
                  ,plaats
                  ,klassement
                  ,categorie
                  ,partner
            FROM bad_tornooien
            WHERE spelers_id = ".$_GET['id']
         ." AND reeks = 'G'
            ORDER BY dt_van DESC";
   $result5 = mysql_query ($sql5,$badm_db) or badm_mysql_die();
// format results by row
   while ($badm = mysql_fetch_object($result5))
   { 
       echo "<br>";
       echo $badm->datum_van;
       if ($badm->datum_tot != '')
          echo " - ".$badm->datum_tot;
       else
          echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
       echo " - ".$badm->plaats;
       if (strlen($badm->categorie) == 0)
         echo " (".$badm->klassement.")";
       else
         echo " (".$badm->categorie.")";
       echo " - met ";
       echo $badm->partner;
    }
   mysql_free_result($result5);
?>
     </TD>
    </TR>
    <TR class="kleinetekst">
     <TD align="right">E-mail</TD>
     <TD><?php echo $speler->email; ?></TD>
    </TR>
    <TR class="kleinetekst">
     <TD align="right">Website</TD>
     <TD><A HREF="<?php echo $speler->website; ?>" TARGET="_blank"><?php echo $speler->website;?></A>
     </TD>
    </TR>
   </TABLE>
  </TD>
 </TR>
</TABLE>
</BODY>
</HTML>