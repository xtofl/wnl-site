<?php
  require_once "../functies/website_usage.php";
  setcookie ("usid", $_POST["username"], time()+7948800); //3 maanden
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="nl">
<head>
<title>W&amp;L Badmintonvereniging vzw - Spelersprofiel bewerken</title>
<meta http-equiv=Content-Type content="text/html; charset=iso-8859-1">
<link href="http://www.badmintonsport.be/css/badminton.css" rel="stylesheet" type="text/css">
<style type="text/css">
.kleingeel {
    font-family: Verdana, Arial, Helvetica, sans-serif;
    font-size:   10pt;
    font-weight: bold;
    color:       #FFCC00;
}
</style>
<script language="JavaScript" type="text/javascript" src="http://www.badmintonsport.be/scripts/validate_form.js"></script>
<script language="JavaScript" type="text/javascript" src="http://www.badmintonsport.be/scripts/CalendarPopup.js"></script>
<script language="JavaScript" type="text/javascript" src="http://www.badmintonsport.be/scripts/modal_dialog.js"></script>
<script language="JavaScript" type="text/javascript">
<!--
  document.write(getCalendarStyles());
  var cal = new CalendarPopup("datediv");
  cal.showNavigationDropdowns();

 // dit zorgt ervoor dat wanneer je vanuit de spelersinfo pagina op de "back"-toets klikt op terug te keren naar deze pagina
 // dat dat niet lukt.  Je blijft op de spelersinfo pagina.
  javascript:window.history.forward(1);

  var plaats = "";
  // Calls a dialog window with LOV-values for this field
function showLov()
{
  if (navigator.appName == "Microsoft Internet Explorer")
  {
    plaats = window.showModalDialog("lovMS.html", window, "status:no; resizable:no; font-size:16px; dialogWidth:20em; dialogHeight: 20em");
    document.update_spelers.plaats.value = plaats;
  }
  else
  {
    openDialog('lovNS.php', 300, 400, setPrefs);
  }
}
// Function to run upon closing the dialog with "OK".
function setPrefs()
{
  // We are just displaying the returned value in a text box.
  document.update_spelers.plaats.value = dialogWin.returnedValue;
}

  var oPopup = window.createPopup();
  function showHelp()
  {
  var oPopupBody = oPopup.document.body;
  oPopupBody.style.backgroundColor = "lightyellow";
  oPopupBody.style.border = "solid black 1px";
  oPopupBody.style.margin = "2px";
  oPopupBody.innerHTML = oHelp.innerHTML;
  oPopup.show(0, 0, 500, 0);
  var realHeight = oPopupBody.scrollHeight - 50;
  // Hides the dimension detector popup object.
  oPopup.hide();
  oPopupBody.style.overflow = "auto";
  // Shows the actual popup object with correct height.
  oPopup.show(700, 5, 500, realHeight, document.body);
  }
-->
</script>
</head>

<body>
<?php
   require_once "../functies/badm_db.inc.php";
   $badm_db = badm_conn_db();

/***************************************
 * display_form
 **************************************/
function display_form($conn)
{
  $id = $_POST["spelers_id"];
  $sql = "SELECT naam
                ,geb_plaats
                ,DATE_FORMAT(geb_dt, '%Y/%m/%d') AS geb_dt
                ,woonplaats
                ,lengte
                ,gewicht
                ,beroep
                ,handig
                ,DATE_FORMAT(start_dt, '%Y/%m/%d') AS start_dt
                ,DATE_FORMAT(club_dt, '%Y/%m/%d') AS club_dt
                ,racket
                ,klassement
                ,lidnr
                ,email
                ,website
				,zend_inschrijving
            FROM bad_spelers
           WHERE id = ".$id;
  $result = mysql_query ($sql, $conn) or badm_mysql_die();
  // format results by row
  $speler = mysql_fetch_object($result);
  echo "<table border=\"0\" cellpadding=\"0\" cellspacing=\"1\">\n";
  echo " <tr style=\"border-color:#CCCCCC;background-color:#666699;\">\n";
  echo "  <td align=\"center\" colspan=\"2\" class=\"kleingeel\"><b>";
  echo $speler->naam;
  echo "</b></td></tr>\n";
  echo "<form name=\"update_spelers\" action=\"spelersinfo.php?id=";
  echo $id;
  echo "&a=y\" method=\"post\">\n";
  echo "<input type=\"hidden\" name=\"naam\" value=\"";
  echo $speler->naam;
  echo "\">\n";
  echo "<input type=\"hidden\" name=\"usid\" value=\"";
  echo $_POST["username"];
  echo "\"><tr><td>Geboorteplaats :</td>\n";
  echo "<td><input type=\"text\" name=\"geb_plaats\" size=\"36\" maxlength=\"36\" value=\"";
  echo $speler->geb_plaats;
  echo "\"></td></tr><tr><td>Geboortedatum :</td>";
  if ($speler->geb_dt == '')
  {
    echo "<td><input type=\"text\" name=\"geb_dt\" size=\"10\" maxlength=\"10\" value=\"";
    echo $speler->geb_dt;
    echo "\"></td></tr>\n";
  }
  else
  {
    echo "<td><input DISABLED type=\"text\" name=\"geb_dt\" size=\"10\" maxlength=\"10\" value=\"";
    echo $speler->geb_dt;
    echo "\" READONLY> (yyyy-mm-dd)</td></tr>\n";
  }
  echo "<tr><td>Woonplaats :</td>\n";
  echo "<td><input type=\"text\" name=\"woonplaats\" size=\"36\" maxlength=\"36\" value=\"";
  echo $speler->woonplaats;
  echo "\"></td></tr><tr><td>Lengte :</td>\n";
  echo "<td><input type=\"text\" name=\"lengte\" size=\"4\" maxlength=\"4\" value=\"";
  echo $speler->lengte;
  echo "\"> (in meter, bv. 1.75)</td></tr><tr><td>Gewicht :</td>\n";
  echo "<td><input type=\"text\" name=\"gewicht\" size=\"3\" maxlength=\"3\" value=\"";
  echo $speler->gewicht;
  echo "\"> (in kg)</td></tr><tr><td>Beroep/studies :</td>\n";
  echo "<td><input type=\"text\" name=\"beroep\" size=\"50\" maxlength=\"75\" value=\"";
  echo $speler->beroep;
  echo "\"></td></tr><tr><td>Links-/rechtshandig :</td><td>\n";
  if ($speler->handig == 'R')
  {
    echo "<input type=\"radio\" name=\"handig\" value=\"R\" CHECKED>Rechtshandig";
    echo "<input type=\"radio\" name=\"handig\" value=\"L\">Linkshandig";
  }
  else if ($speler->handig == 'L')
  {
    echo "<input type=\"radio\" name=\"handig\" value=\"R\">Rechtshandig";
    echo "<input type=\"radio\" name=\"handig\" value=\"L\" CHECKED>Linkshandig";
  }
  else
  {
    echo "<input type=\"radio\" name=\"handig\" value=\"R\">Rechtshandig";
    echo "<input type=\"radio\" name=\"handig\" value=\"L\">Linkshandig";
  }
  echo "</td></tr><tr><td>Speelt badminton sinds :</td>\n";
  echo "<td><input type=\"text\" name=\"start_dt\" size=\"10\" maxlength=\"10\" value=\"";
  echo $speler->start_dt;
  echo "\"> (yyyy/mm/dd)</td></tr><tr><td>Lid van W&L sinds :</td>\n";
  echo "<td><input type=\"text\" name=\"club_dt\" size=\"10\" maxlength=\"10\" value=\"";
  echo $speler->club_dt;
  echo "\"> (yyyy/mm/dd)</td></tr><tr><td>Racket :</td>\n";
  echo "<td><input type=\"text\" name=\"racket\" size=\"50\" maxlength=\"50\" value=\"";
  echo $speler->racket;
  echo "\"></td></tr><tr><td>Klassement :</td>\n";
  echo "<td><input DISABLED type=\"text\" name=\"klassement\" size=\"2\" maxlength=\"2\" value=\"";
  echo $speler->klassement;
  echo "\" READONLY></td></tr><tr><td>Lidnummer :</td>";
  if ($speler->lidnr == '')
  {
     echo "<td><input type=\"text\" name=\"lidnr\" size=\"5\" maxlength=\"5\" value=\"";
     echo $speler->lidnr;
     echo "\"></td></tr>";
  }
  else
  {
    echo "<td><input DISABLED type=\"text\" name=\"lidnr\" size=\"5\" maxlength=\"5\" value=\"";
    echo $speler->lidnr;
    echo "\" READONLY></td></tr>";
  }
  echo "<tr><td>E-mail :</td>\n";
  echo "<td><input type=\"text\" name=\"email\" size=\"36\" maxlength=\"100\" value=\"";
  echo $speler->email;
  echo "\"></td></tr><tr><td>Website :</td>\n";
  echo "<td><input type=\"text\" name=\"website\" size=\"36\" maxlength=\"75\" value=\"";
  echo $speler->website;
  echo "\"></td></tr><tr><td>Inschrijving mailen :</td>\n";
  echo "<td><select name=\"zend_inschrijving\" title=\"Hiermee geef je aan of je via mail wilt verwittigd worden wanneer een tornooiinschrijving verzonden wordt, en zo ja, in CC of in BCC\">";
  echo "     <option".($speler->zend_inschrijving == "" ? " selected" : "")."></option>\n";
  echo "     <option".($speler->zend_inschrijving == "CC" ? " selected" : "").">CC</option>\n";
  echo "     <option".($speler->zend_inschrijving == "BCC" ? " selected" : "").">BCC</option>\n";
  echo "</select></td></tr>\n";
  // Tornooi overwinning toevoegen
  echo "<TR bordercolor=\"#CCCCCC\" bgcolor=\"#666699\"><TD align=\"center\" colspan=\"2\" class=\"kleingeel\">Een gewonnen tornooi toevoegen:</td></tr><tr><td>Datum van :</td>";
  echo "<td><input type=\"text\" name=\"dt_van\" size=\"10\" maxlength=\"10\">";
  echo "<A HREF=\"#\" onClick=\"cal.select(document.update_spelers.dt_van,'anchor1','yyyy/MM/dd'); return false;\" NAME=\"anchor1\" ID=\"anchor1\"><IMG src=\"../images/b_calendar.png\" alt=\"kalender\" border=\"none\"></A> (yyyy/mm/dd)</td></tr>";
  echo "<tr><td>Datum tot :</td>\n";
  echo "<td><input type=\"text\" name=\"dt_tot\" size=\"10\" maxlength=\"10\">";
  echo "<A HREF=\"#\" onClick=\"cal.select(document.update_spelers.dt_tot,'anchor2','yyyy/MM/dd'); return false;\" NAME=\"anchor2\" ID=\"anchor2\"><IMG src=\"../images/b_calendar.png\" alt=\"kalender\" border=\"none\"></A> (yyyy/mm/dd)</td></tr>";
  echo "<tr><td>Naam en plaats :</td>\n";
  echo "<td><input type=\"text\" name=\"plaats\" size=\"50\" maxlength=\"100\" title=\"naam en plaats van het tornooi; klik op het pijltje voor keuzes\">";
  //<!-- the LOV icon -->
  echo "<IMG src=\"http://www.badmintonsport.be/images/lov_button.jpg\" alt=\"List Of Values\" onClick=\"showLov()\" style=\"cursor: pointer;\"></td></tr>";
  echo "<tr><td>Klassement :</td>\n";
  echo "<td><select name=\"klassement2\" title=\"klassement waarin je gespeeld hebt; zet je eigen klassement bij jeugd- of veteranentornooi\">\n";
  echo "            <option>D </option>\n";
  echo "            <option>C2</option>\n";
  echo "            <option>C1</option>\n";
  echo "            <option>B2</option>\n";
  echo "            <option>B1</option>\n";
  echo "            <option>A </option>\n";
  echo "            <option>open C</option>\n";
  echo "            <option>open B</option></SELECT>\n";
  echo "<select name=\"categorie\" title=\"leeftijdscategorie; alleen van toepassing bij jeugd- of veteranentornooi, zoniet NVT(niet van toepassing)\">\n";
  echo "            <option>NVT</option>\n";
  echo "            <option>-9 </option>\n";
  echo "            <option>-11</option>\n";
  echo "            <option>-13</option>\n";
  echo "            <option>-15</option>\n";
  echo "            <option>-17</option>\n";
  echo "            <option>-19</option>\n";
  echo "             <option>35+</option>\n";
  echo "            <option>45+</option></select> ( &nbsp; NVT = Niet van toepassing ) </td>\n";
  echo " <tr><td>Reeks :</td>\n";
  echo "  <td><input type=\"radio\" name=\"reeks\" value=\"E\"> Enkel \n";
  echo "      <input type=\"radio\" name=\"reeks\" value=\"D\"> Dubbel \n";
  echo "      <input type=\"radio\" name=\"reeks\" value=\"G\"> Gemengd </td>\n </tr>\n";
  echo " <tr>\n  <td>Partner :</td>\n";
  echo "  <td><input type=\"text\" name=\"partner\" size=\"30\" maxlength=\"40\" title=\"Partner; geef eerst de voornaam en dan de achternaam; als partner niet van W&L zet dan club tussen haakjes\"></td>";
  echo " </tr><td><input type=\"hidden\" name=\"spelers_id\" value=\"".$id."\"></td></tr>";
  echo " <tr bordercolor=\"#CCCCCC\" bgcolor=\"#666699\">";
  echo "  <td align=\"center\" colspan=\"2\" class=\"kleingeel\">Gebruikersnaam en/of paswoord wijzigen:</td>\n";
  echo " </tr>\n";
  echo " <tr>\n";
  echo "  <td>Nieuwe gebruikersnaam :</td>\n";
  echo "  <td><input type=\"text\" name=\"nieuwe_gebruikersnaam\" size=\"9\" maxlength=\"10\"></td>\n";
  echo " </tr>\n";
  echo " <tr>\n";
  echo "  <td>Nieuw paswoord :</td>\n";
  echo "  <td><input type=\"password\" name=\"nieuw_paswoord1\" size=\"9\" maxlength=\"10\"></td>\n";
  echo " </tr>\n";
  echo " <tr>\n";
  echo "  <td>Herhaal paswoord :</td>\n";
  echo "  <td><input type=\"password\" name=\"nieuw_paswoord2\" size=\"9\" maxlength=\"10\"></td>\n";
  echo " </tr>\n";
  echo " <tr>\n";
  echo "  <td><input type=\"button\" value=\"verzenden\" onClick=\"checkSubmitCount();\">\n";
  echo "      &nbsp; <BUTTON accesskey=\"H\" onclick=\"showHelp();\">Help!</BUTTON></td>\n";
  echo " </tr>\n</form>\n</table>\n";
}
/************************************************************************************************/

   if (!isset($_POST["username"])) //dus als er geen username is ingevoerd 
   {
      echo "jonge, je bent hier op de verkeerde manier gekomen";
   }
   else //als dat WEL is gedaan...
   {
// create SQL statement 
      $sql = "SELECT level
                FROM users
               WHERE UPPER(usid) = UPPER('".$_POST["username"]."')
                 AND paswoord = '".$_POST["password"]."'";
      $result1 = mysql_query($sql, $badm_db) or badm_mysql_die();
   // format results by row
      $login = mysql_fetch_object($result1);
      if (mysql_num_rows($result1) != 1) 
      {
      //username+password zijn ingegeven maar kloppen niet !
         echo "Wrong username / password<br>";
         //echo "Cookies are set, but they won't help you get in...<br>";
      }
      else if (mysql_num_rows($result1) == 1) 
      {
         //echo "login succesfull<br>";
         //echo "welcome ".$_POST["username"]."<br>";
         //nu de level van de user checken
         if ($login->level < 3)
         {
            //mag alleen eigen profiel aanpassen
            // create SQL statement
            $sql = "SELECT id
                      FROM users
                     WHERE UPPER(usid) = UPPER('".$_POST["username"]."')
                       AND paswoord = '".$_POST["password"]."'
                       AND UPPER(naam) = UPPER('".$_POST["naam"]."')";
            $result2 = mysql_query($sql, $badm_db) or badm_mysql_die();
            $login = mysql_fetch_object($result2);
            if (mysql_num_rows($result2) == 0) 
            {
               echo "Je kan enkel je eigen gegevens aanpassen!";
            }
            else if (mysql_num_rows($result2) == 1)
            {
              //echo "hello level 2 dude !";
              display_form($badm_db);
            }
         }
         else if($login->level == 3)
         {
            //echo "hello admin !";
            display_form($badm_db);
         }
         else if (mysql_result($result1,"0","level") == 1)
         {
            //echo "hello level 1 dude !";
         }
         else if (mysql_result($result1,"0","level") == 0)
         {
            //echo "hello dude level 0!";
         }
      }
   }
?>
<!-- Division voor de kalender popup-->
<DIV ID="datediv" STYLE="position:absolute; visibility:hidden; background-color:white;"></DIV>
<!-- Popup body met de Help tekst-->
<DIV STYLE="display:none;" id="oHelp">
<H3>Hoe een overwinning op een tornooi toevoegen?</H3>
<U><B>1. Datum Van</B></U><BR>
Vul hier de datum in wanneer het tornooi startte.  Vul een datum als volgt in <NOBR>25-09-2004</NOBR> 
of <NOBR>2004-09-25</NOBR> als je 25 september 2004 bedoelt.<BR>
<U><B>2. Datum Tot</B></U><BR>
Indien het tornooi over meerdere dagen loopt vul dan hier de datum in van de laaste dag van het tornooi.
Als het tornooi op 1 dag gespeeld werd laat dit veld dan leeg (niet opnieuw de begindatum invullen).<BR>
<U><B>3. Naam en plaats</B></U><BR>
Klik op het pijltje naast dit vak.  Je krijgt dan een schermpje met tornooien.  Kies hieruit het 
toepasselijke tornooi.  Je kan een kleinere selectie maken door bovenaan in het zoekvak een deel
van de tornooinaam in te vullen.  Je kan hierbij wildcards gebruiken, dat zijn speciale tekens (% en _).
De % staat voor een willekeurig aantal karakters, de _ voor juist ��n.  Als je bijvoorbeeld zoekt
naar b% vind je alle tornooien die met een b beginnen.  Zoek je naar %antwerpen% dan vind je alle
tornooien uit Antwerpen.  Vind je het tornooi dat je gewonnen hebt niet in deze lijst, klik dan op 
Cancel en vul zelf het tornooi in.  Gebruik hierbij hoofdletters en zet tussen haakjes de gemeente
waar het tornooi plaats vond als dat niet duidelijk blijkt uit de naam van het tornooi.<BR>
<U><B>4. Klassement</B></U><BR>
Kies hier het klassement waarin je gewonnen hebt.  Dat kan dus hoger zijn dan je eigen klassement
op dat moment.<BR>
<U><B>5. Leeftijdscategorie</B></U><BR>
Dit veld moet alleen ingevuld worden als je een jeugd- of veteranentornooi gewonnen hebt.  Anders 
laat je het op "NVT" staan.  Vul ook weer de categorie in waarin je de overwinning behaald hebt.  
Als je dus als 12-jarige gewonnen hebt in de -15 vul je hier -15 in (en niet -13).<BR>
<U><B>6. Reeks</B></U><BR>
Enkel, dubbel of gemengd.<BR>
<U><B>7. Partner</B></U><BR>
Alleen invullen bij dubbel of gemengd.  Vul eerst de voornaam in en dan pas de achternaam.  Zorg
ervoor dat de naam correct gespeld is, dan wordt de overwinning automatisch ook aan het profiel
van je partner toegevoegd zodat die dat niet ook nog eens hoeft te doen.  Is je partner niet van
W&L schrijf dan tussen haakjes ook de naam van zijn/haar club.<BR>
<U><B>8. Verzenden</B></U><BR>
Als alles correct is ingevuld klik je op verzenden.  Je resultaten zouden nu zichtbaar moeten zijn
in je eigen profiel, eventueel ook in het profiel van je partner en op de tornooiresultatenpagina.
Ook de statistiekenpagina kan er nu anders uit zien.<BR>
Het is niet mogelijk om zelf je tornooigegevens nog te wijzigen nadat je ze verstuurd hebt.
Heb je toch een foutje gemaakt, laat dan iets weten aan de webmaster.
</div>
</body>
</html>
<?php
  log_website_usage($start_time, null, $badm_db);
?>