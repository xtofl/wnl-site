<?php
  require_once "../functies/website_usage.php";
?>
<html>
<head>
<style type="text/css">
  .text {font: 12px Verdana,Arial,Helvetica; color: white; font-weight: bold  ; font-style: normal; text-decoration: none;}
  .body {font: 11px Verdana,Arial,Helvetica; color: black; font-weight: normal; font-style: normal; title-decoration: none;}
</style>
</head>
<body>
<?php
   require_once "../functies/badm_db.inc.php";
   $badm_db = badm_conn_db();
   include "../functies/Tabel.php";
//speler + aantal overwinningen per geslacht
  $sql1 = "SELECT s.naam
               , COUNT(*) AS aantal
               , s.geslacht
            FROM bad_tornooien t
               , bad_spelers s
           WHERE s.id = t.spelers_id
             AND s.eind_dt IS NULL
             AND t.categorie IS NULL
           GROUP BY t.spelers_id
           ORDER BY 2 DESC
               , s.geslacht";
//speler + aantal overwinningen per klassement, geslacht
   $sql2 = "SELECT s.naam
                , s.klassement
                , s.geslacht
                , COUNT(*) AS aantal
             FROM bad_tornooien t
                , bad_spelers s
            WHERE s.id = t.spelers_id
              AND s.eind_dt IS NULL
              AND t.categorie IS NULL
            GROUP BY t.spelers_id
            ORDER BY IFNULL(s.klassement, 'E')
                , s.geslacht
                , 4 DESC";
//speler + aantal overwinningen per klassement, geslacht, discipline
  $sql3 = "SELECT s.naam
               , COUNT( * ) AS aantal
               , s.klassement
               , s.geslacht
               , t.reeks
            FROM bad_tornooien t
               , bad_spelers s
           WHERE s.id = t.spelers_id
             AND s.eind_dt IS NULL
             AND t.categorie IS NULL
           GROUP BY t.spelers_id
               , t.reeks
           ORDER BY IFNULL(s.klassement, 'E')
               , s.geslacht
               , t.reeks
               , 2 DESC";
//speler + aantal overwinningen per discipline
  $sql4 = "SELECT s.naam
               , COUNT(*) AS aantal
               , t.reeks
               , s.geslacht
            FROM bad_tornooien t
               , bad_spelers s
           WHERE s.id = t.spelers_id
             AND s.eind_dt IS NULL
             AND t.categorie IS NULL
           GROUP BY t.spelers_id, t.reeks
           ORDER BY t.reeks
               , s.geslacht
               , 2 DESC";
//Aantal overwinningen dit seizoen per speler
  $sql5 = "SELECT sp.id
                , sp.naam
                , count(*) AS aantal
             FROM bad_spelers   AS sp
                , bad_tornooien AS tor
            WHERE tor.spelers_id = sp.id
              AND sp.eind_dt IS NULL
              AND IF(DATE_FORMAT(tor.dt_van, '%m') < 8, DATE_FORMAT(tor.dt_van, '%Y'), DATE_FORMAT(tor.dt_van, '%Y') + 1) = IF(DATE_FORMAT(NOW(), '%m') < 8, DATE_FORMAT(NOW(), '%Y'), DATE_FORMAT(NOW(), '%Y') + 1)
            GROUP BY sp.id
            ORDER BY 3 DESC
                , tor.dt_van DESC";
  $result1 = mysql_query ($sql1, $badm_db) or badm_mysql_die();
  $result2 = mysql_query ($sql2, $badm_db) or badm_mysql_die();
  $result3 = mysql_query ($sql3, $badm_db) or badm_mysql_die();
  $result4 = mysql_query ($sql4, $badm_db) or badm_mysql_die();
  $result5 = mysql_query ($sql5, $badm_db) or badm_mysql_die();

  $gevondenM = False;
  $gevondenV = False;
  while (!($gevondenM && $gevondenV))
  {
    $speler1 = mysql_fetch_object($result1);
    if ($speler1->geslacht == 'V' && !$gevondenV)
    {
      $naamTVT = $speler1->naam; //vrouw met meeste overwinningen
      $aantalTVT = $speler1->aantal;
      $gevondenV = True;
    }
    elseif ($speler1->geslacht == 'M' && !$gevondenM)
    {
      $naamTMT = $speler1->naam; //man met meeste overwinningen
      $aantalTMT = $speler1->aantal;
      $gevondenM = True;
    }
  }

  $speler2 = mysql_fetch_object($result2);
  $naamAMT = $speler2->naam; //man A met meeste overwinningen
  $aantalAMT = $speler2->aantal; //dat aantal overwinningen
  $gevondenA = False;
  $gevondenB1M = False;
  $gevondenB1V = False;
  $gevondenB2M = False;
  $gevondenB2V = False;
  $gevondenC1M = False;
  $gevondenC1V = False;
  $gevondenC2M = False;
  $gevondenC2V = False;
  $gevondenDM = False;
  $gevondenDV = False;
  for ($i = 1; $i < mysql_num_rows($result2); $i++)
  {
    $speler2 = mysql_fetch_object($result2);
    if ($speler2->klassement == 'A' && $speler2->geslacht == 'V' && !$gevondenA)
    {
      $naamAVT = $speler2->naam; //vrouw A met meeste overwinningen
      $aantalAVT = $speler2->aantal;
      $gevondenA = True;
    }
    if ($speler2->klassement == 'B1' && $speler2->geslacht == 'M' && !$gevondenB1M)
    {
      $naamB1MT = $speler2->naam; //man B1 met meeste overwinningen
      $aantalB1MT = $speler2->aantal;
      $gevondenB1M = True;
    }
    if ($speler2->klassement == 'B1' && $speler2->geslacht == 'V' && !$gevondenB1V)
    {
      $naamB1VT = $speler2->naam; //vrouw B1 met meeste overwinningen
      $aantalB1VT = $speler2->aantal;
      $gevondenB1V = True;
    }
    if ($speler2->klassement == 'B2' && $speler2->geslacht == 'M' && !$gevondenB2M)
    {
      $naamB2MT = $speler2->naam; //man B2 met meeste overwinningen
      $aantalB2MT = $speler2->aantal;
      $gevondenB2M = True;
    }
    if ($speler2->klassement == 'B2' && $speler2->geslacht == 'V' && !$gevondenB2V)
    {
      $naamB2VT = $speler2->naam; //vrouw B2 met meeste overwinningen
      $aantalB2VT = $speler2->aantal;
      $gevondenB2V = True;
    }
    if ($speler2->klassement == 'C1' && $speler2->geslacht == 'M' && !$gevondenC1M)
    {
      $naamC1MT = $speler2->naam; //man C1 met meeste overwinningen
      $aantalC1MT = $speler2->aantal;
      $gevondenC1M = True;
    }
    if ($speler2->klassement == 'C1' && $speler2->geslacht == 'V' && !$gevondenC1V)
    {
      $naamC1VT = $speler2->naam; //vrouw C1 met meeste overwinningen
      $aantalC1VT = $speler2->aantal;
      $gevondenC1V = True;
    }
    if ($speler2->klassement == 'C2' && $speler2->geslacht == 'M' && !$gevondenC2M)
    {
      $naamC2MT = $speler2->naam; //man C2 met meeste overwinningen
      $aantalC2MT = $speler2->aantal;
      $gevondenC2M = True;
    }
    if ($speler2->klassement == 'C2' && $speler2->geslacht == 'V' && !$gevondenC2V)
    {
      $naamC2VT = $speler2->naam; //vrouw C2 met meeste overwinningen
      $aantalC2VT = $speler2->aantal;
      $gevondenC2V = True;
    }
    if ($speler2->klassement == 'D' && $speler2->geslacht == 'M' && !$gevondenDM)
    {
      $naamDMT = $speler2->naam; //man D met meeste overwinningen
      $aantalDMT = $speler2->aantal;
      $gevondenDM = True;
    }
    if ($speler2->klassement == 'D' && $speler2->geslacht == 'V' && !$gevondenDV)
    {
      $naamDVT = $speler2->naam; //vrouw D met meeste overwinningen
      $aantalDVT = $speler2->aantal;
      $gevondenDV = True;
    }
  }

  $speler3 = mysql_fetch_object($result3);
  $naamAME = $speler3->naam; //man A met meeste overwinningen Enkel
  $aantalAME = $speler3->aantal; //dat aantal overwinningen
  $gevondenAMD = False;
  $gevondenAMG = False;
  $gevondenAVE = False;
  $gevondenAVD = False;
  $gevondenAVG = False;
  $gevondenB1ME = False;
  $gevondenB1VE = False;
  $gevondenB1MD = False;
  $gevondenB1VD = False;
  $gevondenB1MG = False;
  $gevondenB1VG = False;
  $gevondenB2ME = False;
  $gevondenB2VE = False;
  $gevondenB2MD = False;
  $gevondenB2VD = False;
  $gevondenB2MG = False;
  $gevondenB2VG = False;
  $gevondenC1ME = False;
  $gevondenC1VE = False;
  $gevondenC1MD = False;
  $gevondenC1VD = False;
  $gevondenC1MG = False;
  $gevondenC1VG = False;
  $gevondenC2ME = False;
  $gevondenC2VE = False;
  $gevondenC2MD = False;
  $gevondenC2VD = False;
  $gevondenC2MG = False;
  $gevondenC2VG = False;
  $gevondenDME = False;
  $gevondenDVE = False;
  $gevondenDMD = False;
  $gevondenDVD = False;
  $gevondenDMG = False;
  $gevondenDVG = False;
  for ($i = 1; $i < mysql_num_rows($result3); $i++)
  {
    $speler3 = mysql_fetch_object($result3);
    if ($speler3->klassement == 'A' && $speler3->geslacht == 'M' && $speler3->reeks == 'D' && !$gevondenAMD)
    {
      $naamAMD = $speler3->naam; //man A met meeste overwinningen Dubbel
      $aantalAMD = $speler3->aantal;
      $gevondenAMD = True;
    }
    if ($speler3->klassement == 'A' && $speler3->geslacht == 'M' && $speler3->reeks == 'G' && !$gevondenAMG)
    {
      $naamAMG = $speler3->naam; //man A met meeste overwinningen Gemengd
      $aantalAMG = $speler3->aantal;
      $gevondenAMG = True;
    }

    if ($speler3->klassement == 'A' && $speler3->geslacht == 'V' && $speler3->reeks == 'E' && !$gevondenAVE)
    {
      $naamAVE = $speler3->naam; //vrouw A met meeste overwinningen Enkel
      $aantalAVE = $speler3->aantal;
      $gevondenAVE = True;
    }
    if ($speler3->klassement == 'A' && $speler3->geslacht == 'V' && $speler3->reeks == 'D' && !$gevondenAVD)
    {
      $naamAVD = $speler3->naam; //vrouw A met meeste overwinningen Dubbel
      $aantalAVD = $speler3->aantal;
      $gevondenAVD = True;
    }
    if ($speler3->klassement == 'A' && $speler3->geslacht == 'V' && $speler3->reeks == 'G' && !$gevondenAVG)
    {
      $naamAVG = $speler3->naam; //vrouw A met meeste overwinningen Gemengd
      $aantalAVG = $speler3->aantal;
      $gevondenAVG = True;
    }
    if ($speler3->klassement == 'B1' && $speler3->geslacht == 'M' && $speler3->reeks == 'E' && !$gevondenB1ME)
    {
      $naamB1ME = $speler3->naam; //man B1 met meeste overwinningen Enkel
      $aantalB1ME = $speler3->aantal;
      $gevondenB1ME = True;
    }
    if ($speler3->klassement == 'B1' && $speler3->geslacht == 'M' && $speler3->reeks == 'D' && !$gevondenB1MD)
    {
      $naamB1MD = $speler3->naam; //man B1 met meeste overwinningen Dubbel
      $aantalB1MD = $speler3->aantal;
      $gevondenB1MD = True;
    }
    if ($speler3->klassement == 'B1' && $speler3->geslacht == 'M' && $speler3->reeks == 'G' && !$gevondenB1MG)
    {
      $naamB1MG = $speler3->naam; //man B1 met meeste overwinningen Gemengd
      $aantalB1MG = $speler3->aantal;
      $gevondenB1MG = True;
    }
    if ($speler3->klassement == 'B1' && $speler3->geslacht == 'V' && $speler3->reeks == 'E' && !$gevondenB1VE)
    {
      $naamB1VE = $speler3->naam; //vrouw B1 met meeste overwinningen Enkel
      $aantalB1VE = $speler3->aantal;
      $gevondenB1VE = True;
    }
    if ($speler3->klassement == 'B1' && $speler3->geslacht == 'V' && $speler3->reeks == 'D' && !$gevondenB1VD)
    {
      $naamB1VD = $speler3->naam; //vrouw B1 met meeste overwinningen Dubbel
      $aantalB1VD = $speler3->aantal;
      $gevondenB1VD = True;
    }
    if ($speler3->klassement == 'B1' && $speler3->geslacht == 'V' && $speler3->reeks == 'G' && !$gevondenB1VG)
    {
      $naamB1VG = $speler3->naam; //vrouw B1 met meeste overwinningen Gemengd
      $aantalB1VG = $speler3->aantal;
      $gevondenB1VG = True;
    }
    if ($speler3->klassement == 'B2' && $speler3->geslacht == 'M' && $speler3->reeks == 'E' && !$gevondenB2ME)
    {
      $naamB2ME = $speler3->naam; //man B2 met meeste overwinningen Enkel
      $aantalB2ME = $speler3->aantal;
      $gevondenB2ME = True;
    }
    if ($speler3->klassement == 'B2' && $speler3->geslacht == 'M' && $speler3->reeks == 'D' && !$gevondenB2MD)
    {
      $naamB2MD = $speler3->naam; //man B2 met meeste overwinningen Dubbel
      $aantalB2MD = $speler3->aantal;
      $gevondenB2MD = True;
    }
    if ($speler3->klassement == 'B2' && $speler3->geslacht == 'M' && $speler3->reeks == 'G' && !$gevondenB2MG)
    {
      $naamB2MG = $speler3->naam; //man B2 met meeste overwinningen Gemengd
      $aantalB2MG = $speler3->aantal;
      $gevondenB2MG = True;
    }
    if ($speler3->klassement == 'B2' && $speler3->geslacht == 'V' && $speler3->reeks == 'E' && !$gevondenB2VE)
    {
      $naamB2VE = $speler3->naam; //vrouw B2 met meeste overwinningen Enkel
      $aantalB2VE = $speler3->aantal;
      $gevondenB2VE = True;
    }
    if ($speler3->klassement == 'B2' && $speler3->geslacht == 'V' && $speler3->reeks == 'D' && !$gevondenB2VD)
    {
      $naamB2VD = $speler3->naam; //vrouw B2 met meeste overwinningen Dubbel
      $aantalB2VD = $speler3->aantal;
      $gevondenB2VD = True;
    }
    if ($speler3->klassement == 'B2' && $speler3->geslacht == 'V' && $speler3->reeks == 'G' && !$gevondenB2VG)
    {
      $naamB2VG = $speler3->naam; //vrouw B2 met meeste overwinningen Gemengd
      $aantalB2VG = $speler3->aantal;
      $gevondenB2VG = True;
    }
    if (!$gevondenC1ME && $speler3->klassement == 'C1' && $speler3->geslacht == 'M' && $speler3->reeks == 'E')
    {
      $naamC1ME = $speler3->naam; //man C1 met meeste overwinningen Enkel
      $aantalC1ME = $speler3->aantal;
      $gevondenC1ME = True;
    }
    if (!$gevondenC1MD && $speler3->klassement == 'C1' && $speler3->geslacht == 'M' && $speler3->reeks == 'D')
    {
      $naamC1MD = $speler3->naam; //man C1 met meeste overwinningen Dubbel
      $aantalC1MD = $speler3->aantal;
      $gevondenC1MD = True;
    }
    if (!$gevondenC1MG && $speler3->klassement == 'C1' && $speler3->geslacht == 'M' && $speler3->reeks == 'G')
    {
      $naamC1MG = $speler3->naam; //man C1 met meeste overwinningen Gemengd
      $aantalC1MG = $speler3->aantal;
      $gevondenC1MG = True;
    }
    if (!$gevondenC1VE && $speler3->klassement == 'C1' && $speler3->geslacht == 'V' && $speler3->reeks == 'E')
    {
      $naamC1VE = $speler3->naam; //vrouw C1 met meeste overwinningen Enkel
      $aantalC1VE = $speler3->aantal;
      $gevondenC1VE = True;
    }
    if (!$gevondenC1VD && $speler3->klassement == 'C1' && $speler3->geslacht == 'V' && $speler3->reeks == 'D')
    {
      $naamC1VD = $speler3->naam; //vrouw C1 met meeste overwinningen Dubbel
      $aantalC1VD = $speler3->aantal;
      $gevondenC1VD = True;
    }
    if (!$gevondenC1VG && $speler3->klassement == 'C1' && $speler3->geslacht == 'V' && $speler3->reeks == 'G')
    {
      $naamC1VG = $speler3->naam; //vrouw C1 met meeste overwinningen Gemengd
      $aantalC1VG = $speler3->aantal;
      $gevondenC1VG = True;
    }
    if (!$gevondenC2ME && $speler3->klassement == 'C2' && $speler3->geslacht == 'M' && $speler3->reeks == 'E')
    {
      $naamC2ME = $speler3->naam; //man C2 met meeste overwinningen Enkel
      $aantalC2ME = $speler3->aantal;
      $gevondenC2ME = True;
    }
    if (!$gevondenC2MD && $speler3->klassement == 'C2' && $speler3->geslacht == 'M' && $speler3->reeks == 'D')
    {
      $naamC2MD = $speler3->naam; //man C2 met meeste overwinningen Dubbel
      $aantalC2MD = $speler3->aantal;
      $gevondenC2MD = True;
    }
    if (!$gevondenC2MG && $speler3->klassement == 'C2' && $speler3->geslacht == 'M' && $speler3->reeks == 'G')
    {
      $naamC2MG = $speler3->naam; //man C2 met meeste overwinningen Gemengd
      $aantalC2MG = $speler3->aantal;
      $gevondenC2MG = True;
    }
    if (!$gevondenC2VE && $speler3->klassement == 'C2' && $speler3->geslacht == 'V' && $speler3->reeks == 'E')
    {
      $naamC2VE = $speler3->naam; //vrouw C2 met meeste overwinningen Enkel
      $aantalC2VE = $speler3->aantal;
      $gevondenC2VE = True;
    }
    if (!$gevondenC2VD && $speler3->klassement == 'C2' && $speler3->geslacht == 'V' && $speler3->reeks == 'D')
    {
      $naamC2VD = $speler3->naam; //vrouw C2 met meeste overwinningen Dubbel
      $aantalC2VD = $speler3->aantal;
      $gevondenC2VD = True;
    }
    if (!$gevondenC2VG && $speler3->klassement == 'C2' && $speler3->geslacht == 'V' && $speler3->reeks == 'G')
    {
      $naamC2VG = $speler3->naam; //vrouw C2 met meeste overwinningen Gemengd
      $aantalC2VG = $speler3->aantal;
      $gevondenC2VG = True;
    }
    if (!$gevondenDME && $speler3->klassement == 'D' && $speler3->geslacht == 'M' && $speler3->reeks == 'E')
    {
      $naamDME = $speler3->naam; //man D met meeste overwinningen Enkel
      $aantalDME = $speler3->aantal;
      $gevondenDME = True;
    }
    if (!$gevondenDMD && $speler3->klassement == 'D' && $speler3->geslacht == 'M' && $speler3->reeks == 'D')
    {
      $naamDMD = $speler3->naam; //man D met meeste overwinningen Dubbel
      $aantalDMD = $speler3->aantal;
      $gevondenDMD = True;
    }
    if (!$gevondenDMG && $speler3->klassement == 'D' && $speler3->geslacht == 'M' && $speler3->reeks == 'G')
    {
      $naamDMG = $speler3->naam; //man D met meeste overwinningen Gemengd
      $aantalDMG = $speler3->aantal;
      $gevondenDMG = True;
    }
    if (!$gevondenDVE && $speler3->klassement == 'D' && $speler3->geslacht == 'V' && $speler3->reeks == 'E')
    {
      $naamDVE = $speler3->naam; //vrouw D met meeste overwinningen Enkel
      $aantalDVE = $speler3->aantal;
      $gevondenDVE = True;
    }
    if (!$gevondenDVD && $speler3->klassement == 'D' && $speler3->geslacht == 'V' && $speler3->reeks == 'D')
    {
      $naamDVD = $speler3->naam; //vrouw D met meeste overwinningen Dubbel
      $aantalDVD = $speler3->aantal;
      $gevondenDVD = True;
    }
    if (!$gevondenDVG && $speler3->klassement == 'D' && $speler3->geslacht == 'V' && $speler3->reeks == 'G')
    {
      $naamDVG = $speler3->naam; //vrouw D met meeste overwinningen Gemengd
      $aantalDVG = $speler3->aantal;
      $gevondenDVG = True;
    }
  }

  $speler4 = mysql_fetch_object($result4);
  $naamTME = $speler4->naam; //man met meeste overwinningen Enkel
  $aantalTME = $speler4->aantal; //dat aantal overwinningen
  $gevondenEV = False;
  $gevondenDM = False;
  $gevondenDV = False;
  $gevondenGM = False;
  $gevondenGV = False;
  for ($i = 1; $i < mysql_num_rows($result4); $i++)
  {
    $speler4 = mysql_fetch_object($result4);
    if ($speler4->reeks == 'E' && $speler4->geslacht == 'V' && !$gevondenEV)
    {
      $naamTVE = $speler4->naam; //vrouw met meeste overwinningen Enkel
      $aantalTVE = $speler4->aantal;
      $gevondenEV = True;
    }
    if ($speler4->reeks == 'D' && $speler4->geslacht == 'M' && !$gevondenDM)
    {
      $naamTMD = $speler4->naam; //man met meeste overwinningen Dubbel
      $aantalTMD = $speler4->aantal;
      $gevondenDM = True;
    }
    if ($speler4->reeks == 'D' && $speler4->geslacht == 'V' && !$gevondenDV)
    {
      $naamTVD = $speler4->naam; //vrouw met meeste overwinningen Dubbel
      $aantalTVD = $speler4->aantal;
      $gevondenDV = True;
    }
    if ($speler4->reeks == 'G' && $speler4->geslacht == 'M' && !$gevondenGM)
    {
      $naamTMG = $speler4->naam; //man met meeste overwinningen Gemengd
      $aantalTMG = $speler4->aantal;
      $gevondenGM = True;
    }
    if ($speler4->reeks == 'G' && $speler4->geslacht == 'V' && !$gevondenGV)
    {
      $naamTVG = $speler4->naam; //vrouw met meeste overwinningen Gemengd
      $aantalTVG = $speler4->aantal;
      $gevondenGV = True;
    }
  }

  $positie = 0;
  $aantal = 0;
  for ($i = 1; $i <= 10; $i++)
  {
    $speler5 = mysql_fetch_object($result5);
    if ($speler5->aantal != $aantal)
    {
      $positie = $i;
    }
    $data[] = "<b>".$positie.".</b>&nbsp;";
    $data[] = "<a href=\"http://www.badmintonsport.be/spelers/spelersinfo.php?id=".$speler5->id."\" target=\"_top\">".$speler5->naam."</a>";
    $data[] = "&nbsp;&nbsp;".$speler5->aantal;
    $aantal = $speler5->aantal;
  } 

   mysql_free_result($result1);
   mysql_free_result($result2);
   mysql_free_result($result3);
   mysql_free_result($result4);
?>
<P><FONT face="Verdana"><B>Wie heeft al het meeste (offici�le) tornooien gewonnen?</B></FONT></P>
<TABLE width="100%" cellspacing="2" cellpadding="0" border="1px" bordercolor="white">
  <TR align="center" valign="center">
    <TD colspan="2">
    </TD>
    <TD bgColor=#666699 class="text">Enkel
    </TD>
    <TD bgColor=#666699 class="text">Dubbel
    </TD>
    <TD bgColor=#666699 class="text">Gemengd
    </TD>
    <TD bgColor=#666699 class="text">Totaal
    </TD>
  </TR>
  <TR align="center" valign="center">
    <TD rowspan="2" bgColor=#666699 class="text">A
    </TD>
    <TD bgColor=#666699 class="text">M
    </TD>
    <TD bgcolor="#DFDFDF" class="body"><?php echo $naamAME." (".$aantalAME.")"; ?>
    </TD>
    <TD bgcolor="#DFDFDF" class="body"><?php echo $naamAMD." (".$aantalAMD.")"; ?>
    </TD>
    <TD bgcolor="#DFDFDF" class="body"><?php echo $naamAMG." (".$aantalAMG.")"; ?>
    </TD>
    <TD bgcolor="#DFDFDF" class="body"><B><?php echo $naamAMT." (".$aantalAMT.")"; ?></B>
    </TD>
  </TR>
  <TR align="center" valign="center">
    <TD bgColor=#666699 class="text">V
    </TD>
    <TD bgcolor="#DFDFDF" class="body"><?php echo $naamAVE." (".$aantalAVE.")"; ?>
    </TD>
    <TD bgcolor="#DFDFDF" class="body"><?php echo $naamAVD." (".$aantalAVD.")"; ?>
    </TD>
    <TD bgcolor="#DFDFDF" class="body"><?php echo $naamAVG." (".$aantalAVG.")"; ?>
    </TD>
    <TD bgcolor="#DFDFDF" class="body"><B><?php echo $naamAVT." (".$aantalAVT.")"; ?></B>
    </TD>
  </TR>
  <TR align="center" valign="center">
    <TD rowspan="2" bgColor=#666699 class="text">B1
    </TD>
    <TD bgColor=#666699 class="text">M
    </TD>
    <TD bgcolor="#DFDFDF" class="body"><?php echo $naamB1ME." (".$aantalB1ME.")"; ?>
    </TD>
    <TD bgcolor="#DFDFDF" class="body"><?php echo $naamB1MD." (".$aantalB1MD.")"; ?>
    </TD>
    <TD bgcolor="#DFDFDF" class="body"><?php echo $naamB1MG." (".$aantalB1MG.")"; ?>
    </TD>
    <TD bgcolor="#DFDFDF" class="body"><B><?php echo $naamB1MT." (".$aantalB1MT.")"; ?></B>
    </TD>
  </TR>
  <TR align="center" valign="center">
    <TD bgColor=#666699 class="text">V
    </TD>
    <TD bgcolor="#DFDFDF" class="body"><?php echo $naamB1VE." (".$aantalB1VE.")"; ?>
    </TD>
    <TD bgcolor="#DFDFDF" class="body"><?php echo $naamB1VD." (".$aantalB1VD.")"; ?>

    </TD>
    <TD bgcolor="#DFDFDF" class="body"><?php echo $naamB1VG." (".$aantalB1VG.")"; ?>
    </TD>
    <TD bgcolor="#DFDFDF" class="body"><B><?php echo $naamB1VT." (".$aantalB1VT.")"; ?></B>
    </TD>
  </TR>
  <TR align="center" valign="center">
    <TD rowspan="2" bgColor=#666699 class="text">B2
    </TD>
    <TD bgColor=#666699 class="text">M
    </TD>
    <TD bgcolor="#DFDFDF" class="body"><?php echo $naamB2ME." (".$aantalB2ME.")"; ?>
    </TD>
    <TD bgcolor="#DFDFDF" class="body"><?php echo $naamB2MD." (".$aantalB2MD.")"; ?>
    </TD>
    <TD bgcolor="#DFDFDF" class="body"><?php echo $naamB2MG." (".$aantalB2MG.")"; ?>
    </TD>
    <TD bgcolor="#DFDFDF" class="body"><B><?php echo $naamB2MT." (".$aantalB2MT.")"; ?></B>
    </TD>
  </TR>
  <TR align="center" valign="center">
    <TD bgColor=#666699 class="text">V
    </TD>
    <TD bgcolor="#DFDFDF" class="body"><?php echo $naamB2VE." (".$aantalB2VE.")"; ?>
    </TD>
    <TD bgcolor="#DFDFDF" class="body"><?php echo $naamB2VD." (".$aantalB2VD.")"; ?>
    </TD>
    <TD bgcolor="#DFDFDF" class="body"><?php echo $naamB2VG." (".$aantalB2VG.")"; ?>
    </TD>
    <TD bgcolor="#DFDFDF" class="body"><B><?php echo $naamB2VT." (".$aantalB2VT.")"; ?></B>
    </TD>
  </TR>
  <TR align="center" valign="center">
    <TD rowspan="2" bgColor=#666699 class="text">C1
    </TD>
    <TD bgColor=#666699 class="text">M
    </TD>
    <TD bgcolor="#DFDFDF" class="body"><?php echo $naamC1ME." (".$aantalC1ME.")"; ?>
    </TD>
    <TD bgcolor="#DFDFDF" class="body"><?php echo $naamC1MD." (".$aantalC1MD.")"; ?>
    </TD>
    <TD bgcolor="#DFDFDF" class="body"><?php echo $naamC1MG." (".$aantalC1MG.")"; ?>
    </TD>
    <TD bgcolor="#DFDFDF" class="body"><B><?php echo $naamC1MT." (".$aantalC1MT.")"; ?></B>
    </TD>
  </TR>
  <TR align="center" valign="center">
    <TD bgColor=#666699 class="text">V
    </TD>
    <TD bgcolor="#DFDFDF" class="body"><?php echo $naamC1VE." (".$aantalC1VE.")"; ?>
    </TD>
    <TD bgcolor="#DFDFDF" class="body"><?php echo $naamC1VD." (".$aantalC1VD.")"; ?>
    </TD>
    <TD bgcolor="#DFDFDF" class="body"><?php echo $naamC1VG." (".$aantalC1VG.")"; ?>
    </TD>
    <TD bgcolor="#DFDFDF" class="body"><B><?php echo $naamC1VT." (".$aantalC1VT.")"; ?></B>
    </TD>
  </TR>
  <TR align="center" valign="center">
    <TD rowspan="2" bgColor=#666699 class="text">C2
    </TD>
    <TD bgColor=#666699 class="text">M
    </TD>
    <TD bgcolor="#DFDFDF" class="body"><?php echo $naamC2ME." (".$aantalC2ME.")"; ?>
    </TD>
    <TD bgcolor="#DFDFDF" class="body"><?php echo $naamC2MD." (".$aantalC2MD.")"; ?>
    </TD>
    <TD bgcolor="#DFDFDF" class="body"><?php echo $naamC2MG." (".$aantalC2MG.")"; ?>
    </TD>
    <TD bgcolor="#DFDFDF" class="body"><B><?php echo $naamC2MT." (".$aantalC2MT.")"; ?></B>
    </TD>
  </TR>
  <TR align="center" valign="center">
    <TD bgColor=#666699 class="text">V
    </TD>
    <TD bgcolor="#DFDFDF" class="body"><?php echo $naamC2VE." (".$aantalC2VE.")"; ?>
    </TD>
    <TD bgcolor="#DFDFDF" class="body"><?php echo $naamC2VD." (".$aantalC2VD.")"; ?>
    </TD>
    <TD bgcolor="#DFDFDF" class="body"><?php echo $naamC2VG." (".$aantalC2VG.")"; ?>
    </TD>
    <TD bgcolor="#DFDFDF" class="body"><B><?php echo $naamC2VT." (".$aantalC2VT.")"; ?></B>
    </TD>
  </TR>
  <TR align="center" valign="center">
    <TD rowspan="2" bgColor=#666699 class="text">D
    </TD>
    <TD bgColor=#666699 class="text">M
    </TD>
    <TD bgcolor="#DFDFDF" class="body"><?php echo $naamDME." (".$aantalDME.")"; ?>
    </TD>
    <TD bgcolor="#DFDFDF" class="body"><?php echo $naamDMD." (".$aantalDMD.")"; ?>
    </TD>
    <TD bgcolor="#DFDFDF" class="body"><?php echo $naamDMG." (".$aantalDMG.")"; ?>
    </TD>
    <TD bgcolor="#DFDFDF" class="body"><B><?php echo $naamDMT." (".$aantalDMT.")"; ?></B>
    </TD>
  </TR>
  <TR align="center" valign="center">
    <TD bgColor=#666699 class="text">V
    </TD>
    <TD bgcolor="#DFDFDF" class="body"><?php echo $naamDVE." (".$aantalDVE.")"; ?>
    </TD>
    <TD bgcolor="#DFDFDF" class="body"><?php echo $naamDVD." (".$aantalDVD.")"; ?>
    </TD>
    <TD bgcolor="#DFDFDF" class="body"><?php echo $naamDVG." (".$aantalDVG.")"; ?>
    </TD>
    <TD bgcolor="#DFDFDF" class="body"><B><?php echo $naamDVT." (".$aantalDVT.")"; ?></B>
    </TD>
  </TR>
  <TR align="center" valign="center">
    <TD colspan="2" bgColor=#666699 class="text">Totaal M
    </TD>
    <TD bgcolor="#DFDFDF" class="body"><B><?php echo $naamTME." (".$aantalTME.")"; ?></B>
    </TD>
    <TD bgcolor="#DFDFDF" class="body"><B><?php echo $naamTMD." (".$aantalTMD.")"; ?></B>
    </TD>
    <TD bgcolor="#DFDFDF" class="body"><B><?php echo $naamTMG." (".$aantalTMG.")"; ?></B>
    </TD>
    <TD bgcolor="CCCCCC" class="body"><B><?php echo $naamTMT." (".$aantalTMT.")"; ?></B>
    </TD>
  </TR>
  <TR align="center" valign="center">
    <TD colspan="2" bgColor=#666699 class="text">Totaal V
    </TD>
    <TD bgcolor="#DFDFDF" class="body"><B><?php echo $naamTVE." (".$aantalTVE.")"; ?></B>
    </TD>
    <TD bgcolor="#DFDFDF" class="body"><B><?php echo $naamTVD." (".$aantalTVD.")"; ?></B>
    </TD>
    <TD bgcolor="#DFDFDF" class="body"><B><?php echo $naamTVG." (".$aantalTVG.")"; ?></B>
    </TD>
    <TD bgcolor="CCCCCC" class="body"><B><?php echo $naamTVT." (".$aantalTVT.")"; ?></B>
    </TD>
  </TR>
</TABLE></P>
<P><FONT face="Verdana"><B>Top 10 tornooioverwinningen dit seizoen:</B></FONT>
<TABLE  cellspacing="20">
<TR><TD>
<?php
  $uitlijning = array('right', 'left', 'right');
  makeTable ($data, "3", $uitlijning);
?>
</td></tr>
</table></p>
</body>
</html>
<?php
  log_website_usage($start_time, null, $badm_db);
  mysql_close($badm_db);
?>