<HTML>
<HEAD>
<TITLE>Tornooien</TITLE>

<STYLE>
 input.lov   { border : none}
 td.label    { border-bottom-style : solid;
               border-bottom-width : thin;
               border-bottom-color : black}
 #Scrollable { height   : 250px;
               width    : 270px;
               overflow : auto}

</STYLE>

<SCRIPT language="JavaScript" type="text/javascript">
// previously highlighted element
var preEl;
// original background color
var orgBColor;
// original text color
var orgTColor;
// original font weight
var orgFWeight;
// Highlight an element when clicked on it
// and restore the previously highlighted item to its original settings
function highlight(element)
{
  if(typeof(preEl)!= 'undefined') //some element is already highlighted
  {
    // restore this element to its original settings
    preEl.style.backgroundColor = orgBColor;
    preEl.style.color = orgTColor;
    preEl.style.fontWeight = orgFWeight;
  }
  // store settings of new element
  orgBColor = element.style.backgroundColor;
  orgTColor = element.style.color;
  orgFWeight = element.style.fontWeight;
  // highlight new element
  element.style.backgroundColor = 'blue';
  element.style.color = 'white';
  element.style.fontWeight = 'bolder';
  // set previous element to new element
  preEl = element;
}

function closeme()
{
  window.close();
}
function handleCancel()
{
  closeme();
  return false;
}
function handleOK()
{
  if (opener && !opener.closed)
  {
    if (transferData())
    {
      opener.dialogWin.returnFunc();
      closeme();
    }
  }
  else
  {
    alert("You have closed the main window.\n\nNo action will be taken on the choices in this dialog box.");
    closeme();
  }
  return false;
}

function transferData()
{
  if (opener && !opener.closed)
  {
    if (typeof(preEl)!= 'undefined')
    {
      opener.dialogWin.returnedValue = preEl.value;
    }
    else
    {
      alert('Choose a value first.');
      return false;
    }
  }
  return true;
}

function doSearch()
{
  document.lov_data.submit();
}

</SCRIPT>
</HEAD>
<BODY bgcolor="#BBBBBB" onLoad="if (opener) opener.blockEvents();" onUnload="if (opener) opener.unblockEvents();">
<FORM name="lov_data" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
 <TABLE width="100%" height="100%">
  <TR>
<?php
if (!isset($_POST['p_search_string']))
{
  $search_string = "%";
}
else
{
  $search_string = $_POST['p_search_string'];
}
?>
   <TD align="center" height="40">Zoek &nbsp; <INPUT type="text" name="p_search_string" value="<?php echo $search_string; ?>"></TD>
  </TR>
  <TR>
   <TD valign="top">
    <TABLE width="100%">
     <TR>
      <TD class="label" valign="top">Tornooi</TD>
     </TR>
     <TR>
      <TD>
       <DIV id="Scrollable">
<?php
/* Display the requested values. */
    require_once "../functies/badm_db.inc.php";
    $badm_db = badm_conn_db();
// Create SQL statement
 $sql = 'SELECT DISTINCT plaats
           FROM bad_tornooien
          WHERE plaats LIKE "'.$search_string.'"
          ORDER BY plaats';
 $result = mysql_query ($sql, $badm_db) or badm_mysql_die();
// format results by row
 $found = FALSE;
 while ($tornooi = mysql_fetch_object($result))
 {
    echo "        <INPUT type=\"text\" class=\"lov\" value=\"".$tornooi->plaats."\" size=\"39\" onClick=\"highlight(this);\" onMouseOver=\"this.style.cursor='pointer'\"></BR>";
    $found = TRUE;
 }
 if (!$found)
 {
   echo "No data found";
 }
?>
       </DIV>
      </TD>
     </TR>
    </TABLE>
   </TD>
  </TR>
  <TR>
   <TD align="center" height="40">
    <INPUT type="button" value=" Zoek " onClick="doSearch();">
    <INPUT type="button" value="  OK  " onClick="handleOK();">
    <INPUT type="button" value="Cancel" onClick="handleCancel();">
   </TD>
  </TR>
 </TABLE>
</FORM>
</BODY>
</HTML>