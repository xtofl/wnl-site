<?php
/**
 * inschrijving.php
 *
 * object     : Pagina om je in te schrijven
 * author     : Freek Ceymeulen
 * created    : 10/11/2012
 *              23/11/2013 jeugd = -19 ipv -18
 *                         statuut jeugd valt weg
 *                         basisbedrag = 98 voor CT, 78 voor R
 *                         geen vroegboekkorting meer noch boete
 *                         Sporthal Heilig-Hart Heverlee mag weg
 *                         administratief lid
 *                         tussen 1 en 25 december mogen alleen bestaande leden inschrijven
 *              14/11/2014 wijziging naar half jaar
 *              31/05/2015 terug naar volledig jaar, van 1 juli t/m 30 juni
 * parameters : email : e-mail address (not set when new inscription)
 *              ogm : gestructureerde mededeling (not set when new inscription)
 **/

  require_once "../functies/website_usage.php";
  // Connect to the DB (must be done before calling sess.php)
  require_once "../functies/badm_db.inc.php";
  $badm_db2 = badm_conn_db();
  /* 11/11/2014 FC commented out because of warning "Warning: mysql_real_escape_string() [function.mysql-real-escape-string]: Access denied for user 'root'@'localhost' (using password: NO) in /home/badmin/public_html/functies/sess.php on line 232"
                                                    "Warning: mysql_real_escape_string() [function.mysql-real-escape-string]: A link to the server could not be established in /home/badmin/public_html/functies/sess.php on line 232"
                   and I don't think it's used on this page */
  //require_once "../functies/sess.php";
  require_once "../functies/general_functions.php";

  $debug = "";

/*
-- Functions --
*/

  function quote_smart($value)
  {
    // Stripslashes
    if (get_magic_quotes_gpc())
    {
       $value = stripslashes($value);
    }
    // Quote if not integer
    if (!is_numeric($value))
    {
        $value = "'" . mysql_real_escape_string($value) . "'";
    }
    return $value;
  }

  function log_action($user_wijz, $actie, $id, $stmt, $conn)
  {
    if (LOGGING)
    {
      // Sla de bewerking op in audit tabel
      $audit = "INSERT INTO admin_audit (user_id, actie, id, inhoud)
                VALUES ('%s','%s',%d,'%s')";
      $sql  = sprintf($audit, mysql_real_escape_string($user_wijz)
                            , mysql_real_escape_string($actie)
                            , mysql_real_escape_string($id)
                            , mysql_real_escape_string(addslashes($stmt)));
      $result = mysql_query($sql, $conn) or die("Invalid query: " . mysql_error());
    }
  }

  function build_options2($domain, $selected, $conn)
  {
    $sql_stmt = sprintf("SELECT value, description FROM codes WHERE domain = '%s' ORDER BY description"
                       , mysql_real_escape_string($domain, $conn));
    $rslt = mysql_query($sql_stmt, $conn) or badm_mysql_die();
    echo "<option value=\"EMPTY\"".((strtoupper($selected)) == 'EMPTY' ? " selected" : "")."></option>\n";
    while ($row = mysql_fetch_object($rslt))
    {
      echo "<option value=\"".$row->value."\"";
      echo (strtoupper($row->value) == strtoupper($selected)) ? " selected" : "";
      echo ">".$row->description."</option>\n";
    }
    mysql_free_result($rslt);
  }
  function write_speeluur($id, $check, $uur, $type, $reserve, $prijs, $description)
  {
    if ($reserve=='Y')
    {
       $color = ';color:orange';
    }
    elseif ($reserve=='V')
    {
       $color = ';color:red';
    }
    else
    {
       $color = '';
    }
    echo '                    <td><input type="checkbox"'; echo ($reserve=='V') ? ' disabled' : ''; echo ' id="uur'.$id.'" name="uur'.$id.'" class="input"'.$check.' onChange="calculate_speelurenbedrag('.$id.');"></td>';
    echo '                    <td title="'.$description.'" style="cursor:help'.$color.'">'; echo $uur; echo ' (<span id="prijs'.$id.'">'.$prijs.'</span> �)</td>';
    echo '                    <td>'.$type.'</td>';
  }

  function save_subscription($conn)
  {
    global $page;
    global $debug;
    global $message;
    global $voornaam;
    global $achternaam;
    global $geboortedatum;
    global $klassement;
    global $lidnr;
    global $email;
    global $email2;
    global $geslacht;
    global $statuut;
    global $adres;
    global $postcode;
    global $gemeente;
    global $tel;
    global $gsm;
    global $maat;
    global $bedrag;
    global $initiatie;
    global $meewerken;
    global $ogm;
    global $new_id;
    $voornaam = $_POST['voornaam'];
    $achternaam = $_POST['achternaam'];
    $geboortedatum = $_POST['geboortedatum'];
    $klassement = $_POST['klassement'];
    $lidnr = $_POST['lidnr'];
    $email = $_POST['email'];
    $email2 = $_POST['email2'];
    $geslacht = $_POST['geslacht'];
    $statuut = $_POST['statuut'];
    $adres = $_POST['straat'];
    $postcode = $_POST['postcode'];
    $gemeente = $_POST['gemeente'];
    $tel = $_POST['telefoon'];
    $gsm = $_POST['mobiel'];
    $maat = $_POST['shirt'];
    $bedrag = $_POST['bedrag'];
    if (isset($_POST["administratief_lid"]))
    {
      $administratief_lid = "J";
    }
    else
    {
      $administratief_lid = "N";
    }
    if (isset($_POST["initiatie"]))
    {
      $initiatie = "Y";
    }
    else
    {
      $initiatie = "N";
    }
    if (isset($_POST["werking"]))
    {
      $meewerken = "Y";
    }
    else
    {
      $meewerken = "N";
    }
    $ogm = $_POST['ogm'];
    $debug .= "<br>save_subscription()\n";
    $debug .= "<br>OGM = ".$ogm."\n";
    $lidnummer = (strlen($lidnr) > 0) ? "lidnr = '".substr($lidnr, 0, 10)."'" : "lidnr = NULL";
    $update_stmt = "UPDATE inschrijvingen
                       SET voornaam = '%s'
                         , achternaam = '%s'
                         , %s
                         , klassement = '%s'
                         , geslacht = '%s'
                         , statuut = '%s'
                         , geboortedatum = '%s'
                         , email2 = '%s'
                         , adres = '%s'
                         , postcode = '%s'
                         , gemeente = '%s'
                         , tel = '%s'
                         , gsm = '%s'
                         , maat = '%s'
                         , bedrag = '%s'
                         , initiatie = '%s'
                         , meewerken = '%s'
                         , datum = NOW()
                         , ip = '%s'
                         , expire_dt = NOW()
                         , administratief_lid = '%s'
                     WHERE ogm = '%s'";
    $sql  = sprintf($update_stmt, substr(mysql_real_escape_string($voornaam), 0, 40)
                                , substr(mysql_real_escape_string($achternaam), 0, 50)
                                , $lidnummer
                                , mysql_real_escape_string(strtoupper($klassement))
                                , mysql_real_escape_string(strtoupper($geslacht))
                                , mysql_real_escape_string(strtoupper($statuut))
                                , mysql_real_escape_string(get_valid_date($geboortedatum))
                                , substr(mysql_real_escape_string($email2), 0, 320)
                                , substr(mysql_real_escape_string($adres), 0, 100)
                                , substr(mysql_real_escape_string($postcode), 0, 10)
                                , substr(mysql_real_escape_string($gemeente), 0, 55)
                                , substr(mysql_real_escape_string($tel), 0, 30)
                                , substr(mysql_real_escape_string($gsm), 0, 30)
                                , substr(mysql_real_escape_string(strtoupper($maat)), 0, 4)
                                , mysql_real_escape_string($bedrag)
                                , mysql_real_escape_string($initiatie)
                                , mysql_real_escape_string($meewerken)
                                , mysql_real_escape_string($_SERVER['REMOTE_ADDR'])
                                , mysql_real_escape_string($administratief_lid)
                                , mysql_real_escape_string($ogm));
    $result = mysql_query($sql, $conn) or die("Invalid query: " . mysql_error());

    require_once "../admin/dml/bad_spelers.php";
    $todays_date = date("Y-m-d");
    $club_dt = date("Y-m-d"); //strtotime($todays_date);
    $eind_dt = date("Y-m-d"); //"2014-01-01"; //strtotime("2014-01-01");
    $inschrijving_dt = date("Y-m-d"); //strtotime($todays_date);
    $numRows = 0;

    $current_month = date("n");
    $current_day = date("d");
    if ($current_month == 6 && $current_day <= 25 && empty($lidnr))
    {
      $message = "De inschrijvingen zijn momenteel beperkt tot bestaande leden (met VBL lidnummer). Nieuwe leden kunnen inschrijven vanaf 1 juli.";
    }
    else
    {
      if (!empty($lidnr))
      {
         $query = "SELECT *, DATE_FORMAT(geb_dt, '%%d/%%m/%%Y') AS geboortedatum FROM bad_spelers WHERE lidnr = %d";
         $sql  = sprintf($query, mysql_real_escape_string($lidnr, $conn));
         $result2 = mysql_query($sql, $conn) or badm_mysql_die();
      }
      else
      {
         $query = "SELECT *, DATE_FORMAT(geb_dt, '%%d/%%m/%%Y') AS geboortedatum FROM bad_spelers WHERE LOWER(voornaam) = '%s' AND LOWER(achternaam) = '%s' AND geb_dt = DATE_FORMAT('%s', '%%Y-%%m-%%d')";
         $sql  = sprintf($query, mysql_real_escape_string(strtolower($voornaam))
                               , mysql_real_escape_string(strtolower($achternaam))
                               , mysql_real_escape_string(get_valid_date($geboortedatum)));
         $result2 = mysql_query($sql, $conn) or badm_mysql_die();
      }
      if (mysql_num_rows($result2) == 0)
      {
        if ($lidnr == 0)
        {
           $lidnr = null;
        }
        if ($current_month == 6 && $current_day <= 25)
        {
          $message = "De inschrijvingen zijn momenteel beperkt tot bestaande leden. Nieuwe leden kunnen inschrijven vanaf 1 juli.";
        }
        else
        {
          $message = insert_speler($conn, $achternaam, $voornaam, $geboortedatum, $klassement, $geslacht, $lidnr, $statuut, $adres, null, null, $postcode, $gemeente, $email2, $tel, $gsm, $club_dt, $eind_dt, $maat, $competitie, null, 'online inschrijving', $inschrijving_dt, $bedrag, $administratief_lid);
          $debug .= "<br>spelers id: ".$new_id."\n";
        }
      }
      else
      {
        $speler = mysql_fetch_object($result2);
        if (is_null($speler->eind_dt) && $current_month < 6 && $current_month > 7)
        {
          $message = 'Deze persoon is al ingeschreven. Voor wijzigingen tijdens het seizoen (speeluren e.d.), neem contact op met het <a href="mailto:wlgclaes@yahoo.com">secretariaat</a>.';
        }
        else
        {
          $new_id = $speler->id;
          $message = update_speler($conn, $new_id, $achternaam, $voornaam, $geboortedatum, $klassement, $geslacht, $lidnr, $statuut, $adres, $postcode, $gemeente, $email2, nvl($tel, $speler->tel), nvl($gsm, $speler->gsm), $speler->club_dt, null, $maat, $speler->competitie, null, $bedrag, $speler->clubblad, $speler->remark, $speler->administratief_lid);
          if ($message == "Spelersinfo gewijzigd. ")
          {
             $message = '';
          }
        }
      }
      mysql_free_result($result2);
      // tijdelijke patch, ik weet niet hoe het komt dat lidnr soms op 0 komt
      $update_stmt = "UPDATE bad_spelers SET lidnr = NULL WHERE lidnr = 0";
      $result = mysql_query($update_stmt, $conn) or die("Invalid query: " . mysql_error());
    }
    $debug .= "<br>lengte message = ".strlen($message)."\n";
    if (strlen($message) > 0)
    {
      if ($message == "Deze persoon bestaat al in de database! Ga na of hij niet op non-actief staat.")
      {
         if (empty($lidnr))
         {
            $message = "Gelieve je VBL lidnummer in te vullen. Dit kan je vinden op de spelerspagina van de W&amp;L website.";
         }
         else
         {
            $message = "Gelieve je correct VBL lidnummer in te vullen. Dit kan je vinden op de spelerspagina van de W&amp;L website. Als je een nieuw lid bent laat lidnummer dan leeg.";
         }
      }
      $page = "3"; // ga terug naar inschrijvingsformulier
      mail("freek.ceymeulen@pandora.be", $message, "new_id = ".$new_id.", ogm = ".$ogm.", lidnr = ".$lidnr, "From: webmaster@badmintonsport.be");
    }
    else //if (empty($message))
    {
       // Verwijder alle records uit tabel LEDEN_UREN voor deze speler
       require_once "../admin/dml/leden_uren.php";
       delete_leden_uren($conn, $new_id);

       // Voeg deze speler toe in tabel leden_uren voor alle aangevinkte uren
       $sql = "SELECT id, dag, uur, plaats, prijs FROM bad_speeluren";
       $result2 = mysql_query($sql, $conn) or die("Invalid query: " . mysql_error());

       $uren = "<td>Speeluren</td><td>";
       while ($row = mysql_fetch_object($result2))
       {
         // Alleen checkboxen die aangevinkt zijn worden gepost
         $speeluur = "uur".$row->id;
         if (isset($_POST[$speeluur]))
         {
           // ga na of er nog plaatsen beschikbaar zijn
           $query = "SELECT u.aantal_plaatsen
                          , u.aantal_reserve AS aantal_reserve_plaatsen
                          , SUM(CASE reserve WHEN 'N' THEN 1 ELSE 0 END) AS aantal_spelers
                          , SUM(CASE reserve WHEN 'Y' THEN 1 ELSE 0 END) AS aantal_reserven
                       FROM bad_speeluren u
                       LEFT OUTER JOIN leden_uren lu ON u.id = lu.speeluren_id
                      WHERE u.id = $row->id
                      GROUP BY u.aantal_plaatsen
                             , u.aantal_reserve";
           $resultaat = mysql_query($query, $conn) or badm_mysql_die();
           $uur = mysql_fetch_object($resultaat);
           if ($uur->aantal_spelers < $uur->aantal_plaatsen)
           {
              $reserve = 'N';
              insert_leden_uren($conn, $new_id, $row->id, $reserve);
              $uren .= "<br>".$row->dag." ".$row->uur.", ".$row->plaats." (".$row->prijs.")";
           }
           elseif ($uur->aantal_reserven < $uur->aantal_reserve_plaatsen)
           {
              $reserve = 'Y';
              insert_leden_uren($conn, $new_id, $row->id, $reserve);
              $uren .= "<br>".$row->dag." ".$row->uur.", ".$row->plaats." (".$row->prijs.") RESERVE";
           }
           else
           {
              mail("freek.ceymeulen@pandora.be", "Plaats niet meer beschikbaar", "uur id ".$row->id." is volzet voor speler ".$voornaam." ".$achternaam, "From: webmaster@badmintonsport.be");
           }
         }
       }
       $uren .= "</td>";

       $update_stmt = "UPDATE inschrijvingen
                          SET spelers_id = %d
                        WHERE ogm = '%s'";
       $sql  = sprintf($update_stmt, $new_id
                                   , mysql_real_escape_string($ogm));
       $result = mysql_query($sql, $conn) or die("Invalid query: " . mysql_error());

       $debug .= "<br>stuur bevestigingsmail naar ".$email2."\n";
       // stuur bevestigingsmail
       $body  = "Beste ".$voornaam.",";
       $body .= "<br><br>Bedankt voor je inschrijving.";
       $body .= "<br><br>Vergeet niet om <b>".$bedrag." �</b> te storten op rekeningnummer <b>BE 02 3300 7219 5740</b> van W&amp;L bv vzw met gestructureerde mededeling <b>+++".$ogm."+++</b>.";
       $body .= "<br>Pas dan is je inschrijving geldig.";
       $body .= "<br><br><span style=\"text-decoration:underline\">Overzicht</span>";
       $body .= "<table><tr><td>voornaam          </td><td>".$voornaam          .((isset($speler) && $speler->voornaam != $voornaam) ? ' (oud: '.$speler->voornaam.')' : '')."</td></tr>
                        <tr><td>achternaam        </td><td>".$achternaam        .((isset($speler) && $speler->achternaam != $achternaam) ? ' (oud: '.$speler->achternaam.')' : '')."</td></tr>
                        <tr><td>geboortedatum     </td><td>".$geboortedatum     .((isset($speler) && $speler->geboortedatum != $geboortedatum) ? ' (oud: '.$speler->geboortedatum.')' : '')."</td></tr>
                        <tr><td>klassement        </td><td>".$klassement        .((isset($speler) && $speler->klassement != $klassement) ? ' (oud: '.$speler->klassement.')' : '')."</td></tr>
                        <tr><td>lidnr             </td><td>".$lidnr             ."</td></tr>
                        <tr><td>email             </td><td>".$email             .((isset($speler) && $speler->email != $email) ? ' (oud: '.$speler->email.')' : '')."</td></tr>
                        <tr><td>geslacht          </td><td>".$geslacht          .((isset($speler) && $speler->geslacht != $geslacht) ? ' (oud: '.$speler->geslacht.')' : '')."</td></tr>
                        <tr><td>statuut           </td><td>".$statuut           .((isset($speler) && $speler->type_speler != $statuut) ? ' (oud: '.$speler->type_speler.')' : '')."</td></tr>
                        <tr><td>adres             </td><td>".$adres             .((isset($speler) && $speler->adres != $adres) ? ' (oud: '.$speler->adres.')' : '')."</td></tr>
                        <tr><td>postcode          </td><td>".$postcode          .((isset($speler) && $speler->postcode != $postcode) ? ' (oud: '.$speler->postcode.')' : '')."</td></tr>
                        <tr><td>gemeente          </td><td>".$gemeente          .((isset($speler) && $speler->woonplaats != $gemeente) ? ' (oud: '.$speler->woonplaats.')' : '')."</td></tr>
                        <tr><td>tel               </td><td>".$tel               .((isset($speler) && str_replace('/', '', strtr($speler->tel, '+.-', '///')) != str_replace('/', '', strtr($tel, '+.-', '///'))) ? ' (oud: '.$speler->tel.')' : '')."</td></tr>
                        <tr><td>gsm               </td><td>".$gsm               .((isset($speler) && str_replace('/', '', strtr($speler->gsm, '+.-', '///')) != str_replace('/', '', strtr($gsm, '+.-', '///'))) ? ' (oud: '.$speler->gsm.')' : '')."</td></tr>
                        <tr><td>maat              </td><td>".$maat              .((isset($speler) && $speler->maat != $maat) ? ' (oud: '.$speler->maat.')' : '')."</td></tr>
                        <tr><td>administratief lid</td><td>".$administratief_lid.((isset($speler) && $speler->administratief_lid != $administratief_lid) ? ' (oud: '.$speler->administratief_lid.')' : '')."</td></tr>
                        <tr><td>Ik wil graag meewerken aan activiteiten en werking van onze club. </td><td>".($meewerken == "Y" ? "Ja" : "Nee")."</td></tr>
                        <tr><td>Ik heb interesse in initiatiesessies mits extra betaling</td><td>".($initiatie == "Y" ? "Ja" : "Nee")."</td></tr>";
       $body .= "<tr>".$uren."</tr></table>";
       $body .= "<br><br>W&amp;L bv";
       if($email != $email2)
       {
          $cc = "\r\nCc: ".$email;
       }
       mail($email2, "Inschrijving W&L bv - bevestiging", $body, "MIME-Version: 1.0\r\nContent-type: text/html; charset=iso-8859-1\r\nFrom: W&L bv <webmaster@badmintonsport.be>".$cc."\r\nBcc: freek.ceymeulen@pandora.be,wlgclaes@yahoo.com");
    }
  }

  function check_ogm($conn, $ogm, $email)
  {
    global $debug;
    $debug .= "<br>check_ogm(".$ogm.")\n";
    // Kijk of deze OGM bestaat en nog niet gebruikt werd
    $query = "SELECT * FROM inschrijvingen WHERE ogm = '%s' AND expire_dt IS NOT NULL";
    $sql  = sprintf($query, mysql_real_escape_string($ogm, $conn) );
    $debug .= "\n<br>".$sql;
    $result = mysql_query($sql, $conn) or badm_mysql_die();
    if (mysql_num_rows($result) == 0)
    {
       mysql_free_result($result);
       mail("freek.ceymeulen@pandora.be", "OGM bestaat niet of is al gebruikt: ".$ogm, $email, "From: webmaster@badmintonsport.be");
       return false;
    }
    mysql_free_result($result);
    return true;
  }

  function check_request($conn, $email)
  {
    global $debug;
    $debug .= "<br>check_request(".$email.")\n";
    // Security: Er mogen max 5 inschrijvingsformulieren per IP-adres aangevraagd worden
    $query = "SELECT COUNT(*) AS num_open_entries FROM inschrijvingen WHERE ip = '%s' AND expire_dt IS NOT NULL AND creation_dt > CURDATE( ) - INTERVAL 300 DAY";
    $sql  = sprintf($query, mysql_real_escape_string($_SERVER['REMOTE_ADDR'], $conn) );
    $debug .= "\n<br>".$sql;
    $result = mysql_query($sql, $conn) or badm_mysql_die();
    $row = mysql_fetch_object($result);
    if ($row->num_open_entries == 4)
    {
       mysql_free_result($result);
       mail("freek.ceymeulen@pandora.be", "Max open entries bereikt voor IP: ".$_SERVER['REMOTE_ADDR'], $email, "From: webmaster@badmintonsport.be");
       return true;
    }
    elseif ($row->num_open_entries == 5)
    {
       mysql_free_result($result);
       return false;
    }
    else
    {
      return true;
    }
    mysql_free_result($result);
  }

  function add_ogm($conn, $email)
  {
     global $ogm;
     $insert_stmt  = "INSERT INTO inschrijvingen (creation_dt, email)";
     $insert_stmt .= "VALUES (CURRENT_TIMESTAMP, '%s')";
     $sql  = sprintf($insert_stmt, substr(mysql_real_escape_string($email), 0, 100));
     $debug .= "\n<br>".$sql;
     $result2 = mysql_query($sql, $conn) or die("Invalid query: ".$sql . mysql_error());
     $new_id = mysql_insert_id();
     // Voeg OGM toe
     $update_stmt = "UPDATE inschrijvingen
                        SET ogm = '%s'
                      WHERE id = %d";
     $ogm = $new_id % 97;
     if ($ogm == 0)
     {
        $ogm = 97;
     }
     $ogm = $new_id.str_pad($ogm, 2, "0", STR_PAD_LEFT);
     $ogm = str_pad($ogm, 12, "0", STR_PAD_LEFT);
     //$ogm = '+++'.substr($ogm, 0, 3).'/'.substr($ogm, 3, 4).'/'.substr($ogm, 7, 5).'+++';
     $ogm = substr($ogm, 0, 3).'/'.substr($ogm, 3, 4).'/'.substr($ogm, 7, 5);
     $sql = sprintf($update_stmt, substr(mysql_real_escape_string($ogm), 0, 20)
                                , mysql_real_escape_string($new_id));
     $debug .= "\n<br>".$sql;
     $result3 = mysql_query($sql, $conn) or die("Invalid query: " . mysql_error());
  }

  function send_url($conn, $email)
  {
    global $debug;
    global $ogm;
    $debug .= "<br>send_url(".$email.")\n";
    // Kijk of er al een record bestaat voor dit email adres dat nog niet gebruikt werd
    $query = "SELECT * FROM inschrijvingen WHERE LOWER(email) = LOWER('%s') AND expire_dt IS NULL";
    $sql  = sprintf($query, mysql_real_escape_string($email, $conn) );
    $debug .= "\n<br>".$sql;
    $result = mysql_query($sql, $conn) or badm_mysql_die();
    if (mysql_num_rows($result) == 0)
    {
      // Voeg toe
      add_ogm($conn, $email);
    }
    else
    {
       $row = mysql_fetch_object($result);
       $ogm = $row->ogm;
    }
    mysql_free_result($result);
    $headers  = "MIME-Version: 1.0\r\n";
    $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
    $headers .= "From: W&L bv <webmaster@badmintonsport.be>\r\n";
    //$headers .= "BCC: webmaster@badmintonsport.be\r\n";
    $headers .= "BCC: freek.ceymeulen@telenet.be,wlgclaes@yahoo.com\r\n";
    mail($email, "Inschrijvingsformulier W&L bv", "Beste,<br><br>U kan het inschrijvingsformulier terugvinden op onze website, via het aanklikken van onderstaande link:<br><a href=\"http://www.badmintonsport.be/inschrijven/inschrijving.php?email=".$email."&ogm=".$ogm."\">http://www.badmintonsport.be/inschrijven/inschrijving.php?email=".$email."&ogm=".$ogm."</a><br><br>Mocht deze link niet werken, dan kunt u ook de url copi�ren en in de adresbalk van uw browser plakken.<br><br>Let op, indien u niet een verzoek om het inschrijvingsformulier hebt ingediend, meld dan een probleem aan de webmaster.<br><br>Bestaande leden kunnen hun VBL lidnummer opzoeken op de <a href=\"http://www.badmintonsport.be/spelers.php\">spelerspagina</a> van de W&amp;L website, wie nog nooit lid geweest is mag lidnummer leeg laten.<br>De inschrijving is pas gelukt wanneer je een bevestigingsmail ontvangen hebt.<br><br>Indien je problemen ondervindt bij het inschrijven, gelieve dit dan te melden aan de <a href=\"mailto:webmaster@badmintonsport.be\">webmaster</a>. (met foutmelding en screen shot graag)<br><br>W&L bv", $headers);
  }

/*
-- Begin --
*/
  global $ogm;
  $ogm = $_GET["ogm"];
  $debug .= "<br>email=".$_REQUEST["email"];
  if (isset($_REQUEST["bedrag"]))
  {
    $page = "4"; // bedankt
    if ((!isset($_REQUEST["message"])))
    {
       save_subscription($badm_db2);
    }
  }
  elseif (isset($_GET["email"]))
  {
    if (isset($_GET["again"]))
    {
       add_ogm($badm_db2, $_GET["email"]);
       $page = "3"; // inschrijvingsformulier
    }
    elseif (check_ogm($badm_db2, $ogm, $_REQUEST["email"]))
    {
      $page = "3"; // inschrijvingsformulier
    }
    else
    {
      $page = "5"; // error
    }
  }
  elseif (isset($_REQUEST["email"]))
  {
    $page = "2"; // email verzonden
    if ((!isset($_REQUEST["message"])))
    {
       if (check_request($badm_db2, $_POST["email"]))
       {
         send_url($badm_db2, $_REQUEST["email"]);
       }
       else
       {
          $page = "6"; // error
       }
    }
  }
  else
  {
    $page = "1"; // email adres
  }
  $debug .= "<br>page=".$page;

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html lang="nl"><!-- InstanceBegin template="/Templates/templatesub1php.dwt.php" codeOutsideHTMLIsLocked="false" -->
<?php
    // Connect to database
    require_once "/home/badmin/public_html/functies/badm_db.inc.php";
    $badm_db = badm_conn_db();
?>
<head>
<META HTTP-EQUIV="Content-Type"      CONTENT="text/html; charset=iso-8859-1">
<META NAME="robots"                  CONTENT="index,follow">
<META NAME="keywords"                CONTENT="badminton,batminton,w&amp;l,badmintonvereniging,club,badmintonclub,sport,Belgium,Veltem,Herent,Leuven,Winksele,Belgi�">
<META NAME="description"             CONTENT="W&amp;L bv : De grootste en leukste badmintonclub van Brabant">
<META NAME="abstract"                CONTENT="badminton, club">
<META NAME="Area"                    CONTENT="General">
<META NAME="Author"                  CONTENT="W&amp;L bv">
<META NAME="Copyright"               CONTENT="(c) 2003-2015 W&amp;L bv">
<META NAME="Creator"                 CONTENT="Freek Ceymeulen">
<META NAME="Generator"               CONTENT="W&amp;L bv">
<META NAME="Owner"                   CONTENT="W&ampL bv">
<META NAME="Publisher"               CONTENT="W&ampL bv">
<META NAME="Rating"                  CONTENT="General">
<META NAME="revisit-after"           CONTENT="7 Days">
<META NAME="revisit"                 CONTENT="7 Days">
<META NAME="document-classification" CONTENT="Internet">
<META NAME="document-type"           CONTENT="Public">
<META NAME="document-rating"         CONTENT="Safe for Kids">
<META NAME="document-distribution"   CONTENT="Global">
<META HTTP-EQUIV="Content-Type"      CONTENT="text/html; charset=iso-8859-1">
<META HTTP-EQUIV="Audience"          CONTENT="General">
<META HTTP-EQUIV="content-language"  CONTENT="NL">
<META HTTP-EQUIV="Pragma"            CONTENT="no-cache">
<META HTTP-EQUIV="Expires"           CONTENT="now">
<!-- InstanceBeginEditable name="doctitle" -->
<title>W&amp;L badmintonvereniging vzw - Inschrijving</title>
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="head" -->
<script type="text/javascript" language="JavaScript" src="http://www.badmintonsport.be/scripts/createXmlHttpRequest.js"></script>
<!--<script type="text/javascript" language="JavaScript" src="../scripts/CalendarPopup.js"></script>
<script type="text/javascript" language="JavaScript">
  document.write(getCalendarStyles());
  var cal = new CalendarPopup("datediv");
  cal.showNavigationDropdowns();
-->
<script type="text/javascript" language="JavaScript">
function uncheckall()
{
  var ins = document.getElementsByTagName('input');
  for (var i=0; i<ins.length; i++)
  {
    if (ins[i].getAttribute('type') == 'checkbox')
    {
      ins[i].checked = false;
    }
  }
}
function atLeastOneHourChecked()
{
  var aloc = false;
  var ins = document.getElementsByTagName('input');
  for (var i=0; i<ins.length; i++)
  {
    if (ins[i].getAttribute('type') == 'checkbox' && ins[i].getAttribute('class') == 'input')
    {
      if(ins[i].checked)
      {
         aloc = true;
      }
    }
  }
  if (!aloc)
  {
    ;
      //alert("Gelieve minstens 1 speeluur aan te vinken");
      //document.getElementById('message').innerHTML = '<img src="../images/warning_16.png" alt=""> Gelieve minstens 1 speeluur aan te vinken';
      //document.getElementById('message').style.display = 'block';
  }
  return aloc;
}
function atLeastOneChecked()
{
  if (document.getElementById('administratief_lid').checked || atLeastOneHourChecked())
  {
    return true;
  }
  else
  {
    //alert("Gelieve minstens 1 speeluur aan te vinken");
    document.getElementById('message').innerHTML = '<img src="../images/warning_16.png" alt=""> Gelieve minstens 1 speeluur aan te vinken';
    document.getElementById('message').style.display = 'block';
    return false;
  }
}
var jeugd = false;
// ------------------------------------------------------------------------------
// deze functie controleert de juistheid van het e-mailadres op meerdere punten
// ------------------------------------------------------------------------------
function checkMail(emailAddress)
{
   var foundAtSymbol = 0;
   var foundDot = 0;
   var md;

// Go through each character in the email address.
   for (var x=0; x<emailAddress.length - 1; x++)
   {
      md = emailAddress.substr(x, 1);

   // Is the character an @ symbol?
      if (md == '@')
         foundAtSymbol++;

   // Count how many dots there are after the @ symbol.
      if (md == '.' && foundAtSymbol == 1)
         foundDot++;
   }

// Is there only one @ symbol, and are there more than one dots?
   if (foundDot > 0 && foundAtSymbol == 1)
   {
      return true;
   }
   else
   {
      return false;
   }
}
// ------------------------------------------------------------------------------
// deze functie controleert de juistheid van het datumformaat
// ------------------------------------------------------------------------------
function checkDateFormat(datum)
{
  // Required format = DD/MM/YYYY
  if (datum.length == 10)
  {
    var arr = datum.split("/");
    if (arr[0].length != 2 || arr[1].length != 2 || arr[2].length != 4)
    {
      return false;
    }
  }
  else
  {
    return false;
  }
  return true;
}
function isAlphabet($object)
{
  var alphaExp = /^[A-Za-z -]{2,50}$/;
  if($object.value.match(alphaExp))
  {
    return true;
  }
  else
  {
    return false;
  }
}
function isNumeric($object)
{
  var numericExpression = /^[0-9]+$/;
  if($object.value.match(numericExpression))
  {
    return true;
  }
  else
  {
    return false;
  }
}
var focus = 0;
//check voornaam
function checkVoornaam()
{
   first_name = document.inschrijven.elements['voornaam'];
   if (first_name.value == "" || first_name.value == null)
   {
      //alert("Gelieve je voornaam in te vullen");
      document.getElementById('message').innerHTML = '<img src="../images/warning_16.png" alt=""> Gelieve je voornaam in te vullen';
      document.getElementById('message').style.display = 'block';
      //first_name.focus();
      if(focus==0)
      {
         setTimeout("first_name.focus();",1);
         focus = 1;
      }
      return(false);
   }
   if (first_name.value.length < 2)
   {
      //alert("Gelieve je voornaam aan te vullen, hij is te kort");
      document.getElementById('message').innerHTML = '<img src="../images/warning_16.png" alt=""> Gelieve je voornaam aan te vullen, hij is te kort';
      document.getElementById('message').style.display = 'block';
      //first_name.focus();
      if(focus==0)
      {
         setTimeout("first_name.focus();",1);
         focus = 1;
      }
      return(false);
   }
   if (!isAlphabet(first_name))
   {
      //alert("Gelieve je voornaam te corrigeren, hij bevat cijfers of andere tekens");
      document.getElementById('message').innerHTML = '<img src="../images/warning_16.png" alt=""> Gelieve je voornaam te corrigeren, hij bevat cijfers of andere tekens';
      document.getElementById('message').style.display = 'block';
      //first_name.focus();
      if(focus==0)
      {
         setTimeout("first_name.focus();",1);
         focus = 1;
      }
      return(false);
   }
   focus = 0;
   document.getElementById('message').style.display = 'none';
   return true;
}
//check achternaam
function checkAchternaam()
{
   last_name = document.inschrijven.elements['achternaam'];
   if (last_name.value == "" || last_name.value == null)
   {
      //alert("Gelieve je achternaam in te vullen");
      document.getElementById('message').innerHTML = '<img src="../images/warning_16.png" alt=""> Gelieve je achternaam in te vullen';
      document.getElementById('message').style.display = 'block';
      //last_name.focus();
      if(focus==0)
      {
         setTimeout("last_name.focus();",1);
         focus = 1;
      }
      return(false);
   }
   if (last_name.value.length < 2)
   {
      //alert("Gelieve je achternaam aan te vullen, hij is te kort");
      document.getElementById('message').innerHTML = '<img src="../images/warning_16.png" alt=""> Gelieve je achternaam aan te vullen, hij is te kort';
      document.getElementById('message').style.display = 'block';
      //last_name.focus();
      if(focus==0)
      {
         setTimeout("last_name.focus();",1);
         focus = 1;
      }
      return(false);
   }
   if (!isAlphabet(last_name))
   {
      //alert("Gelieve je achternaam te corrigeren, hij bevat cijfers of andere tekens");
      document.getElementById('message').innerHTML = '<img src="../images/warning_16.png" alt=""> Gelieve je achternaam te corrigeren, hij bevat cijfers of andere tekens';
      document.getElementById('message').style.display = 'block';
      //last_name.focus();
      if(focus==0)
      {
         setTimeout("last_name.focus();",1);
         focus = 1;
      }
      return(false);
   }
   focus = 0;
   document.getElementById('message').style.display = 'none';
   return true;
}
//check VBL lidnummer
function checkLidnr()
{
   lidnr = document.inschrijven.elements['lidnr'];
   if(lidnr.value != "" && lidnr.value != null)
   {
      if (lidnr.value.length != 8)
      {
         //alert("Gelieve je VBL lidnummer te corrigeren, het moet 8 cijfers lang zijn");
         document.getElementById('message').innerHTML = '<img src="../images/warning_16.png" alt=""> Gelieve je VBL lidnummer te corrigeren, het moet 8 cijfers lang zijn';
         document.getElementById('message').style.display = 'block';
         //lidnr.focus();
         if(focus==0)
         {
            setTimeout("lidnr.focus();",1);
            focus = 1;
         }
         return(false);
      }
      if (!isNumeric(lidnr))
      {
         //alert("Gelieve je VBL lidnummer te corrigeren, het is niet numeriek");
         document.getElementById('message').innerHTML = '<img src="../images/warning_16.png" alt=""> Gelieve je VBL lidnummer te corrigeren, het is niet numeriek';
         document.getElementById('message').style.display = 'block';
         //lidnr.focus();
         if(focus==0)
         {
            setTimeout("lidnr.focus();",1);
            focus = 1;
         }
         return(false);
      }
      if (lidnr.value.substring(0,1) != '5')
      {
         //alert("Gelieve je VBL lidnummer te corrigeren, het moet starten met 500 of 501");
         document.getElementById('message').innerHTML = '<img src="../images/warning_16.png" alt=""> Gelieve je VBL lidnummer te corrigeren, het moet starten met 5';
         document.getElementById('message').style.display = 'block';
         //lidnr.focus();
         if(focus==0)
         {
            setTimeout("lidnr.focus();",1);
            focus = 1;
         }
         return(false);
      }
   }
<?php
  $current_month = date("n");
  $current_day = date("d");
  if ($current_month == 6 && $current_day <= 25)
  {
?>
   else
   {
     //alert("De inschrijvingen zijn momenteel beperkt tot bestaande leden (met VBL lidnummer). Nieuwe leden kunnen inschrijven vanaf 1 juli.");
     document.getElementById('message').innerHTML = '<img src="../images/warning_16.png" alt=""> De inschrijvingen zijn momenteel beperkt tot bestaande leden (met VBL lidnummer). Nieuwe leden kunnen inschrijven vanaf 1 juli.';
     document.getElementById('message').style.display = 'block';
     //lidnr.focus();
     if(focus==0)
     {
        setTimeout("lidnr.focus();",1);
        focus = 1;
     }
     return(false);
   }
<?php
  }
?>
   focus = 0;
   document.getElementById('message').style.display = 'none';
   return true;
}
//check E-mail address
function checkEmail()
{
   email = document.inschrijven.elements['email'];
   if (email.value == "" || email.value == null)
   {
      //alert("Gelieve je e-mail adres in te vullen");
      document.getElementById('message').innerHTML = '<img src="../images/warning_16.png" alt=""> Gelieve je e-mail adres in te vullen';
      document.getElementById('message').style.display = 'block';
      //email.focus();
      if(focus==0)
      {
         setTimeout("email.focus();",1);
         focus = 1;
      }
      return(false);
   }
   if (!checkMail(email.value))
   {
      //alert("Gelieve je e-mail adres te corrigeren, het is niet correct");
      document.getElementById('message').innerHTML = '<img src="../images/warning_16.png" alt=""> Gelieve je e-mail adres te corrigeren, het is niet correct';
      document.getElementById('message').style.display = 'block';
      //email.focus();
      if(focus==0)
      {
         setTimeout("email.focus();",1);
         focus = 1;
      }
      return(false);
   }
   focus = 0;
   document.getElementById('message').style.display = 'none';
   return true;
}
//check straat
function checkStraat()
{
   street = document.inschrijven.elements['straat'];
   if (street.value == "" || street.value == null)
   {
      //alert("Gelieve je straat in te vullen");
      document.getElementById('message').innerHTML = '<img src="../images/warning_16.png" alt=""> Gelieve je straat in te vullen';
      document.getElementById('message').style.display = 'block';
      //street.focus();
      if(focus==0)
      {
         setTimeout("street.focus();",1);
         focus = 1;
      }
      return(false);
   }
   if (street.value.length < 2)
   {
      //alert("Gelieve je straat aan te vullen");
      document.getElementById('message').innerHTML = '<img src="../images/warning_16.png" alt=""> Gelieve je straat aan te vullen';
      document.getElementById('message').style.display = 'block';
      //street.focus();
      if(focus==0)
      {
         setTimeout("street.focus();",1);
         focus = 1;
      }
      return(false);
   }
   focus = 0;
   document.getElementById('message').style.display = 'none';
   return true;
}
//check geboortedatum
function daysInMonth(month,year)
{ // months are 1-12
  var dd = new Date(year, month, 0);
  return dd.getDate();
}
function checkGeboortedatum()
{
   birth_dt = document.inschrijven.elements['geboortedatum'];
   if (birth_dt.value == "" || birth_dt.value == null)
   {
      //alert("Gelieve je geboortedatum in te vullen");
      document.getElementById('message').innerHTML = '<img src="../images/warning_16.png" alt=""> Gelieve je geboortedatum in te vullen';
      document.getElementById('message').style.display = 'block';
      //birth_dt.focus();
      if(focus==0)
      {
         setTimeout("birth_dt.focus();",1);
         focus = 1;
      }
      return(false);
   }
  var today = new Date();
   today.setFullYear(2014,0,1); // 1 januari 2014
  var d = birth_dt.value;
  if (!/\d{2}\/\d{2}\/\d{4}/.test(d))
  { // check valid format
      //alert("Gelieve je geboortedatum in te vullen in het juiste formaat: DD/MM/YYYY");
      document.getElementById('message').innerHTML = '<img src="../images/warning_16.png" alt=""> Gelieve je geboortedatum in te vullen in het juiste formaat: DD/MM/YYYY';
      document.getElementById('message').style.display = 'block';
      //birth_dt.focus();
      if(focus==0)
      {
         setTimeout("birth_dt.focus();",1);
         focus = 1;
      }
      return(false);
  }

  d = d.split("/");
  var byr = parseInt(d[2]);
  var nowyear = today.getFullYear();
  if (byr >= nowyear-4)
  { // check valid year
      //alert("Helaas, je bent nog te jong. Inschrijven vanaf 5 jaar.");
      document.getElementById('message').innerHTML = '<img src="../images/warning_16.png" alt=""> Helaas, je bent nog te jong. Inschrijven vanaf 5 jaar.';
      document.getElementById('message').style.display = 'block';
      if(focus==0)
      {
         setTimeout("birth_dt.focus();",1);
         focus = 1;
      }
      return(false);
  }
  if (byr < 1926)
  { // check valid year
      //alert("Ongeldige geboortedatum");
      document.getElementById('message').innerHTML = '<img src="../images/warning_16.png" alt=""> Ongeldige geboortedatum';
      document.getElementById('message').style.display = 'block';
      if(focus==0)
      {
         setTimeout("birth_dt.focus();",1);
         focus = 1;
      }
      return(false);
  }
  var bmth = parseInt(d[1],10)-1; // radix 10!
  if (bmth <0 || bmth >11)
  { // check valid month 0-11
      //alert("Ongeldige geboortedatum");
      document.getElementById('message').innerHTML = '<img src="../images/warning_16.png" alt=""> Ongeldige geboortedatum';
      document.getElementById('message').style.display = 'block';
      if(focus==0)
      {
         setTimeout("birth_dt.focus();",1);
         focus = 1;
      }
      return(false);
  }
  var bdy = parseInt(d[0],10); // radix 10!
  var dim = daysInMonth(bmth+1,byr);
  if (bdy <1 || bdy > dim)
  { // check valid date according to month
      //alert("Ongeldige geboortedatum");
      document.getElementById('message').innerHTML = '<img src="../images/warning_16.png" alt=""> Ongeldige geboortedatum';
      document.getElementById('message').style.display = 'block';
      if(focus==0)
      {
         setTimeout("birth_dt.focus();",1);
         focus = 1;
      }
      return(false);
  }

  var age = nowyear - byr;
  var nowmonth = today.getMonth();
  var nowday = today.getDate();
  var age_month = nowmonth - bmth;
  var age_day = nowday - bdy;
  if (age < 19 )
  {
     ;//jeugd = true;
  }
  else if (age == 19 && age_month <= 0 && age_day <=0)
   {
     ;//jeugd = true;
   }
   else
   {
     jeugd = false;
   }
   focus = 0;
   document.getElementById('message').style.display = 'none';
   //calculate_basisbedrag();
   return true;
}
//check postcode
function checkPostcode()
{
   zip_code = document.inschrijven.elements['postcode'];
   if (zip_code.value == "" || zip_code.value == null)
   {
      //alert("Gelieve je postcode in te vullen");
      document.getElementById('message').innerHTML = '<img src="../images/warning_16.png" alt=""> Gelieve je postcode in te vullen';
      document.getElementById('message').style.display = 'block';
      //zip_code.focus();
      if(focus==0)
      {
         setTimeout("zip_code.focus();",1);
         focus = 1;
      }
      return(false);
   }
   if (!isNumeric(zip_code))
   {
      //alert("Gelieve je postcode te corrigeren, ze is niet numeriek");
      document.getElementById('message').innerHTML = '<img src="../images/warning_16.png" alt=""> Gelieve je postcode te corrigeren, ze is niet numeriek';
      document.getElementById('message').style.display = 'block';
      //zip_code.focus();
      if(focus==0)
      {
         setTimeout("zip_code.focus();",1);
         focus = 1;
      }
      return(false);
   }
   focus = 0;
   document.getElementById('message').style.display = 'none';
   return true;
}
//check klassement
function checkKlassement()
{
   klassement = document.inschrijven.elements['klassement'];
   if (klassement.value == "EMPTY")
   {
      //alert("Gelieve je klassement in te vullen");
      document.getElementById('message').innerHTML = '<img src="../images/warning_16.png" alt=""> Gelieve je klassement in te vullen';
      document.getElementById('message').style.display = 'block';
      //klassement.focus();
      if(focus==0)
      {
         setTimeout("klassement.focus();",1);
         focus = 1;
      }
      return(false);
   }
   focus = 0;
   document.getElementById('message').style.display = 'none';
   return true;
}
//check statuut
function checkStatuut()
{
  if (!document.getElementById('administratief_lid').checked)
  {
     statuut = document.inschrijven.elements['statuut'];
     if (statuut.value == "EMPTY")
     {
        //alert("Gelieve je statuut in te vullen");
        document.getElementById('message').innerHTML = '<img src="../images/warning_16.png" alt=""> Gelieve je statuut in te vullen';
        document.getElementById('message').style.display = 'block';
        //statuut.focus();
        if(focus==0)
        {
           setTimeout("statuut.focus();",1);
           focus = 1;
        }
        return(false);
     }
     focus = 0;
     document.getElementById('message').style.display = 'none';
     calculate_basisbedrag();
  }
  return true;
}
//check geslacht
function checkGeslacht()
{
   gender = document.inschrijven.elements['geslacht'];
   if (gender.value == "EMPTY")
   {
      //alert("Gelieve je geslacht in te vullen");
      document.getElementById('message').innerHTML = '<img src="../images/warning_16.png" alt=""> Gelieve je geslacht in te vullen';
      document.getElementById('message').style.display = 'block';
      //gender.focus();
      if(focus==0)
      {
         setTimeout("gender.focus();",1);
         focus = 1;
      }
      return(false);
   }
   focus = 0;
   document.getElementById('message').style.display = 'none';
   return true;
}
//check tel/gsm
function checkTel()
{
   tel = document.inschrijven.elements['telefoon'];
   if (tel.value == "" || tel.value == null)
   {
      gsm = document.inschrijven.elements['mobiel'];
      if (gsm.value == "" || gsm.value == null)
      {
         //alert("Gelieve je telefoonnummer en/of gsm-nummer in te vullen");
         document.getElementById('message').innerHTML = '<img src="../images/warning_16.png" alt=""> Gelieve je telefoonnummer en/of gsm-nummer in te vullen';
         document.getElementById('message').style.display = 'block';
         //tel.focus();
         if(focus==0)
         {
            setTimeout("tel.focus();",1);
            focus = 1;
         }
         return(false);
      }
   }
   focus = 0;
   document.getElementById('message').style.display = 'none';
   return true;
}
//check maat
function checkMaat()
{
   size = document.inschrijven.elements['shirt'];
   if (size.value == "EMPTY")
   {
      //alert("Gelieve je maat in te vullen");
      document.getElementById('message').innerHTML = '<img src="../images/warning_16.png" alt=""> Gelieve je maat in te vullen';
      document.getElementById('message').style.display = 'block';
      //size.focus();
      if(focus==0)
      {
         setTimeout("size.focus();",1);
         focus = 1;
      }
      return(false);
   }
   focus = 0;
   document.getElementById('message').style.display = 'none';
   return true;
}
//check reglement
function checkReglement()
{
   reglement = document.inschrijven.elements['reglement'];
   if (!reglement.checked)
   {
      //alert("Gelieve akkoord te gaan met het huishoudelijk reglement");
      document.getElementById('message').innerHTML = '<img src="../images/warning_16.png" alt=""> Gelieve akkoord te gaan met het huishoudelijk reglement';
      document.getElementById('message').style.display = 'block';
      //reglement.focus();
      if(focus==0)
      {
         setTimeout("reglement.focus();",1);
         focus = 1;
      }
      return(false);
   }
   focus = 0;
   document.getElementById('message').style.display = 'none';
   return true;
}
function validateForm()
{
  document.getElementById('message').innerHTML = '';
<?php
   if ($page == "1")
   {
?>
  if(checkEmail())
  {
    return true;
  }
<?php
   }
  else
  {
?>
   if(checkVoornaam())
  {
    if(checkAchternaam())
    {
      if(checkLidnr())
      {
        if(checkEmail())
        {
          if(checkStraat())
          {
            if(checkGeboortedatum())
            {
              if(checkPostcode())
              {
                if(checkKlassement())
                {
                  if(checkStatuut())
                  {
                    if(checkGeslacht())
                    {
                      if(checkTel())
                      {
                        if(checkMaat())
                        {
                          if(atLeastOneChecked())
                          {
                              if (checkReglement())
                              {
                              return true;
                             }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
<?php
  }
?>
  //alert(document.getElementById('message').innerHTML);
  return false;
}
var submitcount = 0;
// ------------------------------------------------------------------------------
// Main function
// ------------------------------------------------------------------------------
function checkSubmitCount()
{
  if (validateForm())
  {
    submitcount = submitcount + 1;
    if (submitcount == 1)
    {
      document.inschrijven.submit();
      //alert('Not implemented.');
    }
    else
    {
      if (submitcount == 2)
      {
        alert("Je hebt deze gegevens al verzonden");
      }
      else
      {
        alert("Je hebt deze gegevens nu al" + submitcount.toString() + " keer verzonden");
      }
    }
  }
}
<?php
  $current_month = date("n");
  if ($current_month == 6)
  {
    $exp_date = date("Y")+"-06-25";
  }
  else
  {
    $exp_date = "2015-06-25";
  }
  $todays_date = date("Y-m-d");
  $today = strtotime($todays_date);
  $expiration_date = strtotime($exp_date);
  if ($today < $expiration_date)
  {
    //echo "var vroegboekkorting = 30;";
    echo "var vroegboekkorting = 0;";
    echo "var boete = 0;";
  }
  else
  {
    echo "var vroegboekkorting = 0;";
    if ($current_month == 12 || $current_month == 1 || $current_month == 2)
    {
        echo "var boete = 0;";
    }
    else
    {
        //echo "var boete = -30;";
        echo "var boete = 0;";
    }
  }
  if ($current_month == 12)
  {
    echo "var discount_pct = ".(4/6).";";
  }
  elseif ($current_month == 1)
  {
    echo "var discount_pct = ".(4/6).";";
  }
  elseif ($current_month == 2)
  {
    echo "var discount_pct = ".(4/6).";";
  }
  elseif ($current_month == 3)
  {
    echo "var discount_pct = ".(2/6).";";
  }
  elseif ($current_month == 4)
  {
    echo "var discount_pct = ".(2/6).";";
  }
  elseif ($current_month == 5)
  {
    echo "var discount_pct = ".(2/6).";";
  }
  else
  {
    echo "var discount_pct = ".(6/6).";";
  }
?>
var basisbedrag = 0;
var speelurenbedrag = 0;
var totaalbedrag = 0;
function calculate_bedrag()
{
  totaalbedrag = (basisbedrag + speelurenbedrag - vroegboekkorting + boete) * discount_pct;
  totaalbedrag = Math.round(totaalbedrag*100)/100;
  document.getElementById('totaalbedrag').innerHTML = totaalbedrag;
  document.getElementById('bedrag').value = totaalbedrag;
}
function calculate_basisbedrag()
{
  if (jeugd)
  {
    basisbedrag = 125;
  }
  else
  {
      l_statuut = document.getElementById('statuut');
      if (l_statuut)
      {
        if (l_statuut.value == "R")
        {
          basisbedrag = 100;
        }
        else
        {
          if (l_statuut.value == "CT")
          {
            basisbedrag = 125;
          }
          else
          {
            if(document.getElementById('geboortedatum').value == "" || l_statuut.value == "EMPTY")
            {
                basisbedrag = 125;
            }
            else
            {
                  //alert("Je bent te oud voor het statuut van jeugdspeler (<19j)");
                  document.getElementById('message').innerHTML = '<img src="../images/warning_16.png" alt=""> Je bent te oud voor het statuut van jeugdspeler (<19j)';
                  document.getElementById('message').style.display = 'block';
               l_statuut.value = 'CT';
               basisbedrag = 125;
             }
          }
      }
    }
  }
  l_basisbedrag = document.getElementById('basisbedrag');
  if (l_basisbedrag)
  {
    l_basisbedrag.innerHTML = basisbedrag;
    calculate_bedrag();
  }
}
function calculate_speelurenbedrag ($id)
{
  if (document.getElementById('administratief_lid').checked)
  {
    //alert("Als administratief lid kan je geen speeluren aanvinken");
    document.getElementById('message').innerHTML = '<img src="../images/warning_16.png" alt=""> Als administratief lid kan je geen speeluren aanvinken';
    document.getElementById('message').style.display = 'block';
    document.getElementById('uur'+$id).checked = false;
  }
  else
  {
    document.getElementById('message').style.display = 'none';
    prijs = parseInt(document.getElementById('prijs'+$id).innerHTML);
    if (document.getElementById('uur'+$id).checked)
    {
      speelurenbedrag = speelurenbedrag + prijs;
    }
    else
    {
      speelurenbedrag = speelurenbedrag - prijs;
    }
    document.getElementById('speelurenbedrag').innerHTML = speelurenbedrag;
    calculate_bedrag();
  }
}
function set_administrative_member ()
{
  document.getElementById('message').style.display = 'none';
  //prijs = parseInt(document.getElementById('prijs0').innerHTML);
  if (document.getElementById('administratief_lid').checked)
  {
    if (atLeastOneHourChecked())
    {
      //alert("Als administratief lid kan je geen speeluren aanvinken");
      document.getElementById('message').innerHTML = '<img src="../images/warning_16.png" alt=""> Als administratief lid kan je geen speeluren aanvinken';
      document.getElementById('message').style.display = 'block';
      document.getElementById('administratief_lid').checked = false;
    }
    else
    {
      //speelurenbedrag = 0;
      basisbedrag = 42; // prijs;
      document.getElementById('basisbedrag').innerHTML = basisbedrag;
    }
  }
  else
  {
    speelurenbedrag = 0;
    calculate_basisbedrag();
  }
  document.getElementById('speelurenbedrag').innerHTML = speelurenbedrag;
  calculate_bedrag();
}
function popupFieldHelp(pURL,pname,pWidth,pHeight,pScroll,pResizable)
{
  if(!pURL){pURL = 'about:blank'}
  if(!pname){pname = 'Popup'}
  if(!pWidth){pWidth = 600}
  if(!pHeight){pHeight = 600}
  if(!pScroll){pScroll = 'yes'}
  if(!pResizable){pResizable = 'yes'}
  l_Window = window.open(pURL, pname, 'toolbar=no,scrollbars='+pScroll+',location=no,statusbar=no,menubar=no,resizable='+pResizable+',width='+pWidth+',height='+pHeight);
  if (l_Window.opener == null){l_Window.opener = self;}
  l_Window.focus();
  //  return l_Window;
  return;
}
function showHelp (pId)
{
  popupFieldHelp('http://www.badmintonsport.be/tornooien/help.php?id='+pId, null, 500, 350);
}
//-->
</script>
<style type="text/css">
<!--
.help {
    cursor : help;
    text-decoration : none;
    white-space : nowrap;
}
a.help {
    color : black;
    text-decoration : none;
}
#personaliaFrame {
    border : 1px solid #006600;
}
#personalia {
    background-color : #EFEFEF;
}
#inschrijvingFrame {
    border : 1px solid #006600;
}
#inschrijving {
    background-color : #EFEFEF;
}
#message {
    background-color : lightyellow;
    border : 1px solid black;
    color : red;
    display : <?php echo (isset($message)) ? "block" : "none"; ?>;
    margin-bottom : 5px;
    margin-top : 5px;
    padding : 5px;
}
#control {
      background-color : #cccccc;
      border           : 1px #999999 solid;
      height           : 24px;
      margin-bottom    : 10px;
      margin-top       : 10px;
      padding          : 3px;
      width            : 99%;
}
table.xpbutton td.R {
    width : 4px;
}
table.xpbutton td.L {
    width : 4px;
}
table.xpbutton td.R img {
    display : block;
}
table.xpbutton td.L img {
    display : block;
}
table.xpbutton {
    color   : #333333;
    display : inline;
}
table.xpbutton td.C {
    background-image  : url(../images/button_xp_center.gif);
    background-repeat : repeat-x;
    white-space       : nowrap;
}
table.xpbutton td.C a {
    display         : block;
    font-family     : Verdana, Geneva, Arial, Helvetica, sans-serif;
    font-size       : 12px;
    font-weight     : bold;
    padding-left    : 3px;
    padding-right   : 3px;
    text-decoration : none;
    white-space     : nowrap;
}
table.xpbutton td.C a:visited {
    display         : block;
    font-family     : Verdana, Geneva, Arial, Helvetica, sans-serif;
    font-size       : 12px;
    font-weight     : bold;
    padding-left    : 3px;
    padding-right   : 3px;
    text-decoration : none;
    white-space     : nowrap;
}
-->
</style>

<!-- InstanceEndEditable -->
<link href="http://www.badmintonsport.be/css/badminton.css" rel="stylesheet" type="text/css">
<script type="text/javascript" language="JavaScript" src="http://www.badmintonsport.be/scripts/stm31.js"></script>
<script type="text/javascript" language="JavaScript" src="http://www.badmintonsport.be/scripts/dw_rotator.js"></script>
<script type="text/javascript" language="JavaScript">
<!--
function initRotator()
{
  var rotator1 = new dw_Rotator("sponsors_banner", 4000, "http://www.badmintonsport.be/images/", "_blank");
  rotator1.addImages("Veltem_Motors_banner.png", "banner_verhoeven.jpg");
  //rotator1.addImages("Veltem_Motors_banner.png");
  rotator1.addActions("http://www.veltem-motors.be", "http://www.isolatieverhoeven.be");
  //rotator1.addActions("http://www.veltem-motors.be");
  dw_Rotator.start();
}
function doLoad()
{
  // uncheck all checkboxes
  uncheckall();
  // initialize the menu
  st_onload();
  // initialize the banner rotator
  initRotator();
  calculate_basisbedrag();
}
//-->
</script>
</head>
<body class="badminton" onload="doLoad();">
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <!--DWLayoutTable-->
  <tr>
  <td colspan="2">
   <table width="100%" border="0" cellpadding="0" cellspacing="0">
    <!--DWLayoutTable-->
    <tr>
     <td width="100%" style="background-color: #006600;"><img src="http://www.badmintonsport.be/images/pt_transp.gif" width="1px" height="4px" alt=""></td>
    </tr>
    <tr>
     <td align="center" valign="top" bgcolor="#CCCCCC">
      <table width="100%" border="0" cellspacing="0" cellpadding="3">
       <tr align="center" valign="middle">
        <td rowspan="2" align="left">
       <img src="http://www.badmintonsport.be/images/logow&amp;l-100.gif" width="100" height="80" alt="logo">
      </td>
        <td>
       <h4><abbr title="Winksele en Leuven">W&amp;L</abbr> Badmintonvereniging <abbr lang="nl" title="vereniging zonder winstoogmerk">vzw</abbr><br>
           Mechelsesteenweg 387<br>
             3020 Herent</h4>
    </td>
        <td align="right"><a href="javascript:void();" onclick="return dw_Rotator.doClick(0)"  onmouseover="dw_Rotator.pause(0)" onmouseout="dw_Rotator.resume(0)" onfocus="this.blur()"><img id="sponsors_banner" name="sponsors_banner" src="http://www.badmintonsport.be/images/Veltem_Motors_banner.png" alt="sponsors" width="320" height="100" border="0"></a></td>
       </tr>
      </table>
   </td>
    </tr>
    <tr>
     <td bgcolor="#336600"><img src="http://www.badmintonsport.be/images/pt_transp.gif" width="1px" height="4px" alt=""></td>
    </tr>
   </table>
  </td>
 </tr>
 <tr>
  <td width="150" height="213" valign="top">
<script type="text/javascript" language="JavaScript1.2">
<!--
stm_bm(["W&L menu",400,"","http://www.badmintonsport.be/scripts/blank.gif",0,"","",0,0,250,0,1000,1,0,0]);
stm_bp("p0",[1,4,0,0,1,4,0,7,100,"progid:DXImageTransform.Microsoft.Fade(overlap=.5,enabled=0,Duration=0.30)",-2,"progid:DXImageTransform.Microsoft.Pixelate(MaxSquare=15,enabled=0,Duration=0.50)",-2,50,0,0,"#669999","#006633","",3,0,0,"#ffffff"]);
stm_ai("p0i0",[0,"Home","","",-1,-1,0,"http://www.badmintonsport.be/index.php","_self","Startpagina","","","",0,0,0,"","",0,0,0,0,1,"#fffff7",0,"#b5bed6",0,"","",3,3,0,0,"#fffff7","#000000","#000000","#000000","10pt Verdana","10pt Verdana",0,0]);
stm_aix("p0i1","p0i0",[0,"Algemeen","","",-1,-1,0,"","_self","informatie over de club","","","",0,0,0,"http://www.badmintonsport.be/scripts/arrow_r.gif","http://www.badmintonsport.be/scripts/arrow_r.gif",7,7]);
stm_bpx("p1","p0",[1,2,0,0,1,4,0]);
stm_aix("p1i0","p0i0",[0,"Bestuur","","",-1,-1,0,"http://www.badmintonsport.be/algemeen/bestuur.php","_self","Bestuursleden","","","",0,0,0,"","",0,0,0,1]);
stm_bpx("p1","p0",[1,2,0,0,1,4,0,0]);
stm_aix("p2i0","p1i0",[0,"Mededelingen","","",-1,-1,0,"http://www.badmintonsport.be/algemeen/Bestuur/bestuursmededelingen.php","_self","Bestuursmededelingen"]);
stm_ep();
stm_aix("p1i1","p0i0",[0,"Medewerkers","","",-1,-1,0,"http://www.badmintonsport.be/algemeen/medewerkers.php","_self","Medewerkers","","","",0,0,0,"","",0,0,0,1]);
stm_aix("p1i2","p0i0",[0,"Speeluren","","",-1,-1,0,"http://www.badmintonsport.be/algemeen/speeluren.php","_self","Speeluren","","","",0,0,0,"","",0,0,0,1]);
stm_aix("p1i3","p0i1",[0,"Geschiedenis","","",-1,-1,0,"","_self","Geschiedenis","","","",0,0,0,"","",0,0,0,1]);
stm_bpx("p2","p0",[1,2,0,0,1,4,0,0]);
stm_aix("p2i0","p1i0",[0,"Clubkampioenen","","",-1,-1,0,"http://www.badmintonsport.be/algemeen/Geschiedenis/clubkampioenen.php","_self","Winnaars Clubkampioenschap"]);
stm_aix("p2i1","p1i0",[0,"Nationale Kampioenen","","",-1,-1,0,"http://www.badmintonsport.be/algemeen/Geschiedenis/nationalekampioenen.php","_self","Nationale Kampioenen W&L"]);
stm_ep();
stm_aix("p1i4","p0i0",[0,"Sponsors","","",-1,-1,0,"http://www.badmintonsport.be/algemeen/sponsors.php","_self","Sponsors","","","",0,0,0,"","",0,0,0,1]);
stm_aix("p1i5","p0i0",[0,"Artikels","","",-1,-1,0,"http://www.badmintonsport.be/algemeen/artikels.php","_self","Artikels over de club","","","",0,0,0,"","",0,0,0,1]);
stm_ep();
stm_aix("p0i2","p0i0",[0,"Competitie","","",-1,-1,0,"http://www.badmintonsport.be/competitie.php","_self","Competitie: ploegen en uitslagen"]);
stm_aix("p0i3","p0i1",[0,"Tornooien","","",-1,-1,0,"http://www.badmintonsport.be/tornooien/tornooioverzicht.php","_self","Tornooien"]);
stm_bpx("p1","p0",[1,2,0,0,1,4,0]);
stm_aix("p1i0","p0i1",[0,"Inschrijvingen","","",-1,-1,0,"_self","Schrijf je hier in voor tornooien"]);
stm_bpx("p1","p0",[1,2,0,0,1,4,0,0]);
//-->
</script>
<?php
// create SQL statement
   $sql = "SELECT id
                , organisatie
                , doelgroep
                , DATE_FORMAT(start_dt, '%d/%m') AS start_dt
                , verzonden
           FROM inschr_torn
       WHERE DATE_ADD(dt_inschr, INTERVAL 21 DAY ) > NOW()
       ORDER BY dt_inschr DESC";
   $result = mysql_query ($sql, $badm_db) or badm_mysql_die();
// format results by row
   $teller = 0;
   echo "<script type=\"text/javascript\" language=\"JavaScript1.2\">\n";
   while ($badm = mysql_fetch_object($result))
   {
      echo "stm_aix(\"p2i";
      echo $teller;
      echo "\",\"p1i0\",[1,\"";
      echo $badm->organisatie;
      if ($badm->doelgroep == 'J' || $badm->doelgroep == 'V')
      {
        echo $badm->doelgroep == 'J' ? " (Jeugd)" : " (Veteranen)";
        if ($badm->verzonden == 0) //inschrijvingsperiode nog niet afgelopen
        {
          echo " <B>".$badm->start_dt."</B>";
        }
        else
        {
          echo "  <IMG src=http://www.badmintonsport.be/images/closed.png>";
        }
      }
      else
      {
        if ($badm->verzonden == 0) //inschrijvingsperiode nog niet afgelopen
        {
          echo " <B>".$badm->start_dt."</B>";
        }
        else
        {
          echo "  <IMG src=http://www.badmintonsport.be/images/closed.png>";
        }
      }
      echo "\",\"\",\"\",-1,-1,0,\"http://www.badmintonsport.be/tornooien/inschrijvingen.php?id=";
      echo $badm->id;
      echo "\",\"_self\",\"Inschrijving ";
      echo $badm->organisatie;
      echo "\"]);\n";
      $teller = $teller + 1;
   }
   echo"</script>\n";
?>
<script type="text/javascript" language="JavaScript1.2">
<!--
stm_ep();
stm_aix("p1i1","p0i0",[0,"Aanvangsuren","","",-1,-1,0,"http://www.badmintonsport.be/tornooien/aanvangsuren.php","_self","Aanvangsuren tornooien"]);
stm_aix("p1i2","p0i0",[0,"Kalender","","",-1,-1,0,"http://www.badmintonsport.be/docs/tornooikalender.pdf","_blank","Tornooikalender"]);
stm_aix("p1i3","p0i1",[0,"Resultaten","","",-1,-1,0,"","_self","Tornooiresultaten W&L spelers"]);
stm_bpx("p2","p0",[1,2,0,0,1,4,0,0]);
//-->
</script>
<?php
// create SQL statement
    $sql = "SELECT  DISTINCT IF(DATE_FORMAT(dt_van, '%m') < 8
, DATE_FORMAT(dt_van, '%Y') - 1
, DATE_FORMAT(dt_van, '%Y')) AS start
, IF(DATE_FORMAT(dt_van, '%m') < 8
, DATE_FORMAT(dt_van, '%Y')
, DATE_FORMAT(dt_van, '%Y') + 1) AS einde
           FROM bad_tornooien
           ORDER BY start DESC";
   $result = mysql_query ($sql, $badm_db) or badm_mysql_die();
// format results by row
   $teller = 0;
   echo "<script type=\"text/javascript\" language=\"JavaScript\">\n";
   while ($badm = mysql_fetch_object($result))
   {
      echo "stm_aix(\"p2i".$teller."\",\"p1i0\",[0,\"Seizoen ".$badm->start."-".$badm->einde."\",\"\",\"\",-1,-1,0,\"http://www.badmintonsport.be/tornooien/resultaten.php?s=".$badm->einde."\",\"_self\",\"Seizoen ".$badm->start."-".$badm->einde."\"]);\n";
      $teller = $teller + 1;
   }
   echo"</script>\n";
   //mysql_free_result($result);
   mysql_close($badm_db);
?>
<script type="text/javascript" language="JavaScript">
<!--stm_ep();
stm_ep();
stm_ep();
stm_aix("p0i4","p0i0",[0,"Spelers","","",-1,-1,0,"http://www.badmintonsport.be/spelers.php","_self","Spelersprofielen en statistieken"]);
stm_aix("p0i5","p0i1",[0,"Jeugd","","",-1,-1,0,"","_self","Informatie over en voor de jeugdspelers"]);
stm_bpx("p1","p0",[1,2,0,0,1,4,0]);
stm_aix("p1i0","p0i0",[0,"Algemeen","","",-1,-1,0,"http://www.badmintonsport.be/jeugd/jeugd.php","_self","Jeugd trainers"]);
stm_aix("p1i1","p0i0",[0,"Teams","","",-1,-1,0,"http://www.badmintonsport.be/jeugd/jeugdteams.php","_self","Jeugdteams"]);
stm_aix("p1i2","p0i0",[0,"J-team","","",-1,-1,0,"http://www.badmintonsport.be/jeugd/J-team.php","_self","J-team"]);
stm_aix("p1i3","p0i0",[0,"Resultaten","","",-1,-1,0,"http://www.badmintonsport.be/jeugd/resultaten_jeugd.php?s=2014","_self","Resultaten jeugtornooien W&L spelers"]);
stm_aix("p1i4","p0i0",[0,"Jeugdtornooi","","",-1,-1,0,"http://www.badmintonsport.be/jeugd/jeugdtornooi.php","_self","Jeugdtornooi W&L"]);
stm_ep();
stm_aix("p0i6","p0i0",[0,"Recreanten","","",-1,-1,0,"http://www.badmintonsport.be/recreant.php","_self","Recreanten"]);
stm_aix("p0i7","p0i1",[0,"Foto's","","",-1,-1,0,"http://users.pandora.be/ikkegg","_blank","badminton foto's"]);
//-->
</script>
<?php
/*************************************************************************************************
 * Create a link to every photo directory that exists
 ************************************************************************************************/
/* Get the root dirctory of the website */
  $rootdir = $_SERVER['DOCUMENT_ROOT'];
/* directory where all the photo's are located in subdirectories */
  $dir = $rootdir."/pictures";
/* Get all the subdirectories of the pictures directory */
  include $rootdir."/functies/getDirectoryListing.php";
  $subdirs = getDirectoryListing($dir, "a", 1, 0, "none");
/* Number of subdirs in directory */
  $subdir_cnt = count($subdirs);
/* Display a link to every subdirectory */
  for ($i = 0; $i < $subdir_cnt; $i++)
  {
    echo "<script type=\"text/javascript\" language=\"JavaScript\">\n";
    if ($i == 0) // first subdirectory
    {
       echo "stm_bpx(\"p1\",\"p0\",[1,2,0,0,1,4,0]);\n";
    }
    echo "stm_aix(\"p0i".$i."\",\"p0i0\",[0,\"".$subdirs[$i]."\",\"\",\"\",-1,-1,0,\"http://www.badmintonsport.be/pictures/showThumbnails.php?dir=".$subdirs[$i]."\",\"_self\",\"".$subdirs[$i]."\"]);\n";
    if ($i == $subdir_cnt - 1) // last subdirectory
    {
      echo "stm_ep();\n";
    }
    echo"</script>\n";
  }
?>
<script type="text/javascript" language="JavaScript">
<!--
stm_ai("p0i8",[6,4,"#006633","",0,0,0]);
stm_aix("p0i9","p0i0",[0,"Links","","",-1,-1,0,"http://www.badmintonsport.be/links.php","_self","Links"]);
stm_aix("p0i10","p0i0",[0,"Message Board","","",-1,-1,0,"http://www.badmintonsport.be/messageboard/messageboard.php?p=1&m=n&id=0","_blank","Message Board"]);
stm_aix("p0i11","p0i0",[0,"Laatste Nieuws","","",-1,-1,0,"http://www.badmintonsport.be/laatste_nieuws.php","_self","De laatste wijzigingen aan de W&L website"]);
stm_ep();
stm_em();
//-->
</script>
      &nbsp;
 <!-- InstanceBeginEditable name="counter" -->
<!-- InstanceEndEditable -->
       <!-- InstanceBeginEditable name="wiens verjaardag is het" -->
<!-- InstanceEndEditable -->
  </td>
  <td width="100%" valign="top">
<!-- InstanceBeginEditable name="general" -->
   <table width="100%" border="0" align="left" cellpadding="0" cellspacing="0">
    <tr>
     <td class="kleinetekst" width="100%">

      <form name="inschrijven" action="inschrijving.php" method="post">

<?php
  $debug .= "<br>page=".$page;
  switch ($page)
  {
    case "1": // email adres invullen
?>
      <h1>Inschrijven</h1>

      Vul je email adres in en klik op OK. Een email met de link naar het inschrijvingsformulier wordt verzonden naar het opgegeven email adres.

       <br><br><input type="text" id="email" name="email" size="40" maxlength="100" value="<?php echo $_POST["email"]; ?>" onChange="return checkEmail();"><br><br>

      <div id="message"><?php echo $message; ?></div>
      <div>
       <table class="xpbutton" cellspacing="0" cellpadding="0">
        <tr>
         <td class="L"><img src="../images/button_xp_left.gif" width="4px" height="24px" alt="" /></td>
         <td class="C"> <a href="javascript:checkSubmitCount();" title="Opslaan">&nbsp; OK &nbsp;</a> </td>
         <td class="R"><img src="../images/button_xp_right.gif" width="4px" height="24px" alt="" /></td>
        </tr>
       </table>
      </div>
      <p class="kleinetekst">Indien je geen mail ontvangt, check dan even of je geen typfout gemaakt hebt in je mailadres, en of de mail niet in je spam folder terecht gekomen is.</p>
<?php
      break;
    case "2": // email verzonden
?>
      <h1>Inschrijven</h1>

      Een email met de link naar het inschrijvingsformulier werd verzonden naar <?php echo $_POST["email"]; ?>.
      <br>Raadpleeg je mailbox en klik op de link in de email om verder te gaan.
      <br>Als je de mail niet ontvangt, kijk dan eens in je folder met ongewenste berichten (spam).
<?php
      break;
    case "3": // inschrijvingsformulier
?>
      <h1>Inschrijvingsformulier</h1>
      <table width="100%" cellspacing="0" cellpadding="0">
       <tr>
        <td>
        <table id="personaliaFrame">
         <tr>
          <td>
           <table id="personalia" class="kleinetekst">
            <tr>
             <td>Voornaam</td>
             <td><input type="text" id="voornaam" name="voornaam" size="15" maxlength="30" value="<?php echo $_POST["voornaam"]; ?>" onChange="return checkVoornaam();"></td>
             <td title="bv. 50061450">Lidnummer <abbr lang="nl" title="Vlaamse Badminton Liga">VBL</abbr></td>
             <td title="bv. 50061450"><input type="text" id="lidnr" name="lidnr" size="8" maxlength="8" value="<?php echo $_POST["lidnr"]; ?>" onChange="return checkLidnr();">&nbsp;<a href="http://www.badmintonsport.be/spelers.php" target="_blank" title="Zoek je VBL lidnummer hier">Help</a></td>
            </tr>
            <tr>
             <td>Achternaam</td>
             <td><input type="text" id="achternaam" name="achternaam" size="20" maxlength="30" value="<?php echo $_POST["achternaam"]; ?>" onChange="return checkAchternaam();"></td>
             <td>Klassement</td>
             <td><select id="klassement" name="klassement" onChange="return checkKlassement();">
<?php
  build_options2("klassement", nvl($_POST["klassement"], 'EMPTY'), $badm_db2);
?>
                 </select></td>
            </tr>
            <tr>
             <td>Statuut</td>
             <td><select id="statuut" name="statuut" onChange="return checkStatuut();">
<?php
  build_options2("statuut", nvl($_POST["statuut"], 'EMPTY'), $badm_db2);
?>
                 </select></td>
             <td>Geslacht</td>
             <td><select id="geslacht" name="geslacht" onChange="return checkGeslacht();">
<?php
  build_options2("geslacht", nvl($_POST["geslacht"], 'EMPTY'), $badm_db2);
?>
                 </select></td>
            </tr>
            <tr>
             <td title="bv. Kerkstraat 12A bus 3">Straat + huisnummer + bus</td>
             <td title="bv. Kerkstraat 12A bus 3"><input type="text" id="straat" name="straat" size="40" maxlength="50" value="<?php echo $_POST["straat"]; ?>" onChange="return checkStraat();"></td>
             <!--<td><a style="text-decoration:none;cursor : help;" href="javascript:showHelp('3')">Geboortedatum</a></td>-->
             <td title="bv. 23/04/1986">Geboortedatum</td>
             <td title="bv. 23/04/1986"><input type="text" id="geboortedatum" name="geboortedatum" size="10" maxlength="10" value="<?php echo $_POST["geboortedatum"]; ?>" onChange="return checkGeboortedatum();">
             <!--<a href="#" onClick="cal.select(document.inschrijven.geboortedatum, 'anchor1', 'dd/MM/yyyy'); return false;" name="anchor1" id="anchor1">
                        <img src="../images/b_calendar.png" alt="kalender" border="none"></a>-->&nbsp;(DD/MM/JJJJ)</td>
            </tr>
            <tr>
             <td>Postcode + Gemeente</td>
             <td><input type="text" id="postcode" name="postcode" size="4" maxlength="4" value="<?php echo $_POST["postcode"]; ?>">&nbsp;<input type="text" id="gemeente" name="gemeente" size="31" maxlength="36" value="<?php echo $_POST["gemeente"]; ?>"></td>
             <td title="bv. 02/758.89.03">Telefoon</td>
             <td title="bv. 02/758.89.03"><input type="text" id="telefoon" name="telefoon" size="10" maxlength="12" value="<?php echo $_POST["telefoon"]; ?>"></td>
            </tr>
            <tr>
             <td>E-mail</td>
             <td><input type="text" id="email2" name="email2" size="40" maxlength="100" value="<?php echo $_REQUEST["email"]; ?>" onChange="return checkEmail();"></td>
             <td title="bv. 0496/03.17.66">GSM</td>
             <td title="bv. 0496/03.17.66"><input type="text" id="mobiel" name="mobiel" size="13" maxlength="13" value="<?php echo $_POST["mobiel"]; ?>"></td>
            </tr>
            <tr>
             <td>Maat T-shirt</td>
             <td><select id="shirt" name="shirt" onChange="return checkMaat();">
<?php
  build_options2("maat", nvl($_POST["maat"], 'EMPTY'), $badm_db2);
?>
                 </select></td>
            </tr>
           </table>
          </td>
         </tr>
        </table>
        </td>
       </tr>
      </table>
      <br>
      <div id="message"><?php echo $message; ?></div>

      Vink de uren aan die je in 2015-2016 wenst te spelen. Je betaalt dan voor deze uren. Plaats je muis over een speeluur voor meer info.

      <table id="inschrijvingFrame">
       <tr>
        <td>
         <table border="0" cellspacing="0" cellpadding="5" rules="groups" id="inschrijving" class="kleinetekst">
                 <colgroup span="3">
                 <colgroup span="3">
                  <tr>
                    <td class="td2" colspan="9" align="center">Ivo Van Dammezaal <b>Veltem</b><hr></td>
                  </tr>
<?php
  $query = "SELECT LOWER(u.dag) AS dag
                 , u.uur
                 , u.plaats
                 , u.type
                 , u.doelgroep
                 , u.id
                 , u.beschrijving
                 , u.aantal_plaatsen
                 , u.aantal_reserve AS aantal_reserve_plaatsen
                 , u.prijs
                 , CASE u.dag WHEN 'maandag'   THEN 1
                              WHEN 'dinsdag'   THEN 2
                              WHEN 'woensdag'  THEN 3
                              WHEN 'donderdag' THEN 4
                              WHEN 'vrijdag'   THEN 5
                              WHEN 'zaterdag'  THEN 6
                              WHEN 'zondag'    THEN 7
                   END AS dagnummer
                 , SUM(CASE reserve WHEN 'N' THEN 1 ELSE 0 END) AS aantal_spelers
                 , SUM(CASE reserve WHEN 'Y' THEN 1 ELSE 0 END) AS aantal_reserven
              FROM bad_speeluren u
              LEFT OUTER JOIN leden_uren lu ON u.id = lu.speeluren_id
             WHERE plaats = 'VELTEM'
             GROUP BY LOWER(u.dag)
                    , u.uur
                    , u.plaats
                    , u.type
                    , u.doelgroep
                    , u.id
                    , u.beschrijving
                    , u.aantal_plaatsen
                    , u.aantal_reserve
                    , u.prijs
                    , CASE u.dag WHEN 'maandag'   THEN 1
                                 WHEN 'dinsdag'   THEN 2
                                 WHEN 'woensdag'  THEN 3
                                 WHEN 'donderdag' THEN 4
                                 WHEN 'vrijdag'   THEN 5
                                 WHEN 'zaterdag'  THEN 6
                                 WHEN 'zondag'    THEN 7
                      END
             ORDER BY 11, 2, 3, 5";
  $resultaat = mysql_query($query, $badm_db) or badm_mysql_die();

  $totaal = mysql_num_rows($resultaat);
  $num_rows = ceil($totaal / 3);
  $ids = array();
  $checks = array();
  $uren = array();
  $types = array();
  $prijs = array();
  $reserve = array();
  $description = array();
  while ($rij = mysql_fetch_object($resultaat))
  {
    // Put the data in 7 arrays
    $ids[] = $rij->id;
    $checks[] = $rij->spelers_id;
    $uren[] = $rij->dag."&nbsp;".$rij->uur."&nbsp;";
    $types[] = "&nbsp;".$rij->type."&nbsp;".$rij->doelgroep."&nbsp;";
    $prijs[] = $rij->prijs;
    $description[] = $rij->beschrijving;
    if ($rij->aantal_spelers < $rij->aantal_plaatsen)
    {
      $reserve[] = 'N';
    }
    elseif ($rij->aantal_reserven < $rij->aantal_reserve_plaatsen)
    {
      $reserve[] = 'Y'; // nog reserveplaatsen
    }
    else
    {
      $reserve[] = 'V'; // volledig volzet
    }
  }
  // Write speeluren
  for ($i=0; $i < $num_rows; $i++)
  {
    echo "                  <tr>\n";
    $checked = is_null($checks[$i]) ? "" : " CHECKED";
    // kolom 1
    write_speeluur($ids[$i], $checked, $uren[$i], $types[$i], $reserve[$i], $prijs[$i], $description[$i]);
    $index = $i + $num_rows;
    if (array_key_exists((int)$index, $ids))
    {
      $checked = is_null($checks[$index]) ? "" : " CHECKED";
      // kolom 2
      write_speeluur($ids[$index], $checked, $uren[$index], $types[$index], $reserve[$index], $prijs[$index], $description[$index]);
    }
    else
    {
      echo '<td class="td2">&nbsp;</td><td class="td2">&nbsp;</td><td class="td2">&nbsp;</td>';
    }
    $index = $i + (2 * $num_rows);
    if (array_key_exists((int)$index, $ids))
    {
      $checked = is_null($checks[$i + (2 * $num_rows)]) ? "" : " CHECKED";
      // kolom 3
      write_speeluur($ids[$index], $checked, $uren[$index], $types[$index], $reserve[$index], $prijs[$index], $description[$index]);
    }
    else
    {
      echo '<td class="td2">&nbsp;</td><td class="td2">&nbsp;</td><td class="td2">&nbsp;</td>';
    }
    echo "                  </tr>\n";
  }
?>
                  <tr>
                    <td colspan="3" align="center"><hr>Sportcomplex <b>Kessel-Lo</b><hr></td>
                    <td colspan="3" align="center"><hr>Sporthal <abbr lang="nl" title="Katholieke Hogeschool Leuven">KHL</abbr> <b>Heverlee</b><hr></td>
                    <td colspan="3" align="center">&nbsp;</td>
                  </tr>
                  <tr>
<?php
  $query  = "SELECT LOWER(u.dag) AS dag
                 , u.uur
                 , u.plaats
                 , u.type
                 , u.doelgroep
                 , u.id
                 , u.beschrijving
                 , u.aantal_plaatsen
                 , u.aantal_reserve AS aantal_reserve_plaatsen
                 , u.prijs
                 , CASE u.dag WHEN 'maandag'   THEN 1
                              WHEN 'dinsdag'   THEN 2
                              WHEN 'woensdag'  THEN 3
                              WHEN 'donderdag' THEN 4
                              WHEN 'vrijdag'   THEN 5
                              WHEN 'zaterdag'  THEN 6
                              WHEN 'zondag'    THEN 7
                   END AS dagnummer
                 , SUM(CASE reserve WHEN 'N' THEN 1 ELSE 0 END) AS aantal_spelers
                 , SUM(CASE reserve WHEN 'Y' THEN 1 ELSE 0 END) AS aantal_reserven
              FROM bad_speeluren u
              LEFT OUTER JOIN leden_uren lu ON u.id = lu.speeluren_id
             WHERE u.id = 7
             GROUP BY LOWER(u.dag)
                    , u.uur
                    , u.plaats
                    , u.type
                    , u.doelgroep
                    , u.id
                    , u.beschrijving
                    , u.aantal_plaatsen
                    , u.aantal_reserve
                    , u.prijs
                    , CASE u.dag WHEN 'maandag'   THEN 1
                                 WHEN 'dinsdag'   THEN 2
                                 WHEN 'woensdag'  THEN 3
                                 WHEN 'donderdag' THEN 4
                                 WHEN 'vrijdag'   THEN 5
                                 WHEN 'zaterdag'  THEN 6
                                 WHEN 'zondag'    THEN 7
                      END";
  $result = mysql_query($query, $badm_db) or badm_mysql_die();
  $uur7 = mysql_fetch_object($result);
  if ($uur7->aantal_spelers < $uur7->aantal_plaatsen)
  {
    $reserve = 'N';
  }
  elseif ($uur7->aantal_reserven < $uur7->aantal_reserve_plaatsen)
  {
    $reserve = 'Y'; // nog reserveplaatsen
  }
  else
  {
    $reserve = 'V'; // volledig volzet
  }
  mysql_free_result($result);
  $debug .= "<br>reserve = ".$reserve."\n";
  $checked = is_null($uur7->spelers_id) ? "" : " CHECKED";
  write_speeluur(7, $checked, $uur7->dag."&nbsp;".$uur7->uur."&nbsp;", "&nbsp;".$uur7->type."&nbsp;".$uur7->doelgroep."&nbsp;", $reserve, $uur7->prijs, $uur7->beschrijving);

  $query  = "SELECT LOWER(u.dag) AS dag
                 , u.uur
                 , u.plaats
                 , u.type
                 , u.doelgroep
                 , u.id
                 , u.beschrijving
                 , u.aantal_plaatsen
                 , u.aantal_reserve AS aantal_reserve_plaatsen
                 , u.prijs
                 , CASE u.dag WHEN 'maandag'   THEN 1
                              WHEN 'dinsdag'   THEN 2
                              WHEN 'woensdag'  THEN 3
                              WHEN 'donderdag' THEN 4
                              WHEN 'vrijdag'   THEN 5
                              WHEN 'zaterdag'  THEN 6
                              WHEN 'zondag'    THEN 7
                   END AS dagnummer
                 , SUM(CASE reserve WHEN 'N' THEN 1 ELSE 0 END) AS aantal_spelers
                 , SUM(CASE reserve WHEN 'Y' THEN 1 ELSE 0 END) AS aantal_reserven
              FROM bad_speeluren u
              LEFT OUTER JOIN leden_uren lu ON u.id = lu.speeluren_id
             WHERE u.id = 2
             GROUP BY LOWER(u.dag)
                    , u.uur
                    , u.plaats
                    , u.type
                    , u.doelgroep
                    , u.id
                    , u.beschrijving
                    , u.aantal_plaatsen
                    , u.aantal_reserve
                    , u.prijs
                    , CASE u.dag WHEN 'maandag'   THEN 1
                                 WHEN 'dinsdag'   THEN 2
                                 WHEN 'woensdag'  THEN 3
                                 WHEN 'donderdag' THEN 4
                                 WHEN 'vrijdag'   THEN 5
                                 WHEN 'zaterdag'  THEN 6
                                 WHEN 'zondag'    THEN 7
                      END";
  $result = mysql_query($query, $badm_db) or badm_mysql_die();
  $uur2 = mysql_fetch_object($result);
  if ($uur2->aantal_spelers < $uur2->aantal_plaatsen)
  {
    $reserve = 'N';
  }
  elseif ($uur2->aantal_reserven < $uur2->aantal_reserve_plaatsen)
  {
    $reserve = 'Y'; // nog reserveplaatsen
  }
  else
  {
    $reserve = 'V'; // volledig volzet
  }
  mysql_free_result($result);
  $debug .= "<br>reserve = ".$reserve."\n";
  $checked = is_null($uur2->spelers_id) ? "" : " CHECKED";
  write_speeluur(2, $checked, $uur2->dag."&nbsp;".$uur2->uur."&nbsp;", "&nbsp;".$uur2->type."&nbsp;".$uur2->doelgroep."&nbsp;", $reserve, $uur2->prijs, $uur2->beschrijving);
/*
  $query  = "SELECT LOWER(u.dag) AS dag
                 , u.uur
                 , u.plaats
                 , u.type
                 , u.doelgroep
                 , u.id
                 , u.beschrijving
                 , u.aantal_plaatsen
                 , u.aantal_reserve AS aantal_reserve_plaatsen
                 , u.prijs
                 , CASE u.dag WHEN 'maandag'   THEN 1
                              WHEN 'dinsdag'   THEN 2
                              WHEN 'woensdag'  THEN 3
                              WHEN 'donderdag' THEN 4
                              WHEN 'vrijdag'   THEN 5
                              WHEN 'zaterdag'  THEN 6
                              WHEN 'zondag'    THEN 7
                   END AS dagnummer
                 , SUM(CASE reserve WHEN 'N' THEN 1 ELSE 0 END) AS aantal_spelers
                 , SUM(CASE reserve WHEN 'Y' THEN 1 ELSE 0 END) AS aantal_reserven
              FROM bad_speeluren u
              LEFT OUTER JOIN leden_uren lu ON u.id = lu.speeluren_id
             WHERE u.id = 2
             GROUP BY LOWER(u.dag)
                    , u.uur
                    , u.plaats
                    , u.type
                    , u.doelgroep
                    , u.id
                    , u.beschrijving
                    , u.aantal_plaatsen
                    , u.aantal_reserve
                    , u.prijs
                    , CASE u.dag WHEN 'maandag'   THEN 1
                                 WHEN 'dinsdag'   THEN 2
                                 WHEN 'woensdag'  THEN 3
                                 WHEN 'donderdag' THEN 4
                                 WHEN 'vrijdag'   THEN 5
                                 WHEN 'zaterdag'  THEN 6
                                 WHEN 'zondag'    THEN 7
                      END";
  $result = mysql_query($query, $badm_db) or badm_mysql_die();
  $uur2 = mysql_fetch_object($result);
  if ($uur2->aantal_spelers < $uur2->aantal_plaatsen)
  {
    $reserve = 'N';
  }
  elseif ($uur2->aantal_reserven < $uur2->aantal_reserve_plaatsen)
  {
    $reserve = 'Y'; // nog reserveplaatsen
  }
  else
  {
    $reserve = 'V'; // volledig volzet
  }
   mysql_free_result($result);
  $debug .= "<br>reserve = ".$reserve."\n";
  $checked = is_null($uur2->spelers_id) ? "" : " CHECKED";
  write_speeluur(2, $checked, $uur2->dag."&nbsp;".$uur2->uur."&nbsp;", "&nbsp;".$uur2->type."&nbsp;".$uur2->doelgroep."&nbsp;", $reserve, $uur2->prijs, $uur2->beschrijving);
*/
    echo '                    <td><input type="checkbox" id="administratief_lid" name="administratief_lid" onChange="set_administrative_member();"></td>';
    echo '                    <td colspan="2" title="Enkel aansluiting Badminton Vlaanderen" style="cursor:help">Administratief lid (<span id="prijs0">42</span> �)</td>';
?>
                  </tr>
                </table>
        </td>
       </tr>
      </table><br>
      <table>
       <tr>
        <td style="padding-right:100px">
         <table>
          <tr>
           <td>Basisbedrag</td><td align="right"><span id="basisbedrag"><?php echo nvl($_POST["basisbedrag"], 0); ?></span> �</td>
          </tr>
          <tr>
           <td>Speeluren</td><td align="right"><span id="speelurenbedrag"><?php echo nvl($_POST["speelurenbedrag"], 0); ?></span> �</td>
          </tr>
<?php
  //$debug .= "<br>expiration_date = ".$expiration_date."\n";
  //$debug .= "<br>today = ".$today."\n";
  if ($today < $expiration_date && 1==0)
  {
?>
          <tr>
           <td style="padding-right:30px">Vroegboekkorting</td><td align="right">-30 �</td>
          </tr>
<?php
  }
  else
  {
?>
          <tr>
           <td style="padding-right:30px">Korting</td><td align="right">-<script>
document.write(Math.round((100-(discount_pct*100))*100)/100);
</script> %</td>
          </tr>
<?php
  }
?>
          <tr>
           <td>Totaal</td><td align="right" style="font-weight:bold;border-top:1px solid black"><span id="totaalbedrag"><?php echo nvl($_POST["totaalbedrag"], 0); ?></span> �</td>
          </tr>
         </table>
        </td>
        <td valign="top"><span style="color:orange">oranje</span>
        <br><span style="color:red">rood</span>
        </td>
        <td valign="top">: dit speeluur is volzet, maar je kan je nog op de wachtlijst zetten
        <br>: dit speeluur is volzet, inschrijven kan niet meer
        </td>
       </tr>
      </table>
      <br><input type="checkbox" id="initiatie" name="initiatie">Ik heb interesse in <b>initiatiesessies</b> mits extra betaling.
      <br><input type="checkbox" id="werking" name="werking">Ik wil graag meewerken aan activiteiten en werking van onze club.
      <br><input type="checkbox" id="reglement" name="reglement">Ik verklaar me akkoord met het <a href="http://www.badmintonsport.be/huishoudelijk_reglement.php" target="_blank">huishoudelijk reglement</a> van <abbr title="Winksele en Leuven">W&amp;L</abbr> <abbr lang="nl" title="badmintonvereniging">bv</abbr>.
      <div id="control">
          <span style="vertical-align:top">Ik stort op rekeningnr <b>BE 02 3300 7219 5740</b> van <abbr title="Winksele en Leuven">W&amp;L</abbr> <abbr lang="nl" title="badmintonvereniging">bv</abbr> <abbr lang="nl" title="vereniging zonder winstoogmerk">vzw</abbr> met <abbr lang="nl" title="gestructureerde mededeling">ogm</abbr> <b>+++<?php echo $ogm; ?>+++</b></span>&nbsp;
       <table class="xpbutton" cellspacing="0" cellpadding="0">
        <tr>
         <td class="L"><img src="../images/button_xp_left.gif" width="4px" height="24px" alt="" /></td>
         <td class="C"> <a href="javascript:checkSubmitCount();" title="Opslaan">&nbsp; OK &nbsp;</a> </td>
         <td class="R"><img src="../images/button_xp_right.gif" width="4px" height="24px" alt="" /></td>
        </tr>
       </table>
      </div>
      Door op OK te klikken neem je een optie op de gekozen uren. Je inschrijving is pas geldig na betaling.<br><br>
      <input type="hidden" id="bedrag" name="bedrag" value="<?php echo nvl($_POST["bedrag"], 0); ?>">
      <input type="hidden" id="ogm" name="ogm" value="<?php echo $ogm; ?>">
      <input type="hidden" id="email" name="email" value="<?php echo $_REQUEST["email"]; ?>">

      <!-- Division voor de kalender popup-->
      <div id="datediv" style="position:absolute; visibility:hidden; background-color:white; layer-background-color:white;"></div>
<?php
      break;
    case "4": // bedankt
?>
      <br><br>
      Bedankt voor je inschrijving.
      <br><br>
      Vergeet niet om <b><?php echo nvl($_POST["bedrag"], 0); ?> �</b> te storten op rekeningnummer <b>BE 02 3300 7219 5740</b> van <abbr title="Winksele en Leuven">W&amp;L</abbr> <abbr lang="nl" title="badmintonvereniging">bv</abbr> <abbr lang="nl" title="vereniging zonder winstoogmerk">vzw</abbr> met gestructureerde mededeling <b>+++<?php echo $_POST["ogm"]; ?>+++</b>.
      <br>Pas dan is je inschrijving geldig.
      <br><br>
      <abbr title="Winksele en Leuven">W&amp;L</abbr> <abbr lang="nl" title="badmintonvereniging">bv</abbr>
      <br><br>
      <a href="http://www.badmintonsport.be/inschrijven/inschrijving.php?email=<?php echo $_REQUEST["email"]; ?>&again=Y">Nog iemand inschrijven?</a>
      <p>Gelieve niet de 'Back' knop van je browser te gebruiken, want dan wordt je vorige inschrijving overschreven!!</p>
<?php
      break;
    case "5": // error
      mail("freek.ceymeulen@pandora.be", "Possible break-in attemp on page ".$_SERVER['PHP_SELF']." from IP ".$_SERVER['REMOTE_ADDR']." using method ".$_SERVER['REQUEST_METHOD']." on url ".$_SERVER['REQUEST_URI'].": Error.", "From: webmaster@badmintonsport.be");
?>
      <br><br>
      Deze url is niet meer geldig.
      <br><br>
      Vraag een nieuwe link naar het inschrijvingsformulier aan op <a href="http://www.badmintonsport.be/inschrijven/inschrijving.php" target="_self" title="Inschrijvingsformulier">http://www.badmintonsport.be/inschrijvingen/inschrijving.php</a>.
      <br><br>
      <abbr title="Winksele en Leuven">W&amp;L</abbr> <abbr lang="nl" title="badmintonvereniging">bv</abbr>
<?php
      break;
    case "6": // error (te veel aanvragen vanaf hetzelfde IP-adres)
      mail("freek.ceymeulen@pandora.be", "Possible break-in attemp on page ".$_SERVER['PHP_SELF']." from IP ".$_SERVER['REMOTE_ADDR']." using method ".$_SERVER['REQUEST_METHOD']." on url ".$_SERVER['REQUEST_URI'].": Te veel aanvragen vanaf hetzelfde IP-adres.", "From: webmaster@badmintonsport.be");
?>
      <br><br>
      Aanvraag geweigerd.
      <br><br>
      Contacteer de <a href="mailto:webmaster@badmintonsport.be">webmaster@badmintonsport.be</a> voor meer info.
      <br><br>
      <abbr title="Winksele en Leuven">W&amp;L</abbr> <abbr lang="nl" title="badmintonvereniging">bv</abbr>
<?php
      break;
    default:
      mail("freek.ceymeulen@pandora.be", "Possible break-in attemp on page ".$_SERVER['PHP_SELF']." from IP ".$_SERVER['REMOTE_ADDR']." using method ".$_SERVER['REQUEST_METHOD']." on url ".$_SERVER['REQUEST_URI'].": page \"".$page."\" is not defined.", "From: webmaster@badmintonsport.be");
  } // end page switch
?>
     </form>

     </td>
    </tr>
   </table>
     <!-- InstanceEndEditable -->
  </td>
 </tr>
 <tr>
  <td height="0"></td>
  <td></td>
 </tr>
 <tr>
  <td height="1"><img src="http://www.badmintonsport.be/images/spacer.gif" alt="" width="150" height="1"></td>
  <td></td>
 </tr>
</table>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <!--DWLayoutTable-->
 <tr>
  <td width="100%" height="50" valign="top" bgcolor="#FFFFFF">
   <table width="100%" border="0" cellpadding="0" cellspacing="0" style="border-color: #CCCCCC" bgcolor="#CCCCCC">
       <!--DWLayoutTable-->
    <tr>
     <td align="center" valign="middle" style="border-color: #CCCCCC" bgcolor="#006600">
         <img src="http://www.badmintonsport.be/images/spacer.gif" width="200" height="4" alt=""></td>
    </tr>
    <tr>
     <td width="100%" align="center" valign="middle" style="border-color: #CCCCCC" bgcolor="#CCCCCC" class="gmenu">Last change:
 <!-- InstanceBeginEditable name="datum" -->11/11/2014
 <!-- InstanceEndEditable -->
             E-mail: <a href="mailto:webmaster@badmintonsport.be">webmaster@badmintonsport.be</a></td>
    </tr>
    <tr>
     <td align="center" valign="middle" style="border-color: #CCCCCC" bgcolor="#006600" class="gmenu">
          <img src="http://www.badmintonsport.be/images/spacer.gif" width="200" height="4" alt=""></td>
    </tr>
   </table>
  </td>
 </tr>
</table>
</body>
<!-- InstanceEnd --></html>
<?php
  log_website_usage($start_time, $actie, $badm_db2);
  mysql_close($badm_db2);
  //echo $debug;
?>