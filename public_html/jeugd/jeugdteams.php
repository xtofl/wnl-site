<?php
  require_once "../functies/website_usage.php";
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html lang="nl"><!-- InstanceBegin template="/Templates/templatesub1php.dwt.php" codeOutsideHTMLIsLocked="false" -->
<?php
    // Connect to database
    require_once "/home/badmin/public_html/functies/badm_db.inc.php";
    $badm_db = badm_conn_db();
?>
<head>
<META HTTP-EQUIV="Content-Type"      CONTENT="text/html; charset=iso-8859-1">
<META NAME="robots"                  CONTENT="index,follow">
<META NAME="keywords"                CONTENT="badminton,batminton,w&amp;l,badmintonvereniging,club,badmintonclub,sport,Belgium,Veltem,Herent,Leuven,Winksele,Belgi�">
<META NAME="description"             CONTENT="W&amp;L bv : De grootste en leukste badmintonclub van Brabant">
<META NAME="abstract"                CONTENT="badminton, club">
<META NAME="Area"                    CONTENT="General">
<META NAME="Author"                  CONTENT="W&amp;L bv">
<META NAME="Copyright"               CONTENT="(c) 2003-2004 W&amp;L bv">
<META NAME="Creator"                 CONTENT="W&amp;L bv">
<META NAME="Generator"               CONTENT="W&amp;L bv">
<META NAME="Owner"                   CONTENT="W&ampL bv">
<META NAME="Publisher"               CONTENT="W&ampL bv">
<META NAME="Rating"                  CONTENT="General">
<META NAME="revisit-after"           CONTENT="7 Days">
<META NAME="revisit"                 CONTENT="7 Days">
<META NAME="document-classification" CONTENT="Internet">
<META NAME="document-type"           CONTENT="Public">
<META NAME="document-rating"         CONTENT="Safe for Kids">
<META NAME="document-distribution"   CONTENT="Global">
<META HTTP-EQUIV="Content-Type"      CONTENT="text/html; charset=iso-8859-1">
<META HTTP-EQUIV="Audience"          CONTENT="General">
<META HTTP-EQUIV="content-language"  CONTENT="NL">
<META HTTP-EQUIV="Pragma"            CONTENT="no-cache">
<META HTTP-EQUIV="Expires"           CONTENT="now">
<!-- InstanceBeginEditable name="doctitle" -->
<title>W&amp;L badmintonvereniging vzw - Jeugd teams en programma</title>
<script type="text/javascript" language="JavaScript" src="../scripts/sorttable.js"></script>
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
<link href="http://www.badmintonsport.be/css/badminton.css" rel="stylesheet" type="text/css">
<script type="text/javascript" language="JavaScript" src="http://www.badmintonsport.be/scripts/stm31.js"></script>
<script type="text/javascript" language="JavaScript" src="http://www.badmintonsport.be/scripts/dw_rotator.js"></script>
<script type="text/javascript" language="JavaScript">
<!--
function initRotator()
{
  var rotator1 = new dw_Rotator("sponsors_banner", 4000, "http://www.badmintonsport.be/images/", "_blank");
  rotator1.addImages("Veltem_Motors_banner.png", "banner_verhoeven.jpg");
  //rotator1.addImages("Veltem_Motors_banner.png");
  rotator1.addActions("http://www.veltem-motors.be", "http://www.isolatieverhoeven.be");
  //rotator1.addActions("http://www.veltem-motors.be");
  dw_Rotator.start();
}
function doLoad()
{
  // initialize the menu
  st_onload();
  // initialize the banner rotator
  initRotator();
}
//-->
</script>
</head>
<body class="badminton" onload="doLoad();">
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <!--DWLayoutTable-->
  <tr>
  <td colspan="2">
   <table width="100%" border="0" cellpadding="0" cellspacing="0">
    <!--DWLayoutTable-->
    <tr> 
     <td width="100%" style="background-color: #006600;"><img src="http://www.badmintonsport.be/images/pt_transp.gif" width="1px" height="4px" alt=""></td>
    </tr>
    <tr> 
     <td align="center" valign="top" bgcolor="#CCCCCC">
      <table width="100%" border="0" cellspacing="0" cellpadding="3">
       <tr align="center" valign="middle"> 
        <td rowspan="2" align="left">
	     <img src="http://www.badmintonsport.be/images/logow&amp;l-100.gif" width="100" height="80" alt="logo">
	    </td>
        <td>
	     <h4>W&amp;L Badmintonvereniging vzw<br>
	         Mechelsesteenweg 387<br>
             3020 Herent</h4>
		</td>
        <td align="right"><a href="javascript:void();" onclick="return dw_Rotator.doClick(0)"  onmouseover="dw_Rotator.pause(0)" onmouseout="dw_Rotator.resume(0)" onfocus="this.blur()"><img id="sponsors_banner" name="sponsors_banner" src="http://www.badmintonsport.be/images/Veltem_Motors_banner.png" alt="sponsors" width="320" height="100" border="0"></a></td>
       </tr>
      </table>
	 </td>
    </tr>
    <tr> 
     <td bgcolor="#336600"><img src="http://www.badmintonsport.be/images/pt_transp.gif" width="1px" height="4px" alt=""></td>
    </tr>
   </table>
  </td>
 </tr>
 <tr>
  <td width="150" height="213" valign="top">
<script type="text/javascript" language="JavaScript1.2">
<!--
stm_bm(["W&L menu",400,"","http://www.badmintonsport.be/scripts/blank.gif",0,"","",0,0,250,0,1000,1,0,0]);
stm_bp("p0",[1,4,0,0,1,4,0,7,100,"progid:DXImageTransform.Microsoft.Fade(overlap=.5,enabled=0,Duration=0.30)",-2,"progid:DXImageTransform.Microsoft.Pixelate(MaxSquare=15,enabled=0,Duration=0.50)",-2,50,0,0,"#669999","#006633","",3,0,0,"#ffffff"]);
stm_ai("p0i0",[0,"Home","","",-1,-1,0,"http://www.badmintonsport.be/index.php","_self","Startpagina","","","",0,0,0,"","",0,0,0,0,1,"#fffff7",0,"#b5bed6",0,"","",3,3,0,0,"#fffff7","#000000","#000000","#000000","10pt Verdana","10pt Verdana",0,0]);
stm_aix("p0i1","p0i0",[0,"Algemeen","","",-1,-1,0,"","_self","informatie over de club","","","",0,0,0,"http://www.badmintonsport.be/scripts/arrow_r.gif","http://www.badmintonsport.be/scripts/arrow_r.gif",7,7]);
stm_bpx("p1","p0",[1,2,0,0,1,4,0]);
stm_aix("p1i0","p0i0",[0,"Bestuur","","",-1,-1,0,"http://www.badmintonsport.be/algemeen/bestuur.php","_self","Bestuursleden","","","",0,0,0,"","",0,0,0,1]);
stm_bpx("p1","p0",[1,2,0,0,1,4,0,0]);
stm_aix("p2i0","p1i0",[0,"Mededelingen","","",-1,-1,0,"http://www.badmintonsport.be/algemeen/Bestuur/bestuursmededelingen.php","_self","Bestuursmededelingen"]);
stm_ep();
stm_aix("p1i1","p0i0",[0,"Medewerkers","","",-1,-1,0,"http://www.badmintonsport.be/algemeen/medewerkers.php","_self","Medewerkers","","","",0,0,0,"","",0,0,0,1]);
stm_aix("p1i2","p0i0",[0,"Speeluren","","",-1,-1,0,"http://www.badmintonsport.be/algemeen/speeluren.php","_self","Speeluren","","","",0,0,0,"","",0,0,0,1]);
stm_aix("p1i3","p0i1",[0,"Geschiedenis","","",-1,-1,0,"","_self","Geschiedenis","","","",0,0,0,"","",0,0,0,1]);
stm_bpx("p2","p0",[1,2,0,0,1,4,0,0]);
stm_aix("p2i0","p1i0",[0,"Clubkampioenen","","",-1,-1,0,"http://www.badmintonsport.be/algemeen/Geschiedenis/clubkampioenen.php","_self","Winnaars Clubkampioenschap"]);
stm_aix("p2i1","p1i0",[0,"Nationale Kampioenen","","",-1,-1,0,"http://www.badmintonsport.be/algemeen/Geschiedenis/nationalekampioenen.php","_self","Nationale Kampioenen W&L"]);
stm_ep();
stm_aix("p1i4","p0i0",[0,"Sponsors","","",-1,-1,0,"http://www.badmintonsport.be/algemeen/sponsors.php","_self","Sponsors","","","",0,0,0,"","",0,0,0,1]);
stm_aix("p1i5","p0i0",[0,"Artikels","","",-1,-1,0,"http://www.badmintonsport.be/algemeen/artikels.php","_self","Artikels over de club","","","",0,0,0,"","",0,0,0,1]);
stm_ep();
stm_aix("p0i2","p0i0",[0,"Competitie","","",-1,-1,0,"http://www.badmintonsport.be/competitie.php","_self","Competitie: ploegen en uitslagen"]);
stm_aix("p0i3","p0i1",[0,"Tornooien","","",-1,-1,0,"http://www.badmintonsport.be/tornooien/tornooioverzicht.php","_self","Tornooien"]);
stm_bpx("p1","p0",[1,2,0,0,1,4,0]);
stm_aix("p1i0","p0i1",[0,"Inschrijvingen","","",-1,-1,0,"_self","Schrijf je hier in voor tornooien"]);
stm_bpx("p1","p0",[1,2,0,0,1,4,0,0]);
//-->
</script>
<?php
// create SQL statement 
   $sql = "SELECT id
                , organisatie
                , doelgroep
                , DATE_FORMAT(start_dt, '%d/%m') AS start_dt
                , verzonden
           FROM inschr_torn
		   WHERE DATE_ADD(dt_inschr, INTERVAL 21 DAY ) > NOW()
		   ORDER BY dt_inschr DESC";
   $result = mysql_query ($sql, $badm_db) or badm_mysql_die();
// format results by row
   $teller = 0;
   echo "<script type=\"text/javascript\" language=\"JavaScript1.2\">\n";
   while ($badm = mysql_fetch_object($result))
   {
      echo "stm_aix(\"p2i";
      echo $teller;
      echo "\",\"p1i0\",[1,\"";
      echo $badm->organisatie;
      if ($badm->doelgroep == 'J' || $badm->doelgroep == 'V')
      {
        echo $badm->doelgroep == 'J' ? " (Jeugd)" : " (Veteranen)";
        if ($badm->verzonden == 0) //inschrijvingsperiode nog niet afgelopen
        {
          echo " <B>".$badm->start_dt."</B>";
        }
        else
        {
          echo "  <IMG src=http://www.badmintonsport.be/images/closed.png>";
        }
      }
      else
      {
        if ($badm->verzonden == 0) //inschrijvingsperiode nog niet afgelopen
        {
          echo " <B>".$badm->start_dt."</B>";
        }
        else
        {
          echo "  <IMG src=http://www.badmintonsport.be/images/closed.png>";
        }
      }
      echo "\",\"\",\"\",-1,-1,0,\"http://www.badmintonsport.be/tornooien/inschrijvingen.php?id=";
      echo $badm->id;
      echo "\",\"_self\",\"Inschrijving ";
      echo $badm->organisatie;
      echo "\"]);\n";
      $teller = $teller + 1;
   }
   echo"</script>\n";
?>
<script type="text/javascript" language="JavaScript1.2">
<!--
stm_ep();
stm_aix("p1i1","p0i0",[0,"Aanvangsuren","","",-1,-1,0,"http://www.badmintonsport.be/tornooien/aanvangsuren.php","_self","Aanvangsuren tornooien"]);
stm_aix("p1i2","p0i0",[0,"Kalender","","",-1,-1,0,"http://www.badmintonsport.be/docs/tornooikalender.pdf","_blank","Tornooikalender"]);
stm_aix("p1i3","p0i1",[0,"Resultaten","","",-1,-1,0,"","_self","Tornooiresultaten W&L spelers"]);
stm_bpx("p2","p0",[1,2,0,0,1,4,0,0]);
//-->
</script>
<?php
// create SQL statement
    $sql = "SELECT  DISTINCT IF(DATE_FORMAT(dt_van, '%m') < 8
, DATE_FORMAT(dt_van, '%Y') - 1
, DATE_FORMAT(dt_van, '%Y')) AS start
, IF(DATE_FORMAT(dt_van, '%m') < 8
, DATE_FORMAT(dt_van, '%Y')
, DATE_FORMAT(dt_van, '%Y') + 1) AS einde
           FROM bad_tornooien
           ORDER  BY start DESC ";
   $result = mysql_query ($sql, $badm_db) or badm_mysql_die();
// format results by row
   $teller = 0;
   echo "<script type=\"text/javascript\" language=\"JavaScript\">\n";
   while ($badm = mysql_fetch_object($result))
   {
      echo "stm_aix(\"p2i".$teller."\",\"p1i0\",[0,\"Seizoen ".$badm->start."-".$badm->einde."\",\"\",\"\",-1,-1,0,\"http://www.badmintonsport.be/tornooien/resultaten.php?s=".$badm->einde."\",\"_self\",\"Seizoen ".$badm->start."-".$badm->einde."\"]);\n";
      $teller = $teller + 1;
   }
   echo"</script>\n";
   //mysql_free_result($result);
   mysql_close($badm_db);
?>
<script type="text/javascript" language="JavaScript">
<!--stm_ep();
stm_ep();
stm_ep();
stm_aix("p0i4","p0i0",[0,"Spelers","","",-1,-1,0,"http://www.badmintonsport.be/spelers.php","_self","Spelersprofielen en statistieken"]);
stm_aix("p0i5","p0i1",[0,"Jeugd","","",-1,-1,0,"","_self","Informatie over en voor de jeugdspelers"]);
stm_bpx("p1","p0",[1,2,0,0,1,4,0]);
stm_aix("p1i0","p0i0",[0,"Algemeen","","",-1,-1,0,"http://www.badmintonsport.be/jeugd/jeugd.php","_self","Jeugd trainers"]);
stm_aix("p1i1","p0i0",[0,"Teams","","",-1,-1,0,"http://www.badmintonsport.be/jeugd/jeugdteams.php","_self","Jeugdteams"]);
stm_aix("p1i2","p0i0",[0,"J-team","","",-1,-1,0,"http://www.badmintonsport.be/jeugd/J-team.php","_self","J-team"]);
stm_aix("p1i3","p0i0",[0,"Resultaten","","",-1,-1,0,"http://www.badmintonsport.be/jeugd/resultaten_jeugd.php?s=2008","_self","Resultaten jeugtornooien W&L spelers"]);
stm_aix("p1i4","p0i0",[0,"Jeugdtornooi","","",-1,-1,0,"http://www.badmintonsport.be/jeugd/jeugdtornooi.php","_self","Jeugdtornooi W&L"]);
stm_ep();
stm_aix("p0i6","p0i0",[0,"Recreanten","","",-1,-1,0,"http://www.badmintonsport.be/recreant.php","_self","Recreanten"]);
stm_aix("p0i7","p0i1",[0,"Foto's","","",-1,-1,0,"http://users.pandora.be/ikkegg","_blank","badminton foto's"]);
//-->
</script>
<?php
/*************************************************************************************************
 * Create a link to every photo directory that exists
 ************************************************************************************************/
/* Get the root dirctory of the website */
  $rootdir = $_SERVER['DOCUMENT_ROOT'];
/* directory where all the photo's are located in subdirectories */
  $dir = $rootdir."/pictures";
/* Get all the subdirectories of the pictures directory */
  include $rootdir."/functies/getDirectoryListing.php";
  $subdirs = getDirectoryListing($dir, "a", 1, 0, "none");
/* Number of subdirs in directory */
  $subdir_cnt = count($subdirs);
/* Display a link to every subdirectory */
  for ($i = 0; $i < $subdir_cnt; $i++)
  {
    echo "<script type=\"text/javascript\" language=\"JavaScript\">\n";
    if ($i == 0) // first subdirectory
    {
       echo "stm_bpx(\"p1\",\"p0\",[1,2,0,0,1,4,0]);\n";
    }
		echo "stm_aix(\"p0i".$i."\",\"p0i0\",[0,\"".$subdirs[$i]."\",\"\",\"\",-1,-1,0,\"http://www.badmintonsport.be/pictures/showThumbnails.php?dir=".$subdirs[$i]."\",\"_self\",\"".$subdirs[$i]."\"]);\n";
    if ($i == $subdir_cnt - 1) // last subdirectory
    {
      echo "stm_ep();\n";
    }
    echo"</script>\n";
  }
?>
<script type="text/javascript" language="JavaScript">
<!--
stm_ai("p0i8",[6,4,"#006633","",0,0,0]);
stm_aix("p0i9","p0i0",[0,"Links","","",-1,-1,0,"http://www.badmintonsport.be/links.php","_self","Links"]);
stm_aix("p0i10","p0i0",[0,"Message Board","","",-1,-1,0,"http://www.badmintonsport.be/messageboard/messageboard.php?p=1&m=n&id=0","_blank","Message Board"]);
stm_aix("p0i11","p0i0",[0,"Laatste Nieuws","","",-1,-1,0,"http://www.badmintonsport.be/laatste_nieuws.php","_self","De laatste wijzigingen aan de W&L website"]);
stm_aix("p0i12","p0i0",[0,"Team Veltem Motors","","",-1,-1,0,"http://www.everyoneweb.com/teamveltemmotors","_self","De fansite van Team Veltem Motors"]);
stm_ep();
stm_em();
//-->
</script>
      &nbsp;
 <!-- InstanceBeginEditable name="counter" --><!-- InstanceEndEditable -->
       <!-- InstanceBeginEditable name="wiens verjaardag is het" --><!-- InstanceEndEditable -->
  </td>
  <td width="100%" valign="top">
<!-- InstanceBeginEditable name="general" -->
      <h1><a name="top" id="top"></a>W&amp;L jeugdteams, programma en speeluren</h1>
      <table width="90%" border="1">
        <tr align="center">
          <td width="20%"><p><a href="#a">A team</a></p></td>
          <td width="20%"><p><a href="#b">B team</a></p></td>
          <td width="20%"><p><a href="#c">C team</a></p></td>
          <td width="20%"><p><a href="#programma">Programma</a></p></td>
          <td width="20%"><p><a href="#speeluren">Speeluren</a></p></td>
        </tr>
      </table>
      <br />
      <table width="90%" border="0" cellspacing="1" cellpadding="4">
        <tr align="left" valign="top" bgcolor="#999999">
          <td colspan="2" class="geel"><a name="a" id="a"></a>A team</td>
        </tr>
        <tr align="left" valign="top">
          <td width="50%" bgcolor="#F1F1F1"> <p class="kleinetekst">Deze spelers
              zijn allen -15, -17 of -19 en door de club uitverkozen om voor het
              W&amp;L A-team te spelen. </p>
            <p class="kleinetekst">Ze trainen twee extra uren binnen de club in
              het A-team en zitten ook nog in hun gewone team. </p>
            <p class="kleinetekst">Bijna alle spelers zitten in de VVBBC-selectie,
              sommigen in de VBL-selectie.</p></td>
          <td width="50%" bgcolor="#DFDFDF" class="kleinetekst">
        <ins>Barrez Sielke<br></ins>
      <ins>Beckers Valentijn<br></ins>
      <del>Billiau Jonatan<br></del>
            <del>Bonte Wouter<br></del>
            <del>Christiaens Ruben<br></del>
      <ins>De Raeymacker Xavier<br></ins>
      <ins>Dewulf Benoit<br></ins>
            Elst Janne<br>
            Golinski Freek<br>
            <del>Hauwaerts Pieter<br></del>
            Hauwaerts Karolien<br>
            <del>Laporte Fr&eacute;deric<br></del>
            <del>Mommaerts Hendrik<br></del>
            <del>Mommaerts Jonas<br></del>
            <del>Neerinckx Heleen<br></del>
            Nemeth Jens<br>
      <ins>Permentiers Robin</ins><br>
            <del>Simkens Tomas<br></del>
      Slegers Jonas<br>
            <del>Snoeck Jelske<br></del>
      <ins>Tegethoff Jana</ins><br>
            <del>Van Herk Wim<br></del>
      <ins>Van steen Yana</ins><br>
      <ins>Ver Elst Emma</ins><br>
            Walraet Matthias<br> <br /> <br /> </td>
        </tr>
        <tr align="left" valign="top" bgcolor="#999999">
          <td colspan="2" class="geel"><a name="b" id="b"></a>B team</td>
        </tr>
        <tr align="left" valign="top" class="kleinetekst">
          <td width="50%" bgcolor="#F1F1F1"> <p class="kleinetekst">Deze spelers zijn allen -9, -11
              of -13 en door de club uitverkozen om voor het W&amp;L B-team te
              spelen. </p>
            <p class="kleinetekst">Ze trainen twee extra uren binnen de club in het B-team en zitten
              ook nog in hun gewone team.</p>
            <p class="kleinetekst">Bijna alle spelers zitten in de VVBBC-selectie. </p></td>
          <td width="50%" bgcolor="#DFDFDF">
        Adriaens Floris<br>
      <ins>Attendu Xavier</ins><br>
      <ins>Barrez Janne</ins><br>
            <del>Barrez Sielke</del><br>
            <del>Bekkers Valentijn</del><br>
      <ins>De Pauw Chlo�</ins><br>
      <ins>De Maesschalck Dries</ins><br>
            <del>Deraeymaeker Xavier</del><br>
            Derzelle Birgit<br>
      <ins>Desie Ellen</ins><br>
            <del>Dewulf Benoit</del><br>
            <del>Kupers Sofie</del><br>
      <ins>Langendries Julie</ins><br>
            <del>Meys Liesbeth</del><br>
            Mispelter Sien<br>
      <ins>Oleffe Floris</ins><br>
      <ins>Roekens Matthias</ins><br>
      <ins>Rooms Bernd</ins><br>
      <ins>Roose Sofie</ins><br>
            <del>Simkens Simon</del><br>
      <ins>Spanhove Reindert</ins><br>
            <del>Tegethoff Jana</del><br>
      <ins>Temi&ntilde;o Lasuen Mikel</ins><br>
      <ins>Van Rossom Tom</ins><br>
            <del>Van Steen yana</del><br>
            <del>Ver Elst Emma</del><br>
            <del>Ver Elst Matthias</del><br> <br /> <br /> </td>
        </tr>
        <tr align="left" valign="top" bgcolor="#999999">
          <td colspan="2" class="geel"><a name="c" id="c"></a>C team</td>
        </tr>
        <tr align="left" valign="top" class="kleinetekst">
          <td bgcolor="#F1F1F1"><p class="kleinetekst">Deze spelers zijn allen
              -9 en -11 en door de club uitverkozen om voor het W&amp;L C-team
              te spelen. </p>
            <p class="kleinetekst">Ze trainen twee extra uren binnen de club in
              het C-team en zitten ook nog in hun gewone team. </p>
            <p class="kleinetekst">Bijna alle spelers zitten in de VVBBC-selectie.</p> </td>
          <td bgcolor="#DFDFDF">
        <ins>Abeloos Natan</ins><br>
        <ins>Balesse Andreas</ins><br>
        <del>Barrez Janne</del><br>
      <ins>Bogemans Anna</ins><br>
      <ins>Brack� Elias</ins><br>
      <ins>Brumagne Alexander</ins><br>
            De Pauw Lawrence<br>
            <del>Desie Ellen</del><br>
      <ins>De Venter Jeroen</ins><br>
      <ins>De Venter Jonathan</ins><br>
      <ins>De Venter Karlijn</ins><br>
      <ins>De Venter Yannick</ins><br>
            <del>Huegaerts Kevin</del><br>
            Kupers Laurens<br>
            <del>Langendries Julie</del><br>
            <del>Oleffe Floris</del><br>
            <del>Paulus Joren</del><br>
            <del>Piot Niels</del><br>
            <del>Reindert Spanhove</del><br>
      <ins>Ringoet Evert</ins><br>
      <ins>Roose Eva</ins><br>
            <del>Roose Sofie</del><br>
            <del>Temi&ntilde;o Lasuen Mikel</del><br>
            <del>Van Espen Kim</del><br>
            <del>Van Rossom Sam</del><br>
      <ins>Van Hoof Jolien</ins><br>
            <del>Van rossom Tom</del><br>
      <ins>Van Tilt Tim</ins><br>
            <br> <br /> <br /> </td>
        </tr>
        <tr align="left" valign="top">
          <td colspan="2" class="geel"><p><a href="#top"><img src="../images/top.gif" alt="top" width="21" height="11" border="0" /></a></p>
            </td>
        </tr>
        <tr align="left" valign="top" bgcolor="#999999">
          <td colspan="2" class="geel"><a name="programma" id="programma"></a>Programma
<!--            (<a href="../docs/programma_jeugd.pdf" title="Programma Jeugd VVBBC en VBL" target="_blank">pdf</a>)-->
         (<a href="http://www.vvbbc.be/cms/index.php?option=com_content&task=view&id=17&Itemid=35" title="website VVBBC" target="_blank">meer info</a>)
         </td>
        </tr>
        <tr align="left" valign="top">
          <td colspan="2">
            <table class="sortable" width="100%" border="0" cellspacing="1" cellpadding="4" align="center">
              <tr bgcolor="#CCCCCC" class="kleinetitel">
                <th width="25%">Club</th>
                <th width="25%">Gemeente</th>
                <th width="25%">Datum</th>
                <th width="25%">Type</th>
              </tr>
              <tr>
                <td bgcolor="#F1F1F1" class="kleinetekst">Den Dijk</td>
                <td bgcolor="#DFDFDF" class="kleinetekst">&nbsp;</td>
                <td bgcolor="#F1F1F1" class="kleinetekst">20/09/2008</td>
                <td bgcolor="#F1F1F1" class="kleinetekst"><abbr lang="nl" title="Vereniging voor Vlaams-Brabantse Badminton Clubs">VVBBC</abbr></td>
              </tr>
              <tr>
                <td bgcolor="#F1F1F1" class="kleinetekst">Glabbad</td>
                <td bgcolor="#DFDFDF" class="kleinetekst">Glabbeek</td>
                <td bgcolor="#F1F1F1" class="kleinetekst">19/10/2008</td>
                <td bgcolor="#F1F1F1" class="kleinetekst"><abbr lang="nl" title="Vereniging voor Vlaams-Brabantse Badminton Clubs">VVBBC</abbr></td>
              </tr>
              <tr>
                <td bgcolor="#F1F1F1" class="kleinetekst">Tots</td>
                <td bgcolor="#DFDFDF" class="kleinetekst">Keerbergen</td>
                <td bgcolor="#F1F1F1" class="kleinetekst">31/10/2008 </td>
                <td bgcolor="#F1F1F1" class="kleinetekst"><abbr lang="nl" title="Vereniging voor Vlaams-Brabantse Badminton Clubs">VVBBC</abbr></td>
              </tr>
              <tr>
                <td bgcolor="#F1F1F1" class="kleinetekst">Blauwput</td>
                <td bgcolor="#DFDFDF" class="kleinetekst">Kessel-Lo</td>
                <td bgcolor="#F1F1F1" class="kleinetekst">11/11/2008</td>
               <td bgcolor="#F1F1F1" class="kleinetekst"><abbr lang="nl" title="Vereniging voor Vlaams-Brabantse Badminton Clubs">VVBBC</abbr></td>
              </tr>
              <tr>
                <td bgcolor="#F1F1F1" class="kleinetekst">De Treffers </td>
                <td bgcolor="#DFDFDF" class="kleinetekst">Overijse</td>
                <td bgcolor="#F1F1F1" class="kleinetekst">16/11/2008</td>
                <td bgcolor="#F1F1F1" class="kleinetekst"><abbr lang="nl" title="Vereniging voor Vlaams-Brabantse Badminton Clubs">VVBBC</abbr></td>
              </tr>
              <tr>
                <td bgcolor="#F1F1F1" class="kleinetekst"><abbr lang="nl" title="Winksele en Leuven">W&amp;L</abbr> <abbr lang="nl" title="badmintonvereniging">bv</abbr></td>
                <td bgcolor="#DFDFDF" class="kleinetekst">Veltem</td>
                <td bgcolor="#F1F1F1" class="kleinetekst">03/01/2009</td>
                <td bgcolor="#F1F1F1" class="kleinetekst"><abbr lang="nl" title="Vlaamse Badminton Liga">VBL</abbr></td>
              </tr>
              <tr>
                <td bgcolor="#F1F1F1" class="kleinetekst">Herne</td>
                <td bgcolor="#DFDFDF" class="kleinetekst">Herne</td>
                <td bgcolor="#F1F1F1" class="kleinetekst">17/01/2009</td>
        <td bgcolor="#F1F1F1" class="kleinetekst"><abbr lang="nl" title="Vereniging voor Vlaams-Brabantse Badminton Clubs">VVBBC</abbr></td>
              </tr>
              <tr>
                <td bgcolor="#F1F1F1" class="kleinetekst">Shuttles Busters</td>
                <td bgcolor="#DFDFDF" class="kleinetekst">Scherpenheuvel</td>
                <td bgcolor="#F1F1F1" class="kleinetekst">31/01/2009</td>
        <td bgcolor="#F1F1F1" class="kleinetekst"><abbr lang="nl" title="Vereniging voor Vlaams-Brabantse Badminton Clubs">VVBBC</abbr></td>
              </tr>
              <tr>
                <td bgcolor="#F1F1F1" class="kleinetekst">Shuttles Opwijk</td>
                <td bgcolor="#DFDFDF" class="kleinetekst">Opwijk</td>
                <td bgcolor="#F1F1F1" class="kleinetekst">07/02/2009</td>
        <td bgcolor="#F1F1F1" class="kleinetekst"><abbr lang="nl" title="Vereniging voor Vlaams-Brabantse Badminton Clubs">VVBBC</abbr></td>
              </tr>
              <tr>
                <td bgcolor="#F1F1F1" class="kleinetekst">Nero's Hoeilaart</td>
                <td bgcolor="#DFDFDF" class="kleinetekst">Hoeilaart</td>
                <td bgcolor="#F1F1F1" class="kleinetekst">21/02/2009</td>
        <td bgcolor="#F1F1F1" class="kleinetekst"><abbr lang="nl" title="Vereniging voor Vlaams-Brabantse Badminton Clubs">VVBBC</abbr></td>
              </tr>
              <tr>
                <td bgcolor="#F1F1F1" class="kleinetekst">Wolvertem</td>
                <td bgcolor="#DFDFDF" class="kleinetekst">Wolvertem</td>
                <td bgcolor="#F1F1F1" class="kleinetekst">07/03/2009</td>
        <td bgcolor="#F1F1F1" class="kleinetekst"><abbr lang="nl" title="Vereniging voor Vlaams-Brabantse Badminton Clubs">VVBBC</abbr></td>
              </tr>
              <tr>
                <td bgcolor="#F1F1F1" class="kleinetekst">Everbergse</td>
                <td bgcolor="#DFDFDF" class="kleinetekst">Everberg</td>
                <td bgcolor="#F1F1F1" class="kleinetekst">21/03/2009</td>
        <td bgcolor="#F1F1F1" class="kleinetekst"><abbr lang="nl" title="Vereniging voor Vlaams-Brabantse Badminton Clubs">VVBBC</abbr></td>
              </tr>
              <tr>
                <td bgcolor="#F1F1F1" class="kleinetekst"><abbr lang="nl" title="Winksele en Leuven">W&amp;L</abbr> <abbr lang="nl" title="badmintonvereniging">bv</abbr></td>
                <td bgcolor="#DFDFDF" class="kleinetekst">Veltem</td>
                <td bgcolor="#F1F1F1" class="kleinetekst">05/04/2009</td>
        <td bgcolor="#F1F1F1" class="kleinetekst"><abbr lang="nl" title="Vereniging voor Vlaams-Brabantse Badminton Clubs">VVBBC</abbr></td>
              </tr>
            </table></td>
        </tr>
        <tr align="left" valign="top">
          <td colspan="2" class="geel"><a href="#top"><img src="../images/top.gif" alt="top" width="21" height="11" border="0" /></a></td>
        </tr>
        <tr align="left" valign="top">
          <td colspan="2" bgcolor="#999999" class="geel"><a name="speeluren" id="speeluren"></a>Speeluren</td>
        </tr>
        <tr align="left" valign="top">
          <td colspan="2"> <table width="100%" border="0" cellspacing="1" cellpadding="4" align="center">
              <tr bgcolor="#CCCCCC" class="kleinetitel">
                <td valign="top">Team</td>
                <td>Dag</td>
                <td>Uur</td>
              </tr>
              <tr class="kleinetekst">
                <td width="34%" valign="top" bgcolor="#F1F1F1"> Team 1</td>
                <td width="33%" bgcolor="#DFDFDF"> Dinsdag<br />
                  Woensdag<br />
                  Vrijdag </td>
                <td width="33%" bgcolor="#F1F1F1"> 17.00-18.00 uur<br />
                  16.00-17.00 uur<br />
                  17.00-18.00 uur</td>
              </tr>
              <tr class="kleinetekst">
                <td width="34%" valign="top" bgcolor="#F1F1F1"> Team 2 </td>
                <td width="33%" bgcolor="#DFDFDF"> Woensdag<br />
                  Vrijdag </td>
                <td width="33%" bgcolor="#F1F1F1"> 17.00-18.00 uur<br />
                  17.00-18.00 uur </td>
              </tr>
              <tr class="kleinetekst">
                <td width="34%" valign="top" bgcolor="#F1F1F1"> Team 3 </td>
                <td width="33%" bgcolor="#DFDFDF"> Dinsdag<br />
                  Woensdag </td>
                <td width="33%" bgcolor="#F1F1F1"> 18.00-19.00 uur<br />
                  16.00-18.00 uur </td>
              </tr>
              <tr class="kleinetekst">
                <td width="34%" bgcolor="#F1F1F1"> Team 4 </td>
                <td width="33%" bgcolor="#DFDFDF"> Woensdag </td>
                <td width="33%" bgcolor="#F1F1F1"> 14.00-16.00 uur </td>
              </tr>
              <tr class="kleinetekst">
                <td width="34%" bgcolor="#F1F1F1"> Team 5 </td>
                <td width="33%" bgcolor="#DFDFDF"> Woensdag </td>
                <td width="33%" bgcolor="#F1F1F1"> 14.00-16.00 uur </td>
              </tr>
              <tr class="kleinetekst">
                <td width="34%" bgcolor="#F1F1F1"> Team 6 </td>
                <td width="33%" bgcolor="#DFDFDF"> Donderdag</td>
                <td width="33%" bgcolor="#F1F1F1"> 18.00-20.00 uur</td>
              </tr>
              <tr class="kleinetekst">
                <td width="34%" bgcolor="#F1F1F1"> Team 7 </td>
                <td width="33%" bgcolor="#DFDFDF"> Donderdag</td>
                <td width="33%" bgcolor="#F1F1F1"> 18.00-20.00 uur</td>
              </tr>
              <tr class="kleinetekst">
                <td width="34%" bgcolor="#F1F1F1"> A-team</td>
                <td width="33%" bgcolor="#DFDFDF"> Maandag</td>
                <td width="33%" bgcolor="#F1F1F1"> 18.00-20.00 uur</td>
              </tr>
              <tr class="kleinetekst">
                <td width="34%" bgcolor="#F1F1F1">B-team</td>
                <td width="33%" bgcolor="#DFDFDF"> Vrijdag </td>
                <td width="33%" bgcolor="#F1F1F1"> 17.00-19.00 uur</td>
              </tr>
              <tr class="kleinetekst">
                <td bgcolor="#F1F1F1">C-team</td>
                <td bgcolor="#DFDFDF">Maandag<br>
                  Donderdag </td>
                <td bgcolor="#F1F1F1">17.00-18.00 uur</td>
              </tr>
            </table></td>
        </tr>
      </table>
      <a href="#top"><img src="../images/top.gif" alt="top" width="21" height="11" border="0" /></a>
      <p>&nbsp; </p>
      <!-- InstanceEndEditable -->
  </td>
 </tr>
 <tr>
  <td height="0"></td>
  <td></td>
 </tr>
 <tr>
  <td height="1"><img src="http://www.badmintonsport.be/images/spacer.gif" alt="" width="150" height="1"></td>
  <td></td>
 </tr>
</table>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <!--DWLayoutTable-->
 <tr>
  <td width="100%" height="50" valign="top" bgcolor="#FFFFFF">
   <table width="100%" border="0" cellpadding="0" cellspacing="0" style="border-color: #CCCCCC" bgcolor="#CCCCCC">
       <!--DWLayoutTable-->
    <tr>
     <td align="center" valign="middle" style="border-color: #CCCCCC" bgcolor="#006600">
         <img src="http://www.badmintonsport.be/images/spacer.gif" width="200" height="4" alt=""></td>
    </tr>
    <tr>
     <td width="100%" align="center" valign="middle" style="border-color: #CCCCCC" bgcolor="#CCCCCC" class="gmenu">Last change:
 <!-- InstanceBeginEditable name="datum" -->
            <!-- #BeginDate format:It1 -->5-07-2008<!-- #EndDate -->
            <!-- InstanceEndEditable -->
             E-mail: <a href="mailto:webmaster@badmintonsport.be">webmaster@badmintonsport.be</a></td>
    </tr>
    <tr>
     <td align="center" valign="middle" style="border-color: #CCCCCC" bgcolor="#006600" class="gmenu">
          <img src="http://www.badmintonsport.be/images/spacer.gif" width="200" height="4" alt=""></td>
    </tr>
   </table>
  </td>
 </tr>
</table>
</body>
<!-- InstanceEnd --></html>
<?php
  require_once "../functies/badm_db.inc.php";
  $badm_db = badm_conn_db();
  log_website_usage($start_time, null, $badm_db);
  mysql_close($badm_db);
?>