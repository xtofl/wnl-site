<HTML>
<head>
<meta name="keywords" content="badminton,w&l,badmintonvereniging,vereniging,sport,belgium,veltem,herent,leuven,Winksele,belgi�,tornooi,tornooien,resultaten,uitslagen" />
<meta name="description" content="W&L Badmintonvereniging: tornooiresultaten jeugd" />
<title>W&amp;L badmintonvereniging vzw - Tornooiresultaten Jeugd</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link href="http://www.badmintonsport.be/css/badminton.css" rel="stylesheet" type="text/css">
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" class="badminton">
  <table width="99%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td><h1>Tornooiresultaten jeugd seizoen 
<?php
   if (!isset($_GET['s']))
     $seizoen = date("Y");
   else
     $seizoen = $_GET['s'];

   $prev_year = (int)$seizoen - 1;
   echo $prev_year."-".$seizoen;
?>
        </h1></td>
      <td width="100" valign="center"><H3>(Totaal: 
<?php
    require $_SERVER['DOCUMENT_ROOT']."/badm_db.inc.php";
    $badm_db = badm_conn_db();
// create SQL statement om het aantal overwinningen van dit seizoen te berekenen
   $sql = "SELECT CONCAT(DATE_FORMAT(tor.dt_van, '%d/%m/%y'), IF(tor.dt_tot IS NULL, ' ' , ' - '), IFNULL(DATE_FORMAT(tor.dt_tot, '%d/%m/%y'), '')) as datum
                 ,tor.plaats as plaats
                 ,IF (tor.reeks = 'G', CONCAT('GD', tor.klassement), CONCAT(CASE sp.geslacht WHEN 'M' THEN 'H' ELSE 'D' END, tor.reeks, tor.klassement)) as discipline
                 ,CONCAT(sp.naam, IF(tor.partner = '', ' ' , ' & '), IFNULL(tor.partner, '')) as winnaar
           FROM bad_spelers as sp
               ,bad_tornooien as tor
           WHERE tor.spelers_id = sp.id
           AND tor.categorie IS NOT NULL
           AND IF(DATE_FORMAT(tor.dt_van, '%m') < 8, DATE_FORMAT(tor.dt_van, '%Y'), DATE_FORMAT(tor.dt_van, '%Y') + 1) = ".$seizoen
        ." ORDER BY tor.dt_van DESC, tor.klassement, discipline";
   $result = mysql_query ($sql, $badm_db) or badm_mysql_die();
// format results by row
   $dat = "nu";   //startwaarde
   $pla = "hier"; //startwaarde
   $dis = "Q";    //startwaarde
   $aant_overwinningen = 0;
   while ($badm = mysql_fetch_object($result))
   {
      if ($dat != $badm->datum || $pla != $badm->plaats || $dis != $badm->discipline)
      {
         $aant_overwinningen = $aant_overwinningen + 1;
      }
      $dat = $badm->datum;
      $pla = $badm->plaats;
      $dis = $badm->discipline;
   }
   echo $aant_overwinningen;
?>
	  )</H3>
      </td>
    </tr>
  </table>  
  <table width="99%" border="0" cellspacing="0" cellpadding="2">
    <tr bgcolor="#666699" class="geel">
      <td ALIGN="center" width="11%"><p> Datum      </p></td>
      <td ALIGN="center" width="23%"><p> Tornooi    </p></td>
      <td ALIGN="center" width="10%"><p> Categorie  </p></td>
      <td ALIGN="center" width="56%"><p> Winnaar(s) </p></td>
    </tr>
<?php
// create SQL statement 
   $sql = "SELECT CONCAT(DATE_FORMAT(tor.dt_van, '%d/%m/%y'), IF(tor.dt_tot IS NULL, ' ' , ' - '), IFNULL(DATE_FORMAT(tor.dt_tot, '%d/%m/%y'), '')) as datum
                 ,tor.plaats as plaats
                 ,IF (tor.reeks = 'G', CONCAT('GD', ' ', tor.categorie), CONCAT(CASE sp.geslacht WHEN 'M' THEN 'H' ELSE 'D' END, tor.reeks, ' ', tor.categorie)) as discipline
                 ,CONCAT(sp.naam, IF(tor.partner = '', ' ' , ' & '), IFNULL(tor.partner, '')) as winnaar
           FROM bad_spelers as sp
               ,bad_tornooien as tor
           WHERE tor.spelers_id = sp.id
           AND tor.categorie IS NOT NULL
           AND IF(DATE_FORMAT(tor.dt_van, '%m') < 8, DATE_FORMAT(tor.dt_van, '%Y'), DATE_FORMAT(tor.dt_van, '%Y') + 1) = ".$seizoen
        ." ORDER BY tor.dt_van DESC, tor.klassement, discipline";
   $result = mysql_query ($sql, $badm_db) or badm_mysql_die();
   // format results by row
   $dat = "nu";   //startwaarde
   $pla = "hier"; //startwaarde
   $dis = "Q";    //startwaarde
   while ($badm = mysql_fetch_object($result))
   {
      if ($dat != $badm->datum || $pla != $badm->plaats || $dis != $badm->discipline)
      {
         echo "<tr align=\"left\" valign=\"top\">";
         if ($dat != $badm->datum || $pla != $badm->plaats)
         {
            $new = True;
            echo "<td width=\"20%\" bgcolor=\"#CCCCCC\" STYLE=\"border-top: 0.1em solid white\"><p>";
            echo $badm->datum;
         }
         else
         {
            $new = False;
            echo "<td width=\"20%\"><p>&nbsp;";
         }
         echo "</p></td>";
         if ($new)
         {
            echo "<td width=\"30%\" bgcolor=\"#CCCCCC\" STYLE=\"border-top: 0.1em solid white\"><p>";
            echo $badm->plaats;
         }
         else
         {
            echo "<td width=\"30%\"><p>&nbsp";
         }
         echo "</p></td>";
         if ($dat != $badm->datum || $pla != $badm->plaats || $dis != $badm->discipline)
         {
            if ($new)
              echo "<td width=\"8%\" STYLE=\"border-left: 0.1em solid #666699; border-top: 0.1em solid #666699\"><p>";
            else
              echo "<td width=\"8%\" STYLE=\"border-left: 0.1em solid #666699\"><p>";
            echo $badm->discipline;
            if ($new)
              echo "</p></td><td width=\"42%\" STYLE=\"border-top: 0.1em solid #666699; border-right: 0.1em solid #666699\"><p>";
            else
              echo "</p></td><td width=\"42%\" STYLE=\"border-right: 0.1em solid #666699\"><p>";
            echo $badm->winnaar;
            echo "</p></td></tr>";
         }
      }
      $dat = $badm->datum;
      $pla = $badm->plaats;
      $dis = $badm->discipline;
   }
   mysql_close($badm_db);
?>
    <tr bgcolor="#666699" class="geel" height="2">
      <td colspan="2" STYLE="border-top: 0.1em solid white"  >&nbsp;</td>
      <td colspan="2" STYLE="border-top: 0.1em solid #666699">&nbsp;</td>
    </tr>
  </table>
</body>
</html>