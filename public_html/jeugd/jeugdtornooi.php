<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html><!-- InstanceBegin template="/Templates/templatesub1php.dwt.php" codeOutsideHTMLIsLocked="false" -->
<?php
    // Connect to database
    require_once "/home/badmin/public_html/functies/badm_db.inc.php";
    $badm_db = badm_conn_db();
?>
<head>
<META HTTP-EQUIV="Content-Type"      CONTENT="text/html; charset=iso-8859-1">
<META NAME="robots"                  CONTENT="index,follow">
<META NAME="keywords"                CONTENT="badminton,batminton,w&l,badmintonvereniging,club,badmintonclub,sport,Belgium,Veltem,Herent,Leuven,Winksele,Belgi�" />
<META NAME="description"             CONTENT="W&L bv : De grootste en leukste badmintonclub van Brabant" />
<META NAME="abstract"                CONTENT="badminton, club">
<META NAME="Area"                    CONTENT="General">
<META NAME="Author"                  CONTENT="W&L bv">
<META NAME="Copyright"               CONTENT="(c) 2003-2004 W&L bv">
<META NAME="Creator"                 CONTENT="W&L bv">
<META NAME="Generator"               CONTENT="W&L bv">
<META NAME="Owner"                   CONTENT="W&L bv">
<META NAME="Publisher"               CONTENT="W&L bv">
<META NAME="Rating"                  CONTENT="General">
<META NAME="revisit-after"           CONTENT="7 Days">
<META NAME="revisit"                 CONTENT="7 Days">
<META NAME="document-classification" CONTENT="Internet">
<META NAME="document-type"           CONTENT="Public">
<META NAME="document-rating"         CONTENT="Safe for Kids">
<META NAME="document-distribution"   CONTENT="Global">
<META HTTP-EQUIV="Content-Type"      CONTENT="text/html; charset=iso-8859-1" />
<META HTTP-EQUIV="Audience"          CONTENT="General">
<META HTTP-EQUIV="content-language"  CONTENT="NL">
<META HTTP-EQUIV="Pragma"            CONTENT="no-cache">
<META HTTP-EQUIV="Expires"           CONTENT="now">
<!-- InstanceBeginEditable name="doctitle" -->
<title>W&amp;L badmintonvereniging vzw - Jeugdtornooi</title>
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="head" --><!-- InstanceEndEditable -->
<link href="../css/badminton.css" rel="stylesheet" type="text/css">
<script type="text/javascript" language="JavaScript" src="http://www.badmintonsport.be/scripts/stm31.js"></script>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" class="badminton">
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <!--DWLayoutTable-->
 <tr>
  <td height="95" colspan="2" valign="top">
   <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <!--DWLayoutTable-->
    <tr>          
     <td width="100%" height="95">
      <table width="100%" border="0" cellpadding="0" cellspacing="0" bordercolor="#CCCCCC" bgcolor="#FFFFFF">
              <!--DWLayoutTable-->
       <tr> 
        <td width="100%" align="center" valign="middle" bgcolor="#006600"><img src="http://www.badmintonsport.be/images/spacer.gif" width="200" height="4"></td>
       </tr>
       <tr> 
        <td align="center" valign="middle" bgcolor="#CCCCCC"><img src="http://www.badmintonsport.be/images/pt_transp.gif" width="500" height="5"></td>
       </tr>
       <tr> 
        <td align="center" valign="top" bgcolor="#CCCCCC">
         <table width="100%" border="0" cellspacing="0" cellpadding="3">
          <tr align="center" valign="middle"> 
           <td rowspan="2" align="left"><img src="http://www.badmintonsport.be/images/logow&l-100.gif" width="100" height="80"></td>
           <td>&nbsp;</td>
           <td><h4>W&amp;L Badmintonvereniging vzw, Mechelsesteenweg 
                         387, 3020 Herent</h4></td>
          </tr>
          <tr align="center" valign="middle"> 
           <td>&nbsp;</td>
           <td><img src="http://www.badmintonsport.be/images/chinas.gif" width="400" height="60"></td>
          </tr>
         </table>
        </td>
       </tr>
       <tr> 
        <td align="center" valign="middle" bgcolor="#CCCCCC"><img src="http://www.badmintonsport.be/images/pt_transp.gif" width="500" height="5"></td>
       </tr>
       <tr> 
        <td align="center" valign="middle" bgcolor="#336600"><img src="http://www.badmintonsport.be/images/spacer.gif" width="200" height="4"></td>
       </tr>
      </table>
     </td>
    </tr>
   </table>
  </td>
 </tr>
 <tr>
  <td width="150" height="213" valign="top">
<script type="text/javascript" language="JavaScript1.2">
<!--
stm_bm(["W&L menu",400,"","http://www.badmintonsport.be/scripts/blank.gif",0,"","",0,0,250,0,1000,1,0,0]);
stm_bp("p0",[1,4,0,0,1,4,0,7,100,"progid:DXImageTransform.Microsoft.Fade(overlap=.5,enabled=0,Duration=0.30)",-2,"progid:DXImageTransform.Microsoft.Pixelate(MaxSquare=15,enabled=0,Duration=0.50)",-2,50,0,0,"#669999","#006633","",3,0,0,"#ffffff"]);
stm_ai("p0i0",[0,"Home","","",-1,-1,0,"http://www.badmintonsport.be/index.php","_self","Startpagina","","","",0,0,0,"","",0,0,0,0,1,"#fffff7",0,"#b5bed6",0,"","",3,3,0,0,"#fffff7","#000000","#000000","#000000","10pt Verdana","10pt Verdana",0,0]);
stm_aix("p0i1","p0i0",[0,"Algemeen","","",-1,-1,0,"","_self","informatie over de club","","","",0,0,0,"http://www.badmintonsport.be/scripts/arrow_r.gif","http://www.badmintonsport.be/scripts/arrow_r.gif",7,7]);
stm_bpx("p1","p0",[1,2,0,0,1,4,0]);
stm_aix("p1i0","p0i0",[0,"Bestuur","","",-1,-1,0,"http://www.badmintonsport.be/algemeen/bestuur.php","_self","Bestuursleden","","","",0,0,0,"","",0,0,0,1]);
stm_bpx("p1","p0",[1,2,0,0,1,4,0,0]);
stm_aix("p2i0","p1i0",[0,"Mededelingen","","",-1,-1,0,"http://www.badmintonsport.be/algemeen/Bestuur/bestuursmededelingen.php","_self","Bestuursmededelingen"]);
stm_ep();
stm_aix("p1i1","p0i0",[0,"Medewerkers","","",-1,-1,0,"http://www.badmintonsport.be/algemeen/medewerkers.php","_self","Medewerkers","","","",0,0,0,"","",0,0,0,1]);
stm_aix("p1i2","p0i0",[0,"Speeluren","","",-1,-1,0,"http://www.badmintonsport.be/algemeen/speeluren.php","_self","Speeluren","","","",0,0,0,"","",0,0,0,1]);
stm_aix("p1i3","p0i1",[0,"Geschiedenis","","",-1,-1,0,"","_self","Geschiedenis","","","",0,0,0,"","",0,0,0,1]);
stm_bpx("p2","p0",[1,2,0,0,1,4,0,0]);
stm_aix("p2i0","p1i0",[0,"Clubkampioenen","","",-1,-1,0,"http://www.badmintonsport.be/algemeen/Geschiedenis/clubkampioenen.php","_self","Winnaars Clubkampioenschap"]);
stm_aix("p2i1","p1i0",[0,"Nationale Kampioenen","","",-1,-1,0,"http://www.badmintonsport.be/algemeen/Geschiedenis/nationalekampioenen.php","_self","Nationale Kampioenen W&L"]);
stm_ep();
stm_aix("p1i4","p0i0",[0,"Sponsors","","",-1,-1,0,"http://www.badmintonsport.be/algemeen/sponsors.php","_self","Sponsors","","","",0,0,0,"","",0,0,0,1]);
stm_aix("p1i5","p0i0",[0,"Artikels","","",-1,-1,0,"http://www.badmintonsport.be/algemeen/artikels.php","_self","Artikels over de club","","","",0,0,0,"","",0,0,0,1]);
stm_ep();
stm_aix("p0i2","p0i0",[0,"Competitie","","",-1,-1,0,"http://www.badmintonsport.be/competitie.php","_self","Competitie: ploegen en uitslagen"]);
stm_aix("p0i3","p0i1",[0,"Tornooien","","",-1,-1,0,"","Tornooien"]);
stm_bpx("p1","p0",[1,2,0,0,1,4,0]);
stm_aix("p1i0","p0i1",[0,"Inschrijvingen","","",-1,-1,0,"_self","Schrijf je hier in voor tornooien"]);
stm_bpx("p1","p0",[1,2,0,0,1,4,0,0]);
//-->
</script>
<?php
// create SQL statement 
   $sql = "SELECT id
                , organisatie
                , doelgroep
                , DATE_FORMAT(start_dt, '%d/%m') AS start_dt
                , verzonden
           FROM inschr_torn
		   WHERE DATE_ADD(dt_inschr, INTERVAL 21 DAY ) > NOW()
		   ORDER BY dt_inschr DESC";
   $result = mysql_query ($sql, $badm_db) or badm_mysql_die();
// format results by row
   $teller = 0;
   echo "<script type=\"text/javascript\" language=\"JavaScript1.2\">";
   while ($badm = mysql_fetch_object($result))
   {
      echo "stm_aix(\"p2i";
      echo $teller;
      echo "\",\"p1i0\",[1,\"";
      echo $badm->organisatie;
      if ($badm->doelgroep == 'J' || $badm->doelgroep == 'V')
      {
        echo $badm->doelgroep == 'J' ? " (Jeugd)" : " (Veteranen)";
        if ($badm->verzonden == 0) //inschrijvingsperiode nog niet afgelopen
        {
          echo " <B>".$badm->start_dt."</B>";
        }
        else
        {
          echo "  <IMG src=http://www.badmintonsport.be/images/closed.png>";
        }
        echo "\",\"\",\"\",-1,-1,0,\"http://www.badmintonsport.be/tornooien/inschrijvingen_jeugd.php?id=";
      }
      else
      {
        if ($badm->verzonden == 0) //inschrijvingsperiode nog niet afgelopen
        {
          echo " <B>".$badm->start_dt."</B>";
        }
        else
        {
          echo "  <IMG src=http://www.badmintonsport.be/images/closed.png>";
        }
        echo "\",\"\",\"\",-1,-1,0,\"http://www.badmintonsport.be/tornooien/inschrijvingen.php?id=";
      }
      echo $badm->id;
      echo "\",\"_self\",\"Inschrijving ";
      echo $badm->organisatie;
      echo "\"]);";
      $teller = $teller + 1;
   }
   echo"</script>";
?>
<script type="text/javascript" language="JavaScript1.2">
<!--
stm_ep();
stm_aix("p1i1","p0i0",[0,"Aanvangsuren","","",-1,-1,0,"http://www.badmintonsport.be/tornooien/aanvangsuren.php","_self","Aanvangsuren tornooien"]);
stm_aix("p1i2","p0i0",[0,"Kalender","","",-1,-1,0,"http://www.badmintonsport.be/docs/tornooikalender20052006.pdf","_blank","Tornooikalender seizoen 2005 - 2006"]);
stm_aix("p1i3","p0i1",[0,"Resultaten","","",-1,-1,0,"","_self","Tornooiresultaten W&L spelers"]);
stm_bpx("p2","p0",[1,2,0,0,1,4,0,0]);
//--></script>
<?php
// create SQL statement
    $sql = "SELECT  DISTINCT IF(DATE_FORMAT(dt_van, '%m') < 8
, DATE_FORMAT(dt_van, '%Y') - 1
, DATE_FORMAT(dt_van, '%Y')) AS start
, IF(DATE_FORMAT(dt_van, '%m') < 8
, DATE_FORMAT(dt_van, '%Y')
, DATE_FORMAT(dt_van, '%Y') + 1) AS einde
           FROM bad_tornooien
           ORDER  BY start DESC ";
   $result = mysql_query ($sql, $badm_db) or badm_mysql_die();
// format results by row
   $teller = 0;
   echo"<script type=\"text/javascript\" language=\"JavaScript\">";
   while ($badm = mysql_fetch_object($result))
   {
      echo "stm_aix(\"p2i".$teller."\",\"p1i0\",[0,\"Seizoen ".$badm->start."-".$badm->einde."\",\"\",\"\",-1,-1,0,\"http://www.badmintonsport.be/tornooien/resultaten.php?s=".$badm->einde."\",\"_self\",\"Seizoen ".$badm->start."-".$badm->einde."\"]);";
      $teller = $teller + 1;
   }
   echo"</script>";
   //mysql_free_result($result);
   mysql_close($badm_db);
?>
<script type="text/javascript" language="JavaScript">
<!--stm_ep();
stm_ep();
stm_ep();
stm_aix("p0i4","p0i0",[0,"Spelers","","",-1,-1,0,"http://www.badmintonsport.be/spelers.php","_self","Spelersprofielen en statistieken"]);
stm_aix("p0i5","p0i1",[0,"Jeugd","","",-1,-1,0,"","_self","Informatie over en voor de jeugdspelers"]);
stm_bpx("p1","p0",[1,2,0,0,1,4,0]);
stm_aix("p1i0","p0i0",[0,"Algemeen","","",-1,-1,0,"http://www.badmintonsport.be/jeugd/jeugd.php","_self","Jeugd trainers"]);
stm_aix("p1i1","p0i0",[0,"Teams","","",-1,-1,0,"http://www.badmintonsport.be/jeugd/jeugdteams.php","_self","Jeugdteams"]);
stm_aix("p1i2","p0i0",[0,"J-team","","",-1,-1,0,"http://www.badmintonsport.be/jeugd/J-team.php","_self","J-team"]);
stm_aix("p1i3","p0i0",[0,"Resultaten","","",-1,-1,0,"http://www.badmintonsport.be/jeugd/resultaten_jeugd.php?s=2006","_self","Resultaten jeugtornooien W&L spelers"]);
stm_aix("p1i4","p0i0",[0,"Jeugdtornooi","","",-1,-1,0,"http://www.badmintonsport.be/jeugd/jeugdtornooi.php","_self","Jeugdtornooi W&L"]);
stm_ep();
stm_aix("p0i6","p0i0",[0,"Recreanten","","",-1,-1,0,"http://www.badmintonsport.be/recreant.php","_self","Recreanten"]);
stm_aix("p0i7","p0i1",[0,"Foto's","","",-1,-1,0,"http://users.pandora.be/ikkegg","_blank","badminton foto's"]);
//-->
</script>
<?php
/*************************************************************************************************
 * Create a link to every photo directory that exists
 ************************************************************************************************/
/* Get the root dirctory of the website */
  $rootdir = $_SERVER['DOCUMENT_ROOT'];
/* directory where all the photo's are located in subdirectories */
  $dir = $rootdir."/pictures";
/* Get all the subdirectories of the pictures directory */
  include $rootdir."/functies/getDirectoryListing.php";
  $subdirs = getDirectoryListing($dir, "a", 1, 0, "none");
/* Number of subdirs in directory */
  $subdir_cnt = count($subdirs);
/* Display a link to every subdirectory */
  for ($i = 0; $i < $subdir_cnt; $i++)
  {
    echo"<script type=\"text/javascript\" language=\"JavaScript\">";
    if ($i == 0) // first subdirectory
    {
       echo "stm_bpx(\"p1\",\"p0\",[1,2,0,0,1,4,0]);\n";
    }
		echo "stm_aix(\"p0i".$i."\",\"p0i0\",[0,\"".$subdirs[$i]."\",\"\",\"\",-1,-1,0,\"http://www.badmintonsport.be/pictures/showThumbnails.php?dir=".$subdirs[$i]."\",\"_self\",\"".$subdirs[$i]."\"]);\n";
    if ($i == $subdir_cnt - 1) // last subdirectory
    {
      echo "stm_ep();\n";
    }
    echo"</script>";
  }
?>
<script type="text/javascript" language="JavaScript">
<!--
stm_ai("p0i8",[6,4,"#006633","",0,0,0]);
stm_aix("p0i9","p0i0",[0,"Links","","",-1,-1,0,"http://www.badmintonsport.be/links.php","_self","Links"]);
stm_aix("p0i10","p0i0",[0,"Message board","","",-1,-1,0,"http://www.badmintonsport.be/messageboard/messageboard.php?p=1&m=n&id=0","_blank","Message Board"]);
stm_ep();
stm_em();
//--></script>
      &nbsp;
 <!-- InstanceBeginEditable name="counter" --><!-- InstanceEndEditable -->
       <!-- InstanceBeginEditable name="wiens verjaardag is het" --><!-- InstanceEndEditable -->
  </td>
    <td width="100%" valign="top"> <!-- InstanceBeginEditable name="general" --> 
      <h1><a name="top" id="top"></a>12e jeugdtornooi W&amp;L - zaterdag 8 januari 2011</h1>
      <h3 align="left"><strong>-9 jaar  (minibad &ndash; alleen enkels), -11 jaar (J1), -13 jaar (J2), -15 jaar (J3), -17  jaar (J4)</strong><br>
          <strong>Klassementen D  en C</strong></h3>
      <table width="90%" border="0" cellspacing="1" cellpadding="4">
        <tr align="left" valign="top" bgcolor="#666699" class="kleinetitel"> 
          <td colspan="2" class="geel">Algemene informatie <a href="../docs/2011-jeugdtornooi.pdf" target="_blank">pdf 
            document</a></td>
        </tr>
        <tr align="left" valign="top"> 
          <td width="30%" bgcolor="#F1F1F1" class="kleinetitel"> Plaats</td>
          <td width="70%" bgcolor="#DFDFDF" class="kleinetekst"> Sporthal Ivo 
            Van Damme<br />
            Overstraat &#8211; 3020 Veltem, tel: 016/48.88.05<br />
            Zie ook: <a href="../plan1.htm" target="_blank">plan en wegbeschrijving</a></td>
        </tr>
        <tr align="left" valign="top"> 
          <td bgcolor="#F1F1F1" class="kleinetitel">Informatie</td>
          <td bgcolor="#DFDFDF" class="kleinetekst"> Peter Simkens<br />
            Rulenslaan 21, 3020 Winksele<br />
            Tel. : 016/20.82.81<br /> <a href="mailto:jeugdtornooi@badmintonsport.be">E-mail</a></td>
        </tr>
        <tr align="left" valign="top"> 
          <td bgcolor="#F1F1F1" class="kleinetitel"><p>Inschrijvingen via <a href="http://www.toernooi.nl/sport/tournament.aspx?id=77E9C981-2CDE-4067-9A02-346562541816">www.toernooi.nl</a></p>
          <p class="kleinetekst"><a href="WenL 2011 - Inschrijvingsformulier.xlsx"><strong>[formulier]</strong></a></p></td>
          <td bgcolor="#DFDFDF" class="kleinetekst"><p class="kleinetekst">in te sturen <strong>v&oacute;&oacute;r 
            11 december </strong> aan:<br>
            Peter Simkens<br>
            Rulenslaan 21, 3020 Winksele<br>
            Tel. : 016/20.82.81<br> 
            <a href="mailto:jeugdtornooi@badmintonsport.be">E-mail</a></p>
            <ul>
              <li class="kleinetekst">Bij te weinig inschrijvingen kunnen reeksen worden  samengevoegd. De reeksen in de categorie &ndash;9 jaar zullen alleen georganiseerd  worden bij voldoende deelnemers. Indien er niet genoeg deelnemers zijn in &ndash;9,  zal de tornooi-organisatie de spelers nog v&oacute;&oacute;r de loting de keuze bieden om mee te spelen in &ndash;11 of om de  inschrijving terug in te trekken.</li>
              <li class="kleinetekst">De &ndash;9-jarigen zullen Minibad spelen. Zij spelen alleen maar  enkel. Spelers uit &ndash;9 met een racket volgens de normale maten, kunnen zich  eventueel inschrijven bij &ndash;11.</li>
              <li class="kleinetekst">De inschrijvingen kunnen vervroegd gesloten worden, wanneer  er meer dan 150 inschrijvingen zijn. De inschrijvingen zullen aanvaard worden  op basis van de datum van inschrijving. De inschrijvingen die na een vervroegde  afsluiting binnenkomen zullen in de reserve-lijst komen.</li>
              <li class="kleinetekst">Bij een groot aantal inschrijvingen kan de tornooi-organisatie  beslissen om de reeksen van het dubbelspel met rechtstreekse uitschakeling te  spelen. In het enkelspel zullen de reeksen in ieder geval beginnen met poules.</li>
              <li class="kleinetekst">Als datum van inschrijving geldt de postdatum of de datum  waarop de e-mail voor inschrijving is verstuurd of waarop de inschrijving op de  website <a href="http://www.toernooi.nl/sport/tournament.aspx?id=77E9C981-2CDE-4067-9A02-346562541816">www.toernooi.nl</a> is gebeurd.  Inschrijvingen per e-mail zijn slechts geldig na bevestiging. Telefonische  inschrijvingen worden niet aanvaard.</li>
              <li class="kleinetekst">Het jeugdtornooi staat ook open voor internationale  deelnemers.</li>
            </ul></td>
        </tr>
        <tr align="left" valign="top"> 
          <td bgcolor="#F1F1F1" class="kleinetitel">Inschrijvingsrecht</td>
          <td bgcolor="#DFDFDF" class="kleinetekst">4 &euro; per discipline te 
            betalen bij aankomst in de zaal<br>
            Elke inschrijving verplicht tot betaling. (Behoudens art. 112, C100)          </td>
        </tr>
        <tr align="left" valign="top"> 
          <td bgcolor="#F1F1F1" class="kleinetitel">Hoofdscheidsrechter</td>
          <td bgcolor="#DFDFDF" class="kleinetekst">Dhr. Sven Huypens</td>
        </tr>
        <tr align="left" valign="top"> 
          <td bgcolor="#F1F1F1" class="kleinetitel">Dopingcontrole</td>
          <td bgcolor="#DFDFDF" class="kleinetekst">Dhr. Guido Claes</td>
        </tr>
        <tr align="left" valign="top"> 
          <td bgcolor="#F1F1F1" class="kleinetitel">Shuttles</td>
          <td bgcolor="#DFDFDF" class="kleinetekst">Yonex MAVIS 300 wit of geel, 
            verkrijgbaar in de zaal</td>
        </tr>
        <tr align="left" valign="top"> 
          <td bgcolor="#F1F1F1" class="kleinetitel">Loting</td>
          <td bgcolor="#DFDFDF" class="kleinetekst">21 december  
            om 20u<br>
            Ten huize van Peter Simkens<br>
            Rulenslaan 21, 3020 Winksele</td>
        </tr>
        <tr align="left" valign="top"> 
          <td bgcolor="#F1F1F1" class="kleinetitel">Reglement</td>
          <td bgcolor="#DFDFDF" class="kleinetekst"><ul>
            <li class="kleinetekst">Het tornooi wordt gespeeld volgens de reglementen van de BBF  en volgens de VBL-reglementen C200 en C250. Voor de afwezige spelers is het  reglement G211 van toepassing.</li>
            <li class="kleinetekst">De kledij moet conform zijn aan art. 117 en 118 van regl.  C100 </li>
            <li class="kleinetekst">Elke deelnemer dient 30 minuten v&oacute;&oacute;r zijn wedstrijd  speelklaar in de zaal aanwezig te zijn.</li>
            <li class="kleinetekst">Elke deelnemer dient bij aanmelding zijn/haar lidkaart  ongevraagd te tonen.</li>
            <li class="kleinetekst">Het tornooi zal gespeeld worden van 9 uur &rsquo;s morgens tot 20  uur &rsquo;s avonds. Gelieve dit reeds bij de voor-inschrijvingen in de club aan de  spelers mee te delen, om alle misverstanden op de tornooidag zelf te mijden.</li>
            <li class="kleinetekst">Elke deelnemer kan gevraagd worden een wedstrijd te tellen.</li>
            <li class="kleinetekst">Coaches dienen de richtlijnen van de referee te volgen.</li>
            <li class="kleinetekst">Men mag de zaal slechts verlaten na toestemming van de  referee.</li>
            <li class="kleinetekst">Spelers die medicatie gebruiken dienen dit voor de aanvang  van hun eerste wedstrijd mee te delen aan de referee en dit d.m.v. een medisch  attest onder gesloten omslag.</li>
            <li class="kleinetekst">De tornooiorganisatie heeft het recht alle maatregelen te  treffen die zij nodig acht voor een goed verloop van het tornooi.</li>
            <li class="kleinetekst">Tijdens een officieel tornooi, kampioenschap, competitie of  evenement, goedgekeurd door BBF of VBL, is het toegestaan om tv-opnames,  geluidsopnames en foto&rsquo;s te maken. Deelname aan het tornooi betekent dat  hiervoor geen bijkomende goedkeuring aan de betrokken spelers, clubs of  officials gevraagd moet worden. Deze tv-opnames, geluidsopnames of foto&rsquo;s  kunnen verspreid worden door de houder van de uitzend-of publicatierechten en  mogen gebruikt worden voor live of opgenomen tv-programma&rsquo;s, internet,  webcasting, radio, film, verspreiding via de pers en andere gelijkaardige  media. BBF en zijn geaffilieerde federaties en elke betrokken fotograaf hebben  het recht elke foto of andere gelijkaardige beelden, die van de speler of  official werden gemaakt tijdens het tornooi, te gebruiken voor niet-commerci&euml;le  doeleinden. </li>
          </ul></td>
        </tr>
      </table>
      <p><a href="#top"><img src="../images/top.gif" alt="top" width="21" height="11" border="0" /></a></p>
      <!-- InstanceEndEditable --> </td>
 </tr>
 <tr>
  <td height="0"></td>
  <td></td>
 </tr>
 <tr>
  <td height="1"><img src="http://www.badmintonsport.be/images/spacer.gif" alt="" width="150" height="1"></td>
  <td></td>
 </tr>
</table>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <!--DWLayoutTable-->
 <tr>
  <td width="100%" height="50" valign="top" bgcolor="#FFFFFF">
   <table width="100%" border="0" cellpadding="0" cellspacing="0" bordercolor="#CCCCCC" bgcolor="#CCCCCC">
       <!--DWLayoutTable-->
    <tr>
     <td align="center" valign="middle" bordercolor="#CCCCCC" bgcolor="#006600">
         <img src="http://www.badmintonsport.be/images/spacer.gif" width="200" height="4"></td>
    </tr>
    <tr>
     <td width="100%" align="center" valign="middle" bordercolor="#CCCCCC" bgcolor="#CCCCCC" class="gmenu">Last change:
 <!-- InstanceBeginEditable name="datum" -->
            <!-- #BeginDate format:It1 -->16-11-2010<!-- #EndDate -->
            <!-- InstanceEndEditable -->
             E-mail: <a href="mailto:webmaster@badmintonsport.be">webmaster@badmintonsport.be</a></td>
    </tr>
    <tr>
     <td align="center" valign="middle" bordercolor="#CCCCCC" bgcolor="#006600" class="gmenu">
          <img src="http://www.badmintonsport.be/images/spacer.gif" width="200" height="4"></td>
    </tr>
   </table>
  </td>
 </tr>
</table>
</body>
<!-- InstanceEnd --></html>