<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html><!-- InstanceBegin template="/Templates/templatesub1php.dwt.php" codeOutsideHTMLIsLocked="false" -->
<?php
    // Connect to database
    require_once "/home/badmin/public_html/functies/badm_db.inc.php";
    $badm_db = badm_conn_db();
?>
<head>
<META HTTP-EQUIV="Content-Type"      CONTENT="text/html; charset=iso-8859-1">
<META NAME="robots"                  CONTENT="index,follow">
<META NAME="keywords"                CONTENT="badminton,batminton,w&l,badmintonvereniging,club,badmintonclub,sport,Belgium,Veltem,Herent,Leuven,Winksele,Belgi�" />
<META NAME="description"             CONTENT="W&L bv : De grootste en leukste badmintonclub van Brabant" />
<META NAME="abstract"                CONTENT="badminton, club">
<META NAME="Area"                    CONTENT="General">
<META NAME="Author"                  CONTENT="W&L bv">
<META NAME="Copyright"               CONTENT="(c) 2003-2004 W&L bv">
<META NAME="Creator"                 CONTENT="W&L bv">
<META NAME="Generator"               CONTENT="W&L bv">
<META NAME="Owner"                   CONTENT="W&L bv">
<META NAME="Publisher"               CONTENT="W&L bv">
<META NAME="Rating"                  CONTENT="General">
<META NAME="revisit-after"           CONTENT="7 Days">
<META NAME="revisit"                 CONTENT="7 Days">
<META NAME="document-classification" CONTENT="Internet">
<META NAME="document-type"           CONTENT="Public">
<META NAME="document-rating"         CONTENT="Safe for Kids">
<META NAME="document-distribution"   CONTENT="Global">
<META HTTP-EQUIV="Content-Type"      CONTENT="text/html; charset=iso-8859-1" />
<META HTTP-EQUIV="Audience"          CONTENT="General">
<META HTTP-EQUIV="content-language"  CONTENT="NL">
<META HTTP-EQUIV="Pragma"            CONTENT="no-cache">
<META HTTP-EQUIV="Expires"           CONTENT="now">
<!-- InstanceBeginEditable name="doctitle" -->
<title>Aanvangsuren W&amp;L badmintonvereniging vzw - Jeugdtornooi</title>
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
<link href="../css/badminton.css" rel="stylesheet" type="text/css">
<script type="text/javascript" language="JavaScript" src="http://www.badmintonsport.be/scripts/stm31.js"></script>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" class="badminton">
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <!--DWLayoutTable-->
 <tr>
  <td height="95" colspan="2" valign="top">
   <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <!--DWLayoutTable-->
    <tr>          
     <td width="100%" height="95">
      <table width="100%" border="0" cellpadding="0" cellspacing="0" bordercolor="#CCCCCC" bgcolor="#FFFFFF">
              <!--DWLayoutTable-->
       <tr> 
        <td width="100%" align="center" valign="middle" bgcolor="#006600"><img src="http://www.badmintonsport.be/images/spacer.gif" width="200" height="4"></td>
       </tr>
       <tr> 
        <td align="center" valign="middle" bgcolor="#CCCCCC"><img src="http://www.badmintonsport.be/images/pt_transp.gif" width="500" height="5"></td>
       </tr>
       <tr> 
        <td align="center" valign="top" bgcolor="#CCCCCC">
         <table width="100%" border="0" cellspacing="0" cellpadding="3">
          <tr align="center" valign="middle"> 
           <td rowspan="2" align="left"><img src="http://www.badmintonsport.be/images/logow&l-100.gif" width="100" height="80"></td>
           <td>&nbsp;</td>
           <td><h4>W&amp;L Badmintonvereniging vzw, Mechelsesteenweg 
                         387, 3020 Herent</h4></td>
          </tr>
          <tr align="center" valign="middle"> 
           <td>&nbsp;</td>
           <td><img src="http://www.badmintonsport.be/images/chinas.gif" width="400" height="60"></td>
          </tr>
         </table>
        </td>
       </tr>
       <tr> 
        <td align="center" valign="middle" bgcolor="#CCCCCC"><img src="http://www.badmintonsport.be/images/pt_transp.gif" width="500" height="5"></td>
       </tr>
       <tr> 
        <td align="center" valign="middle" bgcolor="#336600"><img src="http://www.badmintonsport.be/images/spacer.gif" width="200" height="4"></td>
       </tr>
      </table>
     </td>
    </tr>
   </table>
  </td>
 </tr>
 <tr>
  <td width="150" height="213" valign="top">
<script type="text/javascript" language="JavaScript1.2">
<!--
stm_bm(["W&L menu",400,"","http://www.badmintonsport.be/scripts/blank.gif",0,"","",0,0,250,0,1000,1,0,0]);
stm_bp("p0",[1,4,0,0,1,4,0,7,100,"progid:DXImageTransform.Microsoft.Fade(overlap=.5,enabled=0,Duration=0.30)",-2,"progid:DXImageTransform.Microsoft.Pixelate(MaxSquare=15,enabled=0,Duration=0.50)",-2,50,0,0,"#669999","#006633","",3,0,0,"#ffffff"]);
stm_ai("p0i0",[0,"Home","","",-1,-1,0,"http://www.badmintonsport.be/index.php","_self","Startpagina","","","",0,0,0,"","",0,0,0,0,1,"#fffff7",0,"#b5bed6",0,"","",3,3,0,0,"#fffff7","#000000","#000000","#000000","10pt Verdana","10pt Verdana",0,0]);
stm_aix("p0i1","p0i0",[0,"Algemeen","","",-1,-1,0,"","_self","informatie over de club","","","",0,0,0,"http://www.badmintonsport.be/scripts/arrow_r.gif","http://www.badmintonsport.be/scripts/arrow_r.gif",7,7]);
stm_bpx("p1","p0",[1,2,0,0,1,4,0]);
stm_aix("p1i0","p0i0",[0,"Bestuur","","",-1,-1,0,"http://www.badmintonsport.be/algemeen/bestuur.php","_self","Bestuursleden","","","",0,0,0,"","",0,0,0,1]);
stm_bpx("p1","p0",[1,2,0,0,1,4,0,0]);
stm_aix("p2i0","p1i0",[0,"Mededelingen","","",-1,-1,0,"http://www.badmintonsport.be/algemeen/Bestuur/bestuursmededelingen.php","_self","Bestuursmededelingen"]);
stm_ep();
stm_aix("p1i1","p0i0",[0,"Medewerkers","","",-1,-1,0,"http://www.badmintonsport.be/algemeen/medewerkers.php","_self","Medewerkers","","","",0,0,0,"","",0,0,0,1]);
stm_aix("p1i2","p0i0",[0,"Speeluren","","",-1,-1,0,"http://www.badmintonsport.be/algemeen/speeluren.php","_self","Speeluren","","","",0,0,0,"","",0,0,0,1]);
stm_aix("p1i3","p0i1",[0,"Geschiedenis","","",-1,-1,0,"","_self","Geschiedenis","","","",0,0,0,"","",0,0,0,1]);
stm_bpx("p2","p0",[1,2,0,0,1,4,0,0]);
stm_aix("p2i0","p1i0",[0,"Clubkampioenen","","",-1,-1,0,"http://www.badmintonsport.be/algemeen/Geschiedenis/clubkampioenen.php","_self","Winnaars Clubkampioenschap"]);
stm_aix("p2i1","p1i0",[0,"Nationale Kampioenen","","",-1,-1,0,"http://www.badmintonsport.be/algemeen/Geschiedenis/nationalekampioenen.php","_self","Nationale Kampioenen W&L"]);
stm_ep();
stm_aix("p1i4","p0i0",[0,"Sponsors","","",-1,-1,0,"http://www.badmintonsport.be/algemeen/sponsors.php","_self","Sponsors","","","",0,0,0,"","",0,0,0,1]);
stm_aix("p1i5","p0i0",[0,"Artikels","","",-1,-1,0,"http://www.badmintonsport.be/algemeen/artikels.php","_self","Artikels over de club","","","",0,0,0,"","",0,0,0,1]);
stm_ep();
stm_aix("p0i2","p0i0",[0,"Competitie","","",-1,-1,0,"http://www.badmintonsport.be/competitie.php","_self","Competitie: ploegen en uitslagen"]);
stm_aix("p0i3","p0i1",[0,"Tornooien","","",-1,-1,0,"","Tornooien"]);
stm_bpx("p1","p0",[1,2,0,0,1,4,0]);
stm_aix("p1i0","p0i1",[0,"Inschrijvingen","","",-1,-1,0,"_self","Schrijf je hier in voor tornooien"]);
stm_bpx("p1","p0",[1,2,0,0,1,4,0,0]);
//-->
</script>
<?php
// create SQL statement 
   $sql = "SELECT id
                , organisatie
                , doelgroep
                , DATE_FORMAT(start_dt, '%d/%m') AS start_dt
                , verzonden
           FROM inschr_torn
		   WHERE DATE_ADD(dt_inschr, INTERVAL 21 DAY ) > NOW()
		   ORDER BY dt_inschr DESC";
   $result = mysql_query ($sql, $badm_db) or badm_mysql_die();
// format results by row
   $teller = 0;
   echo "<script type=\"text/javascript\" language=\"JavaScript1.2\">";
   while ($badm = mysql_fetch_object($result))
   {
      echo "stm_aix(\"p2i";
      echo $teller;
      echo "\",\"p1i0\",[1,\"";
      echo $badm->organisatie;
      if ($badm->doelgroep == 'J' || $badm->doelgroep == 'V')
      {
        echo $badm->doelgroep == 'J' ? " (Jeugd)" : " (Veteranen)";
        if ($badm->verzonden == 0) //inschrijvingsperiode nog niet afgelopen
        {
          echo " <B>".$badm->start_dt."</B>";
        }
        else
        {
          echo "  <IMG src=http://www.badmintonsport.be/images/closed.png>";
        }
        echo "\",\"\",\"\",-1,-1,0,\"http://www.badmintonsport.be/tornooien/inschrijvingen_jeugd.php?id=";
      }
      else
      {
        if ($badm->verzonden == 0) //inschrijvingsperiode nog niet afgelopen
        {
          echo " <B>".$badm->start_dt."</B>";
        }
        else
        {
          echo "  <IMG src=http://www.badmintonsport.be/images/closed.png>";
        }
        echo "\",\"\",\"\",-1,-1,0,\"http://www.badmintonsport.be/tornooien/inschrijvingen.php?id=";
      }
      echo $badm->id;
      echo "\",\"_self\",\"Inschrijving ";
      echo $badm->organisatie;
      echo "\"]);";
      $teller = $teller + 1;
   }
   echo"</script>";
?>
<script type="text/javascript" language="JavaScript1.2">
<!--
stm_ep();
stm_aix("p1i1","p0i0",[0,"Aanvangsuren","","",-1,-1,0,"http://www.badmintonsport.be/tornooien/aanvangsuren.php","_self","Aanvangsuren tornooien"]);
stm_aix("p1i2","p0i0",[0,"Kalender","","",-1,-1,0,"http://www.badmintonsport.be/docs/tornooikalender20052006.pdf","_blank","Tornooikalender seizoen 2005 - 2006"]);
stm_aix("p1i3","p0i1",[0,"Resultaten","","",-1,-1,0,"","_self","Tornooiresultaten W&L spelers"]);
stm_bpx("p2","p0",[1,2,0,0,1,4,0,0]);
//--></script>
<?php
// create SQL statement
    $sql = "SELECT  DISTINCT IF(DATE_FORMAT(dt_van, '%m') < 8
, DATE_FORMAT(dt_van, '%Y') - 1
, DATE_FORMAT(dt_van, '%Y')) AS start
, IF(DATE_FORMAT(dt_van, '%m') < 8
, DATE_FORMAT(dt_van, '%Y')
, DATE_FORMAT(dt_van, '%Y') + 1) AS einde
           FROM bad_tornooien
           ORDER  BY start DESC ";
   $result = mysql_query ($sql, $badm_db) or badm_mysql_die();
// format results by row
   $teller = 0;
   echo"<script type=\"text/javascript\" language=\"JavaScript\">";
   while ($badm = mysql_fetch_object($result))
   {
      echo "stm_aix(\"p2i".$teller."\",\"p1i0\",[0,\"Seizoen ".$badm->start."-".$badm->einde."\",\"\",\"\",-1,-1,0,\"http://www.badmintonsport.be/tornooien/resultaten.php?s=".$badm->einde."\",\"_self\",\"Seizoen ".$badm->start."-".$badm->einde."\"]);";
      $teller = $teller + 1;
   }
   echo"</script>";
   //mysql_free_result($result);
   mysql_close($badm_db);
?>
<script type="text/javascript" language="JavaScript">
<!--stm_ep();
stm_ep();
stm_ep();
stm_aix("p0i4","p0i0",[0,"Spelers","","",-1,-1,0,"http://www.badmintonsport.be/spelers.php","_self","Spelersprofielen en statistieken"]);
stm_aix("p0i5","p0i1",[0,"Jeugd","","",-1,-1,0,"","_self","Informatie over en voor de jeugdspelers"]);
stm_bpx("p1","p0",[1,2,0,0,1,4,0]);
stm_aix("p1i0","p0i0",[0,"Algemeen","","",-1,-1,0,"http://www.badmintonsport.be/jeugd/jeugd.php","_self","Jeugd trainers"]);
stm_aix("p1i1","p0i0",[0,"Teams","","",-1,-1,0,"http://www.badmintonsport.be/jeugd/jeugdteams.php","_self","Jeugdteams"]);
stm_aix("p1i2","p0i0",[0,"J-team","","",-1,-1,0,"http://www.badmintonsport.be/jeugd/J-team.php","_self","J-team"]);
stm_aix("p1i3","p0i0",[0,"Resultaten","","",-1,-1,0,"http://www.badmintonsport.be/jeugd/resultaten_jeugd.php?s=2006","_self","Resultaten jeugtornooien W&L spelers"]);
stm_aix("p1i4","p0i0",[0,"Jeugdtornooi","","",-1,-1,0,"http://www.badmintonsport.be/jeugd/jeugdtornooi.php","_self","Jeugdtornooi W&L"]);
stm_ep();
stm_aix("p0i6","p0i0",[0,"Recreanten","","",-1,-1,0,"http://www.badmintonsport.be/recreant.php","_self","Recreanten"]);
stm_aix("p0i7","p0i1",[0,"Foto's","","",-1,-1,0,"http://users.pandora.be/ikkegg","_blank","badminton foto's"]);
//-->
</script>
<?php
/*************************************************************************************************
 * Create a link to every photo directory that exists
 ************************************************************************************************/
/* Get the root dirctory of the website */
  $rootdir = $_SERVER['DOCUMENT_ROOT'];
/* directory where all the photo's are located in subdirectories */
  $dir = $rootdir."/pictures";
/* Get all the subdirectories of the pictures directory */
  include $rootdir."/functies/getDirectoryListing.php";
  $subdirs = getDirectoryListing($dir, "a", 1, 0, "none");
/* Number of subdirs in directory */
  $subdir_cnt = count($subdirs);
/* Display a link to every subdirectory */
  for ($i = 0; $i < $subdir_cnt; $i++)
  {
    echo"<script type=\"text/javascript\" language=\"JavaScript\">";
    if ($i == 0) // first subdirectory
    {
       echo "stm_bpx(\"p1\",\"p0\",[1,2,0,0,1,4,0]);\n";
    }
		echo "stm_aix(\"p0i".$i."\",\"p0i0\",[0,\"".$subdirs[$i]."\",\"\",\"\",-1,-1,0,\"http://www.badmintonsport.be/pictures/showThumbnails.php?dir=".$subdirs[$i]."\",\"_self\",\"".$subdirs[$i]."\"]);\n";
    if ($i == $subdir_cnt - 1) // last subdirectory
    {
      echo "stm_ep();\n";
    }
    echo"</script>";
  }
?>
<script type="text/javascript" language="JavaScript">
<!--
stm_ai("p0i8",[6,4,"#006633","",0,0,0]);
stm_aix("p0i9","p0i0",[0,"Links","","",-1,-1,0,"http://www.badmintonsport.be/links.php","_self","Links"]);
stm_aix("p0i10","p0i0",[0,"Message Board","","",-1,-1,0,"http://www.badmintonsport.be/messageboard/messageboard.php?p=1&m=n&id=0","_blank","Message Board"]);
stm_aix("p0i11","p0i0",[0,"Laatste Nieuws","","",-1,-1,0,"http://www.badmintonsport.be/laatste_nieuws.php","_self","De laatste wijzigingen aan de W&L website"]);
stm_ep();
stm_em();
//--></script>
      &nbsp;
 <!-- InstanceBeginEditable name="counter" --><!-- InstanceEndEditable -->
       <!-- InstanceBeginEditable name="wiens verjaardag is het" --><!-- InstanceEndEditable -->
  </td>
  <td width="100%" valign="top">
<!-- InstanceBeginEditable name="general" --> 
      <h1><a name="top" id="top"></a>5e jeugdtornooi W&amp;L</h1>
      <p class="subtitel"> zaterdag 3 januari 2004</p>
      <p class="kleinetitel">Belangrijke mededeling</p>
      <p class="kleinetekst">Wij verzoeken alle spelers om een <strong>HALF UUR</strong> 
        voor de aangeduide beginuren in de zaal te zijn. Omdat iedereen graag 
        zoveel mogelijk speelt, hebben we voor onze 154 deelnemers bijna 300 wedstrijden 
        op het programma staan! Die kunnen we alleen maar volgens schema spelen, 
        indien iedereen zich stipt op tijd voor zijn wedstrijden kan aanmelden. 
        Het is bovendien ook best mogelijk dat we al in de voormiddag wat voorsprong 
        kunnen nemen op ons wedstrijdschema.<br>
        Dus: wees a.u.b. op tijd! Onze wedstrijdleiding zal streng zijn voor laatkomers.<br>
        Wij vragen ook dat iedereen zijn VBL-lidkaart meebrengt.</p>
      <p class="kleinetitel"> Lijst van de uren van de eerste wedstrijd</p>
      <p class="kleinetekst"> 1. ADRIAENS Floris W&amp;L/VVBBC 10:15<br>
        2. ADRIAENS Jane W&amp;L 9:50<br>
        3. ARNALSTEEN Thomas BC DILBEEK 11:05<br>
        4. ARTOIS Jonas W&amp;L 10:15<br>
        5. ARTOIS Lien W&amp;L 9:25<br>
        6. ARTOIS Thomas W&amp;L 10:40<br>
        7. BARREZ Janne W&amp;L/VVBBC 13:35<br>
        8. BAUTERS Eveline SMASH FOR FUN/P 14:00<br>
        9. BECKERS Valentijn W&amp;L/VVBBC 10:15<br>
        10. BENDER Jenny TERVUREN BC 14:50<br>
        11. BENDER Lesley TERVUREN BC 11:05<br>
        12. BILLEN Bj&ouml;rn DIJLEVALLEI 9:50<br>
        13. BILTEREST Ann EVERBERGSE BC 9:25<br>
        14. BOGAERTS Eli DIJLEVALLEI 9:50<br>
        15. BONTINCK Bram HALLE 9:00<br>
        16. BONTINCK Dien HALLE 9:50<br>
        17. BRONS Thijs BC DILBEEK 10:15<br>
        18. CAPART Antoine TERVUREN BC 10:40<br>
        19. CEULEMANS Gert WIT-WIT RONSE 9:50<br>
        20. CHHAN NIN Stefaan HALLE 9:25<br>
        21. CLAES Arnaud PLUIMPLUKKERS 9:00<br>
        22. CLAES Karolien DE MINTONS 9:25<br>
        23. COENEN Lien GLABBAD 14:50<br>
        24. COESSENS Wout POLDERBOS/PBO 9:50<br>
        25. COLLEMAN Jef DIJLEVALLEI/VVB 9:00<br>
        26. CRAENHALS Katleen W&amp;L 9:00<br>
        27. CRIEL Ine SMASH FOR FUN/P 14:50<br>
        28. D'HONDT Joke LOKERSE/PBO 13:35<br>
        29. DAMMANS Sandra HALLE 9:25<br>
        30. DE BECKER Dylan GLABBAD 9:50<br>
        31. DE BLEEKER Jolein WIT-WIT RONSE 14:00<br>
        32. DE LANDTSHEER Perrine BC DILBEEK 14:00<br>
        33. DE RAEYMAEKER Xavier W&amp;L/VVBBC 10:40<br>
        34. DE VUYST Wietse TERVUREN BC 10:15<br>
        35. DE WAELE Stefaan W&amp;L 10:15<br>
        36. DE WIT Jori HALLE 10:15<br>
        37. DE WITTE Tom HALLE 10:15<br>
        38. DECOSTER Arne BC TIENEN/VVBBC 9:00<br>
        39. DECOSTER Ineke BC TIENEN/VVBBC 11:30<br>
        40. DEFLEM Kasper GLABBAD/VVBBC 9:50<br>
        41. DEGELAEN Pieter HALLE 10:40<br>
        42. DEGROOTE Elias WIT-WIT RONSE 9:00<br>
        43. DEKOSTER Jenifer W&amp;L 9:25<br>
        44. DELEU Philippe BC DILBEEK 11:30<br>
        45. DEMUYLDER Liesa HALLE 14:00<br>
        46. DENO Nele W&amp;L 14:00<br>
        47. DENO Veerle W&amp;L 14:00<br>
        48. DERZELLE Birgit W&amp;L 14:00<br>
        49. DESIE Ellen W&amp;L 14:00<br>
        50. DEWOLF Nicolas NERO'S/VVBBC 9:00<br>
        51. DEWULF Benoit W&amp;L 10:40<br>
        52. DRAGON Ben DE MINTONS 10:15<br>
        53. GARRE Bart HALLE 9:25<br>
        54. GENIE Nathalie W&amp;L 14:00<br>
        55. GOEMAERE Dorien HALLE 13:35<br>
        56. GOEMAERE Kathleen HALLE 9:25<br>
        57. GOOSSENS Tessa HALLE 9:25<br>
        58. HELLINCKX Jelle DIJLEVALLEI 10:40<br>
        59. HENCKENS Liesbet GLABBAD 14:50<br>
        60. HENDRICKX Dylan DIJLEVALLEI/VVB 9:00<br>
        61. HENDRIX David DIJLEVALLEI 9:25<br>
        62. HOLEMANS Frodo DIJLEVALLEI/VVB 10:40<br>
        63. HOLEMANS Yanna DIJLEVALLEI/VVB 13:35<br>
        64. HOLSBEEK Kevin GLABBAD 9:25<br>
        65. HUEGAERTS Kevin W&amp;L 9:00<br>
        66. JANSSEN Kaat GLABBAD 14:00<br>
        67. KELDERS Arne W&amp;L 9:50<br>
        68. KENENS Tanika W&amp;L 9:25<br>
        69. KESTENS Johannes DIJLEVALLEI 9:50<br>
        70. KESTENS Wies DIJLEVALLEI 10:15<br>
        71. KUPERS Sofie W&amp;L 9:25<br>
        72. LAEREMANS Britt EVERBERGSE BC 13:35<br>
        73. LAMPAERT Matthias DIJLEVALLEI 10:15<br>
        74. LANGENDRIES Julie W&amp;L 13:35<br>
        75. LAUREYS Sam DE MINTONS 10:40<br>
        76. LAUWERS Wesley DE MINTONS 10:15<br>
        77. LECOCQ Enovic WIT-WIT RONSE 9:00<br>
        78. LEDERMANN Tom HALLE 12:20<br>
        79. LENAERTS Stijn GLABBAD 9:00<br>
        80. LHERMITTE Jan GLABBAD 9:50<br>
        81. LHERMITTE Wouter GLABBAD 9:25<br>
        82. MAES Ben DE MINTONS 10:40<br>
        83. MAES Rob DE MINTONS 11:30<br>
        84. MANCINI Nicolas BC DILBEEK 10:15<br>
        85. MARCELIS Matti GLABBAD 9:25<br>
        86. MICHIELS Lien HALLE 14:50<br>
        87. MOENS Anneleen EVERBERGSE BC 11:05<br>
        88. MOENS Stijn EVERBERGSE BC 12:45<br>
        89. MOENS Thomas EVERBERGSE BC 10:15<br>
        90. NACHTERGAELE Joris W&amp;L 10:15<br>
        91. NOBELS Koenraad DE MINTONS 10:15<br>
        92. OVERLOOP Brecht W&amp;L 10:40<br>
        93. PANS Jeroen GLABBAD 9:50<br>
        94. PAULUS Joren W&amp;L/VVBBC 10:40<br>
        95. PELGRIMS Nick TERVUREN BC 9:50<br>
        96. PIENS Jasper W&amp;L 10:15<br>
        97. POEDTS Jonathan BC DILBEEK 11:30<br>
        98. POULEYN Carolien HALLE 14:50<br>
        99. POULEYN Lotte HALLE 14:50<br>
        100. PRIELS Jeffrey HERNE/VVBBC 9:50<br>
        101. PRIELS Josip HERNE/VVBBC 10:40<br>
        102. PUISSANT Lennert WIT-WIT RONSE 9:00<br>
        103. PUTSEYS Sven GLABBAD 9:50<br>
        104. RESZCZYNSKI Arne GLABBAD 9:00<br>
        105. RESZCZYNSKI Evelien GLABBAD 14:00<br>
        106. ROELS Stijn DE MINTONS 10:15<br>
        107. ROLIES Hanne BC DILBEEK 14:50<br>
        108. ROMBOUTS Ile TOTS/VVBBC 11:30<br>
        109. ROTTIERS Alexander DE MINTONS 9:50<br>
        110. SALENS Wim W&amp;L 10:15<br>
        111. SARKOZI Roxanne HALLE 9:25<br>
        112. SCHOETERS Wouter DIJLEVALLEI 10:15<br>
        113. SIJMENS Leen DIJLEVALLEI 14:00<br>
        114. SIJMENS Ruben DIJLEVALLEI 9:50<br>
        115. SIMKENS Simon W&amp;L/VVBBC 9:50<br>
        116. SIMONS Amy HALLE 11:30<br>
        117. SLEGERS Jonas W&amp;L/VVBBC 9:25<br>
        118. SMET Leander DE MINTONS 10:15<br>
        119. SMETS Stijn W&amp;L 11:30<br>
        120. SONDERVORST Jonne DIJLEVALLEI 10:40<br>
        121. SPANHOVE Reindert W&amp;L 9:00<br>
        122. SPILEERS Glenn WIT-WIT RONSE 9:00<br>
        123. SYMONS Robin W&amp;L/VVBBC 10:40<br>
        124. TEGETHOFF Sander W&amp;L 10:40<br>
        125. THEYS Daan GLABBAD 9:00<br>
        126. VAN ASSCHE Pieter BC DILBEEK 10:40<br>
        127. VAN DAMME Evi HALLE 9:00<br>
        128. VAN DAMME Lisa HALLE 14:00<br>
        129. VAN DAMME Robbe POLDERBOS/PBO 9:00<br>
        130. VAN DER AUWERA Thomas BC DILBEEK 11:30<br>
        131. VAN DER SYPT Jordy LOKERSE/PBO 9:00<br>
        132. VAN DER SYPT Romy LOKERSE/PBO 13:35<br>
        133. VAN EETVELDE Jo DE MINTONS 10:40<br>
        134. VAN ESPEN Kim W&amp;L 13:35<br>
        135. VAN HEMELRIJCK Jitse DIJLEVALLEI 9:00<br>
        136. VAN HOOREWEDER Thiebaut W&amp;L 11:30<br>
        137. VAN IMPE Silke DRIVE/PBO 14:00<br>
        138. VAN MELE Kristof DE MINTONS 9:50<br>
        139. VAN OEVELEN Jan DE MINTONS 10:15<br>
        140. VAN OEVELEN Pieter DE MINTONS 9:50<br>
        141. VAN ROSSOM Sam W&amp;L 9:50<br>
        142. VAN ROSSOM Tom W&amp;L 9:00<br>
        143. VAN STEEN Yana W&amp;L 9:25<br>
        144. VANDE PLAS Kim TERVUREN BC 14:50<br>
        145. VANDENBOSSCHE Ellen BC DILBEEK 14:00<br>
        146. VANDENBROECKE Sam LOKERSE/PBO 9:00<br>
        147. VANDENHOUCKE Flore WIT-WIT RONSE 14:00<br>
        148. VANDENHOUCKE Robbe WIT-WIT RONSE 12:20<br>
        149. VANDEVENNE Lieselotte W&amp;L 9:25<br>
        150. VANGERVEN Ankatrien W&amp;L 9:25<br>
        151. VANGERVEN Maarten W&amp;L 9:50<br>
        152. VER ELST Emma W&amp;L/VVBBC 9:25<br>
        153. VER ELST Mathias W&amp;L/VVBBC 9:25<br>
        154. VRIJDERS Berdien BUGGENHOUT/PBO 14:50</p>
      <p><a href="#top"><img src="../images/top.gif" alt="top" width="21" height="11" border="0" /></a></p>
      <!-- InstanceEndEditable -->
  </td>
 </tr>
 <tr>
  <td height="0"></td>
  <td></td>
 </tr>
 <tr>
  <td height="1"><img src="http://www.badmintonsport.be/images/spacer.gif" alt="" width="150" height="1"></td>
  <td></td>
 </tr>
</table>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <!--DWLayoutTable-->
 <tr>
  <td width="100%" height="50" valign="top" bgcolor="#FFFFFF">
   <table width="100%" border="0" cellpadding="0" cellspacing="0" bordercolor="#CCCCCC" bgcolor="#CCCCCC">
       <!--DWLayoutTable-->
    <tr>
     <td align="center" valign="middle" bordercolor="#CCCCCC" bgcolor="#006600">
         <img src="http://www.badmintonsport.be/images/spacer.gif" width="200" height="4"></td>
    </tr>
    <tr>
     <td width="100%" align="center" valign="middle" bordercolor="#CCCCCC" bgcolor="#CCCCCC" class="gmenu">Last change:
 <!-- InstanceBeginEditable name="datum" -->
            <!-- #BeginDate format:It1 -->28-12-2005<!-- #EndDate -->
            <!-- InstanceEndEditable -->
             E-mail: <a href="mailto:webmaster@badmintonsport.be">webmaster@badmintonsport.be</a></td>
    </tr>
    <tr>
     <td align="center" valign="middle" bordercolor="#CCCCCC" bgcolor="#006600" class="gmenu">
          <img src="http://www.badmintonsport.be/images/spacer.gif" width="200" height="4"></td>
    </tr>
   </table>
  </td>
 </tr>
</table>
</body>
<!-- InstanceEnd --></html>