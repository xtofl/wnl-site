<?php

require_once "../functies/speeluren.php";
require_once "../functies/badm_db.inc.php";

switch ($_SERVER["REQUEST_METHOD"]) {
    case "GET":
        print_speeluren();
        break;
    default:
        die("methode niet beschikbaar: ".$_SERVER["REQUEST_METHOD"]);
}

function print_speeluren()
{
    $badm_db = badm_conn_db();
    $resultaat = speeluren($badm_db);
    print json_encode($resultaat);
}

?>