<?php
function invoegen_template()
{
   echo "<script type=\"text/javascript\" language=\"JavaScript1.2\">\n";
   echo "<!--\n";
   echo "stm_bm([\"W&L menu\",400,\"\",\"http://www.badmintonsport.be/scripts/blank.gif\",0,\"\",\"\",0,0,250,0,1000,1,0,0]);\n";
   echo "stm_bp(\"p0\",[1,4,0,0,1,4,0,7,100,\"progid:DXImageTransform.Microsoft.Fade(overlap=.5,enabled=0,Duration=0.30)\",-2,\"progid:DXImageTransform.Microsoft.Pixelate(MaxSquare=15,enabled=0,Duration=0.50)\",-2,50,0,0,\"#669999\",\"#006633\",\"\",3,0,0,\"#ffffff\"]);\n";
   echo "stm_ai(\"p0i0\",[0,\"Home\",\"\",\"\",-1,-1,0,\"http://www.badmintonsport.be/index.php\",\"_self\",\"Startpagina\",\"\",\"\",\"\",0,0,0,\"\",\"\",0,0,0,0,1,\"#fffff7\",0,\"#b5bed6\",0,\"\",\"\",3,3,0,0,\"#fffff7\",\"#000000\",\"#000000\",\"#000000\",\"10pt Verdana\",\"10pt Verdana\",0,0]);\n";
   echo "stm_aix(\"p0i1\",\"p0i0\",[0,\"Algemeen\",\"\",\"\",-1,-1,0,\"\",\"_self\",\"informatie over de club\",\"\",\"\",\"\",0,0,0,\"http://www.badmintonsport.be/scripts/arrow_r.gif\",\"http://www.badmintonsport.be/scripts/arrow_r.gif\",7,7]);\n";
   echo "stm_bpx(\"p1\",\"p0\",[1,2,0,0,1,4,0]);\n";
   echo "stm_aix(\"p1i0\",\"p0i1\",[0,\"Bestuur\",\"\",\"\",-1,-1,0,\"http://www.badmintonsport.be/algemeen/bestuur.php\",\"_self\",\"Bestuursleden\",\"\",\"\",\"\",0,0,0,\"\",\"\",0,0,0,1]);\n";
   echo "stm_bpx(\"p1\",\"p0\",[1,2,0,0,1,4,0,0]);\n";
   echo "stm_aix(\"p2i0\",\"p1i0\",[0,\"Mededelingen\",\"\",\"\",-1,-1,0,\"http://www.badmintonsport.be/algemeen/Bestuur/bestuursmededelingen.php\",\"_self\",\"Bestuursmededelingen\"]);\n";
   echo "stm_ep();\n";
   echo "stm_aix(\"p1i1\",\"p0i0\",[0,\"Medewerkers\",\"\",\"\",-1,-1,0,\"http://www.badmintonsport.be/algemeen/medewerkers.php\",\"_self\",\"Medewerkers\",\"\",\"\",\"\",0,0,0,\"\",\"\",0,0,0,1]);\n";
   echo "stm_aix(\"p1i2\",\"p0i0\",[0,\"Speeluren\",\"\",\"\",-1,-1,0,\"http://www.badmintonsport.be/algemeen/speeluren.php\",\"_self\",\"Speeluren\",\"\",\"\",\"\",0,0,0,\"\",\"\",0,0,0,1]);\n";
   echo "stm_aix(\"p1i3\",\"p0i1\",[0,\"Geschiedenis\",\"\",\"\",-1,-1,0,\"\",\"_self\",\"Geschiedenis\",\"\",\"\",\"\",0,0,0,\"\",\"\",0,0,0,1]);\n";
   echo "stm_bpx(\"p2\",\"p0\",[1,2,0,0,1,4,0,0]);\n";
   echo "stm_aix(\"p2i0\",\"p1i0\",[0,\"Clubkampioenen\",\"\",\"\",-1,-1,0,\"http://www.badmintonsport.be/algemeen/Geschiedenis/clubkampioenen.php\",\"_self\",\"Winnaars Clubkampioenschap\"]);\n";
   echo "stm_aix(\"p2i1\",\"p1i0\",[0,\"Nationale Kampioenen\",\"\",\"\",-1,-1,0,\"http://www.badmintonsport.be/algemeen/Geschiedenis/nationalekampioenen.php\",\"_self\",\"Nationale Kampioenen W&L\"]);\n";
   echo "stm_ep();\n";
   echo "stm_aix(\"p1i4\",\"p0i0\",[0,\"Sponsors\",\"\",\"\",-1,-1,0,\"http://www.badmintonsport.be/algemeen/sponsors.php\",\"_self\",\"Sponsors\",\"\",\"\",\"\",0,0,0,\"\",\"\",0,0,0,1]);\n";
   echo "stm_aix(\"p1i5\",\"p0i0\",[0,\"Artikels\",\"\",\"\",-1,-1,0,\"http://www.badmintonsport.be/algemeen/artikels.php\",\"_self\",\"Artikels over de club\",\"\",\"\",\"\",0,0,0,\"\",\"\",0,0,0,1]);\n";
   echo "stm_ep();\n";
   echo "stm_aix(\"p0i2\",\"p0i0\",[0,\"Competitie\",\"\",\"\",-1,-1,0,\"http://www.badmintonsport.be/competitie.php\",\"_self\",\"Competitie: ploegen en uitslagen\"]);\n";
   echo "stm_aix(\"p0i3\",\"p0i1\",[0,\"Tornooien\",\"\",\"\",-1,-1,0,\"http://www.badmintonsport.be/tornooien/tornooioverzicht.php\",\"_self\",\"Tornooien\"]);\n";
   echo "stm_bpx(\"p1\",\"p0\",[1,2,0,0,1,4,0]);\n";
   echo "stm_aix(\"p1i0\",\"p0i1\",[0,\"Inschrijvingen\",\"\",\"\",-1,-1,0,\"\",\"_self\",\"Schrijf je hier in voor tornooien\"]);\n";
   echo "stm_bpx(\"p1\",\"p0\",[1,2,0,0,1,4,0,0]);\n";
   require_once "/home/badmin/public_html/functies/badm_db.inc.php";
    $badm_db = badm_conn_db();
// create SQL statement 
   $sql = "SELECT id
                , tornooi AS organisatie
                , doelgroep
                , DATE_FORMAT(start_dt, '%d/%m') AS start_dt
                , verzonden
           FROM inschr_torn
		   WHERE DATE_ADD(dt_inschr, INTERVAL 21 DAY ) > NOW()
		   ORDER BY dt_inschr DESC";
   $result = mysql_query ($sql, $badm_db) or badm_mysql_die();
// format results by row
   $teller = 0;
   while ($badm = mysql_fetch_object($result))
   {
      echo "stm_aix(\"p2i";
      echo $teller;
      echo "\",\"p1i0\",[1,\"";
      echo $badm->organisatie;
      if ($badm->doelgroep == 'J' || $badm->doelgroep == 'V')
      {
        echo $badm->doelgroep == 'J' ? " (Jeugd)" : " (Veteranen)";
        if ($badm->verzonden == 0) //inschrijvingsperiode nog niet afgelopen
        {
          echo " <b>".$badm->start_dt."</b>";
        }
        else
        {
          echo "  <img src=http://www.badmintonsport.be/images/closed.png>";
        }
      }
      else
      {
        if ($badm->verzonden == 0) //inschrijvingsperiode nog niet afgelopen
        {
          echo " <b>".$badm->start_dt."</b>";
        }
        else
        {
          echo "  <img src=http://www.badmintonsport.be/images/closed.png>";
        }
      }
      echo "\",\"\",\"\",-1,-1,0,\"/tornooien/inschrijvingen.php?id=";
      echo $badm->id;
      echo "\",\"_self\",\"Inschrijving ";
      echo $badm->organisatie;
      echo "\"]);\n";
      $teller = $teller + 1;
   }
   echo "stm_ep();\n";
   echo "stm_aix(\"p1i1\",\"p0i0\",[0,\"Aanvangsuren\",\"\",\"\",-1,-1,0,\"http://www.badmintonsport.be/tornooien/aanvangsuren.php\",\"_self\",\"Aanvangsuren tornooien\"]);\n";
   echo "stm_aix(\"p1i2\",\"p0i0\",[0,\"Kalender\",\"\",\"\",-1,-1,0,\"http://www.badmintonsport.be/docs/tornooikalender.pdf\",\"_blank\",\"Tornooikalender\"]);\n";
   echo "stm_aix(\"p1i3\",\"p0i1\",[0,\"Resultaten\",\"\",\"\",-1,-1,0,\"\",\"_self\",\"Tornooiresultaten W&L spelers\"]);\n";
   echo "stm_bpx(\"p2\",\"p0\",[1,2,0,0,1,4,0,0]);\n";
// create SQL statement 
   $sql = "SELECT  DISTINCT 
IF(DATE_FORMAT(dt_van, '%m') < 8, DATE_FORMAT(dt_van, '%Y') - 1, DATE_FORMAT(dt_van, '%Y')) AS start
,IF(DATE_FORMAT(dt_van, '%m') < 8, DATE_FORMAT(dt_van, '%Y'), DATE_FORMAT(dt_van, '%Y') + 1) AS einde
           FROM bad_tornooien
           ORDER  BY start DESC ";
   $result = mysql_query ($sql, $badm_db) or badm_mysql_die();
// format results by row
   $teller = 0;
   while ($badm = mysql_fetch_object($result))
   {
      echo "stm_aix(\"p2i";
      echo $teller;
      echo "\",\"p1i0\",[0,\"Seizoen ";
      echo $badm->start.'-'.$badm->einde;
      echo "\",\"\",\"\",-1,-1,0,\"http://www.badmintonsport.be/tornooien/resultaten.php?s=";
      echo $badm->einde;
      echo "\",\"_self\",\"Seizoen ";
      echo $badm->start.'-'.$badm->einde;
      echo "\"]);\n";
      $teller = $teller + 1;
   }
   mysql_free_result($result);
   mysql_close($badm_db);
   echo "stm_ep();\n";
   echo "stm_ep();\n";
   echo "stm_aix(\"p0i4\",\"p0i0\",[0,\"Spelers\",\"\",\"\",-1,-1,0,\"http://www.badmintonsport.be/spelers.php\",\"_self\",\"Spelersprofielen en statistieken\"]);\n";
   echo "stm_aix(\"p0i5\",\"p0i1\",[0,\"Jeugd\",\"\",\"\",-1,-1,0,\"\",\"_self\",\"Informatie over en voor de jeugdspelers\"]);\n";
   echo "stm_bpx(\"p1\",\"p0\",[1,2,0,0,1,4,0]);\n";
   echo "stm_aix(\"p1i0\",\"p0i0\",[0,\"Algemeen\",\"\",\"\",-1,-1,0,\"http://www.badmintonsport.be/jeugd/jeugd.php\",\"_self\",\"Jeugd trainers\"]);\n";
   echo "stm_aix(\"p1i1\",\"p0i0\",[0,\"Teams\",\"\",\"\",-1,-1,0,\"http://www.badmintonsport.be/jeugd/jeugdteams.php\",\"_self\",\"Jeugdteams\"]);\n";
   echo "stm_aix(\"p1i2\",\"p0i0\",[0,\"J-team\",\"\",\"\",-1,-1,0,\"http://www.badmintonsport.be/jeugd/J-team.php\",\"_self\",\"J-team\"]);\n";
   echo "stm_aix(\"p1i3\",\"p0i0\",[0,\"Resultaten\",\"\",\"\",-1,-1,0,\"http://www.badmintonsport.be/jeugd/resultaten_jeugd.php?s=2008\",\"_self\",\"Resultaten jeugdtornooien W&L spelers\"]);\n";
   echo "stm_aix(\"p1i4\",\"p0i0\",[0,\"Jeugdtornooi\",\"\",\"\",-1,-1,0,\"http://www.badmintonsport.be/jeugd/jeugdtornooi.php\",\"_self\",\"Jeugdtornooi W&L\"]);\n";
   echo "stm_ep();\n";
   echo "stm_aix(\"p0i6\",\"p0i0\",[0,\"Recreanten\",\"\",\"\",-1,-1,0,\"http://www.badmintonsport.be/recreant.php\",\"_self\",\"Recreanten\"]);\n";
   echo "stm_aix(\"p0i7\",\"p0i1\",[0,\"Foto's\",\"\",\"\",-1,-1,0,\"http://users.pandora.be/ikkegg\",\"_blank\",\"badminton foto's\"]);\n";
/*************************************************************************************************
 * Create a link to every photo directory that exists
 ************************************************************************************************/
/* Get the root dirctory of the website */
  $rootdir = $_SERVER['DOCUMENT_ROOT'];
/* directory where all the photo's are located in subdirectories */
  $dir = $rootdir."/pictures";
/* Get all the subdirectories of the pictures directory */
  include $rootdir."/functies/getDirectoryListing.php";
  $subdirs = getDirectoryListing($dir, "a", 1, 0, "none");
/* Number of subdirs in directory */
  $subdir_cnt = count($subdirs);
/* Display a link to every subdirectory */
  $i = 0;
  foreach ($subdirs as $subdir)
  {
    if ($i == 0) // first subdirectory
    {
       echo "stm_bpx(\"p1\",\"p0\",[1,2,0,0,1,4,0]);\n";
    }
    echo "stm_aix(\"p0i".$i."\",\"p0i0\",[0,\"".$subdir."\",\"\",\"\",-1,-1,0,\"http://www.badmintonsport.be/pictures/showThumbnails.php?dir=".$subdir."\",\"_self\",\"".$subdir."\"]);\n";
    if ($i == $subdir_cnt - 1) // last subdirectory
    {
      echo "stm_ep();\n";
    }
	$i++;
  }
   echo "stm_ai(\"p0i8\",[6,4,\"#006633\",\"\",0,0,0]);\n";
   echo "stm_aix(\"p0i9\",\"p0i0\",[0,\"Links\",\"\",\"\",-1,-1,0,\"http://www.badmintonsport.be/links.php\",\"_self\",\"Links\"]);\n";
   echo "stm_aix(\"p0i10\",\"p0i0\",[0,\"Message board\",\"\",\"\",-1,-1,0,\"http://www.badmintonsport.be/messageboard/messageboard.php?p=1&m=n&id=0\",\"_blank\",\"Message Board\"]);\n";
   echo "stm_aix(\"p0i11\",\"p0i0\",[0,\"Laatste Nieuws\",\"\",\"\",-1,-1,0,\"http://www.badmintonsport.be/laatste_nieuws.php\",\"_self\",\"De laatste wijzigingen aan de W&L website\"]);\n";
   echo "stm_aix(\"p0i12\",\"p0i0\",[0,\"Team Veltem Motors\",\"\",\"\",-1,-1,0,\"http://www.everyoneweb.com/teamveltemmotors\",\"_self\",\"De fansite van Team Veltem Motors\"]);\n";
   echo "stm_ep();\n";
   echo "stm_em();\n";
   echo "//-->\n";
   echo "</script>\n";
}
?>