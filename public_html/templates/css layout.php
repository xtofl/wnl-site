<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<?php
   /* Build the relative path to the root directory of the website */
   $rootdir = "";
   for ($i=1; $i<=substr_count(dirname($_SERVER['PHP_SELF']), "/"); $i++)
   {
     $rootdir = $rootdir."../";
   }
?>
<head>
  <meta http-equiv="Content-Type"      content="text/html charset=iso-8859-1">
  <meta name="robots"                  content="index,follow">
  <meta name="keywords"                content="badminton,batminton,w&l,W&amp;L bv,badmintonvereniging,club,badmintonclub,sport,Belgium,Veltem,Herent,Leuven,Winksele,Vlaanderen,Belgi�">
  <meta name="description"             content="W&amp;L bv : De leukste badmintonclub van Belgi�">
  <meta name="abstract"                content="badminton, club">
  <meta name="Area"                    content="General">
  <meta name="Author"                  content="W&amp;L bv">
  <meta name="Copyright"               content="(c) 2004-2005 W&amp;L bv">
  <meta name="Creator"                 content="W&amp;L bv">
  <meta name="Generator"               content="W&amp;L bv">
  <meta name="Owner"                   content="W&amp;L bv">
  <meta name="Publisher"               content="W&amp;L bv">
  <meta name="Rating"                  content="General">
  <meta name="revisit-after"           content="7 Days">
  <meta name="revisit"                 content="7 Days">
  <meta name="document-classification" content="Internet">
  <meta name="document-type"           content="Public">
  <meta name="document-rating"         content="Safe for Kids">
  <meta name="document-distribution"   content="Global">
  <meta http-equiv="Audience"          content="General">
  <meta http-equiv="content-language"  content="NL">
  <meta http-equiv="Pragma"            content="no-cache">
  <meta http-equiv="Expires"           content="now">

 <title>W&amp;L badmintonvereniging vzw - Homepage</title>

  <link rel="stylesheet" href="<?=$rootdir ?>css/W&amp;L_layout.css" type="text/css">

<script type="text/javascript" language="JavaScript1.2" src="<?php echo $rootdir ?>scripts/stm31.js"></script>
<script type="text/javascript" language="JavaScript" src="<?php echo $rootdir ?>scripts/get_window_height.js"></script>
<script type="text/javascript" language="JavaScript" src="<?php echo $rootdir ?>scripts/set_footer.js"></script>
<script type="text/javascript">
<!-- have people break free from frames automatically when they come to our page
if (parent.frames.length >= 1)
{
  window.top.location.href="<?php echo basename(__FILE__);  ?>";
}
// -->
</script>

</head>

<body>
<!-- -------------------------------------------HEADER------------------------------------------------------------- -->
<div id="header">

 <table width="85%" border="0" cellspacing="0" cellpadding="3">
  <tr>
   <td rowspan="2" align="left">
    <img src="<?php echo $rootdir ?>images/logow&amp;l-100.gif" width="100" height="80" alt="W&amp;L logo">
   </td>
   <td align="center">
    <h4>W&amp;L Badmintonvereniging vzw, Mechelsesteenweg 387, 3020 Herent</h4>
   </td>
  </tr>
  <tr>
   <td align="center">
    <img src="<?php echo $rootdir ?>images/chinas.gif" width="400" height="60" alt="China's Capital">
   </td>
  </tr>
 </table>

</div>
<!-- ------------------------------------------- MENU ------------------------------------------------------------- -->
<div id="leftcontent">

<?php
  require_once $rootdir."functies/show_menu.php";
  show_menu();
?>

</div>
<!-- ------------------------------------------- MAIN ------------------------------------------------------------- -->
<div id="centercontent">

    <span style="float: right" class="kleinetekst">Er zijn 2 bezoekers online</span>

</div>
<!-- ------------------------------------------- LEEG ------------------------------------------------------------- -->
<div id="rightcontent">
</div>
<!-- -------------------------------------------FOOTER------------------------------------------------------------- -->
<div id="footer">

 <center class="kleinetekst">
  Last change: 28/12/05&nbsp;&nbsp;
  E-mail: <a href="mailto:webmaster@badmintonsport.be">webmaster@badmintonsport.be</a>
 </center>

</div>

</body>
</html>