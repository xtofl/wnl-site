<?php
  require_once "functies/website_usage.php";
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html lang="nl"><!-- InstanceBegin template="/Templates/templaterootphp.dwt.php" codeOutsideHTMLIsLocked="false" -->
<?php
   require_once "functies/badm_db.inc.php";
   $badm_db = badm_conn_db();
?>
<head><meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<META NAME="robots"                  CONTENT="index,follow">
<META NAME="keywords"                CONTENT="badminton,batminton,w&amp;l,badmintonvereniging,club,badmintonclub,sport,Belgium,Veltem,Herent,Leuven,Winksele,Belgi�">
<META NAME="description"             CONTENT="W&amp;L bv : De grootste en leukste badmintonclub van Brabant">
<META NAME="abstract"                CONTENT="badminton, club">
<META NAME="Area"                    CONTENT="General">
<META NAME="Author"                  CONTENT="W&amp;L bv">
<META NAME="Copyright"               CONTENT="(c) 2003-2004 W&amp;L bv">
<META NAME="Creator"                 CONTENT="W&amp;L bv">
<META NAME="Generator"               CONTENT="W&amp;L bv">
<META NAME="Owner"                   CONTENT="W&amp;L bv">
<META NAME="Publisher"               CONTENT="W&amp;L bv">
<META NAME="Rating"                  CONTENT="General">
<META NAME="revisit-after"           CONTENT="7 Days">
<META NAME="revisit"                 CONTENT="7 Days">
<META NAME="document-classification" CONTENT="Internet">
<META NAME="document-type"           CONTENT="Public">
<META NAME="document-rating"         CONTENT="Safe for Kids">
<META NAME="document-distribution"   CONTENT="Global">

<META HTTP-EQUIV="Audience"          CONTENT="General">
<META HTTP-EQUIV="content-language"  CONTENT="NL">
<META HTTP-EQUIV="Pragma"            CONTENT="no-cache">
<META HTTP-EQUIV="Expires"           CONTENT="now">
<!-- InstanceBeginEditable name="doctitle" -->
<title>W&amp;L badmintonvereniging vzw - Competitie</title>
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="head" -->
<script language="JavaScript" type="text/JavaScript">
<!--

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}
//-->
</script>
<!-- InstanceEndEditable -->
<link href="http://www.badmintonsport.be/css/badminton.css" rel="stylesheet" type="text/css">
<script type="text/javascript" language="JavaScript1.2" src="http://www.badmintonsport.be/scripts/stm31.js"></script>
<script type="text/javascript" language="JavaScript" src="http://www.badmintonsport.be/scripts/dw_rotator.js"></script>
<script type="text/javascript" language="JavaScript">
<!--
function initRotator()
{
  var rotator1 = new dw_Rotator("sponsors_banner", 4000, "http://www.badmintonsport.be/images/", "_blank");
  rotator1.addImages("Veltem_Motors_banner.png", "banner_verhoeven.jpg");
  //rotator1.addImages("Veltem_Motors_banner.png");
  rotator1.addActions("http://www.veltem-motors.be", "http://www.isolatieverhoeven.be");
  //rotator1.addActions("http://www.veltem-motors.be");
  dw_Rotator.start();
}
function doLoad()
{
  // initialize the menu
  st_onload();
  // initialize the banner rotator
  initRotator();
}
//-->
</script>
</head>
<body class="badminton" onload="doLoad();">
<table width="100%" border="0" cellpadding="0" cellspacing="0">
 <tr>
  <td colspan="2">
   <table width="100%" border="0" cellpadding="0" cellspacing="0">
    <!--DWLayoutTable-->
    <tr> 
     <td width="100%" style="background-color: #006600;"><img src="http://www.badmintonsport.be/images/pt_transp.gif" width="1px" height="4px" alt=""></td>
    </tr>
    <tr> 
     <td align="center" valign="top" bgcolor="#CCCCCC">
      <table width="100%" border="0" cellspacing="0" cellpadding="3">
       <tr align="center" valign="middle"> 
        <td rowspan="2" align="left">
	     <img src="http://www.badmintonsport.be/images/logow&amp;l-100.gif" width="100" height="80" alt="logo">
	    </td>
        <td>
	     <h4>W&amp;L Badmintonvereniging vzw<br>
	         Mechelsesteenweg 387<br>
             3020 Herent</h4>
		</td>
        <td align="right"><a href="javascript:void();" onclick="return dw_Rotator.doClick(0)"  onmouseover="dw_Rotator.pause(0)" onmouseout="dw_Rotator.resume(0)" onfocus="this.blur()"><img id="sponsors_banner" name="sponsors_banner" src="images/Veltem_Motors_banner.png" alt="sponsors" height="100" width="320" border="0"></a></td>
       </tr>
      </table>
	 </td>
    </tr>
    <tr> 
     <td bgcolor="#336600"><img src="http://www.badmintonsport.be/images/pt_transp.gif" width="1px" height="4px" alt=""></td>
    </tr>
   </table>
  </td>
 </tr>
 <tr> 
    <td width="150" valign="top">
<!-- InstanceBeginEditable name="Floating Menu" -->
<!-- InstanceEndEditable --> 
      <script type="text/javascript" language="JavaScript1.2">
<!--
stm_bm(["W&L menu",400,"","http://www.badmintonsport.be/scripts/blank.gif",0,"","",0,0,250,0,1000,1,0,0]);
stm_bp("p0",[1,4,0,0,1,4,0,7,100,"progid:DXImageTransform.Microsoft.Fade(overlap=.5,enabled=0,Duration=0.30)",-2,"progid:DXImageTransform.Microsoft.Pixelate(MaxSquare=15,enabled=0,Duration=0.50)",-2,50,0,0,"#669999","#006633","",3,0,0,"#ffffff"]);
stm_ai("p0i0",[0,"Home","","",-1,-1,0,"index.php","_self","Startpagina","","","",0,0,0,"","",0,0,0,0,1,"#fffff7",0,"#b5bed6",0,"","",3,3,0,0,"#fffff7","#000000","#000000","#000000","10pt Verdana","10pt Verdana",0,0]);
stm_aix("p0i1","p0i0",[0,"Algemeen","","",-1,-1,0,"","_self","informatie over de club","","","",0,0,0,"http://www.badmintonsport.be/scripts/arrow_r.gif","http://www.badmintonsport.be/scripts/arrow_r.gif",7,7]);
stm_bpx("p1","p0",[1,2,0,0,1,4,0]);
stm_aix("p1i0","p0i0",[0,"Bestuur","","",-1,-1,0,"/algemeen/bestuur.php","_self","Bestuursleden","","","",0,0,0,"","",0,0,0,1]);
stm_bpx("p1","p0",[1,2,0,0,1,4,0,0]);
stm_aix("p2i0","p1i0",[0,"Mededelingen","","",-1,-1,0,"/algemeen/Bestuur/bestuursmededelingen.php","_self","Bestuursmededelingen"]);
stm_ep();
stm_aix("p1i1","p0i0",[0,"Medewerkers","","",-1,-1,0,"/algemeen/medewerkers.php","_self","Medewerkers","","","",0,0,0,"","",0,0,0,1]);
stm_aix("p1i2","p0i0",[0,"Speeluren","","",-1,-1,0,"/algemeen/speeluren.php","_self","Speeluren","","","",0,0,0,"","",0,0,0,1]);
stm_aix("p1i3","p0i1",[0,"Geschiedenis","","",-1,-1,0,"","_self","Geschiedenis","","","",0,0,0,"","",0,0,0,1]);
stm_bpx("p2","p0",[1,2,0,0,1,4,0,0]);
stm_aix("p2i0","p1i0",[0,"Clubkampioenen","","",-1,-1,0,"/algemeen/Geschiedenis/clubkampioenen.php","_self","Winnaars Clubkampioenschap"]);
stm_aix("p2i1","p1i0",[0,"Nationale Kampioenen","","",-1,-1,0,"/algemeen/Geschiedenis/nationalekampioenen.php","_self","Nationale Kampioenen W&L"]);
stm_ep();
stm_aix("p1i4","p0i0",[0,"Sponsors","","",-1,-1,0,"/algemeen/sponsors.php","_self","Sponsors","","","",0,0,0,"","",0,0,0,1]);
stm_aix("p1i5","p0i0",[0,"Artikels","","",-1,-1,0,"/algemeen/artikels.php","_self","Artikels","","","",0,0,0,"","",0,0,0,1]);
stm_ep();
stm_aix("p0i2","p0i0",[0,"Competitie","","",-1,-1,0,"/competitie.php","_self","Competitie: ploegen en uitslagen"]);
stm_aix("p0i3","p0i1",[0,"Tornooien","","",-1,-1,0,"/tornooien/tornooioverzicht.php","_self","Tornooien"]);
stm_bpx("p1","p0",[1,2,0,0,1,4,0]);
stm_aix("p1i0","p0i1",[0,"Inschrijvingen","","",-1,-1,0,"","_self","Inschrijvingen","Schrijf je hier in voor tornooien"]);
stm_bpx("p1","p0",[1,2,0,0,1,4,0,0]);
//-->
</script>
<?php
// create SQL statement 
   $sql = "SELECT id
                , organisatie
                , doelgroep
                , DATE_FORMAT(start_dt, '%d/%m') AS start_dt
                , verzonden
           FROM inschr_torn
		   WHERE DATE_ADD(dt_inschr, INTERVAL 21 DAY ) > NOW()
		   ORDER BY dt_inschr DESC";
   $result = mysql_query ($sql, $badm_db) or badm_mysql_die();
// format results by row
   $teller = 0;
   echo"<script type=\"text/javascript\" language=\"JavaScript1.2\">";
   while ($badm = mysql_fetch_object($result))
   {
      echo "stm_aix(\"p2i";
      echo $teller;
      echo "\",\"p1i0\",[1,\"";
      echo $badm->organisatie;
      if ($badm->doelgroep == 'J' || $badm->doelgroep == 'V')
      {
        echo $badm->doelgroep == 'J' ? " (Jeugd)" : " (Veteranen)";
        if ($badm->verzonden == 0) //inschrijvingsperiode nog niet afgelopen
        {
          echo " <B>".$badm->start_dt."</B>";
        }
        else
        {
          echo "  <IMG src=/images/closed.png>";
        }
      }
      else
      {
       if ($badm->verzonden == 0) //inschrijvingsperiode nog niet afgelopen
        {
          echo " <B>".$badm->start_dt."</B>";
        }
        else
        {
          echo "  <IMG src=/images/closed.png>";
        }
      }
      echo "\",\"\",\"\",-1,-1,0,\"/tornooien/inschrijvingen.php?id=";
      echo $badm->id;
      echo "\",\"_self\",\"Inschrijving ";
      echo $badm->organisatie;
      echo "\"]);\n";
      $teller = $teller + 1;
   }
   echo"</script>\n";
?>
<script type="text/javascript" language="JavaScript1.2">
<!--
stm_ep();
stm_aix("p1i1","p0i0",[0,"Aanvangsuren","","",-1,-1,0,"/tornooien/aanvangsuren.php","_self","Aanvangsuren tornooien"]);
stm_aix("p1i2","p0i0",[0,"Kalender","","",-1,-1,0,"http://www.badmintonsport.be/docs/tornooikalender.pdf","_blank","Tornooikalender"]);
stm_aix("p1i3","p0i1",[0,"Resultaten","","",-1,-1,0,"","_self","Tornooiresultaten W&L spelers"]);
stm_bpx("p2","p0",[1,2,0,0,1,4,0,0]);
//-->
</script>
<?php
// create SQL statement   
 $sql = "SELECT  DISTINCT 
IF(DATE_FORMAT(dt_van, '%m') < 8, DATE_FORMAT(dt_van, '%Y') - 1, DATE_FORMAT(dt_van, '%Y')) AS start
,IF(DATE_FORMAT(dt_van, '%m') < 8, DATE_FORMAT(dt_van, '%Y'), DATE_FORMAT(dt_van, '%Y') + 1) AS einde
           FROM bad_tornooien
           ORDER  BY start DESC ";
   $result = mysql_query ($sql, $badm_db) or badm_mysql_die();
// format results by row
   $teller = 0;
   echo"<script type=\"text/javascript\" language=\"JavaScript1.2\">";
   while ($badm = mysql_fetch_object($result))
   {
       echo "stm_aix(\"p2i";
      echo $teller;
      echo "\",\"p1i0\",[0,\"Seizoen ";
      echo $badm->start.'-'.$badm->einde;
      echo "\",\"\",\"\",-1,-1,0,\"/tornooien/resultaten.php?s=";
      echo $badm->einde;
      echo "&view_id=22\",\"_self\",\"Seizoen ";
      echo $badm->start.'-'.$badm->einde;
      echo "\"]);\n";
      $teller = $teller + 1;
   }
   echo"</script>\n";
   //mysql_free_result($result);
   mysql_close($badm_db);
?>
<script type="text/javascript" language="JavaScript1.2">
<!--
stm_ep();
stm_ep();
stm_aix("p0i4","p0i0",[0,"Spelers","","",-1,-1,0,"/spelers.php","_self","Spelersprofielen"]);
stm_aix("p0i5","p0i1",[0,"Jeugd","","",-1,-1,0,"","_self","Informatie over en voor de jeugdspelers"]);
stm_bpx("p1","p0",[1,2,0,0,1,4,0]);
stm_aix("p1i0","p0i0",[0,"Algemeen","","",-1,-1,0,"/jeugd/jeugd.php","_self","Jeugd trainers"]);
stm_aix("p1i1","p0i0",[0,"Teams","","",-1,-1,0,"/jeugd/jeugdteams.php","_self","Jeugdteams"]);
stm_aix("p1i2","p0i0",[0,"J-team","","",-1,-1,0,"/jeugd/J-team.php","_self","J-team"]);
stm_aix("p1i3","p0i0",[0,"Resultaten","","",-1,-1,0,"/jeugd/resultaten_jeugd.php?s=2008","_self","Resultaten jeugdtornooien W&L spelers"]);
stm_aix("p1i4","p0i0",[0,"Jeugdtornooi","","",-1,-1,0,"/jeugd/jeugdtornooi.php","_self","Jeugdtornooi W&L"]);
stm_ep();
stm_aix("p0i6","p0i0",[0,"Recreanten","","",-1,-1,0,"/recreant.php","_self","Recreanten"]);
stm_aix("p0i7","p0i1",[0,"Foto's","","",-1,-1,0,"http://users.pandora.be/ikkegg","_blank","badminton foto's"]);
//-->
</script>
<?php
/*************************************************************************************************
 * Create a link to every photo directory that exists
 ************************************************************************************************/
/* Get the root dirctory of the website */
  $rootdir = $_SERVER['DOCUMENT_ROOT'];
/* directory where all the photo's are located in subdirectories */
  $dir = $rootdir."/pictures";
/* Get all the subdirectories of the pictures directory */
  include $rootdir."/functies/getDirectoryListing.php";
  $subdirs = getDirectoryListing($dir, "a", 1, 0, "none");
/* Number of subdirs in directory */
  $subdir_cnt = count($subdirs);
/* Display a link to every subdirectory */
  $i = 0;
  foreach ($subdirs as $subdir)
  {
    echo "<script type=\"text/javascript\" language=\"JavaScript1.2\">\n";
    if ($i == 0) // first subdirectory
    {
       echo "stm_bpx(\"p1\",\"p0\",[1,2,0,0,1,4,0]);\n";
    }
	echo "stm_aix(\"p0i".$i."\",\"p0i0\",[0,\"".$subdir."\",\"\",\"\",-1,-1,0,\"/pictures/showThumbnails.php?dir=".$subdir."\",\"_self\",\"".$subdir."\"]);\n";
    if ($i == $subdir_cnt - 1) // last subdirectory
    {
      echo "stm_ep();\n";
    }
    echo"</script>\n";
	$i++;
  }
?>
      <script type="text/javascript" language="JavaScript1.2">
<!--
stm_ai("p0i8",[6,4,"#006633","",0,0,0]);
stm_aix("p0i9","p0i0",[0,"Links","","",-1,-1,0,"/links.php","_self","Links"]);
stm_aix("p0i10","p0i0",[0,"Message board","","",-1,-1,0,"/messageboard/messageboard.php?p=1&m=n&id=0","_blank","Message Board"]);
stm_aix("p0i11","p0i0",[0,"Laatste Nieuws","","",-1,-1,0,"/laatste_nieuws.php","_self","De laatste wijzigingen aan de W&L website"]);
stm_aix("p0i12","p0i0",[0,"Team Veltem Motors","","",-1,-1,0,"http://www.everyoneweb.com/teamveltemmotors","_self","De fansite van Team Veltem Motors"]);
stm_ep();
stm_em();
//-->
</script>
<!-- InstanceBeginEditable name="Floating Menu end" --><!-- InstanceEndEditable --> 
      &nbsp;&nbsp; <!-- InstanceBeginEditable name="counter" --><!-- InstanceEndEditable --> 
      <!-- InstanceBeginEditable name="Poll" --><!-- InstanceEndEditable --> <!-- InstanceBeginEditable name="wiens verjaardag is het" --><!-- InstanceEndEditable --> 
    </td>
    <td width="100%" valign="top">
	  <!-- InstanceBeginEditable name="general" --> 
      <?php
/**
 * PRINT_PLOEGEN
 * 
 * object     : print de informatie van alle competitieploegen voor het gegeven type
 * author     : Freek Ceymeulen
 * created    : 15/08/2005
 * parameters : $type : ploegtype [GEMENGD|DAMES|HEREN]
 *              $conn : connectie met de database
 **/
  function print_ploegen( $type, $conn, $seizoen )
  {
    echo "        <tr>\n";
    echo "          <td bgcolor=\"#666699\" class=\"geel\"> <a name=\"".strtolower($type)."\"></a><b>$type</b></td>\n";
    echo "        </tr>\n";
    
    // Zoek alle competitieploegen voor dit type
    $query = "SELECT id
                   , afdeling
                   , sporthal
                   , uur
                   , thumb_url
                   , image_url
                FROM bad_competitieploegen
               WHERE type = '%s'
			     AND seizoen = '%s'
               ORDER BY ploegnummer";
    $sql  = sprintf($query, mysql_real_escape_string($type)
	                      , mysql_real_escape_string($seizoen));
    $result = mysql_query($sql, $conn) or badm_mysql_die();
    while ($row = mysql_fetch_object($result))
    {
      // Zoek de ploegkapitein op
      $query = "SELECT s.naam
                     , s.adres
                     , s.postcode
                     , s.woonplaats
                     , IF (s.tel IS NULL, s.gsm, tel) AS tel
                     , s.email
                  FROM bad_spelers s
                     , clubfuncties f
                     , leden_functies lf
                 WHERE s.id = lf.spelers_id
                   AND f.id = lf.functies_id
                   AND f.functie = '%s'
                   AND lf.ref_id = %d";
      $sql  = sprintf($query, mysql_real_escape_string('KAPITEIN')
                            , $row->id);
      $result1 = mysql_query($sql, $conn) or badm_mysql_die();
      $row1 = mysql_fetch_object($result1);

      echo "        <tr>\n";
      echo "          <td><table width=\"100%\" border=\"1\" cellspacing=\"0\" cellpadding=\"2\">\n";
      echo "              <tr bordercolor=\"#CCCCCC\" bgcolor=\"#CCCCCC\">\n";
      echo "                <td colspan=\"3\" class=\"kleinetitel\"><strong><a name=\"".$row->id."\">".htmlspecialchars($row->afdeling)."</a></strong></td>\n";
      echo "              </tr>\n";
      echo "              <tr align=\"left\" bordercolor=\"#CCCCCC\">\n";
      echo "                <td width=\"30%\" valign=\"top\" class=\"kleinetekst\">Ploegkapitein: \n";
      echo "                  <br>\n";
      echo "                  ".htmlspecialchars($row1->naam)."<br>\n";
      echo "                  ".htmlspecialchars($row1->adres)."<br>\n";
      echo "                  ".$row1->postcode." ".htmlspecialchars($row1->woonplaats)."<br>\n";
      echo "                  tel: ".$row1->tel."<br>\n";
      // Ik bouw het e-mail adres op de client via Javascript, op die manier kunnen robots het niet herkennen en vermijden we spam.
      echo "<script type=\"text/javascript\" language=\"JavaScript\">\n";
      echo "<!--\n";
      echo " naam = '".htmlspecialchars(substr($row1->email, 0, strpos($row1->email, "@")))."';\n";
      echo " akrol = '@';\n";
      echo " domein = '".htmlspecialchars(substr($row1->email, strpos($row1->email, "@") + 1))."';\n";
      echo " document.write('<br> <a href=\"mailto:'+naam+akrol+domein+'\">E-mail</a>');\n";
      echo "-->\n";
      echo "</script>\n";
      echo "<noscript>\n";
      echo "Email adres afgeschermd mbv Javascript.<br>Laat Javascript toe voor contact.\n";
      echo "</noscript>\n";
      echo "                  <br> <br> </td>\n";
      echo "                <td width=\"35%\" valign=\"top\" class=\"kleinetekst\"> <p class=\"kleinetekst\">Thuiswedstrijden \n";
      echo "                    [<a href=\"plan1.htm".($row->sporthal == "Veltem"?"#veltem":"#kessello")."\" target=\"_blank\">plan en wegbeschrijving</a>]<br>\n";
      // Bepaal het adres van de sporthal
      if ($row->sporthal == 'Veltem')
      {
        echo "                    Ivo Van Dammezaal<br>\n";
        echo "                    Overstraat 21<br>\n";
        echo "                    3020 Veltem-Beisem<br>\n";
        echo "                    tel. 016/48.88.05</p>\n";
      }
      elseif ($row->sporthal == 'Kessel-Lo')
      {
        echo "                    Sporthal Kessel-lo<br>\n";
        echo "                    Stadionlaan 4<br>\n";
        echo "                    3010 Kessel-Lo<br>\n";
        echo "                    tel. 016/25.34.80</p>\n";
      }
      // Bepaal de link naar de uitslagen
 //     if (strpos($row->afdeling, 'Provinciale') === false)
 //     {
	    if ($row->afdeling == '1ste Nationale' && $type == 'GEMENGD')
		{
          $url = "http://www.toernooi.nl/sport/draw.aspx?id=9988F720-EB50-4603-884D-642DBAE4B8DE&draw=1";
		}
		elseif ($row->afdeling == '2de Nationale' && $type == 'GEMENGD')
		{
          $url = "http://www.toernooi.nl/sport/draw.aspx?id=D6BA8286-AD57-4EC8-A80C-6E6F8949708F&draw=5";
		}
		elseif ($row->afdeling == '2de Liga B' and $type == 'GEMENGD')
		{
          $url = "http://www.toernooi.nl/sport/draw.aspx?id=BFF38108-16C4-4E54-A162-32E71EB05580&draw=16";
		}
                elseif ($row->afdeling == '3de Provinciale' and $type == 'GEMENGD')
		{
          $url = "http://www.toernooi.nl/sport/draw.aspx?id=4CF3C2A8-2BAA-44EE-854F-E42F6F5241DF&draw=3";
		}
                elseif ($row->afdeling == '2de Provinciale' and $type == 'GEMENGD')
		{
          $url = "http://badmintonvlaanderen.toernooi.nl/sport/draw.aspx?id=1B8DDE36-BF06-4D3B-B055-8A27F2A98993&draw=2";
		}
                elseif ($row->afdeling == '1ste Provinciale' and $type == 'GEMENGD')
		{
          $url = "http://badmintonvlaanderen.toernooi.nl/sport/draw.aspx?id=78DC125B-C9A3-407E-A30B-EE6E6E5720DE&draw=2";
		}
                elseif ($row->afdeling == '5de Provinciale B' and $type == 'GEMENGD')
		{
          $url = "http://badmintonvlaanderen.toernooi.nl/sport/draw.aspx?id=78DC125B-C9A3-407E-A30B-EE6E6E5720DE&draw=8";
		}
		elseif ($row->afdeling == '1ste Liga' and $type == 'DAMES')
		{
          $url = "http://badmintonvlaanderen.toernooi.nl/sport/draw.aspx?id=BFF38108-16C4-4E54-A162-32E71EB05580&draw=8";
		}
		elseif ($row->afdeling == '3de Liga C' and $type == 'DAMES')
		{
          $url = "http://badmintonvlaanderen.toernooi.nl/sport/draw.aspx?id=BFF38108-16C4-4E54-A162-32E71EB05580&draw=13";
		}
	        elseif ($row->afdeling == '1ste Liga' and $type == 'HEREN')
		{
          $url = "http://badmintonvlaanderen.toernooi.nl/sport/draw.aspx?id=BFF38108-16C4-4E54-A162-32E71EB05580&draw=1";
		}
		elseif ($row->afdeling == '2de Liga B' and $type == 'HEREN')
		{
          $url = "http://badmintonvlaanderen.toernooi.nl/sport/draw.aspx?id=BFF38108-16C4-4E54-A162-32E71EB05580&draw=3";
		}
		elseif ($row->afdeling == '3de Liga C' and $type == 'HEREN')
		{
          $url = "http://www.toernooi.nl/sport/draw.aspx?id=FC8C6496-B6B3-4450-BA3A-78585EC61D2D&draw=6";
		}
		elseif ($row->afdeling == '1ste Provinciale' and $type == 'HEREN')
		{
          $url = "http://badmintonvlaanderen.toernooi.nl/sport/draw.aspx?id=343780FD-21F4-46B3-A393-56462FDBE9DC&draw=1";
		}
		elseif ($row->afdeling == '2de Provinciale A' and $type == 'HEREN')
		{
          $url = "http://badmintonvlaanderen.toernooi.nl/sport/draw.aspx?id=78DC125B-C9A3-407E-A30B-EE6E6E5720DE&draw=13";
		}
		elseif ($row->afdeling == '2de Provinciale B' and $type == 'HEREN')
		{
          $url = "http://badmintonvlaanderen.toernooi.nl/sport/draw.aspx?id=78DC125B-C9A3-407E-A30B-EE6E6E5720DE&draw=14";
		}
		elseif ($row->afdeling == '3de Provinciale A' and $type == 'HEREN')
		{
          $url = "http://badmintonvlaanderen.toernooi.nl/sport/draw.aspx?id=343780FD-21F4-46B3-A393-56462FDBE9DC&draw=4";
		}
		elseif ($row->afdeling == '3de Provinciale B' and $type == 'HEREN')
		{
          $url = "http://www.toernooi.nl/sport/draw.aspx?id=24881&draw=7";
		}
		elseif ($row->afdeling == '4de Provinciale C' and $type == 'HEREN')
		{
          $url = "http://badmintonvlaanderen.toernooi.nl/sport/draw.aspx?id=78DC125B-C9A3-407E-A30B-EE6E6E5720DE&draw=19";
		}
		elseif ($row->afdeling == '4de Provinciale B' and $type == 'GEMENGD')
		{
          $url = "http://badmintonvlaanderen.toernooi.nl/sport/draw.aspx?id=1B8DDE36-BF06-4D3B-B055-8A27F2A98993&draw=5";
		}
        //$link = "zie <abbr lang=\"nl\" title=\"Vlaamse Badminton Liga\">VBL</abbr>";
		$link = "zie toernooi.nl";
  //    }
 /**********
      else
      {
        $pp = substr($row->afdeling, 0, 1);
        if (!strpos($row->afdeling, 'Provinciale') === false)
        {
          $pp = $pp.trim(substr($row->afdeling, strpos($row->afdeling, 'Provinciale') + 11));
        }
        $url = "http://www.vvbbc.com/comp/poule.php?sz=".$seizoen."&ds=".substr($type, 0, 1)."&pp=".$pp;
        $link = "zie <abbr lang=\"nl\" title=\"Vereniging voor Vlaams-Brabantse Badminton Clubs\">VVBBC</abbr>";
      }
********************/ 

      echo "                  <p class=\"kleinetekst\">Resultaten: <a href=\"$url\" target=\"_blank\">$link</a></p></td>\n";
      // Toon een foto van de ploeg indien beschikbaar
      if (!is_null($row->thumb_url))
      {
        if (!is_null($row->image_url))
        {
          $img = "<a href=\"$row->image_url\" target=\_blank\" border=\"0\"><img src=\"$row->thumb_url\" alt=\"\"></a>\n";
        }
        else
        {
          $img = "<img src=\"$row->thumb_url\" alt=\"\">\n";
        }
      }
      else
      {
        $img = "&nbsp;";
      }
      
      echo "                <td width=\"35%\" align=\"center\" valign=\"middle\" class=\"kleinetekst\">$img</td>\n";
      echo "              </tr>\n";
      echo "            </table></td>\n";
      echo "        </tr>\n";
    }
  }
  
  // Bepaal huidig seizoen
  if (date("m") > 7)
  {
    $seizoen = date("Y")."-".(date("Y") + 1);
  }
  else
  {
    $seizoen = (date("Y") - 1)."-".date("Y");
  }

  // Maak connectie met de database
  require_once "functies/badm_db.inc.php";
  $badm_db = badm_conn_db();
?>
      <h1>Competitie <?php echo $seizoen; ?> </h1>
      <table width="75%" border="1" cellspacing="0" cellpadding="0">
        <tr align="center" valign="middle"> 
          <td width="25%"><p><a href="#dames">Dames</a></p></td>
          <td width="25%"><p><a href="#heren">Heren</a></p></td>
          <td width="25%"><p><a href="#gemengd">Gemengd</a></p></td>
        </tr>
      </table>
      <?php
  // Zoek het aantal competitieploegen per reeks
  $query = "SELECT COUNT(*) AS aantal
              FROM bad_competitieploegen
             WHERE type = '%s'
			   AND seizoen = '%s'";
  $sql  = sprintf($query, mysql_real_escape_string('GEMENGD')
                        , $seizoen);
  $result = mysql_query($sql, $badm_db) or badm_mysql_die();
  $row = mysql_fetch_object($result);
  $aantal_ploegen_gemengd = $row->aantal;
  $sql  = sprintf($query, mysql_real_escape_string('DAMES')
                        , $seizoen);
  $result = mysql_query($sql, $badm_db) or badm_mysql_die();
  $row = mysql_fetch_object($result);
  $aantal_ploegen_dames = $row->aantal;
  $sql  = sprintf($query, mysql_real_escape_string('HEREN')
                        , $seizoen);
  $result = mysql_query($sql, $badm_db) or badm_mysql_die();
  $row = mysql_fetch_object($result);
  $aantal_ploegen_heren = $row->aantal;
?>
      <p><span class="kleinetekst"><abbr lang="nl" title="Winksele en Leuven">W&amp;L</abbr> 
        heeft dit seizoen 
        <?=$aantal_ploegen_gemengd;?>
        ploegen in de gemengde competitie, 
        <?=$aantal_ploegen_dames;?>
        in de dames- en 
        <?=$aantal_ploegen_heren;?>
        in de herencompetitie. </span> </p>
      <?php
  // Zoek de competitieverantwoordelijke
  $query = "SELECT s.naam
                 , s.adres
                 , s.postcode
                 , s.woonplaats
                 , IF (s.tel IS NULL, s.gsm, s.tel) AS tel
                 , s.email
              FROM bad_spelers s
                 , clubfuncties f
                 , leden_functies lf
             WHERE s.id = lf.spelers_id
               AND f.id = lf.functies_id
               AND f.functie = 'COMPETITIE'";
  $result = mysql_query($query, $badm_db) or badm_mysql_die();
  $row = mysql_fetch_object($result);
?>
      <p class="kleinetekst"> De <strong>competitieverantwoordelijke</strong> 
        van <abbr title="Winksele en Leuven">W&amp;L</abbr> <abbr lang="nl" title="badmintonvereniging">bv</abbr> 
        is 
        <?=htmlspecialchars($row->naam);?>
        .<br>
        <?=htmlspecialchars($row->adres);?>
        , 
        <?=$row->postcode;?>
        <?=htmlspecialchars($row->woonplaats);?>
        , tel: 
        <?=$row->tel;?>
        , <a href="mailto:competitie@badmintonsport.be">E-mail</a> </p>
      <table width="95%" border="0" cellspacing="0" cellpadding="0">
        <?php
  print_ploegen('GEMENGD', $badm_db, $seizoen);
  print_ploegen('DAMES', $badm_db, $seizoen);
  print_ploegen('HEREN', $badm_db, $seizoen);
?>
      </table>
     
      <table width="80%" border="1" class="kleinetekst">
        <tr>
          <td><p class="kleinetekst"><strong>Veltem Vets</strong></p>
          <p class="kleinetekst">Ploegkapitein<br>
          <a href="mailto:reile@pandora.be">Reile Ramaekers</a></p></td>
          <td> <p class="kleinetekst">Onze veteranenploeg, de "Veltem Vets" spelen ook in 2015. Speeldata en resultaten vind je op <a href="http://badmintonvlaanderen.toernooi.nl/sport/tournament.aspx?id=3D47D08B-F928-4924-B0A1-ECE0C80B48DF" target="_blank">badmintonvlaanderen.toernooi.nl</a></p></td>
          <td>&nbsp;</td>
        </tr>
      </table>
      <p class="kleinetekst"><br>
        <br>
      </p>
	  <!-- InstanceEndEditable -->
	</td>
  </tr>
  <tr> 
    <td height="0"></td>
    <td></td>
  </tr>
  <tr> 
    <td height="1"><img src="http://www.badmintonsport.be/images/spacer.gif" alt="" width="150" height="1"></td>
    <td></td>
  </tr>
</table>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <!--DWLayoutTable-->
  <tr> 
    <td width="100%" height="50" valign="top" bgcolor="#FFFFFF">
	  <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#CCCCCC">
        <!--DWLayoutTable-->
        <tr> 
          <td align="center" bordercolor="#CCCCCC" bgcolor="#006600"><img src="http://www.badmintonsport.be/images/spacer.gif" width="1" height="4" alt=""></td>
        </tr>
        <tr> 
          <td width="100%" align="center" valign="middle" bordercolor="#CCCCCC" bgcolor="#CCCCCC" class="gmenu">
		    Last change: <!-- InstanceBeginEditable name="datum" -->
            <!-- #BeginDate format:It1 -->2-08-2011<!-- #EndDate -->
            <!-- InstanceEndEditable --> 
            E-mail: <a href="mailto:webmaster@badmintonsport.be">webmaster@badmintonsport.be</a></td>
        </tr>
        <tr>
          <td align="center" bordercolor="#CCCCCC" bgcolor="#006600" class="gmenu"><img src="http://www.badmintonsport.be/images/spacer.gif" width="1" height="4" alt=""></td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</body>
<!-- InstanceEnd --></html>
<?php
  log_website_usage($start_time, null, $badm_db);
?>