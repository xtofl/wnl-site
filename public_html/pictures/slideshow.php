<?php
  require_once "../functies/website_usage.php";
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="nl">
<head>
<title>Slideshow</title>
<style>
<!--
/* to not see scrollbars */
html {
    overflow: hidden;
}
-->
</style>
<?php
if (isset($_GET['width']) AND isset($_GET['height']))
{
  // get the browser width and height variables
  $max_width  = $_GET['width'];  //maximum width allowed for pictures
  $max_height = $_GET['height'];  //maximum height allowed for pictures
}
else
{
  // pass the browser width and height variables
  // (preserve original query string)
  echo "<script language='javascript'>\n";
  echo "  location.href=\"${_SERVER['SCRIPT_NAME']}?${_SERVER['QUERY_STRING']}"
           . "&width=\" + screen.width + \"&height=\" + screen.height;\n";
  echo "</script>\n";
  exit();
}

// Get directory to show pictures from
$dir = $_GET['dir'];

/* Get all the files in the specified directory */
  include "../functies/getDirectoryListing.php";
  $files = getDirectoryListing($dir, "a", 0, 0);
?>
<!-- disable image toolbar -->
<meta http-equiv="imagetoolbar" content="no">
<script language="JavaScript" type="text/javascript" src="http://www.badmintonsport.be/scripts/disableRightClick.js"></script>
<script language="Javascript" type="text/javascript">
<!--
var curImage = -1;
var numImages = <?php echo count($files); ?>;
var fotos = new Array();
<?php
//for ($i = 0; $i < count($files); $i++)
$i = 0;
foreach ($files as $photo)
{
  $image = $dir.'/'.$photo; //full path of image
  //$image = str_replace(' ','%20', $image); //replace spaces
  $imageSize = getimagesize($image); //get the actual size of the picture
  $width = $imageSize[0];  // get width of picture 
  $height = $imageSize[1];  // get height of picture
  //change height to fit in browser without changing proportions
  if ($height > $max_height && $width > $max_width)
  {
    $relHeight = $height / $max_height;
    $relWidth = $width / $max_width;
    if ($relHeight > $relWidth)
    {
      $height = $max_height;
      $width = $width / $relHeight;
    }
    else
    {
      $height = $height / $relWidth;
      $width = $max_width;
    }
  }
  elseif ($height > $max_height && $width < $max_width)
  {
    $height = $max_height;
    $width = $width * ($max_height / $height);
  }
  elseif ($height < $max_height && $width > $max_width)
  {
    $width = $max_width;
    $height = $height * ($max_width / $width);
  }

  echo "fotos[".$i."] = new Image(".round($width).",".round($height).");\n";
  //echo "fotos[".$i."].src = '".addslashes($dir).$files[$i]."';\n";
  echo "fotos[".$i."].src = '".$dir.'/'.$photo."';\n";

  if ($i == 0)
  {
    // Set display width and height for the first photo
    $startHeight = round($height);
    $startWidth  = round($width);
  }
  $i++;
}
?>
function swapPicture()
{
  //check if the browser will allow JavaScript to change an image
  if (document.images)
  {
    var nextImage = curImage + 1;
    if (nextImage >= numImages)
      nextImage = 0;
    // check if the nextImage image to be shown is ready (or has been loaded)
    if (fotos[nextImage] && fotos[nextImage].complete)
    {
      var target = null;
      //set the target where the next image is to be put into
      if (document.images.myImage)
        target = document.images.myImage;
      if (document.all && document.getElementById("myImage"))
        target = document.getElementById("myImage");
      //put the new image into the target placeholder
      target.src    = fotos[nextImage].src;
      target.height = fotos[nextImage].height;
      target.width  = fotos[nextImage].width;
      //update the counter to the currently showing picture
      curImage = nextImage;
      //set a time interval (in milliseconds) upon which to swap the image
      setTimeout("swapPicture()", 5000);
 
    }
    else
    {
      setTimeout("swapPicture()", 500);
    }
  }
}
//initiate the whole slideshow process
setTimeout("swapPicture()", 5000);

// disable right-click
if (document.layers) //Netscape
{
  document.captureEvents(Event.MOUSEDOWN);
}
document.onmousedown=noway;
//-->
</script>
</head>
<body bgcolor="black" scroll="no" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<table width="100%" height="100%">
 <tr>
  <td align="center" valign="center">
   <img  id="myImage" name="myImage" src="
<?php
 // Toon de eerste foto
 echo $dir.'/'.$files[0].'" height="'.$startHeight.'" width="'.$startWidth;
?>
" title="to stop: ALT+F4">
  </td>
 </tr>
</table>
</body>
<!-- Created by Freek Ceymeulen -->
</html>
<?php
  require_once "../functies/badm_db.inc.php";
  $badm_db = badm_conn_db();
  log_website_usage($start_time, $dir, $badm_db);
  mysql_close($badm_db);
?>