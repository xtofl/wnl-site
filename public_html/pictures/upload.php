<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html lang="nl"><!-- InstanceBegin template="/Templates/templatesub1php.dwt.php" codeOutsideHTMLIsLocked="false" -->
<?php
    // Connect to database
    require_once "/home/badmin/public_html/functies/badm_db.inc.php";
    $badm_db = badm_conn_db();
?>
<head>
<META HTTP-EQUIV="Content-Type"      CONTENT="text/html; charset=iso-8859-1">
<META NAME="robots"                  CONTENT="index,follow">
<META NAME="keywords"                CONTENT="badminton,batminton,w&amp;l,badmintonvereniging,club,badmintonclub,sport,Belgium,Veltem,Herent,Leuven,Winksele,Belgi�">
<META NAME="description"             CONTENT="W&amp;L bv : De grootste en leukste badmintonclub van Brabant">
<META NAME="abstract"                CONTENT="badminton, club">
<META NAME="Area"                    CONTENT="General">
<META NAME="Author"                  CONTENT="W&amp;L bv">
<META NAME="Copyright"               CONTENT="(c) 2003-2004 W&amp;L bv">
<META NAME="Creator"                 CONTENT="W&amp;L bv">
<META NAME="Generator"               CONTENT="W&amp;L bv">
<META NAME="Owner"                   CONTENT="W&ampL bv">
<META NAME="Publisher"               CONTENT="W&ampL bv">
<META NAME="Rating"                  CONTENT="General">
<META NAME="revisit-after"           CONTENT="7 Days">
<META NAME="revisit"                 CONTENT="7 Days">
<META NAME="document-classification" CONTENT="Internet">
<META NAME="document-type"           CONTENT="Public">
<META NAME="document-rating"         CONTENT="Safe for Kids">
<META NAME="document-distribution"   CONTENT="Global">
<META HTTP-EQUIV="Content-Type"      CONTENT="text/html; charset=iso-8859-1">
<META HTTP-EQUIV="Audience"          CONTENT="General">
<META HTTP-EQUIV="content-language"  CONTENT="NL">
<META HTTP-EQUIV="Pragma"            CONTENT="no-cache">
<META HTTP-EQUIV="Expires"           CONTENT="now">
<!-- InstanceBeginEditable name="doctitle" -->
<title>W&amp;L badmintonvereniging vzw - Foto Upload</title>
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
<link href="http://www.badmintonsport.be/css/badminton.css" rel="stylesheet" type="text/css">
<script type="text/javascript" language="JavaScript" src="http://www.badmintonsport.be/scripts/stm31.js"></script>
<script type="text/javascript" language="JavaScript" src="http://www.badmintonsport.be/scripts/dw_rotator.js"></script>
<script type="text/javascript" language="JavaScript">
<!--
function initRotator()
{
  var rotator1 = new dw_Rotator("sponsors_banner", 4000, "http://www.badmintonsport.be/images/", "_blank");
  rotator1.addImages("Veltem_Motors_banner.png", "china.jpg");
  rotator1.addActions("http://www.veltem-motors.be", "http://www.chinacapital.be");
  dw_Rotator.start();
}
function doLoad()
{
  // initialize the menu
  st_onload();
  // initialize the banner rotator
  initRotator();
}
//-->
</script>
</head>
<body class="badminton" onload="doLoad();">
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <!--DWLayoutTable-->
  <tr>
  <td colspan="2">
   <table width="100%" border="0" cellpadding="0" cellspacing="0">
    <!--DWLayoutTable-->
    <tr> 
     <td width="100%" style="background-color: #006600;"><img src="http://www.badmintonsport.be/images/pt_transp.gif" width="1px" height="4px" alt=""></td>
    </tr>
    <tr> 
     <td align="center" valign="top" bgcolor="#CCCCCC">
      <table width="100%" border="0" cellspacing="0" cellpadding="3">
       <tr align="center" valign="middle"> 
        <td rowspan="2" align="left">
	     <img src="http://www.badmintonsport.be/images/logow&amp;l-100.gif" width="100" height="80" alt="logo">
	    </td>
        <td>
	     <h4>W&amp;L Badmintonvereniging vzw<br>
	         Mechelsesteenweg 387<br>
             3020 Herent</h4>
		</td>
        <td align="right"><a href="javascript:void();" onclick="return dw_Rotator.doClick(0)"  onmouseover="dw_Rotator.pause(0)" onmouseout="dw_Rotator.resume(0)" onfocus="this.blur()"><img id="sponsors_banner" name="sponsors_banner" src="http://www.badmintonsport.be/images/Veltem_Motors_banner.png" alt="sponsors" width="320" height="100" border="0"></a></td>
       </tr>
      </table>
	 </td>
    </tr>
    <tr> 
     <td bgcolor="#336600"><img src="http://www.badmintonsport.be/images/pt_transp.gif" width="1px" height="4px" alt=""></td>
    </tr>
   </table>
  </td>
 </tr>
 <tr>
  <td width="150" height="213" valign="top">
<script type="text/javascript" language="JavaScript1.2">
<!--
stm_bm(["W&L menu",400,"","http://www.badmintonsport.be/scripts/blank.gif",0,"","",0,0,250,0,1000,1,0,0]);
stm_bp("p0",[1,4,0,0,1,4,0,7,100,"progid:DXImageTransform.Microsoft.Fade(overlap=.5,enabled=0,Duration=0.30)",-2,"progid:DXImageTransform.Microsoft.Pixelate(MaxSquare=15,enabled=0,Duration=0.50)",-2,50,0,0,"#669999","#006633","",3,0,0,"#ffffff"]);
stm_ai("p0i0",[0,"Home","","",-1,-1,0,"http://www.badmintonsport.be/index.php","_self","Startpagina","","","",0,0,0,"","",0,0,0,0,1,"#fffff7",0,"#b5bed6",0,"","",3,3,0,0,"#fffff7","#000000","#000000","#000000","10pt Verdana","10pt Verdana",0,0]);
stm_aix("p0i1","p0i0",[0,"Algemeen","","",-1,-1,0,"","_self","informatie over de club","","","",0,0,0,"http://www.badmintonsport.be/scripts/arrow_r.gif","http://www.badmintonsport.be/scripts/arrow_r.gif",7,7]);
stm_bpx("p1","p0",[1,2,0,0,1,4,0]);
stm_aix("p1i0","p0i0",[0,"Bestuur","","",-1,-1,0,"http://www.badmintonsport.be/algemeen/bestuur.php","_self","Bestuursleden","","","",0,0,0,"","",0,0,0,1]);
stm_bpx("p1","p0",[1,2,0,0,1,4,0,0]);
stm_aix("p2i0","p1i0",[0,"Mededelingen","","",-1,-1,0,"http://www.badmintonsport.be/algemeen/Bestuur/bestuursmededelingen.php","_self","Bestuursmededelingen"]);
stm_ep();
stm_aix("p1i1","p0i0",[0,"Medewerkers","","",-1,-1,0,"http://www.badmintonsport.be/algemeen/medewerkers.php","_self","Medewerkers","","","",0,0,0,"","",0,0,0,1]);
stm_aix("p1i2","p0i0",[0,"Speeluren","","",-1,-1,0,"http://www.badmintonsport.be/algemeen/speeluren.php","_self","Speeluren","","","",0,0,0,"","",0,0,0,1]);
stm_aix("p1i3","p0i1",[0,"Geschiedenis","","",-1,-1,0,"","_self","Geschiedenis","","","",0,0,0,"","",0,0,0,1]);
stm_bpx("p2","p0",[1,2,0,0,1,4,0,0]);
stm_aix("p2i0","p1i0",[0,"Clubkampioenen","","",-1,-1,0,"http://www.badmintonsport.be/algemeen/Geschiedenis/clubkampioenen.php","_self","Winnaars Clubkampioenschap"]);
stm_aix("p2i1","p1i0",[0,"Nationale Kampioenen","","",-1,-1,0,"http://www.badmintonsport.be/algemeen/Geschiedenis/nationalekampioenen.php","_self","Nationale Kampioenen W&L"]);
stm_ep();
stm_aix("p1i4","p0i0",[0,"Sponsors","","",-1,-1,0,"http://www.badmintonsport.be/algemeen/sponsors.php","_self","Sponsors","","","",0,0,0,"","",0,0,0,1]);
stm_aix("p1i5","p0i0",[0,"Artikels","","",-1,-1,0,"http://www.badmintonsport.be/algemeen/artikels.php","_self","Artikels over de club","","","",0,0,0,"","",0,0,0,1]);
stm_ep();
stm_aix("p0i2","p0i0",[0,"Competitie","","",-1,-1,0,"http://www.badmintonsport.be/competitie.php","_self","Competitie: ploegen en uitslagen"]);
stm_aix("p0i3","p0i1",[0,"Tornooien","","",-1,-1,0,"http://www.badmintonsport.be/tornooien/tornooioverzicht.php","_self","Tornooien"]);
stm_bpx("p1","p0",[1,2,0,0,1,4,0]);
stm_aix("p1i0","p0i1",[0,"Inschrijvingen","","",-1,-1,0,"_self","Schrijf je hier in voor tornooien"]);
stm_bpx("p1","p0",[1,2,0,0,1,4,0,0]);
//-->
</script>
<?php
// create SQL statement 
   $sql = "SELECT id
                , organisatie
                , doelgroep
                , DATE_FORMAT(start_dt, '%d/%m') AS start_dt
                , verzonden
           FROM inschr_torn
		   WHERE DATE_ADD(dt_inschr, INTERVAL 21 DAY ) > NOW()
		   ORDER BY dt_inschr DESC";
   $result = mysql_query ($sql, $badm_db) or badm_mysql_die();
// format results by row
   $teller = 0;
   echo "<script type=\"text/javascript\" language=\"JavaScript1.2\">\n";
   while ($badm = mysql_fetch_object($result))
   {
      echo "stm_aix(\"p2i";
      echo $teller;
      echo "\",\"p1i0\",[1,\"";
      echo $badm->organisatie;
      if ($badm->doelgroep == 'J' || $badm->doelgroep == 'V')
      {
        echo $badm->doelgroep == 'J' ? " (Jeugd)" : " (Veteranen)";
        if ($badm->verzonden == 0) //inschrijvingsperiode nog niet afgelopen
        {
          echo " <B>".$badm->start_dt."</B>";
        }
        else
        {
          echo "  <IMG src=http://www.badmintonsport.be/images/closed.png>";
        }
      }
      else
      {
        if ($badm->verzonden == 0) //inschrijvingsperiode nog niet afgelopen
        {
          echo " <B>".$badm->start_dt."</B>";
        }
        else
        {
          echo "  <IMG src=http://www.badmintonsport.be/images/closed.png>";
        }
      }
      echo "\",\"\",\"\",-1,-1,0,\"http://www.badmintonsport.be/tornooien/inschrijvingen.php?id=";
      echo $badm->id;
      echo "\",\"_self\",\"Inschrijving ";
      echo $badm->organisatie;
      echo "\"]);\n";
      $teller = $teller + 1;
   }
   echo"</script>\n";
?>
<script type="text/javascript" language="JavaScript1.2">
<!--
stm_ep();
stm_aix("p1i1","p0i0",[0,"Aanvangsuren","","",-1,-1,0,"http://www.badmintonsport.be/tornooien/aanvangsuren.php","_self","Aanvangsuren tornooien"]);
stm_aix("p1i2","p0i0",[0,"Kalender","","",-1,-1,0,"http://www.badmintonsport.be/docs/tornooikalender.pdf","_blank","Tornooikalender"]);
stm_aix("p1i3","p0i1",[0,"Resultaten","","",-1,-1,0,"","_self","Tornooiresultaten W&L spelers"]);
stm_bpx("p2","p0",[1,2,0,0,1,4,0,0]);
//-->
</script>
<?php
// create SQL statement
    $sql = "SELECT  DISTINCT IF(DATE_FORMAT(dt_van, '%m') < 8
, DATE_FORMAT(dt_van, '%Y') - 1
, DATE_FORMAT(dt_van, '%Y')) AS start
, IF(DATE_FORMAT(dt_van, '%m') < 8
, DATE_FORMAT(dt_van, '%Y')
, DATE_FORMAT(dt_van, '%Y') + 1) AS einde
           FROM bad_tornooien
           ORDER  BY start DESC ";
   $result = mysql_query ($sql, $badm_db) or badm_mysql_die();
// format results by row
   $teller = 0;
   echo "<script type=\"text/javascript\" language=\"JavaScript\">\n";
   while ($badm = mysql_fetch_object($result))
   {
      echo "stm_aix(\"p2i".$teller."\",\"p1i0\",[0,\"Seizoen ".$badm->start."-".$badm->einde."\",\"\",\"\",-1,-1,0,\"http://www.badmintonsport.be/tornooien/resultaten.php?s=".$badm->einde."\",\"_self\",\"Seizoen ".$badm->start."-".$badm->einde."\"]);\n";
      $teller = $teller + 1;
   }
   echo"</script>\n";
   //mysql_free_result($result);
   mysql_close($badm_db);
?>
<script type="text/javascript" language="JavaScript">
<!--stm_ep();
stm_ep();
stm_ep();
stm_aix("p0i4","p0i0",[0,"Spelers","","",-1,-1,0,"http://www.badmintonsport.be/spelers.php","_self","Spelersprofielen en statistieken"]);
stm_aix("p0i5","p0i1",[0,"Jeugd","","",-1,-1,0,"","_self","Informatie over en voor de jeugdspelers"]);
stm_bpx("p1","p0",[1,2,0,0,1,4,0]);
stm_aix("p1i0","p0i0",[0,"Algemeen","","",-1,-1,0,"http://www.badmintonsport.be/jeugd/jeugd.php","_self","Jeugd trainers"]);
stm_aix("p1i1","p0i0",[0,"Teams","","",-1,-1,0,"http://www.badmintonsport.be/jeugd/jeugdteams.php","_self","Jeugdteams"]);
stm_aix("p1i2","p0i0",[0,"J-team","","",-1,-1,0,"http://www.badmintonsport.be/jeugd/J-team.php","_self","J-team"]);
stm_aix("p1i3","p0i0",[0,"Resultaten","","",-1,-1,0,"http://www.badmintonsport.be/jeugd/resultaten_jeugd.php?s=2008","_self","Resultaten jeugtornooien W&L spelers"]);
stm_aix("p1i4","p0i0",[0,"Jeugdtornooi","","",-1,-1,0,"http://www.badmintonsport.be/jeugd/jeugdtornooi.php","_self","Jeugdtornooi W&L"]);
stm_ep();
stm_aix("p0i6","p0i0",[0,"Recreanten","","",-1,-1,0,"http://www.badmintonsport.be/recreant.php","_self","Recreanten"]);
stm_aix("p0i7","p0i1",[0,"Foto's","","",-1,-1,0,"http://users.pandora.be/ikkegg","_blank","badminton foto's"]);
//-->
</script>
<?php
/*************************************************************************************************
 * Create a link to every photo directory that exists
 ************************************************************************************************/
/* Get the root dirctory of the website */
  $rootdir = $_SERVER['DOCUMENT_ROOT'];
/* directory where all the photo's are located in subdirectories */
  $dir = $rootdir."/pictures";
/* Get all the subdirectories of the pictures directory */
  include $rootdir."/functies/getDirectoryListing.php";
  $subdirs = getDirectoryListing($dir, "a", 1, 0, "none");
/* Number of subdirs in directory */
  $subdir_cnt = count($subdirs);
/* Display a link to every subdirectory */
  for ($i = 0; $i < $subdir_cnt; $i++)
  {
    echo "<script type=\"text/javascript\" language=\"JavaScript\">\n";
    if ($i == 0) // first subdirectory
    {
       echo "stm_bpx(\"p1\",\"p0\",[1,2,0,0,1,4,0]);\n";
    }
		echo "stm_aix(\"p0i".$i."\",\"p0i0\",[0,\"".$subdirs[$i]."\",\"\",\"\",-1,-1,0,\"http://www.badmintonsport.be/pictures/showThumbnails.php?dir=".$subdirs[$i]."\",\"_self\",\"".$subdirs[$i]."\"]);\n";
    if ($i == $subdir_cnt - 1) // last subdirectory
    {
      echo "stm_ep();\n";
    }
    echo"</script>\n";
  }
?>
<script type="text/javascript" language="JavaScript">
<!--
stm_ai("p0i8",[6,4,"#006633","",0,0,0]);
stm_aix("p0i9","p0i0",[0,"Links","","",-1,-1,0,"http://www.badmintonsport.be/links.php","_self","Links"]);
stm_aix("p0i10","p0i0",[0,"Message Board","","",-1,-1,0,"http://www.badmintonsport.be/messageboard/messageboard.php?p=1&m=n&id=0","_blank","Message Board"]);
stm_aix("p0i11","p0i0",[0,"Laatste Nieuws","","",-1,-1,0,"http://www.badmintonsport.be/laatste_nieuws.php","_self","De laatste wijzigingen aan de W&L website"]);
stm_aix("p0i12","p0i0",[0,"Team Veltem Motors","","",-1,-1,0,"http://www.everyoneweb.com/teamveltemmotors","_self","De fansite van Team Veltem Motors"]);
stm_ep();
stm_em();
//-->
</script>
      &nbsp;
 <!-- InstanceBeginEditable name="counter" -->
<!-- InstanceEndEditable -->
       <!-- InstanceBeginEditable name="wiens verjaardag is het" -->
<!-- InstanceEndEditable -->
  </td>
  <td width="100%" valign="top">
<!-- InstanceBeginEditable name="general" -->
<?php

/***************************************
 * notify_about_new_pictures
 **************************************/
 function notify_about_new_pictures($user, $dirname)
 {
   // Connect
   require_once "../functies/badm_db.inc.php";
   $badm_db = badm_conn_db();
   // Check that the item is not yet added to the RSS feed
   $query = "SELECT id FROM rss WHERE title = '%s'";
   $sql  = sprintf($query, mysql_real_escape_string($dirname) );
   $result = mysql_query($sql, $badm_db) or badm_mysql_die();
   $rss = mysql_fetch_object($result);
   if (mysql_num_rows($result) == 0)
   {
     $stmt = "INSERT INTO rss ( title, link, description )
	          VALUES ( '%s', '%s', '%s' )";
	 $sql  = sprintf($stmt, mysql_real_escape_string($dirname)
	                      , mysql_real_escape_string("http://www.badmintonsport.be/pictures/showThumbnails.php?dir=".$dirname)
						  , mysql_real_escape_string("New pictures added by ".$user) );
	 $result = mysql_query($sql, $badm_db) or badm_mysql_die();
   }
 }

/***************************************
 * ask_boxes
 **************************************/
 function ask_boxes( $user, $dirname )
 {
   echo '<form name="form1" method="post" action="'.$_SERVER['PHP_SELF'].'">';
   if ($dirname == 'DUMMY')
   {
     echo " <p>Je kan meerdere foto's tegelijk opladen.  Ik stel voor dat je de eerste keer begint";
     echo ' met 1 foto, dan krijg je een idee van hoe lang het duurt om 1 foto op te laden.</P>';
   }
   echo " <p>Hoeveel foto's wil je tegelijk opladen?</P>";
   echo ' <input name="uploadNeed" type="text" id="uploadNeed" size="2" maxlength="2"> (maximum 10)</p>';
   echo ' <input type="hidden" name="username" value="'.$user.'">';
   echo ' <input type="hidden" name="action" value="1">';
   // Deze parameter bevat de naam van de directory indien deze gekend is
   echo ' <input type="hidden" name="dir" value="'.$dirname.'">';
   echo ' <BR><input type="submit" name="Submit" value="Verder">';
   if ($dirname != 'DUMMY')
   {
     echo " <input type=\"button\" value=\"Stop en toon foto's\" onClick=\"location.href = 'http://www.badmintonsport.be/pictures/showThumbnails.php?dir=".$dirname."'\">";
	 notify_about_new_pictures($user, $dirname);
   }
   echo '</form>';
 } // end ask_boxes
/***************************************
 * show_upload_form
 **************************************/
 function show_upload_form( $user, $uploadNeed, $dirname )
 {
   echo "<script language=\"JavaScript\" type=\"text/JavaScript\">\n";
   echo "<!--\n";
   echo "function doValidate()
         {
           if (document.form1.dir.value == \"\")
           {
              alert(\"Je hebt nog geen naam ingevuld voor deze reeks foto's\");
              document.form1.dir.focus();
           }
           else
           {
              document.form1.submit();
           }
         }\n";
   echo "//-->\n";
   echo "</script>\n";
  // uploadNeed must be in range 1-10
   if ($uploadNeed < 1)
   {
     $uploadNeed = 1;
   }
   if ($uploadNeed > 11)
   {
     $uploadNeed = 10;
   }
   echo '<form name="form1" enctype="multipart/form-data" method="post" action="'.$_SERVER['PHP_SELF'].'">';
   echo ' <input type="hidden" name="MAX_FILE_SIZE" value="200000" />';
   echo "<P>Geef hier een naam voor je reeks foto's.  Onder deze naam komen de foto's in de menu van de website.";
   echo "<BR>(Gebruik geen speciale karakters zoals \\ / & . , @ < > % ? + : * ' \" � !). Spaties zijn toegestaan.</P>";
   if ($dirname == 'DUMMY')
   {
      $disable = '';
      $dirname = '';
   }
   else
   {
      /* $disable = ' DISABLED'; disabled elements are not submitted */;
   }
   echo ' <input type="text" name="dir" size="30" maxlength="30" value="'.$dirname.'"'.$disable.'>';

   echo "<P>Richtlijnen:<BR>
         <UL>
     <LI>Zorg ervoor dat je foto's niet op hun zij liggen.</LI>
     <LI>Geef je foto's een beschrijvende naam. &nbsp; De foto's worden getoond in volgorde van hun
         naam, dus als je wilt dat je foto's in chronologische volgorde verschijnen op de website
         begin je best de naam van de foto met 001, 002 enz... in de gewenste volgorde.</LI>
     <LI>De ideale foto past net op een volledig scherm. &nbsp; Ongeveer 1280 op 1024 pixels.</LI>
     <LI>Foto's moeten minstens 10 kB groot zijn en maximum 200 kB.</LI>
     <LI>Let erop dat je geen twee keer dezelfde foto oplaadt.</LI>
     <LI>Enkel foto's van het type JPEG, GIF, en PNG zijn toegestaan.</LI>
     <LI>De upload snelheid is o.a. afhankelijk van je internetverbinding. &nbsp; Het kan dus best wel
         even duren vooraleer je foto's opgeladen zijn, zeker als je er 10 tegelijk oplaadt.
         Heb dus geduld, je krijgt zeker een bevestiging zo gauw de upload gebeurd is.</LI>
     <LI>Onmiddellijk na het opladen zijn je foto's zichtbaar op de website.</LI>
     <LI>Mocht je problemen ondervinden gelieve dat dan aan de webmaster te melden samen met
         de eventuele foutmelding(en).</LI></UL></P>";
   echo '<P>Klik op de Browse knop om je foto(s) te selecteren.</P>';
  // start of dynamic form
   for($x=0; $x<$uploadNeed; $x++)
   {
     echo ' <input name="uploadFile'.$x.'" type="file" id="uploadFile'.$x.'" size="100">';
   }

   echo ' <input name="uploadNeed" type="hidden" value="'.$uploadNeed.'">';
   echo ' <input type="hidden" name="username" value="'.$user.'">';
   echo ' <input type="hidden" name="action" value="2">';
   echo ' <BR><input type="button" name="Submit" value="Opladen" onClick="doValidate();">';
   echo '</form>';
 } // end show_upload_form

/***************************************
 * copy_files
 **************************************/
 function copy_files( $user, $uploaddir, $uploadNeed )
 {
    $uploaddirpath = '/home/badmin/public_html/pictures';

  // Check directory name for unpermitted characters
   if (ereg('[\&.,@<>%?+:*"�!]', $uploaddir))
   {
     echo "<P>Your directory name contains a special character that is not allowed</P>";
   }
   else
   {
     if (!file_exists($uploaddir) && !is_dir($uploaddir))
     {
       if (substr($uploaddir, 0, 1) != '/')
       {
         $uploaddir = "/$uploaddir"; 
       }
       if (!mkdir($uploaddirpath.$uploaddir, 0755))
       {
         echo "<p>Could not create directory $uploaddir</p>\n";
       }
       else // directory is created
       {
        // Give this map the necessary privileges to upload photo's to
         chmod($uploaddirpath.$uploaddir, 0777);
		 $uploaddir = substr($uploaddir, 1);
       }
     } // end dir already exists
   } // end directory check

  // start for loop
   for($x=0; $x<$uploadNeed; $x++)
   {
     $file = $_FILES['uploadFile'.$x]['name'];
     $filesize = $_FILES['uploadFile'.$x]['size'];
	 $errors = $_FILES['uploadFile'.$x]['error'];

	 if ($errors == UPLOAD_ERR_INI_SIZE)
	 {
	   echo "<p>Foto $file is te groot.  De server laat max. 2MB toe.</p>\n";
	 }
     elseif ($errors == UPLOAD_ERR_FORM_SIZE)
     {
       echo "<p>Foto ".$file." is te groot.  Je foto mag max. 200kB groot zijn.</p>\n";
     }
     elseif (strlen($file) == 0)
     {
       // Geen foto ingevuld, doe niets
       ;
     }
     elseif ($filesize > 200000)
     {
       echo "<p>Foto ".$file." is te groot.  (filesize: ".$filesize." bytes)</p>\n";
     }
     elseif ($filesize < 10000)
     {
       echo "<p>Foto $file is te klein. (filesize: $filesize bytes)</p>\n";
     }
     else
     {

      // Check file extensions
       $dot_pos = strrpos($file, "."); //position of last occurence of . in filename
       if ($dot_pos != FALSE)
       {
         $ext = strtoupper(substr($file, $dot_pos + 1));
         if ($ext = 'JPG' || $ext = 'JPEG' || $ext = 'GIF' || $ext = 'PNG')
         {

          // Check that the file is an image
           $result_array = getimagesize($_FILES['uploadFile'.$x]['tmp_name']);
           if($result_array != FALSE)
           {
             $mime_type = $result_array['mime'];
             switch ($mime_type)
             {
               case "image/jpeg" : ;//echo "file is jpeg type";
                 break;
               case "image/gif"  : ;//echo "file is gif type";
                 break;
               //case "image/bmp"  : echo "file is bitmap type";
                 //break;
               case "image/png"  : ;//echo "file is png type";
                 break;
               default: echo "<P>Photo $file is an image, but not of a supported type: $mime_type.
                              Supported types are: jpeg, jpg, gif, png</P>";
                 break;
             }

            // strip file_name of slashes
             $file = stripslashes($file);
             $file = str_replace("'", "", $file);

             $file_name = $uploaddirpath.'/'.$uploaddir.'/'.$file;

            // Copy
             if (move_uploaded_file($_FILES['uploadFile'.$x]['tmp_name'], $file_name))
             {
               echo "<P>Foto $file uploaded successfully!</P>";
             }
             else
             {
               echo "<P>Photo $file could not be uploaded!</P>";
              /* Display debug info
               print "<pre>";
               print_r($_FILES);
               print "</pre>";*/
             }
           }
           else
           {
              echo "<P>File $file is not a valid image file.</P>";
           }
         }
         else // extension not in ('jpg', 'gif', 'jpeg', 'bmp')
         {
           echo "<P>Photo $file has not a valid extension: $ext</P>";
         }
       }
       else // no . found in filename
       {
         echo "<P>Photo $file has no extension.</P>";
       }
     } // end size check
   } // end for loop
   echo "<p>Wens je nog meer foto's toe te voegen aan $uploaddir?</p>";
   ask_boxes($user, $uploaddir);
 } // end copy_files

/***************************************
 * check_login
 **************************************/
 function check_login($user)
 {
   require_once "../functies/badm_db.inc.php";
   $badm_db = badm_conn_db();

   // Check if password is correct
   // and if user has rights to upload photo's
   $query = "SELECT level FROM users WHERE usid = '%s' AND paswoord = '%s'";
   $sql  = sprintf($query, mysql_real_escape_string($user)
                         , mysql_real_escape_string($_POST["password"]) );
   $result = mysql_query($sql, $badm_db) or badm_mysql_die();
   $login = mysql_fetch_object($result);
   if (!mysql_num_rows($result) == 1) 
   {
     // username+password zijn ingegeven maar kloppen niet !
     echo "Wrong username / password<br>";
     echo '<A HREF="javascript:history.go(-1)">[Go Back]</A>';
   }
   else //if(mysql_num_rows($result) == 1) 
   {
     if ($login->level < 2)
     {
       echo "Je hebt blijkbaar niet de nodige bevoegdheden om foto's op de website te zetten.<BR>";
       echo "Heb je foto's die je graag op de site wil zetten, vraag dan toestemming aan de webmaster.";
     }
     else
     {
       process($user);
     } // end level check
   } // end password check
 } // end check login

/***************************************
 * process
 **************************************/
 function process($user)
 {
   if (!isset($_POST['action']))
   {
     // Show first screen
     ask_boxes($user, 'DUMMY');
   }
   else
   {
     $action = $_POST['action'];
     switch ($action)
     {
       case "1" :
         show_upload_form($user, $_POST['uploadNeed'], $_POST['dir']);
         break;
       case "2" :
         copy_files($user, $_POST['dir'], $_POST['uploadNeed']);
         break;
       default :
        echo "Problem: no action specified.";
        echo '<A HREF="javascript:history.go(-1)">[Go Back]</A>';
        exit;
        break;
     }
   } // end action
 } // end process
/*********
 * BEGIN *
 *********/
?>

<table width="99%" height="99%">
 <tr>
  <td align="left" valign="middle">
   <h1>Foto's opladen naar de website</h1>

<?php
 if (!isset($_POST['username']))
 {
   echo "Jonge, je bent hier op een verkeerde manier gekomen";
   exit;
 }
 else
 {
   $gebruikersnaam = $_POST['username'];
 }
 if (isset($_POST['password']))
 {
   if ($_POST['password'] != "")
   {
     check_login($gebruikersnaam);
   }
 }
 else
 {
   process($gebruikersnaam);
 }
?>

  </td>
 </tr>
</table>
<!-- InstanceEndEditable -->
  </td>
 </tr>
 <tr>
  <td height="0"></td>
  <td></td>
 </tr>
 <tr>
  <td height="1"><img src="http://www.badmintonsport.be/images/spacer.gif" alt="" width="150" height="1"></td>
  <td></td>
 </tr>
</table>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <!--DWLayoutTable-->
 <tr>
  <td width="100%" height="50" valign="top" bgcolor="#FFFFFF">
   <table width="100%" border="0" cellpadding="0" cellspacing="0" style="border-color: #CCCCCC" bgcolor="#CCCCCC">
       <!--DWLayoutTable-->
    <tr>
     <td align="center" valign="middle" style="border-color: #CCCCCC" bgcolor="#006600">
         <img src="http://www.badmintonsport.be/images/spacer.gif" width="200" height="4" alt=""></td>
    </tr>
    <tr>
     <td width="100%" align="center" valign="middle" style="border-color: #CCCCCC" bgcolor="#CCCCCC" class="gmenu">Last change:
 <!-- InstanceBeginEditable name="datum" -->09-10-2004
 <!-- InstanceEndEditable -->
             E-mail: <a href="mailto:webmaster@badmintonsport.be">webmaster@badmintonsport.be</a></td>
    </tr>
    <tr>
     <td align="center" valign="middle" style="border-color: #CCCCCC" bgcolor="#006600" class="gmenu">
          <img src="http://www.badmintonsport.be/images/spacer.gif" width="200" height="4" alt=""></td>
    </tr>
   </table>
  </td>
 </tr>
</table>
</body>
<!-- InstanceEnd --></html>