<?php
  require_once "../functies/website_usage.php";
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="nl">
<head>
  <title>W&amp;L Badmintonvereniging vzw - Pictures</title>
  <style type="text/css">
  <!--
  img {
      border : none;
}
  .visited {
      filter : alpha(Opacity=99
                       ,FinishOpacity=60
                       ,style=1
                       ,StartX=0
                       ,FinishX=2
                       ,StartY=0
                       ,FinishY=2);
}
  -->
  </style>
<?php
if (isset($_GET['width']) AND isset($_GET['height']))
{
  // get the browser width and height variables
  $max_width  = $_GET['width'];  //maximum width allowed for pictures
  $max_height = $_GET['height'];  //maximum height allowed for pictures
}
else
{
  // pass the browser width and heigth variables
  // (preserve original query string)
  echo "<script language=\"javascript\">\n";
  echo "  location.href=\"${_SERVER['SCRIPT_NAME']}?${_SERVER['QUERY_STRING']}"
           . "&width=\" + screen.width + \"&height=\" + screen.height;\n";
  echo "</script>\n";
  exit();
}
?>
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
  <link href="../css/badminton.css" rel="stylesheet" type="text/css">
  <script src="../scripts/stm31.js"></script>
  <script type="text/javascript" src="../scripts/floating.js"></script>
</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<table width="100%" border="0" cellpadding="0" cellspacing="0" bordercolor="#CCCCCC" bgcolor="#FFFFFF">
 <tr> 
  <td align="center" valign="middle" bgcolor="#006600">
   <img src="../images/spacer.gif" width="200" height="4"></td>
 </tr>
 <tr> 
  <td align="center" valign="middle" bgcolor="#CCCCCC">
   <img src="../images/pt_transp.gif" width="500" height="5"></td>
 </tr>
 <tr>
  <td align="center" valign="top" bgcolor="#CCCCCC">
   <table width="100%" border="0" cellspacing="0" cellpadding="3">
    <tr align="center" valign="middle"> 
     <td rowspan="2" align="left">
      <img src="../images/logow&l-100.gif" width="100" height="80"></td>
     <td>&nbsp;</td>
     <td><h4>W&amp;L Badmintonvereniging vzw, Mechelsesteenweg 
                          387, 3020 Herent</h4></td>
    </tr>
    <tr align="center" valign="middle"> 
     <td>&nbsp;</td>
     <td><img src="../images/chinas.gif" width="400" height="60"></td>
    </tr>
   </table>
  </td>
 </tr>
 <tr> 
  <td align="center" valign="middle" bgcolor="#CCCCCC"><img src="../images/pt_transp.gif" width="500" height="5"></td>
 </tr>
 <tr> 
  <td align="center" valign="middle" bgcolor="#336600"><img src="../images/spacer.gif" width="200" height="4"></td>
 </tr>
</table>
<TABLE width="100%" border="0" cellpadding="0" cellspacing="0">
 <TR>
  <TD width="150" valign="top">
<!-- deze div dient voor de floating menu -->
  <DIV id="divStayTopLeft" style="position:absolute; visibility:inherit;">
<?php
   // need to call this before doing any other db action because a connection is made in this file
   require "../templates/template.php";
   invoegen_template(); //menu

/* Get the directory where the pictures reside */
  $dir = $_GET['dir'];
  //echo $dir;
?>
   <BR>
   <P>Klik hier voor een <A href="#" onclick="window.open('slideshow.php?dir=<?php echo $dir; ?>','','fullscreen=yes,menubar=no');" title="to stop slideshow: ALT+F4">slideshow</A>.</P>
   <BR>
   Foto's toevoegen?<BR>
   <table cellspacing="2">
   <form name="login" action="upload.php" method="post">
    <tr class="kleinetekst">
     <td>Username :</td>
     <td><input type="text" name="username" size="7" maxlength="25"></td>
     </tr>
    <tr class="kleinetekst">
     <td>Paswoord :</td>
     <td><input type="password" name="password" size="7" maxlength="25"></td>
    </tr>
    <tr>
     <td><input type="submit" value="login"></td>
    </tr>
   </form>
   </table>
  </DIV>
<script>
<!--
// dit zorgt ervoor dat de div met id divStayTopLeft (zijnde de menu) mee naar beneden beweegt bij het scrollen
JSFX_FloatTopDiv();
-->
</script>

  </TD>
<?php
/* Get the current directory */
  //$CurrentDir = getcwd();

/* Get all the files in the specified directory */
  // function is already included via the template
  //include "../functies/getDirectoryListing.php";
  $files = getDirectoryListing($dir, "a", 0, 0);
  //echo "This directory contains following files:<br><br>";
  //print_r($files);
/* Number of files in directory */
  $pic_cnt = count($files);
  //echo $pic_cnt;

/* Width and height of thumnails */
  $thn_width = 150;

/* Number of thumbnails on per row */
  $thn_row = ceil(($max_width - 300) / $thn_width);
  //echo $thn_row;
?>
  <td height="500" valign="top">
   <table cellPadding="0" width="98%" border="0" cellspacing="4" bgcolor="#f1f1f1">
    <tr>
     <td colspan="<?php echo $thn_row; ?>" align="center"><h1><?php echo $dir; ?></h1></td>
    </tr>
    <tr>
<?php

//for ($i = 0; $i < count($files); $i++)
$i = 0;
foreach ($files as $photo)
{
  echo "<td align=\"center\"><a href=\"javascript:void(0);\" onClick=\"window.open('phpThumb.demo.showpic.php?src=".$dir.'/'.$photo."&title=".$photo."','','width=1024,height=768,resizable=no,status=no,menubar=no,toolbar=no,scrollbars=no');\"><img src=\"phpThumb.php?src=".$dir.'/'.$photo."&w=150&h=150\" onMouseUp=\"className='visited'\" alt=\"".$photo."\"></a></td>";

  if (($i + 1) % $thn_row == 0)  //deling geeft geen rest
  {
    // new row
    echo "</tr><tr>";
  }
  $i++;
};

?>
<td>&nbsp;</td>
    </tr>
   </table>
  </td>
 </tr>
</table>
<table width="100%" border="0" cellpadding="0" cellspacing="0" bordercolor="#CCCCCC" bgcolor="#CCCCCC">
 <tr> 
  <td align="center" valign="middle" bordercolor="#CCCCCC" bgcolor="#006600">
   <img src="../images/spacer.gif" width="200" height="4"></td>
 </tr>
 <tr> 
  <td width="100%" align="center" valign="middle" bordercolor="#CCCCCC" bgcolor="#CCCCCC" class="gmenu">Last 
      change: 10-05-2006 E-mail: <a href="mailto:webmaster@badmintonsport.be">webmaster@badmintonsport.be</a></td>
 </tr>
 <tr>
  <td align="center" valign="middle" bordercolor="#CCCCCC" bgcolor="#006600" class="gmenu">
   <img src="../images/spacer.gif" width="200" height="4"></td>
 </tr>
</table>
</body>
</html>
<?php
  require_once "../functies/badm_db.inc.php";
  $badm_db = badm_conn_db();
  log_website_usage($start_time, $dir, $badm_db);
  mysql_close($badm_db);
?>