<html>
<head>
<title>Aantal views per pagina</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>

<body>
<P>Meet sinds 25 januari 2004</P>
<?php
      $color10 = "#FFFF00";
      $color20 = "#FF0000";
      $color50 = "#008000";
      $color1k = "#0000FF";
      $color2k = "#0000A0";
      $color5k = "#000040";
   require "functies/badm_db.inc.php";
   $badm_db = badm_conn_db();
   $sql = 'SELECT pagina
                 ,clicks
           FROM clickcount
		   ORDER BY clicks DESC';
   $result = mysql_query ($sql, $badm_db) or badm_mysql_die();
   echo "<TABLE width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
   while ($views = mysql_fetch_object($result))
   {
      if ($views->clicks < 10){ 
         $color = "$color10"; 
      } 
      elseif (($views->clicks >= 10 ) 
             and ($views->clicks < 25)){ 
         $color = "$color20"; 
      } 
      elseif (($views->clicks >= 20 ) 
              and ($views->clicks < 100)){ 
         $color = "$color50"; 
      } 
      elseif (($views->clicks >= 50 ) 
              and ($views->clicks < 250)){ 
         $color = "$color1k"; 
      } 
      elseif (($views->clicks >= 100 ) 
              and ($views->clicks < 500)){ 
         $color = "$color2k"; 
      } 
      elseif ($views->clicks >= 500){ 
         $color = "$color5k"; 
      }
	  $width = $views->clicks / 2;
	  echo "<TABLE>";
      echo "<tr><td align=\"left\" width=\"220\"><b>".$views->pagina."</b></td>\n"; 
      echo "<td align=\"right\" width=\"60\"><b>".$views->clicks."</b></td>\n"; 
      echo "<td align=\"left\" width=\"".$width."\" bgcolor=\"".$color."\"></td></tr>\n";
	  echo "</TABLE>";
   }
   echo "</TABLE>\n";
   mysql_close($badm_db);
?>
</body>
</html>