/*
|| function to highlight a table row with the colors specified below when clicked.
|| put  onClick="HighLightTR('#c9cc99','#cc3333');"  inside every TR-tag.
*/

var preEl;
var orgBColor;
var orgTColor;
function HighLightTR( backColor, textColor )
{
  if(typeof(preEl) != 'undefined')
  {
     preEl.bgColor = orgBColor;
     try
     {
       ChangeTextColor(preEl, orgTColor);
     }
     catch(e) {;}
  }
  var el = event.srcElement;
  el = el.parentElement;
  orgBColor = el.bgColor;
  orgTColor = el.style.color;
  el.bgColor = backColor;
  try
  {
    ChangeTextColor(el, textColor);
  }
  catch(e) {;}
  preEl = el;
}
function ChangeTextColor(a_obj, a_color)
{
   for (i = 0; i < a_obj.cells.length; i++)
   {
     a_obj.cells(i).style.color = a_color;
   }
}