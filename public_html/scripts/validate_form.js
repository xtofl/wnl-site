function validateForm()
{

//check of dt_van leeg is
   if (document.update_spelers.elements['dt_van'].value != "")
   {
   //check of plaats leeg is
      if (document.update_spelers.elements['plaats'].value == "")
      {
         alert("Vul een waarde in voor het veld <Naam> en <Plaats>");
         document.update_spelers.elements['plaats'].focus();
         return(false);
      }
   }

//check of Email juist is
   if (document.update_spelers.elements['email'].value != "")
   {
      if (!checkEmail(document.update_spelers.elements['email'].value))
      {
         alert("Het e-mail adres dat je ingevuld hebt is niet correct");
         document.update_spelers.elements['email'].focus();
         return(false);
      }
   }

//check of nieuw paswoord lang genoeg is
  if (document.update_spelers.elements['nieuw_paswoord1'].value != "")
  {
    if (document.update_spelers.elements['nieuw_paswoord1'].value.length < 6)
    {
      alert("Een paswoord moet minstens 6 karakters lang zijn.");
      document.update_spelers.elements['nieuw_paswoord1'].focus();
      return(false);
    }
  }

//check of nieuw paswoord correct is
  if (document.update_spelers.elements['nieuw_paswoord1'].value != "")
  {
    if (document.update_spelers.elements['nieuw_paswoord1'].value != document.update_spelers.elements['nieuw_paswoord2'].value)
    {
      alert("De nieuwe paswoorden zijn niet gelijk");
      document.update_spelers.elements['nieuw_paswoord1'].focus();
      return(false);
    }
  }

// valideer veld Inschrijving mailen
  if (document.update_spelers.elements['zend_inschrijving'].options[document.update_spelers.elements['zend_inschrijving'].selectedIndex].text != "" && document.update_spelers.elements['email'].value == "")
  {
    alert("Het heeft geen zin om het veld <Inschrijving Mailen> in te vullen als je geen e-mail adres opgeeft.");
    document.update_spelers.elements['email'].focus();
    return(false);
  }

//check datum formaat
  if (document.update_spelers.elements['start_dt'].value != "")
  {
    if (!checkDateFormat(document.update_spelers.elements['start_dt'].value))
    {
      alert("De datum in het veld <Speelt Badminton Sinds> is niet in het correcte formaat.");
      document.update_spelers.elements['start_dt'].focus();
      return(false);
    }
  }
  if (document.update_spelers.elements['club_dt'].value != "")
  {
    if (!checkDateFormat(document.update_spelers.elements['club_dt'].value))
    {
      alert("De datum in het veld <Lid Van W&L Sinds> is niet in het correcte formaat.");
      document.update_spelers.elements['club_dt'].focus();
      return(false);
    }
  }
  if (document.update_spelers.elements['dt_van'].value != "")
  {
    if (!checkDateFormat(document.update_spelers.elements['dt_van'].value))
    {
      alert("De datum in het veld <Datum Van> is niet in het correcte formaat.");
      document.update_spelers.elements['dt_van'].focus();
      return(false);
    }
  }
  if (document.update_spelers.elements['dt_tot'].value != "")
  {
    if (!checkDateFormat(document.update_spelers.elements['dt_tot'].value))
    {
      alert("De datum in het veld <Datum Tot> is niet in het correcte formaat.");
      document.update_spelers.elements['dt_tot'].focus();
      return(false);
    }
  }

  return true;
}

// ------------------------------------------------------------------------------
// deze functie controleert de juistheid van het e-mailadres op meerdere punten
// ------------------------------------------------------------------------------
function checkEmail(emailAddress)
{
   var foundAtSymbol = 0;
   var foundDot = 0;
   var md;

// Go through each character in the email address.
   for (var x=0; x<emailAddress.length - 1; x++)
   {
      md = emailAddress.substr(x, 1);

   // Is the character an @ symbol?
      if (md == '@')
         foundAtSymbol++;

   // Count how many dots there are after the @ symbol.
      if (md == '.' && foundAtSymbol == 1)
         foundDot++;
   }

// Is there only one @ symbol, and are there more than one dots?
   if (foundDot > 0 && foundAtSymbol == 1)
   {
      return true;
   }
   else
   {
      return false;
   }
}
// ------------------------------------------------------------------------------
// deze functie controleert de juistheid van het datumformaat
// ------------------------------------------------------------------------------
function checkDateFormat(datum)
{
  // Required format = YYYY/MM/DD
  if (datum.length == 10)
  {
    var arr = datum.split("/");
    if (arr[0].length != 4 || arr[1].length != 2 || arr[2].length != 2)
    {
      return false;
    }
  }
  else
  {
    return false;
  }
  return true;
}
var submitcount = 0;
// ------------------------------------------------------------------------------
// Main function
// ------------------------------------------------------------------------------
function checkSubmitCount()
{
  if (validateForm())
  {
    submitcount = submitcount + 1;
    if (submitcount == 1)
    {
      document.update_spelers.submit();
    }
    else
    {
      if (submitcount == 2)
      {
        alert("Je hebt deze gegevens al verzonden");
      }
      else
      {
        alert("Je hebt deze gegevens nu al" + submitcount.toString() + " keer verzonden");
      }
    }
  }
}