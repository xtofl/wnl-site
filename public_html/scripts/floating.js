/*
Floating Menu script- Roy Whittle (http://www.javascript-fx.com/)
Script featured on/available at http://www.dynamicdrive.com/
This notice must stay intact for use
*/

/*
Plaats hetgeen je floating wilt tussen deze div tags: <div id="divStayTopLeft" style="position:absolute; visibility:inherit;"></div>
en roep de functie JSFX_FloatTopDiv() aan na deze div
*/

//Enter "frombottom" or "fromtop"
var verticalpos = "fromtop"

//Maakt een object 'floating'
function JSFX_FloatTopDiv()
{
   var startX = 0,
   startY = 130;
   var ns = (navigator.appName.indexOf("Netscape") != -1);
   var d = document;
   function ml(id)
   {
      var el = d.getElementById?d.getElementById(id):d.all?d.all[id]:d.layers[id];
      if (d.layers)
         el.style = el;
      el.sP = function(x,y){this.style.left = x;this.style.top = y;};
      el.x = startX;
      if (verticalpos == "fromtop")
         el.y = startY;
      else
      {
         el.y = ns ? pageYOffset + innerHeight : document.body.scrollTop + document.body.clientHeight;
         el.y -= startY;
      }
      return el;
   }
   window.stayTopLeft = function()
   {
      if (verticalpos == "fromtop")
      {
         var pY = ns ? pageYOffset : document.body.scrollTop;
         ftlObj.y += (pY + startY - ftlObj.y)/8;
      }
      else
      {
         var pY = ns ? pageYOffset + innerHeight : document.body.scrollTop + document.body.clientHeight;
         ftlObj.y += (pY - startY - ftlObj.y)/8;
      }
      ftlObj.sP(ftlObj.x, ftlObj.y);
      setTimeout("stayTopLeft()", 10);
   }
   ftlObj = ml("divStayTopLeft");
   stayTopLeft();
}