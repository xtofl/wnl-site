<!--
function showMenu()
{
stm_bm(["W&L menu",400,"","http://www.badmintonsport.be/scripts/blank.gif",0,"","",0,0,250,0,1000,1,0,0]);
stm_bp("p0",[1,4,0,0,1,4,0,7,100,"progid:DXImageTransform.Microsoft.Fade(overlap=.5,enabled=0,Duration=0.30)",-2,"progid:DXImageTransform.Microsoft.Pixelate(MaxSquare=15,enabled=0,Duration=0.50)",-2,50,0,0,"#669999","red","",3,0,0,"#ffffff"]);
stm_ai("p0i0",[0,"Home","","",-1,-1,0,"index.php","_self","Startpagina","","","",0,0,0,"","",0,0,0,0,1,"#CCCCCC",0,"#ffffff",0,"","",3,3,0,0,"#ffffff","#000000","#191970","#000000","10pt Verdana","10pt Verdana",0,0]);
stm_aix("p0i1","p0i0",[0,"Algemeen","","",-1,-1,0,"","_self","informatie over de club","","","",0,0,0,"scripts/arrow_r_black.gif","scripts/arrow_r_red.gif",7,7]);
stm_bpx("p1","p0",[1,2,0,0,1,4,0]);
stm_aix("p1i0","p0i0",[0,"Bestuur","","",-1,-1,0,"/algemeen/bestuur.php","_self","Bestuursleden","","","",0,0,0,"","",0,0,0,1]);
stm_aix("p1i1","p0i0",[0,"Medewerkers","","",-1,-1,0,"/algemeen/medewerkers.php","_self","Medewerkers","","","",0,0,0,"","",0,0,0,1]);
stm_aix("p1i2","p0i0",[0,"Speeluren","","",-1,-1,0,"/algemeen/speeluren.php","_self","Speeluren","","","",0,0,0,"","",0,0,0,1]);
stm_aix("p1i3","p0i1",[0,"Geschiedenis","","",-1,-1,0,"","_self","Geschiedenis","","","",0,0,0,"","",0,0,0,1]);
stm_bpx("p2","p0",[1,2,0,0,1,4,0,0]);
stm_aix("p2i0","p1i0",[0,"Clubkampioenen","","",-1,-1,0,"/algemeen/Geschiedenis/clubkampioenen.php","_self","Winnaars Clubkampioenschap"]);
stm_aix("p2i1","p1i0",[0,"Nationale Kampioenen","","",-1,-1,0,"/algemeen/Geschiedenis/nationalekampioenen.php","_self","Nationale Kampioenen W&L"]);
stm_ep();
stm_aix("p1i4","p0i0",[0,"Sponsors","","",-1,-1,0,"/algemeen/sponsors.php","_self","Sponsors","","","",0,0,0,"","",0,0,0,1]);
stm_aix("p1i5","p0i0",[0,"Artikels","","",-1,-1,0,"/algemeen/artikels.php","_self","Artikels","","","",0,0,0,"","",0,0,0,1]);
stm_ep();
stm_aix("p0i2","p0i0",[0,"Competitie","","",-1,-1,0,"/competitie.php","_self","Competitie: ploegen en uitslagen"]);
stm_aix("p0i3","p0i1",[0,"Tornooien","","",-1,-1,0,"/competitie.php","_self","Tornooien"]);
stm_bpx("p1","p0",[1,2,0,0,1,4,0]);
stm_aix("p1i0","p0i1",[0,"Inschrijvingen","","",-1,-1,0,"","_self","Inschrijvingen","Schrijf je hier in voor tornooien"]);
stm_bpx("p1","p0",[1,2,0,0,1,4,0,0]);
stm_aix("p2i0","p1i0",[1,"Dibad (Dilsen) <B>12/03</B>","","",-1,-1,0,"/tornooien/inschrijvingen.php?id=62","_self","Inschrijving Dibad (Dilsen)"]);stm_aix("p2i1","p1i0",[1,"Smash (Heusden-Zolder) (Jeugd) <B>09/02</B>","","",-1,-1,0,"/tornooien/inschrijvingen_jeugd.php?id=64","_self","Inschrijving Smash (Heusden-Zolder)"]);stm_aix("p2i2","p1i0",[1,"Belgische Kamp.(Torhout) (Jeugd) <B>26/02</B>","","",-1,-1,0,"/tornooien/inschrijvingen_jeugd.php?id=65","_self","Inschrijving Belgische Kamp.(Torhout)"]);stm_aix("p2i3","p1i0",[1,"Belgische Kamp. (Torhout) (Veteranen) <B>26/02</B>","","",-1,-1,0,"/tornooien/inschrijvingen_jeugd.php?id=66","_self","Inschrijving Belgische Kamp. (Torhout)"]);stm_aix("p2i4","p1i0",[1,"BC Halle <B>20/02</B>","","",-1,-1,0,"/tornooien/inschrijvingen.php?id=60","_self","Inschrijving BC Halle"]);stm_aix("p2i5","p1i0",[1,"Opbad (Opglabbeek) <B>23/01</B>","","",-1,-1,0,"/tornooien/inschrijvingen.php?id=61","_self","Inschrijving Opbad (Opglabbeek)"]);stm_aix("p2i6","p1i0",[1,"Smash (Ekeren) <B>29/01</B>","","",-1,-1,0,"/tornooien/inschrijvingen.php?id=63","_self","Inschrijving Smash (Ekeren)"]);stm_aix("p2i7","p1i0",[1,"BC Stekene <B>15/01</B>","","",-1,-1,0,"/tornooien/inschrijvingen.php?id=51","_self","Inschrijving BC Stekene"]);stm_aix("p2i8","p1i0",[1,"Brugge BBAC <B>22/01</B>","","",-1,-1,0,"/tornooien/inschrijvingen.php?id=49","_self","Inschrijving Brugge BBAC"]);stm_aix("p2i9","p1i0",[1,"Olympia BC <B>15/01</B>","","",-1,-1,0,"/tornooien/inschrijvingen.php?id=58","_self","Inschrijving Olympia BC"]);stm_aix("p2i10","p1i0",[1,"De Nekker  <IMG src=/images/closed.png>","","",-1,-1,0,"/tornooien/inschrijvingen.php?id=59","_self","Inschrijving De Nekker"]);stm_aix("p2i11","p1i0",[1,"W&L (Jeugd)  <IMG src=/images/closed.png>","","",-1,-1,0,"/tornooien/inschrijvingen_jeugd.php?id=57","_self","Inschrijving W&L"]);stm_aix("p2i12","p1i0",[1,"TOTS Keerbergen  <IMG src=/images/closed.png>","","",-1,-1,0,"/tornooien/inschrijvingen.php?id=44","_self","Inschrijving TOTS Keerbergen"]);
stm_ep();
stm_aix("p1i1","p0i0",[0,"Aanvangsuren","","",-1,-1,0,"/tornooien/aanvangsuren.php","_self","Aanvangsuren tornooien"]);
stm_aix("p1i2","p0i0",[0,"Kalender","","",-1,-1,0,"/docs/tornooikalender20042005.pdf","_blank","Tornooikalender seizoen 2004 - 2005"]);
stm_aix("p1i3","p0i1",[0,"Resultaten","","",-1,-1,0,"","_self","Tornooiresultaten W&L spelers"]);
stm_bpx("p2","p0",[1,2,0,0,1,4,0,0]);
stm_aix("p2i0","p1i0",[0,"Seizoen 2004-2005","","",-1,-1,0,"/tornooien/resultaten.php?s=2005&view_id=22","_self","Seizoen 2004-2005"]);stm_aix("p2i1","p1i0",[0,"Seizoen 2003-2004","","",-1,-1,0,"/tornooien/resultaten.php?s=2004&view_id=22","_self","Seizoen 2003-2004"]);stm_aix("p2i2","p1i0",[0,"Seizoen 2002-2003","","",-1,-1,0,"/tornooien/resultaten.php?s=2003&view_id=22","_self","Seizoen 2002-2003"]);stm_aix("p2i3","p1i0",[0,"Seizoen 2001-2002","","",-1,-1,0,"/tornooien/resultaten.php?s=2002&view_id=22","_self","Seizoen 2001-2002"]);stm_aix("p2i4","p1i0",[0,"Seizoen 2000-2001","","",-1,-1,0,"/tornooien/resultaten.php?s=2001&view_id=22","_self","Seizoen 2000-2001"]);stm_aix("p2i5","p1i0",[0,"Seizoen 1999-2000","","",-1,-1,0,"/tornooien/resultaten.php?s=2000&view_id=22","_self","Seizoen 1999-2000"]);stm_aix("p2i6","p1i0",[0,"Seizoen 1998-1999","","",-1,-1,0,"/tornooien/resultaten.php?s=1999&view_id=22","_self","Seizoen 1998-1999"]);stm_aix("p2i7","p1i0",[0,"Seizoen 1997-1998","","",-1,-1,0,"/tornooien/resultaten.php?s=1998&view_id=22","_self","Seizoen 1997-1998"]);stm_aix("p2i8","p1i0",[0,"Seizoen 1996-1997","","",-1,-1,0,"/tornooien/resultaten.php?s=1997&view_id=22","_self","Seizoen 1996-1997"]);stm_aix("p2i9","p1i0",[0,"Seizoen 1995-1996","","",-1,-1,0,"/tornooien/resultaten.php?s=1996&view_id=22","_self","Seizoen 1995-1996"]);stm_aix("p2i10","p1i0",[0,"Seizoen 1994-1995","","",-1,-1,0,"/tornooien/resultaten.php?s=1995&view_id=22","_self","Seizoen 1994-1995"]);stm_aix("p2i11","p1i0",[0,"Seizoen 1993-1994","","",-1,-1,0,"/tornooien/resultaten.php?s=1994&view_id=22","_self","Seizoen 1993-1994"]);stm_aix("p2i12","p1i0",[0,"Seizoen 1992-1993","","",-1,-1,0,"/tornooien/resultaten.php?s=1993&view_id=22","_self","Seizoen 1992-1993"]);stm_aix("p2i13","p1i0",[0,"Seizoen 1990-1991","","",-1,-1,0,"/tornooien/resultaten.php?s=1991&view_id=22","_self","Seizoen 1990-1991"]);stm_aix("p2i14","p1i0",[0,"Seizoen 1989-1990","","",-1,-1,0,"/tornooien/resultaten.php?s=1990&view_id=22","_self","Seizoen 1989-1990"]);stm_aix("p2i15","p1i0",[0,"Seizoen 1988-1989","","",-1,-1,0,"/tornooien/resultaten.php?s=1989&view_id=22","_self","Seizoen 1988-1989"]);stm_aix("p2i16","p1i0",[0,"Seizoen 1987-1988","","",-1,-1,0,"/tornooien/resultaten.php?s=1988&view_id=22","_self","Seizoen 1987-1988"]);
stm_ep();
stm_ep();
stm_aix("p0i4","p0i0",[0,"Spelers","","",-1,-1,0,"/spelers.php","_self","Spelersprofielen"]);
stm_aix("p0i5","p0i1",[0,"Jeugd","","",-1,-1,0,"","_self","Informatie over en voor de jeugdspelers"]);
stm_bpx("p1","p0",[1,2,0,0,1,4,0]);
stm_aix("p1i0","p0i0",[0,"Algemeen","","",-1,-1,0,"/jeugd/jeugd.php","_self","Jeugd trainers"]);
stm_aix("p1i1","p0i0",[0,"Teams","","",-1,-1,0,"/jeugd/jeugdteams.php","_self","Jeugdteams"]);
stm_aix("p1i2","p0i0",[0,"Resultaten","","",-1,-1,0,"/jeugd/resultaten_jeugd.php?s=2005","_self","Resultaten jeugdtornooien W&L spelers"]);
stm_aix("p1i3","p0i0",[0,"Jeugdtornooi","","",-1,-1,0,"/jeugd/jeugdtornooi.php","_self","Jeugdtornooi W&L"]);
stm_ep();
stm_aix("p0i6","p0i0",[0,"Recreanten","","",-1,-1,0,"/recreant.php","_self","Recreanten"]);
stm_aix("p0i7","p0i1",[0,"Foto's","","",-1,-1,0,"http://users.pandora.be/ikkegg","_blank","badminton foto's"]);
stm_bpx("p1","p0",[1,2,0,0,1,4,0]);
stm_aix("p0i0","p0i0",[0,"competitie 2e ploeg","","",-1,-1,0,"/pictures/showThumbnails.php?dir=competitie 2e ploeg","_self","competitie 2e ploeg"]);
stm_aix("p0i1","p0i0",[0,"Vive La Faute fuif","","",-1,-1,0,"/pictures/showThumbnails.php?dir=Vive La Faute fuif","_self","Vive La Faute fuif"]);
stm_aix("p0i2","p0i0",[0,"Grande-Synthe","","",-1,-1,0,"/pictures/showThumbnails.php?dir=Grande-Synthe","_self","Grande-Synthe"]);
stm_aix("p0i3","p0i0",[0,"VVBBC","","",-1,-1,0,"/pictures/showThumbnails.php?dir=VVBBC","_self","VVBBC"]);
stm_ep();
stm_ai("p0i8",[6,4,"#ffffff","",0,0,0]);
stm_aix("p0i9","p0i0",[0,"Links","","",-1,-1,0,"/links.php","_self","Links"]);
stm_aix("p0i10","p0i0",[0,"Message board","","",-1,-1,0,"/messageboard/messageboard.php?p=1&m=n&id=0","_blank","Message Board"]);
stm_ep();
stm_em();
}
-->