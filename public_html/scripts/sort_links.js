// Object constructor
function linkRecord(plaats, naam, url)
{
   this.plaats            = plaats;
   this.naam              = naam;
   this.url               = url;
}
// Function to handle sorting
// property : column name by which to order
function sortBy(property)
{
   document.body.style.cursor = 'progress';
   window.status = 'Working...';
   sortProp = property;
   linkArray = linkArray.sort(sortFunc);
   sortResult = '<TABLE cellspacing="1" cellpadding="4" width="100%" align="center">';
   for (j = 0; j < linkArray.length; j++)
   {
      sortResult += '<TR>'
+'<td width="35%" bgcolor="#f1f1f1"><p>'+linkArray[j].plaats
+'</p></td><td width="50%" bgcolor="#dfdfdf"> <p><a href="'+linkArray[j].url
+'" target="_blank" title="'+linkArray[j].naam
+'">'+linkArray[j].naam
+'</a></p></td></tr>';
   }
   sortResult += '</TABLE>';
   document.getElementById('sortData').innerHTML = sortResult;
//   showDiv(property);
   window.status = 'Gesorteerd op '+property;
   document.body.style.cursor = 'default'; 
}
function sortFunc(linkArray1, linkArray2)
{
   if (linkArray1[sortProp] < linkArray2[sortProp])
      retVal = -1;
   else if (linkArray1[sortProp] > linkArray2[sortProp])
      retVal = 1;
   else
      retVal = 0;
   return retVal;
}