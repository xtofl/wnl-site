
  var READY_STATE_COMPLETE = 4;                // The response is complete and available for further client side handling
  var PAGE_SUCCESS = 200;                      // The http code send by the server on success of the request is 200
  var requestObject = createXmlHttpRequest();  // Create new Http request object
  var isWorking = false;                       // Avoids the function being called a second time when it is still running
  var action;                                  // Name identifying an action to perform with the returned data

  function requestData($url, $action, $params)
  {
    if (!isWorking && requestObject)
    {
      // The first argument specifies the access, which can be any allowed server access like GET, POST or HEAD. The second argument references the URI source to read from.
      // The third argument is a Boolean type that defines whether the call should be synchronous (false) or asynchronous (true). Using synchronous requests will makes
      // the browser wait for the serve to respond, which is not what you want to see happening in a production application when fetching lot of data from the server.
      requestObject.open('POST', $url, true);
      action = $action;
      isWorking = true;
      requestObject.onreadystatechange = onReadyStateChangeResponse;  // callback that is called on state change
      requestObject.send($params);  // If this was a POST request, then the null argument value could be replaced with a list of request parameters that for the server to work with.
    }
  }

  function onReadyStateChangeResponse()
  {
    if (requestObject.readyState == READY_STATE_COMPLETE)
    {
      isWorking = false;
      if (requestObject.status == PAGE_SUCCESS)
      {
        processData(action);
      }
      else if (requestObject.status == 404)
      {
        alert("Requested url is not found");
      }
      else if (requestObject.status == 403)
      {
        alert("Access Denied");
      }
      else
      {
        alert('Failed with HTTP status code '+requestObject.status);
      }
    }
  }

// Returns a XmlHttprequest handle to the caller

  function createXmlHttpRequest()
  {
    var XmlHttpRequestObject;
    if (typeof XMLHttpRequest != "undefined") // Mozilla, Safari, Opera, any non-IE browser that supports asynchronous JavaScript request and response
    {
      XmlHttpRequestObject = new XMLHttpRequest();
    }
    else if (window.ActiveXObject) // Microsoft Internet Explorer
    {
      // look up the highest possible MSXML version
      var tryPossibleVersions=["MSXML2.XMLHttp.5.0"
                              ,"MSXML2.XMLHttp.4.0"
                              ,"MSXML2.XMLHttp.3.0"
                              ,"MSXML2.XMLHttp"
                              ,"Microsoft.XMLHttp"];

      // The Microsoft browser distinguishes its version of XmlHttp support and as a developer you should prefer using the latest available support.
      for (i=0; i< tryPossibleVersions.length; i++)
      {
        try
        {
          XmlHttpRequestObject = new ActiveXObject(tryPossibleVersions[i]);
          break;
        }
        catch (xmlHttpRequestObjectError)
        {
          //ignore
        }
      }
    }
    return XmlHttpRequestObject;
  }