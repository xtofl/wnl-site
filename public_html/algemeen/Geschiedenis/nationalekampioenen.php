<?php
  require_once "../../functies/website_usage.php";
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="nl">
<head>
 <meta http-equiv="Pragma" content="no-cache">
 <meta http-equiv="Expires" content="now">
 <meta name="robots" content="index,follow">
 <meta name="keywords" content="badminton,batminton,w&l,W&amp;L,badmintonvereniging,club,badmintonclub,sport,Belgium,Veltem,Herent,Leuven,Winksele,Belgi�,Belgisch,Belgische,kampioen,kampioenen,nationale,nationaal,BK,kampioenschap" />
 <meta name="description" content="W&L bv : Nationale Kampioenen" />
 <title>W&amp;L badmintonvereniging vzw - Nationale Kampioenen</title>
 <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<style type="text/css">
<!--
td {
    font-family: Verdana;
    font-size: xx-small;
}
.plaats {
    background-color: #8080A6;
    color: white;
    font-weight: bold;
    text-align: center;
}
.discipline {
    background-color: #F1F1F1;
    text-align: center;
}
.winnaar {
    background-color: #DFDFDF;
}
-->
</style>
 <link href="http://www.badmintonsport.be/css/badminton.css" rel="stylesheet" type="text/css">
 <script type="text/javascript" language="JavaScript1.2" src="http://www.badmintonsport.be/scripts/stm31.js"></script>
</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" class="badminton">
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <!--DWLayoutTable-->
  <tr> 
    <td height="95" colspan="2" valign="top"> <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <!--DWLayoutTable-->
        <tr> 
          <td width="100%" height="95"> <table width="100%" border="0" cellpadding="0" cellspacing="0" bordercolor="#CCCCCC" bgcolor="#FFFFFF">
              <!--DWLayoutTable-->
              <tr> 
                <td align="center" valign="middle" bgcolor="#006600"> <img src="../../images/spacer.gif" width="200" height="4"></td>
              </tr>
              <tr> 
                <td align="center" valign="middle" bgcolor="#CCCCCC"> <img src="../../images/pt_transp.gif" width="500" height="5"></td>
              </tr>
              <tr> 
                <td width="100%" align="center" valign="middle" bgcolor="#CCCCCC"> 
                  <h4><abbr lang="nl" title="Winksele en Leuven">W&amp;L</abbr> Badmintonvereniging vzw, Mechelsesteenweg 387, 3020 
                    Herent</h4></td>
              </tr>
              <tr> 
                <td align="center" valign="top" bgcolor="#CCCCCC"> <img src="../../images/chinas.gif" width="400" height="60"></td>
              </tr>
              <tr> 
                <td align="center" valign="middle" bgcolor="#CCCCCC"> <img src="../../images/pt_transp.gif" width="500" height="5"></td>
              </tr>
              <tr> 
                <td align="center" valign="middle" bgcolor="#336600"> <img src="../../images/spacer.gif" width="200" height="4"></td>
              </tr>
            </table></td>
        </tr>
      </table></td>
  </tr>
  <tr> 
    <td width="150" height="213" valign="top">
<?php
   require "../../templates/template.php";
   invoegen_template(); //menu
?>
    </td>
    <td width="100%" valign="top"> <table cellpadding="4" cellspacing="1" width="97%">
        <tr> 
          <td colspan="3" class="plaats">2005</td>
        </tr>
        <tr> 
          <td class="discipline"> DE A </td>
          <td class="winnaar"><b>Nathalie Descamps</b> wint van<br>
            ?</td>
          <td class="discipline">&nbsp;</td>
        </tr>
        <tr> 
          <td class="discipline"> HD A </td>
          <td class="winnaar"><b>Wouter Claes</b> en Ruud Kuijten winnen van<br>
            ?</td>
          <td class="discipline">&nbsp;</td>
        </tr>
        <tr> 
          <td class="discipline"> DD A </td>
          <td class="winnaar"><b>Katrien Claes</b> en <b>Nathalie Descamps</b> winnen van<br>
            ?</td>
          <td class="discipline">&nbsp;</td>
        </tr>
        <tr> 
          <td class="discipline"> GD A </td>
          <td class="winnaar"><b>Wouter Claes</b> en <b>Nathalie Descamps</b> winnen van<br>
            ?</td>
          <td class="discipline">&nbsp;</td>
        </tr>
        <tr> 
          <td colspan="3" class="plaats"> 31/01/2004 - 01/02/2004 (Veltem) </td>
        </tr>
        <tr> 
          <td class="discipline"> DE A </td>
          <td class="winnaar"><b>Nathalie Descamps</b> wint van<br>
            Agnieszka Czerwinska</td>
          <td class="discipline"> 11-2 / 11-9 </td>
        </tr>
        <tr> 
          <td class="discipline"> HD A </td>
          <td class="winnaar"><b>Wouter Claes</b> en Ruud Kuijten winnen van<br>
            Patrick Boonen/David Vandewinkel</td>
          <td class="discipline"> 15-8 / 15-5 </td>
        </tr>
        <tr> 
          <td class="discipline"> GD A </td>
          <td class="winnaar"><b>Wouter Claes</b> en Corina Herrle winnen van<br>
            Luigi Dalli Cardillo/Veerle Rakels</td>
          <td class="discipline"> 15-3 / 15-13 </td>
        </tr>
        <tr> 
          <td colspan="3" class="plaats"> 01/02/2003 - 02/02/2003 (Louvain-la-Neuve) 
          </td>
        </tr>
        <tr> 
          <td class="discipline"> HD A </td>
          <td class="winnaar"><b>Claes Wouter</b> en Mawet Frederic winnen van<br>
            Kuijten Ruud/Willems Hans</td>
          <td class="discipline"> 15-5 / 14-17 / 15-12 </td>
        </tr>
        <tr> 
          <td colspan="3" class="plaats"> 02/02/2002 - 03/02/2002 (Kortrijk) </td>
        </tr>
        <tr> 
          <td width="10%" class="discipline"> HD A </td>
          <td width="70%" class="winnaar"><b>Claes Wouter</b> en Mawet Frederic 
            winnen van<br>
            De Rijcke Stefaan/Genaux Philippe</td>
          <td width="20%" class="discipline"> 7-2 / 5-7 / 7-0 / 7-5 </td>
        </tr>
        <tr> 
          <td width="10%" class="discipline">GD A </td>
          <td width="70%" class="winnaar"><b>Claes Wouter</b> en Albinus Manon 
            winnen van<br>
            De Rijcke Stefaan/Robbrecht Sofie</td>
          <td width="20%" class="discipline"> /WO </td>
        </tr>
        <tr> 
          <td colspan="3" class="plaats"> 03/02/2001 - 04/02/2001 (Louvain-la-Neuve) 
          </td>
        </tr>
        <tr> 
          <td width="10%" class="discipline">HD A </td>
          <td width="70%" class="winnaar"><b>Claes Wouter</b> en Mawet Frederic 
            winnen van<br>
            Patrick Boonen/David Vandewinkel </td>
          <td width="20%" class="discipline"> 15-8 / 17-15 </td>
        </tr>
        <tr> 
          <td width="10%" class="discipline">GD A </td>
          <td width="70%" class="winnaar"><b>Claes Wouter</b> en Albinus Manon 
            winnen van<br>
            Kuijten Ruud/Biesbrouck Elke </td>
          <td width="20%" class="discipline"> 15-3 / 15-10 </td>
        </tr>
        <tr> 
          <td colspan="3" class="plaats"> 07/02/1998 - 08/02/1998 (Neerpelt) </td>
        </tr>
        <tr> 
          <td width="10%" class="discipline">GD A </td>
          <td width="70%" class="winnaar"><b>Claes Wouter</b> en Albinus Manon 
            winnen van ? </td>
          <td width="20%" class="discipline"> ? </td>
        </tr>
      </table></td>
  </tr>
  <tr> 
    <td height="0"></td>
    <td></td>
  </tr>
  <tr> 
    <td height="1"><img src="../../images/spacer.gif" alt="" width="150" height="1"></td>
    <td></td>
  </tr>
</table>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <!--DWLayoutTable-->
  <tr> 
    <td width="100%" height="50" valign="top" bgcolor="#FFFFFF"> <table width="100%" border="0" cellpadding="0" cellspacing="0" bordercolor="#CCCCCC" bgcolor="#CCCCCC">
        <!--DWLayoutTable-->
        <tr> 
          <td align="center" valign="middle" bordercolor="#CCCCCC" bgcolor="#006600"> 
            <img src="../../images/spacer.gif" width="200" height="4"></td>
        </tr>
        <tr> 
          <td width="100%" align="center" valign="middle" bordercolor="#CCCCCC" bgcolor="#CCCCCC" class="gmenu">Last 
            change: 30-05-2004 &nbsp; E-mail: <a href="mailto:webmaster@badmintonsport.be">webmaster@badmintonsport.be</a></td>
        </tr>
        <tr> 
          <td align="center" valign="middle" bordercolor="#CCCCCC" bgcolor="#006600" class="gmenu"> 
            <img src="../../images/spacer.gif" width="200" height="4"></td>
        </tr>
      </table></td>
  </tr>
</table>
</body>
</html>
<?php
  require_once "../../functies/badm_db.inc.php";
  $badm_db = badm_conn_db();
  log_website_usage($start_time, null, $badm_db);
  mysql_close($badm_db);
?>