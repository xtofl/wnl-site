<?php
  require_once "../functies/website_usage.php";
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html lang="nl"><!-- InstanceBegin template="/Templates/templatesub1php.dwt.php" codeOutsideHTMLIsLocked="false" -->
<?php
    // Connect to database
    require_once "/home/badmin/public_html/functies/badm_db.inc.php";
    $badm_db = badm_conn_db();
?>
<head>
<META HTTP-EQUIV="Content-Type"      CONTENT="text/html; charset=iso-8859-1">
<META NAME="robots"                  CONTENT="index,follow">
<META NAME="keywords"                CONTENT="badminton,batminton,w&amp;l,badmintonvereniging,club,badmintonclub,sport,Belgium,Veltem,Herent,Leuven,Winksele,Belgi�">
<META NAME="description"             CONTENT="W&amp;L bv : De grootste en leukste badmintonclub van Brabant">
<META NAME="abstract"                CONTENT="badminton, club">
<META NAME="Area"                    CONTENT="General">
<META NAME="Author"                  CONTENT="W&amp;L bv">
<META NAME="Copyright"               CONTENT="(c) 2003-2004 W&amp;L bv">
<META NAME="Creator"                 CONTENT="W&amp;L bv">
<META NAME="Generator"               CONTENT="W&amp;L bv">
<META NAME="Owner"                   CONTENT="W&ampL bv">
<META NAME="Publisher"               CONTENT="W&ampL bv">
<META NAME="Rating"                  CONTENT="General">
<META NAME="revisit-after"           CONTENT="7 Days">
<META NAME="revisit"                 CONTENT="7 Days">
<META NAME="document-classification" CONTENT="Internet">
<META NAME="document-type"           CONTENT="Public">
<META NAME="document-rating"         CONTENT="Safe for Kids">
<META NAME="document-distribution"   CONTENT="Global">
<META HTTP-EQUIV="Content-Type"      CONTENT="text/html; charset=iso-8859-1">
<META HTTP-EQUIV="Audience"          CONTENT="General">
<META HTTP-EQUIV="content-language"  CONTENT="NL">
<META HTTP-EQUIV="Pragma"            CONTENT="no-cache">
<META HTTP-EQUIV="Expires"           CONTENT="now">
<!-- InstanceBeginEditable name="doctitle" -->
<title>W&amp;L badmintonvereniging vzw - Medewerkers</title>
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
<link href="http://www.badmintonsport.be/css/badminton.css" rel="stylesheet" type="text/css">
<script type="text/javascript" language="JavaScript" src="http://www.badmintonsport.be/scripts/stm31.js"></script>
<script type="text/javascript" language="JavaScript" src="http://www.badmintonsport.be/scripts/dw_rotator.js"></script>
<script type="text/javascript" language="JavaScript">
<!--
function initRotator()
{
  var rotator1 = new dw_Rotator("sponsors_banner", 4000, "http://www.badmintonsport.be/images/", "_blank");
  rotator1.addImages("Veltem_Motors_banner.png", "banner_verhoeven.jpg");
  //rotator1.addImages("Veltem_Motors_banner.png");
  rotator1.addActions("http://www.veltem-motors.be", "http://www.isolatieverhoeven.be");
  //rotator1.addActions("http://www.veltem-motors.be");
  dw_Rotator.start();
}
function doLoad()
{
  // initialize the menu
  st_onload();
  // initialize the banner rotator
  initRotator();
}
//-->
</script>
</head>
<body class="badminton" onload="doLoad();">
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <!--DWLayoutTable-->
  <tr>
  <td colspan="2">
   <table width="100%" border="0" cellpadding="0" cellspacing="0">
    <!--DWLayoutTable-->
    <tr> 
     <td width="100%" style="background-color: #006600;"><img src="http://www.badmintonsport.be/images/pt_transp.gif" width="1px" height="4px" alt=""></td>
    </tr>
    <tr> 
     <td align="center" valign="top" bgcolor="#CCCCCC">
      <table width="100%" border="0" cellspacing="0" cellpadding="3">
       <tr align="center" valign="middle"> 
        <td rowspan="2" align="left">
	     <img src="http://www.badmintonsport.be/images/logow&amp;l-100.gif" width="100" height="80" alt="logo">
	    </td>
        <td>
	     <h4>W&amp;L Badmintonvereniging vzw<br>
	         Mechelsesteenweg 387<br>
             3020 Herent</h4>
		</td>
        <td align="right"><a href="javascript:void();" onclick="return dw_Rotator.doClick(0)"  onmouseover="dw_Rotator.pause(0)" onmouseout="dw_Rotator.resume(0)" onfocus="this.blur()"><img id="sponsors_banner" name="sponsors_banner" src="http://www.badmintonsport.be/images/Veltem_Motors_banner.png" alt="sponsors" width="320" height="100" border="0"></a></td>
       </tr>
      </table>
	 </td>
    </tr>
    <tr> 
     <td bgcolor="#336600"><img src="http://www.badmintonsport.be/images/pt_transp.gif" width="1px" height="4px" alt=""></td>
    </tr>
   </table>
  </td>
 </tr>
 <tr>
  <td width="150" height="213" valign="top">
<script type="text/javascript" language="JavaScript1.2">
<!--
stm_bm(["W&L menu",400,"","http://www.badmintonsport.be/scripts/blank.gif",0,"","",0,0,250,0,1000,1,0,0]);
stm_bp("p0",[1,4,0,0,1,4,0,7,100,"progid:DXImageTransform.Microsoft.Fade(overlap=.5,enabled=0,Duration=0.30)",-2,"progid:DXImageTransform.Microsoft.Pixelate(MaxSquare=15,enabled=0,Duration=0.50)",-2,50,0,0,"#669999","#006633","",3,0,0,"#ffffff"]);
stm_ai("p0i0",[0,"Home","","",-1,-1,0,"http://www.badmintonsport.be/index.php","_self","Startpagina","","","",0,0,0,"","",0,0,0,0,1,"#fffff7",0,"#b5bed6",0,"","",3,3,0,0,"#fffff7","#000000","#000000","#000000","10pt Verdana","10pt Verdana",0,0]);
stm_aix("p0i1","p0i0",[0,"Algemeen","","",-1,-1,0,"","_self","informatie over de club","","","",0,0,0,"http://www.badmintonsport.be/scripts/arrow_r.gif","http://www.badmintonsport.be/scripts/arrow_r.gif",7,7]);
stm_bpx("p1","p0",[1,2,0,0,1,4,0]);
stm_aix("p1i0","p0i0",[0,"Bestuur","","",-1,-1,0,"http://www.badmintonsport.be/algemeen/bestuur.php","_self","Bestuursleden","","","",0,0,0,"","",0,0,0,1]);
stm_bpx("p1","p0",[1,2,0,0,1,4,0,0]);
stm_aix("p2i0","p1i0",[0,"Mededelingen","","",-1,-1,0,"http://www.badmintonsport.be/algemeen/Bestuur/bestuursmededelingen.php","_self","Bestuursmededelingen"]);
stm_ep();
stm_aix("p1i1","p0i0",[0,"Medewerkers","","",-1,-1,0,"http://www.badmintonsport.be/algemeen/medewerkers.php","_self","Medewerkers","","","",0,0,0,"","",0,0,0,1]);
stm_aix("p1i2","p0i0",[0,"Speeluren","","",-1,-1,0,"http://www.badmintonsport.be/algemeen/speeluren.php","_self","Speeluren","","","",0,0,0,"","",0,0,0,1]);
stm_aix("p1i3","p0i1",[0,"Geschiedenis","","",-1,-1,0,"","_self","Geschiedenis","","","",0,0,0,"","",0,0,0,1]);
stm_bpx("p2","p0",[1,2,0,0,1,4,0,0]);
stm_aix("p2i0","p1i0",[0,"Clubkampioenen","","",-1,-1,0,"http://www.badmintonsport.be/algemeen/Geschiedenis/clubkampioenen.php","_self","Winnaars Clubkampioenschap"]);
stm_aix("p2i1","p1i0",[0,"Nationale Kampioenen","","",-1,-1,0,"http://www.badmintonsport.be/algemeen/Geschiedenis/nationalekampioenen.php","_self","Nationale Kampioenen W&L"]);
stm_ep();
stm_aix("p1i4","p0i0",[0,"Sponsors","","",-1,-1,0,"http://www.badmintonsport.be/algemeen/sponsors.php","_self","Sponsors","","","",0,0,0,"","",0,0,0,1]);
stm_aix("p1i5","p0i0",[0,"Artikels","","",-1,-1,0,"http://www.badmintonsport.be/algemeen/artikels.php","_self","Artikels over de club","","","",0,0,0,"","",0,0,0,1]);
stm_ep();
stm_aix("p0i2","p0i0",[0,"Competitie","","",-1,-1,0,"http://www.badmintonsport.be/competitie.php","_self","Competitie: ploegen en uitslagen"]);
stm_aix("p0i3","p0i1",[0,"Tornooien","","",-1,-1,0,"http://www.badmintonsport.be/tornooien/tornooioverzicht.php","_self","Tornooien"]);
stm_bpx("p1","p0",[1,2,0,0,1,4,0]);
stm_aix("p1i0","p0i1",[0,"Inschrijvingen","","",-1,-1,0,"_self","Schrijf je hier in voor tornooien"]);
stm_bpx("p1","p0",[1,2,0,0,1,4,0,0]);
//-->
</script>
<?php
// create SQL statement 
   $sql = "SELECT id
                , organisatie
                , doelgroep
                , DATE_FORMAT(start_dt, '%d/%m') AS start_dt
                , verzonden
           FROM inschr_torn
		   WHERE DATE_ADD(dt_inschr, INTERVAL 21 DAY ) > NOW()
		   ORDER BY dt_inschr DESC";
   $result = mysql_query ($sql, $badm_db) or badm_mysql_die();
// format results by row
   $teller = 0;
   echo "<script type=\"text/javascript\" language=\"JavaScript1.2\">\n";
   while ($badm = mysql_fetch_object($result))
   {
      echo "stm_aix(\"p2i";
      echo $teller;
      echo "\",\"p1i0\",[1,\"";
      echo $badm->organisatie;
      if ($badm->doelgroep == 'J' || $badm->doelgroep == 'V')
      {
        echo $badm->doelgroep == 'J' ? " (Jeugd)" : " (Veteranen)";
        if ($badm->verzonden == 0) //inschrijvingsperiode nog niet afgelopen
        {
          echo " <B>".$badm->start_dt."</B>";
        }
        else
        {
          echo "  <IMG src=http://www.badmintonsport.be/images/closed.png>";
        }
      }
      else
      {
        if ($badm->verzonden == 0) //inschrijvingsperiode nog niet afgelopen
        {
          echo " <B>".$badm->start_dt."</B>";
        }
        else
        {
          echo "  <IMG src=http://www.badmintonsport.be/images/closed.png>";
        }
      }
      echo "\",\"\",\"\",-1,-1,0,\"http://www.badmintonsport.be/tornooien/inschrijvingen.php?id=";
      echo $badm->id;
      echo "\",\"_self\",\"Inschrijving ";
      echo $badm->organisatie;
      echo "\"]);\n";
      $teller = $teller + 1;
   }
   echo"</script>\n";
?>
<script type="text/javascript" language="JavaScript1.2">
<!--
stm_ep();
stm_aix("p1i1","p0i0",[0,"Aanvangsuren","","",-1,-1,0,"http://www.badmintonsport.be/tornooien/aanvangsuren.php","_self","Aanvangsuren tornooien"]);
stm_aix("p1i2","p0i0",[0,"Kalender","","",-1,-1,0,"http://www.badmintonsport.be/docs/tornooikalender.pdf","_blank","Tornooikalender"]);
stm_aix("p1i3","p0i1",[0,"Resultaten","","",-1,-1,0,"","_self","Tornooiresultaten W&L spelers"]);
stm_bpx("p2","p0",[1,2,0,0,1,4,0,0]);
//-->
</script>
<?php
// create SQL statement
    $sql = "SELECT  DISTINCT IF(DATE_FORMAT(dt_van, '%m') < 8
, DATE_FORMAT(dt_van, '%Y') - 1
, DATE_FORMAT(dt_van, '%Y')) AS start
, IF(DATE_FORMAT(dt_van, '%m') < 8
, DATE_FORMAT(dt_van, '%Y')
, DATE_FORMAT(dt_van, '%Y') + 1) AS einde
           FROM bad_tornooien
           ORDER  BY start DESC ";
   $result = mysql_query ($sql, $badm_db) or badm_mysql_die();
// format results by row
   $teller = 0;
   echo "<script type=\"text/javascript\" language=\"JavaScript\">\n";
   while ($badm = mysql_fetch_object($result))
   {
      echo "stm_aix(\"p2i".$teller."\",\"p1i0\",[0,\"Seizoen ".$badm->start."-".$badm->einde."\",\"\",\"\",-1,-1,0,\"http://www.badmintonsport.be/tornooien/resultaten.php?s=".$badm->einde."\",\"_self\",\"Seizoen ".$badm->start."-".$badm->einde."\"]);\n";
      $teller = $teller + 1;
   }
   echo"</script>\n";
   //mysql_free_result($result);
   mysql_close($badm_db);
?>
<script type="text/javascript" language="JavaScript">
<!--stm_ep();
stm_ep();
stm_ep();
stm_aix("p0i4","p0i0",[0,"Spelers","","",-1,-1,0,"http://www.badmintonsport.be/spelers.php","_self","Spelersprofielen en statistieken"]);
stm_aix("p0i5","p0i1",[0,"Jeugd","","",-1,-1,0,"","_self","Informatie over en voor de jeugdspelers"]);
stm_bpx("p1","p0",[1,2,0,0,1,4,0]);
stm_aix("p1i0","p0i0",[0,"Algemeen","","",-1,-1,0,"http://www.badmintonsport.be/jeugd/jeugd.php","_self","Jeugd trainers"]);
stm_aix("p1i1","p0i0",[0,"Teams","","",-1,-1,0,"http://www.badmintonsport.be/jeugd/jeugdteams.php","_self","Jeugdteams"]);
stm_aix("p1i2","p0i0",[0,"J-team","","",-1,-1,0,"http://www.badmintonsport.be/jeugd/J-team.php","_self","J-team"]);
stm_aix("p1i3","p0i0",[0,"Resultaten","","",-1,-1,0,"http://www.badmintonsport.be/jeugd/resultaten_jeugd.php?s=2008","_self","Resultaten jeugtornooien W&L spelers"]);
stm_aix("p1i4","p0i0",[0,"Jeugdtornooi","","",-1,-1,0,"http://www.badmintonsport.be/jeugd/jeugdtornooi.php","_self","Jeugdtornooi W&L"]);
stm_ep();
stm_aix("p0i6","p0i0",[0,"Recreanten","","",-1,-1,0,"http://www.badmintonsport.be/recreant.php","_self","Recreanten"]);
stm_aix("p0i7","p0i1",[0,"Foto's","","",-1,-1,0,"http://users.pandora.be/ikkegg","_blank","badminton foto's"]);
//-->
</script>
<?php
/*************************************************************************************************
 * Create a link to every photo directory that exists
 ************************************************************************************************/
/* Get the root dirctory of the website */
  $rootdir = $_SERVER['DOCUMENT_ROOT'];
/* directory where all the photo's are located in subdirectories */
  $dir = $rootdir."/pictures";
/* Get all the subdirectories of the pictures directory */
  include $rootdir."/functies/getDirectoryListing.php";
  $subdirs = getDirectoryListing($dir, "a", 1, 0, "none");
/* Number of subdirs in directory */
  $subdir_cnt = count($subdirs);
/* Display a link to every subdirectory */
  for ($i = 0; $i < $subdir_cnt; $i++)
  {
    echo "<script type=\"text/javascript\" language=\"JavaScript\">\n";
    if ($i == 0) // first subdirectory
    {
       echo "stm_bpx(\"p1\",\"p0\",[1,2,0,0,1,4,0]);\n";
    }
		echo "stm_aix(\"p0i".$i."\",\"p0i0\",[0,\"".$subdirs[$i]."\",\"\",\"\",-1,-1,0,\"http://www.badmintonsport.be/pictures/showThumbnails.php?dir=".$subdirs[$i]."\",\"_self\",\"".$subdirs[$i]."\"]);\n";
    if ($i == $subdir_cnt - 1) // last subdirectory
    {
      echo "stm_ep();\n";
    }
    echo"</script>\n";
  }
?>
<script type="text/javascript" language="JavaScript">
<!--
stm_ai("p0i8",[6,4,"#006633","",0,0,0]);
stm_aix("p0i9","p0i0",[0,"Links","","",-1,-1,0,"http://www.badmintonsport.be/links.php","_self","Links"]);
stm_aix("p0i10","p0i0",[0,"Message Board","","",-1,-1,0,"http://www.badmintonsport.be/messageboard/messageboard.php?p=1&m=n&id=0","_blank","Message Board"]);
stm_aix("p0i11","p0i0",[0,"Laatste Nieuws","","",-1,-1,0,"http://www.badmintonsport.be/laatste_nieuws.php","_self","De laatste wijzigingen aan de W&L website"]);
stm_aix("p0i12","p0i0",[0,"Team Veltem Motors","","",-1,-1,0,"http://www.everyoneweb.com/teamveltemmotors","_self","De fansite van Team Veltem Motors"]);
stm_ep();
stm_em();
//-->
</script>
      &nbsp;
 <!-- InstanceBeginEditable name="counter" --><!-- InstanceEndEditable -->
       <!-- InstanceBeginEditable name="wiens verjaardag is het" --><!-- InstanceEndEditable -->
  </td>
  <td width="100%" valign="top">
<!-- InstanceBeginEditable name="general" -->
      <h1><a name="top"></a>Medewerkers</h1>
      <table width="100%" border="1" cellpadding="0" cellspacing="0" bordercolor="#666699">
        <tr>
          <td width="25%" style="padding:10px"><p><a href="#eindredacteur">Eindredacteur</a></p></td>
          <td width="25%" style="padding:10px"><p><a href="#competitieverantwoordelijke">Competitieverantwoordelijke</a></p></td>
          <td width="25%" style="padding:10px"><p><a href="#tornooiverantwoordelijke">Tornooiverantwoordelijke</a></p></td>
        </tr>
        <tr>
          <td width="25%" style="padding:10px"><p><a href="#recreantenverantwoordelijke">recreantenverantwoordelijke</a></p></td>
          <td width="25%" style="padding:10px"><p><a href="#scheidsrechter">Internationaal scheidsrechter</a></p></td>
          <td width="25%" style="padding:10px"><p><a href="#webmasters">Webmasters</a></p></td>
        </tr>
        <tr>
          <td width="25%" style="padding:10px"><p><a href="#j-team">J-team</a></p></td>
          <td width="25%" style="padding:10px"><p><a href="#evenementen">Evenementen</a></p></td>
          <td width="25%" style="padding:10px"><p><a href="#kansengroepen">Co�rdinator kansengroepen</a></p></td>
        </tr>
      </table>
	  <br>
<?php
// Maak connectie met de database
  require_once "../functies/badm_db.inc.php";
  $badm_db = badm_conn_db();
?>
<?php
/**********************************************************
   Haal de gegevens van de hoofdredacteur op.
**********************************************************/
//$result = mysql_query("SELECT s.naam, s.adres, s.postcode, s.woonplaats, IF (s.gsm IS NULL, s.tel, tel) AS tel, s.email FROM bad_spelers s, clubfuncties f, leden_functies lf WHERE s.id = lf.spelers_id AND f.id = lf.functies_id AND f.functie = 'HOOFDREDACTEUR'", $badm_db);
?>
<!--            <table width="100%" border="1" cellpadding="0" cellspacing="0">
             <tr>
              <td colspan="2" align="left" valign="top" bgcolor="#666699" class="kleingeel">
               <a name="hoofdredacteur" id="hoofdredacteur"></a>Hoofdredacteur Boekske:&nbsp;
               <?php //echo mysql_result($result, 0, "naam"); ?>
              </td>
             </tr>
             <tr align="left" valign="top">
              <td width="30%" style="padding:10px">
               <p><strong>persoonlijke gegevens</strong>
                  <br />
                  <br />
                  <?php //echo mysql_result($result, 0, "adres"); ?><br />
                  <?php //echo mysql_result($result, 0, "postcode"); ?>&nbsp;<?php //echo mysql_result($result, 0, "woonplaats"); ?><br />
                  <?php //echo mysql_result($result, 0, "tel"); ?><br />
                  E-mail: <a href="mailto:<?php //echo mysql_result($result, 0, "email"); ?>"><?php //echo mysql_result($result, 0, "email"); ?></a><br />
               </p>
              </td>
              <td width="70%" style="padding:10px">
			  <p>&nbsp;</p></td>
             </tr>
            </table>
            <a href="#top"><img src="../images/top.gif" width="20" height="12" border="0" /></a>-->
<?php
/************************************************************
   Haal de gegevens van de co�rdinator voor kansengroepen op.
*************************************************************/
$result = mysql_query("SELECT s.naam, s.adres, s.postcode, s.woonplaats, IF (s.gsm IS NULL, s.tel, tel) AS tel, s.email, f.omschrijving FROM bad_spelers s, clubfuncties f, leden_functies lf WHERE s.id = lf.spelers_id AND f.id = lf.functies_id AND f.functie = 'KANSENGROEPEN'", $badm_db);
?>
            <table width="100%" border="1" cellpadding="0" cellspacing="0">
             <tr>
              <td colspan="2" align="left" valign="top" bgcolor="#666699" class="kleingeel">
               <a name="kansengroepen" id="kansengroepen"></a><?php echo mysql_result($result, 0, "omschrijving"); ?>:&nbsp;
               <?php echo mysql_result($result, 0, "naam"); ?>
              </td>
             </tr>
             <tr align="left" valign="top">
              <td width="30%" style="padding:10px">
               <p><strong>persoonlijke gegevens</strong>
                  <br />
                  <br />
                  <img src="../images/guidoclaes.jpg" height="136" align="top" /></p><p>
                  <?php echo mysql_result($result, 0, "adres"); ?><br />
                  <?php echo mysql_result($result, 0, "postcode"); ?>&nbsp;<?php echo mysql_result($result, 0, "woonplaats"); ?><br />
                  <?php echo mysql_result($result, 0, "tel"); ?><br />
                  E-mail: <a href="mailto:<?php echo mysql_result($result, 0, "email"); ?>"><?php echo mysql_result($result, 0, "email"); ?></a><br />
               </p>
              </td>
              <td width="70%" style="padding:10px"><p>Als hoofdtaak de sportparticipatie van deze bijzondere doelgroepen en hun aansluiting bij een sportclub te bevorderen
<br>Proberen het klassieke sportaanbod van de club eveneens naar een specifiek en duurzaam sportaanbod voor deze groepen te laten evolueren
<br>Proberen duurzame sportactiviteiten te organiseren voor deze groep met het oog op een blijvende aansluiting
<br>Proberen tevens de verschillende leeftijdsgroepen te bereiken
<br>Doelgericht promotie voeren 
<br>Doelgerichte opleiding en bijscholing volgen om betere sportgekwalificeerde trainers te hebben
<br>Netwerk proberen uit te breiden naar ouderenorganisatie, seniorenraad, enz...
<br>Aandacht besteden aan de aspecten van kansen en diversiteit en de zien hoe hij de aansluiting van maatschappelijk achtergestelde groepen kan verhogen.
</p></td>
             </tr>
            </table>
            <a href="#top"><img src="../images/top.gif" width="20" height="12" border="0" /></a>
<?php
/**********************************************************
   Haal de gegevens van de eindredacteur op.
**********************************************************/
$result = mysql_query("SELECT s.naam, s.adres, s.postcode, s.woonplaats, IF (s.gsm IS NULL, s.tel, tel) AS tel, s.email FROM bad_spelers s, clubfuncties f, leden_functies lf WHERE s.id = lf.spelers_id AND f.id = lf.functies_id AND f.functie = 'EINDREDACTEUR'", $badm_db);
?>
            <table width="100%" border="1" cellpadding="0" cellspacing="0">
             <tr>
              <td colspan="2" align="left" valign="top" bgcolor="#666699" class="kleingeel">
               <a name="eindredacteur" id="eindredacteur"></a>Eindredacteur Boekske:&nbsp;
               <?php echo mysql_result($result, 0, "naam"); ?>
              </td>
             </tr>
             <tr align="left" valign="top">
              <td width="30%" style="padding:10px">
               <p><strong>persoonlijke gegevens</strong>
                  <br />
                  <br />
                  <img src="../images/tomassimkens.jpg" height="136" align="top" /></p><p>
                  <?php echo mysql_result($result, 0, "adres"); ?><br />
                  <?php echo mysql_result($result, 0, "postcode"); ?>&nbsp;<?php echo mysql_result($result, 0, "woonplaats"); ?><br />
                  <?php echo mysql_result($result, 0, "tel"); ?><br />
                  E-mail: <a href="mailto:<?php echo mysql_result($result, 0, "email"); ?>"><?php echo mysql_result($result, 0, "email"); ?></a><br />
               </p>
              </td>
              <td width="70%" style="padding:10px">
			  <p>Drie maal per jaar zorg ik ervoor dat het clubblad �t Boekske verschijnt. 
			  Dan zoek ik uit welke artikels er in kunnen komen of zoek ik naar andere mensen die een artikeltje willen schrijven.</p>
              <p>Ik train altijd mee met de andere competitiespelers op woensdagavond van 20u tot 22u in Kessel-Lo.</p>
              </td>
             </tr>
            </table>
            <a href="#top"><img src="../images/top.gif" width="20" height="12" border="0" /></a>
<?php
/***********************************************
   Haal de gegevens van de scheidsrechter(s) op.
************************************************/
$result = mysql_query("SELECT s.naam, s.adres, s.postcode, s.woonplaats, IF (s.tel IS NULL, s.gsm, tel) AS tel, s.email FROM bad_spelers s, clubfuncties f, leden_functies lf WHERE s.id = lf.spelers_id AND f.id = lf.functies_id AND f.functie = 'INTERNAT_SCHEIDS'", $badm_db);
?>
      <table width="100%" border="1" cellpadding="0" cellspacing="0">
        <tr>
          <td colspan="2" align="left" valign="top" bgcolor="#666699" class="kleingeel" style="padding:10px">
           <a name="scheidsrechter" id="scheidsrechter"></a>Internationaal
            scheidsrechter: <?php echo mysql_result($result, 0, "naam"); ?></td>
        </tr>
        <tr align="left" valign="top">
          <td width="30%" style="padding:10px">
		   <p><strong><img src="../images/Vanhorenbeeck.jpg" width="120" height="132" /></strong></p>
            <p><strong>persoonlijke gegevens</strong></p>
            <p><?php echo mysql_result($result, 0, "adres"); ?><br />
              <?php echo mysql_result($result, 0, "postcode"); ?>&nbsp;<?php echo mysql_result($result, 0, "woonplaats"); ?><br />
              <?php echo mysql_result($result, 0, "tel"); ?><br />
              e-mail: <a href="mailto:<?php echo mysql_result($result, 0, "email"); ?>"><?php echo mysql_result($result, 0, "email"); ?></a><br />
              <br />
            </p></td>
          <td width="70%" style="padding:10px">
            <p>Bert is reeds vele jaren scheidsrechter, de meeste
              tornooispelers zullen hem wel al eens tegen het lijf gelopen zijn
              op een of ander tornooi in Belgi&euml;. </p>
            <p>Misschien minder geweten is dat hij ook op de Europese tornooien
              als <abbr lang="en" title="European Badminton Federation">EBU</abbr>-scheidsrechter (European Badminton Union) actief is en hierdoor
              regelmatig met de internationale top in contact komt.</p>
            <p>Bert is voorzitter van de commissie wedstrijdfunctionarissen in
              Vlaams-Brabant.</p>
            <p>In de functie van wedstrijdleider is hij ook regelmatig terug te
              vinden op competitiewedstrijden, vooral in de nationale competitie.</p>
            <ul>
              <li><abbr lang="en" title="Badminton World Federation">BWF</abbr> accredited umpire (2008)</li>
              <li><abbr lang="en" title="European Badminton Federation">EBU</abbr>-certificated Umpire: 19/04/2002</li>
              <li>Internationaal Scheidsrechter: 25/02/2001</li>
              <li>Nationaal Scheidsrechter: 22/02/1998</li>
              <li>Provinciaal Scheidsrechter: 06/11/1994</li>
              </ul>
		 </td>
        </tr>
      </table>
      <a href="#top"><img src="../images/top.gif" width="20" height="12" border="0" /></a>
<?php
/****************************************************************************************************
   Haal de gegevens van de tornooiverantwoordelijke op.  Deze persoon heeft level 2 in de userstabel.
*****************************************************************************************************/
$result = mysql_query("SELECT s.naam, s.adres, s.postcode, s.woonplaats, IF (s.tel IS NULL, s.gsm, tel) AS tel, s.email FROM bad_spelers s, clubfuncties f, leden_functies lf WHERE s.id = lf.spelers_id AND f.id = lf.functies_id AND f.functie = 'TORNOOI' AND ( lf.eind_dt IS NULL OR lf.eind_dt > NOW() )", $badm_db);
?>
      <table width="100%" border="1" cellpadding="0" cellspacing="0">
        <tr>
          <td colspan="2" align="left" valign="top" bgcolor="#666699" class="kleingeel" style="padding:10px">
		  <a name="tornooiverantwoordelijke" id="tornooiverantwoordelijke"></a>Tornooiverantwoordelijke (jeugd):
            <?php echo mysql_result($result, 0, "naam"); ?></td>
        </tr>
        <tr align="left" valign="top">
          <td width="30%" style="padding:10px">
               <p><strong>persoonlijke gegevens</strong>
                  <br />
                  <br />
                  <img src="../images/kristelvanroey.jpg" height="136" align="top" /></p><p>
            <p><?php echo mysql_result($result, 0, "naam"); ?><br />
              <?php echo mysql_result($result, 0, "adres"); ?><br />
              <?php echo mysql_result($result, 0, "postcode"); ?>&nbsp;<?php echo mysql_result($result, 0, "woonplaats"); ?><br />
              <?php echo mysql_result($result, 0, "tel"); ?></p>
            <p>E-mail: <a href="mailto:tornooien@badmintonsport.be">tornooien@badmintonsport.be</a> of <a href="mailto:<?php echo mysql_result($result, 0, "email"); ?>"><?php echo mysql_result($result, 0, "email"); ?></a><strong><br />
              </strong><br />
            </p></td>
          <td width="70%" style="padding:10px">
           <p>Als tornooiverantwoordelijke voor de jeugd leid ik inschrijvingen voor <abbr lang="nl" title="Vereniging voor Vlaams-Brabantse Badminton Clubs">VVBBC</abbr>-cups en jeugdtornooien in goede banen. 
		   Verder probeer ik nieuwe leden een beetje wegwijs te maken binnen het badmintonwereldje en binnen onze club.</p>
		   <p>Ik ben aanwezig wanneer mijn kinderen trainen. 
		   Dit is op maandag van 18u tot 20u, op woensdag van 14u tot 16u en op donderdag van 18u tot 20u, telkens in Veltem. 
		   Zelf speel ik recreatief op dinsdag van 19u tot 21u, ook in Veltem.</p>
		  </td>
        </tr>
      </table><a href="#top"><img src="../images/top.gif" width="20" height="12" border="0" /></a>
<?php
/**********************************************************
   Haal de gegevens van de competitieverantwoordelijke op.
**********************************************************/
$result = mysql_query("SELECT s.naam, s.adres, s.postcode, s.woonplaats, IF (s.gsm IS NULL, s.tel, tel) AS tel, s.email FROM bad_spelers s, clubfuncties f, leden_functies lf WHERE s.id = lf.spelers_id AND f.id = lf.functies_id AND f.functie = 'COMPETITIE'", $badm_db);
?>
            <table width="100%" border="1" cellpadding="0" cellspacing="0">
             <tr>
              <td colspan="2" align="left" valign="top" bgcolor="#666699" class="kleingeel">
               <a name="competitieverantwoordelijke" id="competitieverantwoordelijke"></a>Competitieverantwoordelijke:&nbsp;
               <?php echo mysql_result($result, 0, "naam"); ?>
              </td>
             </tr>
             <tr align="left" valign="top">
              <td width="30%" style="padding:10px">
               <p><strong>persoonlijke gegevens</strong>
                  <br />
                  <br />
                  <img src="../images/nelevervloet.jpg" height="136" align="top" /></p><p>
                  <?php echo mysql_result($result, 0, "adres"); ?><br />
                  <?php echo mysql_result($result, 0, "postcode"); ?>&nbsp;<?php echo mysql_result($result, 0, "woonplaats"); ?><br />
                  <?php echo mysql_result($result, 0, "tel"); ?><br />
                  E-mail: <a href="mailto:competitie@badmintonsport.be">competitie@badmintonsport.be</a> of <a href="mailto:<?php echo mysql_result($result, 0, "email"); ?>"><?php echo mysql_result($result, 0, "email"); ?></a><br />
               </p>
              </td>
              <td width="70%" style="padding:10px">
			  <p>Taken:</p>
			  <ul><li>spelers uitnodigen om competitie te spelen</li>
                  <li>competitieploegen vormen en ploegkapiteins zoeken</li>
                  <li>opvolgen competitiekalender</li>
                  <li>aanspreekpunt bij problemen ivm ontmoetingen of tekort aan spelers</li>
		      </ul>
              <p>Normaal train ik mee op woensdagavond van 20u tot 22u in Kessel-Lo en op donderdagavond van 20u tot 22u in Veltem. 
			  Maar door de zwangerschap heb ik nu mijn racket opgeborgen tot volgende zomer en ben ik vooral te vinden aan de zijlijn als supporter 
			  of eens in de cafetaria na een training</p>
			  </td>
             </tr>
            </table>
            <a href="#top"><img src="../images/top.gif" width="20" height="12" border="0" /></a>
<?php
/*********************************************************
   Haal de gegevens van de Recreantenverantwoordelijke op.
*********************************************************/
$result = mysql_query("SELECT s.naam, s.adres, s.postcode, s.woonplaats, IF (s.tel IS NULL, s.gsm, tel) AS tel, s.email FROM bad_spelers s, clubfuncties f, leden_functies lf WHERE s.id = lf.spelers_id AND f.id = lf.functies_id AND f.functie = 'RECREANT'", $badm_db);
?>
            <table width="100%" border="1" cellpadding="0" cellspacing="0">
             <tr>
              <td colspan="2" align="left" valign="top" bgcolor="#666699" class="kleingeel">
               <a name="recreantenverantwoordelijke" id="recreantenverantwoordelijke"></a>Recreantenverantwoordelijke:
              </td>
             </tr>
             <tr align="left" valign="top">
              <td width="30%" style="padding:10px">
               <p><strong>persoonlijke gegevens</strong>
                  <br />
                  <br />
                  <img src="../images/johaneyben.jpg" height="136" align="top" /></p><p>
                  <?php echo mysql_result($result, 0, "adres"); ?><br />
                  <?php echo mysql_result($result, 0, "postcode"); ?>&nbsp;<?php echo mysql_result($result, 0, "woonplaats"); ?><br />
                  <?php echo mysql_result($result, 0, "tel"); ?><br />
                  E-mail: <a href="mailto:<?php echo mysql_result($result, 0, "email"); ?>"><?php echo mysql_result($result, 0, "email"); ?></a><br />
               </p>
              </td>
              <td width="70%" style="padding:10px">
               <p>Ik verdedig de belangen van de recreanten op de bestuursvergaderingen. 
			   Ook wil ik, ondanks het uitblijven van enige reactie, een bruisende recreantenwerking in het leven roepen: 
			   recreantenontmoetingen, samen deelnemen aan recreantentornooien, ontmoetingsnamiddagen, ...</p>
               <p>Ik speel zelf vrij spel elke dinsdag van 21u tot 23u in Veltem. 
			   Op woensdag om 17u zet ik meestal mijn jongste zoon af voor zijn badmintontraining in Veltem.</p>
              </td>
             </tr>
            </table>
            <a href="#top"><img src="../images/top.gif" width="20" height="12" border="0" /></a>
<?php
/***********************************************
   Haal de gegevens van het J-team op.
************************************************/
$result = mysql_query("SELECT s.naam, s.adres, s.postcode, s.woonplaats, IF (s.tel IS NULL, s.gsm, s.tel) AS tel, s.email FROM bad_spelers s, clubfuncties f, leden_functies lf WHERE s.id = lf.spelers_id AND f.id = lf.functies_id AND f.functie = 'J-TEAM'", $badm_db);
while ($row = mysql_fetch_array($result, MYSQL_ASSOC))
{
  $data[] = $row;
}
?>
      <table width="100%" border="1" cellpadding="0" cellspacing="0">
        <tr>
          <td colspan="2" align="left" valign="top" bgcolor="#666699" class="kleingeel"><a name="j-team" id="j-team"></a>J-team:
<?php
  $names = "";
  for ($i=0; $i<count($data);$i++)
  {
    $names = " - ".$data[$i]["naam"];
  }
  echo substr($names, 3)
?></td>
        </tr>
        <tr align="left" valign="top">
          <td width="30%" style="padding:10px">
            <p><strong>persoonlijke gegevens</strong></p>
            <?php
  for ($i=0; $i<count($data);$i++)
  {
    echo "<p>";
    echo $data[$i]["naam"]."<br>\n";
  echo $data[$i]["adres"]."<br>\n";
  echo $data[$i]["postcode"]."&nbsp;".$data[$i]["woonplaats"]."<br>\n";
  echo $data[$i]["tel"]."\n";
  echo "<p>E-mail: <a href=\"mailto:".$data[$i]["email"]."\">".$data[$i]["email"]."</a><br>\n";
  }
?>
            <p>Nicole Cossey<br>
              e-mail: <a href="mailto:nicole.cossey@pandora.be">nicole.cossey@pandora.be</a>
            <p>Gilbert Adriaens<br>
              e-mail: <a href="mailto:gilbert@adriaens.net">gilbert@adriaens.net</a></td>
          <td width="70%" style="padding:10px">
<p>Wij zijn een groep ouders die zich samen inzetten voor een enthousiaste 
        en hechte <abbr title="Winksele en Leuven">W&amp;L</abbr>-jeugd.</p>
      <p>En wat doen wij zoal ?<br>
        De jeugdspelers en hun ouders helpen met hun wensen en vragen.<br>
        Regelmatig aanwezig zijn op de jeugdtrainingen en supporteren op de tornooien 
        van de <abbr title="Vereniging voor Vlaams-Brabantse Badminton Clubs">VVBBC</abbr>-Cup.<br>
        Een stevige hand toesteken bij ons Internationale Jeugdtornooi op 7 januari 
        2006.<br>
        Enkele verrassingsactiviteiten organiseren.<br>
        &#8230;<br>
        Heb je een goed idee ?<br>
        Heb je een inlichting nodig ?<br>
        Heb je een vraag voor &eacute;&eacute;n van de trainers ?<br>
        Wil je ons J-Team vervoegen ?</p>
      <p><font color="#008000"><strong>&#8230; A L TI J D W E L K O M !!</strong></font></p>
          </td>
        </tr>
      </table>
            <a href="#top"><img src="../images/top.gif" width="20" height="12" border="0" /></a>
<?php
/***********************************************
   Haal de gegevens van de webmasters op.
************************************************/
$result = mysql_query("SELECT s.naam, s.adres, s.postcode, s.woonplaats, IF (s.tel IS NULL, s.gsm, s.tel) AS tel, s.email FROM bad_spelers s, clubfuncties f, leden_functies lf WHERE s.id = lf.spelers_id AND f.id = lf.functies_id AND f.functie = 'WEBMASTER' AND lf.eind_dt IS NULL", $badm_db);
?>
      <table width="100%" border="1" cellpadding="0" cellspacing="0">
        <tr>
          <td colspan="2" align="left" valign="top" bgcolor="#666699" class="kleingeel"><a name="webmasters" id="webmasters"></a>Webmasters:
            <?php echo mysql_result($result, 0, "naam"); ?> - <?php echo mysql_result($result, 1, "naam"); ?></td>
        </tr>
        <tr align="left" valign="top">
          <td width="30%" style="padding:10px">
            <p><strong>persoonlijke gegevens</strong></p>
            <p><?php echo mysql_result($result, 0, "naam"); ?><br />
               <?php echo mysql_result($result, 0, "adres"); ?><br />
               <?php echo mysql_result($result, 0, "postcode"); ?>&nbsp;<?php echo mysql_result($result, 0, "woonplaats"); ?><br />
               <?php echo mysql_result($result, 0, "tel"); ?></p>
            <p><?php echo mysql_result($result, 1, "naam"); ?><br />
               <?php echo mysql_result($result, 1, "adres"); ?><br />
               <?php echo mysql_result($result, 1, "postcode"); ?>&nbsp;<?php echo mysql_result($result, 1, "woonplaats"); ?><br />
               <?php echo mysql_result($result, 1, "tel"); ?></p>
            <p>E-mail: <a href="mailto:webmaster@badmintonsport.be">webmaster@badmintonsport.be</a><br />
            </p>
            </td>
          <td width="70%" style="padding:10px">
<p>Na het vertrek van Rudy hebben Greet en Freek de web site
              van <abbr title="Winksele en Leuven">W&amp;L</abbr> in een nieuw kleedje gestoken.</p>
            <p>Freek zorgt voor de dynamische pagina's
              en Greet voor de layout.</p>
            <p>Er is &eacute;&eacute;n gezamenlijk e-mail adres waarop je reacties
              kwijt kan. We hopen dat je je weg vindt op deze site. </p>
            <p>We willen Rudy van harte danken voor zijn enthousiasme en de energie
              die hij in de <abbr title="Winksele en Leuven">W&amp;L</abbr> site heeft gestoken. </p>
          </td>
        </tr>
      </table>
      <a href="#top"><img src="../images/top.gif" width="20" height="12" border="0" /></a>
      <table width="100%" border="1" cellpadding="0" cellspacing="0">
        <tr>
          <td colspan="2" align="left" valign="top" bgcolor="#666699" class="kleingeel">
       <a name="evenementen" id="evenementen"></a>Evenementen:
          </td>
        </tr>
        <tr align="left" valign="top">
          <td width="30%" style="padding:10px">Organisatie Clubfeest</td>
          <td width="70%" style="padding:10px">Karolien Hauwaerts, Hilde Adriaens en Anke Gambart</td>
        </tr>
        <tr align="left" valign="top">
          <td width="30%" style="padding:10px">Organisatie Clubkampioenschap</td>
          <td width="70%" style="padding:10px">Greet Louw en Freek Ceymeulen</td>
        </tr>
        <tr align="left" valign="top">
          <td width="30%" style="padding:10px">Organisatie Jeugdtornooi</td>
          <td width="70%" style="padding:10px">Heleen Neerinckx</td>
        </tr>
        <tr align="left" valign="top">
          <td width="30%" style="padding:10px">Verantwoordelijke feestelijkheden</td>
          <td width="70%" style="padding:10px">Heleen Neerinckx</td>
        </tr>
        <tr align="left" valign="top">
          <td width="30%" style="padding:10px">Organisatie <abbr title="Winksele en Leuven">W&amp;L</abbr> quiz</td>
          <td width="70%" style="padding:10px">Greet Louw, Joeri Ven en Freek Ceymeulen</td>
        </tr>
      </table>
      <p>&nbsp;</p>
      <!-- InstanceEndEditable -->
  </td>
 </tr>
 <tr>
  <td height="0"></td>
  <td></td>
 </tr>
 <tr>
  <td height="1"><img src="http://www.badmintonsport.be/images/spacer.gif" alt="" width="150" height="1"></td>
  <td></td>
 </tr>
</table>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <!--DWLayoutTable-->
 <tr>
  <td width="100%" height="50" valign="top" bgcolor="#FFFFFF">
   <table width="100%" border="0" cellpadding="0" cellspacing="0" style="border-color: #CCCCCC" bgcolor="#CCCCCC">
       <!--DWLayoutTable-->
    <tr>
     <td align="center" valign="middle" style="border-color: #CCCCCC" bgcolor="#006600">
         <img src="http://www.badmintonsport.be/images/spacer.gif" width="200" height="4" alt=""></td>
    </tr>
    <tr>
     <td width="100%" align="center" valign="middle" style="border-color: #CCCCCC" bgcolor="#CCCCCC" class="gmenu">Last change:
 <!-- InstanceBeginEditable name="datum" -->
  <!-- #BeginDate format:It1 -->9-05-2012<!-- #EndDate -->
            <!-- InstanceEndEditable -->
             E-mail: <a href="mailto:webmaster@badmintonsport.be">webmaster@badmintonsport.be</a></td>
    </tr>
    <tr>
     <td align="center" valign="middle" style="border-color: #CCCCCC" bgcolor="#006600" class="gmenu">
          <img src="http://www.badmintonsport.be/images/spacer.gif" width="200" height="4" alt=""></td>
    </tr>
   </table>
  </td>
 </tr>
</table>
</body>
<!-- InstanceEnd --></html>
<?php
  log_website_usage($start_time, null, $badm_db);
  mysql_close($badm_db);
?>