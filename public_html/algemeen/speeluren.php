<?php
  require_once "../functies/website_usage.php";
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html lang="nl"><!-- InstanceBegin template="/Templates/templatesub1php.dwt.php" codeOutsideHTMLIsLocked="false" -->
<?php
    // Connect to database
    require_once "/home/badmin/public_html/functies/badm_db.inc.php";
    $badm_db = badm_conn_db();
?>
<head>
<META HTTP-EQUIV="Content-Type"      CONTENT="text/html; charset=iso-8859-1">
<META NAME="robots"                  CONTENT="index,follow">
<META NAME="keywords"                CONTENT="badminton,batminton,w&amp;l,badmintonvereniging,club,badmintonclub,sport,Belgium,Veltem,Herent,Leuven,Winksele,Belgi�">
<META NAME="description"             CONTENT="W&amp;L bv : De grootste en leukste badmintonclub van Brabant">
<META NAME="abstract"                CONTENT="badminton, club">
<META NAME="Area"                    CONTENT="General">
<META NAME="Author"                  CONTENT="W&amp;L bv">
<META NAME="Copyright"               CONTENT="(c) 2003-2004 W&amp;L bv">
<META NAME="Creator"                 CONTENT="W&amp;L bv">
<META NAME="Generator"               CONTENT="W&amp;L bv">
<META NAME="Owner"                   CONTENT="W&ampL bv">
<META NAME="Publisher"               CONTENT="W&ampL bv">
<META NAME="Rating"                  CONTENT="General">
<META NAME="revisit-after"           CONTENT="7 Days">
<META NAME="revisit"                 CONTENT="7 Days">
<META NAME="document-classification" CONTENT="Internet">
<META NAME="document-type"           CONTENT="Public">
<META NAME="document-rating"         CONTENT="Safe for Kids">
<META NAME="document-distribution"   CONTENT="Global">
<META HTTP-EQUIV="Content-Type"      CONTENT="text/html; charset=iso-8859-1">
<META HTTP-EQUIV="Audience"          CONTENT="General">
<META HTTP-EQUIV="content-language"  CONTENT="NL">
<META HTTP-EQUIV="Pragma"            CONTENT="no-cache">
<META HTTP-EQUIV="Expires"           CONTENT="now">
<!-- InstanceBeginEditable name="doctitle" -->
<title>W&amp;L badmintonvereniging vzw - speeluren</title>
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="head" --><!-- InstanceEndEditable -->
<link href="http://www.badmintonsport.be/css/badminton.css" rel="stylesheet" type="text/css">
<script type="text/javascript" language="JavaScript" src="http://www.badmintonsport.be/scripts/stm31.js"></script>
<script type="text/javascript" language="JavaScript" src="http://www.badmintonsport.be/scripts/dw_rotator.js"></script>
<script type="text/javascript" language="JavaScript">
<!--
function initRotator()
{
  var rotator1 = new dw_Rotator("sponsors_banner", 4000, "http://www.badmintonsport.be/images/", "_blank");
  rotator1.addImages("Veltem_Motors_banner.png", "banner_verhoeven.jpg");
  //rotator1.addImages("Veltem_Motors_banner.png");
  rotator1.addActions("http://www.veltem-motors.be", "http://www.isolatieverhoeven.be");
  //rotator1.addActions("http://www.veltem-motors.be");
  dw_Rotator.start();
}
function doLoad()
{
  // initialize the menu
  st_onload();
  // initialize the banner rotator
  initRotator();
}
//-->
</script>
</head>
<body class="badminton" onload="doLoad();">
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <!--DWLayoutTable-->
  <tr>
  <td colspan="2">
   <table width="100%" border="0" cellpadding="0" cellspacing="0">
    <!--DWLayoutTable-->
    <tr> 
     <td width="100%" style="background-color: #006600;"><img src="http://www.badmintonsport.be/images/pt_transp.gif" width="1px" height="4px" alt=""></td>
    </tr>
    <tr> 
     <td align="center" valign="top" bgcolor="#CCCCCC">
      <table width="100%" border="0" cellspacing="0" cellpadding="3">
       <tr align="center" valign="middle"> 
        <td rowspan="2" align="left">
	     <img src="http://www.badmintonsport.be/images/logow&amp;l-100.gif" width="100" height="80" alt="logo">
	    </td>
        <td>
	     <h4>W&amp;L Badmintonvereniging vzw<br>
	         Mechelsesteenweg 387<br>
             3020 Herent</h4>
		</td>
        <td align="right"><a href="javascript:void();" onclick="return dw_Rotator.doClick(0)"  onmouseover="dw_Rotator.pause(0)" onmouseout="dw_Rotator.resume(0)" onfocus="this.blur()"><img id="sponsors_banner" name="sponsors_banner" src="http://www.badmintonsport.be/images/Veltem_Motors_banner.png" alt="sponsors" width="320" height="100" border="0"></a></td>
       </tr>
      </table>
	 </td>
    </tr>
    <tr> 
     <td bgcolor="#336600"><img src="http://www.badmintonsport.be/images/pt_transp.gif" width="1px" height="4px" alt=""></td>
    </tr>
   </table>
  </td>
 </tr>
 <tr>
  <td width="150" height="213" valign="top">
<script type="text/javascript" language="JavaScript1.2">
<!--
stm_bm(["W&L menu",400,"","http://www.badmintonsport.be/scripts/blank.gif",0,"","",0,0,250,0,1000,1,0,0]);
stm_bp("p0",[1,4,0,0,1,4,0,7,100,"progid:DXImageTransform.Microsoft.Fade(overlap=.5,enabled=0,Duration=0.30)",-2,"progid:DXImageTransform.Microsoft.Pixelate(MaxSquare=15,enabled=0,Duration=0.50)",-2,50,0,0,"#669999","#006633","",3,0,0,"#ffffff"]);
stm_ai("p0i0",[0,"Home","","",-1,-1,0,"http://www.badmintonsport.be/index.php","_self","Startpagina","","","",0,0,0,"","",0,0,0,0,1,"#fffff7",0,"#b5bed6",0,"","",3,3,0,0,"#fffff7","#000000","#000000","#000000","10pt Verdana","10pt Verdana",0,0]);
stm_aix("p0i1","p0i0",[0,"Algemeen","","",-1,-1,0,"","_self","informatie over de club","","","",0,0,0,"http://www.badmintonsport.be/scripts/arrow_r.gif","http://www.badmintonsport.be/scripts/arrow_r.gif",7,7]);
stm_bpx("p1","p0",[1,2,0,0,1,4,0]);
stm_aix("p1i0","p0i0",[0,"Bestuur","","",-1,-1,0,"http://www.badmintonsport.be/algemeen/bestuur.php","_self","Bestuursleden","","","",0,0,0,"","",0,0,0,1]);
stm_bpx("p1","p0",[1,2,0,0,1,4,0,0]);
stm_aix("p2i0","p1i0",[0,"Mededelingen","","",-1,-1,0,"http://www.badmintonsport.be/algemeen/Bestuur/bestuursmededelingen.php","_self","Bestuursmededelingen"]);
stm_ep();
stm_aix("p1i1","p0i0",[0,"Medewerkers","","",-1,-1,0,"http://www.badmintonsport.be/algemeen/medewerkers.php","_self","Medewerkers","","","",0,0,0,"","",0,0,0,1]);
stm_aix("p1i2","p0i0",[0,"Speeluren","","",-1,-1,0,"http://www.badmintonsport.be/algemeen/speeluren.php","_self","Speeluren","","","",0,0,0,"","",0,0,0,1]);
stm_aix("p1i3","p0i1",[0,"Geschiedenis","","",-1,-1,0,"","_self","Geschiedenis","","","",0,0,0,"","",0,0,0,1]);
stm_bpx("p2","p0",[1,2,0,0,1,4,0,0]);
stm_aix("p2i0","p1i0",[0,"Clubkampioenen","","",-1,-1,0,"http://www.badmintonsport.be/algemeen/Geschiedenis/clubkampioenen.php","_self","Winnaars Clubkampioenschap"]);
stm_aix("p2i1","p1i0",[0,"Nationale Kampioenen","","",-1,-1,0,"http://www.badmintonsport.be/algemeen/Geschiedenis/nationalekampioenen.php","_self","Nationale Kampioenen W&L"]);
stm_ep();
stm_aix("p1i4","p0i0",[0,"Sponsors","","",-1,-1,0,"http://www.badmintonsport.be/algemeen/sponsors.php","_self","Sponsors","","","",0,0,0,"","",0,0,0,1]);
stm_aix("p1i5","p0i0",[0,"Artikels","","",-1,-1,0,"http://www.badmintonsport.be/algemeen/artikels.php","_self","Artikels over de club","","","",0,0,0,"","",0,0,0,1]);
stm_ep();
stm_aix("p0i2","p0i0",[0,"Competitie","","",-1,-1,0,"http://www.badmintonsport.be/competitie.php","_self","Competitie: ploegen en uitslagen"]);
stm_aix("p0i3","p0i1",[0,"Tornooien","","",-1,-1,0,"http://www.badmintonsport.be/tornooien/tornooioverzicht.php","_self","Tornooien"]);
stm_bpx("p1","p0",[1,2,0,0,1,4,0]);
stm_aix("p1i0","p0i1",[0,"Inschrijvingen","","",-1,-1,0,"_self","Schrijf je hier in voor tornooien"]);
stm_bpx("p1","p0",[1,2,0,0,1,4,0,0]);
//-->
</script>
<?php
// create SQL statement 
   $sql = "SELECT id
                , organisatie
                , doelgroep
                , DATE_FORMAT(start_dt, '%d/%m') AS start_dt
                , verzonden
           FROM inschr_torn
		   WHERE DATE_ADD(dt_inschr, INTERVAL 21 DAY ) > NOW()
		   ORDER BY dt_inschr DESC";
   $result = mysql_query ($sql, $badm_db) or badm_mysql_die();
// format results by row
   $teller = 0;
   echo "<script type=\"text/javascript\" language=\"JavaScript1.2\">\n";
   while ($badm = mysql_fetch_object($result))
   {
      echo "stm_aix(\"p2i";
      echo $teller;
      echo "\",\"p1i0\",[1,\"";
      echo $badm->organisatie;
      if ($badm->doelgroep == 'J' || $badm->doelgroep == 'V')
      {
        echo $badm->doelgroep == 'J' ? " (Jeugd)" : " (Veteranen)";
        if ($badm->verzonden == 0) //inschrijvingsperiode nog niet afgelopen
        {
          echo " <B>".$badm->start_dt."</B>";
        }
        else
        {
          echo "  <IMG src=http://www.badmintonsport.be/images/closed.png>";
        }
      }
      else
      {
        if ($badm->verzonden == 0) //inschrijvingsperiode nog niet afgelopen
        {
          echo " <B>".$badm->start_dt."</B>";
        }
        else
        {
          echo "  <IMG src=http://www.badmintonsport.be/images/closed.png>";
        }
      }
      echo "\",\"\",\"\",-1,-1,0,\"http://www.badmintonsport.be/tornooien/inschrijvingen.php?id=";
      echo $badm->id;
      echo "\",\"_self\",\"Inschrijving ";
      echo $badm->organisatie;
      echo "\"]);\n";
      $teller = $teller + 1;
   }
   echo"</script>\n";
?>
<script type="text/javascript" language="JavaScript1.2">
<!--
stm_ep();
stm_aix("p1i1","p0i0",[0,"Aanvangsuren","","",-1,-1,0,"http://www.badmintonsport.be/tornooien/aanvangsuren.php","_self","Aanvangsuren tornooien"]);
stm_aix("p1i2","p0i0",[0,"Kalender","","",-1,-1,0,"http://www.badmintonsport.be/docs/tornooikalender.pdf","_blank","Tornooikalender"]);
stm_aix("p1i3","p0i1",[0,"Resultaten","","",-1,-1,0,"","_self","Tornooiresultaten W&L spelers"]);
stm_bpx("p2","p0",[1,2,0,0,1,4,0,0]);
//-->
</script>
<?php
// create SQL statement
    $sql = "SELECT  DISTINCT IF(DATE_FORMAT(dt_van, '%m') < 8
, DATE_FORMAT(dt_van, '%Y') - 1
, DATE_FORMAT(dt_van, '%Y')) AS start
, IF(DATE_FORMAT(dt_van, '%m') < 8
, DATE_FORMAT(dt_van, '%Y')
, DATE_FORMAT(dt_van, '%Y') + 1) AS einde
           FROM bad_tornooien
           ORDER  BY start DESC ";
   $result = mysql_query ($sql, $badm_db) or badm_mysql_die();
// format results by row
   $teller = 0;
   echo "<script type=\"text/javascript\" language=\"JavaScript\">\n";
   while ($badm = mysql_fetch_object($result))
   {
      echo "stm_aix(\"p2i".$teller."\",\"p1i0\",[0,\"Seizoen ".$badm->start."-".$badm->einde."\",\"\",\"\",-1,-1,0,\"http://www.badmintonsport.be/tornooien/resultaten.php?s=".$badm->einde."\",\"_self\",\"Seizoen ".$badm->start."-".$badm->einde."\"]);\n";
      $teller = $teller + 1;
   }
   echo"</script>\n";
   //mysql_free_result($result);
   mysql_close($badm_db);
?>
<script type="text/javascript" language="JavaScript">
<!--stm_ep();
stm_ep();
stm_ep();
stm_aix("p0i4","p0i0",[0,"Spelers","","",-1,-1,0,"http://www.badmintonsport.be/spelers.php","_self","Spelersprofielen en statistieken"]);
stm_aix("p0i5","p0i1",[0,"Jeugd","","",-1,-1,0,"","_self","Informatie over en voor de jeugdspelers"]);
stm_bpx("p1","p0",[1,2,0,0,1,4,0]);
stm_aix("p1i0","p0i0",[0,"Algemeen","","",-1,-1,0,"http://www.badmintonsport.be/jeugd/jeugd.php","_self","Jeugd trainers"]);
stm_aix("p1i1","p0i0",[0,"Teams","","",-1,-1,0,"http://www.badmintonsport.be/jeugd/jeugdteams.php","_self","Jeugdteams"]);
stm_aix("p1i2","p0i0",[0,"J-team","","",-1,-1,0,"http://www.badmintonsport.be/jeugd/J-team.php","_self","J-team"]);
stm_aix("p1i3","p0i0",[0,"Resultaten","","",-1,-1,0,"http://www.badmintonsport.be/jeugd/resultaten_jeugd.php?s=2008","_self","Resultaten jeugtornooien W&L spelers"]);
stm_aix("p1i4","p0i0",[0,"Jeugdtornooi","","",-1,-1,0,"http://www.badmintonsport.be/jeugd/jeugdtornooi.php","_self","Jeugdtornooi W&L"]);
stm_ep();
stm_aix("p0i6","p0i0",[0,"Recreanten","","",-1,-1,0,"http://www.badmintonsport.be/recreant.php","_self","Recreanten"]);
stm_aix("p0i7","p0i1",[0,"Foto's","","",-1,-1,0,"http://users.pandora.be/ikkegg","_blank","badminton foto's"]);
//-->
</script>
<?php
/*************************************************************************************************
 * Create a link to every photo directory that exists
 ************************************************************************************************/
/* Get the root dirctory of the website */
  $rootdir = $_SERVER['DOCUMENT_ROOT'];
/* directory where all the photo's are located in subdirectories */
  $dir = $rootdir."/pictures";
/* Get all the subdirectories of the pictures directory */
  include $rootdir."/functies/getDirectoryListing.php";
  $subdirs = getDirectoryListing($dir, "a", 1, 0, "none");
/* Number of subdirs in directory */
  $subdir_cnt = count($subdirs);
/* Display a link to every subdirectory */
  for ($i = 0; $i < $subdir_cnt; $i++)
  {
    echo "<script type=\"text/javascript\" language=\"JavaScript\">\n";
    if ($i == 0) // first subdirectory
    {
       echo "stm_bpx(\"p1\",\"p0\",[1,2,0,0,1,4,0]);\n";
    }
		echo "stm_aix(\"p0i".$i."\",\"p0i0\",[0,\"".$subdirs[$i]."\",\"\",\"\",-1,-1,0,\"http://www.badmintonsport.be/pictures/showThumbnails.php?dir=".$subdirs[$i]."\",\"_self\",\"".$subdirs[$i]."\"]);\n";
    if ($i == $subdir_cnt - 1) // last subdirectory
    {
      echo "stm_ep();\n";
    }
    echo"</script>\n";
  }
?>
<script type="text/javascript" language="JavaScript">
<!--
stm_ai("p0i8",[6,4,"#006633","",0,0,0]);
stm_aix("p0i9","p0i0",[0,"Links","","",-1,-1,0,"http://www.badmintonsport.be/links.php","_self","Links"]);
stm_aix("p0i10","p0i0",[0,"Message Board","","",-1,-1,0,"http://www.badmintonsport.be/messageboard/messageboard.php?p=1&m=n&id=0","_blank","Message Board"]);
stm_aix("p0i11","p0i0",[0,"Laatste Nieuws","","",-1,-1,0,"http://www.badmintonsport.be/laatste_nieuws.php","_self","De laatste wijzigingen aan de W&L website"]);
stm_aix("p0i12","p0i0",[0,"Team Veltem Motors","","",-1,-1,0,"http://www.everyoneweb.com/teamveltemmotors","_self","De fansite van Team Veltem Motors"]);
stm_ep();
stm_em();
//-->
</script>
      &nbsp;
 <!-- InstanceBeginEditable name="counter" --><!-- InstanceEndEditable -->
       <!-- InstanceBeginEditable name="wiens verjaardag is het" --><!-- InstanceEndEditable -->
  </td>
  <td width="100%" valign="top">
<!-- InstanceBeginEditable name="general" -->
      <table width="100%" border="0" align="left" cellpadding="0" cellspacing="0">
        <!--DWLayoutTable-->
        <tr> 
          <td width="100%" height="213" valign="top" bgcolor="#FFFFFF"> 
            <h1>Speeluren</h1>
            <table width="100%" border="0" align="left" cellpadding="0" cellspacing="0">
        <!--DWLayoutTable-->
              <tr> 
                <td width="100%" height="213" valign="top" bgcolor="#FFFFFF"><p class="kleinetitel">Sluitingsdagen sporthallen Leuven (Heverlee): <a href="http://www.leuven.be/vrije-tijd/sport/sportaccommodatie/sporthallen-en-sportzalen/index.jsp" target="_blank">zie infopagina stad Leuven</a><span class="pzonderwit">.</span>
                  </p>
                  <p class="subtitel">Plannetjes en wegbeschrijving sporthallen</p>
                  <p>Volg 
                    deze link voor de <a href="../plan1.htm" target="_blank">adressen, 
                    plannetje en wegbeschrijving naar de sporthallen</a></p>
                  <p>Bij W&amp;L kan je op onderstaande dagen 
                    spelen.<br />
                    Om te weten of er plaats is, contacteer je best de secretaris: 
                    <a href="mailto:wlgclaes@yahoo.com">Guido Claes</a>, tel 016/228446</p>
                  <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#666699">
                    <tr> 
                      <td class="geel">Maandag </td>
                    </tr>
                  </table>
                  <table width="100%" border="1" cellpadding="0" cellspacing="0" bordercolor="#CCCCCC">
                    <tr bordercolor="#CCCCCC" bgcolor="#CCCCCC" class="kleinetitel"> 
                      <td width="20%"><strong>Sporthal</strong></td>
                      <td width="15%"><strong>Uur</strong></td>
                      <td width="30%"><strong>Wie</strong></td>
                      <td width="35%"><strong>Dagverantwoordelijke</strong></td>
                    </tr>
                    <tr bordercolor="#CCCCCC" class="kleinetekst"> 
                      <td width="20%">Heverlee (H.Hart-Hertogstraat 178)</td>
                      <td width="15%">21.00-23.00</td>
                      <td width="30%">Vrij spel seniors</td>
                      <td width="35%">Rudy Smets</td>
                    </tr>
                    <tr class="kleinetekst"> 
                      <td>Veltem-Beisem</td>
                      <td>17.00-18.00</td>
                      <td>Vrij spel jeugd</td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr class="kleinetekst"> 
                      <td>Veltem-Beisem</td>
                      <td>18.00-20.00</td>
                      <td>Training Jeugd</td>
                      <td>Nico Claes</td>
                    </tr>
                  </table>
                  <br> <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#666699">
                    <tr> 
                      <td class="geel">Dinsdag</td>
                    </tr>
                  </table>
                  <table width="100%" border="1" cellpadding="0" cellspacing="0" bordercolor="#CCCCCC">
                    <tr bordercolor="#CCCCCC" bgcolor="#CCCCCC" class="kleinetitel"> 
                      <td width="20%"><strong>Sporthal</strong></td>
                      <td width="15%"><strong>Uur</strong></td>
                      <td width="30%"><strong>Wie</strong></td>
                      <td width="35%"><strong>Dagverantwoordelijke</strong></td>
                    </tr>
                    <tr bordercolor="#CCCCCC" class="kleinetekst"> 
                      <td width="20%">Veltem-Beisem</td>
                      <td width="15%">17.00-18.00</td>
                      <td width="30%">Vrij spel jeugd tussen 6j en 11j</td>
                      <td width="35%">Nico Claes</td>
                    </tr>
                    <tr bordercolor="#CCCCCC" class="kleinetekst"> 
                      <td width="20%">Veltem-Beisem</td>
                      <td width="15%">18.00-19.00</td>
                      <td width="30%">Training Jeugd</td>
                      <td width="35%">Nico Claes</td>
                    </tr>
                    <tr class="kleinetekst"> 
                      <td>Veltem-Beisem</td>
                      <td>19.00-20.00</td>
                      <td>Vrij spel seniores</td>
                      <td>Kristel Van Roey</td>
                    </tr>
                    <tr bordercolor="#CCCCCC" class="kleinetekst"> 
                      <td width="20%">Veltem-Beisem</td>
                      <td width="15%">20.00-21.00</td>
                      <td width="30%">Vrij spel seniores</td>
                      <td width="35%">                        Mia Van Aerschot</td>
                    </tr>
                    <tr bordercolor="#CCCCCC" class="kleinetekst"> 
                      <td width="20%">Veltem-Beisem</td>
                      <td width="15%">21.00-23.00</td>
                      <td width="30%">Vrij spel seniores</td>
                      <td width="35%">Filip Durie<br>
                        Johan Feys<br>
                        Kris Raemaekers<br>
                        Kristin Zuallaert</td>
                    </tr>
                    <tr bordercolor="#CCCCCC" class="kleinetekst"> 
                      <td width="20%">Heverlee</td>
                      <td width="15%">21.00-23.00</td>
                      <td width="30%">Vrij spel seniores</td>
                      <td width="35%">Freek Ceymeulen</td>
                    </tr>
                  </table>
                  <br> <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#666699">
                    <tr> 
                      <td class="geel">Woensdag</td>
                    </tr>
                  </table>
                  <table width="100%" border="1" cellpadding="0" cellspacing="0" bordercolor="#CCCCCC">
                    <tr bordercolor="#CCCCCC" bgcolor="#CCCCCC" class="kleinetitel"> 
                      <td width="20%"><strong>Sporthal</strong></td>
                      <td width="15%"><strong>Uur</strong></td>
                      <td width="30%"><strong>Wie</strong></td>
                      <td width="35%"><strong>Dagverantwoordelijke</strong></td>
                    </tr>
                    <tr class="kleinetekst">
                      <td>Veltem-Beisem</td>
                      <td>14.00-18.00</td>
                      <td>Training jeugd</td>
                      <td>Guido Claes (14:00-16:00)<br>
                        Nico Claes (16:00-18:00)</td>
                    </tr>
                    <tr bordercolor="#CCCCCC" class="kleinetekst"> 
                      <td width="20%">Kessel-Lo</td>
                      <td width="15%">20.00-22.00</td>
                      <td width="30%">Intensieve training jeugd/seniores (hoger 
                        dan C1)</td>
                      <td width="35%">&nbsp;</td>
                    </tr>
                    <tr bordercolor="#CCCCCC" class="kleinetekst"> 
                      <td width="20%">Kessel-Lo</td>
                      <td width="15%">22.00-23.00</td>
                      <td width="30%">Vrij spel jeugd/seniores (hoger dan C1)</td>
                      <td width="35%">&nbsp;</td>
                    </tr>
                  </table>
                  <br> <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#666699">
                    <tr> 
                      <td class="geel">Donderdag</td>
                    </tr>
                  </table>
                  <table width="100%" border="1" cellpadding="0" cellspacing="0" bordercolor="#CCCCCC">
                    <tr bordercolor="#CCCCCC" bgcolor="#CCCCCC" class="kleinetitel"> 
                      <td width="20%"><strong>Sporthal</strong></td>
                      <td width="15%"><strong>Uur</strong></td>
                      <td width="30%"><strong>Wie</strong></td>
                      <td width="35%"><strong>Dagverantwoordelijke</strong></td>
                    </tr>
                    <tr bordercolor="#CCCCCC" class="kleinetekst"> 
                      <td>Veltem-Beisem</td>
                      <td>17.00-18.00</td>
                      <td>Intensieve training jeugd</td>
                      <td>Nico Claes</td>
                    </tr>
                    <tr bordercolor="#CCCCCC" class="kleinetekst"> 
                      <td width="20%">Veltem-Beisem</td>
                      <td width="15%">18.00-20.00</td>
                      <td width="30%">Intensieve training jeugd</td>
                      <td width="35%">Nico Claes</td>
                    </tr>
                    <tr bordercolor="#CCCCCC" class="kleinetekst"> 
                      <td width="20%">Veltem-Beisem</td>
                      <td width="15%">20.00-22.00</td>
                      <td width="30%">Training seniors</td>
                      <td width="35%">Sven Verhoeven</td>
                    </tr>
                  </table>
                  <br> <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#666699">
                    <tr> 
                      <td class="geel">Vrijdag</td>
                    </tr>
                  </table>
                  <table width="100%" border="1" cellpadding="0" cellspacing="0" bordercolor="#CCCCCC">
                    <tr bordercolor="#CCCCCC" bgcolor="#CCCCCC" class="kleinetitel"> 
                      <td width="20%"><strong>Sporthal</strong></td>
                      <td width="15%"><strong>Uur</strong></td>
                      <td width="30%"><strong>Wie</strong></td>
                      <td width="35%"><strong>Dagverantwoordelijke</strong></td>
                    </tr>
                    <tr bordercolor="#CCCCCC" class="kleinetekst"> 
                      <td width="20%">Veltem-Beisem</td>
                      <td width="15%">17.00-19.00</td>
                      <td width="30%">Training jeugd</td>
                      <td width="35%">Nico Claes</td>
                    </tr>
                    <tr bordercolor="#CCCCCC" class="kleinetekst"> 
                      <td width="20%">Veltem-Beisem</td>
                      <td width="15%">19.00-21.00</td>
                      <td width="30%">Vrij spel seniors</td>
                      <td width="35%">Bart Dehaese <br>
                        Pascal Vermeulen </td>
                    </tr>
                  </table>
                  <br> <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#666699">
                    <tr> 
                      <td class="geel">Zaterdag</td>
                    </tr>
                  </table>
                  <table width="100%" border="1" cellpadding="0" cellspacing="0" bordercolor="#CCCCCC">
                    <tr bordercolor="#CCCCCC" bgcolor="#CCCCCC" class="kleinetitel"> 
                      <td width="20%"><strong>Sporthal</strong></td>
                      <td width="15%"><strong>Uur</strong></td>
                      <td width="30%"><strong>Wie</strong></td>
                      <td width="35%"><strong>Dagverantwoordelijke</strong></td>
                    </tr>
                    <tr bordercolor="#CCCCCC" class="kleinetekst"> 
                      <td width="20%">Veltem-Beisem</td>
                      <td width="15%">10.00-12.00</td>
                      <td width="30%">Vrij spel seniors</td>
                      <td width="35%">Jef Uyttendaele</td>
                    </tr>
                </table></td>
        </tr>
      </table>
          </p></td>
        </tr>
      </table>
      <!-- InstanceEndEditable -->
  </td>
 </tr>
 <tr>
  <td height="0"></td>
  <td></td>
 </tr>
 <tr>
  <td height="1"><img src="http://www.badmintonsport.be/images/spacer.gif" alt="" width="150" height="1"></td>
  <td></td>
 </tr>
</table>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <!--DWLayoutTable-->
 <tr>
  <td width="100%" height="50" valign="top" bgcolor="#FFFFFF">
   <table width="100%" border="0" cellpadding="0" cellspacing="0" style="border-color: #CCCCCC" bgcolor="#CCCCCC">
       <!--DWLayoutTable-->
    <tr>
     <td align="center" valign="middle" style="border-color: #CCCCCC" bgcolor="#006600">
         <img src="http://www.badmintonsport.be/images/spacer.gif" width="200" height="4" alt=""></td>
    </tr>
    <tr>
     <td width="100%" align="center" valign="middle" style="border-color: #CCCCCC" bgcolor="#CCCCCC" class="gmenu">Last change:
 <!-- InstanceBeginEditable name="datum" -->
            <!-- #BeginDate format:It1 -->11-11-2010<!-- #EndDate -->
            <!-- InstanceEndEditable -->
             E-mail: <a href="mailto:webmaster@badmintonsport.be">webmaster@badmintonsport.be</a></td>
    </tr>
    <tr>
     <td align="center" valign="middle" style="border-color: #CCCCCC" bgcolor="#006600" class="gmenu">
          <img src="http://www.badmintonsport.be/images/spacer.gif" width="200" height="4" alt=""></td>
    </tr>
   </table>
  </td>
 </tr>
</table>
</body>
<!-- InstanceEnd --></html>
<?php
  require_once "../functies/badm_db.inc.php";
  $badm_db = badm_conn_db();
  log_website_usage($start_time, null, $badm_db);
  mysql_close($badm_db);
?>