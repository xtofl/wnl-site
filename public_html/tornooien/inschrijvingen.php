<?php
/**
 * inschrijvingen.php
 *
 * object     : Page with info and subscriptions about a tournament
 * author     : Freek Ceymeulen
 * created    : 08/06/2007
 * parameters : $id : id of tournament in inschr_torn table
 **/

  require_once "../functies/website_usage.php";
  // Connect to the DB (must be done before calling sess.php)
  require_once "../functies/badm_db.inc.php";
  $badm_db2 = badm_conn_db();
  require_once "../functies/sess.php";

  if ($_SESSION['auth'] == true)
  {
    $login = "Ingelogd als ".$_SESSION['username']." <a href=\"../login.php?action=logout\" title=\"Uitloggen\"><img src=\"../images/exit_16.png\" width=\"16px\" height=\"16px\" alt=\"Uitloggen\"></a>";
  }
  // Get the tournament info
  $query = "SELECT *, DATE_FORMAT(dt_inschr, '%%d/%%m/%%Y') AS inschrijvingsdatum FROM inschr_torn WHERE id = %d";
  $sql  = sprintf($query, mysql_real_escape_string($_REQUEST["id"]));
  $result = mysql_query($sql, $badm_db2) or badm_mysql_die();
  $numrows = mysql_num_rows($result);
  $tornooi = mysql_fetch_object($result);
  // Get the organizing club info
  $query = "SELECT * FROM bad_clubs WHERE naam = '%s'";
  $sql  = sprintf($query, mysql_real_escape_string($tornooi->organisatie));
  $result = mysql_query($sql, $badm_db2) or badm_mysql_die();
  $club = mysql_fetch_object($result);
  // Define organisation
  if (strlen($club->url) > 0)
  {
    $organisatie = "<a href=\"".$club->url."\" target=\"_blank\" title=\"Ga naar de website van ".$tornooi->organisatie."\">".$tornooi->organisatie."</a>";
  }
  else
  {
    $organisatie = $tornooi->organisatie;
  }
  // Define location
  if (strlen($club->plaats) > 0)
  {
    $plaats = "<a href=\"javascript:requestData('ajax_get_data.php?clubnaam=".str_replace("'", "\'", str_replace("&", "#AMP#", $tornooi->organisatie))."&rang=1', 'adresSporthal', 'clubnaam=".str_replace("'", "\'", str_replace("&", "#AMP#", $tornooi->organisatie))."&rang=1');\" title=\"Toon adres sporthal\">".$club->plaats."</a>";
  }
  else
  {
    $plaats = $tornooi->organisatie;
  }
  // Define info link
  if ($tornooi->info)
  {
    $doctype = strtolower(substr($tornooi->info, strpos($tornooi->info, '.', strlen($tornooi->info)-5) + 1));
    if ($doctype == "doc" || $doctype == "pdf" || $doctype == "xls")
    {
      $info = "<a href=\"".$tornooi->info."\" target=\"_blank\" title=\"Uitnodiging ".$tornooi->tornooi." (".$doctype.")\"><img src=\"../images/ico_".$doctype."_16.png\" width=\"16px\" height=\"16px\" alt=\"Uitnodiging ".$tornooi->tornooi." (".$doctype.")\"></a>";
    }
    else
    {
      $info = "<a href=\"".$tornooi->info."\" target=\"_blank\" title=\"Uitnodiging ".$tornooi->tornooi."\">Uitnodiging</a>";
    }
  }
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html lang="nl"><!-- InstanceBegin template="/Templates/templatesub1php.dwt.php" codeOutsideHTMLIsLocked="false" -->
<?php
    // Connect to database
    require_once "/home/badmin/public_html/functies/badm_db.inc.php";
    $badm_db = badm_conn_db();
?>
<head>
<META HTTP-EQUIV="Content-Type"      CONTENT="text/html; charset=iso-8859-1">
<META NAME="robots"                  CONTENT="index,follow">
<META NAME="keywords"                CONTENT="badminton,batminton,w&amp;l,badmintonvereniging,club,badmintonclub,sport,Belgium,Veltem,Herent,Leuven,Winksele,Belgi�">
<META NAME="description"             CONTENT="W&amp;L bv : De grootste en leukste badmintonclub van Brabant">
<META NAME="abstract"                CONTENT="badminton, club">
<META NAME="Area"                    CONTENT="General">
<META NAME="Author"                  CONTENT="W&amp;L bv">
<META NAME="Copyright"               CONTENT="(c) 2003-2004 W&amp;L bv">
<META NAME="Creator"                 CONTENT="W&amp;L bv">
<META NAME="Generator"               CONTENT="W&amp;L bv">
<META NAME="Owner"                   CONTENT="W&ampL bv">
<META NAME="Publisher"               CONTENT="W&ampL bv">
<META NAME="Rating"                  CONTENT="General">
<META NAME="revisit-after"           CONTENT="7 Days">
<META NAME="revisit"                 CONTENT="7 Days">
<META NAME="document-classification" CONTENT="Internet">
<META NAME="document-type"           CONTENT="Public">
<META NAME="document-rating"         CONTENT="Safe for Kids">
<META NAME="document-distribution"   CONTENT="Global">
<META HTTP-EQUIV="Content-Type"      CONTENT="text/html; charset=iso-8859-1">
<META HTTP-EQUIV="Audience"          CONTENT="General">
<META HTTP-EQUIV="content-language"  CONTENT="NL">
<META HTTP-EQUIV="Pragma"            CONTENT="no-cache">
<META HTTP-EQUIV="Expires"           CONTENT="now">
<!-- InstanceBeginEditable name="doctitle" -->
<title>W&amp;L badmintonvereniging vzw - Tornooi Inschrijvingen</title>
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="head" -->
<script type="text/javascript" language="JavaScript" src="http://www.badmintonsport.be/scripts/sorttable.js"></script>
<script type="text/javascript" language="JavaScript" src="http://www.badmintonsport.be/scripts/createXmlHttpRequest.js"></script>
<script type="text/javascript" language="JavaScript">
<!--
function popupFieldHelp(pURL,pname,pWidth,pHeight,pScroll,pResizable)
{
  if(!pURL){pURL = 'about:blank'}
  if(!pname){pname = 'Popup'}
  if(!pWidth){pWidth = 600}
  if(!pHeight){pHeight = 600}
  if(!pScroll){pScroll = 'yes'}
  if(!pResizable){pResizable = 'yes'}
  l_Window = window.open(pURL, pname, 'toolbar=no,scrollbars='+pScroll+',location=no,statusbar=no,menubar=no,resizable='+pResizable+',width='+pWidth+',height='+pHeight);
  if (l_Window.opener == null){l_Window.opener = self;}
  l_Window.focus();
  //  return l_Window;
  return;
}
function showHelp (pId)
{
  popupFieldHelp('http://www.badmintonsport.be/tornooien/help.php?id='+pId, null, 500, 350);
}
function processData($action)
{
  if ($action == "adresSporthal")
  {
    var response = requestObject.responseText;
    document.getElementById("adresSporthal").innerHTML = response;
  }
}
function removeSubscription(inschr_spel_id)
{
  var answer = confirm('Ben je zeker dat je deze inschrijving wil verwijderen?');
  if (answer)
  {
    window.location = 'http://www.badmintonsport.be/tornooien/inschrijving.php?tornooi_id=<?php echo $_REQUEST["id"]; ?>&actie=delete&inschr_spel_id='+inschr_spel_id;
  }
}
//-->
</script>
<!--
-->
<style type="text/css">
<!--
a img {
    border : 0;
}
#logout {
    float: right;
}
#tornooiInfoFrame {
    border : 1px solid #006600;
}
#sporthalInfoFrame {
    border : 1px solid #006600;
    padding : 10px;
}
#tornooiInfo {
    background-color : #EFEFEF;
}
#ingeschrevenen {
    border : 1px solid #006600;
    padding : 2;
    width : 100%;
}
#ingeschrevenen th {
    background-color : #EFEFEF;
    color : #800000; /* bordeaux */
}
.help {
    cursor : help;
    font-weight : bold;
    text-decoration : none;
    white-space : nowrap;
}
a.help {
    color : black;
    text-decoration : none;
}
#control {
      background-color : #cccccc;
      border           : 1px #999999 solid;
      height           : 24px;
      margin-bottom    : 10px;
      margin-top       : 10px;
      padding          : 3px;
      width            : 99%;
}
#closed {
    color : red;
    font-weight : bold;
}
table.xpbutton td.R {
    width : 4px;
}
table.xpbutton td.L {
    width : 4px;
}
table.xpbutton td.R img {
    display : block;
}
table.xpbutton td.L img {
    display : block;
}
table.xpbutton {
    color   : #333333;
    display : inline;
}
table.xpbutton td.C {
    background-image  : url(../images/button_xp_center.gif);
    background-repeat : repeat-x;
    white-space       : nowrap;
}
table.xpbutton td.C a {
    display         : block;
    font-family     : Verdana, Geneva, Arial, Helvetica, sans-serif;
    font-size       : 12px;
    font-weight     : bold;
    padding-left    : 3px;
    padding-right   : 3px;
    text-decoration : none;
    white-space     : nowrap;
}
table.xpbutton td.C a:visited {
    display         : block;
    font-family     : Verdana, Geneva, Arial, Helvetica, sans-serif;
    font-size       : 12px;
    font-weight     : bold;
    padding-left    : 3px;
    padding-right   : 3px;
    text-decoration : none;
    white-space     : nowrap;
}
-->
</style>
<!-- InstanceEndEditable -->
<link href="http://www.badmintonsport.be/css/badminton.css" rel="stylesheet" type="text/css">
<script type="text/javascript" language="JavaScript" src="http://www.badmintonsport.be/scripts/stm31.js"></script>
<script type="text/javascript" language="JavaScript" src="http://www.badmintonsport.be/scripts/dw_rotator.js"></script>
<script type="text/javascript" language="JavaScript">
<!--
function initRotator()
{
  var rotator1 = new dw_Rotator("sponsors_banner", 4000, "http://www.badmintonsport.be/images/", "_blank");
  rotator1.addImages("Veltem_Motors_banner.png", "china.jpg");
  rotator1.addActions("http://www.veltem-motors.be", "http://www.chinacapital.be");
  dw_Rotator.start();
}
function doLoad()
{
  // initialize the menu
  st_onload();
  // initialize the banner rotator
  initRotator();
}
//-->
</script>
</head>
<body class="badminton" onload="doLoad();">
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <!--DWLayoutTable-->
  <tr>
  <td colspan="2">
   <table width="100%" border="0" cellpadding="0" cellspacing="0">
    <!--DWLayoutTable-->
    <tr> 
     <td width="100%" style="background-color: #006600;"><img src="http://www.badmintonsport.be/images/pt_transp.gif" width="1px" height="4px" alt=""></td>
    </tr>
    <tr> 
     <td align="center" valign="top" bgcolor="#CCCCCC">
      <table width="100%" border="0" cellspacing="0" cellpadding="3">
       <tr align="center" valign="middle"> 
        <td rowspan="2" align="left">
	     <img src="http://www.badmintonsport.be/images/logow&amp;l-100.gif" width="100" height="80" alt="logo">
	    </td>
        <td>
	     <h4>W&amp;L Badmintonvereniging vzw<br>
	         Mechelsesteenweg 387<br>
             3020 Herent</h4>
		</td>
        <td align="right"><a href="javascript:void();" onclick="return dw_Rotator.doClick(0)"  onmouseover="dw_Rotator.pause(0)" onmouseout="dw_Rotator.resume(0)" onfocus="this.blur()"><img id="sponsors_banner" name="sponsors_banner" src="http://www.badmintonsport.be/images/Veltem_Motors_banner.png" alt="sponsors" width="320" height="100" border="0"></a></td>
       </tr>
      </table>
	 </td>
    </tr>
    <tr> 
     <td bgcolor="#336600"><img src="http://www.badmintonsport.be/images/pt_transp.gif" width="1px" height="4px" alt=""></td>
    </tr>
   </table>
  </td>
 </tr>
 <tr>
  <td width="150" height="213" valign="top">
<script type="text/javascript" language="JavaScript1.2">
<!--
stm_bm(["W&L menu",400,"","http://www.badmintonsport.be/scripts/blank.gif",0,"","",0,0,250,0,1000,1,0,0]);
stm_bp("p0",[1,4,0,0,1,4,0,7,100,"progid:DXImageTransform.Microsoft.Fade(overlap=.5,enabled=0,Duration=0.30)",-2,"progid:DXImageTransform.Microsoft.Pixelate(MaxSquare=15,enabled=0,Duration=0.50)",-2,50,0,0,"#669999","#006633","",3,0,0,"#ffffff"]);
stm_ai("p0i0",[0,"Home","","",-1,-1,0,"http://www.badmintonsport.be/index.php","_self","Startpagina","","","",0,0,0,"","",0,0,0,0,1,"#fffff7",0,"#b5bed6",0,"","",3,3,0,0,"#fffff7","#000000","#000000","#000000","10pt Verdana","10pt Verdana",0,0]);
stm_aix("p0i1","p0i0",[0,"Algemeen","","",-1,-1,0,"","_self","informatie over de club","","","",0,0,0,"http://www.badmintonsport.be/scripts/arrow_r.gif","http://www.badmintonsport.be/scripts/arrow_r.gif",7,7]);
stm_bpx("p1","p0",[1,2,0,0,1,4,0]);
stm_aix("p1i0","p0i0",[0,"Bestuur","","",-1,-1,0,"http://www.badmintonsport.be/algemeen/bestuur.php","_self","Bestuursleden","","","",0,0,0,"","",0,0,0,1]);
stm_bpx("p1","p0",[1,2,0,0,1,4,0,0]);
stm_aix("p2i0","p1i0",[0,"Mededelingen","","",-1,-1,0,"http://www.badmintonsport.be/algemeen/Bestuur/bestuursmededelingen.php","_self","Bestuursmededelingen"]);
stm_ep();
stm_aix("p1i1","p0i0",[0,"Medewerkers","","",-1,-1,0,"http://www.badmintonsport.be/algemeen/medewerkers.php","_self","Medewerkers","","","",0,0,0,"","",0,0,0,1]);
stm_aix("p1i2","p0i0",[0,"Speeluren","","",-1,-1,0,"http://www.badmintonsport.be/algemeen/speeluren.php","_self","Speeluren","","","",0,0,0,"","",0,0,0,1]);
stm_aix("p1i3","p0i1",[0,"Geschiedenis","","",-1,-1,0,"","_self","Geschiedenis","","","",0,0,0,"","",0,0,0,1]);
stm_bpx("p2","p0",[1,2,0,0,1,4,0,0]);
stm_aix("p2i0","p1i0",[0,"Clubkampioenen","","",-1,-1,0,"http://www.badmintonsport.be/algemeen/Geschiedenis/clubkampioenen.php","_self","Winnaars Clubkampioenschap"]);
stm_aix("p2i1","p1i0",[0,"Nationale Kampioenen","","",-1,-1,0,"http://www.badmintonsport.be/algemeen/Geschiedenis/nationalekampioenen.php","_self","Nationale Kampioenen W&L"]);
stm_ep();
stm_aix("p1i4","p0i0",[0,"Sponsors","","",-1,-1,0,"http://www.badmintonsport.be/algemeen/sponsors.php","_self","Sponsors","","","",0,0,0,"","",0,0,0,1]);
stm_aix("p1i5","p0i0",[0,"Artikels","","",-1,-1,0,"http://www.badmintonsport.be/algemeen/artikels.php","_self","Artikels over de club","","","",0,0,0,"","",0,0,0,1]);
stm_ep();
stm_aix("p0i2","p0i0",[0,"Competitie","","",-1,-1,0,"http://www.badmintonsport.be/competitie.php","_self","Competitie: ploegen en uitslagen"]);
stm_aix("p0i3","p0i1",[0,"Tornooien","","",-1,-1,0,"http://www.badmintonsport.be/tornooien/tornooioverzicht.php","_self","Tornooien"]);
stm_bpx("p1","p0",[1,2,0,0,1,4,0]);
stm_aix("p1i0","p0i1",[0,"Inschrijvingen","","",-1,-1,0,"_self","Schrijf je hier in voor tornooien"]);
stm_bpx("p1","p0",[1,2,0,0,1,4,0,0]);
//-->
</script>
<?php
// create SQL statement 
   $sql = "SELECT id
                , organisatie
                , doelgroep
                , DATE_FORMAT(start_dt, '%d/%m') AS start_dt
                , verzonden
           FROM inschr_torn
		   WHERE DATE_ADD(dt_inschr, INTERVAL 21 DAY ) > NOW()
		   ORDER BY dt_inschr DESC";
   $result = mysql_query ($sql, $badm_db) or badm_mysql_die();
// format results by row
   $teller = 0;
   echo "<script type=\"text/javascript\" language=\"JavaScript1.2\">\n";
   while ($badm = mysql_fetch_object($result))
   {
      echo "stm_aix(\"p2i";
      echo $teller;
      echo "\",\"p1i0\",[1,\"";
      echo $badm->organisatie;
      if ($badm->doelgroep == 'J' || $badm->doelgroep == 'V')
      {
        echo $badm->doelgroep == 'J' ? " (Jeugd)" : " (Veteranen)";
        if ($badm->verzonden == 0) //inschrijvingsperiode nog niet afgelopen
        {
          echo " <B>".$badm->start_dt."</B>";
        }
        else
        {
          echo "  <IMG src=http://www.badmintonsport.be/images/closed.png>";
        }
      }
      else
      {
        if ($badm->verzonden == 0) //inschrijvingsperiode nog niet afgelopen
        {
          echo " <B>".$badm->start_dt."</B>";
        }
        else
        {
          echo "  <IMG src=http://www.badmintonsport.be/images/closed.png>";
        }
      }
      echo "\",\"\",\"\",-1,-1,0,\"http://www.badmintonsport.be/tornooien/inschrijvingen.php?id=";
      echo $badm->id;
      echo "\",\"_self\",\"Inschrijving ";
      echo $badm->organisatie;
      echo "\"]);\n";
      $teller = $teller + 1;
   }
   echo"</script>\n";
?>
<script type="text/javascript" language="JavaScript1.2">
<!--
stm_ep();
stm_aix("p1i1","p0i0",[0,"Aanvangsuren","","",-1,-1,0,"http://www.badmintonsport.be/tornooien/aanvangsuren.php","_self","Aanvangsuren tornooien"]);
stm_aix("p1i2","p0i0",[0,"Kalender","","",-1,-1,0,"http://www.badmintonsport.be/docs/tornooikalender.pdf","_blank","Tornooikalender"]);
stm_aix("p1i3","p0i1",[0,"Resultaten","","",-1,-1,0,"","_self","Tornooiresultaten W&L spelers"]);
stm_bpx("p2","p0",[1,2,0,0,1,4,0,0]);
//-->
</script>
<?php
// create SQL statement
    $sql = "SELECT  DISTINCT IF(DATE_FORMAT(dt_van, '%m') < 8
, DATE_FORMAT(dt_van, '%Y') - 1
, DATE_FORMAT(dt_van, '%Y')) AS start
, IF(DATE_FORMAT(dt_van, '%m') < 8
, DATE_FORMAT(dt_van, '%Y')
, DATE_FORMAT(dt_van, '%Y') + 1) AS einde
           FROM bad_tornooien
           ORDER  BY start DESC ";
   $result = mysql_query ($sql, $badm_db) or badm_mysql_die();
// format results by row
   $teller = 0;
   echo "<script type=\"text/javascript\" language=\"JavaScript\">\n";
   while ($badm = mysql_fetch_object($result))
   {
      echo "stm_aix(\"p2i".$teller."\",\"p1i0\",[0,\"Seizoen ".$badm->start."-".$badm->einde."\",\"\",\"\",-1,-1,0,\"http://www.badmintonsport.be/tornooien/resultaten.php?s=".$badm->einde."\",\"_self\",\"Seizoen ".$badm->start."-".$badm->einde."\"]);\n";
      $teller = $teller + 1;
   }
   echo"</script>\n";
   //mysql_free_result($result);
   mysql_close($badm_db);
?>
<script type="text/javascript" language="JavaScript">
<!--stm_ep();
stm_ep();
stm_ep();
stm_aix("p0i4","p0i0",[0,"Spelers","","",-1,-1,0,"http://www.badmintonsport.be/spelers.php","_self","Spelersprofielen en statistieken"]);
stm_aix("p0i5","p0i1",[0,"Jeugd","","",-1,-1,0,"","_self","Informatie over en voor de jeugdspelers"]);
stm_bpx("p1","p0",[1,2,0,0,1,4,0]);
stm_aix("p1i0","p0i0",[0,"Algemeen","","",-1,-1,0,"http://www.badmintonsport.be/jeugd/jeugd.php","_self","Jeugd trainers"]);
stm_aix("p1i1","p0i0",[0,"Teams","","",-1,-1,0,"http://www.badmintonsport.be/jeugd/jeugdteams.php","_self","Jeugdteams"]);
stm_aix("p1i2","p0i0",[0,"J-team","","",-1,-1,0,"http://www.badmintonsport.be/jeugd/J-team.php","_self","J-team"]);
stm_aix("p1i3","p0i0",[0,"Resultaten","","",-1,-1,0,"http://www.badmintonsport.be/jeugd/resultaten_jeugd.php?s=2008","_self","Resultaten jeugtornooien W&L spelers"]);
stm_aix("p1i4","p0i0",[0,"Jeugdtornooi","","",-1,-1,0,"http://www.badmintonsport.be/jeugd/jeugdtornooi.php","_self","Jeugdtornooi W&L"]);
stm_ep();
stm_aix("p0i6","p0i0",[0,"Recreanten","","",-1,-1,0,"http://www.badmintonsport.be/recreant.php","_self","Recreanten"]);
stm_aix("p0i7","p0i1",[0,"Foto's","","",-1,-1,0,"http://users.pandora.be/ikkegg","_blank","badminton foto's"]);
//-->
</script>
<?php
/*************************************************************************************************
 * Create a link to every photo directory that exists
 ************************************************************************************************/
/* Get the root dirctory of the website */
  $rootdir = $_SERVER['DOCUMENT_ROOT'];
/* directory where all the photo's are located in subdirectories */
  $dir = $rootdir."/pictures";
/* Get all the subdirectories of the pictures directory */
  include $rootdir."/functies/getDirectoryListing.php";
  $subdirs = getDirectoryListing($dir, "a", 1, 0, "none");
/* Number of subdirs in directory */
  $subdir_cnt = count($subdirs);
/* Display a link to every subdirectory */
  for ($i = 0; $i < $subdir_cnt; $i++)
  {
    echo "<script type=\"text/javascript\" language=\"JavaScript\">\n";
    if ($i == 0) // first subdirectory
    {
       echo "stm_bpx(\"p1\",\"p0\",[1,2,0,0,1,4,0]);\n";
    }
		echo "stm_aix(\"p0i".$i."\",\"p0i0\",[0,\"".$subdirs[$i]."\",\"\",\"\",-1,-1,0,\"http://www.badmintonsport.be/pictures/showThumbnails.php?dir=".$subdirs[$i]."\",\"_self\",\"".$subdirs[$i]."\"]);\n";
    if ($i == $subdir_cnt - 1) // last subdirectory
    {
      echo "stm_ep();\n";
    }
    echo"</script>\n";
  }
?>
<script type="text/javascript" language="JavaScript">
<!--
stm_ai("p0i8",[6,4,"#006633","",0,0,0]);
stm_aix("p0i9","p0i0",[0,"Links","","",-1,-1,0,"http://www.badmintonsport.be/links.php","_self","Links"]);
stm_aix("p0i10","p0i0",[0,"Message Board","","",-1,-1,0,"http://www.badmintonsport.be/messageboard/messageboard.php?p=1&m=n&id=0","_blank","Message Board"]);
stm_aix("p0i11","p0i0",[0,"Laatste Nieuws","","",-1,-1,0,"http://www.badmintonsport.be/laatste_nieuws.php","_self","De laatste wijzigingen aan de W&L website"]);
stm_ep();
stm_em();
//-->
</script>
      &nbsp;
 <!-- InstanceBeginEditable name="counter" -->
<!-- InstanceEndEditable -->
       <!-- InstanceBeginEditable name="wiens verjaardag is het" -->
<!-- InstanceEndEditable -->
  </td>
  <td width="100%" valign="top">
<!-- InstanceBeginEditable name="general" -->
   <table width="100%" border="0" align="left" cellpadding="0" cellspacing="0">
    <tr>
     <td width="100%" height="100%">

<div id="logout" class="kleinetekst"><?php echo $login; ?></div>
<h1><?php echo $tornooi->tornooi; ?></h1>

<table width="100%">
 <tr>
  <td>
  <table id="tornooiInfoFrame">
   <tr>
    <td>
     <table id="tornooiInfo" class="kleinetekst">
      <tr>
       <td>Datum</td>
       <td>&nbsp;:&nbsp;</td>
       <td><?php echo $tornooi->datum; ?></td>
      </tr>
      <tr>
       <td>Organisatie</td>
       <td>&nbsp;:&nbsp;</td>
       <td><?php echo $organisatie; ?></td>
      </tr>
      <tr>
       <td><a class="help" href="javascript:showHelp('1')">Type</a></td>
       <td>&nbsp;:&nbsp;</td>
       <td><?php echo $tornooi->TYPE; ?></td>
      </tr>
      <tr>
       <td>Plaats</td>
       <td>&nbsp;:&nbsp;</td>
       <td><?php echo $plaats; ?></td>
      </tr>
      <tr>
       <td><a class="help" href="javascript:showHelp('2')">Reeksen</a></td>
       <td>&nbsp;:&nbsp;</td>
       <td><?php echo $tornooi->reeks_dis; ?></td>
      </tr>
      <tr>
       <td>Opmerking</td>
       <td>&nbsp;:&nbsp;</td>
       <td><?php echo $tornooi->opmerking; ?></td>
      </tr>
      <tr>
       <td>Meer info</td>
       <td>&nbsp;:&nbsp;</td>
       <td><?php echo $info; ?></td>
      </tr>
      <tr>
       <td>Inschrijven voor</td>
       <td>&nbsp;:&nbsp;</td>
       <td><?php echo $tornooi->inschrijvingsdatum; ?></td>
      </tr>
     </table>
    </td>
   </tr>
  </table>
  </td>
  <td id="adresSporthal"></td>
 </tr>
</table>
<div id="control">
 <table class="xpbutton" cellspacing="0" cellpadding="0">
  <tr>
<?php
  if ($tornooi->verzonden == '0')
  {
    echo "   <td class=\"L\"><img src=\"../images/button_xp_left.gif\" width=\"4px\" height=\"24px\" alt=\"\" /></td>\n";
    echo "   <td class=\"C\"><a href=\"inschrijving.php?tornooi_id=".$_REQUEST["id"]."\">Inschrijven</a></td>\n";
    echo "   <td class=\"R\"><img src=\"../images/button_xp_right.gif\" width=\"4px\" height=\"24px\" alt=\"\" /></td>\n";
  }
  else
  {
    echo "   <td id=\"closed\">Deze inschrijving is afgesloten</td>\n";
  }
?>
  </tr>
 </table>
</div>

<table id="ingeschrevenen" class="sortable kleinetekst">
 <tbody>
  <tr>
<?php
  if ($tornooi->verzonden == '0')
  {
    echo "   <th>&nbsp;</th>\n";
  }
?>
   <th>Naam</th>
   <th>Klas</th>
   <th>Lidnr</th>
<?php
  if ($tornooi->zelfde_disc_mag == 'j')
  {
?>
   <th>Disc.&nbsp;1</th>
   <th>Disc.&nbsp;2</th>
   <th>Disc.&nbsp;3</th>
<?php
  }
  else
  {
?>
   <th>E</th>
   <th>D</th>
   <th>G</th>
<?php
  }
  if ($tornooi->zelfde_disc_mag == 'j')
  {
?>
   <th>Partner</th>
<?php
  }
?>
   <th>Dubbelpartner</th>
   <th>Mixedpartner</th>
<?php
  if ($tornooi->verzonden == '0')
  {
    echo "   <th>&nbsp;</th>\n";
  }
?>
  </tr>
<?php
  // Store the name of all enrolled players for this tournament in an array
  // Later, we use this array to check if the partners are enrolled themselves
  $query = "SELECT naam
              FROM inschr_spel
             WHERE inschr_torn_id = %d";
  $sql  = sprintf($query, mysql_real_escape_string($_REQUEST["id"], $badm_db));
  $result = mysql_query($sql, $badm_db2) or badm_mysql_die();
  while ($names = mysql_fetch_row($result))
  {
    $name_array[] = $names[0];
  }
  // Get the enrolled players
  $query = "SELECT id
                 , naam
                 , lidnr
                 , klassement
                 , IF (enkel = '0', '&nbsp;', 'X') AS enkel
                 , IF (dubbel = '0', '&nbsp;', 'X') AS dubbel
                 , IF (gemengd = '0', '&nbsp;', 'X') AS gemengd
                 , partner
                 , dub_partner
                 , mix_partner
                 , clubE
                 , clubD
                 , lidnrE
                 , lidnrD
                 , clubG
                 , lidnrG
                 , discipline1
                 , discipline2
                 , discipline3
              FROM inschr_spel
             WHERE inschr_torn_id = %d
             ORDER BY klassement, naam";
  $sql  = sprintf($query, mysql_real_escape_string($_REQUEST["id"], $badm_db));
  $result = mysql_query($sql, $badm_db2) or badm_mysql_die();
  // If nobody has enrolled, inform about this
  if ( mysql_num_rows($result) == 0)
  {
    echo "  <tr>\n";
    // Check if tournament is closed
    if ($tornooi->verzonden == '0')
    {
      // if not, columns with edit and delete icon are displayed
      echo "   <td colspan=\"10\"><p class=\"kleinetekst\">Er heeft zich nog niemand ingeschreven voor dit tornooi</p></td>\n";
    }
    else
    {
      echo "   <td colspan=\"8\"><p class=\"kleinetekst\">Er heeft zich niemand ingeschreven voor dit tornooi</p></td>\n";
    }
    echo "  </tr>\n";
  }
  // Loop over all enrolled players
  while ($speler = mysql_fetch_object($result))
  {
    // Initialize
    $clubinfoE = "";
    $warningE = "";
    $clubinfoD = "";
    $warningD = "";
    $clubinfoG = "";
    $warningG = "";
    // Build partner info
    if (strlen($speler->partner) > 0 && strtolower($speler->partner) != 'x')
    {
      if ($speler->clubE != "W&L bv" && $speler->clubE != "W&L")
      {
        $clubinfoE = " <span nowrap>(".$speler->clubE.",&nbsp;".$speler->lidnrE.")</span>";
      }
      else
      {
        // Check if double partner is enrolled
        if (!in_array($speler->partner, $name_array))
        {
          // If not, display warning icon
          $warningE = " <img src=\"../images/warning_16.png\" alt=\"Deze speler is zelf nog niet ingeschreven\" title=\"Deze speler is zelf nog niet ingeschreven\">";
        }
      }
    }
    // Build double partner info
    if (strlen($speler->dub_partner) > 0 && strtolower($speler->dub_partner) != 'x')
    {
      if ($speler->clubD != "W&L bv" && $speler->clubD != "W&L")
      {
        $clubinfoD = " <span nowrap>(".$speler->clubD.",&nbsp;".$speler->lidnrD.")</span>";
      }
      else
      {
        // Check if double partner is enrolled
        if (!in_array($speler->dub_partner, $name_array))
        {
          // If not, display warning icon
          $warningD = " <img src=\"../images/warning_16.png\" alt=\"Deze speler is zelf nog niet ingeschreven\" title=\"Deze speler is zelf nog niet ingeschreven\">";
        }
      }
    }
    // Build mixed partner info
    if (strlen($speler->mix_partner) > 0 && strtolower($speler->mix_partner) != 'x')
    {
      if ($speler->clubG != "W&L bv" && $speler->clubG != "W&L")
      {
        $clubinfoG = " <span nowrap>(".$speler->clubG.",&nbsp;".$speler->lidnrG.")</span>";
      }
      else
      {
        // Check if mixed partner is enrolled
        if (!in_array($speler->mix_partner, $name_array))
        {
          // If not, display warning icon
          $warningG = " <img src=\"../images/warning_16.png\" alt=\"Deze speler is zelf nog niet ingeschreven\" title=\"Deze speler is zelf nog niet ingeschreven\">";
        }
      }
    }
    echo "  <tr>\n";
    // Check if tournament is closed
    if ($tornooi->verzonden == '0')
    {
      // if not, display edit icon
      echo "   <td><a href=\"inschrijving.php?tornooi_id=".$_REQUEST["id"]."&inschr_spel_id=".$speler->id."\" title=\"Wijzigen\"><img src=\"../images/edit_16.png\" width=\"16px\" height=\"16px\" alt=\"Wijzigen\"></a></td>\n";
    }
    echo "   <td nowrap>".$speler->naam."</td>\n";
    echo "   <td align=\"center\">".$speler->klassement."</td>\n";
    echo "   <td align=\"right\">".$speler->lidnr."</td>\n";
    if ($tornooi->zelfde_disc_mag == 'j')
    {
      if ($speler->enkel == 'X')
      {
        echo "   <td align=\"center\">".$speler->discipline1."</td>\n";
      }
      else
      {
        echo "   <td>&nbsp;</td>\n";
      }
      if ($speler->dubbel == 'X')
      {
        echo "   <td align=\"center\">".$speler->discipline2."</td>\n";
      }
      else
      {
        echo "   <td>&nbsp;</td>\n";
      }
      if ($speler->gemengd == 'X')
      {
        echo "   <td align=\"center\">".$speler->discipline3."</td>\n";
      }
      else
      {
        echo "   <td>&nbsp;</td>\n";
      }
    }
    else
    {
      echo "   <td align=\"center\">".$speler->enkel."</td>\n";
      echo "   <td align=\"center\">".$speler->dubbel."</td>\n";
      echo "   <td align=\"center\">".$speler->gemengd."</td>\n";
    }
    if ($tornooi->zelfde_disc_mag == 'j')
    {
      echo "   <td>".$speler->partner.$warningE.$clubinfoE."</td>\n";
    }
    echo "   <td>".$speler->dub_partner.$warningD.$clubinfoD."</td>\n";
    echo "   <td>".$speler->mix_partner.$warningG.$clubinfoG."</td>\n";
    // Check if tournament is closed
    if ($tornooi->verzonden == '0')
    {
      // if not, display delete icon
      echo "   <td><a href=\"javascript:removeSubscription(".$speler->id.");\" title=\"Verwijderen\"><img src=\"../images/delete_16.png\" width=\"16px\" height=\"16px\" alt=\"Verwijderen\"></a></td>\n";
    }
    echo "  </tr>\n";
  }
?>
</table>
&nbsp;<a href="inschrijvingen_excel.php?id=<?= $_REQUEST["id"] ?>" target="_blank" title="Excel bestand met de inschrijvingen"><img src="../images/ico_xls_16.png" alt="Excel bestand met de inschrijvingen"></a>

   </td>
    </tr>
   </table>
     <!-- InstanceEndEditable -->
  </td>
 </tr>
 <tr>
  <td height="0"></td>
  <td></td>
 </tr>
 <tr>
  <td height="1"><img src="http://www.badmintonsport.be/images/spacer.gif" alt="" width="150" height="1"></td>
  <td></td>
 </tr>
</table>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <!--DWLayoutTable-->
 <tr>
  <td width="100%" height="50" valign="top" bgcolor="#FFFFFF">
   <table width="100%" border="0" cellpadding="0" cellspacing="0" style="border-color: #CCCCCC" bgcolor="#CCCCCC">
       <!--DWLayoutTable-->
    <tr>
     <td align="center" valign="middle" style="border-color: #CCCCCC" bgcolor="#006600">
         <img src="http://www.badmintonsport.be/images/spacer.gif" width="200" height="4" alt=""></td>
    </tr>
    <tr>
     <td width="100%" align="center" valign="middle" style="border-color: #CCCCCC" bgcolor="#CCCCCC" class="gmenu">Last change:
 <!-- InstanceBeginEditable name="datum" -->01/07/2007
 <!-- InstanceEndEditable -->
             E-mail: <a href="mailto:webmaster@badmintonsport.be">webmaster@badmintonsport.be</a></td>
    </tr>
    <tr>
     <td align="center" valign="middle" style="border-color: #CCCCCC" bgcolor="#006600" class="gmenu">
          <img src="http://www.badmintonsport.be/images/spacer.gif" width="200" height="4" alt=""></td>
    </tr>
   </table>
  </td>
 </tr>
</table>
</body>
<!-- InstanceEnd --></html>
<?php
  log_website_usage($start_time, $ref, $badm_db2);
?>