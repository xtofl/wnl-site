<?php
/**
 * help.php
 *
 * object     : Show help for an input label
 * author     : Freek Ceymeulen
 * created    : 05/06/2007
 * parameters : $id : id of record in help_messages table
 **/

  //require_once "../functies/website_usage.php";
  require_once "../functies/badm_db.inc.php";
  $badm_db = badm_conn_db();

  $query = "SELECT item_name, description FROM help_messages WHERE id = %d";
  $sql = sprintf($query, mysql_real_escape_string($_REQUEST["id"]));
  $result = mysql_query($sql, $badm_db) or badm_mysql_die();
  $help = mysql_fetch_object($result);
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="nl">
 <head>
  <title>W&amp;L badmintonvereniging vzw - Help</title>
  <meta http-equiv="content-type" content="text/html;charset=utf-8" />
  <style type="text/css">
  <!--
  body {
      margin : 0;
  }
  #title {
      background-color : #cccccc;
      border           : 1px #999999 solid;
      height           : 30px;
      width            : 100%;
  }
  .fieldtitlebold {
      font-family  : Verdana, Geneva, Arial, Helvetica, sans-serif;
      font-size    : 10pt;
      font-weight  : bold;
      padding-left : 4px;
  }
  table.xpbutton td.R {
      width : 4px;
  }
  table.xpbutton td.L {
      width : 4px;
  }
  table.xpbutton td.R img {
      display : block;
  }
  table.xpbutton td.L img {
      display : block;
  }
  table.xpbutton {
      color   : #333333;
      display : inline;
  }
  table.xpbutton td.C {
      background-image  : url(../images/button_xp_center.gif);
      background-repeat : x;
      white-space       : nowrap;
  }
  table.xpbutton td.C a {
      display         : block;
      font-family     : Verdana, Geneva, Arial, Helvetica, sans-serif;
      font-size       : 12px;
      font-weight     : bold;
      padding-left    : 3px;
      padding-right   : 3px;
      text-decoration : none;
      white-space     : nowrap;
  }
  table.xpbutton td.C a:visited {
      display         : block;
      font-family     : Verdana, Geneva, Arial, Helvetica, sans-serif;
      font-size       : 12px;
      font-weight     : bold;
      padding-left    : 3px;
      padding-right   : 3px;
      text-decoration : none;
      white-space     : nowrap;
  }
  .instructiontext {
      font-family : Verdana, Geneva, Arial, Helvetica, sans-serif;
      font-size   : 12px;
      margin      : 4px;
      padding     : 4px;
  }
  -->
  </style>
 </head>

 <body>
  <table id="title">
   <tr>
    <td class="fieldtitlebold"><?php echo $help->item_name; ?></td>
    <td align="right" valign="middle" width="20%" nowrap>
     <table class="xpbutton" cellspacing="0" cellpadding="0">
      <tr>
       <td class="L"><img src="../images/button_xp_left.gif" width="4px" height="24px" alt="" /></td>
       <td class="C"><a href="javascript:window.close();">Close</a></td>
       <td class="R"><img src="../images/button_xp_right.gif" width="4px" height="24px" alt="" /></td>
      </tr>
     </table>
    </td>
   </tr>
  </table>
  <div class="instructiontext"><?php echo $help->description; ?></div>
 </body>
</html>
<?php
  //log_website_usage($start_time, $id, $badm_db);
?>