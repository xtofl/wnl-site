<?php
/**
 * ajax_get_data.php
 *
 * object     : Methods to return data that are requested asynchronous
 * author     : Freek Ceymeulen
 * created    : 17/06/2007
 * parameters : $clubnaam : If set then return address of sports hall of given club
 *              $rang     : (optional) Defines which sports hall to return if the club has more than one, default = 1
 **/

 /*
 || Functions
 */

  function print_address_sporthal($club_naam, $rang = 1)
  {
    global $debug;
    $debug .= "<br>print_address_sporthal(".$club_naam.",".$rang.")";
    require_once "../functies/badm_db.inc.php";
    $badm_db = badm_conn_db();

    $select_stmt = "
  SELECT sh.naam
       , sh.adres
       , sh.postcode
       , sh.gemeente
       , sh.tel
       , sh.afstand
       , sh.tijd
    FROM sporthallen sh
       , sporthallen_clubs sc
       , bad_clubs bc
   WHERE sh.id = sc.sporthallen_id
     AND sc.clubs_id = bc.id
     AND bc.naam = '%s'
     AND sc.rang = %d";

    $query = sprintf($select_stmt, mysql_real_escape_string(str_replace("#AMP#", "&", str_replace("\'", "'", $club_naam)))
                                 , mysql_real_escape_string($rang) );
    $debug .= "<br>".$query;
    $rslt = mysql_query($query, $badm_db) or badm_mysql_die();

    if (mysql_num_rows($rslt) > 0)
    {

      $row = mysql_fetch_row($rslt);

      echo "<p>".$row[0]."<br>\n";
      echo $row[1]."<br>\n";
      echo $row[2]." ".$row[3]."<br>\n";
      echo "tel: ".$row[4]."<br>\n";
      echo "<span title=\"afstand en tijd vanaf de sporthal in Veltem\">afstand: ".$row[5]." km (".$row[6].")</span><br>\n";
      echo "[<a href=\"http://link2.map24.com/?lid=1bf222f9&maptype=JAVA&width0=1500&street0=".$row[1]."&zip0=".$row[2]."&city0=".$row[3]."&state0=&country0=BE&logo_url=&name0=".$row[0]."&description0=\" target=\"_blank\">plan</a>]</p>\n";

    }
    else
    {
      echo "<font color=\"red\">Geen adres gevonden.</font>";
    }
    mysql_free_result($rslt);
    //echo $debug;
  }

  function get_personalia_by_voornaam($voornaam)
  {
    global $debug;
    $debug .= "<br>get_personalia_by_voornaam(".$voornaam.")";
    require_once "../functies/badm_db.inc.php";
    $badm_db = badm_conn_db();

    $return = "";
    $query = "SELECT achternaam
                   , DATE_FORMAT(geb_dt, '%%d/%%m/%%Y') AS geb_dt
                   , klassement
                   , lidnr
                   , geslacht
                   , FLOOR((TO_DAYS(NOW()) - TO_DAYS(geb_dt)) / 365)  AS leeftijd
                   , email
                FROM bad_spelers
               WHERE voornaam = '%s'
                 AND eind_dt IS NULL
                 AND type_speler <> 'R'";
    $sql  = sprintf($query, mysql_real_escape_string($voornaam) );
    $result = mysql_query($sql, $badm_db) or badm_mysql_die();
    if (mysql_num_rows($result) == 1)
    {
      $speler = mysql_fetch_assoc($result);
      foreach ($speler as $key => $value)
      {
        $return .= "|".$value;
      }
    }
    if ($return != "")
    {
      echo substr($return, 1);
    }
  }

  function get_personalia_by_naam($naam)
  {
    global $debug;
    $debug .= "<br>get_personalia_by_naam(".$naam.")";
    require_once "../functies/badm_db.inc.php";
    $badm_db = badm_conn_db();

    $status = -1;
    $return = "";
    $query = "SELECT DATE_FORMAT(geb_dt, '%%d/%%m/%%Y') AS geb_dt
                   , klassement
                   , lidnr
                   , 'W&L bv' AS club
                   , geslacht
                   , FLOOR((TO_DAYS(NOW()) - TO_DAYS(geb_dt)) / 365)  AS leeftijd
                   , email
                FROM bad_spelers
               WHERE naam = '%s'
                 AND eind_dt IS NULL";
    $sql  = sprintf($query, mysql_real_escape_string($naam) );
    $result = mysql_query($sql, $badm_db) or badm_mysql_die();
    if (mysql_num_rows($result) == 1)
    {
      $speler = mysql_fetch_assoc($result);
      foreach ($speler as $key => $value)
      {
        $return .= "|".$value;
      }
    }
    elseif (mysql_num_rows($result) > 1)
    {
      $return = "||||W&L bv|||";
    }
    if ($return != "")
    {
      echo substr($return, 1);
      $status = 0;
    }
    return $status;
  }

  function get_personalia_other_club($naam)
  {
    global $debug;
    $debug .= "<br>get_personalia_other_club(".$naam.")";
    require_once "../functies/badm_db.inc.php";
    $badm_db = badm_conn_db();

    $status = -1;
    $return = "";
    $query = "SELECT NULL AS geb_dt
                   , klasD
                   , lidnrD
                   , clubD
                   , NULL AS geslacht
                   , NULL AS leeftijd
                   , NULL AS email
                   , dt_creatie
                FROM inschr_spel
               WHERE dub_partner = '%s'
                 AND ((clubD IS NOT NULL AND clubD <> '?') OR (lidnrD IS NOT NULL AND lidnrD <> '?'))
              UNION ALL
              SELECT NULL AS geb_dt
                   , klasG
                   , lidnrG
                   , clubG
                   , NULL AS geslacht
                   , NULL AS leeftijd
                   , NULL AS email
                   , dt_creatie
                FROM inschr_spel
               WHERE mix_partner = '%s'
                 AND ((clubG IS NOT NULL AND clubG <> '?') OR (lidnrG IS NOT NULL AND lidnrG <> '?'))
              UNION ALL
              SELECT NULL AS geb_dt
                   , klasE
                   , lidnrE
                   , clubE
                   , NULL AS geslacht
                   , NULL AS leeftijd
                   , NULL AS email
                   , dt_creatie
                FROM inschr_spel
               WHERE partner = '%s'
                 AND ((clubE IS NOT NULL AND clubE <> '?') OR (lidnrE IS NOT NULL AND lidnrE <> '?'))
               ORDER BY dt_creatie DESC";
    $sql  = sprintf($query, mysql_real_escape_string($naam)
                          , mysql_real_escape_string($naam)
                          , mysql_real_escape_string($naam) );
    $result = mysql_query($sql, $badm_db) or badm_mysql_die();
    if (mysql_num_rows($result) != 0)
    {
      $speler = mysql_fetch_assoc($result);
      foreach ($speler as $key => $value)
      {
        $return .= "|".$value;
      }
    }
    if ($return != "")
    {
      echo substr($return, 1);
      $status = 0;
    }
    return $status;
  }

 /*
 || Begin
 */

  $debug = "";
  if (isset($_REQUEST["clubnaam"]))
  {
    $clubnaam = $_REQUEST["clubnaam"];
    if (!empty($clubnaam))
    {
      print_address_sporthal($clubnaam, $_REQUEST["rang"]);
    }
  }
  elseif (isset($_REQUEST["voornaam"]))
  {
    $voornaam = $_REQUEST["voornaam"];
    if (!empty($voornaam))
    {
      get_personalia_by_voornaam($voornaam);
    }
  }
  elseif (isset($_REQUEST["naam"]))
  {
    $naam = $_REQUEST["naam"];
    if (!empty($naam))
    {
      $status = get_personalia_by_naam($naam);
    }
  }
  elseif (isset($_REQUEST["partner"]))
  {
    $partner = $_REQUEST["partner"];
    if (!empty($partner) && strtolower($partner) != "x")
    {
      $status = get_personalia_by_naam($partner);
      if ($status != 0)
      {
        $status = get_personalia_other_club($partner);
      }
    }
  }
  //mail("freek.ceymeulen@pandora.be", "Debug", $debug, "From: webmaster@badmintonsport.be");
?>