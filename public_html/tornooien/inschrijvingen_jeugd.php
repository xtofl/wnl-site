<?php
  require_once "../functies/website_usage.php";
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="nl">
<head>
<title>W&amp;L badmintonvereniging vzw - Inschrijvingen Jeugdtornooien</title>
<meta http-equiv="Content-type" content="text/html; charset=iso-8859-1" />
<link href="../css/badminton.css" rel="stylesheet" type="text/css">
<script language="javascript" type="text/javascript">
<!--
function submitforms()
{
   if (validateform())
   {
   //de waarden van de 3 forms samenbrengen in 1 form
      document.inschrijving.naam.value    = document.vulin.inschrijving.naam.value;
      document.inschrijving.klas.value    = document.vulin.inschrijving.klas.value;
      document.inschrijving.lidnr.value   = document.vulin.inschrijving.lidnr.value;
      document.inschrijving.geboren.value = document.vulin.inschrijving.geboren.value;
      document.inschrijving.e.value       = document.vulin.inschrijving.e.value;
      document.inschrijving.d.value       = document.vulin.inschrijving.d.value;
      document.inschrijving.g.value       = document.vulin.inschrijving.g.value;
      document.inschrijving.dubbelp.value = document.vulin.inschrijving.dubbelp.value;
      document.inschrijving.mixedp.value  = document.vulin.inschrijving.mixedp.value;
      if (document.verstuur.again && document.verstuur.again.checked == true)
      {
         document.inschrijving.again.value = document.verstuur.again.value;
      }
      //de waarden verzenden
      document.inschrijving.submit();
   }
   return false;
}
function validateform()
{
   //check of alles leeg is
   if (document.vulin.inschrijving.naam.value != "" && document.vulin.inschrijving.naam.value != "Vul in")
   {
   //check of klassement leeg is
      if(document.vulin.inschrijving.klas.value == "")
      {
         alert("Vul een waarde in voor het veld Klassement");
         document.vulin.inschrijving.klas.focus();
         return false;
      }
      else
      {
         //check of klassement correct is
         if (document.vulin.inschrijving.klas.value != "A"
         && document.vulin.inschrijving.klas.value != "B1"
         && document.vulin.inschrijving.klas.value != "B2"
         && document.vulin.inschrijving.klas.value != "C1"
         && document.vulin.inschrijving.klas.value != "C2"
         && document.vulin.inschrijving.klas.value != "D")
         {
            alert("Het ingevulde klassement is ongeldig");
            document.vulin.inschrijving.klas.focus();
            return false;
         }
      }
   //check of lidnr leeg is
      if(document.vulin.inschrijving.lidnr.value == "")
      {
         alert("Vul een waarde in voor het veld Lidnummer");
         document.vulin.inschrijving.lidnr.focus();
         return false;
      }
      else
      {
      //check of lidnummer correct is
         if (document.vulin.inschrijving.lidnr.value.length > 5)
         {
            alert("Het ingevulde lidnummer is te lang");
            document.vulin.inschrijving.lidnr.focus();
            return false;
         }
         if (!isInteger(document.vulin.inschrijving.lidnr.value))
         {
            alert("Het ingevulde lidnummer is ongeldig");
            document.vulin.inschrijving.lidnr.focus();
            return false;
         }
      }
   //check of geboren leeg is
      if(document.vulin.inschrijving.geboren.value == "")
      {
         alert("Vul je geboortedatum in in het veld Geboren");
         document.vulin.inschrijving.geboren.focus();
         return false;
      }
  //check of discipline ingevuld is
      if(document.vulin.inschrijving.e.value == "")
      {
         if(document.vulin.inschrijving.d.value == "")
         {
            if(document.vulin.inschrijving.g.value == "")
            {
               alert("Zet een X bij de discipline(s) die je wil spelen");
               document.vulin.inschrijving.e.focus();
               return false;
            }
         }
      }
   //check of discipline en partner overeenstemmen
      if (document.vulin.inschrijving.dubbelp.value != "" && document.vulin.inschrijving.d.value == "")
      {
         alert("Zet een X bij Dubbel of maak het veld Dubbelpartner leeg");
         document.vulin.inschrijving.d.focus();
         return false;
      }
      if (document.vulin.inschrijving.mixedp.value != "" && document.vulin.inschrijving.g.value == "")
      {
         alert("Zet een X bij Gemengd of maak het veld Mixed Partner leeg");
         document.vulin.inschrijving.g.focus();
         return false;
      }
   }
   return true;
}
function isInteger(s)
{   var i;
    for (i = 0; i < s.length; i++)
    {   
        // Check that current character is number.
        var c = s.charAt(i);
        if ((c < "0") || (c > "9"))
        {
           return false;
        }
        else
        {
           return true;
        }
    }
}

// disable right-click
var popup="Sorry, right-click is disabled.\n\nThis Site Copyright �2004";
function noway(go)
{
  if (document.all)
  {
    if (event.button == 2)
    {
      alert(popup);
      return false;
    }
  }
  if (document.layers)
  {
    if (go.which == 3)
    {
      alert(popup);
      return false;
    }
  }
}
if (document.layers)
{
  document.captureEvents(Event.MOUSEDOWN);
}
document.onmousedown=noway;

//-->
</script>

<script type="text/javascript" language="JavaScript1.2" src="http://www.badmintonsport.be/scripts/stm31.js"></script>
<style type="text/css">
<!--
A.speciaal:link {
    font-size : 8pt;
    text-decoration : none
}
.inputveld {
    border-style : none;
    border : none;
}
//-->
</style>
</head>
<!-------------------------------------------HOOFDING---------------------------------------------------------------->
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" class="badminton">
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td height="95" colspan="2" valign="top">
      <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
          <td width="100%" height="95">
            <table width="100%" border="0" cellpadding="0" cellspacing="0" bordercolor="#CCCCCC" bgcolor="#FFFFFF">
              <tr>
                <td align="center" valign="middle" bgcolor="#006600"><img src="../images/spacer.gif" width="200" height="4"></td>
              </tr>
              <tr>
                <td align="center" valign="middle" bgcolor="#CCCCCC"><img src="../images/pt_transp.gif" width="500" height="5"></td>
              </tr>
              <tr>
                <td width="100%" align="center" valign="middle" bgcolor="#CCCCCC">
                  <h4>W&amp;L Badmintonvereniging vzw, Mechelsesteenweg 387, 3020 Herent</h4>
                  </td>
              </tr>
              <tr>
                <td align="center" valign="top" bgcolor="#CCCCCC"><img src="../images/chinas.gif" width="400" height="60"></td>
              </tr>
              <tr>
                <td align="center" valign="middle" bgcolor="#CCCCCC"><img src="../images/pt_transp.gif" width="500" height="5"></td>
              </tr>
              <tr>
                <td align="center" valign="middle" bgcolor="#336600"><img src="../images/spacer.gif" width="200" height="4"></td>
              </tr>
            </table>
		  </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td width="150" height="213" valign="top">
<!----------------------------------------------------------EINDE HOOFDING------------------------------------------------>
<?php
   require "../templates/template.php";
   invoegen_template(); //menu
echo "    </td>\n";
echo "    <td width=\"100%\" valign=\"top\">\n";
echo "      <p class=\"subtitel\">\n";
//Het tornooi id toekennen.  Dit wordt anders verkregen afhankelijk vanwaar deze pagina werd aangeroepen
   if ($_POST['id'] != '')
      $id = $_POST['id'];
   else
      $id = $_GET['id'];
   if (!is_numeric($id))
   {
     echo "Error: There is no id $id in the db.";
     exit;
   }
   require_once "../functies/badm_db.inc.php";
   $badm_db = badm_conn_db();
      //----------------------------------------INSERT nieuwe inschrijving---------------------------------------------------
      if ($_POST["mode"] == 2)  //na inschrijving
      {
      //check of er wel iets ingevuld is
         if ($_POST["naam"] != 'Vul in' AND $_POST["naam"] != '')
         {
           if ($_POST["e"] != '')
              $e = 1;
           else
              $e = 0;
           if ($_POST["d"] != '')
              $d = 1;
           else
              $d = 0;
           if ($_POST["g"] != '')
              $g = 1;
           else
              $g = 0;
         $insert = "INSERT INTO inschr_spel
           ( naam
           , lidnr
           , klassement
           , geb_dt
           , enkel
           , dubbel
           , gemengd
           , dub_partner
           , mix_partner
           , inschr_torn_id
           , usid_wijz ) 
                   valueS ('"
              .$_POST["naam"]
              ."', '" .$_POST["lidnr"]
              ."', '" .$_POST["klas"]
              ."', '" .$_POST["geboren"]
              ."', '" .$e
              ."', '" .$d
              ."', '" .$g
              ."', '" .$_POST["dubbelp"]
              ."', '" .$_POST["mixedp"]
              ."', '" .$_POST["inschr_torn_id"]
              ."', '" .$_POST["usid"]
              ."') ";
         $result2 = mysql_query($insert, $badm_db) or die("Invalid query: " . mysql_error());
         }
      }
      //------------------------------UPDATE al dan niet gewijzigde records-------------------------------------------------
      if ($_POST["mode"] == 2)  //na inschrijving
      {
      //check of er wel records zijn om te updaten
         if ($_POST["update"] != '')
         {
            $ids = explode("/", $_POST["update"]); //id's van de te updaten records
            foreach ($ids as $i => $value)
            {
               $naam = "naam".$ids[$i];
               $lidnr = "lidnr".$ids[$i];
               $klassement = "klas".$ids[$i];
               $geboren = "geboren".$ids[$i];
               $enkel = "e".$ids[$i];
               $dubbel = "d".$ids[$i];
               $gemengd = "g".$ids[$i];
               $dub_partner = "dubbelp".$ids[$i];
               $mix_partner = "mixedp".$ids[$i];
            //check of het record niet is leeggemaakt, zo ja verwijder dit record
               if ($_POST[$naam] == ''
               and $_POST[$lidnr] == ''
               and $_POST[$klassement] == ''
               and $_POST[$geboren] == ''
               and $_POST[$enkel] == ''
               and $_POST[$dubbel] == ''
               and $_POST[$gemengd] == ''
               and $_POST[$dub_partner] == ''
               and $_POST[$mix_partner] == '')
               {
                  $delete = "DELETE FROM inschr_spel WHERE id = ".$ids[$i];
                  $result = mysql_query($delete, $badm_db) or die("Invalid query: " . mysql_error());
               }
               else
               {
                  if ($_POST[$enkel] != '')
                     $e = 1;
                  else
                     $e = 0;
                  if ($_POST[$dubbel] != '')
                     $d = 1;
                  else
                     $d = 0;
                  if ($_POST[$gemengd] != '')
                     $g = 1;
                  else
                     $g = 0;
                  // change date from format dd-mm-yyyy to yyyy-mm-dd
                  $dt_geb = SUBSTR($_POST[$geboren], 6).SUBSTR($_POST[$geboren], 2, 4).SUBSTR($_POST[$geboren], 0, 2);
                  $update = "UPDATE inschr_spel
                             SET naam = '".$_POST[$naam]
                             ."',lidnr = '".$_POST[$lidnr]
                             ."',klassement = '".$_POST[$klassement]
                             ."',geb_dt = '".$dt_geb
                             ."',enkel = '".$e
                             ."',dubbel = '".$d
                             ."',gemengd = '".$g
                             ."',dub_partner = '".$_POST[$dub_partner]
                             ."',mix_partner = '".$_POST[$mix_partner]
                             ."',inschr_torn_id = '".$_POST["inschr_torn_id"]
                             ."',usid_wijz = '".$_POST["usid"]
                             ."' WHERE id = ".$ids[$i];
                  $result = mysql_query($update, $badm_db) or die("Invalid query: " . mysql_error());
               }
            }
         }
      }
   $disabled = True; //geeft aan hoe een formulierveld getoond mag worden
   $superuser = False; //tornooiverantwoordelijke mag alle inschrijvingen aanpassen
//-------------------------------------------------INLOGGEN-------------------------------------------------------------
   if ($_POST["mode"] == 1)  //na login
   {
      if(!isset($_POST["username"])) //dus als er geen username is ingevoerd 
      {
         $message = "<font color = \"red\">jonge, je bent hier op de verkeerde manier gekomen<br></font>";
      }
      else //als dat WEL is gedaan...
      {
      // create SQL statement 
         $query = "SELECT level, naam, paswoord FROM users WHERE usid = '%s' AND paswoord = '%s'";
         $sql = sprintf($query, mysql_real_escape_string($_POST["username"])
                              , mysql_real_escape_string($_POST["password"]));
         $result = mysql_query($sql, $badm_db) or badm_mysql_die();
      // format results by row
         $login = mysql_fetch_object($result);
         $usernaam = $login->naam;
         $paswoord = $login->paswoord;
         if(mysql_num_rows($result) != 1) 
         {
      //username+password zijn ingegeven maar kloppen niet !
            $message = "<font COLOR = \"red\"><b>Wrong username / password</b></font><br>";
         }
         else if(mysql_num_rows($result) == 1) 
         {
            //echo "login succesfull<br>";
            $message = "Welkom ".$usernaam."<br>";
            $disabled = False;
			if ($login->level == 3) //administrator
			   $superuser = True;
            else
            {
              // check if user is tornooiverantwoordelijke
              $tv = mysql_query("SELECT *
                                   FROM bad_spelers s
                                      , clubfuncties f
                                      , leden_functies lf
                                  WHERE s.id = lf.spelers_id
                                    AND f.id = lf.functies_id
                                    AND f.functie = 'TORNOOI'
                                    AND s.naam = '".$usernaam."'", $badm_db) or badm_mysql_die();
              if (mysql_num_rows($tv) == 1)
                 $superuser = True;
            }
         }
      }
   }
//----------------------------------------------TORNOOI INFO--------------------------------------------------------
// create SQL statement voor tornooidetails
   $sql = "SELECT id
                , CONCAT( datum,\" \"
                        , organisatie,\" \"
                        , type,\" &nbsp;&nbsp; \"
                        , reeks_dis ) AS torn
                , DATE_formAT(dt_inschr, '%d-%m-%Y') as dt_inschr
                , info
                , enkel
                , dubbel
                , gemengd
           FROM inschr_torn
		   WHERE id = ".$id;
   $result = mysql_query ($sql, $badm_db) or badm_mysql_die();
// format results by row
   $tornooi = mysql_fetch_object($result);
   $inschr_torn_id = $tornooi->id;
echo "   <h1 align=\"left\">\n";
   echo $tornooi->torn;
//toon afbeelding van shuttle die wanneer erop geklikt wordt meer info geeft over het tornooi in kwestie
echo "&nbsp;<a href=\"";
   echo $tornooi->info;
echo "\"  target=\"_blank\" title=\"Meer info?\"><IMG SRC=\"../images/favicon.gif\" border=0></a>";
echo "   </h1><h3>inschrijven v��r: ";
   echo $tornooi->dt_inschr;
   mysql_free_result($result);
echo "      &nbsp;&nbsp;</h3>\n";
echo "      </p>\n";
echo $message;  //(fout)melding afbeelden (indien er een is)
echo "      <form name=\"inschrijving\" action=\"inschrijvingen_jeugd.php?id=".$id."\" method=\"post\">\n";
echo "      <table width=\"99%\" border=\"1\" cellspacing=\"0\" cellpadding=\"2\">\n";
//------------------------------------------TABEL HOOFDING-------------------------------------------------------
echo "        <tr bgcolor=\"#666699\" class=\"geel\">\n";
echo "          <td align=\"center\" width=\"21%\">\n";
echo "            <p>Naam</p>\n";
echo "          </td>\n";
echo "          <td align=\"center\" width=\"4%\">\n";
echo "            <p>Klas</p>\n";
echo "          </td>\n";
echo "          <td align=\"center\" width=\"6%\">\n";
echo "            <p>Lidnr</p>\n";
echo "          </td>\n";
echo "          <td align=\"center\" width=\"10%\">\n";
echo "            <p>Geboren</p>\n";
echo "          </td>\n";
echo "          <td align=\"center\" width=\"4%\">\n";
echo "            <p>E</p>\n";
echo "          </td>\n";
echo "          <td align=\"center\" width=\"4%\">\n";
echo "            <p>D</p>\n";
echo "          </td>\n";
echo "          <td align=\"center\" width=\"4%\">\n";
echo "            <p>G</p>\n";
echo "          </td>\n";
echo "          <td align=\"center\" width=\"22%\">\n";
echo "            <p>Dubbelpartner</p>\n";
echo "          </td>\n";
echo "          <td align=\"center\" width=\"22%\">\n";
echo "            <p>Mixed Partner</p>\n";
echo "          </td>\n";
echo "        </tr>\n";
//----------------------------------------------------INGESCHREVENEN-----------------------------------------------------
// create SQL statement voor ingeschrevenen
   $sql = "SELECT id
                , naam
                , lidnr
                , klassement
                , DATE_formAT(geb_dt, '%d-%m-%Y') AS geb_dt
                , IF (enkel = '0', '', 'X') AS enkel
                , IF (dubbel = '0', '', 'X') AS dubbel
                , IF (gemengd = '0', '', 'X') AS gemengd
                , dub_partner
                , mix_partner
                , UPPER(usid_wijz) AS usid_wijz
           FROM inschr_spel
           WHERE inschr_torn_id = ".$id."
           ORDER BY klassement, naam";
   $result = mysql_query ($sql, $badm_db) or badm_mysql_die();
// format results by row
   if ($_POST["again"] == 'yes') //nog een inschrijving doen
   {
      $superuser = True;
	  $disabled = False;
   }
   while ($speler = mysql_fetch_object($result))
   {
      $updateable = False; //geeft aan of een record gewijzigd mag worden door de user
      if ($usernaam == $speler->naam OR $usernaam == $speler->dub_partner OR $usernaam == $speler->mix_partner OR (strtoupper($_POST["username"]) == $speler->usid_wijz AND $_POST["password"] == $paswoord))
      {
         $updateable = True;
         $array[] = $speler->id;  //id's van wijzigbare records worden in array bewaard om later update te kunnen uitvoeren
      }
	  if ($superuser)
	  {
	     $updateable = True;
		 $array[] = $speler->id;
      }
      echo "<tr align=\"left\" valign=\"center\">\n";
      echo "<td><input ";
      if ($disabled OR !$updateable) {echo "disabled ";}
      echo "type=\"text\" name =\"naam";
      echo $speler->id;
      echo "\" size=\"24\" value=\"";
      echo $speler->naam;
      echo "\" maxlength=30 class=\"inputveld\"></td>\n";
      echo "<td align=\"center\"><p><input ";
      if ($disabled OR !$updateable)
         echo "disabled ";
      echo "type=\"text\" name =\"klas";
      echo $speler->id;
      echo "\" size=\"2\" value=\"";
      echo $speler->klassement;
      echo "\" maxlength=2 class=\"inputveld\"></p></td>\n";
      echo "<td align=\"center\"><p><input ";
      if ($disabled OR !$updateable)
         echo "disabled ";
      echo "type=\"text\" name =\"lidnr";
      echo $speler->id;
      echo "\" size=\"4\" value=\"";
      echo $speler->lidnr;
      echo "\" maxlength=5 class=\"inputveld\"></p></td>\n";
      echo "<td align=\"center\"><p><input ";
      if ($disabled OR !$updateable)
         echo "disabled ";
      echo "type=\"text\" name =\"geboren";
      echo $speler->id;
      echo "\" size=\"10\" value=\"";
      echo $speler->geb_dt;
      echo "\" maxlength=10 class=\"inputveld\"></p></td>\n";
      echo "<td align=\"center\"><p><input ";
      if ($disabled OR !$updateable OR $tornooi->enkel == 'n')
         echo "disabled ";
      echo "type=\"text\" name =\"e";
      echo $speler->id;
      echo "\" size=\"1\" value=\"";
      echo $speler->enkel;
      echo "\" maxlength=1 class=\"inputveld\"></p></td>\n";
      echo "<td align=\"center\"><p><input ";
      if ($disabled OR !$updateable OR $tornooi->dubbel == 'n')
         echo "disabled ";
      echo "type=\"text\" name =\"d";
      echo $speler->id;
      echo "\" size=\"1\" value=\"";
      echo $speler->dubbel;
      echo "\" maxlength=1 class=\"inputveld\"></p></td>\n";
      echo "<td align=\"center\"><p><input ";
      if ($disabled OR !$updateable OR $tornooi->gemengd == 'n')
         echo "disabled ";
      echo "type=\"text\" name =\"g";
      echo $speler->id;
      echo "\" size=\"1\" value=\"";
      echo $speler->gemengd;
      echo "\" maxlength=1 class=\"inputveld\"></p></td>\n";
      echo "<td><p><input ";
      if ($disabled OR !$updateable OR $tornooi->dubbel == 'n')
         echo "disabled ";
      echo "type=\"text\" name =\"dubbelp";
      echo $speler->id;
      echo "\" size=\"27\" value=\"";
      echo $speler->dub_partner;
      echo "\" maxlength=45 class=\"inputveld\"></p></td>\n";
      echo "<td><p><input ";
      if ($disabled OR !$updateable OR $tornooi->gemengd == 'n')
         echo "disabled ";
      echo "type=\"text\" name =\"mixedp";
      echo $speler->id;
      echo "\" size=\"27\" value=\"";
      echo $speler->mix_partner;
      echo "\" maxlength=45 class=\"inputveld\"></p></td>\n";
      echo "</tr>\n";
   }
      echo "      </table>\n";
      echo "<input type=\"hidden\" name=\"mode\" value=\"2\">\n";
      echo "<input type=\"hidden\" name=\"id\" value=\"".$id."\">\n";
      echo "<input type=\"hidden\" name=\"usid\" value=\"".$_POST["username"]."\">\n";
      echo "<input type=\"hidden\" name=\"inschr_torn_id\" value=\"".$inschr_torn_id."\">\n";
	  if (/*$_POST["mode"] == 1 AND*/ mysql_num_rows($result) != 0 AND isset($array))  //mode = 1 na login
	  {
		$update = implode("/", $array);
      }
      echo "<input type=\"hidden\" name=\"update\" value=\"".$update."\">\n";
      //in de volgende velden komt een copie van de waarden uit het form in het iframe
      echo "<input type=\"hidden\" name=\"naam\">\n";
      echo "<input type=\"hidden\" name=\"klas\">\n";
      echo "<input type=\"hidden\" name=\"lidnr\">\n";
      echo "<input type=\"hidden\" name=\"geboren\">\n";
      echo "<input type=\"hidden\" name=\"e\">\n";
      echo "<input type=\"hidden\" name=\"d\">\n";
      echo "<input type=\"hidden\" name=\"g\">\n";
      echo "<input type=\"hidden\" name=\"dubbelp\">\n";
      echo "<input type=\"hidden\" name=\"mixedp\">\n";
      echo "<input type=\"hidden\" name=\"again\" value=\"no\">\n";
//-------------------------------------------------IFRAME----------------------------------------------------------------------
   if (($_POST["mode"] == 1 AND !$disabled) OR ($_POST["again"] == 'yes'))  //voeg leeg record toe onderaan
   {
      echo "<iframe src=\"vulin_jeugd.php?id=".$inschr_torn_id."&naam=".$usernaam."\" name=\"vulin\" height=\"32\" width=\"99%\" scrolling=\"no\" frameborder=\"0\" marginheight=\"0\" marginwidth=\"0\">";
      echo "Uw browser ondersteunt geen iframes.  Breng de webmaster hiervan op de hoogte a.u.b. <br>D�sol� mais votre navigateur ne supporte pas les cadres locaux.";
      echo "</iframe>";
   }
echo "      </form>\n";
//-------------------------------------------------KNOP VERZENDEN-----------------------------------------------------------------
   if (($_POST["mode"] == 1 AND !$disabled) OR ($_POST["again"] == 'yes'))  //toon knop om in te schrijven
   {
      echo "<form name=\"verstuur\" action=\"inschrijvingen_jeugd.php?id=".$id."\" method=\"post\" onSubmit=\"return submitforms();\">\n";
   	  if ($superuser)
	     echo "<input type=\"checkbox\" name=\"again\" value=\"yes\">Doe hierna nog een inschrijving";
      echo "<br><input type=\"submit\" value=\"Bevestig je inschrijving!\"><br>\n";
	  if ($superuser)
	     echo "<br><a href=\"preview_jeugd.php?id=".$id."&m=p\" target=\"_blank\">Verstuur de inschrijvingen</a>\n";
      echo "      </form>\n";
   }
//-----------------------------------------------LOGIN form----------------------------------------------------------------------
   $mode = $_POST["mode"];
if ($_POST["again"] == 'yes')
   $mode = 1;
if ($mode != 1)  //toon login form
{
// create SQL statement: check of inschrijving nog niet afgesloten is
   $sql = "SELECT verzonden
           FROM inschr_torn
		   WHERE id = ".$id;
   $result = mysql_query ($sql, $badm_db) or badm_mysql_die();
// format results by row
   $verzonden = mysql_fetch_object($result);
   if ($verzonden->verzonden == 1) //afgesloten
   {
      echo "<font COLOR = \"red\"><H2>Deze inschrijving is afgesloten</H2></font>";
   }
   else //toon inlog module
   {
      echo "<h3>Schrijf in of wijzig je inschrijving:</h3>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href=\"../docs/Inschrijven_website.doc\" target=\"_blank\">Hulp nodig?</a>\n";
      echo "<table border=\"0\" cellpadding=\"0\" cellspacing=\"2\">\n";
      echo "<form name=\"login\" action=\"inschrijvingen_jeugd.php\" method=\"post\">\n";
      echo "<tr>\n";
      echo "<td>Username :</td>\n";
      echo "<td><input type=\"text\" name=\"username\" size=\"12\" maxlength=\"25\"></td>\n";
      echo "</tr>\n";
      echo "<tr>\n";
      echo "<td>Paswoord :</td>\n";
      echo "<td><input type=\"password\" name=\"password\" size=\"12\" maxlength=\"25\"></td>\n";
      echo "</tr>\n";
      echo "<tr>\n";
      echo "<td></td>\n";
      echo "</tr>\n";
      echo "<tr>\n";
      echo "<td>\n";
      echo "<input type=\"hidden\" name=\"mode\" value=\"1\">\n";
      echo "<input type=\"hidden\" name=\"id\" value=\"".$_GET['id']."\">\n";
      echo "</td>\n";
      echo "<td>\n";
      echo "<input type=\"submit\" value=\"login\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\n";
      echo "Vraag je username en paswoord aan de <a href=\"mailto:webmaster@badmintonsport.be?subject=Login gegevens website&body=Graag mijn gebruikersnaam en paswoord.\">webmaster</a>.\n";
      echo "</td>\n";
      echo "</tr>\n";
      echo "</form>\n";
      echo "</table>\n";
   }
}
//------------------------------------------------------EINDE LOGINform------------------------------------------------------
   mysql_free_result($result);
?>
      </td>
  </tr>
  <tr> 
    <td>&nbsp;</td>
    <td>
<?php
  require "../functies/browser.php";
  $browser = get_user_agent();
  // Browser test
  if ($browser != "Internet Explorer")
  {
    echo "! Het inschrijven met de ".$browser." browser wordt (nog) niet ondersteund. Gebruik Microsoft Internet Explorer.";
  }
?>
    &nbsp;</td>
  </tr>
  <tr> 
    <td height="1"><img src="../images/spacer.gif" alt="" width="150" height="1"></td>
    <td></td>
  </tr>
</table>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr> 
    <td width="100%" height="50" valign="top" bgcolor="#FFFFFF"><table width="100%" border="0" cellpadding="0" cellspacing="0" bordercolor="#CCCCCC" bgcolor="#CCCCCC">
        <tr> 
          <td align="center" valign="middle" bordercolor="#CCCCCC" bgcolor="#006600"><img src="../images/spacer.gif" width="200" height="4"></td>
        </tr>
        <tr> 
          <td width="100%" align="center" valign="middle" bordercolor="#CCCCCC" bgcolor="#CCCCCC" class="gmenu">
            Last change: 06-01-2004
            E-mail: <a href="mailto:webmaster@badmintonsport.be">webmaster@badmintonsport.be</a></td>
        </tr>
        <tr>
          <td align="center" valign="middle" bordercolor="#CCCCCC" bgcolor="#006600" class="gmenu"><img src="../images/spacer.gif" width="200" height="4"></td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</body>
</html>
<?php
  log_website_usage($start_time, null, $badm_db);
  mysql_close($badm_db);
?>