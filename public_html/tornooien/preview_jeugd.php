<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
<head>
<title>W&amp;L badmintonvereniging vzw - Inschrijving Tornooi Verzenden</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link href="../css/badminton.css" rel="stylesheet" type="text/css">
</head>
<BODY>
<?php
   require_once "../functies/badm_db.inc.php";
   $badm_db = badm_conn_db();
/*********************************************************************************/
/* Checken of er een tornooi is waarvan de inschrijvingsperiode vandaag vervalt. */
/* De lijst met ingeschrevenen verzenden naar de organisatie                     */
/* Tabel inschr_torn aanpassen zodat deze mailing slechts 1 keer gebeurt.        */
/*********************************************************************************/
// create SQL statement voor tornooi
   $sql = "SELECT id
                 ,organisatie
                 ,email
                 ,naam
                 ,taal
                 ,verzonden
                 ,usid_wijz
           FROM inschr_torn
           WHERE id = ".$_GET['id'];
   $result = mysql_query ($sql, $badm_db) or badm_mysql_die();
   $tornooi = mysql_fetch_object($result);
   // create SQL statement voor ingeschrevenen
      $sql2 = "SELECT i.id
                    , i.naam
                    , i.lidnr
                    , i.klassement
                    , DATE_FORMAT(i.geb_dt, '%d-%m-%Y') AS geb_dt
                    , (YEAR(CURDATE()) - YEAR(i.geb_dt)) - (RIGHT(CURDATE(), 5) < RIGHT(i.geb_dt, 5)) AS leeftijd
                    , s.geslacht
                    , IF (i.enkel = '0', ' &nbsp; ', 'X') AS enkel
                    , IF (i.dubbel = '0', ' &nbsp; ', 'X') AS dubbel
                    , IF (i.gemengd = '0', ' &nbsp; ', 'X') AS gemengd
                    , IF (i.dub_partner = '', ' &nbsp; ', i.dub_partner) AS dub_partner
                    , IF (i.mix_partner = '', ' &nbsp; ', i.mix_partner) AS mix_partner
                    , UPPER(i.usid_wijz) AS usid_wijz
              FROM inschr_spel i
                 , bad_spelers s
              WHERE inschr_torn_id = ".$tornooi->id
           ." AND i.naam  = s.naam
              AND i.lidnr = s.lidnr
              ORDER BY i.klassement
                     , leeftijd
                     , i.naam";
      $result2 = mysql_query ($sql2, $badm_db) or badm_mysql_die();

      /**************/
      /* recipients */
      /**************/
         $to  = $tornooi->email;
         //$to .= "freek.ceymeulen@pandora.be"; // note the comma

      /***********/
      /* subject */
      /***********/
         if ($tornooi->taal == 'N')
            $subject = "Inschrijvingen voor ".$tornooi->organisatie;
         else if ($tornooi->taal == 'F')
            $subject = "Inscriptions pour ".$tornooi->organisatie;

      /***********/
      /* message */
      /***********/
         $message = "
<html>
<head>
 <title>W&L inschrijvingen voor ".$tornooi->organisatie."</title>
</head>
<body>
<!-- 
Als u deze tekst te zien krijgt is uw email programma 
niet correct ingesteld om dit HTML-bericht te tonen.
Breng ons a.u.b. hiervan op de hoogte!

If you're seeing this message, your email program 
is not properly reading this HTML formatted message. 
--> 
<p>";
      if ($tornooi->taal == 'N')
         $message .= "Geachte ".$tornooi->naam.",";
      else if ($tornooi->taal == 'F')
         $message .= "Madame, Monsieur,";
      if ($tornooi->taal == 'N')
         $message .= "</p><p>Hier zijn de inschrijvingen van W&L bv voor jullie jeugdtornooi";
      else if ($tornooi->taal == 'F')
         $message .= "</p><p>Ici les inscriptions de W&L bv pour votre tournoi";
      $message .= "
</p>
<table width=\"99%\" border=\"1\" cellspacing=\"0\" cellpadding=\"4\">
        <tr>
          <th ALIGN=\"center\" width=\"20%\" bgcolor=\"#cccccc\">
            <p>Naam</p>
          </th>
          <th ALIGN=\"center\" width=\"4%\" bgcolor=\"#cccccc\">
            <p>Klas</p>
          </th>
          <th ALIGN=\"center\" width=\"6%\" bgcolor=\"#cccccc\">
            <p>Lidnr</p>
          </th>
          <th ALIGN=\"center\" width=\"10%\" bgcolor=\"#cccccc\">
            <p>Geboren</p>
          </th>
          <th ALIGN=\"center\" width=\"4%\" bgcolor=\"#cccccc\">
            <p>Leeftijd</p>
          </th>
          <th ALIGN=\"center\" width=\"4%\" bgcolor=\"#cccccc\">
            <p>Geslacht</p>
          </th>
          <th ALIGN=\"center\" width=\"4%\" bgcolor=\"#cccccc\">
            <p>E</p>
          </th>
          <th ALIGN=\"center\" width=\"4%\" bgcolor=\"#cccccc\">
            <p>D</p>
          </th>
          <th ALIGN=\"center\" width=\"4%\" bgcolor=\"#cccccc\">
            <p>G</p>
          </th>
          <th ALIGN=\"center\" width=\"20%\" bgcolor=\"#cccccc\">
            <p>Dubbelpartner</p>
          </th>
          <th ALIGN=\"center\" width=\"20%\" bgcolor=\"#cccccc\">
            <p>Mixed Partner</p>
          </th>
        </tr>";
      // format results by row
         while ($speler = mysql_fetch_object($result2))
         {
            $message .= "<tr ALIGN=\"left\" valign=\"center\">
          <td>
            <p>".$speler->naam."</p>
          </td>
          <td ALIGN=\"center\">
            <p>".$speler->klassement."</p>
          </td>
          <td ALIGN=\"center\">
            <p>".$speler->lidnr."</p>
          </td>
          <td ALIGN=\"center\">
            <p>".$speler->geb_dt."</p>
          </td>
          <td ALIGN=\"center\">
            <p>".$speler->leeftijd."</p>
          </td>
          <td ALIGN=\"center\">
            <p>".$speler->geslacht."</p>
          </td>
          <td ALIGN=\"center\">
            <p>".$speler->enkel."</p>
          </td>
          <td ALIGN=\"center\">
            <p>".$speler->dubbel."</p>
          </td>
          <td ALIGN=\"center\">
            <p>".$speler->gemengd."</p>
          </td>
          <td>
            <p>".$speler->dub_partner."</p>
          </td>
          <td>
            <p>".$speler->mix_partner."</p>
          </td>
		</tr>";
         }
         $message .= "</table>";
         if ($tornooi->taal == 'N')
         {
            $message .= "
	       <p>Zou u ons willen bevestigen als u deze mail goed ontvangen hebt? &nbsp;&nbsp;Dank u.<br></p>
           <p>Met vriendelijke groeten,</p>";
         }
         else if ($tornooi->taal == 'F')
         {
            $message .= "
	       <p>Pourriez-vous me confirmer la bonne r�ception de cet e-mail? &nbsp;&nbsp;Merci.<br></p>
           <p>nos sinc�res salutations,</p>";
         }
/*********************************************************
   Haal de gegevens van de tornooiverantwoordelijke op.
*********************************************************/
$tv = mysql_query("SELECT s.naam, s.adres, s.postcode, s.woonplaats, IF (s.tel IS NULL, s.gsm, tel) AS tel, s.gsm, s.email FROM bad_spelers s, clubfuncties f, leden_functies lf WHERE s.id = lf.spelers_id AND f.id = lf.functies_id AND f.functie = 'TORNOOI'", $badm_db);
	     $message .= "
		   <p>".mysql_result($tv, 0, "naam")."<br>tornooiverantwoordelijke W&L bv<br>tornooien@badmintonsport.be<br>".mysql_result($tv, 0, "tel")."</p>
</body>
</html>
";

      /* To send HTML mail, you can set the Content-type header. */
         $headers  = "MIME-Version: 1.0\r\n";
         $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";

      /**********************/
      /* additional headers */
      /**********************/
         /*$headers .= "To: ".$tornooi->naam." <".$tornooi->email.">\r\n";*/
         $headers .= "From: W&L bv <wimpetes@hotmail.com>\r\n";
         /*$headers .= "Cc: freek.ceymeulen@pandora.be\r\n";*/
         $headers .= "Bcc: freek.ceymeulen@pandora.be, ".mysql_result($tv, 0, "email").", tornooien@badmintonsport.be\r\n";

         if ($_GET['m'] == 'p') //preview
		 {
		    echo "Aan : ".$to;
			echo "<BR>Onderwerp : ".$subject;
			echo "<BR>Bericht : ".$message;
			echo "<BR><A HREF=\"inschrijvingen.php?id=".$id."\">Cancel</A>\n";
			echo "&nbsp;&nbsp;<A HREF=\"preview_jeugd.php?id=".$id."&m=s\">Verzend!</A>\n";
         }
		 else if ($_GET['m'] == 's') //send
		 {
         /* and now mail it */
            if (mail($to, $subject, $message, $headers))
            {
               $update = "UPDATE inschr_torn
                          SET verzonden = '1'
		                  WHERE id = ".$tornooi->id;
               $result = mysql_query($update, $badm_db) or die("Invalid query: " . mysql_error());
			   echo "Verzonden...";
            }
			else
			   echo "Mislukt.";
         }
/*******************************************************************************************************/ 
/* Einde mailing                                                                                       */
/*******************************************************************************************************/
?>
</BODY>
</HTML>