<?php
/**
 * inschrijvingen_excel.css
 *
 * object    : creates an Excel file and forces a download of this file
 * author    : Freek Ceymeulen
 * created   : 05/07/2005
 **/

  // Connect to the DB (must be done before calling sess.php)
  require_once "../functies/badm_db.inc.php";
  $badm_db = badm_conn_db();
  /*require_once('../functies/sess.php');

  if ($_SESSION['auth'] != true)
  {
    // niet ingelogd -> ga naar login pagina
    //session_write_close;
    mysql_close($badm_db);
    header("Location: http://www.badmintonsport.be/admin/login.php");
    exit;
  }*/
/*
--------------------------------------------------------------------------------------------
|| FUNCTIONS
--------------------------------------------------------------------------------------------
*/
function ForceFileDownload($file)
{
  $filesize = @filesize($file);
  header("Content-Description: File Transfer");
  header("Content-Length: " . $filesize);
  header("Content-Disposition: attachment; filename=".basename($file));
  header("Content-Type: application/octet-stream");
  header("Content-Type: application/force-download");
  header("Content-Type: application/download");
  header("Content-Transfer-Encoding: binary");
  header("Cache-Control: no-cache, must-revalidate, post-check=0, pre-check=0");
  header("Pragma:no-cache");
  header("Expires:0");
  // Set the number of seconds the script is allowed to run
  @set_time_limit(600);
  readfile($file);
}
/*
--------------------------------------------------------------------------------------------
|| BEGIN
--------------------------------------------------------------------------------------------
*/
  $sep = "\t";

  if (isset($_REQUEST['id']))
  {
    $id = $_REQUEST['id'];
    if (!is_numeric($id))
    {
      mail("freek.ceymeulen@pandora.be", "Possible break-in attempt", "On page ".$_SERVER['PHP_SELF']." from IP ".$_SERVER['REMOTE_ADDR']." using method ".$_SERVER['REQUEST_METHOD']." on url ".$_SERVER['REQUEST_URI'].": Tornooi_id \"".$id."\" is not numeric.", "From: webmaster@badmintonsport.be");
      exit;
    }
    $query = "SELECT tornooi
                   , datum
                   , DATE_FORMAT(start_dt, '%%d/%%m/%%Y') AS start_dt
                   , organisatie
                   , type
                   , doelgroep
                   , reeks_dis
                   , enkel
                   , dubbel
                   , gemengd
                   , zelfde_disc_mag
                   , opmerking
                   , info
                   , dt_inschr
                   , email
                   , naam
                   , taal
                   , verzonden
                   , publiceer_uren
                FROM inschr_torn
               WHERE id = %d";
    $sql  = sprintf($query, mysql_real_escape_string($id));
    $result = mysql_query($sql, $badm_db) or badm_mysql_die();
    if (mysql_num_rows($result) == 0)
    {
      echo "No data found";
    }
    else
    {
      $tornooi = mysql_fetch_object($result);
      $query = "SELECT SUBSTRING(naam, LOCATE(' ', naam) + 1, LENGTH(naam) - LOCATE(' ', naam))  AS Naam
                     , SUBSTRING(naam, 1, LOCATE(' ', naam))  AS Voornaam
                     , lidnr
                     , klassement
                     , DATE_FORMAT(geb_dt, '%%d/%%m/%%Y') AS Geboortedatum
                     , (TO_DAYS(NOW()) - TO_DAYS(geb_dt)) / 365 AS Leeftijd
                     , enkel
                     , reeksE
                     , discipline1
                     , partner
                     , klasE
                     , clubE
                     , lidnrE
                     , dubbel
                     , reeksD
                     , discipline2
                     , dub_partner
                     , klasD
                     , clubD
                     , lidnrD
                     , gemengd
                     , reeksG
                     , discipline3
                     , mix_partner
                     , klasG
                     , clubG
                     , lidnrG
                     , aanvangsuur
                     , DATE_FORMAT(dt_verzonden, '%%d/%%m/%%Y') AS dt_verzonden
                     , DATE_FORMAT(dt_creatie, '%%d/%%m/%%Y') AS dt_creatie
                 FROM inschr_spel
                WHERE inschr_torn_id = %d
                ORDER BY 1, 2";
      $sql  = sprintf($query, mysql_real_escape_string($_REQUEST['id']));
      $result = mysql_query($sql, $badm_db) or badm_mysql_die();
      if (mysql_num_rows($result) == 0)
      {
        echo "No data found";
      }
      else
      {
        $file = "inschrijvingen.xls";
        if (!$file_handle = fopen($file, "w"))
        {
          echo "<br>Kan bestand ($file) niet openen/aanmaken";
          exit;
        }
        $data = "Naam".$sep."Voornaam".$sep."VBL Lidnummer".$sep."Klassement".$sep."Geboortedatum".$sep."Leeftijd".$sep.
                "Enkel".$sep."Reeks Enkel".$sep."Discipline 1".$sep."Partner".$sep."Klassement Partner".$sep."Club Partner"
                .$sep."Lidnr Partner".$sep."Dubbel".$sep."Reeks Dubbel".$sep."Discipline 2".$sep."Dubbel Partner".$sep.
                "Klassement Partner".$sep."Club Partner".$sep."Lidnr Partner".$sep."Gemengd".$sep."Reeks Gemengd".$sep.
                "Discipline 3".$sep."Mixed Partner".$sep."Klassement Partner".$sep."Club Partner".$sep."Lidnr Partner"
                .$sep."Aanvangsuur".$sep."Verzonden".$sep."Creatie\n";
        if (!fwrite($file_handle, $data))
        {
          echo "<br/>Kan niet schrijven naar bestand ($file)";
        }

        while ($row = mysql_fetch_array($result, MYSQL_NUM))
        {
          $data = "";
          for ($i=0; $i < count($row); $i++)
          {
            if ($i == count($row) - 1)
            {
              $data .= $row[$i]."\n";
            }
            else
            {
              $data .= $row[$i].$sep;
            }
          }
          if (!fwrite($file_handle, $data))
          {
            echo "<br/>Kan niet schrijven naar bestand ($file)";
          }
        }
        //echo "<br>Export met succes gemaakt naar $file";
        fclose($file_handle);
       ForceFileDownload($file);
     }
   }
 }
  //mysql_close($badm_db);
?>