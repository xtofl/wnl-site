<html>
<head>
<title></title>
<meta http-equiv="Content-type" content="text/html; charset=iso-8859-1" />
<link href="../css/badminton.css" rel="stylesheet" type="text/css">
<style type="text/css">
<!--
.inputveld {
    border-style : none;
    border : none;
}
-->
</style>
</head>
<body>
      <table width="100%" border="1" cellspacing="0" cellpadding="2">
      <form name="inschrijving">
      <tr align="left" valign="center">
      <td width="24%">
        <input type="text" name ="naam" size="24" value="
<?php
   // Connect to DB
   require_once "../functies/badm_db.inc.php";
   $badm_db = badm_conn_db();
   
   $query = "SELECT enkel
                  , dubbel
                  , gemengd
               FROM inschr_torn
              WHERE id = %d";
   $sql = sprintf($query, mysql_real_escape_string($_GET["id"]));
   $result = mysql_query ($sql, $badm_db) or badm_mysql_die();
   $tornooi = mysql_fetch_object($result);
   mysql_free_result($result);

      $gevonden = False;
      if ($_GET["naam"] != '')
      {
         // create SQL statement: haal klassement en lidnr op
         $sql = "SELECT naam
                      , klassement
                      , lidnr
                 FROM bad_spelers
		 WHERE naam = '".$_GET["naam"]."'";
         $result = mysql_query ($sql, $badm_db) or badm_mysql_die();
         if(mysql_num_rows($result) == 1)
         {
            $gegevens = mysql_fetch_object($result);
            $gevonden = True;
            echo $gegevens->naam;
         }
         else
         {
            echo $_GET["naam"];
         }
         mysql_free_result($result);
      }
      else
      {
         echo "Vul in";
      }
      mysql_close($badm_db);
?>
" maxlength=30 class="inputveld" onMouseEnter="window.status='Geef eerst de voornaam';return true" onMouseLeave="window.status='';return true" onFocus="window.status='Geef eerst de voornaam';return true" onChange="location.href='vulin.php?id=<?=$_GET["id"]?>&naam='+this.value"></TD>
      <td align="center" width="4%"><p><input type="text" name="klas" size="2" value="
<?php
      if ($gevonden)
      {
         echo $gegevens->klassement;
      }
?>
" maxlength=2 class="inputveld" onMouseEnter="window.status='Klassement';return true" onMouseLeave="window.status='';return true"></p></TD>
      <td align="center" width="6%"><p><input type="text" name="lidnr" size="4" value="
<?php
      if ($gevonden)
      {
         echo $gegevens->lidnr;
      }
?>
" maxlength=5 class="inputveld" onMouseEnter="window.status='Lidnummer bij de VBL';return true" onMouseLeave="window.status='';return true"></p></TD>
      <td align="center" width="4%"> <p><input type="text"
<?php
   if ($tornooi->enkel == 'n')
      echo " disabled ";
?>
name="e"       size="1"  value="" maxlength=1  class="inputveld" onMouseEnter="window.status='Zet een X als je enkel wil spelen';return true" onMouseLeave="window.status='';return true" onFocus="window.status='Zet een X als je enkel wil spelen';return true"></p></TD>
      <td align="center" width="4%"> <p><input type="text"
<?php
   if ($tornooi->dubbel == 'n')
      echo " disabled ";
?>
name="d"       size="1"  value="" maxlength=1  class="inputveld" onMouseEnter="window.status='Zet een X als je dubbel wil spelen';return true" onMouseLeave="window.status='';return true"></p></TD>
      <td align="center" width="4%"> <p><input type="text"
<?php
   if ($tornooi->gemengd == 'n')
      echo " disabled ";
?>
name="g"       size="1"  value="" maxlength=1  class="inputveld" onMouseEnter="window.status='Zet een X als je dubbel gemengd wil spelen';return true" onMouseLeave=\"window.status='';return true"></p></TD>
      <td                width="27%"><p><input type="text"
<?php
   if ($tornooi->dubbel == 'n')
      echo " disabled ";
?>
name="dubbelp" size="27" value="" maxlength=45 class="inputveld" onMouseEnter="window.status='Als je partner van een andere club is, geef dan ook lidnummer en club achter de naam';return true" onMouseLeave="window.status='';return true" onFocus="window.status='Als je partner van een andere club is, geef dan ook lidnummer en club achter de naam';return true"></p></TD>
      <td                width="27%"><p><input type="text"
<?php
   if ($tornooi->gemengd == 'n')
      echo " disabled ";
?>
name="mixedp"  size="27" value="" maxlength=45 class="inputveld" onMouseEnter="window.status='Als je partner van een andere club is, geef dan ook lidnummer en club achter de naam';return true" onMouseLeave="window.status='';return true" onFocus="window.status='Als je partner van een andere club is, geef dan ook lidnummer en club achter de naam';return true"></p></TD>
      </tr>
      </form>
      </table>
<?php
      if ($gevonden)
      {
         echo "<script language=\"JavaScript\"><!--\n";
         echo "document.inschrijving.e.focus();\n";
         echo "//--></script>\n";
      } 
      else
      {
         if (isset($_GET["naam"]))
         {
            echo "<script language=\"JavaScript\"><!--\n";
            echo "document.inschrijving.klas.focus();\n";
            echo "//--></script>\n";
         }
         else
         {
            echo "<script language=\"JavaScript\"><!--\n";
            echo "document.inschrijving.naam.focus();\n";
            echo "//--></script>\n";
         }
      }
?>
</body>
</html>