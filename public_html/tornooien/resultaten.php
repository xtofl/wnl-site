<?php
  require_once "../functies/website_usage.php";
?>
<!DOCtype HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<META NAME="robots"                  CONTENT="index,follow">
<META NAME="keywords"                CONTENT="resultaten,tornooi,toernooi,overwinning,winnaar,tornooien,badminton,batminton,w&l,badmintonvereniging,club,badmintonclub,sport,Belgium,Veltem,Herent,Leuven,Winksele,Belgi�" />
<META NAME="description"             CONTENT="W&L bv : tornooiresultaten" />
<META NAME="abstract"                CONTENT="badminton, club">
<META NAME="Area"                    CONTENT="General">
<META NAME="Author"                  CONTENT="W&L bv">
<META NAME="Copyright"               CONTENT="(c) 2003-2004 W&L bv">
<META NAME="Creator"                 CONTENT="W&L bv">
<META NAME="Generator"               CONTENT="W&L bv">
<META NAME="Owner"                   CONTENT="W&L bv">
<META NAME="Publisher"               CONTENT="W&L bv">
<META NAME="Rating"                  CONTENT="General">
<META NAME="revisit-after"           CONTENT="7 Days">
<META NAME="revisit"                 CONTENT="7 Days">
<META NAME="document-classification" CONTENT="Internet">
<META NAME="document-type"           CONTENT="Public">
<META NAME="document-rating"         CONTENT="Safe for Kids">
<META NAME="document-distribution"   CONTENT="Global">
<META HTTP-EQUIV="Content-Type"      CONTENT="text/html; charset=iso-8859-1" />
<META HTTP-EQUIV="Audience"          CONTENT="General">
<META HTTP-EQUIV="content-language"  CONTENT="NL">
<META HTTP-EQUIV="Pragma"            CONTENT="no-cache">
<META HTTP-EQUIV="Expires"           CONTENT="now">
<title>W&amp;L badmintonvereniging vzw - Resultaten</title>
<link href="http://www.badmintonsport.be/css/badminton.css" rel="stylesheet" type="text/css">
<script type="text/javascript" language="JavaScript1.2" src="http://www.badmintonsport.be/scripts/stm31.js"></script>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" class="badminton">
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr> 
    <td height="95" colspan="2" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr> 
          <td width="100%" height="95"><table width="100%" border="0" cellpadding="0" cellspacing="0" bordercolor="#CCCCCC" bgcolor="#FFFFFF">
              <tr> 
                <td align="center" valign="middle" bgcolor="#006600"><img src="../images/spacer.gif" width="200" height="4"></td>
              </tr>
              <tr> 
                <td align="center" valign="middle" bgcolor="#CCCCCC"><img src="../images/pt_transp.gif" width="500" height="5"></td>
              </tr>
              <tr> 
                <td width="100%" align="center" valign="middle" bgcolor="#CCCCCC"> 
                  <h4>W&amp;L Badmintonvereniging vzw, Mechelsesteenweg 387, 3020 
                    Herent</h4>
                  </td>
              </tr>
              <tr>
                <td align="center" valign="top" bgcolor="#CCCCCC"><img src="../images/chinas.gif" width="400" height="60"></td>
              </tr>
              <tr> 
                <td align="center" valign="middle" bgcolor="#CCCCCC"><img src="../images/pt_transp.gif" width="500" height="5"></td>
              </tr>
              <tr> 
                <td align="center" valign="middle" bgcolor="#336600"><img src="../images/spacer.gif" width="200" height="4"></td>
              </tr>
            </table></td>
        </tr>
      </table>
   </td>
  </tr>
  <tr>
    <td width="150" height="213" valign="top">
<?php
   require "../templates/template.php";
   invoegen_template();  //menu
?>
       &nbsp; 
    </td>
    <td width="100%" valign="top">
      <table width="99%" border="0" cellspacing="0" cellpadding="0">
	    <tr>
		  <td><h1>Resultaten seizoen 
<?php
   $seizoen = (int)$_GET['s'] - 1;
   echo $seizoen."-".$_GET['s'];
?>
      </h1>
	      </td>
		  <td width="100" valign="center"><H3>(Totaal: 
<?php
    require_once "../functies/badm_db.inc.php";
    $badm_db = badm_conn_db();
// create SQL statement om het aantal overwinningen van dit seizoen te berekenen
   $sql = "SELECT CONCAT(DATE_FORMAT(tor.dt_van, '%d/%m/%y'), IF(tor.dt_tot IS NULL, ' ' , ' - '), IFNULL(DATE_FORMAT(tor.dt_tot, '%d/%m/%y'), '')) as datum
                 ,tor.plaats as plaats
                 ,IF (tor.reeks = 'G', CONCAT('GD', tor.klassement), CONCAT(CASE sp.geslacht WHEN 'M' THEN 'H' ELSE 'D' END, tor.reeks, tor.klassement)) as discipline
                 ,CONCAT(sp.naam, IF(tor.partner = '', ' ' , ' & '), IFNULL(tor.partner, '')) as winnaar
           FROM bad_spelers as sp
               ,bad_tornooien as tor
           WHERE tor.spelers_id = sp.id
           AND tor.categorie IS NULL
           AND IF(DATE_FORMAT(tor.dt_van, '%m') < 8, DATE_FORMAT(tor.dt_van, '%Y'), DATE_FORMAT(tor.dt_van, '%Y') + 1) = ".$_GET['s']
        ." ORDER BY tor.dt_van DESC, tor.klassement, discipline";
   $result = mysql_query ($sql, $badm_db) or badm_mysql_die();
// format results by row
   $dat = "nu";   //startwaarde
   $pla = "hier"; //startwaarde
   $dis = "Q";    //startwaarde
   $aant_overwinningen = 0;
   while ($badm = mysql_fetch_object($result))
   {
      if ($dat != $badm->datum || $pla != $badm->plaats || $dis != $badm->discipline)
      {
         $aant_overwinningen = $aant_overwinningen + 1;
      }
      $dat = $badm->datum;
      $pla = $badm->plaats;
      $dis = $badm->discipline;
   }
   echo $aant_overwinningen;
?>
	  )</H3>
	      </td>
	    </tr>
      </table>  
      <table width="99%" border="1" cellspacing="0" cellpadding="2">
        <tr bgcolor="#666699" class="geel">
          <td ALIGN="center" width="11%">
            <p>Datum</p>
          </td>
          <td ALIGN="center" width="23%">
            <p>Tornooi</p>
          </td>
          <td ALIGN="center" width="10%">
            <p>Discipline</p>
          </td>
          <td ALIGN="center" width="56%">
            <p>Winnaar(s)</p>
          </td>
        </tr>
<?php
// create SQL statement 
   $sql = "SELECT CONCAT(DATE_FORMAT(tor.dt_van, '%d/%m/%y'), IF(tor.dt_tot IS NULL, ' ' , ' - '), IFNULL(DATE_FORMAT(tor.dt_tot, '%d/%m/%y'), '')) as datum
                 ,tor.plaats as plaats
                 ,IF (tor.reeks = 'G', CONCAT('GD', tor.klassement), CONCAT(CASE sp.geslacht WHEN 'M' THEN 'H' ELSE 'D' END, tor.reeks, tor.klassement)) as discipline
                 ,CONCAT(sp.naam, IF(IFNULL(tor.partner, '') = '', ' ' , ' & '), IFNULL(tor.partner, '')) as winnaar
           FROM bad_spelers as sp
               ,bad_tornooien as tor
           WHERE tor.spelers_id = sp.id
           AND tor.categorie IS NULL
           AND IF(DATE_FORMAT(tor.dt_van, '%m') < 8, DATE_FORMAT(tor.dt_van, '%Y'), DATE_FORMAT(tor.dt_van, '%Y') + 1) = ".$_GET['s']
        ." ORDER BY tor.dt_van DESC, tor.plaats, tor.klassement, discipline";
   $result = mysql_query ($sql, $badm_db) or badm_mysql_die();
   // format results by row
   $dat = "nu";   //startwaarde
   $pla = "hier"; //startwaarde
   $dis = "Q";    //startwaarde
   while ($badm = mysql_fetch_object($result))
   {
      if ($dat != $badm->datum || $pla != $badm->plaats || $dis != $badm->discipline)
      {
         echo "<tr align=\"left\" valign=\"top\"><td width=\"20%\"><p>";
         if ($dat != $badm->datum || $pla != $badm->plaats)
            echo $badm->datum;
         echo "</p></td><td width=\"30%\"><p>";
         if ($dat != $badm->datum || $pla != $badm->plaats)
            echo $badm->plaats;
         echo "</p></td><td width=\"8%\"><p>";
         if ($dat != $badm->datum || $pla != $badm->plaats || $dis != $badm->discipline)
         {
            echo $badm->discipline;
            echo "</p></td><td width=\"42%\"><p>";
            echo $badm->winnaar;
            echo "</p></td></tr>";
         }
      }
      $dat = $badm->datum;
      $pla = $badm->plaats;
      $dis = $badm->discipline;
   }
?>
        </tr>
      </table>
      <p>&nbsp;</p>
    </td>
  </tr>
  <tr> 
    <td height="1"><img src="../images/spacer.gif" alt="" width="150" height="1"></td>
    <td></td>
  </tr>
</table>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <!--DWLayoutTable-->
  <tr> 
    <td width="100%" height="50" valign="top" bgcolor="#FFFFFF"><table width="100%" border="0" cellpadding="0" cellspacing="0" bordercolor="#CCCCCC" bgcolor="#CCCCCC">
        <!--DWLayoutTable-->
        <tr> 
          <td align="center" valign="middle" bordercolor="#CCCCCC" bgcolor="#006600"><img src="../images/spacer.gif" width="200" height="4"></td>
        </tr>
        <tr> 
          <td width="100%" align="center" valign="middle" bordercolor="#CCCCCC" bgcolor="#CCCCCC" class="gmenu">Last change: <!-- InstanceBeginEditable name="datum" -->
            <!-- #BeginDate format:It1 -->04-06-2007<!-- #EndDate -->
            <!-- InstanceEndEditable --> 
            E-mail: <a href="mailto:webmaster@badmintonsport.be">webmaster@badmintonsport.be</a></td>
        </tr>
        <tr>
          <td align="center" valign="middle" bordercolor="#CCCCCC" bgcolor="#006600" class="gmenu"><img src="../images/spacer.gif" width="200" height="4"></td>
        </tr>
      </table> </td>
  </tr>
</table>
</body>
</html>
<?php
  log_website_usage($start_time, $seizoen, $badm_db);
?>