<?php
/**
 * inschrijving.php
 *
 * object     : Page to enroll for a tournament
 * author     : Freek Ceymeulen
 * created    : 11/06/2007
 * parameters : id : id of inscription in inschr_spel table (not set when new inscription)
 *              tornooi_id : id of tournament in inschr_torn table
 *              actie : [add|edit|delete]
 **/

  require_once "../functies/website_usage.php";
  // Connect to the DB (must be done before calling sess.php)
  require_once "../functies/badm_db.inc.php";
  $badm_db2 = badm_conn_db();
  require_once "../functies/sess.php";
  require_once "../functies/general_functions.php";

  $debug = "";

  if ($_SESSION['auth'] == true)
  {
    $login = "Ingelogd als <a href=\"../login.php?action=logout&ref=".$PHP_SELF."?".urlencode($_SERVER["QUERY_STRING"])."\" title=\"Klik hier als je niet ".$_SESSION['username']." bent\">".$_SESSION['username']."</a>";
  }
  else
  {
    // niet ingelogd -> ga naar login pagina
    // geef deze pagina als parameter mee zodat na succesvolle login teruggekeerd kan worden naar deze pagina
    //session_write_close;
    mysql_close($badm_db2);
    header("Location: http://www.badmintonsport.be/login.php?ref=".$PHP_SELF."?".urlencode($_SERVER["QUERY_STRING"]));
    //header("Location: http://www.badmintonsport.be/login.php?ref=".$_SERVER["REQUEST_URI"]);
    exit;
  }
/*
-- Functions --
*/
  function handle_submitted_data()
  {
    global $debug;
    $debug .= "<br>Handle_Submitted_Data\n";
    global $voornaam;
    global $achternaam;
    global $naam;
    global $geb_dt;
    global $klassement;
    global $lidnr;
    global $email;
    global $discipline1;
    global $enkel;
    global $reeksE;
    global $partner;
    global $klasE;
    global $clubE;
    global $lidnrE;
    global $emailE;
    global $discipline2;
    global $dubbel;
    global $reeksD;
    global $dub_partner;
    global $klasD;
    global $clubD;
    global $lidnrD;
    global $emailD;
    global $discipline3;
    global $gemengd;
    global $reeksG;
    global $mix_partner;
    global $klasG;
    global $clubG;
    global $lidnrG;
    global $emailG;
    global $direct;
    global $again;
    global $disableE;
    global $disableD;
    global $disableG;
    if (isset($_REQUEST["voornaam"]))
    {
      $voornaam   = $_REQUEST["voornaam"];
      $achternaam = $_REQUEST["achternaam"];
      $naam       = $voornaam." ".$achternaam;
    }
    else
    {
      $naam = $_REQUEST["naam"];
    }
    $geb_dt      = $_REQUEST["geboortedatum"];
    $klassement  = $_REQUEST["klassement"];
    $lidnr       = $_REQUEST["lidnr"];
    $email       = $_REQUEST["email"];
    $discipline1 = $_REQUEST["discipline1"];
    if (isset($_REQUEST["enkel"]))
    {
      $enkel = "1";
    }
    else
    {
      $enkel = "0";
      $disableE = " disabled";
    }
    $reeksE      = $_REQUEST["reeksE"];
    $partner     = $_REQUEST["partner"];
    $klasE       = $_REQUEST["klasE"];
    $clubE       = $_REQUEST["clubE"];
    $lidnrE      = $_REQUEST["lidnrE"];
    $emailE      = $_REQUEST["emailE"];
    $discipline2 = $_REQUEST["discipline2"];
    if (isset($_REQUEST["dubbel"]))
    {
      $dubbel = "1";
    }
    else
    {
      $dubbel = "0";
      $disableD = " disabled";
    }
    $reeksD      = $_REQUEST["reeksD"];
    $dub_partner = $_REQUEST["dub_partner"];
    $klasD       = $_REQUEST["klasD"];
    $clubD       = $_REQUEST["clubD"];
    $lidnrD      = $_REQUEST["lidnrD"];
    $emailD      = $_REQUEST["emailD"];
    $discipline3 = $_REQUEST["discipline3"];
    if (isset($_REQUEST["gemengd"]))
    {
      $gemengd = "1";
    }
    else
    {
      $gemengd = "0";
      $disableG = " disabled";
    }
    $reeksG      = $_REQUEST["reeksG"];
    $mix_partner = $_REQUEST["mix_partner"];
    $klasG       = $_REQUEST["klasG"];
    $clubG       = $_REQUEST["clubG"];
    $lidnrG      = $_REQUEST["lidnrG"];
    $emailG      = $_REQUEST["emailG"];
    $direct      = (isset($_REQUEST["direct"])) ? " CHECKED" : "";
    $again       = (isset($_REQUEST["again"])) ? " CHECKED" : "";
  }

  function handleCheckboxes($E, $D, $G)
  {
    global $debug;
    $debug .= "<br>handleCheckboxes(".$E.",".$D.",".$G.")\n";
    global $enkel;
    global $dubbel;
    global $gemengd;
    $enkel   = ($E == "1") ? " CHECKED" : "";
    $dubbel  = ($D == "1") ? " CHECKED" : "";
    $gemengd = ($G == "1") ? " CHECKED" : "";
  }

  function emptyForm()
  {
    global $debug;
    $debug .= "<br>emptyForm\n";
    global $voornaam;
    global $achternaam;
    global $naam;
    global $geb_dt;
    global $klassement;
    global $lidnr;
    global $email;
    global $discipline1;
    global $enkel;
    global $reeksE;
    global $partner;
    global $klasE;
    global $clubE;
    global $lidnrE;
    global $emailE;
    global $discipline2;
    global $dubbel;
    global $reeksD;
    global $dub_partner;
    global $klasD;
    global $clubD;
    global $lidnrD;
    global $emailD;
    global $discipline3;
    global $gemengd;
    global $reeksG;
    global $mix_partner;
    global $klasG;
    global $clubG;
    global $lidnrG;
    global $emailG;
    global $direct;
    global $again;
    global $disableE;
    global $disableD;
    global $disableG;
    $voornaam    = "";
    $achternaam  = "";
    $naam        = "";
    $geb_dt      = "";
    $leeftijd    = "";
    $klassement  = "";
    $lidnr       = "";
    $email       = "";
    $discipline1 = "enkel";
    $discipline2 = "dubbel";
    $discipline3 = "gemengd";
    $enkel       = "";
    $dubbel      = "";
    $gemengd     = "";
    $reeksE      = "";
    $reeksD      = "";
    $reeksG      = "";
    $partner     = "";
    $dub_partner = "";
    $mix_partner = "";
    $klasE       = "";
    $klasD       = "";
    $klasG       = "";
    $clubE       = "";
    $clubD       = "";
    $clubG       = "";
    $lidnrE      = "";
    $lidnrD      = "";
    $lidnrG      = "";
    $emailE      = "";
    $emailD      = "";
    $emailG      = "";
    $disableE    = " disabled";
    $disableD    = " disabled";
    $disableG    = " disabled";
  }

  function check_policy($conn, $actie, $gebruikersnaam, $spelersnaam, $inschr_spel_id)
  {
    global $debug;
    $debug .= "<br>Check_Policy(".$actie.",".$gebruikersnaam.",".$spelersnaam.",".$tornooi_id.")\n";
    $err_msg = "";
	if (!isset($_SESSION['tornooiverantwoordelijke']))
	{
      $query = "SELECT * FROM inschr_spel WHERE id = %d AND ( usid_wijz = '%s' OR naam = '%s' OR dub_partner = '%s' OR mix_partner = '%s' )";
      $sql  = sprintf($query, mysql_real_escape_string($inschr_spel_id, $conn)
                            , mysql_real_escape_string($gebruikersnaam, $conn)
                            , mysql_real_escape_string($spelersnaam, $conn)
                            , mysql_real_escape_string($spelersnaam, $conn)
                            , mysql_real_escape_string($spelersnaam, $conn) );
      $debug .= "\n<br>".$sql;
      $result = mysql_query($sql, $conn) or badm_mysql_die();
      if (mysql_num_rows($result) != 1)
      {
        $err_msg = "Sorry ".substr($spelersnaam, 0, strpos($spelersnaam, " ")).", maar je mag deze inschrijving niet ".$actie;
      }
      mysql_free_result($result);
	}
    $debug .= "<br>Return=".$err_msg;
    return $err_msg;
  }

  function sendMails ($addressE, $messageE, $addressD, $messageD, $addressG, $messageG, $tornooi, $email)
  {
    global $debug;
    $debug .= "<br>SendMails(".$addressD.",".$messageD.",".$addressG.",".$messageG.",".$tornooi.")\n";
    $headers  = "MIME-Version: 1.0\r\n";
    $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
    $headers .= "From: W&L bv <webmaster@badmintonsport.be>\r\n";
    if (!empty($email))
    {
      $headers .= "CC: ".$email."\r\n";
    }
    if (isset($addressE))
    {
      mail($addressE, $tornooi, $messageE, $headers);
    }
    if (isset($addressD))
    {
      mail($addressD, $tornooi, $messageD, $headers);
    }
    if (isset($addressG))
    {
      mail($addressG, $tornooi, $messageG, $headers);
    }
  }

  function check_data($conn, $naam, $doelgroep, $tornooi_id, $tornooi, $inschr_spel_id, $zelfde_disc_mag)
  {
    global $debug;
    $debug .= "<br>check_data(".$naam.",".$doelgroep.",".$tornooi_id.",".$tornooi.",".$inschr_spel_id.",".$zelfde_disc_mag.")\n";
    global $addressE;
    global $addressD;
    global $addressG;
    global $messageE;
    global $messageD;
    global $messageG;
    global $emailE;
    global $emailD;
    global $emailG;
    $err_msg = "";
    if (empty($naam))
    {
      $err_msg .= "<br>Je bent je naam vergeten in te vullen.";
    }
    if (empty($_REQUEST["lidnr"]))
    {
      $err_msg .= "<br>Je bent je VBL lidnummer vergeten in te vullen.";
    }
    if (empty($_REQUEST["klassement"]))
    {
      $err_msg .= "<br>Je bent je klassement vergeten in te vullen.";
    }
    if ($doelgroep == 'J' || $doelgroep == 'V')
    {
      if (empty($_REQUEST["geboortedatum"]))
      {
        $err_msg .= "<br>Je bent je geboortedatum vergeten in te vullen.";
      }
      elseif (!ereg("^([0-9]{2})/([0-9]{2})/([0-9]{4})$", $_REQUEST["geboortedatum"], $parts))
      {
        $err_msg .= "<br>Je geboortedatum is niet correct in het formaat DD/MM/JJJJ.";
      }
      elseif (!checkdate($parts[2],$parts[1],$parts[3]))
      {
        $err_msg .= "<br>Je geboortedatum is niet correct. Controleer of de maand tussen 1 en 12 is en of de dag geldig is voor die maand.";
      }
      elseif (intval($parts[3]) < (date("Y") - 77))
      {
        $err_msg .= "<br>Je geboortedatum is niet correct. Je moet nog leven om aan een tornooi deel te nemen.";
      }
    }
    if (isset($_REQUEST["enkel"]))
    {
      if (empty($_REQUEST["reeksE"]))
      {
        $err_msg .= "<br>Je bent de reeks voor het enkelspel vergeten in te vullen.";
      }
      if ($_REQUEST["discipline1"] != 'enkel')
      {
        if (empty($_REQUEST["partner"]))
        {
          $err_msg .= "<br>Je bent je partner voor discipline 1 vergeten opgeven. Als je geen partner hebt en toch wenst te dubbelen zet dan een kruisje.";
        }
        elseif (strtolower($_REQUEST["partner"]) != 'x')
        {
          if (empty($_REQUEST["klasE"]))
          {
            $err_msg .= "<br>Je bent het klassement van je partner voor discipline 1 vergeten in te vullen.";
          }
          if (empty($_REQUEST["clubE"]))
          {
            $err_msg .= "<br>Je bent de club van je partner voor discipline 1 vergeten in te vullen.";
          }
          elseif ($_REQUEST["clubE"] == "W&L bv" || $_REQUEST["clubE"] == "W&L")
          {
            // Check is very difficult in this case
            /*TODO*/
            ;
          }
          if (empty($_REQUEST["lidnrE"]))
          {
            $err_msg .= "<br>Je bent het VBL lidnummer van je partner voor discipline 1 vergeten in te vullen.";
          }
          if ($err_msg == "" && strtolower($_SESSION['username']) != strtolower($naam))
          {
            // Notify this player about his/her subscription
            $addressE = $emailE;
            $messageE = "Beste ".ucwords(substr($naam, 0, strpos($naam, " "))).",<br><br>".$_SESSION['username']." heeft jou zojuist ingeschreven voor het tornooi van <a href=\"http://www.badmintonsport.be/tornooien/inschrijvingen.php?id=".$tornooi_id."\">".$tornooi."</a>.<br><br>Mvg,<br>webmaster <a href=\"http://www.badmintonsport.be\">W&amp;L</a> bv";
          }
        }
      }
    }
    if (isset($_REQUEST["dubbel"]))
    {
      if (empty($_REQUEST["reeksD"]))
      {
        $err_msg .= "<br>Je bent de reeks voor discipline 2 vergeten in te vullen.";
      }
      if ($_REQUEST["discipline2"] != 'enkel')
      {
        if (empty($_REQUEST["dub_partner"]))
        {
          $err_msg .= "<br>Je bent je partner voor discipline 2 vergeten opgeven. Als je geen partner hebt en toch wenst te dubbelen zet dan een kruisje.";
        }
        elseif (strtolower($_REQUEST["dub_partner"]) != 'x')
        {
          if (empty($_REQUEST["klasD"]))
          {
            $err_msg .= "<br>Je bent het klassement van je partner voor discipline 2 vergeten in te vullen.";
          }
          if (empty($_REQUEST["clubD"]))
          {
            $err_msg .= "<br>Je bent de club van je partner voor discipline 2 vergeten in te vullen.";
          }
          elseif ($_REQUEST["clubD"] == "W&L bv" || $_REQUEST["clubD"] == "W&L")
          {
            if ($zelfde_disc_mag == 'j')
            {
              // Check is very difficult in this case
              /*TODO*/
              ;
            }
            else
            {
              // Check if partner is already enrolled
              $query = "SELECT dub_partner FROM inschr_spel WHERE inschr_torn_id = %d AND naam = '%s' AND id <> %d";
              $sql  = sprintf($query, mysql_real_escape_string($tornooi_id, $conn)
                                    , mysql_real_escape_string($_REQUEST["dub_partner"], $conn)
                                    , mysql_real_escape_string($inschr_spel_id, $conn) );
              $debug .= "\n<br>".$sql;
              $result = mysql_query($sql, $conn) or badm_mysql_die();
              $partnerD = mysql_fetch_object($result);
              if (mysql_num_rows($result) == 0)
              {
                if (!empty($emailD))
                {
                  if (!isset($_SESSION['tornooiverantwoordelijke']))
                  {
                    // Send a mail to the partner to inform him about this subscription and to ask him to subscribe himself too
                    $addressD = $emailD;
                    $messageD = "Beste ".ucwords(substr($_REQUEST["dub_partner"], 0, strpos($_REQUEST["dub_partner"], " "))).",";
                    $messageD .= "<br><br>".$naam." heeft zich zojuist ingeschreven voor het tornooi van <a href=\"http://www.badmintonsport.be/tornooien/inschrijvingen.php?id=".$tornooi_id."\">".$tornooi."</a> met jou als dubbelpartner.";
                    $messageD .= "<br>Je wordt bij deze vriendelijk verzocht om zelf ook in te schrijven voor dit tornooi.";
                    $messageD .= "<br><br>Mvg,<br>webmaster <a href=\"http://www.badmintonsport.be\">W&amp;L</a> bv";
                  }
                }
              }
              else // Partner is already enrolled
              {
                $debug .= "\n<br>partner=".$partnerD->dub_partner;
                if (strlen($partnerD->dub_partner) == 0 || strtolower($partnerD->dub_partner) == 'x')
                {
                  if (!empty($emailD))
                  {
                    // Send a mail to the partner to inform him about this subscription and to ask him to subscribe himself too
                    $addressD = $emailD;
                    $messageD = "Beste ".ucwords(substr($_REQUEST["dub_partner"], 0, strpos($_REQUEST["dub_partner"], " "))).",";
                    $messageD .= "<br><br>".$naam." heeft zich zojuist ingeschreven voor het tornooi van <a href=\"http://www.badmintonsport.be/tornooien/inschrijvingen.php?id=".$tornooi_id."\">".$tornooi."</a> met jou als dubbelpartner.";
                    $messageD .= "<br>Je wordt bij deze vriendelijk verzocht om ".$naam." aan je inschrijving toe te voegen.";
                    $messageD .= "<br><br>Mvg,<br>webmaster <a href=\"http://www.badmintonsport.be\">W&amp;L</a> bv";
                  }
                }
                elseif (strtolower($partnerD->dub_partner) != strtolower($naam))
                {
                  $err_msg .= "<br>De persoon met wie je wil dubbelen heeft zelf al ingeschreven met iemand anders, nl. ".$partnerD->dub_partner.".";
                }
              }
              // Check if partner is not already partner of someone else
              $query = "SELECT naam FROM inschr_spel WHERE inschr_torn_id = %d AND dub_partner = '%s' AND id <> %d";
              $sql  = sprintf($query, mysql_real_escape_string($tornooi_id, $conn)
                                    , mysql_real_escape_string($_REQUEST["dub_partner"], $conn)
                                    , mysql_real_escape_string($inschr_spel_id, $conn) );
              $debug .= "\n<br>".$sql;
              $result = mysql_query($sql, $conn) or badm_mysql_die();
              $partnerD = mysql_fetch_object($result);
              if (mysql_num_rows($result) != 0)
              {
                $err_msg .= "<br>De persoon met wie je wil dubbelen staat al als partner geboekt met ".$partnerD->naam.".";
              }
            }
          }
          if (empty($_REQUEST["lidnrD"]))
          {
            $err_msg .= "<br>Je bent het VBL lidnummer van je partner voor discipline 2 vergeten in te vullen.";
          }
        }
      }
    }
    if (isset($_REQUEST["gemengd"]))
    {
      if ($_REQUEST["discipline3"] != 'enkel')
      {
        if (empty($_REQUEST["reeksG"]))
        {
          $err_msg .= "<br>Je bent de reeks voor discipline 3 vergeten in te vullen.";
        }
        if (empty($_REQUEST["mix_partner"]))
        {
          $err_msg .= "<br>Je bent je partner voor discipline 3 vergeten opgeven. Als je geen partner hebt en toch wenst te mixen zet dan een kruisje.";
        }
        elseif (strtolower($_REQUEST["mix_partner"]) != 'x')
        {
          if (empty($_REQUEST["klasG"]))
          {
            $err_msg .= "<br>Je bent het klassement van je partner voor discipline 3 vergeten in te vullen.";
          }
          if (empty($_REQUEST["clubG"]))
          {
            $err_msg .= "<br>Je bent de club van je partner voor discipline 3 vergeten in te vullen.";
          }
          elseif ($_REQUEST["clubG"] == "W&L bv" || $_REQUEST["clubG"] == "W&L")
          {
            if ($zelfde_disc_mag == 'j')
            {
              // Check is very difficult in this case
              /*TODO*/
              ;
            }
            else
            {
              // Check if partner is already enrolled
              $query = "SELECT mix_partner FROM inschr_spel WHERE inschr_torn_id = %d AND naam = '%s' AND id <> %d";
              $sql  = sprintf($query, mysql_real_escape_string($tornooi_id, $conn)
                                    , mysql_real_escape_string($_REQUEST["mix_partner"], $conn)
                                    , mysql_real_escape_string($inschr_spel_id, $conn) );
              $debug .= "\n<br>".$sql;
              $result = mysql_query($sql, $conn) or badm_mysql_die();
              $partnerG = mysql_fetch_object($result);
              if (mysql_num_rows($result) == 0)
              {
                // Get the E-mail address of the partner
                if (!empty($emailG))
                {
                  if (!isset($_SESSION['tornooiverantwoordelijke']))
                  {
                    // Send a mail to the partner to inform him about this subscription and to ask him to subscribe himself too
                    $addressG = $emailG;
                    $messageG = "Beste ".ucwords(substr($_REQUEST["mix_partner"], 0, strpos($_REQUEST["mix_partner"], " "))).",";
                    $messageG .= "<br><br>".$naam." heeft zich zojuist ingeschreven voor het tornooi van <a href=\"http://www.badmintonsport.be/tornooien/inschrijvingen.php?id=".$tornooi_id."\">".$tornooi."</a> met jou als mixed partner.";
                    $messageG .= "<br>Je wordt bij deze vriendelijk verzocht om zelf ook in te schrijven voor dit tornooi.";
                    $messageG .= "<br><br>Mvg,<br>webmaster <a href=\"http://www.badmintonsport.be\">W&amp;L</a> bv";
                  }
                }
              }
              else // Partner is already enrolled
              {
                $debug .= "\n<br>partner=".$partnerG->mix_partner;
                if (strlen($partnerG->mix_partner) == 0 || strtolower($partnerG->mix_partner) == 'x')
                {
                  // Get the E-mail address of the partner
                  if (!empty($emailG))
                  {
                    // Send a mail to the partner to inform him/her about this subscription and to ask him/her to subscribe himself too
                    $addressG = $emailG;
                    $messageG = "Beste ".ucwords(substr($_REQUEST["mix_partner"], 0, strpos($_REQUEST["mix_partner"], " "))).",";
                    $messageG .= "<br><br>".$naam." heeft zich zojuist ingeschreven voor het tornooi van <a href=\"http://www.badmintonsport.be/tornooien/inschrijvingen.php?id=".$tornooi_id."\">".$tornooi."</a> met jou als mixed partner.";
                    $messageG .= "<br>Je wordt bij deze vriendelijk verzocht om ".$naam." aan je inschrijving toe te voegen.";
                    $messageG .= "<br><br>Mvg,<br>webmaster <a href=\"http://www.badmintonsport.be\">W&amp;L</a> bv";
                  }
                }
                elseif (strtolower($partnerG->mix_partner) != strtolower($naam))
                {
                  $err_msg .= "<br>De persoon met wie je wil mixen heeft zelf al ingeschreven met iemand anders, nl. ".$partnerG->mix_partner.".";
                }
              }
              // Check if partner is not already partner of someone else
              $query = "SELECT naam FROM inschr_spel WHERE inschr_torn_id = %d AND mix_partner = '%s' AND id <> %d";
              $sql  = sprintf($query, mysql_real_escape_string($tornooi_id, $conn)
                                    , mysql_real_escape_string($_REQUEST["mix_partner"], $conn)
                                    , mysql_real_escape_string($inschr_spel_id, $conn) );
              $debug .= "\n<br>".$sql;
              $result = mysql_query($sql, $conn) or badm_mysql_die();
              $partnerG = mysql_fetch_object($result);
              if (mysql_num_rows($result) != 0)
              {
                $err_msg .= "<br>De persoon met wie je wil mixen staat al als partner geboekt met ".$partnerG->naam.".";
              }
            }
          }
          if (empty($_REQUEST["lidnrG"]))
          {
            $err_msg .= "<br>Je bent het VBL lidnummer van je partner voor discipline 3 vergeten in te vullen.";
          }
        }
      }
    }
    $debug .= "<br>Return=".$err_msg;
    return substr($err_msg, 4);
  }
/*
-- Begin --
*/
  if (isset($_REQUEST["tornooi_id"]))
  {
    $tornooi_id = $_REQUEST["tornooi_id"];
    if (!empty($tornooi_id) && !is_numeric($tornooi_id))
    {
      mail("freek.ceymeulen@pandora.be", "Possible break-in attempt", $_SESSION['username']." on page ".$_SERVER['PHP_SELF']." from IP ".$_SERVER['REMOTE_ADDR']." using method ".$_SERVER['REQUEST_METHOD']." on url ".$PHP_SELF."?".urlencode($_SERVER["QUERY_STRING"])." coming from ".$_SERVER["HTTP_REFERER"].": Tornooi_id \"".$tornooi_id."\" is not numeric.", "From: webmaster@badmintonsport.be");
      exit;
    }
  }
  else
  {
    mail("freek.ceymeulen@pandora.be", "Possible break-in attempt", $_SESSION['username']." on page ".$_SERVER['PHP_SELF']." from IP ".$_SERVER['REMOTE_ADDR']." using method ".$_SERVER['REQUEST_METHOD']." on url ".$PHP_SELF."?".urlencode($_SERVER["QUERY_STRING"])." coming from ".$_SERVER["HTTP_REFERER"].": Tornooi_id is undefined.", "From: webmaster@badmintonsport.be");
  }

  if (isset($_REQUEST["inschr_spel_id"]))
  {
    $inschr_spel_id = $_REQUEST["inschr_spel_id"];
    if (!empty($inschr_spel_id) && !is_numeric($inschr_spel_id))
    {
      mail("freek.ceymeulen@pandora.be", "Possible break-in attempt", $_SESSION['username']." on page ".$_SERVER['PHP_SELF']." from IP ".$_SERVER['REMOTE_ADDR']." using method ".$_SERVER['REQUEST_METHOD']." on url ".$PHP_SELF."?".urlencode($_SERVER["QUERY_STRING"])." coming from ".$_SERVER["HTTP_REFERER"].": Inschr_spel_id \"".$inschr_spel_id."\" is not numeric.", "From: webmaster@badmintonsport.be");
      exit;
    }
  }

  // Get the tournament info
  $query = "SELECT start_dt
                 , date_format(start_dt, '%%Y') AS jaar
                 , tornooi
                 , naam
                 , email
                 , enkel
                 , dubbel
                 , gemengd
                 , doelgroep
                 , taal
                 , zelfde_disc_mag
                 , DATE_FORMAT(dt_inschr, '%%Y-%%m-%%d') AS dt_inschr
              FROM inschr_torn
             WHERE id = %d";
  $sql  = sprintf($query, mysql_real_escape_string($tornooi_id));
  $debug .= "\n<br>".$sql;
  $result = mysql_query($sql, $badm_db2) or badm_mysql_die();
  $tornooi = mysql_fetch_object($result);

  $msg = ""; // Middle of page, when problem with input data
  $msg2 = ""; // At bottom of page, when action is not allowed
  $new_form = false;

  // Check if the subscription date is not closed
  if (strtotime(date("Y-m-d")) > strtotime($tornooi->dt_inschr))
  {
    $msg2 = "Sorry ".substr($_SESSION['username'], 0, strpos($_SESSION['username'], " ")).", maar de inschrijvingsperiode van dit tornooi is verlopen.";
  }
  else
  {
    if (isset($_REQUEST["actie"]))
    {
      $actie = $_REQUEST["actie"];
      $debug .= "\n<br>actie=".$actie;
      switch ($actie)
      {
        case "delete":
          // Check if user is allowed to delete this subscription
          $msg2 = check_policy($badm_db2, "verwijderen", $_SESSION['usid'], $_SESSION['username'], $inschr_spel_id);
          $debug .= "\n<br>msg2=".$msg2;
          if ($msg2 == "")
          {
            // Get some info first
            $inschrijving = mysql_query("SELECT naam, dub_partner, mix_partner, dt_verzonden FROM inschr_spel WHERE id = ".$inschr_spel_id, $badm_db2);
            $p = mysql_result($inschrijving, 0, "naam");
            $dp = mysql_result($inschrijving, 0, "dub_partner");
            $gp = mysql_result($inschrijving, 0, "mix_partner");
            $dt_verzonden = mysql_result($inschrijving, 0, "dt_verzonden");
            // Remove the subscription
            require_once "../admin/dml/inschr_spel.php";
            delete_inschrijving($badm_db2, $inschr_spel_id);
            // Inform partners (if any) about this removal
            if (strtolower($p) != strtolower($_SESSION['username']))
            {
              $lid = mysql_query("SELECT email FROM bad_spelers WHERE naam = '".$p."'", $badm_db2);
              if (mysql_num_rows($lid) == 1)
              {
                $email = mysql_result($lid, 0, "email");
                if (!empty($email))
                {
                  mail( $email
                      , "Info: Inschrijving voor ".$tornooi->tornooi." verwijderd"
                      , "Beste ".ucwords(substr($p, 0, strpos($p, " "))).",<br><br>".$_SESSION['username']." heeft zojuist jouw inschrijving voor het tornooi van <a href=\"http://www.badmintonsport.be/tornooien/inschrijvingen.php?id=".$tornooi_id."\">".$tornooi->tornooi."</a> verwijderd.<br><br>Mvg,<br>webmaster <a href=\"http://www.badmintonsport.be\">W&amp;L</a> bv"
                      , "MIME-Version: 1.0\r\nContent-type: text/html; charset=iso-8859-1\r\nFrom: webmaster@badmintonsport.be");
                }
              }
            }
            if (!empty($dp) && strtolower($dp) != 'x' && strtolower($dp) != strtolower($_SESSION['username']))
            {
              $lid = mysql_query("SELECT email FROM bad_spelers WHERE naam = '".$dp."'", $badm_db2);
              if (mysql_num_rows($lid) == 1)
              {
                $email = mysql_result($lid, 0, "email");
                if (!empty($email))
                {
                  mail( $email
                      , "Info: Inschrijving voor ".$tornooi->tornooi." verwijderd"
                      , "Beste ".ucwords(substr($dp, 0, strpos($dp, " "))).",<br><br>".$_SESSION['username']." heeft zojuist de inschrijving voor het tornooi van <a href=\"http://www.badmintonsport.be/tornooien/inschrijvingen.php?id=".$tornooi_id."\">".$tornooi->tornooi."</a> verwijderd. Jij was de dubbelpartner.<br><br>Mvg,<br>webmaster <a href=\"http://www.badmintonsport.be\">W&amp;L</a> bv"
                      , "MIME-Version: 1.0\r\nContent-type: text/html; charset=iso-8859-1\r\nFrom: webmaster@badmintonsport.be");
                }
              }
            }
            if (!empty($gp) && strtolower($gp) != 'x' && strtolower($gp) != strtolower($_SESSION['username']))
            {
              if (mysql_num_rows($lid) == 1)
              {
                $lid = mysql_query("SELECT email FROM bad_spelers WHERE naam = '".$gp."'", $badm_db2);
                $email = mysql_result($lid, 0, "email");
                if (!empty($email))
                {
                  mail( $email
                      , "Info: Inschrijving voor ".$tornooi->tornooi." verwijderd"
                      , "Beste ".ucwords(substr($gp, 0, strpos($gp, " "))).",<br><br>".$_SESSION['username']." heeft zojuist de inschrijving voor het tornooi van <a href=\"http://www.badmintonsport.be/tornooien/inschrijvingen.php?id=".$tornooi_id."\">".$tornooi->tornooi."</a> verwijderd. Jij was de gemengd dubbelpartner.<br><br>Mvg,<br>webmaster <a href=\"http://www.badmintonsport.be\">W&amp;L</a> bv"
                      , "MIME-Version: 1.0\r\nContent-type: text/html; charset=iso-8859-1\r\nFrom: webmaster@badmintonsport.be");
                }
              }
            }
			// If the subscription has already been send to the organisation, inform them
			if (!empty($dt_verzonden))
			{
			  mail( $tornooi->email
                  , "Info: Inschrijving van ".$p." voor ".$tornooi->tornooi." geannuleerd"
                  , "Beste,<br><br>".$_SESSION['username']." heeft zojuist de inschrijving van ".$p." voor jullie tornooi (<a href=\"http://www.badmintonsport.be/tornooien/inschrijvingen.php?id=".$tornooi_id."\">".$tornooi->tornooi."</a>) verwijderd.<br><br>Mvg,<br>webmaster <a href=\"http://www.badmintonsport.be\">W&amp;L</a> bv"
                  , "MIME-Version: 1.0\r\nContent-type: text/html; charset=iso-8859-1\r\nFrom: tornooien@badmintonsport.be\r\nBcc: tornooien@badmintonsport.be");
			}
            header("Location: http://www.badmintonsport.be/tornooien/inschrijvingen.php?id=".$tornooi_id);
            //mail("freek.ceymeulen@pandora.be", "Debug", $debug, "From: webmaster@badmintonsport.be");
            exit;
          }
          break;
        case "edit":
          // Check if user is allowed to edit this subscription
          $msg2 = check_policy($badm_db2, "wijzigen", $_SESSION['usid'], $_SESSION['username'], $inschr_spel_id);
          $debug .= "\n<br>msg2=".$msg2;
          if ($msg2 == "")
          {
            require_once "../admin/dml/inschr_spel.php";
            handle_submitted_data();
            $msg = check_data($badm_db2, $naam, $tornooi->doelgroep, $tornooi_id, $tornooi->tornooi, $inschr_spel_id, $tornooi->zelfde_disc_mag);
            $debug .= "\n<br>msg=".$msg;
            if ($msg == "")
            {
              if (isset($_REQUEST["direct"]))
              {
                $dt_verzonden = date("d/m/Y");
              }
              update_inschrijving($badm_db2, $inschr_spel_id, $naam, $lidnr, $klassement, $geb_dt, $discipline1, $discipline2, $discipline3, $reeksE, $enkel, $dubbel, $gemengd, $partner, $klasE, $clubE, $lidnrE, $dub_partner, $reeksD, $klasD, $clubD, $lidnrD, $mix_partner, $reeksG, $klasG, $clubG, $lidnrG, null, $tornooi_id, $dt_verzonden );
              if (isset($_REQUEST["direct"]))
              {
                $debug .= "<br>Direct\n";
                // Send this subscription immediately to the tournament organisation
                require_once("/home/badmin/public_html/functies/send_inschr_torn.php");
                $status = send_tournament($badm_db2, $tornooi_id, $tornooi->tornooi, $tornooi->dt_inschr, $tornooi->doelgroep, $tornooi->zelfde_disc_mag, $tornooi->naam, $tornooi->email, $tornooi->taal, "one", $inschr_spel_id);
                if ($status != true)
                {
                  mail("freek.ceymeulen@pandora.be", "Inschrijving niet verzonden", $debug, "From: webmaster@badmintonsport.be");
                }
              }
              sendMails($addressE, $messageE, $addressD, $messageD, $addressG, $messageG, $tornooi->tornooi, $email);
              header("Location: http://www.badmintonsport.be/tornooien/inschrijvingen.php?id=".$tornooi_id);
              //mail("freek.ceymeulen@pandora.be", "Debug", $debug, "From: webmaster@badmintonsport.be");
              exit;
            }
            else
            {
              handleCheckboxes($enkel, $dubbel, $gemengd);
            }
          }
          else
          {
            handleCheckboxes($_REQUEST["enkel"], $_REQUEST["dubbel"], $_REQUEST["gemengd"]);
          }
          break;
        case "add":
          require_once "../admin/dml/inschr_spel.php";
          handle_submitted_data();
          $msg = check_data($badm_db2, $naam, $tornooi->doelgroep, $tornooi_id, $tornooi->tornooi, null, $tornooi->zelfde_disc_mag);
          $debug .= "\n<br>msg=".$msg;
          if ($msg == "")
          {
            if (inschrijving_exists($badm_db2, $tornooi_id, $lidnr))
            {
              $msg = "Deze persoon is reeds ingeschreven.";
              $debug .= "\n<br>msg=".$msg;
              handleCheckboxes($enkel, $dubbel, $gemengd);
            }
            else
            {
              if (isset($_REQUEST["direct"]))
              {
                $dt_verzonden = date("d/m/Y");
              }
              //mail("freek.ceymeulen@pandora.be", "debug", "insert_inschrijving", "From: webmaster@badmintonsport.be");
              insert_inschrijving($badm_db2, $naam, $lidnr, $klassement, $geb_dt, $discipline1, $discipline2, $discipline3, $reeksE, $enkel, $dubbel, $gemengd, $partner, $klasE, $clubE, $lidnrE, $dub_partner, $reeksD, $klasD, $clubD, $lidnrD, $mix_partner, $reeksG, $klasG, $clubG, $lidnrG, $tornooi_id, $dt_verzonden );
              if (isset($_REQUEST["direct"]))
              {
                $debug .= "<br>Direct\n";
                // Send this subscription immediately to the tournament organisation
                require_once("/home/badmin/public_html/functies/send_inschr_torn.php");
                $status = send_tournament($badm_db2, $tornooi_id, $tornooi->tornooi, $tornooi->dt_inschr, $tornooi->doelgroep, $tornooi->zelfde_disc_mag, $tornooi->naam, $tornooi->email, $tornooi->taal, "one", $new_id);
                if ($status != true)
                {
                  mail("freek.ceymeulen@pandora.be", "Inschrijving niet verzonden!", $debug, "From: webmaster@badmintonsport.be");
                }
              }
              sendMails($addressE, $messageE, $addressD, $messageD, $addressG, $messageG, $tornooi->tornooi, $email);
              if (isset($_REQUEST["again"]))
              {
                $debug .= "<br>Again\n";
                // Make request variables empty
                emptyForm();
                $new_form = true;
              }
              else
              {
                header("Location: http://www.badmintonsport.be/tornooien/inschrijvingen.php?id=".$tornooi_id);
                //mail("freek.ceymeulen@pandora.be", "Debug", $debug, "From: webmaster@badmintonsport.be");
                exit;
              }
            }
          }
          else
          {
            handleCheckboxes($_REQUEST["enkel"], $_REQUEST["dubbel"], $_REQUEST["gemengd"]);
          }
          break;
        default:
          mail("freek.ceymeulen@pandora.be", "Possible break-in attempt", $_SESSION['username']." on page ".$_SERVER['PHP_SELF']." from IP ".$_SERVER['REMOTE_ADDR']." using method ".$_SERVER['REQUEST_METHOD']." on url ".$_SERVER['REQUEST_URI'].": actie \"".$actie."\" is not defined.", "From: webmaster@badmintonsport.be");
      } // end actie switch
    } // end $actie is set
  } // end check subscription period

  if ((isset($_REQUEST["inschr_spel_id"]) && !isset($_REQUEST["actie"])) || $actie == "delete")
  {
    // Get the data
    $query = "SELECT *, DATE_FORMAT(geb_dt, '%%d/%%m/%%Y') AS geboren FROM inschr_spel WHERE id = %d";
    $sql  = sprintf($query, mysql_real_escape_string($inschr_spel_id) );
    $debug .= "\n<br>".$sql;
    $result = mysql_query($sql, $badm_db2) or badm_mysql_die();
    $inschrijving = mysql_fetch_object($result);

    $naam        = $inschrijving->naam;
    $geb_dt      = $inschrijving->geboren;
    $klassement  = $inschrijving->klassement;
    $lidnr       = $inschrijving->lidnr;
    $discipline1 = $inschrijving->discipline1;
    if ($inschrijving->enkel == "1")
    {
      $enkel = " CHECKED";
    }
    else
    {
      $enkel = "";
      $disableE = " disabled";
    }
    $reeksE      = $inschrijving->reeksE;
    $partner     = $inschrijving->partner;
    $klasE       = $inschrijving->klasE;
    $clubE       = $inschrijving->clubE;
    $lidnrE      = $inschrijving->lidnrE;
    $discipline2 = $inschrijving->discipline2;
    if ($inschrijving->dubbel == "1")
    {
      $dubbel = " CHECKED";
    }
    else
    {
      $dubbel = "";
      $disableD = " disabled";
    }
    $reeksD      = $inschrijving->reeksD;
    $dub_partner = $inschrijving->dub_partner;
    $klasD       = $inschrijving->klasD;
    $clubD       = $inschrijving->clubD;
    $lidnrD      = $inschrijving->lidnrD;
    $discipline3 = $inschrijving->discipline3;
    if ($inschrijving->gemengd == "1")
    {
      $gemengd = " CHECKED";
    }
    else
    {
      $gemengd = "";
      $disableG = " disabled";
    }
    $reeksG      = $inschrijving->reeksG;
    $mix_partner = $inschrijving->mix_partner;
    $klasG       = $inschrijving->klasG;
    $clubG       = $inschrijving->clubG;
    $lidnrG      = $inschrijving->lidnrG;
  }
  elseif (!isset($_REQUEST["inschr_spel_id"]) && !isset($_REQUEST["actie"]))
  {
    $new_form = true;
    // Get the user data
    $query = "SELECT voornaam
                   , achternaam
                   , DATE_FORMAT(geb_dt, '%%d/%%m/%%Y') AS geb_dt
                   , FLOOR((TO_DAYS($tornooi->start_dt) - TO_DAYS(geb_dt)) / 365)  AS leeftijd
                   , DATE_FORMAT(geb_dt, '%%Y') AS geboortejaar
                   , klassement
                   , lidnr
                   , email
                FROM bad_spelers
               WHERE naam = '%s'";
    $sql  = sprintf($query, mysql_real_escape_string($_SESSION['username']) );
    $debug .= "\n<br>".$sql;
    $result = mysql_query($sql, $badm_db2) or badm_mysql_die();
    $speler = mysql_fetch_object($result);

    $voornaam    = $speler->voornaam;
    $achternaam  = $speler->achternaam;
    $geb_dt      = $speler->geb_dt;
    $leeftijd    = $speler->leeftijd;
    $klassement  = $speler->klassement;
    $lidnr       = $speler->lidnr;
    $email       = $speler->email;
    $klasE       = $klassement;
    $klasD       = $klassement;
    $klasG       = $klassement;
    $discipline1 = "enkel";
    $discipline2 = "dubbel";
    $discipline3 = "gemengd";
    if ($tornooi->doelgroep == 'J' || $tornooi->doelgroep == 'V')
    {
      if ($speler->geboortejaar >= $tornooi->jaar - 9)
      {
        $leeftijdscategorie = "J0";
      }
      if ($speler->geboortejaar >= $tornooi->jaar - 11)
      {
        $leeftijdscategorie = "J1";
      }
      elseif ($speler->geboortejaar >= $tornooi->jaar - 13)
      {
        $leeftijdscategorie = "J2";
      }
      elseif ($speler->geboortejaar >= $tornooi->jaar - 15)
      {
        $leeftijdscategorie = "J3";
      }
      elseif ($speler->geboortejaar >= $tornooi->jaar - 17)
      {
        $leeftijdscategorie = "J4";
      }
      elseif ($speler->geboortejaar >= $tornooi->jaar - 19)
      {
        $leeftijdscategorie = "J5";
      }
      elseif ($leeftijd >= 35)
      {
        if ($klassement == "A" || $klassement == "B1")
        {
          $leeftijdscategorie = "V1";
        }
        elseif ($klassement == "B2" || $klassement == "C1")
        {
          $leeftijdscategorie = "V2";
        }
        if ($klassement == "C2" || $klassement == "D")
        {
          $leeftijdscategorie = "V3";
        }
      }
      $reeksE = $leeftijdscategorie;
      $reeksD = $leeftijdscategorie;
      $reeksG = $leeftijdscategorie;
    }
    else
    {
      $reeksE = $klassement;
      $reeksD = $klassement;
      $reeksG = $klassement;
    }
    $disableE = " disabled";
    $disableD = " disabled";
    $disableG = " disabled";
    // Ga na of er zich al iemand heeft ingeschreven met deze persoon als dubbelpartner
    $query = "SELECT naam, klassement, reeksD, lidnr FROM inschr_spel WHERE inschr_torn_id = %d AND dub_partner = '%s'";
    $sql  = sprintf($query, mysql_real_escape_string($tornooi_id)
                          , mysql_real_escape_string($_SESSION['username']) );
    $result = mysql_query($sql, $badm_db2) or badm_mysql_die();
    if (mysql_num_rows($result) != 0)
    {
      $dub_partner = mysql_result($result, 0, "naam");
      $klasD = mysql_result($result, 0, "klassement");
      $lidnrD = mysql_result($result, 0, "lidnr");
      $reeksD = mysql_result($result, 0, "reeksD");
      $clubD = "W&L bv";
    }
    // Ga na of er zich al iemand heeft ingeschreven met deze persoon als mixed partner
    $query = "SELECT naam, klassement, reeksG, lidnr FROM inschr_spel WHERE inschr_torn_id = %d AND mix_partner = '%s'";
    $sql  = sprintf($query, mysql_real_escape_string($tornooi_id)
                          , mysql_real_escape_string($_SESSION['username']) );
    $result = mysql_query($sql, $badm_db2) or badm_mysql_die();
    if (mysql_num_rows($result) != 0)
    {
      $mix_partner = mysql_result($result, 0, "naam");
      $klasG = mysql_result($result, 0, "klassement");
      $lidnrG = mysql_result($result, 0, "lidnr");
      $reeksG = mysql_result($result, 0, "reeksG");
      $clubG = "W&L bv";
    }
  }
  // if discipline = enkel then don't display partner fields
  if ($discipline1 == "enkel")
  {
    $displayE = " style=\"display:none\"";
  }
  if ($discipline2 == "enkel")
  {
    $displayD = " style=\"display:none\"";
  }
  if ($discipline3 == "enkel")
  {
    $displayG = " style=\"display:none\"";
  }
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html lang="nl"><!-- InstanceBegin template="/Templates/templatesub1php.dwt.php" codeOutsideHTMLIsLocked="false" -->
<?php
    // Connect to database
    require_once "/home/badmin/public_html/functies/badm_db.inc.php";
    $badm_db = badm_conn_db();
?>
<head>
<META HTTP-EQUIV="Content-Type"      CONTENT="text/html; charset=iso-8859-1">
<META NAME="robots"                  CONTENT="index,follow">
<META NAME="keywords"                CONTENT="badminton,batminton,w&amp;l,badmintonvereniging,club,badmintonclub,sport,Belgium,Veltem,Herent,Leuven,Winksele,Belgi�">
<META NAME="description"             CONTENT="W&amp;L bv : De grootste en leukste badmintonclub van Brabant">
<META NAME="abstract"                CONTENT="badminton, club">
<META NAME="Area"                    CONTENT="General">
<META NAME="Author"                  CONTENT="W&amp;L bv">
<META NAME="Copyright"               CONTENT="(c) 2003-2004 W&amp;L bv">
<META NAME="Creator"                 CONTENT="W&amp;L bv">
<META NAME="Generator"               CONTENT="W&amp;L bv">
<META NAME="Owner"                   CONTENT="W&ampL bv">
<META NAME="Publisher"               CONTENT="W&ampL bv">
<META NAME="Rating"                  CONTENT="General">
<META NAME="revisit-after"           CONTENT="7 Days">
<META NAME="revisit"                 CONTENT="7 Days">
<META NAME="document-classification" CONTENT="Internet">
<META NAME="document-type"           CONTENT="Public">
<META NAME="document-rating"         CONTENT="Safe for Kids">
<META NAME="document-distribution"   CONTENT="Global">
<META HTTP-EQUIV="Content-Type"      CONTENT="text/html; charset=iso-8859-1">
<META HTTP-EQUIV="Audience"          CONTENT="General">
<META HTTP-EQUIV="content-language"  CONTENT="NL">
<META HTTP-EQUIV="Pragma"            CONTENT="no-cache">
<META HTTP-EQUIV="Expires"           CONTENT="now">
<!-- InstanceBeginEditable name="doctitle" -->
<title>W&amp;L badmintonvereniging vzw - Inschrijving</title>
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="head" -->
<script type="text/javascript" language="JavaScript" src="http://www.badmintonsport.be/scripts/createXmlHttpRequest.js"></script>
<script type="text/javascript" language="JavaScript">
<!--
function popupFieldHelp(pURL,pname,pWidth,pHeight,pScroll,pResizable)
{
  if(!pURL){pURL = 'about:blank'}
  if(!pname){pname = 'Popup'}
  if(!pWidth){pWidth = 600}
  if(!pHeight){pHeight = 600}
  if(!pScroll){pScroll = 'yes'}
  if(!pResizable){pResizable = 'yes'}
  l_Window = window.open(pURL, pname, 'toolbar=no,scrollbars='+pScroll+',location=no,statusbar=no,menubar=no,resizable='+pResizable+',width='+pWidth+',height='+pHeight);
  if (l_Window.opener == null){l_Window.opener = self;}
  l_Window.focus();
  //  return l_Window;
  return;
}
function showHelp (pId)
{
  popupFieldHelp('http://www.badmintonsport.be/tornooien/help.php?id='+pId, null, 500, 350);
}

function autocompleteReeks()
{
  lv_klassement = document.getElementById('klassement');
  lv_klas = lv_klassement.options[lv_klassement.selectedIndex].value;
  var lv_klassementE = document.getElementById('klasE');
  if (lv_klassementE)
  {
    for (var idx=0;idx < lv_klassementE.options.length;idx++)
    {
      if (lv_klas == lv_klassementE.options[idx].value)
      {
        lv_klassementE.selectedIndex = idx;
      }
    }
  }
  var lv_klassementD = document.getElementById('klasD');
  if (lv_klassementD)
  {
    for (var idx=0;idx < lv_klassementD.options.length;idx++)
    {
      if (lv_klas == lv_klassementD.options[idx].value)
      {
        lv_klassementD.selectedIndex = idx;
      }
    }
  }
  var lv_klassementG = document.getElementById('klasG');
  if (lv_klassementG)
  {
    for (var idx=0;idx < lv_klassementG.options.length;idx++)
    {
      if (lv_klas == lv_klassementG.options[idx].value)
      {
        lv_klassementG.selectedIndex = idx;
      }
    }
  }
  lv_leeftijdscategorie = document.getElementById("leeftijdscategorie");
  if (lv_leeftijdscategorie)
  {
    lv_klas = lv_leeftijdscategorie.options[lv_leeftijdscategorie.selectedIndex].value;
  }
  var lv_reeksE = document.getElementById('reeksE');
  if (lv_reeksE)
  {
    for (var idx=0;idx < lv_reeksE.options.length;idx++)
    {
      if (lv_klas == lv_reeksE.options[idx].value)
      {
        lv_reeksE.selectedIndex = idx;
      }
    }
  }
  var lv_reeksD = document.getElementById('reeksD');
  if (lv_reeksD)
  {
    for (var idx=0;idx < lv_reeksD.options.length;idx++)
    {
      if (lv_klas == lv_reeksD.options[idx].value)
      {
        lv_reeksD.selectedIndex = idx;
      }
    }
  }
  var lv_reeksG = document.getElementById('reeksG');
  if (lv_reeksG)
  {
    for (var idx=0;idx < lv_reeksG.options.length;idx++)
    {
      if (lv_klas == lv_reeksG.options[idx].value)
      {
        lv_reeksG.selectedIndex = idx;
      }
    }
  }
}

function defineAgeCategory($age, $yearOfBirth, $klassement)
{
  lv_leeftijdscategorie = document.getElementById("leeftijdscategorie");
  if (lv_leeftijdscategorie)
  {
    //var currentTime = new Date();
    //var year = currentTime.getFullYear();
    var year = document.getElementById("jaar").value; // Year of tournament
    if ($yearOfBirth >= (year - 9))
    {
      ageCategory = "J0";
    }
    if ($yearOfBirth >= (year - 11))
    {
      ageCategory = "J1";
    }
    else if ($yearOfBirth >= (year - 13))
    {
      ageCategory = "J2";
    }
    else if ($yearOfBirth >= (year - 15))
    {
      ageCategory = "J3";
    }
    else if ($yearOfBirth >= (year - 17))
    {
      ageCategory = "J4";
    }
    else if ($yearOfBirth >= (year - 19))
    {
      ageCategory = "J5";
    }
    else if ($age > 34)
    {
      if ($klassement == "A" || $klassement == "B1")
      {
        ageCategory = "V1";
      }
      else if ($klassement == "B2" || $klassement == "C1")
      {
        ageCategory = "V2";
      }
      else if ($klassement == "C2" || $klassement == "D")
      {
        ageCategory = "V3";
      }
    }
    if (ageCategory)
    {
      for (var idx=0;idx < lv_leeftijdscategorie.options.length;idx++)
      {
        if (ageCategory == lv_leeftijdscategorie.options[idx].value)
        {
          lv_leeftijdscategorie.selectedIndex = idx;
        }
      }
    }
  }
}
var fireOnBlur = true;
function processData($action)
{
  if ($action == "voornaam")
  {
    var response = requestObject.responseText;
    var update = new Array();
    if (response.indexOf('|') != -1)
    {
        update = response.split('|');
        fireOnBlur = false;
        document.getElementById("achternaam").value= update[0];
        document.getElementById("geboortedatum").value= update[1];
        var lv_klassement = document.getElementById("klassement");
        for (var idx=0;idx < lv_klassement.options.length;idx++)
        {
          if (update[2] == lv_klassement.options[idx].value)
          {
            lv_klassement.selectedIndex = idx;
          }
        }
        defineAgeCategory(update[5], update[1].substring(6), update[2]);
        autocompleteReeks();
        document.getElementById("lidnr").value= update[3];
        document.getElementById("email").value= update[6];
        fireOnBlur = true;
    }
  }
  else if ($action == "naam")
  {
    var response = requestObject.responseText;
    var update = new Array();
    var lv_message = document.getElementById('message');
    if (response.indexOf('|') != -1)
    {
        update = response.split('|');
        document.getElementById("geboortedatum").value= update[0];
        var lv_klassement = document.getElementById("klassement");
        for (var idx=0;idx < lv_klassement.options.length;idx++)
        {
          if (update[1] == lv_klassement.options[idx].value)
          {
            lv_klassement.selectedIndex = idx;
          }
        }
        defineAgeCategory(update[5], update[0].substring(6), update[1]);
        autocompleteReeks();
        document.getElementById("lidnr").value= update[2];
        document.getElementById("email").value= update[6];
        lv_message.style.display = 'none';
    }
    else
    {
      lv_message.innerHTML = 'Deze speler werd niet herkend als zijnde lid van W&L bv, check spelling/voornaam eerst!';
      lv_message.style.display = 'block';
    }
  }
  else if ($action == "dub_partner")
  {
    var response = requestObject.responseText;
    var update = new Array();
    var lv_dub_partner = document.getElementById("dub_partner");
    if (lv_dub_partner)
    {
      if (lv_dub_partner != '' && lv_dub_partner.value.toLowerCase() != 'x')
      {
        if (response.indexOf('|') != -1)
        {
            update = response.split('|');
            var lv_klassement = document.getElementById("klasD");
            for (var idx=0;idx < lv_klassement.options.length;idx++)
            {
              if (update[1] == lv_klassement.options[idx].value)
              {
                lv_klassement.selectedIndex = idx;
              }
            }
            document.getElementById("lidnrD").value= update[2];
            document.getElementById("clubD").value= update[3];
            document.getElementById("emailD").value= update[6];
        }
        else
        {
          lv_message = document.getElementById('message');
          lv_message.innerHTML = 'Je dubbelpartner is niet van W&L bv (check spelling/voornaam eerst!). Gelieve zijn/haar club en lidnummer op te geven.';
          lv_message.style.display = 'block';
          document.getElementById("lidnrD").value= "?";
          document.getElementById("clubD").value= "?";
          document.getElementById("emailD").value= "";
        }
      }
    }
  }
  else if ($action == "mix_partner")
  {
    var response = requestObject.responseText;
    var update = new Array();
    var lv_mix_partner = document.getElementById("mix_partner");
    if (lv_mix_partner)
    {
      if (lv_mix_partner != '' && lv_mix_partner.value.toLowerCase() != 'x')
      {
        if (response.indexOf('|') != -1)
        {
            update = response.split('|');
            var lv_klassement = document.getElementById("klasG");
            for (var idx=0;idx < lv_klassement.options.length;idx++)
            {
              if (update[1] == lv_klassement.options[idx].value)
              {
                lv_klassement.selectedIndex = idx;
              }
            }
            document.getElementById("lidnrG").value= update[2];
            document.getElementById("clubG").value= update[3];
            document.getElementById("emailG").value= update[6];
        }
        else
        {
          lv_message = document.getElementById('message');
          lv_message.innerHTML = 'Je mixed partner is niet van W&L bv (check spelling/voornaam eerst!). Gelieve zijn/haar club en lidnummer op te geven.';
          lv_message.style.display = 'block';
          document.getElementById("lidnrG").value= "?";
          document.getElementById("clubG").value= "?";
          document.getElementById("emailD").value= "";
        }
      }
    }
  }
  else if ($action == "partner")
  {
    var response = requestObject.responseText;
    var update = new Array();
    var lv_partner = document.getElementById("partner");
    if (lv_partner)
    {
      if (lv_partner != '' && lv_partner.value.toLowerCase() != 'x')
      {
        if (response.indexOf('|') != -1)
        {
            update = response.split('|');
            var lv_klassement = document.getElementById("klasE");
            for (var idx=0;idx < lv_klassement.options.length;idx++)
            {
              if (update[1] == lv_klassement.options[idx].value)
              {
                lv_klassement.selectedIndex = idx;
              }
            }
            document.getElementById("lidnrE").value= update[2];
            document.getElementById("clubE").value= update[3];
            document.getElementById("emailE").value= update[6];
        }
        else
        {
          lv_message = document.getElementById('message');
          lv_message.innerHTML = 'Je partner voor discipline 1 is niet van W&L bv (check spelling/voornaam eerst!). Gelieve zijn/haar club en lidnummer op te geven.';
          lv_message.style.display = 'block';
          document.getElementById("lidnrE").value= "?";
          document.getElementById("clubE").value= "?";
          document.getElementById("emailD").value= "";
        }
      }
    }
  }
}
function doSubmit()
{
  var lv_error_msg = 'Fout:';
  // Check input
  var lv_voornaam = document.getElementById('voornaam');
  if (lv_voornaam)
  {
    if (lv_voornaam.value.length == 0)
    {
      lv_error_msg = lv_error_msg + '<br>Je hebt geen voornaam ingevuld.';
    }
  }
  var lv_achternaam = document.getElementById('achternaam');
  if (lv_achternaam)
  {
    if (lv_achternaam.value.length == 0)
    {
      lv_error_msg = lv_error_msg + '<br>Je hebt geen achternaam ingevuld.';
    }
  }
  var lv_naam = document.getElementById('naam');
  if (lv_naam)
  {
    if (lv_naam.value.length == 0)
    {
      lv_error_msg = lv_error_msg + '<br>Je hebt geen naam ingevuld.';
    }
  }
  var lv_klassement = document.getElementById('klassement');
  if (lv_klassement.options[lv_klassement.selectedIndex].value.length == 0)
  {
    lv_error_msg = lv_error_msg + '<br>Je hebt geen klassement ingevuld.';
  }
  if (document.getElementById('lidnr').value.length == 0)
  {
    lv_error_msg = lv_error_msg + '<br>Je hebt geen VBL lidnummer ingevuld.';
  }
  var lv_geboortedatum = document.getElementById('geb_dt');
  if (lv_geboortedatum)
  {
    if (lv_geboortedatum.value.length == 0)
    {
      lv_error_msg = lv_error_msg + '<br>Je hebt geen geboortedatum ingevuld.';
    }
  }
  var lv_leeftijdscategorie = document.getElementById('leeftijdscategorie');
  if (lv_leeftijdscategorie)
  {
    if (lv_leeftijdscategorie.value.length == 0)
    {
      lv_error_msg = lv_error_msg + '<br>Je hebt geen leeftijdscategorie ingevuld.';
    }
  }
  var lv_enkel = document.getElementById('enkel');
  if (lv_enkel)
  {
    lv_enkel = lv_enkel.checked;
  }
  var lv_dubbel = document.getElementById('dubbel');
  if (lv_dubbel)
  {
    lv_dubbel = lv_dubbel.checked;
  }
  var lv_gemengd = document.getElementById('gemengd');
  if (lv_gemengd)
  {
    lv_gemengd = lv_gemengd.checked;
  }
  if (!(lv_enkel || lv_dubbel || lv_gemengd))
  {
    lv_error_msg = lv_error_msg + '<br>Je moet minstens 1 discipline aanvinken.';
  }
  if (lv_enkel && document.getElementById('discipline1').value != 'enkel')
  {
    if (document.getElementById('partner').value.length == 0)
    {
      lv_error_msg = lv_error_msg + '<br>Je hebt geen partner ingevuld bij discipline 1. Heb je niemand en wil je toch dubbelen zet dan een kruisje.';
    }
    if (document.getElementById('partner').value.toLowerCase() != 'x')
    {
      if (document.getElementById('clubE').value.length == 0)
      {
        lv_error_msg = lv_error_msg + '<br>Je hebt de club van je partner niet ingevuld bij discipline 1. Ken je zijn/haar club niet zet dan een vraagteken.';
      }
      if (document.getElementById('lidnrE').value.length == 0)
      {
        lv_error_msg = lv_error_msg + '<br>Je hebt het VBL lidnummer van je partner niet ingevuld bij discipline 1. Ken je zijn/haar lidnummer niet zet dan een vraagteken.';
      }
    }
  }
  if (lv_dubbel && document.getElementById('discipline2').value != 'enkel')
  {
    if (document.getElementById('dub_partner').value.length == 0)
    {
      lv_error_msg = lv_error_msg + '<br>Je hebt geen dubbelpartner ingevuld. Heb je niemand en wil je toch dubbelen zet dan een kruisje.';
    }
    if (document.getElementById('dub_partner').value.toLowerCase() != 'x')
    {
      if (document.getElementById('clubD').value.length == 0)
      {
        lv_error_msg = lv_error_msg + '<br>Je hebt de club van je dubbelpartner niet ingevuld. Ken je zijn/haar club niet zet dan een vraagteken.';
      }
      if (document.getElementById('lidnrD').value.length == 0)
      {
        lv_error_msg = lv_error_msg + '<br>Je hebt het VBL lidnummer van je dubbelpartner niet ingevuld. Ken je zijn/haar lidnummer niet zet dan een vraagteken.';
      }
    }
  }
  if (lv_gemengd && document.getElementById('discipline3').value != 'enkel')
  {
    if (document.getElementById('mix_partner').value.length == 0)
    {
      lv_error_msg = lv_error_msg + '<br>Je hebt geen mixed partner ingevuld. Heb je niemand en wil je toch mixed spelen zet dan een kruisje.';
    }
    if (document.getElementById('mix_partner').value.toLowerCase() != 'x')
    {
      if (document.getElementById('clubG').value.length == 0)
      {
        lv_error_msg = lv_error_msg + '<br>Je hebt de club van je mixed partner niet ingevuld. Ken je zijn/haar club niet zet dan een vraagteken.';
      }
      if (document.getElementById('lidnrG').value.length == 0)
      {
        lv_error_msg = lv_error_msg + '<br>Je hebt het VBL lidnummer van je mixed partner niet ingevuld. Ken je zijn/haar lidnummer niet zet dan een vraagteken.';
      }
    }
  }
  if (lv_error_msg == 'Fout:')
  {
    // Submit data
    document.inschrijven.submit();
  }
  else
  {
    //alert(lv_error_msg);
    lv_message = document.getElementById('message');
    lv_message.innerHTML = lv_error_msg;
    lv_message.style.display = 'block';
  }
}

function toggleDiscipline1()
{
  var lv_discipline = document.getElementById('discipline1');
  if (lv_discipline.options[lv_discipline.selectedIndex].value == 'enkel')
  {
    document.forms[0].partner.style.display = 'none';
    document.forms[0].klasE.style.display = 'none';
    document.forms[0].clubE.style.display = 'none';
    document.forms[0].lidnrE.style.display = 'none';
  }
  else
  {
    document.forms[0].partner.style.display = 'inline';
    document.forms[0].klasE.style.display = 'inline';
    document.forms[0].clubE.style.display = 'inline';
    document.forms[0].lidnrE.style.display = 'inline';
  }
}

function toggleDiscipline2()
{
  var lv_discipline = document.getElementById('discipline2');
  if (lv_discipline.options[lv_discipline.selectedIndex].value == 'enkel')
  {
    document.forms[0].dub_partner.style.display = 'none';
    document.forms[0].klasD.style.display = 'none';
    document.forms[0].clubD.style.display = 'none';
    document.forms[0].lidnrD.style.display = 'none';
  }
  else
  {
    document.forms[0].dub_partner.style.display = 'inline';
    document.forms[0].klasD.style.display = 'inline';
    document.forms[0].clubD.style.display = 'inline';
    document.forms[0].lidnrD.style.display = 'inline';
  }
}

function toggleDiscipline3()
{
  var lv_discipline = document.getElementById('discipline3');
  if (lv_discipline.options[lv_discipline.selectedIndex].value == 'enkel')
  {
    document.forms[0].mix_partner.style.display = 'none';
    document.forms[0].klasG.style.display = 'none';
    document.forms[0].clubG.style.display = 'none';
    document.forms[0].lidnrG.style.display = 'none';
  }
  else
  {
    document.forms[0].mix_partner.style.display = 'inline';
    document.forms[0].klasG.style.display = 'inline';
    document.forms[0].clubG.style.display = 'inline';
    document.forms[0].lidnrG.style.display = 'inline';
  }
}

function toggleEnkel()
{
  if (document.forms[0].enkel.checked)
  {
    document.forms[0].reeksE.disabled = false;
    document.forms[0].partner.disabled = false;
    document.forms[0].klasE.disabled = false;
    document.forms[0].clubE.disabled = false;
    document.forms[0].lidnrE.disabled = false;
  }
  else
  {
    document.forms[0].reeksE.disabled = true;
    document.forms[0].partner.disabled = true;
    document.forms[0].klasE.disabled = true;
    document.forms[0].clubE.disabled = true;
    document.forms[0].lidnrE.disabled = true;
  }
}

function toggleDubbel()
{
  if (document.forms[0].dubbel.checked)
  {
    document.forms[0].reeksD.disabled = false;
    document.forms[0].dub_partner.disabled = false;
    document.forms[0].klasD.disabled = false;
    document.forms[0].clubD.disabled = false;
    document.forms[0].lidnrD.disabled = false;
  }
  else
  {
    document.forms[0].reeksD.disabled = true;
    document.forms[0].dub_partner.disabled = true;
    document.forms[0].klasD.disabled = true;
    document.forms[0].clubD.disabled = true;
    document.forms[0].lidnrD.disabled = true;
  }
}

function toggleGemengd()
{
  if (document.forms[0].gemengd.checked)
  {
    document.forms[0].reeksG.disabled = false;
    document.forms[0].mix_partner.disabled = false;
    document.forms[0].klasG.disabled = false;
    document.forms[0].clubG.disabled = false;
    document.forms[0].lidnrG.disabled = false;
  }
  else
  {
    document.forms[0].reeksG.disabled = true;
    document.forms[0].mix_partner.disabled = true;
    document.forms[0].klasG.disabled = true;
    document.forms[0].clubG.disabled = true;
    document.forms[0].lidnrG.disabled = true;
  }
}
//-->
</script>
<style type="text/css">
<!--
.help {
    cursor : help;
    text-decoration : none;
    white-space : nowrap;
}
a.help {
    color : black;
    text-decoration : none;
}
#login {
    float: right;
}
#personaliaFrame {
    border : 1px solid #006600;
}
#personalia {
    background-color : #EFEFEF;
}
#inschrijvingFrame {
    border : 1px solid #006600;
}
#inschrijving {
    background-color : #EFEFEF;
}
#message {
    background-color : lightyellow;
    border : 1px solid black;
    color : red;
    /*display : none;*/
    margin-bottom : 5px;
    margin-top : 5px;
    padding : 5px;
}
#control {
      background-color : #cccccc;
      border           : 1px #999999 solid;
      height           : 24px;
      margin-bottom    : 10px;
      margin-top       : 10px;
      padding          : 3px;
      width            : 99%;
}
#noprivileges {
    color : red;
    font-weight : bold;
}
table.xpbutton td.R {
    width : 4px;
}
table.xpbutton td.L {
    width : 4px;
}
table.xpbutton td.R img {
    display : block;
}
table.xpbutton td.L img {
    display : block;
}
table.xpbutton {
    color   : #333333;
    display : inline;
}
table.xpbutton td.C {
    background-image  : url(../images/button_xp_center.gif);
    background-repeat : repeat-x;
    white-space       : nowrap;
}
table.xpbutton td.C a {
    display         : block;
    font-family     : Verdana, Geneva, Arial, Helvetica, sans-serif;
    font-size       : 12px;
    font-weight     : bold;
    padding-left    : 3px;
    padding-right   : 3px;
    text-decoration : none;
    white-space     : nowrap;
}
table.xpbutton td.C a:visited {
    display         : block;
    font-family     : Verdana, Geneva, Arial, Helvetica, sans-serif;
    font-size       : 12px;
    font-weight     : bold;
    padding-left    : 3px;
    padding-right   : 3px;
    text-decoration : none;
    white-space     : nowrap;
}
-->
</style>

<!-- InstanceEndEditable -->
<link href="http://www.badmintonsport.be/css/badminton.css" rel="stylesheet" type="text/css">
<script type="text/javascript" language="JavaScript" src="http://www.badmintonsport.be/scripts/stm31.js"></script>
<script type="text/javascript" language="JavaScript" src="http://www.badmintonsport.be/scripts/dw_rotator.js"></script>
<script type="text/javascript" language="JavaScript">
<!--
function initRotator()
{
  var rotator1 = new dw_Rotator("sponsors_banner", 4000, "http://www.badmintonsport.be/images/", "_blank");
  rotator1.addImages("Veltem_Motors_banner.png", "china.jpg");
  rotator1.addActions("http://www.veltem-motors.be", "http://www.chinacapital.be");
  dw_Rotator.start();
}
function doLoad()
{
  // initialize the menu
  st_onload();
  // initialize the banner rotator
  initRotator();
}
//-->
</script>
</head>
<body class="badminton" onload="doLoad();">
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <!--DWLayoutTable-->
  <tr>
  <td colspan="2">
   <table width="100%" border="0" cellpadding="0" cellspacing="0">
    <!--DWLayoutTable-->
    <tr> 
     <td width="100%" style="background-color: #006600;"><img src="http://www.badmintonsport.be/images/pt_transp.gif" width="1px" height="4px" alt=""></td>
    </tr>
    <tr> 
     <td align="center" valign="top" bgcolor="#CCCCCC">
      <table width="100%" border="0" cellspacing="0" cellpadding="3">
       <tr align="center" valign="middle"> 
        <td rowspan="2" align="left">
	     <img src="http://www.badmintonsport.be/images/logow&amp;l-100.gif" width="100" height="80" alt="logo">
	    </td>
        <td>
	     <h4>W&amp;L Badmintonvereniging vzw<br>
	         Mechelsesteenweg 387<br>
             3020 Herent</h4>
		</td>
        <td align="right"><a href="javascript:void();" onclick="return dw_Rotator.doClick(0)"  onmouseover="dw_Rotator.pause(0)" onmouseout="dw_Rotator.resume(0)" onfocus="this.blur()"><img id="sponsors_banner" name="sponsors_banner" src="http://www.badmintonsport.be/images/Veltem_Motors_banner.png" alt="sponsors" width="320" height="100" border="0"></a></td>
       </tr>
      </table>
	 </td>
    </tr>
    <tr> 
     <td bgcolor="#336600"><img src="http://www.badmintonsport.be/images/pt_transp.gif" width="1px" height="4px" alt=""></td>
    </tr>
   </table>
  </td>
 </tr>
 <tr>
  <td width="150" height="213" valign="top">
<script type="text/javascript" language="JavaScript1.2">
<!--
stm_bm(["W&L menu",400,"","http://www.badmintonsport.be/scripts/blank.gif",0,"","",0,0,250,0,1000,1,0,0]);
stm_bp("p0",[1,4,0,0,1,4,0,7,100,"progid:DXImageTransform.Microsoft.Fade(overlap=.5,enabled=0,Duration=0.30)",-2,"progid:DXImageTransform.Microsoft.Pixelate(MaxSquare=15,enabled=0,Duration=0.50)",-2,50,0,0,"#669999","#006633","",3,0,0,"#ffffff"]);
stm_ai("p0i0",[0,"Home","","",-1,-1,0,"http://www.badmintonsport.be/index.php","_self","Startpagina","","","",0,0,0,"","",0,0,0,0,1,"#fffff7",0,"#b5bed6",0,"","",3,3,0,0,"#fffff7","#000000","#000000","#000000","10pt Verdana","10pt Verdana",0,0]);
stm_aix("p0i1","p0i0",[0,"Algemeen","","",-1,-1,0,"","_self","informatie over de club","","","",0,0,0,"http://www.badmintonsport.be/scripts/arrow_r.gif","http://www.badmintonsport.be/scripts/arrow_r.gif",7,7]);
stm_bpx("p1","p0",[1,2,0,0,1,4,0]);
stm_aix("p1i0","p0i0",[0,"Bestuur","","",-1,-1,0,"http://www.badmintonsport.be/algemeen/bestuur.php","_self","Bestuursleden","","","",0,0,0,"","",0,0,0,1]);
stm_bpx("p1","p0",[1,2,0,0,1,4,0,0]);
stm_aix("p2i0","p1i0",[0,"Mededelingen","","",-1,-1,0,"http://www.badmintonsport.be/algemeen/Bestuur/bestuursmededelingen.php","_self","Bestuursmededelingen"]);
stm_ep();
stm_aix("p1i1","p0i0",[0,"Medewerkers","","",-1,-1,0,"http://www.badmintonsport.be/algemeen/medewerkers.php","_self","Medewerkers","","","",0,0,0,"","",0,0,0,1]);
stm_aix("p1i2","p0i0",[0,"Speeluren","","",-1,-1,0,"http://www.badmintonsport.be/algemeen/speeluren.php","_self","Speeluren","","","",0,0,0,"","",0,0,0,1]);
stm_aix("p1i3","p0i1",[0,"Geschiedenis","","",-1,-1,0,"","_self","Geschiedenis","","","",0,0,0,"","",0,0,0,1]);
stm_bpx("p2","p0",[1,2,0,0,1,4,0,0]);
stm_aix("p2i0","p1i0",[0,"Clubkampioenen","","",-1,-1,0,"http://www.badmintonsport.be/algemeen/Geschiedenis/clubkampioenen.php","_self","Winnaars Clubkampioenschap"]);
stm_aix("p2i1","p1i0",[0,"Nationale Kampioenen","","",-1,-1,0,"http://www.badmintonsport.be/algemeen/Geschiedenis/nationalekampioenen.php","_self","Nationale Kampioenen W&L"]);
stm_ep();
stm_aix("p1i4","p0i0",[0,"Sponsors","","",-1,-1,0,"http://www.badmintonsport.be/algemeen/sponsors.php","_self","Sponsors","","","",0,0,0,"","",0,0,0,1]);
stm_aix("p1i5","p0i0",[0,"Artikels","","",-1,-1,0,"http://www.badmintonsport.be/algemeen/artikels.php","_self","Artikels over de club","","","",0,0,0,"","",0,0,0,1]);
stm_ep();
stm_aix("p0i2","p0i0",[0,"Competitie","","",-1,-1,0,"http://www.badmintonsport.be/competitie.php","_self","Competitie: ploegen en uitslagen"]);
stm_aix("p0i3","p0i1",[0,"Tornooien","","",-1,-1,0,"http://www.badmintonsport.be/tornooien/tornooioverzicht.php","_self","Tornooien"]);
stm_bpx("p1","p0",[1,2,0,0,1,4,0]);
stm_aix("p1i0","p0i1",[0,"Inschrijvingen","","",-1,-1,0,"_self","Schrijf je hier in voor tornooien"]);
stm_bpx("p1","p0",[1,2,0,0,1,4,0,0]);
//-->
</script>
<?php
// create SQL statement 
   $sql = "SELECT id
                , organisatie
                , doelgroep
                , DATE_FORMAT(start_dt, '%d/%m') AS start_dt
                , verzonden
           FROM inschr_torn
		   WHERE DATE_ADD(dt_inschr, INTERVAL 21 DAY ) > NOW()
		   ORDER BY dt_inschr DESC";
   $result = mysql_query ($sql, $badm_db) or badm_mysql_die();
// format results by row
   $teller = 0;
   echo "<script type=\"text/javascript\" language=\"JavaScript1.2\">\n";
   while ($badm = mysql_fetch_object($result))
   {
      echo "stm_aix(\"p2i";
      echo $teller;
      echo "\",\"p1i0\",[1,\"";
      echo $badm->organisatie;
      if ($badm->doelgroep == 'J' || $badm->doelgroep == 'V')
      {
        echo $badm->doelgroep == 'J' ? " (Jeugd)" : " (Veteranen)";
        if ($badm->verzonden == 0) //inschrijvingsperiode nog niet afgelopen
        {
          echo " <B>".$badm->start_dt."</B>";
        }
        else
        {
          echo "  <IMG src=http://www.badmintonsport.be/images/closed.png>";
        }
      }
      else
      {
        if ($badm->verzonden == 0) //inschrijvingsperiode nog niet afgelopen
        {
          echo " <B>".$badm->start_dt."</B>";
        }
        else
        {
          echo "  <IMG src=http://www.badmintonsport.be/images/closed.png>";
        }
      }
      echo "\",\"\",\"\",-1,-1,0,\"http://www.badmintonsport.be/tornooien/inschrijvingen.php?id=";
      echo $badm->id;
      echo "\",\"_self\",\"Inschrijving ";
      echo $badm->organisatie;
      echo "\"]);\n";
      $teller = $teller + 1;
   }
   echo"</script>\n";
?>
<script type="text/javascript" language="JavaScript1.2">
<!--
stm_ep();
stm_aix("p1i1","p0i0",[0,"Aanvangsuren","","",-1,-1,0,"http://www.badmintonsport.be/tornooien/aanvangsuren.php","_self","Aanvangsuren tornooien"]);
stm_aix("p1i2","p0i0",[0,"Kalender","","",-1,-1,0,"http://www.badmintonsport.be/docs/tornooikalender.pdf","_blank","Tornooikalender"]);
stm_aix("p1i3","p0i1",[0,"Resultaten","","",-1,-1,0,"","_self","Tornooiresultaten W&L spelers"]);
stm_bpx("p2","p0",[1,2,0,0,1,4,0,0]);
//-->
</script>
<?php
// create SQL statement
    $sql = "SELECT  DISTINCT IF(DATE_FORMAT(dt_van, '%m') < 8
, DATE_FORMAT(dt_van, '%Y') - 1
, DATE_FORMAT(dt_van, '%Y')) AS start
, IF(DATE_FORMAT(dt_van, '%m') < 8
, DATE_FORMAT(dt_van, '%Y')
, DATE_FORMAT(dt_van, '%Y') + 1) AS einde
           FROM bad_tornooien
           ORDER  BY start DESC ";
   $result = mysql_query ($sql, $badm_db) or badm_mysql_die();
// format results by row
   $teller = 0;
   echo "<script type=\"text/javascript\" language=\"JavaScript\">\n";
   while ($badm = mysql_fetch_object($result))
   {
      echo "stm_aix(\"p2i".$teller."\",\"p1i0\",[0,\"Seizoen ".$badm->start."-".$badm->einde."\",\"\",\"\",-1,-1,0,\"http://www.badmintonsport.be/tornooien/resultaten.php?s=".$badm->einde."\",\"_self\",\"Seizoen ".$badm->start."-".$badm->einde."\"]);\n";
      $teller = $teller + 1;
   }
   echo"</script>\n";
   //mysql_free_result($result);
   mysql_close($badm_db);
?>
<script type="text/javascript" language="JavaScript">
<!--stm_ep();
stm_ep();
stm_ep();
stm_aix("p0i4","p0i0",[0,"Spelers","","",-1,-1,0,"http://www.badmintonsport.be/spelers.php","_self","Spelersprofielen en statistieken"]);
stm_aix("p0i5","p0i1",[0,"Jeugd","","",-1,-1,0,"","_self","Informatie over en voor de jeugdspelers"]);
stm_bpx("p1","p0",[1,2,0,0,1,4,0]);
stm_aix("p1i0","p0i0",[0,"Algemeen","","",-1,-1,0,"http://www.badmintonsport.be/jeugd/jeugd.php","_self","Jeugd trainers"]);
stm_aix("p1i1","p0i0",[0,"Teams","","",-1,-1,0,"http://www.badmintonsport.be/jeugd/jeugdteams.php","_self","Jeugdteams"]);
stm_aix("p1i2","p0i0",[0,"J-team","","",-1,-1,0,"http://www.badmintonsport.be/jeugd/J-team.php","_self","J-team"]);
stm_aix("p1i3","p0i0",[0,"Resultaten","","",-1,-1,0,"http://www.badmintonsport.be/jeugd/resultaten_jeugd.php?s=2008","_self","Resultaten jeugtornooien W&L spelers"]);
stm_aix("p1i4","p0i0",[0,"Jeugdtornooi","","",-1,-1,0,"http://www.badmintonsport.be/jeugd/jeugdtornooi.php","_self","Jeugdtornooi W&L"]);
stm_ep();
stm_aix("p0i6","p0i0",[0,"Recreanten","","",-1,-1,0,"http://www.badmintonsport.be/recreant.php","_self","Recreanten"]);
stm_aix("p0i7","p0i1",[0,"Foto's","","",-1,-1,0,"http://users.pandora.be/ikkegg","_blank","badminton foto's"]);
//-->
</script>
<?php
/*************************************************************************************************
 * Create a link to every photo directory that exists
 ************************************************************************************************/
/* Get the root dirctory of the website */
  $rootdir = $_SERVER['DOCUMENT_ROOT'];
/* directory where all the photo's are located in subdirectories */
  $dir = $rootdir."/pictures";
/* Get all the subdirectories of the pictures directory */
  include $rootdir."/functies/getDirectoryListing.php";
  $subdirs = getDirectoryListing($dir, "a", 1, 0, "none");
/* Number of subdirs in directory */
  $subdir_cnt = count($subdirs);
/* Display a link to every subdirectory */
  for ($i = 0; $i < $subdir_cnt; $i++)
  {
    echo "<script type=\"text/javascript\" language=\"JavaScript\">\n";
    if ($i == 0) // first subdirectory
    {
       echo "stm_bpx(\"p1\",\"p0\",[1,2,0,0,1,4,0]);\n";
    }
		echo "stm_aix(\"p0i".$i."\",\"p0i0\",[0,\"".$subdirs[$i]."\",\"\",\"\",-1,-1,0,\"http://www.badmintonsport.be/pictures/showThumbnails.php?dir=".$subdirs[$i]."\",\"_self\",\"".$subdirs[$i]."\"]);\n";
    if ($i == $subdir_cnt - 1) // last subdirectory
    {
      echo "stm_ep();\n";
    }
    echo"</script>\n";
  }
?>
<script type="text/javascript" language="JavaScript">
<!--
stm_ai("p0i8",[6,4,"#006633","",0,0,0]);
stm_aix("p0i9","p0i0",[0,"Links","","",-1,-1,0,"http://www.badmintonsport.be/links.php","_self","Links"]);
stm_aix("p0i10","p0i0",[0,"Message Board","","",-1,-1,0,"http://www.badmintonsport.be/messageboard/messageboard.php?p=1&m=n&id=0","_blank","Message Board"]);
stm_aix("p0i11","p0i0",[0,"Laatste Nieuws","","",-1,-1,0,"http://www.badmintonsport.be/laatste_nieuws.php","_self","De laatste wijzigingen aan de W&L website"]);
stm_ep();
stm_em();
//-->
</script>
      &nbsp;
 <!-- InstanceBeginEditable name="counter" -->
<!-- InstanceEndEditable -->
       <!-- InstanceBeginEditable name="wiens verjaardag is het" -->
<!-- InstanceEndEditable -->
  </td>
  <td width="100%" valign="top">
<!-- InstanceBeginEditable name="general" -->
   <table width="100%" border="0" align="left" cellpadding="0" cellspacing="0">
    <tr>
     <td class="kleinetekst" width="100%">

      <div id="login" class="kleinetekst"><?php echo $login; ?>&nbsp;</div>
      <h1><?php echo $tornooi->tornooi; ?></h1>
      <form name="inschrijven" action="inschrijving.php" method="post">
      <table width="100%" cellspacing="0" cellpadding="0">
       <tr>
        <td>
        <table id="personaliaFrame">
         <tr>
          <td>
           <table id="personalia" class="kleinetekst">
<?php
  if ($new_form)
  {
?>
            <tr>
             <td>Voornaam</td>
             <td><input type="text" id="voornaam" name="voornaam" size="15" maxlength="30" value="<?php echo $voornaam; ?>" onBlur="javascript:requestData('ajax_get_data.php?voornaam='+this.value, 'voornaam', 'voornaam='+this.value);"></td>
            </tr>
            <tr>
             <td>Naam</td>
             <td><input type="text" id="achternaam" name="achternaam" size="20" maxlength="30" value="<?php echo $achternaam; ?>" onBlur="javascript:requestData('ajax_get_data.php?naam='+document.forms[0].voornaam.value+' '+this.value, 'naam', 'voornaam='+document.forms[0].voornaam.value+' '+this.value);"></td>
            </tr>
<?php
  }
  else
  {
?>
            <tr>
             <td>Naam</td>
             <td><input type="text" id="naam" name="naam" size="35" maxlength="61" value="<?php echo $naam; ?>" onBlur="javascript:if(fireOnBlur){requestData('ajax_get_data.php?naam='+this.value, 'naam', 'naam='+this.value);}"></td>
            </tr>
<?php
  }
?>
            <tr>
             <td>Klassement</td>
             <td><select id="klassement" name="klassement" onChange="autocompleteReeks();">
<?php
  build_options("klassement", $klassement, $badm_db2);
?>
                 </select></td>
            </tr>
            <tr>
             <td><abbr lang="nl" title="Vlaamse Badminton Liga">VBL</abbr> Lidnummer</td>
             <td><input type="text" id="lidnr" name="lidnr" size="8" maxlength="8" value="<?php echo $lidnr; ?>"></td>
            </tr>
<?php
  if ($tornooi->doelgroep == 'J' || $tornooi->doelgroep == 'V')
  {
?>
            <tr>
             <td><a class="help" href="javascript:showHelp('3')">Geboortedatum</a></td>
             <td><input type="text" id="geboortedatum" name="geboortedatum" size="10" maxlength="10" value="<?php echo $geb_dt; ?>">&nbsp;(DD/MM/JJJJ)</td>
            </tr>
            <tr>
             <td><a class="help" href="javascript:showHelp('2')">Leeftijdscategorie</a></td>
             <td><select id="leeftijdscategorie" name="leeftijdscategorie">
<?php
    build_options("leeftijdscategorie", $leeftijdscategorie, $badm_db2);
?>
                 </select></td>
            </tr>
<?php
  }
  else
  {
    echo "<input type=\"hidden\" name=\"geboortedatum\" value=\"".$geb_dt."\">\n";
  }
?>
           </table>
          </td>
         </tr>
        </table>
        </td>
        <td class="kleinetekst" valign="top">
<?php
  if ($new_form)
  {
    echo "&nbsp;";
  }
  elseif ($inschrijving->dt_verzonden)
  {
    echo "Deze inschrijving werd reeds verzonden naar de <a href=\"mailto:".$tornooi->email."\" title=\"".$tornooi->naam."\">organisatie</a> op ".$inschrijving->dt_verzonden.".";
  }
  else
  {
    echo "Deze inschrijving werd nog niet verzonden naar de <a href=\"mailto:".$tornooi->email."\" title=\"".$tornooi->naam."\">organisatie</a>.";
  }
?>
        </td>
       </tr>
      </table>
<?php
  if ($msg == "")
  {
    echo "<br><div id=\"message\" style=\"display:none\">".$msg."</div>\n";
  }
  else
  {
    echo"<div id=\"message\">".$msg."</div>\n";
  }
?>
      <table id="inschrijvingFrame">
       <tr>
        <td>
         <table id="inschrijving" class="kleinetekst">
          <tr>
           <th colspan="2"><a class="help" href="javascript:showHelp('4')">Discipline</a></th>
           <th><a class="help" href="javascript:showHelp('5')">Reeks</a></th>
           <th><a class="help" href="javascript:showHelp('6')">Partner</a></th>
           <th><a class="help" href="javascript:showHelp('7')">Klas</a></th>
           <th><a class="help" href="javascript:showHelp('9')">Club</a></th>
           <th><a class="help" href="javascript:showHelp('8')">Lidnr</a></th>
          </tr>
<?php
  if ($tornooi->enkel == 'j')
  {
?>
          <tr>
<?php
    if ($tornooi->zelfde_disc_mag == 'j')
    {
      if ($discipline1 != "enkel")
      {
        $display = "";
      }
      echo "           <td>1.<select id=\"discipline1\" name=\"discipline1\" onChange=\"toggleDiscipline1();\">\n";
      build_options("discipline", $discipline1, $badm_db2);
      echo "               </select></td>\n";
    }
    else
    {
?>
           <td>Enkel<input type="hidden" id="discipline1" name="discipline1" value="enkel"></td>
<?php
    }
?>
           <td><input type="checkbox" id="enkel" name="enkel"<?php echo $enkel; ?> value="1" onClick="toggleEnkel();"></td>
           <td><select id="reeksE" name="reeksE"<?php echo $disableE; ?>>
<?php
    if ($tornooi->doelgroep == 'J' || $tornooi->doelgroep == 'V')
    {
      build_options("leeftijdscategorie", $reeksE, $badm_db2);
    }
    else
    {
      build_options("klassement", $reeksE, $badm_db2);
    }
?>
               </select></td>
           <td><input type="text" id="partner" name="partner" size="30" maxlength="61" value="<?php echo $partner; ?>"<?php echo $displayE.$disableE; ?> onBlur="javascript:requestData('ajax_get_data.php?partner='+this.value, 'partner', 'partner='+this.value);"></td>
           <td><select id="klasE" name="klasE"<?php echo $displayE.$disableE; ?>>
<?php
    build_options("klassement", $klasE, $badm_db2);
?>
               </select></td>
           <td><input type="text" id="clubE" name="clubE" size="30" maxlength="30" value="<?php echo $clubE; ?>"<?php echo $displayE.$disableE; ?>></td>
           <td><input type="text" id="lidnrE" name="lidnrE" size="8" maxlength="8" value="<?php echo $lidnrE; ?>"<?php echo $displayE.$disableE; ?>></td>
          </tr>
<?php
  }
  if ($tornooi->dubbel == 'j')
  {
?>
          <tr>
<?php
    if ($tornooi->zelfde_disc_mag == 'j')
    {
      echo "           <td>2.<select id=\"discipline2\" name=\"discipline2\" onChange=\"toggleDiscipline2();\">\n";
      build_options("discipline", $discipline2, $badm_db2);
      echo "               </select></td>\n";
    }
    else
    {
?>
           <td>Dubbel<input type="hidden" id="discipline2" name="discipline2" value="dubbel"></td>
<?php
    }
?>
           <td><input type="checkbox" id="dubbel" name="dubbel"<?php echo $dubbel; ?> value="1" onClick="toggleDubbel();"></td>
           <td><select id="reeksD" name="reeksD"<?php echo $disableD; ?>>
<?php
    if ($tornooi->doelgroep == 'J' || $tornooi->doelgroep == 'V')
    {
      build_options("leeftijdscategorie", $reeksD, $badm_db2);
    }
    else
    {
      build_options("klassement", $reeksD, $badm_db2);
    }
?>
               </select></td>
           <td><input type="text" id="dub_partner" name="dub_partner" size="30" maxlength="61" value="<?php echo $dub_partner; ?>" style="text-transform:capitalize;"<?php echo $displayD.$disableD; ?> onBlur="javascript:requestData('ajax_get_data.php?partner='+this.value, 'dub_partner', 'partner='+this.value);"></td>
           <td><select id="klasD" name="klasD"<?php echo $displayD.$disableD; ?>>
<?php
    build_options("klassement", $klasD, $badm_db2);
?>
               </select></td>
           <td><input type="text" id="clubD" name="clubD" size="30" maxlength="30" value="<?php echo $clubD; ?>"<?php echo $displayD.$disableD; ?>></td>
           <td><input type="text" id="lidnrD" name="lidnrD" size="8" maxlength="8" value="<?php echo $lidnrD; ?>"<?php echo $displayD.$disableD; ?>></td>
          </tr>
<?php
  }
  if ($tornooi->gemengd == 'j')
  {
?>
          <tr>
<?php
    if ($tornooi->zelfde_disc_mag == 'j')
    {
      echo "           <td>3.<select id=\"discipline3\" name=\"discipline3\" onChange=\"toggleDiscipline3();\">\n";
      build_options("discipline", $discipline3, $badm_db2);
      echo "               </select></td>\n";
    }
    else
    {
?>
           <td>Gemengd<input type="hidden" id="discipline3" name="discipline3" value="gemengd"></td>
<?php
    }
?>
           <td><input type="checkbox" id="gemengd" name="gemengd"<?php echo $gemengd; ?> value="1" onClick="toggleGemengd();"></td>
           <td><select id="reeksG" name="reeksG"<?php echo $disableG; ?>>
<?php
    if ($tornooi->doelgroep == 'J' || $tornooi->doelgroep == 'V')
    {
      build_options("leeftijdscategorie", $reeksG, $badm_db2);
    }
    else
    {
      build_options("klassement", $reeksG, $badm_db2);
    }
?>
               </select></td>
           <td><input type="text" id="mix_partner" name="mix_partner" size="30" maxlength="61" value="<?php echo $mix_partner; ?>"<?php echo $displayG.$disableG; ?> onBlur="javascript:requestData('ajax_get_data.php?partner='+this.value, 'mix_partner', 'partner='+this.value);"></td>
           <td><select id="klasG" name="klasG"<?php echo $displayG.$disableG; ?>>
<?php
    build_options("klassement", $klasG, $badm_db2);
?>
               </select></td>
           <td><input type="text" id="clubG" name="clubG" size="30" maxlength="30" value="<?php echo $clubG; ?>"<?php echo $displayG.$disableG; ?>></td>
           <td><input type="text" id="lidnrG" name="lidnrG" size="8" maxlength="8" value="<?php echo $lidnrG; ?>"<?php echo $displayG.$disableG; ?>></td>
          </tr>
<?php
  }
?>
         </table>
        </td>
       </tr>
      </table>
      <input type="checkbox" id="direct" name="direct"<?php echo $direct; ?>>&nbsp;<a class="help" href="javascript:showHelp('10')">Inschrijving onmiddellijk naar organisatie verzenden</a><br>
      <input type="checkbox" id="again" name="again"<?php echo $again; ?>>&nbsp;<a class="help" href="javascript:showHelp('11')">Hierna nog een inschrijving doen</a>

      <div id="control">
       <table class="xpbutton" cellspacing="0" cellpadding="0">
        <tr>
<?php
  if ($msg2 == "")
  {
?>
         <td class="L"><img src="../images/button_xp_left.gif" width="4px" height="24px" alt="" /></td>
         <td class="C"> <a href="javascript:doSubmit();" title="Opslaan">OK</a> </td>
         <td class="R"><img src="../images/button_xp_right.gif" width="4px" height="24px" alt="" /></td>
<?php
  }
?>
         <td class="L"><img src="../images/button_xp_left.gif" width="4px" height="24px" alt="" /></td>
         <td class="C"> <a href="inschrijvingen.php?id=<?php echo $tornooi_id; ?>" title="Terug naar overzicht zonder wijzigingen">Cancel</a></td>
         <td class="R"><img src="../images/button_xp_right.gif" width="4px" height="24px" alt="" /></td>
<?php
  if ($msg2 != "")
  {
?>
         <td>&nbsp;<a class="help" href="javascript:showHelp('12')"><span id="noprivileges" class="kleinetekst"><?php echo $msg2; ?></span></a>&nbsp;</td>
<?php
  }
?>
        </tr>
       </table>
      </div>
<?php
  if (!$new_form)
  {
?>
      <input type="hidden" id="inschr_spel_id" name="inschr_spel_id" value="<?php echo $inschr_spel_id; ?>">
<?php
  }
?>
      <input type="hidden" id="tornooi_id" name="tornooi_id" value="<?php echo $tornooi_id; ?>">
      <input type="hidden" id="actie" name="actie" value="<?php echo (isset($inschr_spel_id)) ? "edit" : "add"; ?>">
      <input type="hidden" id="jaar" name="jaar" value="<?php echo $tornooi->jaar; ?>">
      <input type="hidden" id="email" name="email" value="<?php echo $email; ?>">
      <input type="hidden" id="emailE" name="emailE" value="<?php echo $emailE; ?>">
      <input type="hidden" id="emailD" name="emailD" value="<?php echo $emailD; ?>">
      <input type="hidden" id="emailG" name="emailG" value="<?php echo $emailG; ?>">
     </form><br>

   </td>
    </tr>
   </table>
     <!-- InstanceEndEditable -->
  </td>
 </tr>
 <tr>
  <td height="0"></td>
  <td></td>
 </tr>
 <tr>
  <td height="1"><img src="http://www.badmintonsport.be/images/spacer.gif" alt="" width="150" height="1"></td>
  <td></td>
 </tr>
</table>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <!--DWLayoutTable-->
 <tr>
  <td width="100%" height="50" valign="top" bgcolor="#FFFFFF">
   <table width="100%" border="0" cellpadding="0" cellspacing="0" style="border-color: #CCCCCC" bgcolor="#CCCCCC">
       <!--DWLayoutTable-->
    <tr>
     <td align="center" valign="middle" style="border-color: #CCCCCC" bgcolor="#006600">
         <img src="http://www.badmintonsport.be/images/spacer.gif" width="200" height="4" alt=""></td>
    </tr>
    <tr>
     <td width="100%" align="center" valign="middle" style="border-color: #CCCCCC" bgcolor="#CCCCCC" class="gmenu">Last change:
 <!-- InstanceBeginEditable name="datum" -->01/07/2007
 <!-- InstanceEndEditable -->
             E-mail: <a href="mailto:webmaster@badmintonsport.be">webmaster@badmintonsport.be</a></td>
    </tr>
    <tr>
     <td align="center" valign="middle" style="border-color: #CCCCCC" bgcolor="#006600" class="gmenu">
          <img src="http://www.badmintonsport.be/images/spacer.gif" width="200" height="4" alt=""></td>
    </tr>
   </table>
  </td>
 </tr>
</table>
</body>
<!-- InstanceEnd --></html>
<?php
  log_website_usage($start_time, $actie, $badm_db2);
  mysql_close($badm_db2);
  //echo $debug;
?>