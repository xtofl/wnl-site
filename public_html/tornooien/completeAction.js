function completeAction()
{
	validateForm();
	inschrijving.action = "inschrijvingen.php";
	return true;
}

function validateForm()
{
   //check of naam leeg is
      if(IsEmpty(inschrijving.naam)) 
      {
         alert("Vul een waarde in voor het veld Naam");
         inschrijving.naam.focus();
         return false;
      }
   //check of lidnr leeg is
      if(IsEmpty(inschrijving.lidnr)) 
      {
         alert("Vul een waarde in voor het veld Lidnummer");
         inschrijving.lidnr.focus();
         return false;
      }
   //check of klassement leeg is
      if(IsEmpty(inschrijving.klas))
      {
         alert("Vul een waarde in voor het veld Klassement");
         inschrijving.klas.focus();
         return false;
      }
  //check of discipline ingevuld is
      if(IsEmpty(inschrijving.e))
      {
         if(IsEmpty(inschrijving.d))
         {
            if(IsEmpty(inschrijving.g))
            {
               alert("Zet een X bij de discipline(s) die je wil spelen");
               inschrijving.e.focus();
               return false;
            }
         }
      }
}
