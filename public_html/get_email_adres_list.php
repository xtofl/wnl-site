<html>

<!--

Deze pagina print een string af met alle e-mail adressen van de actieve W&L leden.
Je kan de string copy/pasten naar je e-mail client voor verzending.

-->

<body>
<?php

  require_once "functies/badm_db.inc.php";
  $badm_db = badm_conn_db();
/*
  $sql_statement = "SELECT CONCAT(naam, '&lt;', email, '&gt;') AS adres
                      FROM bad_spelers
                     WHERE eind_dt IS NULL
                       AND email IS NOT NULL";
*/

  $sql_statement = "
SELECT DISTINCT CONCAT(s.naam, '&lt;', s.email, '&gt;') AS adres
  FROM bad_spelers s
     , leden_competitie lc
     , bad_competitieploegen c
 WHERE s.id = lc.spelers_id
   AND c.id = lc.competitieploegen_id
   AND s.eind_dt IS NULL
   AND s.email IS NOT NULL
 ORDER BY 1";

  $result = mysql_query($sql_statement, $badm_db) or badm_mysql_die();

  $separator = ";";
  $to = "";

  while ($row = mysql_fetch_object($result))
  {
    $to .= $separator.$row->adres;
  }

  echo substr($to, 1);

?>

</body>
</html>