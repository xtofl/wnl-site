<?php
/**
 * nieuw_uur.php
 *
 * object     : Add a new hour to table bad_speeluren
 * author     : Freek Ceymeulen
 * created    : 27/04/2005
 * parameters : none
 **/

  // Connect to the DB (must be done before calling sess.php)
  require_once "../functies/badm_db.inc.php";
  $badm_db = badm_conn_db();
  require_once('../functies/sess.php');

  if ($_SESSION['auth'] != true)
  {
    // niet ingelogd -> ga naar login pagina
    // geef deze pagina als parameter mee zodat na succesvolle login teruggekeerd kan worden naar deze pagina
    //session_write_close;
    mysql_close($badm_db);
    header("Location: http://www.badmintonsport.be/admin/login.php?ref=".basename($PHP_SELF));
    exit;
  }
/*
--------------------------------------------------------------------------------------------
|| FUNCTIONS
--------------------------------------------------------------------------------------------
*/
  require_once("../functies/general_functions.php");
?>
<html>
<head>
<link href="http://www.badmintonsport.be/css/admin.css" rel="stylesheet" type="text/css">
<title>W&amp;L Admin Module</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="JavaScript" src="../scripts/modal_dialog.js"></script>
<script language="JavaScript">
<!--
var retval = "";
// Calls a dialog window with LOV-values for this field
function showLov(obj1, obj2)
{
  if (navigator.appName == "Microsoft Internet Explorer")
  {
    retval = window.showModalDialog("http://www.badmintonsport.be/functies/build_dialog_window.php?p_lov_item="+obj1.name+"&p_search_string="+obj1.value.toUpperCase().replace('%', '***')+"&p_filter_min_length=0", window, "status:no; resizable:no; font-size:16px; dialogWidth:20em; dialogHeight: 20em");
    if (retval != null)
    {
      var desc_code_arr = retval.split("|");
      obj1.value = desc_code_arr[0];
      if (obj2 != null)
      {
        obj2.value = desc_code_arr[1];
      }
    }
  }
  else
  {
    //openDialog('lovNS.php', 300, 400, setPrefs);
    alert("This functionality only works with MS Internet Explorer.");
  }
}
-->
</script>
</head>
<body bgcolor="#3A6EA5" link="#000000" vlink="#000000" topmargin="0">

<span style="font-size: 8pt; float: right"><?php echo $_SESSION['username']; ?></span>
<?php
  // Build current date and time string
  list($wday,$mday,$month,$year,$hour,$minutes) = split("( )",date("w j n Y H i",time()));
  $weekday = array('zondag', 'maandag', 'dinsdag', 'woensdag', 'donderdag', 'vrijdag', 'zaterdag');
  $months  = array('januari','februari','maart','april','mei','juni','juli','augustus','september','oktober','november','december');
  $newdate = "$weekday[$wday], $mday ".$months[$month-1]." $year $hour:$minutes";
?>
<span style="font-size: 8pt"><?php echo $newdate; ?></span>

<table border="1" cellspacing="0" cellpadding="0" align="center" width="810">
  <tr bgcolor="#C6C3C6" valign="top"><td>
    <table width="100%" border="0" cellspacing="0" cellpadding="1" align="center">
      <tr bgcolor="#400080">
        <td height="20" class="title">&nbsp;W&amp;L - Nieuw speeluur</td>
        <td height="20" align="right" bgcolor="#000084">
          <a href="http://www.badmintonsport.be/docs/W_L_admin_users_guide.doc" target="_blank"><img src="../poll/image/help.gif" width="16" height="14" border="0" alt="Gebruikershandleiding"></a><a href="login.php?action=logout"><img src="../poll/image/cross.gif" width="16" height="14" border="0" alt="Uitloggen"></a></td>
      </tr>
    </table>
    <table border="0" cellspacing="0" cellpadding="0">
      <tr valign="top">
        <td width="100">
<?php
  // Print the menu
  write_menu($_SERVER['PHP_SELF']);
?>
        </td>
        <td>
          <form name="speeluur" action="dml.php" method="POST">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr><hr>
              <td class="td2">Plaats : </td>
              <td><select class="input" name="plaats">
<?php
  build_options('sporthal', 'Veltem', $badm_db);
?>
                  </select></td>
            </tr>
            <tr>
              <td class="td2">Dag : </td>
              <td><select class="input" name="dag">
<?php
  build_options('dag', 'maandag', $badm_db);
?>
                  </select></td>
            </tr>
            <tr>
              <td class="td2">Uur : </td>
              <td><input class="input" type="text" name="uur" value="u" size="6" maxlenght="10"></td>
            </tr>
            <tr>
              <td class="td2">Unit : </td>
              <td><select class="input" name="unit">
<?php
  build_options('unit', '3', $badm_db);
?>
                  </select></td>
            </tr>
            <tr>
              <td class="td2">Doelgroep : </td>
              <td><select class="input" name="doelgroep">
<?php
  build_options('statuut', 'J', $badm_db);
?>
                  </select></td>
            </tr>
            <tr>
              <td class="td2">Type : </td>
              <td><select class="input" name="type">
<?php
  build_options('SPELTYPE', 'training', $badm_db);
?>
                  </select></td>
            </tr>
            <tr>
              <td class="td2">Beschrijving : </td>
              <td><input class="input" type="text" name="beschrijving" size="30" maxlength="100"></td>
            </tr>
            <tr>
              <td class="td2">Verantwoordelijke : </td>
              <td class="td2">
<?php
  if ( strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE'))
  {
?>
<input class="input" type="text" name="verantwoordelijke" value="" size="30" disabled><input type="hidden" name="verantwoordelijke_id"><img src="../images/lov_button.jpg" alt="List Of Values" onClick="showLov(document.speeluur.verantwoordelijke, document.speeluur.verantwoordelijke_id);" style="CURSOR: pointer; CURSOR: hand">
<?php
  }
  else
  {
    echo "<select class=\"input\" name=\"verantwoordelijke\" onChange=\"document.speeluur.verantwoordelijke_id.value=document.speeluur.verantwoordelijke.value;\">\n<option value=\"\">--geen--</option>\n";
    $sql_stmt = "SELECT id, naam FROM bad_spelers WHERE eind_dt IS NULL ORDER BY 2";
    $rslt = mysql_query($sql_stmt, $badm_db) or badm_mysql_die();
    while ($row = mysql_fetch_object($rslt))
    {
      echo "<option value=\"".$row->id."\"";
      echo ">".$row->naam."</option>\n";
    }
    mysql_free_result($rslt);
    echo "</select>\n<input type=\"hidden\" name=\"verantwoordelijke_id\">\n";
  }
?>

</td>
            </tr>
            <tr>
              <td class="td2">Trainer : </td>
              <td class="td2">
<?php
  if ( strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE'))
  {
?>
<input class="input" type="text" name="trainer" value="" size="30" disabled><input type="hidden" name="trainer_id"><img src="../images/lov_button.jpg" alt="List Of Values" onClick="showLov(document.speeluur.trainer, document.speeluur.trainer_id);" style="CURSOR: pointer; CURSOR: hand">
<?php
  }
  else
  {
    echo "<select class=\"input\" name=\"trainer\" onChange=\"document.speeluur.trainer_id.value=document.speeluur.trainer.value;\">\n<option value=\"\">--geen--</option>\n";
    $sql_stmt = "SELECT id, naam FROM bad_spelers WHERE eind_dt IS NULL ORDER BY 2";
    $rslt = mysql_query($sql_stmt, $badm_db) or badm_mysql_die();
    while ($row = mysql_fetch_object($rslt))
    {
      echo "<option value=\"".$row->id."\"";
      echo ">".$row->naam."</option>\n";
    }
    mysql_free_result($rslt);
    echo "</select>\n<input type=\"hidden\" name=\"trainer_id\">\n";
  }
?>
              </td>
            </tr>
            <tr>
              <td class="td2" colspan="2" align="center"><hr>
                <input type="hidden" name="page" value="nieuw_uur.php">
                <input type="submit" name="action" value="Sla Op" class="button" accesskey="S" title="Voeg een nieuw speeluur toe">
              </td>
            </tr>
          </table>
          </form>
        </td>
      </tr>
    </table>
  </td></tr>
</table>
<br>
<br>
</body>
</html>
<?php
  //mysql_close($badm_db);
?>