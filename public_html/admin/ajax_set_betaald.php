<?php
/**
 * ajax_set_betaald.php
 *
 * object     : Methods to return data that are requested asynchronous
 * author     : Freek Ceymeulen
 * created    : 13/01/2013
 * parameters : $spelers_id : 
 *              $bedrag     :
 **/

 /*
 || Functions
 */

  function set_betaald($spelers_id, $bedrag)
  {
    global $debug;
    $debug .= "<br>set_betaald(".$spelers_id.",".$bedrag.")";
    require_once "../functies/badm_db.inc.php";
    $badm_db = badm_conn_db();

    $update_stmt = "UPDATE bad_spelers
                       SET betaald = %d
                         , eind_dt = NULL
                         , usid_wijz = '%s'
                         , dt_wijz = NOW()
                     WHERE id = %d";
    $sql  = sprintf($update_stmt, mysql_real_escape_string($bedrag)
                                , substr(mysql_real_escape_string($_SESSION['usid']), 0, 10)
                                , mysql_real_escape_string($spelers_id));
    $result = mysql_query($sql, $badm_db) or die("Invalid query: " . mysql_error());
    //echo $debug;
  }

 /*
 || Begin
 */

  $debug = "";
  if (isset($_REQUEST["spelers_id"]))
  {
    $spelers_id = $_REQUEST["spelers_id"];
    if (!empty($spelers_id))
    {
      set_betaald($spelers_id, $_REQUEST["bedrag"]);
    }
  }
  //mail("freek.ceymeulen@pandora.be", "Debug ajax_set_betaald.php", $debug, "From: webmaster@badmintonsport.be");
?>