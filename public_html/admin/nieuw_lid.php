<?php
/**
 * nieuw_lid.php
 *
 * author    : Freek Ceymeulen
 * created   : 01/02/2005
 * variables :
 **/

  // Connect to the DB (must be done before calling sess.php)
  require_once "../functies/badm_db.inc.php";
  $badm_db = badm_conn_db();
  require_once('../functies/sess.php');

  if ($_SESSION['auth'] != true)
  {
    // niet ingelogd -> ga naar login pagina
    // geef deze pagina als parameter mee zodat na succesvolle login teruggekeerd kan worden naar deze pagina
    //session_write_close;
    mysql_close($badm_db);
    header("Location: http://www.badmintonsport.be/admin/login.php?ref=".basename($PHP_SELF));
    exit;
  }
/*
--------------------------------------------------------------------------------------------
|| FUNCTIONS
--------------------------------------------------------------------------------------------
*/
  require_once("../functies/general_functions.php");

  function init($var, $default = "")
  {
    if (isset($_POST[$var]))
    {
      return $_POST[$var];
    }
    elseif (isset($_GET[$var]))
    {
      return $_GET[$var];
    }
    else
    {
      return $default;
    }
  }
/*
--------------------------------------------------------------------------------------------
|| BEGIN
--------------------------------------------------------------------------------------------
*/
  // Initialize
  $voornaam = init("voornaam");
  $naam = init("naam");
  $geslacht = init("geslacht", "M");
  $straat = init("straat");
  $nr = init("nr");
  $bus = init("bus");
  $postcode = init("postcode");
  $gemeente = init("gemeente");
  $geboren = init("geboren");
  $tel = init("tel");
  $gsm = init("gsm");
  $lidnr = init("lidnr");
  $klassement = init("klassement", "D");
  $type_speler = init("type_speler", "J");
  $club_dt = init("club_dt", date("d/m/Y")); // Today
  $eind_dt = init("eind_dt");
  $betaald = init("betaald");
  $competitie = init("competitie");
  $clubkampioenschap = init("clubkampioenschap", "N");
  $clubfeest = init("clubfeest", "N");
  $opmerking = init("opmerking");
  $err_msg = NULL;
  if (isset($_GET['err_msg']))
  {
    $err_msg = $_GET['err_msg'];
  }
?>
<html>
<head>
<title>W&amp;L Admin Module</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="http://www.badmintonsport.be/css/admin.css" rel="stylesheet" type="text/css">
<script language="JavaScript" src="../scripts/CalendarPopup.js"></script>
<script language="JavaScript">
  document.write(getCalendarStyles());
  var cal = new CalendarPopup("datediv");
  cal.showNavigationDropdowns();
</script>
<script>
<!--
function set_focus(p_nummer)
{
  if (p_nummer == '')
  {
    document.forms[0].voornaam.focus();
  }
  else
  {
    document.forms[0].gemeente.focus();
  }
}

function doSubmit(p_action)
{
  if (p_action == 'insert')
  {
    if (document.forms[0].naam.value.length == 0)
    {
      alert('Je bent vergeten een naam in te vullen!');
      document.forms[0].naam.focus();
      return false;
    }
    else if (document.forms[0].eind_dt.value.length != 0)
    {
      alert('Je hebt een einddatum ingevuld.\nDit lid zal als NIET actief beschouwd worden!');
      document.forms[0].eind_dt.focus();
      //return false;
    }
    else if (document.forms[0].voornaam.value.length == 0)
    {
      alert('Je bent vergeten een voornaam in te vullen!');
      document.forms[0].voornaam.focus();
      //return false;
    }
    else if (document.forms[0].geboren.value.length == 0)
    {
      alert('Je bent vergeten een geboortedatum in te vullen!\nDeze informatie is belangrijk voor de werking van de website.');
      document.forms[0].geboren.focus();
      //return false;
    }
    else if (document.forms[0].lidnr.value.length == 0)
    {
      alert('Je bent vergeten een VBL lidnummer in te vullen!\nDeze informatie is belangrijk voor de werking van de website.');
      document.forms[0].lidnr.focus();
      //return false;
    }
    else if (document.forms[0].klassement.value.length == 0)
    {
      alert('Je bent vergeten een klassement in te vullen!\nDeze informatie is belangrijk voor de werking van de website.');
      document.forms[0].klassement.focus();
      //return false;
    }
    document.forms[0].action = "dml.php";
  }
  else if (p_action == 'get_gemeente')
  {
    if (document.forms[0].postcode.value.length == 0)
    {
      return false;
    }
  }
  document.forms[0].submit();
  return true;
}
-->
</script>
</head>
<body bgcolor="#3A6EA5" link="#000000" vlink="#000000" topmargin="0" bottommargin="0" onload="set_focus('<?php echo $postcode; ?>')">

<span style="font-size: 8pt; float: right"><?php echo $_SESSION['username']; ?></span>
<?php
  // Build current date and time string
  list($wday,$mday,$month,$year,$hour,$minutes) = split("( )",date("w j n Y H i",time()));
  $weekday = array('zondag', 'maandag', 'dinsdag', 'woensdag', 'donderdag', 'vrijdag', 'zaterdag');
  $months  = array('januari','februari','maart','april','mei','juni','juli','augustus','september','oktober','november','december');
  $newdate = "$weekday[$wday], $mday ".$months[$month-1]." $year $hour:$minutes";
?>
<span style="font-size: 8pt"><?php echo $newdate; ?></span>

<table width="810" border="1" cellspacing="0" cellpadding="0" align="center">
  <tr bgcolor="#C6C3C6" valign="top">
   <td>

    <table width="100%" border="0" cellspacing="0" cellpadding="1" align="center">
      <tr bgcolor="#400080">
        <td height="20" class="title">&nbsp;W&amp;L - Lid Toevoegen</td>
        <td height="20" align="right" bgcolor="#000084">
          <a href="http://www.badmintonsport.be/docs/W_L_admin_users_guide.doc" target="_blank"><img src="../poll/image/help.gif" width="16" height="14" border="0" alt="Gebruikershandleiding"></a><a href="login.php?action=logout"><img src="../poll/image/cross.gif" width="16" height="14" border="0" alt="Uitloggen"></a></td>
      </tr>
    </table>

    <table border="0" cellspacing="0" cellpadding="0">
      <tr valign="top">
        <td width="100">

<?php
  // Print the menu
  write_menu($_SERVER['PHP_SELF']);
?>

        </td>

        <td>

         <form name="leden" method="post" action="<?php echo basename($PHP_SELF); ?>">

          <table width="100%" border="0" cellspacing="0" cellpadding="4">
<?php
  if (!is_null($err_msg))
  {
    echo '<tr><td colspan="2"><font color="red"><b>'.$err_msg.'</b></font></td></tr>';
  }
?>
            <tr>
              <td colspan="3">
                <table border="0" cellspacing="0" cellpadding="2">
                  <tr>
                    <td class="td2">Voornaam : </td>
                    <td><input type="text" name="voornaam" size="10" maxlength="30" class="input" value="<?php echo $voornaam; ?>"></td>
                    <td class="td2">Naam : </td>
                    <td><input type="text" name="naam" size="20" maxlength="30" class="input" value="<?php echo $naam; ?>"></td>
                    <td class="td2">Geslacht : </td>
                    <td><select name="geslacht" class="input">
<?php
  build_options('geslacht', $geslacht, $badm_db);
?>
                        </select></td>
                  </tr>
                  <tr>
                    <td class="td2">Straat : </td>
                    <td><input type="text" name="straat" size="20" maxlength="40" class="input" value="<?php echo $straat; ?>"></td>
                    <td class="td2">Nr : <input type="text" name="nr" size="3" maxlength="10" class="input" value="<?php echo $nr; ?>" style="align: right"></td>
                    <td class="td2">Bus : <input type="text" name="bus" size="3" maxlength="10" class="input" value="<?php echo $bus; ?>" style="align: right"></td>
                    <td class="td2">Postcode : </td>
<?php
  if (isset($_POST['postcode']))
  {
    $postcode = $_POST['postcode'];
    $query = "SELECT gemeente
                FROM postcode
               WHERE postcode = '%s'";
    $sql  = sprintf($query, mysql_real_escape_string($postcode));
    $result0 = mysql_query($sql, $badm_db) or badm_mysql_die();
  }
?>
                    <td><input type="text" name="postcode" size="4" maxlength="6" class="input" style="align: right" value="<?php echo $postcode; ?>" onBlur="return doSubmit('get_gemeente');"></td>
                  </tr>
                  <tr>
                    <td class="td2">Gemeente : </td>
<?php
  if (isset($_POST['postcode']))
  {
    $query = "SELECT gemeente
                FROM postcode
               WHERE postcode = '%s'
            ORDER BY 1";
    $sql  = sprintf($query, mysql_real_escape_string($postcode));
    $result = mysql_query($sql, $badm_db) or badm_mysql_die();
    if (mysql_num_rows($result) == 0)
    {
      echo '                    <td><input type="text" name="gemeente" size="25" class="input"></td>';
    }
    else
    {
      echo '                    <td><select name="gemeente" class="input">';
      while ($row = mysql_fetch_object($result))
      {
        echo '                     <option value="'.$row->gemeente.'" >'.$row->gemeente.'</option>';
      }
      echo '                    </select></td>';
    }
  }
  else
  {
    echo '                    <td><input type="text" name="gemeente" size="25" maxlength="36" class="input"></td>';
  }
?>
                    <td class="td2">Geboren : </td>
                    <td><input type="text" name="geboren" size="10" maxlength="10" class="input" value="<?php echo $geboren; ?>"></td>
                    <td class="td2">T-shirt : </td>
                    <td><select name="maat" class="input">
<?php
  build_options('maat', $maat, $badm_db);
?>
                        </select></td>
                  </tr>
                  <tr>
                    <td class="td2">E-mail : </td>
                    <td><input type="text" name="email" size="25" maxlength="100" class="input" value="<?php echo $email; ?>"></td>
                    <td class="td2">Tel : </td>
                    <td><input type="text" name="tel" size="10" maxlength="12" class="input" value="<?php echo $tel; ?>"></td>
                    <td class="td2">Gsm : </td>
                    <td><input type="text" name="gsm" size="12" maxlength="13" class="input" value="<?php echo $gsm; ?>"></td>
                  </tr>
                  <tr>
                    <td class="td2">Lidnr : </td>
                    <td><input type="text" name="lidnr" size="8" maxlength="8" class="input" value="<?php echo $lidnr; ?>" style="align: right"></td>
                    <td class="td2">Klassement : </td>
                    <td><select name="klassement" class="input">
<?php
  build_options('klassement', $klassement, $badm_db);
?>
                        </select></td>
                    <td class="td2">Type Speler : </td>
                    <td><select name="type" class="input">
<?php
  build_options('statuut', $statuut, $badm_db);
?>
                        </select></td>
                  </tr>
                  <tr>
                    <td class="td2">Lid vanaf : </td>
                    <td><input type="text" name="club_dt" size="10" maxlength="10" class="input" value="<?php echo $club_dt; ?>">
                        <a href="#" onClick="cal.select(document.leden.club_dt, 'anchor1','yyyy/MM/dd'); return false;" name="anchor1" id="anchor1">
                        <img src="../images/b_calendar.png" alt="kalender" border="none"></a></td>
                    <td class="td2">Gestopt op : </td>
                    <td><input type="text" name="eind_dt" size="10" maxlength="10" class="input" value="<?php echo $eind_dt; ?>">
                        <a href="#" onClick="cal.select(document.leden.eind_dt, 'anchor2','yyyy/MM/dd'); return false;" name="anchor2" id="anchor2">
                        <img src="../images/b_calendar.png" alt="kalender" border="none"></a></td>
                    <td class="td2">Betaald : </td>
                    <td class="td2"><input type="text" name="betaald" size="4" maxlength="7" class="input" value="<?php echo $betaald; ?>">&nbsp;&euro;</td>
                  </tr>
                  <tr>
                    <td class="td2">Competitie ? </td>
                    <td><select name="competitie" class="input">
<?php
  build_options('competitiespeler', $competitiespeler, $badm_db);
?>
                        </select></td>
                    <td class="td2">&nbsp;</td>
                    <td>&nbsp;</td>
                    <td class="td2">&nbsp;</td>
                    <td>&nbsp;</td>
                  </tr>
                  <tr>
                    <td class="td2">Opmerking&nbsp;: </td>
                    <td colspan="5" ><input type="text" name="opmerking" size="120" maxlength="150" class="input" value="<?php echo $opmerking; ?>"></td>
                  </tr>
                </table>
              </td>
            </tr>

            <tr>
              <td colspan="3" align="center"><hr>
                <input type="hidden" name="page" value="nieuw_lid.php">
                <button type="button" accesskey="S" style="width: 75px" title="Opslaan" onClick="return doSubmit('insert');"><span style="text-decoration: underline">S</span>ave</button>&nbsp;
              </td>
            </tr>

           </form>

          </table>
      </td></tr>
    </table>
  </td></tr>
</table>
<br>
<br>
<!-- Division voor de kalender popup-->
<div id="datediv" style="position:absolute; visibility:hidden; background-color:white; layer-background-color:white;"></div>
</body>
</html>
<?php
  //mysql_close($badm_db);
?>