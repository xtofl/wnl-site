<?php
/**
 * leden.php
 *
 * object     : Show the specified records from table bad_spelers.
 *              A filter allows to specify which records to query.
 * author     : Freek Ceymeulen
 * created    : 01/02/2005
 * parameters : order : column to order the data by
 *              err_msg : melding
 **/
error_reporting(E_ALL);

  // Connect to the DB (must be done before calling sess.php)
  require_once "../functies/badm_db.inc.php";
  $badm_db = badm_conn_db();
  require_once('../functies/sess.php');

  if (!isset($_SESSION['auth']))
  {
    // niet ingelogd -> ga naar login pagina
    // geef deze pagina als parameter mee zodat na succesvolle login teruggekeerd kan worden naar deze pagina
    //session_write_close;
    mysql_close($badm_db);
    header("Location: http://www.badmintonsport.be/admin/login.php?ref=".basename($PHP_SELF));
    exit;
  }
/*
--------------------------------------------------------------------------------------------
|| FUNCTIONS
--------------------------------------------------------------------------------------------
*/
  include_once("../functies/general_functions.php");

  function init ($var)
  {
    if (isset($_POST[$var]))
    {
      return $_POST[$var];
    }
    //elseif (isset($_GET[$var]))
    //{
      //return $_GET[$var];
    //}
    else return '';
  }
/*
--------------------------------------------------------------------------------------------
|| BEGIN
--------------------------------------------------------------------------------------------
*/
  // Initialize
  $err_msg = NULL;
  if (isset($_GET['err_msg']))
  {
    $err_msg = $_GET['err_msg'];
  }
  $naam = init('naam');
  $type = init('type');
  $klas = init('klas');
  $gesl = init('gesl');
  $ouder = init('ouder');
  $jonger = init('jonger');
  $maat = init('maat');
  $competitie = init('competitie');
  $club_dt = init('club_dt');
  $eind_dt = init('eind_dt');
  $betaald = init('betaald');
  $command = init('command');
  $where = '';
  $orderBy = '2';

  if ($_SERVER['REQUEST_METHOD'] == 'POST' && $command == 'query')
  {
    // Build order by
    $order = $_POST['order'];
    if ($order == 'voornaam')
    {
      $orderBy = '3';
    }
    elseif ($order == 'achternaam')
    {
      $orderBy = '2';
    }
    elseif ($order == 'type')
    {
      $orderBy = '4, 7';
    }
    elseif ($order == 'klassement')
    {
      $orderBy = '5, 7';
    }
    elseif ($order == 'geslacht')
    {
      $orderBy = '6, 5, 7';
    }
    elseif ($order == 'leeftijd')
    {
      $orderBy = '7';
    }
    elseif ($order == 'adres')
    {
      $orderBy = '9';
    }
    elseif ($order == 'woonplaats')
    {
      $orderBy = '10, 11';
    }
    elseif ($order == 'email')
    {
      $orderBy = '8';
    }
    // Build filter (WHERE-clause)
    if (strlen($naam) > 0)
    {
      $where .= " AND s.naam LIKE '%%".mysql_real_escape_string($naam)."%%'";
    }
    if (strlen($type) > 0)
    {
      $where .= " AND s.type_speler = '".mysql_real_escape_string($type)."'";
    }
    if (strlen($klas) > 0)
    {
      $where .= " AND s.klassement LIKE '%%".mysql_real_escape_string($klas)."%%'";
    }
    if (strlen($gesl) > 0)
    {
      $where .= " AND s.geslacht = '".mysql_real_escape_string($gesl)."'";
    }
    if (strlen($ouder) > 0)
    {
      $where .= " AND YEAR(CURRENT_DATE) - YEAR(s.geb_dt) - (RIGHT(CURRENT_DATE, 5) < RIGHT(s.geb_dt, 5)) > ".mysql_real_escape_string($ouder);
    }
    if (strlen($jonger) > 0)
    {
      $where .= " AND YEAR(CURRENT_DATE) - YEAR(s.geb_dt) - (RIGHT(CURRENT_DATE, 5) < RIGHT(s.geb_dt, 5)) < ".mysql_real_escape_string($jonger);
    }
    if (strlen($maat) > 0)
    {
      $where .= " AND s.maat = '".mysql_real_escape_string($maat)."'";
    }
    if (strlen($competitie) > 0)
    {
      $where .= " AND s.competitie LIKE '%%".mysql_real_escape_string($competitie)."%%'";
    }
    if (strlen($club_dt) > 0)
    {
      $where .= " AND s.club_dt >= '".mysql_real_escape_string(get_valid_date($club_dt))."'";
    }
    if ($betaald == 'on')
    {
      $where .= " AND s.betaald IS NULL";
    }
    if (strlen($eind_dt) > 0)
    {
      $where .= " AND s.eind_dt >= '".mysql_real_escape_string(get_valid_date($eind_dt))."'";
    }
    else
    {
      $where .= " AND ( s.eind_dt IS NULL OR s.eind_dt > NOW() )";
    }
    // We willen dezelfde query uitvoeren wanneer we opnieuw in dit scherm komen
    // daarom slaan we deze informatie op in een cookie
    setcookie ("where", $where, time()+900);  /* verloopt in 15 min */
    setcookie ("order", $orderBy, time()+900);  /* verloopt in 15 min */
  }
  else //komende van een ander scherm
  {
    if (isset($_COOKIE['where']))
    {
      // de str_replace is om de sprintf functie te laten slagen
      $where = str_replace('%', '%%', stripslashes($_COOKIE['where']));
    }
    if (isset($_COOKIE['order']))
    {
      $orderBy = $_COOKIE['order'];
    }
  }
?>
<html>
<head>
<title>W&amp;L Admin Module</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="http://www.badmintonsport.be/css/admin.css" rel="stylesheet" type="text/css">
<SCRIPT LANGUAGE="JavaScript">
<!--
function set_focus() {
  document.leden.zoek.focus();
}
function doSubmit(p_action)
{
  if (p_action == 'send_mail')
  {
    document.forms[0].action = 'zend_email.php';
  }
  else if (p_action == 'print_labels')
  {
    document.forms[0].action = 'print_etiketten_pdf.php?rows=8&topmargin=10&padding=5&leftmargin=5&bottommargin=0&vertspace=1';
//    alert('Deze functie is nog niet ge�mplementeerd');
//    return false;
  }
  else if (p_action == 'to_excel')
  {
    document.forms[0].action = 'leden_excel.php';
    document.forms[0].submit();
    document.forms[0].action = 'leden.php';
    return true;
  }
  else if (p_action == 'disactivate')
  {
    if (confirm("Je staat op het punt om alle leden die nog niet betaald hebben (veld 'betaald' is leeg) op non-actief te zetten.\nWil je doorgaan met deze handeling?"))
    {
      document.forms[0].action = 'dml.php';
      document.forms[0].command.value = 'disactivate';
      document.forms[0].submit();
    }
    else
    {
      return false;
    }
  }
  else //order by
  {
    document.forms[0].order.value = p_action;
  }
  document.forms[0].submit();
  return true;
}
function doReset()
{
  // Clear every filter field
  document.forms[0].naam.value = '';
  document.forms[0].type.value = '';
  document.forms[0].klas.value = '';
  document.forms[0].gesl.value = '';
  document.forms[0].ouder.value = '';
  document.forms[0].jonger.value = '';
  document.forms[0].maat.value = '';
  document.forms[0].competitie.value = '';
  document.forms[0].club_dt.value = '';
  document.forms[0].eind_dt.value = '';
  document.forms[0].ex.value = '';
  document.forms[0].betaald.value = '';
  document.forms[0].clubfeest.value = '';
}
// -->
</SCRIPT>
</head>

<body bgcolor="#3A6EA5" link="#000000" vlink="#000000" topmargin="0" onload="set_focus()">

<span style="font-size: 8pt; float: right"><?php echo $_SESSION['username']; ?></span>
<?php
  // Build current date and time string
  list($wday,$mday,$month,$year,$hour,$minutes) = preg_split("( )",date("w j n Y H i",time()));
  $weekday = array('zondag', 'maandag', 'dinsdag', 'woensdag', 'donderdag', 'vrijdag', 'zaterdag');
  $months  = array('januari','februari','maart','april','mei','juni','juli','augustus','september','oktober','november','december');
  $newdate = "$weekday[$wday], $mday ".$months[$month-1]." $year $hour:$minutes";
?>
<span style="font-size: 8pt"><?php echo $newdate; ?></span>

<table border="1" cellspacing="0" cellpadding="0" align="center" width="810">
  <tr bgcolor="#C6C3C6" valign="top">
   <td>

    <table width="100%" border="0" cellspacing="0" cellpadding="1" align="center">
      <tr bgcolor="#400080">
        <td height="20" class="title">&nbsp;W&amp;L - Ledenoverzicht</td>
        <td height="20" align="right" bgcolor="#000084">
          <a href="../docs/W_L_admin_users_guide.doc" target="_blank"><img src="../poll/image/help.gif" width="16" height="14" border="0" alt="Gebruikershandleiding"></a><a href="login.php?action=logout"><img src="../poll/image/cross.gif" width="16" height="14" border="0" alt="Uitloggen"></a></td>
      </tr>
    </table>

    <table border="0" cellspacing="0" cellpadding="0">
      <tr valign="top">
        <td width="100">
<?php
  // Print the menu
  write_menu($_SERVER['PHP_SELF']);
?>
        </td>
        <td>
         <form name="leden" method="post" action="<?php echo basename($_SERVER['PHP_SELF']); ?>">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
<?php
  if (!is_null($err_msg))
  {
    echo '<tr><td><font color="red"><b>'.$err_msg.'</b></font></td></tr>';
  }
?>
            <tr>
              <td class="td2">
                naam:        <input type="text" name="naam" size="25" class="input" value="<?php echo $naam ?>">
                type speler: <input type="text" name="type" size="3" class="inputUpper" value="<?php echo $type ?>">
                klassement:  <input type="text" name="klas" size="2" class="inputUpper" value="<?php echo $klas ?>">
                geslacht:    <input type="text" name="gesl" size="1" class="inputUpper" value="<?php echo $gesl ?>">
                ouder dan:   <input type="text" name="ouder" size="2" class="input" value="<?php echo $ouder ?>">
                jonger dan:  <input type="text" name="jonger" size="2" class="input" value="<?php echo $jonger ?>"><br/>
                maat:        <input type="text" name="maat" size="2" class="inputUpper" value="<?php echo $maat ?>">
                competitie?: <input type="text" name="competitie" size="6" class="input" value="<?php echo $competitie ?>">
                lid sinds:   <input type="text" name="club_dt" size="10" class="input" value="<?php echo $club_dt ?>">
                gestopt na:  <input type="text" name="eind_dt" size="10" class="input" value="<?php echo $eind_dt ?>">
                niet betaald:     <input type="checkbox" name="betaald"<?php if ($betaald == 'on'){ echo ' checked';} ?>>
                &nbsp;<input type="submit" class="button" name="zoek" value="Zoek" title="Haal de gevraagde leden op">
                &nbsp;<input type="button" value="Reset" class="button" onClick="doReset();">
              </td>
            </tr>
            <tr>
              <td><hr>
                <table border="0" cellspacing="0" cellpadding="3">
                  <tr>
                    <th><a href="#" title="Sorteer volgens achternaam" onClick="doSubmit('achternaam');">Achternaam</a></th>
                    <th><a href="#" title="Sorteer volgens voornaam" onClick="doSubmit('voornaam');">Voornaam</a></th>
                    <th><a href="#" title="Sorteer vogens statuut" onClick="doSubmit('type');">St</a></th>
                    <th><a href="#" title="Sorteer volgens klassement" onClick="doSubmit('klassement');">Kl</a></th>
                    <th><a href="#" title="Sorteer volgens geslacht" onClick="doSubmit('geslacht');">Sex</a></th>
                    <th><a href="#" title="Sorteer volgens leeftijd" onClick="doSubmit('leeftijd');">Lft</a></th>
                    <th><a href="#" title="Sorteer volgens adres" onClick="doSubmit('adres');">Adres</a></th>
                    <th><a href="#" title="Sorteer volgens postcode, gemeente" onClick="doSubmit('woonplaats');">Woonplaats</a></th>
                    <th><a href="#" title="Sorteer volgens E-mail adres" onClick="doSubmit('email');">E-mail</a></th>
                    <th title="Ontvangt 'Boekske'">B</th>
                  </tr>
<?php
  $totaal = 0;
  $ids = '';
  $mailing_list = '';
  if (strlen($where) > 0) //($command == 'query')
  {
    // Build select statement
    $query  = "SELECT s.id
                    , s.achternaam
                    , s.voornaam
                    , s.type_speler
                    , s.klassement
                    , s.geslacht
                    , YEAR(CURRENT_DATE) - YEAR(s.geb_dt) - (RIGHT(CURRENT_DATE, 5) < RIGHT(s.geb_dt, 5)) AS leeftijd
                    , s.email
                    , s.adres
                    , s.postcode
                    , s.woonplaats
                    , s.clubblad
                 FROM bad_spelers s";
    if ($where == '')
    {
      $query .= " WHERE s.eind_dt IS NULL";
    }
    else
    {
      $query .= " WHERE ".substr($where, 4);
    }
    $query .= " ORDER BY %s";
    $sql  = sprintf($query, mysql_real_escape_string($orderBy));
    $result = mysql_query($sql, $badm_db) or badm_mysql_die();
    $totaal = mysql_num_rows($result);
    for ($i=0; $i < $totaal; $i++)
    {
      $row = mysql_fetch_assoc($result);
      echo '                  <tr class="'; echo ($i%2==0) ? "even" : "odd"; echo '">';
      echo '                    <td class="td2"><a href="bewerken_lid.php?id='.$row['id'].'" title="Toon detail">'.$row['achternaam'].'</a></td>';
      echo '                    <td class="td2"><a href="bewerken_lid.php?id='.$row['id'].'" title="Toon detail">'.$row['voornaam'].'</a></td>';
      echo '                    <td align="center" class="td2">'.$row['type_speler'].'</td>';
      echo '                    <td align="center" class="td2">'.$row['klassement'].'</td>';
      echo '                    <td align="center" class="td2">'.$row['geslacht'].'</td>';
      echo '                    <td align="right" class="td2">'.floor($row['leeftijd']).'</td>';
      echo '                    <td class="td2">'.$row['adres'].'</td>';
      echo '                    <td class="td2">'.$row['postcode'].' '.$row['woonplaats'].'</td>';
      echo '                    <td class="td2"><a href="mailto:'.$row['email'].'" title="Verstuur bericht">'.$row['email'].'</a></td>';
      if ($row['clubblad'] == 'J')
      {
        $boekske = '<img src="../images/check.png" alt="Ontvangt het Boekske">';
      }
      else
      {
        $boekske = '&nbsp;';
      }
      echo '                    <td class="td2">'.$boekske.'</td>';
      echo "                  </tr>\n";
      // build list with E-mail adresses
      if (strlen($row['email']) > 0)
      {
        $mailing_list .= ','.$row['voornaam'].' '.$row['achternaam'].'<'.$row['email'].'>';
      }
      // build list with id's
      $ids .= ",'".$row['id']."'";
    }
    mysql_free_result($result);
  }
?>
                </table>
              </td>
            </tr>
            <tr>
              <td class="td2">
                <hr>
                <span style="float: right">
                <input type="hidden" name="page" value="leden.php">
                <input type="hidden" name="command" value="query">
                <input type="hidden" name="order" value="voornaam">
              <!-- E-mail van elke opgevraagde speler => send mail -->
                <input type="hidden" name="aan" value="<?php echo substr($mailing_list, 1); ?>">
              <!-- id van elke opgevraagde speler => export to Excel -->
                <input type="hidden" name="ids" value="<?php echo substr($ids, 1); ?>">
              <!-- Action buttons -->
                <button type="button" name="email" title="Verstuur een e-mail naar alle opgevraagde leden" onClick="return doSubmit('send_mail');" accesskey="E">Verstuur <span style="text-decoration: underline">E</span>mail</button>
                <button type="button" name="excel" title="Open alle gegevens van de opgevraagde leden in Microsoft Excel" onClick="return doSubmit('to_excel');" accesskey="X">Open in E<span style="text-decoration: underline">x</span>cel</button>
<?php
  if (isset($_SESSION['administrator']))
  {
?>
                <button type="button" name="boekske" title="Print alle adressen voor het Boekske op labels" onClick="return doSubmit('print_labels');" accesskey="P"><span style="text-decoration: underline">P</span>rint adressen</button>
                <button type="button" name="remove" title="Zet alle leden die niet betaald hebben op non-actief" onClick="return doSubmit('disactivate');">Zet op non-actief</button>
<?php
  }
?>
                </span>
                Totaal: <?php if ($command != 'delete'){ echo $totaal;} else{ echo "0";} ?> leden</td>
            </tr>
          </table>
        </form>
      </td></tr>
    </table>
  </td></tr>
</table>
<br>
<br>
<?php
  //mysql_close($badm_db);
?>
</body>
</html>