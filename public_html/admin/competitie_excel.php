<?php
/**
 * competitie_excel.php
 * 
 * object    : creates an Excel file and forces a download of this file
 * author    : Freek Ceymeulen
 * created   : 01/05/2005
 **/

  // Connect to the DB (must be done before calling sess.php)
  require_once "../functies/badm_db.inc.php";
  $badm_db = badm_conn_db();
  require_once('../functies/sess.php');

  if ($_SESSION['auth'] != true)
  {
    // niet ingelogd -> ga naar login pagina
    //session_write_close;
    mysql_close($badm_db);
    header("Location: http://www.badmintonsport.be/admin/login.php");
    exit;
  }
/*
--------------------------------------------------------------------------------------------
|| FUNCTIONS
--------------------------------------------------------------------------------------------
*/
function ForceFileDownload($file)
{ 
  $filesize = @filesize($file);
  header("Content-Description: File Transfer");
  header("Content-Length: " . $filesize);
  header("Content-Disposition: attachment; filename=".basename($file)); 
  header("Content-Type: application/octet-stream"); 
  header("Content-Type: application/force-download"); 
  header("Content-Type: application/download"); 
  header("Content-Transfer-Encoding: binary");
  header("Cache-Control: no-cache, must-revalidate, post-check=0, pre-check=0");
  header("Pragma:no-cache"); 
  header("Expires:0");
  // Set the number of seconds the script is allowed to run
  @set_time_limit(600); 
  readfile($file); 
}
/*
--------------------------------------------------------------------------------------------
|| BEGIN
--------------------------------------------------------------------------------------------
*/
  $sep = "\t";
  $query = "
SELECT SUBSTRING(s.naam, LOCATE(' ', s.naam) + 1, LENGTH(s.naam) - LOCATE(' ', s.naam)) AS naam
     , SUBSTRING(s.naam, 1, LOCATE(' ', s.naam)) AS voornaam
     , s.klassement
     , CONCAT(CONCAT(c.type, ' '), c.ploegnummer) AS ploeg
     , c.afdeling
     , lc.type
     , (TO_DAYS(NOW()) - TO_DAYS(s.geb_dt)) / 365 AS leeftijd
     , s.lidnr
     , s.adres
     , s.postcode
     , s.woonplaats
     , IFNULL(s.gsm, s.tel) AS telefoon
     , s.email
  FROM bad_spelers s
     , leden_competitie lc
     , bad_competitieploegen c
 WHERE s.id = lc.spelers_id
   AND c.id = lc.competitieploegen_id
   AND s.eind_dt IS NULL
 ORDER BY 4, 3, 1";
 
  $result = mysql_query($query, $badm_db) or badm_mysql_die();
  if (mysql_num_rows($result) == 0)
  {
    echo "No data found";
  }
  else
  {
    $file = "competitiespelers.xls";    
    if (!$file_handle = fopen($file, "w"))
    {
      echo "<br>Kan bestand ($file) niet openen/aanmaken";
    }
    $data = "Naam".$sep."Voornaam".$sep."Klassement".$sep."Ploeg".$sep."Afdeling".$sep."Basis/Reserve".$sep.
            "Leeftijd".$sep."Lidnummer".$sep."Adres".$sep."Postcode".$sep."Woonplaats".$sep."Tel".$sep.
            "E-mail\n";
    if (!fwrite($file_handle, $data))
    {
      echo "<br/>Kan niet schrijven naar bestand ($file)";
    }
    
    while ($row = mysql_fetch_array($result, MYSQL_NUM))
    {
      $data = "";
      for ($i=0; $i < count($row); $i++)
      {
        if ($i == count($row) - 1)
        {
          $data .= $row[$i]."\n";
        }
        else
        {
          $data .= $row[$i].$sep;
        }
      }
      if (!fwrite($file_handle, $data))
      {
        echo "<br/>Kan niet schrijven naar bestand ($file)";
      }
    }
    //echo "<br>Export met succes gemaakt naar $file";    
    fclose($file_handle);
  }
  //mysql_close($badm_db);
  ForceFileDownload($file);
?>