<?php
/**
 * inschrijvingen.php
 *
 * object     : Show the specified records from table inschrijvingen.
 *              A filter allows to specify which records to query.
 * author     : Freek Ceymeulen
 * created    : 19/11/2012
 * parameters : order : column to order the data by
 *              err_msg : melding
 **/
error_reporting(E_ALL);

  // Connect to the DB (must be done before calling sess.php)
  require_once "../functies/badm_db.inc.php";
  $badm_db = badm_conn_db();
  require_once('../functies/sess.php');

  if (!isset($_SESSION['auth']))
  {
    // niet ingelogd -> ga naar login pagina
    // geef deze pagina als parameter mee zodat na succesvolle login teruggekeerd kan worden naar deze pagina
    //session_write_close;
    mysql_close($badm_db);
    header("Location: http://www.badmintonsport.be/admin/login.php?ref=".basename($PHP_SELF));
    exit;
  }
/*
--------------------------------------------------------------------------------------------
|| FUNCTIONS
--------------------------------------------------------------------------------------------
*/
  include_once("../functies/general_functions.php");

  function init ($var)
  {
    if (isset($_POST[$var]))
    {
      return $_POST[$var];
    }
    //elseif (isset($_GET[$var]))
    //{
      //return $_GET[$var];
    //}
    else return '';
  }
/*
--------------------------------------------------------------------------------------------
|| BEGIN
--------------------------------------------------------------------------------------------
*/
  // Initialize
  $err_msg = NULL;
  if (isset($_GET['err_msg']))
  {
    $err_msg = $_GET['err_msg'];
  }
  $email = init('email');
  $ogm = init('ogm');
  $voornaam = init('voornaam');
  $achternaam = init('achternaam');
  $lidnr = init('lidnr');
  $maat = init('maat');
  $initiatie = init('initiatie');
  $meewerken = init('meewerken');
  $betaald = init('betaald');
  $administratief = init('administratief');
  $command = init('command');
  $where = '';
  $orderBy = 'i.datum DESC';

  if ($_SERVER['REQUEST_METHOD'] == 'POST' && $command == 'query')
  {
    // Build order by
    $order = $_POST['order'];
    if ($order == 'voornaam')
    {
      $orderBy = '3';
    }
    elseif ($order == 'achternaam')
    {
      $orderBy = '2';
    }
    elseif ($order == 'statuut')
    {
      $orderBy = '4, 7';
    }
    elseif ($order == 'klassement')
    {
      $orderBy = '5, 7';
    }
    elseif ($order == 'geslacht')
    {
      $orderBy = '6, 5, 7';
    }
    elseif ($order == 'leeftijd')
    {
      $orderBy = '7';
    }
    elseif ($order == 'email')
    {
      $orderBy = '8';
    }
    elseif ($order == 'lidnr')
    {
      $orderBy = '9';
    }
    elseif ($order == 'datum')
    {
      $orderBy = 'i.datum';
    }
    elseif ($order == 'ogm')
    {
      $orderBy = 'i.ogm';
    }
    elseif ($order == 'bedrag')
    {
      $orderBy = 'CAST(i.bedrag AS DECIMAL), s.betaald';
    }
    elseif ($order == 'betaald')
    {
      $orderBy = 's.betaald, CAST(i.bedrag AS DECIMAL)';
    }
    // Build filter (WHERE-clause)
    if (strlen($ogm) > 0)
    {
      $where .= " AND i.ogm LIKE '%%".mysql_real_escape_string($ogm)."%%'";
    }
    if (strlen($voornaam) > 0)
    {
      $where .= " AND i.voornaam LIKE '%%".mysql_real_escape_string($voornaam)."%%'";
    }
    if (strlen($achternaam) > 0)
    {
      $where .= " AND i.achternaam = '".mysql_real_escape_string($achternaam)."'";
    }
    if (strlen($maat) > 0)
    {
      $where .= " AND i.maat = '".mysql_real_escape_string($maat)."'";
    }
    if (strlen($lidnr) > 0)
    {
      $where .= " AND i.lidnr LIKE '%%".mysql_real_escape_string($lidnr)."%%'";
    }
    if ($initiatie == 'on')
    {
      $where .= " AND i.initiatie = 'Y' AND i.spelers_id IS NOT NULL AND i.creation_dt > NOW() - INTERVAL 9 MONTH";
    }
    if ($meewerken == 'on')
    {
      $where .= " AND i.meewerken = 'Y' AND i.spelers_id IS NOT NULL AND i.creation_dt > NOW() - INTERVAL 9 MONTH";
    }
    if ($betaald == 'on')
    {
      $where .= " AND ( s.betaald IS NULL OR s.betaald = 0 ) AND i.spelers_id IS NOT NULL AND i.creation_dt > NOW() - INTERVAL 9 MONTH";
    }
    if ($administratief == 'on')
    {
      $where .= " AND i.administratief_lid = 'J' AND i.spelers_id IS NOT NULL AND i.creation_dt > NOW() - INTERVAL 9 MONTH";
    }
    if (strlen($where) == 0)
    {
      $where .= " AND i.spelers_id IS NOT NULL AND i.creation_dt > NOW() - INTERVAL 9 MONTH";
    }
    // We willen dezelfde query uitvoeren wanneer we opnieuw in dit scherm komen
    // daarom slaan we deze informatie op in een cookie
    setcookie ("where2", $where, time()+900);  /* verloopt in 15 min */
    setcookie ("order2", $orderBy, time()+900);  /* verloopt in 15 min */
  }
  else //komende van een ander scherm
  {
    if (isset($_COOKIE['where2']))
    {
      // de str_replace is om de sprintf functie te laten slagen
      $where = str_replace('%', '%%', stripslashes($_COOKIE['where2']));
    }
    else
    {
      $where .= " AND i.spelers_id IS NOT NULL";
    }
    if (isset($_COOKIE['order2']))
    {
      $orderBy = $_COOKIE['order2'];
    }
  }
?>
<html>
<head>
<title>W&amp;L Admin Module</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="http://www.badmintonsport.be/css/admin.css" rel="stylesheet" type="text/css">
<script type="text/javascript" language="JavaScript" src="http://www.badmintonsport.be/scripts/createXmlHttpRequest.js"></script>
<script language="JavaScript">
<!--
function set_focus() {
  document.inschrijven.zoek.focus();
}
function doSubmit(p_action)
{
  if (p_action == 'send_mail')
  {
    document.forms[0].action = 'zend_email.php';
  }
  else
  {
    document.forms[0].order.value = p_action;
  }
  document.forms[0].submit();
  return true;
}
function doReset()
{
  // Clear every filter field
  document.forms[0].ogm.value = '';
  document.forms[0].voornaam.value = '';
  document.forms[0].achternaam.value = '';
  document.forms[0].maat.value = '';
  document.forms[0].lidnr.value = '';
  document.forms[0].initiatie.value = '';
  document.forms[0].meewerken.value = '';
  document.forms[0].betaald.value = '';
  document.forms[0].administratief.value = '';
}
// Verandert de gegeven speler van reserve naar basis of omgekeerd voor dit speeluur
function set_betaald($spelers_id, $bedrag)
{
  requestData('ajax_set_betaald.php?spelers_id='+$spelers_id+'&bedrag='+$bedrag, 'set_betaald', 'spelers_id='+$spelers_id+'&bedrag='+$bedrag);
}
function processData($action)
{
  if ($action == "set_betaald")
  {
    //alert("Betaald updated.");
    ;
  }
}
// -->
</script>
</head>

<body bgcolor="#3A6EA5" link="#000000" vlink="#000000" topmargin="0" onload="set_focus()">

<span style="font-size: 8pt; float: right"><?php echo $_SESSION['username']; ?></span>
<?php
  // Build current date and time string
  list($wday,$mday,$month,$year,$hour,$minutes) = split("( )",date("w j n Y H i",time()));
  $weekday = array('zondag', 'maandag', 'dinsdag', 'woensdag', 'donderdag', 'vrijdag', 'zaterdag');
  $months  = array('januari','februari','maart','april','mei','juni','juli','augustus','september','oktober','november','december');
  $newdate = "$weekday[$wday], $mday ".$months[$month-1]." $year $hour:$minutes";
?>
<span style="font-size: 8pt"><?php echo $newdate; ?></span>

<table border="1" cellspacing="0" cellpadding="0" align="center" width="810">
  <tr bgcolor="#C6C3C6" valign="top">
   <td>

    <table width="100%" border="0" cellspacing="0" cellpadding="1" align="center">
      <tr bgcolor="#400080">
        <td height="20" class="title">&nbsp;W&amp;L - Inschrijvingen</td>
        <td height="20" align="right" bgcolor="#000084">
          <a href="../docs/W_L_admin_users_guide.doc" target="_blank"><img src="../poll/image/help.gif" width="16" height="14" border="0" alt="Gebruikershandleiding"></a><a href="login.php?action=logout"><img src="../poll/image/cross.gif" width="16" height="14" border="0" alt="Uitloggen"></a></td>
      </tr>
    </table>

    <table border="0" cellspacing="0" cellpadding="0">
      <tr valign="top">
        <td width="100">
<?php
  // Print the menu
  write_menu($_SERVER['PHP_SELF']);
?>
        </td>
        <td>
         <form name="inschrijven" method="post" action="<?php echo basename($_SERVER['PHP_SELF']); ?>">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
<?php
  if (!is_null($err_msg))
  {
    echo '<tr><td><font color="red"><b>'.$err_msg.'</b></font></td></tr>';
  }
?>
            <tr>
              <td class="td2">
                <abbr lang="nl" title="gestructureerde mededeling">OGM</abbr>:        <input type="text" name="ogm" size="20" class="input" value="<?php echo $ogm ?>">
                voornaam:      <input type="text" name="voornaam" size="15" class="input" value="<?php echo $voornaam ?>">
                achternaam:    <input type="text" name="achternaam" size="20" class="input" value="<?php echo $achternaam ?>">
                VBL lidnr:     <input type="text" name="lidnr" size="8" class="input" value="<?php echo $lidnr ?>">
                maat:          <input type="text" name="maat" size="3" class="inputUpper" value="<?php echo $maat ?>">
                initiatie?:    <input type="checkbox" name="initiatie"<?php echo ($initiatie == 'on') ? " CHECKED" : ""; ?>>
                meewerken?:    <input type="checkbox" name="meewerken"<?php echo ($meewerken == 'on') ? " CHECKED" : ""; ?>>
                niet betaald?: <input type="checkbox" name="betaald"<?php echo ($betaald == 'on') ? " CHECKED" : ""; ?>>
                administratief lid: <input type="checkbox" name="administratief"<?php echo ($administratief == 'on') ? " CHECKED" : ""; ?>>
                &nbsp;<input type="submit" class="button" name="zoek" value="Zoek" title="Haal de gevraagde inschrijvingen op">
                &nbsp;<input type="button" value="Reset" class="button" onClick="doReset();">
              </td>
            </tr>
            <tr>
              <td><hr>
                <table border="0" cellspacing="0" cellpadding="3">
                  <tr>
                    <th><a href="#" title="Sorteer volgens OGM" onClick="doSubmit('ogm');"><abbr lang="nl" title="gestructureerde mededeling">OGM</abbr></a></th>
                    <th><a href="#" title="Sorteer volgens achternaam" onClick="doSubmit('achternaam');">Achternaam</a></th>
                    <th><a href="#" title="Sorteer volgens voornaam" onClick="doSubmit('voornaam');">Voornaam</a></th>
                    <th><a href="#" title="Sorteer vogens statuut" onClick="doSubmit('statuut');">St</a></th>
                    <th><a href="#" title="Sorteer volgens klassement" onClick="doSubmit('klassement');">Kl</a></th>
                    <th><a href="#" title="Sorteer volgens geslacht" onClick="doSubmit('geslacht');">Sex</a></th>
                    <th><a href="#" title="Sorteer volgens leeftijd op 1 jan" onClick="doSubmit('leeftijd');">Lft</a></th>
                    <th><a href="#" title="Sorteer volgens E-mail adres" onClick="doSubmit('email');">E-mail</a></th>
                    <th><a href="#" title="Sorteer volgens lidnr" onClick="doSubmit('lidnr');">VBL Lidnr</a></th>
                    <th><a href="#" title="Sorteer volgens datum van inschrijving" onClick="doSubmit('datum');">Inschrijvingsdatum</a></th>
                    <th><a href="#" title="Sorteer volgens te betalen bedrag" onClick="doSubmit('bedrag');">Te betalen</a></th>
                    <th><a href="#" title="Sorteer volgens betaald bedrag" onClick="doSubmit('betaald');">Betaald</a></th>
                    <th title="Heeft interesse in initiatiesessies mits extra betaling">I</th>
                    <th title="Wil graag meewerken aan activiteiten en werking van onze club">W</th>
                    <th title="Administratief Lid">A</th>
                  </tr>
<?php
  $totaal = 0;
  $ids = '';
  $mailing_list = '';
  if (strlen($where) > 0) //($command == 'query')
  {
    // Build select statement
    $query  = "SELECT i.spelers_id
                    , i.achternaam
                    , i.voornaam
                    , i.statuut
                    , i.klassement
                    , i.geslacht
                    , YEAR( CURRENT_DATE ) + CASE MONTH( current_date ) WHEN 11 THEN 1 WHEN 12 THEN 1 ELSE 0 END - YEAR( i.geboortedatum ) - (  '01-01' < RIGHT( i.geboortedatum, 5 ) ) AS leeftijd_op_1_jan
                    , i.email2
                    , i.lidnr
                    , i.initiatie
                    , i.meewerken
                    , DATE_FORMAT(i.datum, '%%Y-%%m-%%d %%T') AS dt_inschr
                    , i.ogm
                    , i.bedrag
                    , s.betaald
                    , i.spelers_id
                    , i.administratief_lid
                 FROM inschrijvingen i
                 LEFT JOIN bad_spelers s ON i.spelers_id = s.id";
    if ($where == '')
    {
      $query .= " WHERE 1=1";
    }
    else
    {
      $query .= " WHERE ".substr($where, 4);
    }
    $query .= " ORDER BY %s";
    $sql  = sprintf($query, mysql_real_escape_string($orderBy));
    $result = mysql_query($sql, $badm_db) or badm_mysql_die();
    $totaal = mysql_num_rows($result);
    for ($i=0; $i < $totaal; $i++)
    {
      $row = mysql_fetch_assoc($result);
      echo '                  <tr class="'; echo ($i%2==0) ? "even" : "odd"; echo '">';
      echo '                    <td align="center" class="td2"><nobr>'.$row['ogm'].'</nobr></td>';
      if (empty($row['spelers_id']) || $row['spelers_id'] == 0)
      {
        echo '                    <td class="td2">'.$row['achternaam'].'</td>';
        echo '                    <td class="td2">'.$row['voornaam'].'</td>';
      }
      else
      {
        echo '                    <td class="td2"><a href="bewerken_lid.php?id='.$row['spelers_id'].'" title="Toon detail">'.$row['achternaam'].'</a></td>';
        echo '                    <td class="td2"><a href="bewerken_lid.php?id='.$row['spelers_id'].'" title="Toon detail">'.$row['voornaam'].'</a></td>';
      }
      echo '                    <td align="center" class="td2">'.$row['statuut'].'</td>';
      echo '                    <td align="center" class="td2">'.$row['klassement'].'</td>';
      echo '                    <td align="center" class="td2">'.$row['geslacht'].'</td>';
      echo '                    <td align="right" class="td2">'.floor($row['leeftijd_op_1_jan']).'</td>';
      echo '                    <td class="td2"><a href="mailto:'.$row['email2'].'" title="Verstuur bericht">'.$row['email2'].'</a></td>';
      echo '                    <td class="td2">'.$row['lidnr'].'</td>';
      echo '                    <td class="td2">'.$row['dt_inschr'].'</td>';
      echo '                    <td class="td2" align="right">'.$row['bedrag'].'</td>';
      if (empty($row['spelers_id']) || $row['spelers_id'] == 0)
      {
        echo '                    <td class="td2" align="right">'.$row['betaald'].'</td>';
      }
      else
      {
        echo '                    <td class="td2"><input type="text" name="betaald'.$row['spelers_id'].'" class="input" style="text-align:right;border: none" size="6" value="'.$row['betaald'].'" onChange="javascript:set_betaald('.$row['spelers_id'].', this.value);"></td>';
      }
      if ($row['initiatie'] == 'Y')
      {
        $initiatie = '<img src="../images/check.png" alt="Heeft interesse in initiatiesessies mits extra betaling">';
      }
      else
      {
        $initiatie = '&nbsp;';
      }
      echo '                    <td class="td2">'.$initiatie.'</td>';
      if ($row['meewerken'] == 'Y')
      {
        $meewerken = '<img src="../images/check.png" alt="Wil graag meewerken aan activiteiten en werking van onze club">';
      }
      else
      {
        $meewerken = '&nbsp;';
      }
      echo '                    <td class="td2">'.$meewerken.'</td>';
      if ($row['administratief_lid'] == 'J')
      {
        $administratief = '<img src="../images/check.png" alt="Administratief Lid">';
      }
      else
      {
        $administratief = '&nbsp;';
      }
      echo '                    <td class="td2">'.$administratief.'</td>';
      echo "                  </tr>\n";
      // build list with E-mail adresses
      if (strlen($row['email2']) > 0)
      {
        $mailing_list .= ','.$row['voornaam'].' '.$row['achternaam'].'<'.$row['email2'].'>';
      }
      // build list with id's
      $ids .= ",'".$row['spelers_id']."'";
    }
    mysql_free_result($result);
  }
?>
                </table>
              </td>
            </tr>
            <tr>
              <td class="td2">
                <hr>
                <span style="float: right">
                <input type="hidden" name="page" value="<?php echo basename($PHP_SELF); ?>">
                <input type="hidden" name="command" value="query">
                <input type="hidden" name="order" value="ogm">
              <!-- E-mail van elke opgevraagde speler => send mail -->
                <input type="hidden" name="aan" value="<?php echo substr($mailing_list, 1); ?>">
              <!-- Action buttons -->
                <button type="button" name="email" title="Verstuur een e-mail naar alle opgevraagde leden" onClick="return doSubmit('send_mail');" accesskey="E">Verstuur&nbsp;<span style="text-decoration: underline">E</span>mail</button>
                </span>
                Totaal: <?php if ($command != 'delete'){ echo $totaal;} else{ echo "0";} ?> inschrijvingen</td>
            </tr>
          </table>
        </form>
      </td></tr>
    </table>
  </td></tr>
</table>
<br>
<br>
<?php
  //mysql_close($badm_db);
  //echo "<!-- ".$sql." -->";
?>
</body>
</html>