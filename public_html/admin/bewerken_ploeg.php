<?php
/**
 * bewerken_ploeg.php
 *
 * author    : Freek Ceymeulen
 * created   : 01/02/2005
 * variables : id : competitieploegen_id
 *             tab : (optional) the selected tab [spelers|beschikbaarheden|basisopstelling]
 **/
  // Connect to the DB (must be done before calling sess.php)
  require_once "../functies/badm_db.inc.php";
  $badm_db = badm_conn_db();
  require_once('../functies/sess.php');

  if ($_SESSION['auth'] != true)
  {
    // niet ingelogd -> ga naar login pagina
    // geef deze pagina als parameter mee zodat na succesvolle login teruggekeerd kan worden naar deze pagina
  //session_write_close;
  mysql_close($badm_db);
    header("Location: http://www.badmintonsport.be/admin/login.php?ref=".basename($PHP_SELF));
    exit;
  }
/*
--------------------------------------------------------------------------------------------
|| FUNCTIONS
--------------------------------------------------------------------------------------------
*/
  require_once("../functies/general_functions.php");
/*
--------------------------------------------------------------------------------------------
|| BEGIN
--------------------------------------------------------------------------------------------
*/
  $ploeg_id = $_GET['id'];
  if (isset($_REQUEST['tab']))
  {
    $tab = $_REQUEST['tab'];
  }
  else
  {
    $tab = "spelers";
  }
  $err_msg = '';
  // Browser detection: this page only renders well in IE
  /*if ( !strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE') )
  {
     $err_msg .= 'This page is intended to be viewed with Internet Explorer';
  }*/
?>
<html>
<head>
<title>W&amp;L Admin Module</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="http://www.badmintonsport.be/css/admin.css" rel="stylesheet" type="text/css">
<script language="JavaScript" src="http://www.badmintonsport.be/scripts/modal_dialog.js"></script>
<script language="JavaScript" type="text/javascript">
<!--
var retval = "";
// Calls a dialog window with LOV-values for this field
function showLov(obj1, obj2)
{
  if (navigator.appName == "Microsoft Internet Explorer")
  {
    retval = window.showModalDialog("http://www.badmintonsport.be/functies/build_dialog_window.php?p_lov_item="+obj1.name+"&p_search_string="+obj1.value.toUpperCase().replace('%', '***')+"&p_filter_min_length=0", window, "status:no; resizable:no; font-size:16px; dialogWidth:20em; dialogHeight: 20em");
    if (retval != null)
    {
      var desc_code_arr = retval.split("|");
      obj1.value = desc_code_arr[0];
      if (obj2 != null)
      {
        obj2.value = desc_code_arr[1];
        set_update_value('PLOEGEN');
      }
    }
  }
  else
  {
    //openDialog('lovNS.php', 300, 400, setPrefs);
    alert("This functionality only works with MS Internet Explorer.\nContact the webmaster.");
  }
}

// Geeft aan of een update op gegeven tabel dient te gebeuren
function set_update_value(p_table)
{
  if (p_table == 'PLOEGEN')
  {
    document.forms[0].update_ploeg.value = 1;
  }
  else if (p_table == 'WEDSTRIJDEN')
  {
    document.forms[0].update_wedstrijden.value = 1;
  }
  else if (p_table == 'BESCHIKBAAR')
  {
    document.forms[0].update_beschikbaarheden.value = 1;
  }
}

function doSubmit(p_action)
{
  if (p_action == 'update')
  {
    if (document.forms[0].kapitein.value.length == 0)
    {
      alert('Je bent vergeten een ploegkapitein in te vullen!');
      document.forms[0].kapitein.focus();
      return false;
    }
    if (document.forms[0].uur.value.length == 0)
    {
      alert('Je bent vergeten een uur in te vullen!');
      document.forms[0].uur.focus();
      //return false;
    }
    document.forms[0].command.value = 'update';
    document.forms[0].action = 'dml.php';
  }
  else if (p_action == 'delete')
  {
    if (confirm("Ben je zeker dat je deze ploeg wilt verwijderen?\nHiermee worden ook alle wedstrijden verwijderd."))
    {
      document.forms[0].command.value = 'delete';
      document.forms[0].action = 'dml.php';
    }
    else
    {
      return false;
    }
  }
  else if (p_action == 'send_mail')
  {
    document.forms[0].action = 'zend_email.php';
  }
  else if (p_action == 'to_word')
  {
    document.forms[0].action = 'competitieploeg_doc.php?id=<?php echo $ploeg_id; ?>';
    document.forms[0].submit();
    //document.forms[0].action = 'bewerken_ploeg.php';
    return true;
  }
  document.forms[0].submit();
  return true;
}

function show_hide_tab(id)
{
  if (document.forms[0].tabblad.value != id)
  {
    // Close previous tab
    tr = document.getElementById(document.forms[0].tabblad.value);
    span = document.getElementById(document.forms[0].tabblad.value+'tab');
    if (tr.style.display == 'none')
    {
      tr.style.display = 'block';
      span.style.textDecoration = 'none';
      span.style.fontWeight = 'bold';
      span.style.cursor = 'text';
    }
    else
    {
      tr.style.display = 'none';
      span.style.textDecoration = 'underline';
      span.style.fontWeight = 'normal';
      span.style.cursor = 'pointer';
    }
    // Open new tab
    tr = document.getElementById(id);
    span = document.getElementById(id+'tab');
    if (tr.style.display == 'none')
    {
      tr.style.display = 'block';
      span.style.textDecoration = 'none';
      span.style.fontWeight = 'bold';
      span.style.cursor = 'text';
    }
    else
    {
      tr.style.display = 'none';
      span.style.textDecoration = 'underline';
      span.style.fontWeight = 'normal';
      span.style.cursor = 'pointer';
    }
  }
  document.forms[0].tabblad.value = id;
}
-->
</script>
</head>
<body bgcolor="#3A6EA5" link="#000000" vlink="#000000" topmargin="0" bottommargin="0">

<span style="font-size: 8pt; float: right"><?php echo $_SESSION['username']; ?></span>
<?php
  // Build current date and time string
  list($wday,$mday,$month,$year,$hour,$minutes) = split("( )",date("w j n Y H i",time()));
  $weekday = array('zondag', 'maandag', 'dinsdag', 'woensdag', 'donderdag', 'vrijdag', 'zaterdag');
  $months  = array('januari','februari','maart','april','mei','juni','juli','augustus','september','oktober','november','december');
  $newdate = "$weekday[$wday], $mday ".$months[$month-1]." $year $hour:$minutes";
?>
<span style="font-size: 8pt"><?php echo $newdate; ?></span>
<?php
  if (!is_numeric($ploeg_id))
  // Check to avoid people passing sql statements in the url
  {
    $err_msg .= "Security violation, admin has been alerted.";
    mail("freek.ceymeulen@pandora.be", "Possible breakin attempt", "On page ".$_SERVER['PHP_SELF']." from IP ".$_SERVER['REMOTE_ADDR']." using method ".$_SERVER['REQUEST_METHOD']." on url ".$_SERVER['REQUEST_URI'], "From: webmaster@badmintonsport.be");
    //exit;
  }
  else
  {
    $query = "SELECT p.type
                   , p.ploegnummer
                   , p.afdeling
                   , p.sporthal
                   , s.naam AS ploegkapitein
                   , p.uur
                   , lf.spelers_id AS kapitein_id
                   , p.id AS ploeg_id
                FROM bad_competitieploegen p
     LEFT OUTER JOIN leden_functies lf ON p.id = lf.ref_id
                                       AND lf.functies_id = 13
     LEFT OUTER JOIN bad_spelers s ON lf.spelers_id = s.id
               WHERE p.id = %d";
    $sql  = sprintf($query, mysql_real_escape_string($ploeg_id));
    $result = mysql_query($sql, $badm_db) or badm_mysql_die();
    if (mysql_num_rows($result) == 0)
    {
      $err_msg .= "Er werden geen gegevens gevonden voor de competitieploeg met id ".$ploeg_id;
    }
    // format results by row
    $ploeg = mysql_fetch_object($result);
    mysql_free_result($result);
  }
?>
<table border="1" cellspacing="0" cellpadding="0" align="center" width="810">
 <tr bgcolor="#C6C3C6" valign="top">
  <td>

   <table width="100%" border="0" cellspacing="0" cellpadding="1" align="center">
    <tr bgcolor="#400080">
     <td height="20" class="title">&nbsp;W&amp;L - Competitieploeg : <?php echo $ploeg->type.' '.$ploeg->ploegnummer; ?></td>
     <td height="20" align="right" bgcolor="#000084">
         <a href="http://www.badmintonsport.be/docs/W_L_admin_users_guide.doc" target="_blank"><img src="../poll/image/help.gif" width="16" height="14" border="0" alt="Gebruikershandleiding"></a><a href="login.php?action=logout"><img src="../poll/image/cross.gif" width="16" height="14" border="0" alt="Uitloggen"></a></td>
    </tr>
   </table>

   <table border="0" cellspacing="0" cellpadding="0">
    <tr valign="top">
     <td width="100">
<?php
  // Print the menu
  write_menu($_SERVER['PHP_SELF']);
?>
     </td>
     <td>

      <table width="100%" border="0" cellspacing="0" cellpadding="2">
       <form name="bewerken_ploeg" action="<?php echo basename($PHP_SELF); ?>" method="post">
        <input type="hidden" name="id" value="<?php echo $ploeg->ploeg_id; ?>">
<?php
  if (strlen($err_msg) > 0)
  {
    echo "<tr><td colspan=\"4\"><font color=\"red\"><b>".$err_msg."</b></font></td></tr>";
  }
?>
        <tr>
         <td class="td2">Reeks:</td>
         <td class="td2"><select name="reeks" class="input" onChange="set_update_value('PLOEGEN');">
<?php
  build_options('afdeling', $ploeg->afdeling, $badm_db);
?>
                  </select></td>
         <td class="td2">Sporthal:</td>
         <td class="td2"><select name="sporthal" class="input" onChange="set_update_value('PLOEGEN');">
<?php
  build_options('sporthal', $ploeg->sporthal, $badm_db);
?>
                  </select></td>
        </tr>
        <tr>
         <td class="td2">Ploegkapitein:</td>
         <td class="td2">
<?php
  if ( strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE'))
  {
?>
                    <input type="text" name="kapitein" size="25" class="input" value="<?php echo $ploeg->ploegkapitein; ?>" disabled><input type="hidden" name="kapitein_id" onChange="set_update_value('PLOEGEN');"><img src="../images/lov_button.jpg" alt="List Of Values" onClick="showLov(document.bewerken_ploeg.kapitein, document.bewerken_ploeg.kapitein_id);" style="CURSOR: pointer; CURSOR: hand">
<?php
  }
  else
  {
    echo "                    <select class=\"input\" name=\"kapitein\" onChange=\"document.bewerken_ploeg.kapitein_id.value=document.bewerken_ploeg.kapitein.value;set_update_value('PLOEGEN');\">\n";
    if (strlen($ploeg->kapitein_id) == 0)
    {
      echo "                     <option value=\"\" selected>--geen--</option>\n";
    }
    $sql_stmt = "SELECT id, naam FROM bad_spelers WHERE eind_dt IS NULL ORDER BY 2";
    $rslt = mysql_query($sql_stmt, $badm_db) or badm_mysql_die();
    while ($row = mysql_fetch_object($rslt))
    {
      if ($row->id == $ploeg->kapitein_id)
      {
        echo "                     <option value=\"".$row->id."\" selected>".$row->naam."</option>\n";
      }
      else
      {
        echo "                     <option value=\"".$row->id."\">".$row->naam."</option>\n";
      }
    }
    mysql_free_result($rslt);
    echo "                    </select>\n";
    echo "                    <input type=\"hidden\" name=\"kapitein_id\">\n";
  }
?>
         </td>
         <td class="td2">Uur:</td>
         <td class="td2"><input type="text" name="uur" size="5" class="input" value="<?php echo $ploeg->uur; ?>" onChange="set_update_value('PLOEGEN');"></td>
        </tr>
        <tr>
         <td colspan="4" align="left"><hr>&nbsp;&nbsp;
     <span id="spelerstab"
<?php
  if ($tab == "spelers")
  {
    echo " style=\"font-weight: bold;\"";
  }
  else
  {
    echo "style=\"text-decoration: underline; cursor: pointer;\"";
  }
?>
 onClick="show_hide_tab('spelers');">Spelers / Wedstrijden</span> |
       <span id="beschikbaarhedentab"
<?php
  if ($tab == "beschikbaarheden")
  {
    echo " style=\"font-weight: bold;\"";
  }
  else
  {
    echo "style=\"text-decoration: underline; cursor: pointer;\"";
  }
?>
 onClick="show_hide_tab('beschikbaarheden');">Beschikbaarheden</span> |
       <span id="basisopstellingtab"
<?php
  if ($tab == "basisopstelling")
  {
    echo " style=\"font-weight: bold;\"";
  }
  else
  {
    echo "style=\"text-decoration: underline; cursor: pointer;\"";
  }
?>
 onClick="show_hide_tab('basisopstelling');">Basisopstelling</span><hr></td>
        </tr>
        <tr id="spelers" style="display:
<?php
  echo $tab == "spelers" ? "block" : "none"
?>
;">
         <td colspan="4">

          <table border="0" cellspacing="0" cellpadding="3">
           <tr>
            <td width="50%" class="td2">
             <table align="center" border="1" cellspacing="0" cellpadding="3" rules="groups" frame="hsides">
              <tr>
               <th>&nbsp;</th>
               <th>Naam</th>
               <th>Klas</th>
               <th>Lidnr</th>
              </tr>
<?php
  // SPELERS
  $query = "SELECT s.naam
                 , s.klassement
                 , s.lidnr
                 , s.email
                 , s.geslacht
                 , lc.type
                 , s.id
              FROM bad_spelers s
                 , leden_competitie lc
                 , bad_competitieploegen p
             WHERE s.id = lc.spelers_id
               AND p.id = lc.competitieploegen_id
               AND p.id = %d
               AND lc.type IS NOT NULL
          ORDER BY 5, 6, 2, 1";
  $sql  = sprintf($query, mysql_real_escape_string($ploeg_id));
  $result = mysql_query($sql, $badm_db) or badm_mysql_die();
  $mailing_list = '';
  $totaal = mysql_num_rows($result);
  $sexe = 'M';
  $num_male = $totaal; //initialize
  for ($i=0; $i < $totaal; $i++)
  {
    $row = mysql_fetch_assoc($result);
    if ($sexe != $row['geslacht'])
    {
       $sexe = 'V';
       $num_male = $i;
       echo '                  <tbody>';
    }
    echo '                  <tr class="'; echo ($i%2==0) ? "even" : "odd"; echo '">';
    echo '                    <td class="td2">'.($i + 1).'.</td>';
    if ($row['type'] == 'TITULARIS')
    {
      $naam = '<b>'.$row['naam'].'</b>';
    }
    else
    {
      $naam = $row['naam'];
    }
    echo '                    <td class="td2"><a href="bewerken_lid.php?id='.$row['id'].'">'.$naam.'</a></td>';
    echo '                    <td class="td2">'.$row['klassement'].'</td>';
    echo '                    <td class="td2">'.$row['lidnr'].'</td>';
    echo "                  </tr>\n";
    // build list with E-mail adresses
    if (strlen($row['email']) > 0)
    {
      $mailing_list .= ','.$row['naam'].'<'.$row['email'].'>';
    }
  }
  $num_female = $totaal - $num_male;
  mysql_free_result($result);
?>
             </table>

            </td>
            <td valign="top" width="50%">

             <table width="100%" border="0" cellspacing="0" cellpadding="2">
              <tr>
               <td colspan="4">
                <table align="center" border="0" cellspacing="0" cellpadding="3">
                 <tr>
                  <th>&nbsp;</th>
                  <th>Datum</th>
                  <th>Uur</th>
                  <th>Ontmoeting</th>
                  <th>Score</th>
                 </tr>
<?php
  // WEDSTRIJDSCHEMA
  $query = "SELECT DATE_FORMAT(m.datum, '%%d/%%m/%%Y') AS datum
                 , m.uur
                 , m.wedstrijd
                 , m.score
                 , m.id
              FROM competitiematchen m
                 , bad_competitieploegen p
             WHERE m.competitieploegen_id = p.id
               AND p.id = %d
          ORDER BY m.datum, 2";
  $sql  = sprintf($query, mysql_real_escape_string($ploeg_id));
  $result = mysql_query($sql, $badm_db) or badm_mysql_die();
  $totaal = mysql_num_rows($result);
  for ($i=0; $i < $totaal; $i++)
  {
    $row = mysql_fetch_assoc($result);
    $class = ($i%2==0) ? "Even" : "Odd";
    echo '                  <tr class="'.$class.'">';
    echo '                    <td class="td2">'.($i + 1).'.</td>';
    echo '                    <td><input type="text" name="datum'.$row['id'].'" class="updateRow'.$class.'" size="10" value="'.$row['datum'].'" onChange="set_update_value(\'WEDSTRIJDEN\');"></td>';
    echo '                    <td><input type="text" name="uur'.$row['id'].'" class="updateRow'.$class.'" size="3" value="'.$row['uur'].'" onChange="set_update_value(\'WEDSTRIJDEN\');"></td>';
    echo '                    <td><input type="text" name="ontmoeting'.$row['id'].'" class="updateRow'.$class.'" size="22" value="'.$row['wedstrijd'].'" onChange="set_update_value(\'WEDSTRIJDEN\');"></td>';
    if ($row['score'] == "")
    {
      $class = "input";
    }
    else
    {

      $class = "updateRow".$class;
    }
    echo '                    <td><input type="text" name="score'.$row['id'].'" class="'.$class.'" size="3" value="'.$row['score'].'" onChange="set_update_value(\'WEDSTRIJDEN\');"></td>';
    echo "                  </tr>\n";
  }
  mysql_free_result($result);
?>
                 <tr class="odd">
                  <td>&nbsp;</td>
                  <td><input type="text" name="datum0"      class="input" size="10"></td>
                  <td><input type="text" name="uur0"        class="input" size="3"></td>
                  <td><input type="text" name="ontmoeting0" class="input" size="22"></td>
                  <td><input type="text" name="score0"      class="input" size="3"></td>
                 </tr>
                </table>
               </td>
              </tr>
             </table>

            </td>
           </tr>
          </table>

         </td>
        </tr>
        <tr id="beschikbaarheden" style="display:
<?php
  echo $tab == "beschikbaarheden" ? "block" : "none"
?>
;">
         <td colspan="4">

<?php

  /*******************
  * BESCHIKBAARHEDEN *
  *******************/
?>
          <table align="center" border="1" cellspacing="0" cellpadding="0" rules="groups" frame="vsides">
          <colgroup span="4">
<?php
  // Bepaal aantal en grootte van de columngroups
  if ($ploeg->type == 'GEMENGD')
  {
    echo '          <colgroup span="'.$num_male.'">';
    echo '          <colgroup span="'.$num_female.'">';
    echo '          <colgroup span="2">';
  }
  elseif ($ploeg->type == 'HEREN')
  {
    echo '          <colgroup span="'.$num_male.'">';
    echo '          <colgroup span="1">';
  }
  else // DAMES
  {
    echo '          <colgroup span="'.$num_female.'">';
    echo '          <colgroup span="1">';
  }
?>
           <tr height="125">
            <td align="left" valign="top" colspan="4" class="td2">&nbsp;X = beschikbaar<br/>
                                                                  &nbsp;0 = niet beschikbaar<br/>
                                                                  &nbsp;1 = speelt</td>
<?php
  // SPELERS: Alle spelers die in deze ploeg spelen
  $query = "SELECT s.naam
                 , lc.type
              FROM bad_spelers s
                 , leden_competitie lc
                 , bad_competitieploegen p
             WHERE s.id = lc.spelers_id
               AND p.id = lc.competitieploegen_id
               AND lc.type IS NOT NULL
               AND p.id = %d
          ORDER BY s.geslacht
                 , lc.type
                 , s.klassement
                 , s.naam";
  $sql  = sprintf($query, mysql_real_escape_string($ploeg_id));
  $result = mysql_query($sql, $badm_db) or badm_mysql_die();
  while ($row = mysql_fetch_row($result))
  {
    if ($row[1] == 'RESERVE')
    {
      // normal font weight
    if ( !strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE') )
    {
      echo "            <th rowspan=\"2\"><img src=\"http://www.badmintonsport.be/functies/vertical_text.php?tekst=".$row[0]."&size=2\" alt=\"".$row[0]."\"></th>\n";
    }
    else // Microsoft Internet Explorer
    {
        echo "            <th rowspan=\"2\" class=\"vertical\"><span style=\"font-weight: normal\">".$row[0]."</span></th>\n";
      }
    }
    else // TITULARIS
    {
      // bold
    if ( !strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE') )
    {
      echo "            <th rowspan=\"2\"><img src=\"http://www.badmintonsport.be/functies/vertical_text.php?tekst=".$row[0]."&size=3\" alt=\"".$row[0]."\"></th>\n";
    }
    else // Microsoft Internet Explorer
    {
        echo "            <th rowspan=\"2\" class=\"vertical\">".$row[0]."</th>\n";
      }
    }
  }
  mysql_free_result($result);

  if ($ploeg->type != 'DAMES')
  {
    if ( !strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE') )
  {
    echo "            <th rowspan=\"2\"><img src=\"http://www.badmintonsport.be/functies/vertical_text.php?tekst=Totaal Heren&size=3\" alt=\"Totaal Heren\"></th>\n";
  }
  else // Microsoft Internet Explorer
  {
      echo "            <th rowspan=\"2\" class=\"vertical\">Totaal Heren</th>\n";
  }
  }
  if ($ploeg->type != 'HEREN')
  {
    if ( !strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE') )
  {
    echo "            <th rowspan=\"2\"><img src=\"http://www.badmintonsport.be/functies/vertical_text.php?tekst=Totaal Dames&size=3\" alt=\"Totaal Dames\"></th>\n";
  }
  else // Microsoft Internet Explorer
  {
      echo "            <th rowspan=\"2\" class=\"vertical\">Totaal Dames</th>\n";
  }
  }
?>
           </tr>
           <tr>
            <th valign="bottom">Dag</th>
            <th valign="bottom">Datum</th>
            <th valign="bottom">Uur</th>
            <th valign="bottom">Wedstrijd</th>
           </tr>
<?php
  // WEDSTRIJDEN: Alle wedstrijden van deze ploeg
  $queryW = "
SELECT m.id
     , CASE DAYOFWEEK(m.datum)
         WHEN 1 THEN 'Zon'
         WHEN 2 THEN 'Ma'
         WHEN 3 THEN 'Di'
         WHEN 4 THEN 'Wo'
         WHEN 5 THEN 'Do'
         WHEN 6 THEN 'Vr'
         WHEN 7 THEN 'Zat'
       END AS dag
     , DATE_FORMAT(m.datum, '%%d/%%m/%%Y') AS datum
     , m.uur
     , m.wedstrijd
  FROM competitiematchen m
 WHERE m.competitieploegen_id = %d
 ORDER BY m.datum
        , m.uur";
  // BESCHIKBAARHEDEN
  $queryB = "
SELECT b.beschikbaarheid
     , s.geslacht
     , m.id AS competitiematchen_id
     , s.id AS spelers_id
  FROM bad_spelers s INNER JOIN leden_competitie lc
                             ON s.id = lc.spelers_id
                     INNER JOIN bad_competitieploegen c
                             ON c.id = lc.competitieploegen_id
                     INNER JOIN competitiematchen m
                             ON c.id = m.competitieploegen_id
                     LEFT OUTER JOIN beschikbaarheden b
                             ON s.id = b.spelers_id
                            AND b.competitiematchen_id = m.id
 WHERE lc.type IS NOT NULL
   AND c.id = %d
   AND m.id = %d
ORDER BY s.geslacht
       , lc.type
       , s.klassement
       , s.naam";

  $sqlW  = sprintf($queryW, mysql_real_escape_string($ploeg_id));
  $resultW = mysql_query($sqlW, $badm_db) or badm_mysql_die();

  // Array die het totaal per speler bijhoudt (index = spelers_id)
  $totPerSpeler = Array();

  $i = 0; // Aantal wedstrijden
  while ($rowW = mysql_fetch_row($resultW))
  {
    // Verhoog aantal wedstrijden met 1
    $i++;
    // Print de wedstrijdgegevens
    echo "           <tr class=\""; echo ($i%2==0) ? "even" : "odd"; echo "\">\n";
    echo "            <td class=\"td2 center\" style=\"padding-right: 6\">".$rowW[1]."</td>\n";
    echo "            <td class=\"td2 center\" style=\"padding-right: 6\">".$rowW[2]."</td>\n";
    echo "            <td align=\"right\" class=\"td2\" style=\"padding-right: 6\">".$rowW[3]."</td>\n";
    echo "            <td class=\"td2\" style=\"padding-right: 3\">".str_replace(" ", "&nbsp;", htmlentities($rowW[4]))."</td>\n";

    // Zoek de beschikbaarheden voor deze wedstrijd
    $sqlB  = sprintf($queryB, mysql_real_escape_string($ploeg_id)
                            , mysql_real_escape_string($rowW[0]));
    $resultB = mysql_query($sqlB, $badm_db) or badm_mysql_die();

    // Initialize totalen
    $tot_male = 0;
    $tot_female = 0;

    // Bepaal de klasse voor de rij
    $class = ($i%2==0) ? "Odd" : "Even";

    while ($rowB = mysql_fetch_row($resultB))
    {
      // Print de beschikbaarheden
      echo "            <td class=\"td2 center\"><input type=\"text\" name=\"b".$rowB[2]."_".$rowB[3]."\" class=\"updateRow".$class." center\" size=\"1\" value=\"".$rowB[0]."\" onChange=\"set_update_value('BESCHIKBAAR');\"></td>\n";

      // Bereken de totalen per wedstrijd
      if (!is_null($rowB[0]) && $rowB[1] == 'M' && $rowB[0] != '0')
      {
        $tot_male = $tot_male + 1;
      }
      elseif (!is_null($rowB[0]) && $rowB[1] == 'V' && $rowB[0] != '0')
      {
        $tot_female = $tot_female + 1;
      }

      // Hou het totaal per speler bij (index = spelers_id)
      if (key_exists($rowB[3], $totPerSpeler)) // dit element bestaat al in de array
      {
        $totPerSpeler[$rowB[3]] = $totPerSpeler[$rowB[3]] + nvl($rowB[0], 0);
      }
      else
      {
        $totPerSpeler[$rowB[3]] = nvl($rowB[0], 0);
      }

    }

    // Print het totaal aantal beschikbare spelers
    if ($ploeg->type == 'GEMENGD')
    {
      $pos = strpos($ploeg->afdeling, "Nat");
      if ($pos === false) // afdeling is niet Nationale
      {
        $male_limit = 2;
      }
      else
      {
        // in nationale afdeling zijn 3 heren vereist
        $male_limit = 3;
      }
      if ($tot_male < $male_limit)
      {
        echo '            <td class="td2 center"><font color="red">'.$tot_male.'</font></td>';
      }
      else
      {
        echo '            <td class="td2 center">'.$tot_male.'</td>';
      }
      if ($tot_female < 2)
      {
        echo '            <td class="td2 center"><font color="red">'.$tot_female.'</font></td>';
      }
      else
      {
        echo '            <td class="td2 center">'.$tot_female.'</td>';
      }
    }
    elseif ($ploeg->type == 'HEREN')
    {
      if ($tot_male < 4)
      {
        echo '            <td class="td2 center"><font color="red">'.$tot_male.'</font></td>';
      }
      else
      {
        echo '            <td class="td2 center">'.$tot_male.'</td>';
      }
    }
    else // DAMES
    {
      if ($tot_female < 4)
      {
        echo '            <td class="td2 center"><font color="red">'.$tot_female.'</font></td>';
      }
      else
      {
        echo '            <td class="td2 center">'.$tot_female.'</td>';
      }
    }
    echo "           </tr>\n";
  } // endWhile

  echo "<tr><td colspan=\"4\">&nbsp;</td>\n";

  // Print het totaal aantal deelnames per speler
  while (list($key, $val) = each($totPerSpeler))
  {
    echo "<td class=\"td2 center\">".$val."</td>\n";
  }
  echo "<td colspan=\"2\">&nbsp;</td></tr>\n";
  mysql_free_result($resultW);
  if (isset($resultB))
  {
    mysql_free_result($resultB);
  }
?>
          </table>
         </td>
        </tr>
        <tr id="basisopstelling" style="display:
<?php
  echo $tab == "basisopstelling" ? "block" : "none"
?>
;">
         <td colspan="4">
          <table width="250" align="center" border="1" cellspacing="0" cellpadding="3" rules="groups" frame="hsides">
           <tr>
            <th>&nbsp;</th>
            <th>Naam</th>
            <th>Klas</th>
            <th>Lidnr</th>
           </tr>
<?php
  /******************
  * BASISOPSTELLING *
  ******************/
  $query = "SELECT s.naam
                 , s.klassement
                 , s.lidnr
                 , s.geslacht
                 , s.id
              FROM bad_spelers s
                 , leden_competitie lc
                 , bad_competitieploegen p
             WHERE s.id = lc.spelers_id
               AND p.id = lc.competitieploegen_id
               AND p.id = %d
               AND lc.basis = 'Y'

          ORDER BY 4, 2, 3";
  $sql  = sprintf($query, mysql_real_escape_string($ploeg_id)
                        , mysql_real_escape_string($seizoen));
  $result = mysql_query($sql, $badm_db) or badm_mysql_die();
  $totaal = mysql_num_rows($result);
  $sexe = 'M';
  for ($i=0; $i < $totaal; $i++)
  {
    $row = mysql_fetch_assoc($result);
    if ($sexe != $row['geslacht'])
    {
       $sexe = 'V';
       echo '                  <tbody>';
    }
    echo '                  <tr class="'; echo ($i%2==0) ? "even" : "odd"; echo '">';
    echo '                    <td class="td2">'.($i + 1).'.</td>';
    echo '                    <td class="td2"><a href="bewerken_lid.php?id='.$row['id'].'">'.$row['naam'].'</a></td>';
    echo '                    <td class="td2">'.$row['klassement'].'</td>';
    echo '                    <td class="td2">'.$row['lidnr'].'</td>';
    echo "                  </tr>\n";
  }
  mysql_free_result($result);
?>
          </table>
         </td>
        </tr>
        <tr>
         <td class="td2" colspan="4" align="center"><hr>
          <!-- nagaan of een update moet gebeuren -->
          <input type="hidden" name="update_ploeg" value="0">
          <input type="hidden" name="update_wedstrijden" value="0">
          <input type="hidden" name="update_beschikbaarheden" value="0">

          <input type="hidden" name="aan"     value="<?php echo substr($mailing_list, 1); ?>">
          <input type="hidden" name="cc"      value="<?php echo substr($mailing_list_cc, 1); ?>">
          <input type="hidden" name="page"    value="bewerken_ploeg.php">
          <input type="hidden" name="command" value="query">
      <input type="hidden" name="tabblad" value="<?php echo $tab; ?>">
<?php
  $authorization = false;
  // De ploegkapitein van deze ploeg mag de gegevens bewerken
  if (isset($_SESSION['ploegkapitein']))
  {
    $ploegen_ids = explode("#", $_SESSION['ploeg_id']);
    foreach ($ploegen_ids as $id)
    {
      if ($id == $ploeg_id)
      {
        $authorization = true;
      }
    }
  }
  if (isset($_SESSION['administrator']) || isset($_SESSION['competitieverantwoordelijke']) || $authorization)
  {
?>
          <button type="button" name="save" accesskey="S" title="gemaakte wijzigingen opslaan" onClick="return doSubmit('update');"><span style="text-decoration: underline">S</span>ave</button>
<?php
    if (isset($_SESSION['administrator']) || isset($_SESSION['competitieverantwoordelijke']))
    {
?>
          <button type="button" name="delete" accesskey="V" title="Verwijder deze ploeg" onClick="return doSubmit('delete');"><span style="text-decoration: underline">V</span>erwijder</button>
<?php
    }
  }
?>
          <button type="button" name="email" accesskey="E" title="Verstuur een e-mail naar alle spelers van deze ploeg" onClick="return doSubmit('send_mail');">Verstuur <span style="text-decoration: underline">E</span>mail</button>
          <button type="button" name="word" accesskey="W" title="Word document om beschikbaarheid te vragen. Kies Save, en dan rightclick en openen met Word" onClick="return doSubmit('to_word');">Lijst in <span style="text-decoration: underline">W</span>ord</button>
         </td>
        </tr>
       </form>
      </table>
     </td>
    </tr>
   </table>
  </td>
 </tr>
</table>
<br>
<br>
</body>
</html>
<?php
  //mysql_close($badm_db);
?>