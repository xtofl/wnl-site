<?php
/**
 * competitie.php
 * 
 * author    : Freek Ceymeulen
 * created   : 01/02/2005
 * variables : seizoen : (optional) competitieseizoen (format YYYY-YYYY)
 **/
 
  // Connect to the DB (must be done before calling sess.php)
  require_once "../functies/badm_db.inc.php";
  $badm_db = badm_conn_db();
  require_once('../functies/sess.php');
  
  if ($_SESSION['auth'] != true)
  {
    // niet ingelogd -> ga naar login pagina
    // geef deze pagina als parameter mee zodat na succesvolle login teruggekeerd kan worden naar deze pagina
	//session_write_close;
	mysql_close($badm_db);
    header("Location: http://www.badmintonsport.be/admin/login.php?ref=".basename($PHP_SELF));
    exit;
  }
/*
--------------------------------------------------------------------------------------------
|| FUNCTIONS
--------------------------------------------------------------------------------------------
*/
  require_once("../functies/general_functions.php");
/*
--------------------------------------------------------------------------------------------
|| BEGIN
--------------------------------------------------------------------------------------------
*/
  // Initialize
  if (isset($_REQUEST['seizoen']))
  {
    $seizoen = $_REQUEST['seizoen'];
  }
  else
  {
    if (date("m") > 5)
	{
      $seizoen = date("Y")."-".(date("Y") + 1);
	}
	else
	{
	  $seizoen = (date("Y") - 1)."-".date("Y");
	}
  }
  $orderBy = '2, 3';
  if (isset($_GET['order']))
  {
    $order = $_GET['order'];
    if ($order == 'type')
    {
      $orderBy = '2';
    }
    elseif ($order == 'team')
    {
      $orderBy = '3';
    }
    elseif ($order == 'afdeling')
    {
      $orderBy = '4';
    }
    elseif ($order == 'kapitein')
    {
      $orderBy = '6';
    }
  }
?>
<html>
<head>
<title>W&amp;L Admin Module</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="http://www.badmintonsport.be/css/admin.css" rel="stylesheet" type="text/css">
<SCRIPT LANGUAGE="JavaScript">
<!--
function go()
{
  box = document.forms[0].seizoen;
  destination = "http://www.badmintonsport.be/admin/competitie.php?seizoen="+box.options[box.selectedIndex].value;
  if (destination) location.href = destination;
}

function doSubmit(p_action)
{
  if (p_action == 'send_mail')
  {
    document.forms[0].action = 'zend_email.php';
  }
  else if (p_action == 'to_excel')
  {
    document.forms[0].action = 'competitie_excel.php';
    document.forms[0].submit();
    document.forms[0].action = 'competitie.php';
    return true;
  }
  document.forms[0].submit();
  return true;
}
// -->
</SCRIPT>
</head>
<body bgcolor="#3A6EA5" link="#000000" vlink="#000000" topmargin="0">

<span style="font-size: 8pt; float: right"><?php echo $_SESSION['username']; ?></span>
<?php
  // Build current date and time string
  list($wday,$mday,$month,$year,$hour,$minutes) = split("( )",date("w j n Y H i",time()));
  $weekday = array('zondag', 'maandag', 'dinsdag', 'woensdag', 'donderdag', 'vrijdag', 'zaterdag');
  $months  = array('januari','februari','maart','april','mei','juni','juli','augustus','september','oktober','november','december');
  $newdate = "$weekday[$wday], $mday ".$months[$month-1]." $year $hour:$minutes";
?>
<span style="font-size: 8pt"><?php echo $newdate; ?></span>

<table border="1" cellspacing="0" cellpadding="0" align="center" width="810">
  <tr bgcolor="#C6C3C6" valign="top"><td>
    <table width="100%" border="0" cellspacing="0" cellpadding="1" align="center">
      <tr bgcolor="#400080">
        <td height="20" class="title">&nbsp;W&amp;L - Competitie</td>
        <td height="20" align="right" bgcolor="#000084">
          <a href="http://www.badmintonsport.be/docs/W_L_admin_users_guide.doc" target="_blank"><img src="../poll/image/help.gif" width="16" height="14" border="0" alt="Gebruikershandleiding"></a><a href="login.php?action=logout"><img src="../poll/image/cross.gif" width="16" height="14" border="0" alt="Uitloggen"></a></td>
      </tr>
    </table>
    <table border="0" cellspacing="0" cellpadding="0">
      <tr valign="top">
        <td width="100">
<?php
  // Print the menu
  write_menu($_SERVER['PHP_SELF']);
?>
        </td>
        <td>
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
           <form name="competitie" action="zend_email.php" method="POST">
            <tr>
              <td><hr><span>&nbsp;Seizoen</span> <select class="input" name="seizoen" onChange="go();">
<?php
    // Selecteer alle seizoenen waarvoor data bestaan + ook zeker het huidige seizoen
    $sql_stmt  = "SELECT DISTINCT seizoen";
    $sql_stmt .= "  FROM bad_competitieploegen ";
    $sql_stmt .= "UNION ";
    $sql_stmt .= "SELECT CONCAT( IF(DATE_FORMAT(CURRENT_DATE(), '%m') > 5, DATE_FORMAT(CURRENT_DATE(), '%Y'), DATE_FORMAT(CURRENT_DATE(), '%Y') - 1)";
    $sql_stmt .= "             , '-'";
    $sql_stmt .= "             , IF(DATE_FORMAT(CURRENT_DATE(), '%m') > 5, DATE_FORMAT(CURRENT_DATE(), '%Y') + 1, DATE_FORMAT(CURRENT_DATE(), '%Y'))) AS seizoen";
    $sql_stmt .= "  FROM bad_competitieploegen";
    $sql_stmt .= " ORDER BY 1";
    $result = mysql_query($sql_stmt, $badm_db) or badm_mysql_die();
    while ($row = mysql_fetch_object($result))
    {
      if ($row->seizoen == $seizoen)
      {
        echo "<option value=\"".$row->seizoen."\" selected>".$row->seizoen."</option>\n";
      }
	  else
      {
        echo "<option value=\"".$row->seizoen."\">".$row->seizoen."</option>\n";
      }
	}
?>		 </select><hr>
                <table border="1" cellspacing="0" cellpadding="3" frame="below" rules="groups">
                  <tr>
                    <th><a href="<?php echo basename($PHP_SELF); ?>?order=type" title="Sorteer">Type</a></th>
                    <th><a href="<?php echo basename($PHP_SELF); ?>?order=team" title="Sorteer">Team</a></th>
                    <th><a href="<?php echo basename($PHP_SELF); ?>?order=afdeling" title="Sorteer">Afdeling</a></th>
                    <th><a href="<?php echo basename($PHP_SELF); ?>?order=kapitein" title="Sorteer">Kapitein</a></th>
                  </tr>
<?php
  $query = "SELECT c.id
                 , c.type
                 , CONCAT('ploeg', c.ploegnummer) AS ploeg
                 , c.afdeling
                 , s.id AS spelers_id
                 , s.naam AS ploegkapitein
                 , s.email
              FROM bad_competitieploegen c
   LEFT OUTER JOIN leden_functies lf ON c.id = lf.ref_id AND lf.functies_id = 13
   LEFT OUTER JOIN bad_spelers s ON lf.spelers_id = s.id
		     WHERE c.seizoen = '%s'
          ORDER BY %s";
  $sql  = sprintf($query, mysql_real_escape_string($seizoen)
                        , mysql_real_escape_string($orderBy));
  $result = mysql_query($sql, $badm_db) or badm_mysql_die();
  
  $mailing_list = '';
  $totaal = mysql_num_rows($result);
  for ($i=0; $i < $totaal; $i++)
  {
    $row = mysql_fetch_assoc($result);
    echo '                  <tr class="'; echo ($i%2==0) ? "even" : "odd"; echo '">';
    echo '                    <td class="td2">'.initcap($row['type']).'</td>';
    echo '                    <td class="td2"><a href="bewerken_ploeg.php?id='.$row['id'].'">'.$row['ploeg'].'</a></td>';
    echo '                    <td class="td2">'.$row['afdeling'].'</td>';
    echo '                    <td class="td2"><a href="bewerken_lid.php?id='.$row['spelers_id'].'">'.$row['ploegkapitein'].'</a></td>';
    echo '                  </tr>';
    // build list with E-mail adresses
    if (strlen($row['email']) > 0)
    {
      $mailing_list .= ','.$row['ploegkapitein'].'<'.$row['email'].'>';
    }
  }
  mysql_free_result($result);
?>
                </table>
              </td>
            </tr>
            <tr>
              <td class="td2"><hr>
                <span style="float: right">
                  <input type="hidden" name="aan" value="<?php echo substr($mailing_list, 1); ?>">
                  <button type="button" name="email" accesskey="E" title="Verstuur een e-mail naar alle ploegkapiteins" onClick="return doSubmit('send_mail');">Verstuur <span style="text-decoration: underline">E</span>mail</button>
                  <button type="button" name="excel" accesskey="X" title="Lijst van alle competitiespelers per ploeg" onClick="return doSubmit('to_excel');">Lijst in E<span style="text-decoration: underline">x</span>cel</button>
                </span>
                &nbsp;Totaal: <?php echo $totaal; ?> ploegen</td>
            </tr>
            </form>
          </table>
          <div id="doc">
          <h4>&nbsp;&nbsp;&nbsp;&nbsp;Documenten</h4><p>
          &nbsp;&nbsp;<a href="http://www.badmintonsport.be/docs/ploeguitwisselingsformulier.doc" target="_blank">Ploeguitwisselingsformulier</a>
		  <br>&nbsp;&nbsp;<a href="http://www.badmintonsport.be/docs/verzekeringspapieren.doc" target="_blank">Verzekering</a>
          </p>
          </div>
      </td></tr>
    </table>
  </td></tr>
</table>
<br>
<br>
<?php
  //mysql_close($badm_db);
?>
</body>
</html>