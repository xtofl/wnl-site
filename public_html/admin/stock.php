<?php
/**
 * stock.php
 *
 * author     : Freek Ceymeulen
 * created    : 29/05/2006
 * parameters : err_msg
 **/

  // Connect to the DB (must be done before calling sess.php)
  require_once "../functies/badm_db.inc.php";
  $badm_db = badm_conn_db();
  require_once('../functies/sess.php');

  if ($_SESSION['auth'] != true)
  {
    // niet ingelogd -> ga naar login pagina
    // geef deze pagina als parameter mee zodat na succesvolle login teruggekeerd kan worden naar deze pagina
    //session_write_close;
    mysql_close($badm_db);
    header("Location: http://www.badmintonsport.be/admin/login.php?ref=".basename($PHP_SELF));
    exit;
  }

  if (isset($_REQUEST['err_msg']))
  {
    $err_msg = $_REQUEST['err_msg'];
  }
  else
  {
    $err_msg = "";
  }

/*
--------------------------------------------------------------------------------------------
|| FUNCTIONS
--------------------------------------------------------------------------------------------
*/

  require_once("../functies/general_functions.php");

/*
--------------------------------------------------------------------------------------------
|| BEGIN
--------------------------------------------------------------------------------------------
*/
?>

<html>
<head>
<title>W&amp;L Admin Module - Stock</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../css/admin.css" rel="stylesheet" type="text/css">
<script language="JavaScript" type="text/JavaScript" src="../scripts/sorttable.js"></script>
</head>
<body bgcolor="#3A6EA5" link="#000000" vlink="#000000" topmargin="0">

<span style="font-size: 8pt; float: right"><?php echo $_SESSION['username']; ?></span>
<?php
  // Build current date and time string
  list($wday,$mday,$month,$year,$hour,$minutes) = split("( )",date("w j n Y H i",time()));
  $weekday = array('zondag', 'maandag', 'dinsdag', 'woensdag', 'donderdag', 'vrijdag', 'zaterdag');
  $months  = array('januari','februari','maart','april','mei','juni','juli','augustus','september','oktober','november','december');
  $newdate = "$weekday[$wday], $mday ".$months[$month-1]." $year $hour:$minutes";
?>
<span style="font-size: 8pt"><?php echo $newdate; ?></span>

<table border="1" cellspacing="0" cellpadding="0" align="center" width="810">

  <tr bgcolor="#C6C3C6" valign="top"><td>
    <table width="100%" border="0" cellspacing="0" cellpadding="1" align="center">
      <tr bgcolor="#400080">
        <td height="20" class="title">&nbsp;W&amp;L - Stock</td>
        <td height="20" align="right" bgcolor="#000084">
          <a href="../docs/W_L_admin_users_guide.doc" target="_blank"><img src="../poll/image/help.gif" width="16" height="14" border="0" alt="Gebruikershandleiding"></a><a href="login.php?action=logout"><img src="../poll/image/cross.gif" width="16" height="14" border="0" alt="Uitloggen"></a></td>
      </tr>
    </table>

    <table border="0" cellspacing="0" cellpadding="0">
      <tr valign="top">
        <td width="100">
<?php
  // Print the menu
  write_menu($_SERVER['PHP_SELF']);
?>
        </td>

        <td style="padding-left: 10px;">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td><hr>
<?php
  if (strlen($err_msg) > 0)
  {
    echo "<font color=\"red\"><b>".$err_msg."</b></font>";
  }
?>                <table border="1" cellspacing="0" cellpadding="3" class="sortable" id="items">
                  <tr>
                    <th>Item</th>
                    <th>Locatie</th>
                    <th>Hoeveelheid</th>
                    <th>Aankoopprijs (�)</th>
                  </tr>
<?php
    $query = "SELECT si.id
                   , si.name
                   , si.location
                   , si.quantity
                   , si.price
                   , si.description
                FROM stock_items si
               ORDER BY dt_wijz DESC";
    $result = mysql_query($query, $badm_db) or badm_mysql_die();

    $i = 0;
    if (mysql_num_rows($result) == 0)
    {
      echo "<tr><td colspan=\"4\">Er werden geen gegevens gevonden.</td></tr>";
    }
    else
    {
      while ($row = mysql_fetch_object($result))
      {
        $i++;
        echo "<tr class=\"";
        echo ($i%2==0) ? "even" : "odd";
        echo "\">\n";
        echo "<td class=\"td2\"><a href=\"stock_item.php?id=".$row->id."\" target=\"_self\">".$row->name."</a></td>\n";
        echo "<td class=\"td2\">".nvl($row->location, "&nbsp;")."</td>\n";
        echo "<td class=\"td2\" align=\"right\">".nvl($row->quantity, "&nbsp;")."</td>\n";
        echo "<td class=\"td2\" align=\"right\">".nvl($row->price, "&nbsp;")."</td>\n";
        echo "</tr>\n";
      }
    }
    mysql_free_result($result);
?>
                </table>

              </td>
            </tr>
            <tr>
              <td class="td2"><hr>
                <span style="float: right">
                  <button type="button" name="Item Toevoegen" accesskey="T" title="Een item aan de stock toevoegen" onClick="window.location.href='stock_item.php';">Item <span style="text-decoration: underline">T</span>oevoegen</button>
                </span>
                Totaal: <?php echo $i; ?> items</td>
            </tr>
          </table>
      </td></tr>

    </table>
  </td></tr>
</table>
<br>
<br>
<?php
  //mysql_close($badm_db);
?>
</body>
</html>