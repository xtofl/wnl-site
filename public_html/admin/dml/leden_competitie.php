<?php
/**
 * leden_competitie.php
 *
 * object     : Methods to do data manipulation on table LEDEN_COMPETITIE
 * author     : Freek Ceymeulen
 * created    : 24/05/2006
 **/

  function insert_leden_competitie($conn, $spelers_id, $competitieploegen_id, $type, $basis, $reeks, $seizoen)
  {
    if (strtoupper($basis) == "Y")
    {
      // Nagaan of deze speler al niet in een andere ploeg in dezelfde reeks in de basis staat
      $query = "SELECT c.id, c.type, c.ploegnummer, c.afdeling FROM bad_competitieploegen c, leden_competitie lc WHERE c.id = lc.competitieploegen_id AND lc.spelers_id = %d AND lc.basis = 'Y' AND c.type = '%s' AND c.seizoen = '%s'";
      $sql  = sprintf($query, mysql_real_escape_string($spelers_id)
                            , mysql_real_escape_string($reeks)
							, substr(mysql_real_escape_string($seizoen), 0, 9));
      $result = mysql_query($sql, $conn) or die("Invalid query: " . mysql_error());

      if (mysql_num_rows($result) > 0)
      {
        $err_msg = "Deze speler staat al in de basis in een andere ".strtolower($reeks)." ploeg.<br>Een speler kan maar in 1 ploeg in de basis staan in eenzelfde reeks. ";
        return $err_msg;
      }
    }

    $insert_stmt = "INSERT INTO leden_competitie (spelers_id, competitieploegen_id, type, basis, user_id, dt_wijz)
                    VALUES (%d, %d, %s, '%s', '%s', NOW())";
    $sql  = sprintf($insert_stmt, mysql_real_escape_string($spelers_id)
                                , mysql_real_escape_string($competitieploegen_id)
                                , strtoupper($type)
                                , mysql_real_escape_string(strtoupper($basis))
                                , mysql_real_escape_string($_SESSION['usid']));
    $result = mysql_query($sql, $conn) or die("Invalid query: " . mysql_error());

    // Sla deze bewerking op in audit tabel
    log_action($_SESSION['usid'], 'INSERT', $spelers_id, stripslashes($sql), $conn);

    return "";
  }

/*------------------------------------------------------------------------------------------------------
 | Verwijder alle records uit LEDEN_COMPETITIE voor de gegeven <type_id>.
 | Type_id = competitieploegen_id of spelers_id
 -------------------------------------------------------------------------------------------------------*/

  function delete_leden_competitie($conn, $id, $type_id)
  {
    if ($type_id != "competitieploegen_id" && $type_id != "spelers_id")
    {
      exit;
    }
    if (LOGGING)
    {
      $query = "SELECT * FROM leden_competitie WHERE ".$type_id." = %d";
      $sql  = sprintf($query, mysql_real_escape_string($id));
      $result = mysql_query($sql, $conn) or die("Invalid query: " . mysql_error());

      while ($row = mysql_fetch_object($result))
      {
        $dml = "INSERT INTO leden_competitie ( spelers_id, competitieploegen_id, type, basis, user_id, dt_wijz )
                VALUES (%d, %d, '%s', '%s', '%s', '%s')";
        $sql  = sprintf($dml, mysql_real_escape_string($row->spelers_id)
                            , mysql_real_escape_string($row->competitieploegen_id)
							, mysql_real_escape_string($row->type)
                            , mysql_real_escape_string($row->basis)
                            , mysql_real_escape_string($row->user_id)
                            , mysql_real_escape_string($row->dt_wijz));

        // Sla deze bewerking op in audit tabel
        log_action($_SESSION['usid'], 'DELETE', $id, stripslashes($sql), $conn);
      }
      mysql_free_result($result);
    }

    $delete_stmt = "DELETE FROM leden_competitie
                     WHERE ".$type_id." = %d";
    $sql  = sprintf($delete_stmt, mysql_real_escape_string($id));
    $result = mysql_query($sql, $conn) or die("Invalid query: " . mysql_error());

    // Sla deze bewerking op in audit tabel
    log_action($_SESSION['usid'], 'DELETE', $id, stripslashes($sql), $conn);
  }

?>