<?php
/**
 * competitiematchen.php
 *
 * object     : Methods to do data manipulation on table COMPETITIEMATCHEN
 * author     : Freek Ceymeulen
 * created    : 24/05/2006
 **/

/*------------------------------------------------------------------------------------------------------
 | Zorg ervoor dat de score een uniform formaat heeft: 9 - 9
 -------------------------------------------------------------------------------------------------------*/

  function format_score ($score)
  {
    // Remove multiple occurences of whitespace characters
    // and convert them into single spaces
    $score = preg_replace('/\s+/', ' ', $score);

    if ( strlen($score) > 0)
    {
      if ( strpos($score, ' - ') === false )
      {
        if ( strpos($score, ' -') === true )
        {
          $score = str_replace(' -', ' - ', $score);
        }
        elseif ( strpos($score, '- ') === true )
        {
          $score = str_replace('- ', ' - ', $score);
        }
        elseif ( strpos($score, '-') === true )
        {
          $score = str_replace('-', ' - ', $score);
        }
      }
    }
    return $score;
  }

/*------------------------------------------------------------------------------------------------------
 | Voeg een nieuwe competitiematch toe.
 -------------------------------------------------------------------------------------------------------*/

  function insert_competitiematch($conn, $competitieploegen_id, $datum, $uur, $wedstrijd, $score)
  {
    $insert_stmt = "INSERT INTO competitiematchen ( competitieploegen_id, datum, uur, wedstrijd, score, user_id )
                    VALUES (%d, '%s', '%s', '%s', '%s', '%s')";
    $sql  = sprintf($insert_stmt, mysql_real_escape_string($competitieploegen_id)
								, mysql_real_escape_string(get_valid_date($datum))
                                , substr(mysql_real_escape_string($uur), 0, 5)
                                , substr(mysql_real_escape_string($wedstrijd), 0, 75)
                                , substr(mysql_real_escape_string(format_score($score)), 0, 5)
                                , mysql_real_escape_string($_SESSION['usid']));
    $result = mysql_query($sql, $conn) or die("Invalid query: " . mysql_error());
    $new_id = mysql_insert_id();

    // Sla deze bewerking op in audit tabel
    log_action($_SESSION['usid'], 'INSERT', $new_id, stripslashes($sql), $conn);
  }

/*------------------------------------------------------------------------------------------------------
 | Wijzig de gegeven competitiematch.
 -------------------------------------------------------------------------------------------------------*/

  function update_competitiematch($conn, $competitiematch_id, $datum, $uur, $wedstrijd, $score)
  {
    $update_stmt = "UPDATE competitiematchen
                       SET datum = '%s'
                         , uur = '%s'
                         , wedstrijd = '%s'
                         , score = '%s'
                         , user_id = '%s'
                     WHERE id = %d";
     $sql  = sprintf($update_stmt, mysql_real_escape_string(get_valid_date($datum))
                                 , substr(mysql_real_escape_string($uur), 0, 5)
                                 , substr(mysql_real_escape_string($wedstrijd), 0, 75)
                                 , substr(mysql_real_escape_string(format_score($score)), 0, 5)
                                 , substr(mysql_real_escape_string($_SESSION['usid']), 0, 10)
                                 , mysql_real_escape_string($competitiematch_id));
    $result = mysql_query($sql, $conn) or die("Invalid query: " . mysql_error());

    // Sla deze bewerking op in audit tabel
    log_action($_SESSION['usid'], 'UPDATE', $competitiematch_id, stripslashes($sql), $conn);
  }

/*------------------------------------------------------------------------------------------------------
 | Verwijder de gegeven competitiematch
 | Vergeet niet om eerst tabel BESCHIKBAARHEDEN op te kuisen.
 -------------------------------------------------------------------------------------------------------*/

  function delete_competitiematch($conn, $competitiematch_id)
  {
    if (LOGGING)
    {
      $query = "SELECT * FROM competitiematchen WHERE id = %d";
      $sql  = sprintf($query, mysql_real_escape_string($competitiematch_id));
      $result = mysql_query($sql, $conn) or die("Invalid query: " . mysql_error());

      $row = mysql_fetch_object($result);
      $dml = "INSERT INTO competitiematchen(id, competitieploegen_id, datum, uur, wedstrijd, score, user_id, dt_wijz)";
      $dml .= " VALUES (%d, %d, '%s', '%s', '%s', '%s', '%s', '%s')";
      $dml = sprintf($dml, mysql_real_escape_string($row->id)
                         , mysql_real_escape_string($row->competitieploegen_id)
                         , mysql_real_escape_string($row->datum)
                         , mysql_real_escape_string($row->uur)
                         , mysql_real_escape_string($row->wedstrijd)
                         , mysql_real_escape_string($row->score)
                         , mysql_real_escape_string($row->user_id)
                         , mysql_real_escape_string($row->dt_wijz));

      // Sla deze bewerking op in audit tabel
      log_action($_SESSION['usid'], 'DELETE', $competitiematch_id, stripslashes($dml), $conn);

      mysql_free_result($result);
    }
    $delete_stmt = "DELETE FROM competitiematchen WHERE id = %d";
    $sql  = sprintf($delete_stmt, mysql_real_escape_string($competitiematch_id));
    $result = mysql_query($sql, $conn) or die("Invalid query: " . mysql_error());

    // Sla deze bewerking op in audit tabel
    log_action($_SESSION['usid'], 'DELETE', $competitiematch_id, stripslashes($sql), $conn);
  }

/*------------------------------------------------------------------------------------------------------
 | Verwijder alle competitiematchen van de gegeven competitieploeg.
 | Vergeet niet om eerst tabel BESCHIKBAARHEDEN op te kuisen.
 -------------------------------------------------------------------------------------------------------*/

  function delete_competitiematchen($conn, $competitieploegen_id)
  {
    if (LOGGING)
    {
      $query = "SELECT * FROM competitiematchen WHERE competitieploegen_id = %d";
      $sql  = sprintf($query, mysql_real_escape_string($competitieploegen_id));
      $result = mysql_query($sql, $conn) or die("Invalid query: " . mysql_error());

      while ($row = mysql_fetch_object($result))
      {
        $dml = "INSERT INTO competitiematchen ( id, competitieploegen_id, datum, uur, wedstrijd, score, user_id, dt_wijz )
                VALUES (%d, %d, '%s', '%s', '%s', '%s', '%s', '%s')";
        $sql  = sprintf($insert_stmt, mysql_real_escape_string($row->id)
                                    , mysql_real_escape_string($row->competitieploegen_id)
                                    , mysql_real_escape_string($row->datum)
                                    , mysql_real_escape_string($row->uur)
                                    , mysql_real_escape_string($row->wedstrijd)
                                    , mysql_real_escape_string($row->score)
                                    , mysql_real_escape_string($row->user_id)
                                    , mysql_real_escape_string($row->dt_wijz));
        // Sla deze bewerking op in audit tabel
        log_action($_SESSION['usid'], 'DELETE', $competitieploegen_id, stripslashes($sql), $conn);
      }
      mysql_free_result($result);
    }

    $delete_stmt = "DELETE FROM competitiematchen
                     WHERE competitieploegen_id = %d";
    $sql  = sprintf($delete_stmt, mysql_real_escape_string($competitieploegen_id));
    $result = mysql_query($sql, $conn) or die("Invalid query: " . mysql_error());

    // Sla deze bewerking op in audit tabel
    log_action($_SESSION['usid'], 'DELETE', $competitieploegen_id, stripslashes($sql), $conn);
  }

?>