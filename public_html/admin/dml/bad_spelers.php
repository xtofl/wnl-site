<?php
/**
 * bad_spelers.php
 *
 * object     : Methods to do data manipulation on table BAD_SPELERS
 * author     : Freek Ceymeulen
 * created    : 24/05/2006
 *              23/11/2013 Kolom administratief_lid toegevoegd
 **/

/*------------------------------------------------------------------------------------------------------
 | Ga na of iemand al is ingeschreven in de club.
 -------------------------------------------------------------------------------------------------------*/

  function get_speler_id ($conn, $lidnr)
  {
    //mail("freek.ceymeulen@pandora.be", "debug", "inschrijving_exists", "From: webmaster@badmintonsport.be");
    $query = "SELECT id FROM bad_spelers WHERE lidnr = %d";
    $sql  = sprintf($query, mysql_real_escape_string($lidnr, $conn) );
    $result = mysql_query($sql, $conn) or badm_mysql_die();
    if (mysql_num_rows($result) == 0)
    {
      return 0;
    }
    else
    {
      $speler = mysql_fetch_object($result);
      return $speler->id;
    }
  }

/*------------------------------------------------------------------------------------------------------
 | Voeg een nieuwe speler toe
 -------------------------------------------------------------------------------------------------------*/

  function insert_speler($conn, $naam, $voornaam, $geb_dt, $klassement, $geslacht, $lidnr, $type_speler, $straat, $nr, $bus, $postcode, $woonplaats, $email, $tel, $gsm, $club_dt, $eind_dt, $maat, $competitie = null, $betaald = null, $remark, $inschrijving_dt = null, $lidgeld = null, $administratief_lid = 'N')
  {
    global $new_id;

    // Validatie
    $err_msg = "";
    if (strlen($naam) == 0 || strlen($voornaam) == 0)
    {
      $err_msg = "Je bent vergeten een naam in te vullen!";
      return $err_msg;
    }
    // Check of deze speler al niet bestaat in de DB
    $query = "SELECT id
                FROM bad_spelers
               WHERE achternaam = '%s'
                 AND voornaam = '%s'
                 AND woonplaats = '%s'";
    $sql  = sprintf($query, mysql_real_escape_string($naam)
                          , mysql_real_escape_string($voornaam)
                          , mysql_real_escape_string($woonplaats));
    $result = mysql_query($sql, $conn) or die("Invalid query: " . mysql_error());

    if (mysql_num_rows($result) > 0)
    {
      $err_msg .= "Deze persoon bestaat al in de database! Ga na of hij niet op non-actief staat.";
      return $err_msg;
    }

    mysql_free_result($result);

    require_once("../functies/general_functions.php");
/*
    $geb_dt          = (empty($geb_dt))          ? "'".mysql_real_escape_string(get_valid_date($geb_dt))."'" : "NULL";
    $bus             = (empty($bus))             ? " bus ".$bus : "";
    $tel             = (empty($tel))             ? "'".substr($tel, 0, 12)."'" : "NULL";
    $gsm             = (empty($gsm))             ? "'".substr($gsm, 0, 13)."'" : "NULL";
    $club_dt         = (empty($club_dt))         ? "'".mysql_real_escape_string(get_valid_date($club_dt))."'" : "NULL";
    $eind_dt         = (empty($eind_dt))         ? "'".mysql_real_escape_string(get_valid_date($eind_dt))."'" : "NULL";
    $lidnr           = (empty($lidnr))           ? substr(mysql_real_escape_string($lidnr), 0, 8) : "NULL";
    $email           = (empty($email))           ? "'".substr(mysql_real_escape_string($email), 0, 100)."'" : "NULL";
    $lidgeld         = (empty($lidgeld))         ? mysql_real_escape_string($lidgeld) : "NULL";
    $betaald         = (empty($betaald))         ? mysql_real_escape_string($betaald) : "NULL";
    $inschrijving_dt = (empty($inschrijving_dt)) ? "'".mysql_real_escape_string(get_valid_date($inschrijving_dt))."'" : "NULL";
*/
    $geb_dt          = (strlen($geb_dt) > 0)          ? "'".mysql_real_escape_string(get_valid_date($geb_dt))."'" : "NULL";
    $bus             = (strlen($bus) > 0)             ? " bus ".$bus : "";
    $tel             = (strlen($tel) > 0)             ? "'".substr($tel, 0, 12)."'" : "NULL";
    $gsm             = (strlen($gsm) > 0)             ? "'".substr($gsm, 0, 13)."'" : "NULL";
    $club_dt         = (strlen($club_dt) > 0)         ? "'".mysql_real_escape_string(get_valid_date($club_dt))."'" : "NULL";
    $eind_dt         = (strlen($eind_dt) > 0)         ? "'".mysql_real_escape_string(get_valid_date($eind_dt))."'" : "NULL";
    $lidnr           = (strlen($lidnr) > 0)           ? substr(mysql_real_escape_string($lidnr), 0, 8) : "NULL";
    $email           = (strlen($email) > 0)           ? "'".substr(mysql_real_escape_string($email), 0, 100)."'" : "NULL";
    $lidgeld         = (strlen($lidgeld) > 0)         ? mysql_real_escape_string($lidgeld) : "NULL";
    $betaald         = (strlen($betaald) > 0)         ? mysql_real_escape_string($betaald) : "NULL";
    $inschrijving_dt = (strlen($inschrijving_dt) > 0) ? "'".mysql_real_escape_string(get_valid_date($inschrijving_dt))."'" : "NULL";

    $insert_stmt  = "INSERT INTO bad_spelers (naam, achternaam, voornaam, geb_dt, geslacht, klassement, adres, postcode, woonplaats, tel, gsm, club_dt, eind_dt, lidnr, type_speler, email, clubs_id, maat, competitie, lidgeld, betaald, remark, inschrijving_dt, administratief_lid, usid_wijz)";
    $insert_stmt .= "VALUES ('%s', '%s', '%s', %s, '%s', '%s', '%s', '%s', '%s', %s, %s, %s, %s, %d, '%s', %s, %d, '%s', '%s', %d, %d, '%s', %s, '%s', '%s')";
    $sql  = sprintf($insert_stmt, substr(mysql_real_escape_string($voornaam.' '.$naam), 0, 50)
                                , substr(mysql_real_escape_string($naam), 0, 30)
                                , substr(mysql_real_escape_string($voornaam), 0, 20)
                                , $geb_dt
                                , mysql_real_escape_string(strtoupper($geslacht))
                                , mysql_real_escape_string(strtoupper($klassement))
                                , substr(mysql_real_escape_string($straat.' '.$nr.$bus), 0, 50)
                                , substr(mysql_real_escape_string($postcode), 0, 6)
                                , substr(mysql_real_escape_string($woonplaats), 0, 36)
                                , $tel
                                , $gsm
                                , $club_dt
                                , $eind_dt
                                , $lidnr
                                , substr(mysql_real_escape_string(strtoupper($type_speler)), 0, 5)
                                , $email
                                , 1     // clubs_id
                                , substr(mysql_real_escape_string(strtoupper($maat)), 0, 4)
                                , mysql_real_escape_string(strtoupper($competitie))
                                , $lidgeld
                                , $betaald
                                , substr(mysql_real_escape_string($remark), 0, 150)
                                , $inschrijving_dt
                                , substr(strtoupper($administratief_lid), 0, 1)
                                , substr(mysql_real_escape_string($_SESSION['usid']), 0, 10));
    $result = mysql_query($sql, $conn) or die("Invalid query: ".$sql . mysql_error());
    $new_id = mysql_insert_id();

    // Sla deze bewerking op in audit tabel
    log_action($_SESSION['usid'], 'INSERT', $new_id, stripslashes($sql), $conn);

    return $err_msg;
  }

/*------------------------------------------------------------------------------------------------------
 | Wijzig een speler
 -------------------------------------------------------------------------------------------------------*/

  function update_speler($conn, $spelers_id, $achternaam, $voornaam, $geb_dt, $klassement, $geslacht, $lidnr, $type_speler, $adres, $postcode, $woonplaats, $email, $tel, $gsm, $club_dt, $eind_dt, $maat, $competitie, $betaald, $lidgeld, $clubblad, $remark, $administratief_lid)
  {
    // Validatie
    if (strlen($achternaam) == 0)
    {
      return "Je bent vergeten een achternaam in te vullen.";
    }
    if (strlen($voornaam) == 0)
    {
      return "Je bent vergeten een voornaam in te vullen.";
    }

    require_once("../functies/general_functions.php");
/*
    $geb_dt  = (empty($geb_dt))  ? "geb_dt = '".get_valid_date($geb_dt)."'" : "geb_dt = NULL";
    $tel     = (empty($tel))     ? "tel = '".substr($tel, 0, 12)."'" : "tel = NULL";
    $gsm     = (empty($gsm))     ? "gsm = '".substr($gsm, 0, 13)."'" : "gsm = NULL";
    $club_dt = (empty($club_dt)) ? "club_dt = '".get_valid_date($club_dt)."'" : "club_dt = NULL";
    $eind_dt = (empty($eind_dt)) ? "eind_dt = '".get_valid_date($eind_dt)."'" : "eind_dt = NULL";
    $betaald = (empty($betaald)) ? "betaald = ".$betaald : "betaald = NULL";
    $lidgeld = (empty($lidgeld)) ? "lidgeld = ".$lidgeld : "lidgeld = NULL";
    $lidnr   = (empty($lidnr))   ? "lidnr = ".substr($lidnr, 0, 8) : "lidnr = NULL";
*/
    $geb_dt  = (strlen($geb_dt) > 0)  ? "geb_dt = '".get_valid_date($geb_dt)."'" : "geb_dt = NULL";
    $tel     = (strlen($tel) > 0)     ? "tel = '".substr($tel, 0, 12)."'" : "tel = NULL";
    $gsm     = (strlen($gsm) > 0)     ? "gsm = '".substr($gsm, 0, 13)."'" : "gsm = NULL";
    $club_dt = (strlen($club_dt) > 0) ? "club_dt = '".get_valid_date($club_dt)."'" : "club_dt = NULL";
    $eind_dt = (strlen($eind_dt) > 0) ? "eind_dt = '".get_valid_date($eind_dt)."'" : "eind_dt = NULL";
    $betaald = (strlen($betaald) > 0) ? "betaald = ".$betaald : "betaald = NULL";
    $lidgeld = (strlen($lidgeld) > 0) ? "lidgeld = ".$lidgeld : "lidgeld = NULL";
    $lidnr   = (strlen($lidnr) > 0)   ? "lidnr = ".substr($lidnr, 0, 8) : "lidnr = NULL";

    $update_stmt = "UPDATE bad_spelers
                    SET naam = '%s'
                      , achternaam = '%s'
                      , voornaam = '%s'
                      , geslacht = '%s'
                      , adres = %s
                      , postcode = '%s'
                      , woonplaats = %s
                      , %s
                      , maat = '%s'
                      , email = '%s'
                      , %s
                      , %s
                      , %s
                      , klassement = '%s'
                      , type_speler = '%s'
                      , %s
                      , %s
                      , clubblad = '%s'
                      , competitie = '%s'
                      , %s
                      , %s
                      , remark = %s
                      , administratief_lid = '%s'
                      , usid_wijz = '%s'
                    WHERE id = %d";
    $sql  = sprintf($update_stmt, substr(mysql_real_escape_string($voornaam.' '.$achternaam), 0, 50)
                                , substr(mysql_real_escape_string($achternaam), 0, 30)
                                , substr(mysql_real_escape_string($voornaam), 0, 20)
                                , mysql_real_escape_string(strtoupper($geslacht))
                                , substr(quote_smart($adres), 0, 50)
                                , substr(mysql_real_escape_string($postcode), 0, 6)
                                , substr(quote_smart($woonplaats), 0, 36)
                                , $geb_dt
                                , substr(mysql_real_escape_string(strtoupper($maat)), 0, 4)
                                , substr(mysql_real_escape_string($email), 0, 100)
                                , $tel
                                , $gsm
                                , $lidnr
                                , substr(mysql_real_escape_string(strtoupper($klassement)), 0, 2)
                                , substr(mysql_real_escape_string(strtoupper($type_speler)), 0, 5)
                                , $club_dt
                                , $eind_dt
                                , mysql_real_escape_string(strtoupper($clubblad))
                                , mysql_real_escape_string(strtoupper($competitie))
                                , $lidgeld
                                , $betaald
                                , substr(quote_smart($remark), 0, 150)
                                , substr(strtoupper($administratief_lid), 0, 1)
                                , substr(mysql_real_escape_string($_SESSION['usid']), 0, 10)
                                , mysql_real_escape_string($spelers_id));
    $result = mysql_query($sql, $conn) or die("Invalid query: " . mysql_error());

    // Sla deze bewerking op in audit tabel
    log_action($_SESSION['usid'], 'UPDATE', $spelers_id, stripslashes($sql), $conn);

    return "Spelersinfo gewijzigd. ";
  }

?>