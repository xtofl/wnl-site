<?php
/**
 * inschr_spel.php
 *
 * object     : Methods to do data manipulation on table INSCHR_SPEL
 * author     : Freek Ceymeulen
 * created    : 11/06/2007
 **/

 require_once("/home/badmin/public_html/functies/general_functions.php");

/*------------------------------------------------------------------------------------------------------
 | Ga na of iemand al is ingeschreven.
 -------------------------------------------------------------------------------------------------------*/

  function inschrijving_exists ($conn, $tornooi_id, $lidnr)
  {
    //mail("freek.ceymeulen@pandora.be", "debug", "inschrijving_exists", "From: webmaster@badmintonsport.be");
    $query = "SELECT * FROM inschr_spel WHERE inschr_torn_id = %d AND lidnr = %d";
    $sql  = sprintf($query, mysql_real_escape_string($tornooi_id, $conn)
                          , mysql_real_escape_string($lidnr, $conn) );
    $result = mysql_query($sql, $conn) or badm_mysql_die();
    if (mysql_num_rows($result) == 0)
    {
      return false;
    }
    else
    {
      return true;
    }
  }

/*------------------------------------------------------------------------------------------------------
 | Maak een nieuwe inschrijving.
 -------------------------------------------------------------------------------------------------------*/

  function insert_inschrijving($conn, $naam, $lidnr, $klassement, $geb_dt, $discipline1, $discipline2, $discipline3, $reeksE, $enkel, $dubbel, $gemengd, $partner, $klasE, $clubE, $lidnrE, $dub_partner, $reeksD, $klasD, $clubD, $lidnrD, $mix_partner, $reeksG, $klasG, $clubG, $lidnrG, $inschr_torn_id, $dt_verzonden)
  {
    //mail("freek.ceymeulen@pandora.be", "debug", "insert_inschrijving", "From: webmaster@badmintonsport.be");
    global $new_id;

    if (empty($dt_verzonden))
    {
      $dt_verzonden = "NULL";
    }
    else
    {
      $dt_verzonden = "'".get_valid_date($dt_verzonden)."'";
    }
    $insert_stmt  = "INSERT INTO inschr_spel (naam, lidnr, klassement, geb_dt, discipline1, discipline2, discipline3, reeksE, enkel, dubbel, gemengd, partner, klasE, clubE, lidnrE, dub_partner, reeksD, klasD, clubD, lidnrD, mix_partner, reeksG, klasG, clubG, lidnrG, inschr_torn_id, dt_verzonden, dt_creatie, usid_wijz)";
    $insert_stmt .= "     VALUES ('%s',%d,'%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s',%d, %s, NOW(),'%s')";
    $sql  = sprintf($insert_stmt, substr(mysql_real_escape_string(ucwords($naam), $conn), 0, 40)
                                , substr(mysql_real_escape_string($lidnr, $conn), 0, 8)
                                , substr(mysql_real_escape_string($klassement, $conn), 0, 2)
                                , mysql_real_escape_string(get_valid_date($geb_dt), $conn)
                                , substr(mysql_real_escape_string($discipline1, $conn), 0, 7)
                                , substr(mysql_real_escape_string($discipline2, $conn), 0, 7)
                                , substr(mysql_real_escape_string($discipline3, $conn), 0, 7)
                                , substr(mysql_real_escape_string($reeksE, $conn), 0, 2)
                                , substr(mysql_real_escape_string($enkel, $conn), 0, 1)
                                , substr(mysql_real_escape_string($dubbel, $conn), 0, 1)
                                , substr(mysql_real_escape_string($gemengd, $conn), 0, 1)
                                , substr(mysql_real_escape_string(ucwords($partner), $conn), 0, 50)
                                , substr(mysql_real_escape_string($klasE, $conn), 0, 2)
                                , substr(mysql_real_escape_string($clubE, $conn), 0, 30)
                                , substr(mysql_real_escape_string($lidnrE, $conn), 0, 8)
                                , substr(mysql_real_escape_string(ucwords($dub_partner), $conn), 0, 50)
                                , substr(mysql_real_escape_string($reeksD, $conn), 0, 2)
                                , substr(mysql_real_escape_string($klasD, $conn), 0, 2)
                                , substr(mysql_real_escape_string($clubD, $conn), 0, 30)
                                , substr(mysql_real_escape_string($lidnrD, $conn), 0, 8)
                                , substr(mysql_real_escape_string(ucwords($mix_partner), $conn), 0, 50)
                                , substr(mysql_real_escape_string($reeksG, $conn), 0, 2)
                                , substr(mysql_real_escape_string($klasG, $conn), 0, 2)
                                , substr(mysql_real_escape_string($clubG, $conn), 0, 30)
                                , substr(mysql_real_escape_string($lidnrG, $conn), 0, 8)
                                , mysql_real_escape_string($inschr_torn_id, $conn)
                                , $dt_verzonden
                                , substr(mysql_real_escape_string($_SESSION['usid'], $conn), 0, 10));
    //mail("freek.ceymeulen@pandora.be", "debug", $sql, "From: webmaster@badmintonsport.be");
    $result = mysql_query($sql, $conn) or die("Invalid query: " . mysql_error());
    $new_id = mysql_insert_id();

    // Sla deze bewerking op in audit tabel
    //log_action($_SESSION['usid'], 'INSERT', $new_id, stripslashes($sql), $conn);
  }

/*------------------------------------------------------------------------------------------------------
 | Wijzig een inschrijving.
 -------------------------------------------------------------------------------------------------------*/

  function update_inschrijving($conn, $id, $naam, $lidnr, $klassement, $geb_dt, $discipline1, $discipline2, $discipline3, $reeksE, $enkel, $dubbel, $gemengd, $partner, $klasE, $clubE, $lidnrE, $dub_partner, $reeksD, $klasD, $clubD, $lidnrD, $mix_partner, $reeksG, $klasG, $clubG, $lidnrG, $aanvangsuur, $inschr_torn_id, $dt_verzonden)
  {
    if ($aanvangsuur == null)
    {
      $aanvangsuur = "aanvangsuur = aanvangsuur";
    }
    else
    {
      $aanvangsuur = "aanvangsuur = '".substr(mysql_real_escape_string($aanvangsuur, $conn), 0, 10)."'";
    }
    if ($dt_verzonden == null)
    {
      $dt_verzonden = "dt_verzonden = dt_verzonden";
    }
    else
    {
      $dt_verzonden = "dt_verzonden = '".substr(mysql_real_escape_string(get_valid_date($dt_verzonden), $conn), 0, 10)."'";
    }

    $update_stmt = "UPDATE inschr_spel
                       SET naam = '%s'
                         , lidnr = %d
                         , klassement = '%s'
                         , geb_dt = '%s'
                         , discipline1 = '%s'
                         , discipline2 = '%s'
                         , discipline3 = '%s'
                         , reeksE = '%s'
                         , enkel = '%s'
                         , dubbel = '%s'
                         , gemengd = '%s'
                         , partner = '%s'
                         , klasE = '%s'
                         , clubE = '%s'
                         , lidnrE = '%s'
                         , dub_partner = '%s'
                         , reeksD = '%s'
                         , klasD = '%s'
                         , clubD = '%s'
                         , lidnrD = '%s'
                         , mix_partner = '%s'
                         , reeksG = '%s'
                         , klasG = '%s'
                         , clubG = '%s'
                         , lidnrG = '%s'
                         , %s
                         , inschr_torn_id = %d
                         , %s
                         , usid_wijz = '%s'
                         , dt_wijz = NOW()
                     WHERE id = %d";
    $sql  = sprintf($update_stmt, substr(mysql_real_escape_string(ucwords($naam), $conn), 0, 40)
                                , substr(mysql_real_escape_string($lidnr, $conn), 0, 8)
                                , substr(mysql_real_escape_string($klassement, $conn), 0, 2)
                                , mysql_real_escape_string(get_valid_date($geb_dt), $conn)
                                , substr(mysql_real_escape_string($discipline1, $conn), 0, 7)
                                , substr(mysql_real_escape_string($discipline2, $conn), 0, 7)
                                , substr(mysql_real_escape_string($discipline3, $conn), 0, 7)
                                , substr(mysql_real_escape_string($reeksE, $conn), 0, 2)
                                , substr(mysql_real_escape_string($enkel, $conn), 0, 1)
                                , substr(mysql_real_escape_string($dubbel, $conn), 0, 1)
                                , substr(mysql_real_escape_string($gemengd, $conn), 0, 1)
                                , substr(mysql_real_escape_string(ucwords($partner), $conn), 0, 50)
                                , substr(mysql_real_escape_string($klasE, $conn), 0, 2)
                                , substr(mysql_real_escape_string($clubE, $conn), 0, 30)
                                , substr(mysql_real_escape_string($lidnrE, $conn), 0, 8)
                                , substr(mysql_real_escape_string(ucwords($dub_partner), $conn), 0, 50)
                                , substr(mysql_real_escape_string($reeksD, $conn), 0, 2)
                                , substr(mysql_real_escape_string($klasD, $conn), 0, 2)
                                , substr(mysql_real_escape_string($clubD, $conn), 0, 30)
                                , substr(mysql_real_escape_string($lidnrD, $conn), 0, 8)
                                , substr(mysql_real_escape_string(ucwords($mix_partner), $conn), 0, 50)
                                , substr(mysql_real_escape_string($reeksG, $conn), 0, 2)
                                , substr(mysql_real_escape_string($klasG, $conn), 0, 2)
                                , substr(mysql_real_escape_string($clubG, $conn), 0, 30)
                                , substr(mysql_real_escape_string($lidnrG, $conn), 0, 8)
                                , $aanvangsuur
                                , mysql_real_escape_string($inschr_torn_id, $conn)
                                , $dt_verzonden
                                , substr(mysql_real_escape_string($_SESSION['usid'], $conn), 0, 10)
                                , mysql_real_escape_string($id, $conn));
    $result = mysql_query($sql, $conn) or die("Invalid query: " . mysql_error());

    // Sla deze bewerking op in audit tabel
    //log_action($_SESSION['usid'], 'UPDATE', $id, stripslashes($sql), $conn);
  }

/*------------------------------------------------------------------------------------------------------
 | Verwijder een inschrijving.
 -------------------------------------------------------------------------------------------------------*/

  function delete_inschrijving($conn, $id)
  {
    $delete_stmt = "DELETE FROM inschr_spel
                     WHERE id = %d";
    $sql  = sprintf($delete_stmt, mysql_real_escape_string($id, $conn));
    $result = mysql_query($sql, $conn) or die("Invalid query: " . mysql_error());

    // Sla deze bewerking op in audit tabel
    //log_action($_SESSION['usid'], 'DELETE', $id, stripslashes($sql), $conn);
  }

?>