<?php
/**
 * leden_functies.php
 *
 * object     : Methods to do data manipulation on table LEDEN_FUNCTIES
 * author     : Freek Ceymeulen
 * created    : 24/05/2006
 **/

/*------------------------------------------------------------------------------------------------------
 | Voeg een nieuw record toe in tabel LEDEN_FUNCTIES
 -------------------------------------------------------------------------------------------------------*/

  function insert_leden_functies($conn, $spelers_id, $functies_id, $ref_id)
  {
    $insert_stmt  = "INSERT INTO leden_functies (spelers_id, functies_id, ref_id, usid_wijz)";
    $insert_stmt .= "     VALUES ('%d','%d','%d','%s')";
    $sql  = sprintf($insert_stmt, mysql_real_escape_string($spelers_id)
                                , mysql_real_escape_string($functies_id)
                                , mysql_real_escape_string($ref_id)
                                , mysql_real_escape_string($_SESSION['usid']));
    $result = mysql_query($sql, $conn) or die("Invalid query: " . mysql_error());
    $new_id = mysql_insert_id();

    // Sla deze bewerking op in audit tabel
    log_action($_SESSION['usid'], 'INSERT', $new_id, stripslashes($sql), $conn);
  }

/*------------------------------------------------------------------------------------------------------
 | Ken een bestaande functie toe aan een nieuwe persoon
 -------------------------------------------------------------------------------------------------------*/

  function update_leden_functies($conn, $spelers_id, $functies_id, $ref_id)
  {
    $update_stmt = "UPDATE leden_functies
                       SET spelers_id = %d
                     WHERE functies_id = %d
                       AND ref_id = %d";
    $sql  = sprintf($update_stmt, mysql_real_escape_string($spelers_id)
                                , mysql_real_escape_string($functies_id)
                                , mysql_real_escape_string($ref_id));
    $result = mysql_query($sql, $conn) or die("Invalid query: " . mysql_error());

    // Sla deze bewerking op in audit tabel
    log_action($_SESSION['usid'], 'UPDATE', 0, stripslashes($sql), $conn);
  }

/*------------------------------------------------------------------------------------------------------
 | Verwijder het gegeven record uit tabel LEDEN_FUNCTIES
 -------------------------------------------------------------------------------------------------------*/

  function delete_leden_functies($conn, $spelers_id, $functies_id, $ref_id)
  {
    $delete_stmt = "DELETE FROM leden_functies
                    WHERE spelers_id = %d
                      AND functies_id = %d
                      AND ref_id = %d";
    $sql  = sprintf($delete_stmt, mysql_real_escape_string($spelers_id)
                                , mysql_real_escape_string($functies_id)
                                , mysql_real_escape_string($ref_id));
    $result = mysql_query($sql, $conn) or die("Invalid query: " . mysql_error());

    // Sla deze bewerking op in audit tabel
    log_action($_SESSION['usid'], 'DELETE', 0, stripslashes($sql), $conn);
  }

/*------------------------------------------------------------------------------------------------------
 | Ga na of een functie bestaat voor een gegeven referentie
 -------------------------------------------------------------------------------------------------------*/

  function functie_exists($conn, $functies_id, $ref_id)
  {
    $query = "SELECT spelers_id
                FROM leden_functies
               WHERE functies_id = %d
                 AND ref_id = %d";
    $sql  = sprintf($query, mysql_real_escape_string($functies_id)
                          , mysql_real_escape_string($ref_id));
    $result = mysql_query($sql, $conn) or badm_mysql_die();

    return (mysql_num_rows($result) == 0) ? false : true;
  }

?>