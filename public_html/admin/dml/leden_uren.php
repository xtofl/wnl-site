<?php
/**
 * leden_uren.php
 *
 * object     : Methods to do data manipulation on table LEDEN_UREN
 * author     : Freek Ceymeulen
 * created    : 26/05/2006
 **/

/*------------------------------------------------------------------------------------------------------
 | Voeg een nieuw record toe in tabel LEDEN_UREN
 -------------------------------------------------------------------------------------------------------*/

  function insert_leden_uren($conn, $spelers_id, $speeluren_id, $reserve = 'N')
  {
    $insert_stmt = "INSERT INTO leden_uren (spelers_id, speeluren_id, reserve, user_id, dt_wijz)
                    VALUES (%d, %d, '%s', '%s', NOW())";
    $sql  = sprintf($insert_stmt, mysql_real_escape_string($spelers_id)
                                , mysql_real_escape_string($speeluren_id)
                                , mysql_real_escape_string($reserve)
                                , substr(mysql_real_escape_string($_SESSION['usid']), 0, 10));
    $result = mysql_query($sql, $conn) or die("Invalid query: " . mysql_error());

    // Sla deze bewerking op in audit tabel
    log_action($_SESSION['usid'], 'INSERT', $spelers_id, stripslashes($sql), $conn);
  }

/*------------------------------------------------------------------------------------------------------
 | Verwijder alle records uit tabel LEDEN_UREN voor de gegeven speler
 -------------------------------------------------------------------------------------------------------*/

  function delete_leden_uren($conn, $spelers_id)
  {
    if (LOGGING)
    {
      // Selecteer de te deleten records
      $query = "SELECT spelers_id
                     , speeluren_id
                     , user_id
                     , dt_wijz
                 FROM leden_uren
                WHERE spelers_id = %d";
      $sql  = sprintf($query, mysql_real_escape_string($spelers_id));
      $result = mysql_query($sql, $conn) or die("Invalid query: " . mysql_error());

      while ($row = mysql_fetch_object($result))
      {
        // Maak backup van de te deleten records
        $dml = "INSERT INTO leden_uren (spelers_id, speeluren_id, user_id, dt_wijz)";
        $dml .= " VALUES (%d, %d, '%s', '%s')";
        $sql  = sprintf($dml, mysql_real_escape_string($spelers_id)
                            , mysql_real_escape_string($row->speeluren_id)
                            , mysql_real_escape_string($row->user_id)
                            , mysql_real_escape_string($row->dt_wijz));
        // Sla deze bewerking op in audit tabel
        log_action($_SESSION['usid'], 'DELETE', $spelers_id, stripslashes($sql), $conn);
      }
      mysql_free_result($result);
    }

    // Delete alle records in tabel leden_uren voor deze speler
    $delete_stmt = "DELETE FROM leden_uren
                     WHERE spelers_id = %d";
    $sql  = sprintf($delete_stmt, mysql_real_escape_string($spelers_id));
    $result = mysql_query($sql, $conn) or die("Invalid query: " . mysql_error());

    // Sla deze bewerking op in audit tabel
    log_action($_SESSION['usid'], 'DELETE', $spelers_id, stripslashes($sql), $conn);
  }

/*------------------------------------------------------------------------------------------------------
 | Verwijder alle records uit tabel LEDEN_UREN voor het gegeven speeluur
 -------------------------------------------------------------------------------------------------------*/

  function delete_uren_leden($conn, $speeluur_id)
  {
    if (LOGGING)
    {
      // Selecteer de te deleten records
      $query = "SELECT spelers_id
                     , speeluren_id
                     , user_id
                     , dt_wijz
                 FROM leden_uren
                WHERE speeluren_id = %d";
      $sql  = sprintf($query, mysql_real_escape_string($speeluur_id));
      $result = mysql_query($sql, $conn) or die("Invalid query: " . mysql_error());

      while ($row = mysql_fetch_object($result))
      {
        // Maak backup van de te deleten records
        $dml = "INSERT INTO leden_uren (spelers_id, speeluren_id, user_id, dt_wijz)";
        $dml .= " VALUES (%d, %d, '%s', '%s')";
        $sql  = sprintf($dml, mysql_real_escape_string($row->spelers_id)
                            , mysql_real_escape_string($speeluur_id)
                            , mysql_real_escape_string($row->user_id)
                            , mysql_real_escape_string($row->dt_wijz));
        // Sla deze bewerking op in audit tabel
        log_action($_SESSION['usid'], 'DELETE', $speeluur_id, stripslashes($sql), $conn);
      }
      mysql_free_result($result);
    }

    // Delete alle records in tabel leden_uren voor deze speler
    $delete_stmt = "DELETE FROM leden_uren
                     WHERE speeluren_id = %d";
    $sql  = sprintf($delete_stmt, mysql_real_escape_string($speeluur_id));
    $result = mysql_query($sql, $conn) or die("Invalid query: " . mysql_error());

    // Sla deze bewerking op in audit tabel
    log_action($_SESSION['usid'], 'DELETE', $speeluur_id, stripslashes($sql), $conn);
  }

?>