<?php
/**
 * stock_items.php
 *
 * object     : Methods to do data manipulation on table STOCK_ITEMS
 * author     : Freek Ceymeulen
 * created    : 29/05/2006
 **/

/*------------------------------------------------------------------------------------------------------
 | Voeg een nieuw item toe
 -------------------------------------------------------------------------------------------------------*/

  function insert_stock_item($conn, $name, $location, $quantity, $price, $description)
  {
    global $new_id;

    // Validatie
    if (strlen($name) == 0)
    {
      return "Je bent vergeten een naam in te vullen!";
    }

    $location = (strlen($location) == 0) ? "null" : "'".substr(mysql_real_escape_string($location), 0, 64)."'";

    $insert_stmt  = "INSERT INTO stock_items (name, location, quantity, price, description, usid_wijz)";
    $insert_stmt .= "VALUES ('%s', %s, %d, %d, '%s', '%s')";
    $sql  = sprintf($insert_stmt, substr(mysql_real_escape_string($name), 0, 64)
                                , $location
                                , substr(mysql_real_escape_string($quantity), 0, 10)
                                , substr(mysql_real_escape_string($price), 0, 10)
                                , substr(mysql_real_escape_string($description), 0, 256)
                                , substr(mysql_real_escape_string($_SESSION['usid']), 0, 10));
    $result = mysql_query($sql, $conn) or die("Invalid query: ".$sql . mysql_error());
    $new_id = mysql_insert_id();

    // Sla deze bewerking op in audit tabel
    log_action($_SESSION['usid'], 'INSERT', $new_id, stripslashes($sql), $conn);

    return "";
  }

/*------------------------------------------------------------------------------------------------------
 | Wijzig een item
 -------------------------------------------------------------------------------------------------------*/

  function update_stock_item($conn, $item_id, $name, $location, $quantity, $price, $description)
  {
    // Validatie
    if (strlen($name) == 0)
    {
      return "Je bent vergeten een naam in te vullen.";
    }

    $location = (strlen($location) == 0) ? "location = null" : "location = '".substr(mysql_real_escape_string($location), 0, 64)."'";

    $update_stmt = "UPDATE stock_items
                    SET name = '%s'
                      , %s
                      , quantity = '%s'
                      , price = '%s'
                      , description = '%s'
                      , usid_wijz = '%s'
                    WHERE id = %d";
    $sql  = sprintf($update_stmt, substr(mysql_real_escape_string($name), 0, 64)
                                , $location
                                , substr(mysql_real_escape_string($quantity), 0, 10)
                                , substr(mysql_real_escape_string($price), 0, 10)
                                , substr(mysql_real_escape_string($description), 0, 256)
                                , substr(mysql_real_escape_string($_SESSION['usid']), 0, 10)
                                , mysql_real_escape_string($item_id));
    $result = mysql_query($sql, $conn) or die("Invalid query: " . mysql_error());

    // Sla deze bewerking op in audit tabel
    log_action($_SESSION['usid'], 'UPDATE', $item_id, stripslashes($sql), $conn);

    return "";
  }

/*------------------------------------------------------------------------------------------------------
 | Verwijder het gegeven item
 -------------------------------------------------------------------------------------------------------*/

  function delete_stock_item($conn, $id)
  {
    $delete_stmt = "DELETE FROM stock_items
                     WHERE id = %d";
    $sql  = sprintf($delete_stmt, mysql_real_escape_string($id));
    $result = mysql_query($sql, $conn) or die("Invalid query: " . mysql_error());

    // Sla deze bewerking op in audit tabel
    log_action($_SESSION['usid'], 'DELETE', $id, stripslashes($sql), $conn);
  }

?>