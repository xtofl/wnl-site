<?php
/**
 * codes.php
 *
 * object     : Methods to do data manipulation on table CODES
 * author     : Freek Ceymeulen
 * created    : 24/05/2006
 **/
 
 /*------------------------------------------------------------------------------------------------------
 | Voeg een nieuwe code toe
 -------------------------------------------------------------------------------------------------------*/

  function insert_code($conn, $domain, $value, $description)
  {
    $insert_stmt  = "INSERT INTO codes (domain, value, description, usid_wijz)";
    $insert_stmt .= "     VALUES ('%s','%s','%s','%s')";
    $sql  = sprintf($insert_stmt, substr(mysql_real_escape_string($domain), 0, 20)
                                , substr(mysql_real_escape_string($value), 0, 25)
                                , substr(mysql_real_escape_string($description), 0, 50)
                                , substr(mysql_real_escape_string($_SESSION['usid']), 0, 10) );
    $result = mysql_query($sql, $conn) or die("Invalid query: " . mysql_error());
    $new_id = mysql_insert_id();

     // Sla deze bewerking op in audit tabel
    log_action($_SESSION['usid'], 'INSERT', $new_id, stripslashes($sql), $conn);
  }

?>