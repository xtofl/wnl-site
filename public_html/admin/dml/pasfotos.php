<?php
/**
 * pasfotos.php
 *
 * object     : Methods to do data manipulation on table PASFOTOS
 * author     : Freek Ceymeulen
 * created    : 26/05/2006
 **/

/*------------------------------------------------------------------------------------------------------
 | Voeg een nieuwe foto toe
 -------------------------------------------------------------------------------------------------------*/

  function insert_pasfoto($conn, $spelers_id)
  {
    // Ga na of er een pasfoto geupload is
    if (is_uploaded_file($_FILES['pasfoto']['tmp_name']))
    {
      // Check file
      $mime = $_FILES['pasfoto']['type'];
      if ($mime=="image/jpeg" || $mime=="image/pjpeg" || $mime=="image/gif" || $mime=="image/x-png" || $mime=="image/png" || $mime=="image/bmp" || $mime=="image/x-MS-bmp")
      {
        $naam = $_FILES['pasfoto']['name'];
        $array = explode(".", $naam);
        $nr    = count($array);
        $ext  = strtolower($array[$nr-1]);
        if($ext=="jpg" || $ext=="jpeg" || $ext=="gif" || $ext=="png" || $ext=="bmp")
        {
          $data = addslashes(file_get_contents($_FILES['pasfoto']['tmp_name']));

          // Ga na of er al een foto bestond voor deze speler
          $sql = "SELECT COUNT(*) AS aantal FROM pasfotos WHERE spelers_id = %d";
          $query = sprintf($sql, $spelers_id);
          $result = mysql_query($query, $conn);
          $row = mysql_fetch_row($result);

          if ($row[0] == 0)
          {
            $sql = "INSERT INTO pasfotos (data, naam, file_type, file_size, spelers_id, usid_wijz)";
            $sql .= "VALUES ('".$data."', '".$naam."', '".$mime."', ".$_FILES['pasfoto']['size'].", ".$spelers_id.", '".$_SESSION['usid']."')";
            $err_msg = "Pasfoto toegevoegd. ";
          }
          else
          {
            $sql = "UPDATE pasfotos SET naam = '".$naam."'";
            $sql .= ", data = '".$data."'";
            $sql .= ", file_type = '".$mime."'";
            $sql .= ", file_size = ".$_FILES['pasfoto']['size'];
            $sql .= ", usid_wijz = '".$_SESSION['usid']."'";
            $sql .= " WHERE spelers_id = ".$spelers_id;
            $err_msg = "Pasfoto gewijzigd. ";
          }
          $result = mysql_query($sql, $conn);
        }
        else
          return "Error uploading pasfoto: extensie = ".$ext;
      }
      else
        return "Error uploading pasfoto: mime = ".$mime;
    }
    else
    {
      $err_msg = array( 0 => "There is no error, the file uploaded with success. "
                      , 1 => "The uploaded file exceeds the upload_max_filesize directive in php.ini. "
                      , 2 => "The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form. "
                      , 3 => "The uploaded file was only partially uploaded. "
                      , 4 => "No file was uploaded. "
                      , 6 => "Missing a temporary folder. " );
      return "Error uploading pasfoto: ".$err_msg[$_FILES['error']];
    }
    return $err_msg;
  }

?>