<?php
/**
 * beschikbaarheden.php
 *
 * object     : Methods to do data manipulation on table BESCHIKBAARHEDEN
 * author     : Freek Ceymeulen
 * created    : 24/05/2006
 **/

/*------------------------------------------------------------------------------------------------------
 | Voeg een beschikbaarheid toe voor de gegeven match en de gegeven speler
 -------------------------------------------------------------------------------------------------------*/

  function insert_beschikbaarheden($conn, $competitiematchen_id, $spelers_id, $beschikbaarheid)
  {
    $insert_stmt = "INSERT INTO beschikbaarheden ( competitiematchen_id, spelers_id, beschikbaarheid )
                    VALUES ( %d, %d, '%s' )";
    $sql  = sprintf($insert_stmt, mysql_real_escape_string($competitiematchen_id)
                                , mysql_real_escape_string($spelers_id)
                                , mysql_real_escape_string($beschikbaarheid));
    $result = mysql_query($sql, $conn) or die("Invalid query: " . mysql_error());
    $new_id = mysql_insert_id();

    // Sla deze bewerking op in audit tabel
    log_action($_SESSION['usid'], 'INSERT', $new_id, stripslashes($sql), $conn);
  }

/*------------------------------------------------------------------------------------------------------
 | Verwijder alle beschikbaarheden voor de gegeven competitiematch
 -------------------------------------------------------------------------------------------------------*/

  function delete_beschikbaarheid($conn, $competitiematchen_id)
  {
    if (LOGGING)
    {
      $query = "SELECT *
                  FROM beschikbaarheden
                 WHERE competitiematchen_id = %d";
      $sql  = sprintf($query, mysql_real_escape_string($competitiematchen_id));
      $result = mysql_query($sql, $conn) or die("Invalid query: " . mysql_error());

      while ($row = mysql_fetch_object($result))
      {
        $dml = "INSERT INTO beschikbaarheden ( competitiematchen_id, spelers_id, beschikbaarheid )
                VALUES (%d, %d, '%s')";
        $sql  = sprintf($insert_stmt, mysql_real_escape_string($competitiematchen_id)
                                    , mysql_real_escape_string($row->spelers_id)
                                    , mysql_real_escape_string($row->beschikbaarheid));
        // Sla deze bewerking op in audit tabel
        log_action($_SESSION['usid'], 'DELETE', $competitiematchen_id, stripslashes($sql), $conn);
      }
      mysql_free_result($result);
    }

    $delete_stmt = "DELETE FROM beschikbaarheden
                     WHERE competitiematchen_id = %d";
    $sql  = sprintf($delete_stmt, mysql_real_escape_string($competitiematchen_id));
    $result = mysql_query($sql, $conn) or die("Invalid query: " . mysql_error());

    // Sla deze bewerking op in audit tabel
    log_action($_SESSION['usid'], 'DELETE', $competitiematchen_id, stripslashes($sql), $conn);
  }

/*------------------------------------------------------------------------------------------------------
 | Verwijder alle beschikbaarheden voor alle competitiematchen van de gegeven competitieploeg
 -------------------------------------------------------------------------------------------------------*/

  function delete_beschikbaarheden($conn, $competitieploegen_id)
  {
/* schijnt niet te werken in huidige versie van MySQL
        $delete_stmt = "DELETE FROM beschikbaarheden
                         WHERE competitiematchen_id IN ( SELECT id
                                                           FROM competitiematchen
                                                          WHERE competitieploegen_id = %d";
*/
    $query = "SELECT id
                FROM competitiematchen
               WHERE competitieploegen_id = %d";
    $sql  = sprintf($query, mysql_real_escape_string($competitieploegen_id));
    $result1 = mysql_query($sql, $conn) or die("Invalid query: " . mysql_error());

    while ($row = mysql_fetch_object($result1))
    {
      delete_beschikbaarheid($conn, $row->id);
    }
    mysql_free_result($result1);
  }

?>