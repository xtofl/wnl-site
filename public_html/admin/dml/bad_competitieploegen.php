<?php
/**
 * bad_competitieploegen.php
 *
 * object     : Methods to do data manipulation on table BAD_COMPETITIEPLOEGEN
 * author     : Freek Ceymeulen
 * created    : 24/05/2006
 * modified   : 20/08/2006 Field "Seizoen" added to table BAD_COMPETITIEPLOEGEN
 **/

/*------------------------------------------------------------------------------------------------------
 | Maak een nieuwe competitieploeg.
 -------------------------------------------------------------------------------------------------------*/

  function insert_competitieploeg($conn, $seizoen, $type, $ploegnummer, $afdeling, $sporthal, $uur)
  {
    global $new_id;

    // Validatie
    $err_msg = "";
    if (strlen($type) > 0)
    {
      // Ga na of in deze reeks nog geen ploeg bestaat met de opgegeven ploegnummer en of de gegeven ploegnummer
      // de eerstvolgende is
      $query = "SELECT ifnull(MAX(c.ploegnummer), 0) + 1 AS ploegnummer
                  FROM bad_competitieploegen c
                 WHERE c.seizoen = '%s'
				   AND c.type = '%s'";
      $sql  = sprintf($query, mysql_real_escape_string($seizoen)
	                        , mysql_real_escape_string($type));
      $result = mysql_query($sql, $conn) or badm_mysql_die();
      $row = mysql_fetch_object($result);
      $ploegnr = $row->ploegnummer;
      if (($ploegnr - $ploegnummer) != 0)
      {
        $err_msg = "De opgegeven ploegnummer ".$ploegnummer."is niet de eerstvolgende voor de ".$type." reeks.";
        log_action($_SESSION['usid'], 'INSERT', 0, 'Error inserting nieuwe competitieploeg: '.$err_msg, $conn);
        return $err_msg;
      }
      mysql_free_result($result);
    }
    $insert_stmt  = "INSERT INTO bad_competitieploegen (seizoen, type, ploegnummer, afdeling, sporthal, uur, user_id, dt_wijz)";
    $insert_stmt .= "     VALUES ('%s','%s',%d,'%s','%s','%s','%s', NOW())";
    $sql  = sprintf($insert_stmt, substr(mysql_real_escape_string($seizoen), 0, 9)
	                            , mysql_real_escape_string($type)
                                , substr(mysql_real_escape_string($ploegnummer), 0, 2)
                                , substr(mysql_real_escape_string($afdeling), 0, 40)
                                , substr(mysql_real_escape_string($sporthal), 0, 40)
                                , substr(mysql_real_escape_string($uur), 0, 10)
                                , substr(mysql_real_escape_string($_SESSION['usid']), 0, 10));
    $result = mysql_query($sql, $conn) or die("Invalid query: " . mysql_error());
    $new_id = mysql_insert_id();

    // Sla deze bewerking op in audit tabel
    log_action($_SESSION['usid'], 'INSERT', $new_id, stripslashes($sql), $conn);

    return $err_msg;
  }

/*------------------------------------------------------------------------------------------------------
 | Wijzig een competitieploeg.
 -------------------------------------------------------------------------------------------------------*/

  function update_competitieploeg($conn, $competitieploeg_id, $type, $sporthal, $uur)
  {
    $update_stmt = "UPDATE bad_competitieploegen
                       SET afdeling = '%s'
                         , uur = '%s'
                         , sporthal = '%s'
                         , user_id = '%s'
                         , dt_wijz = NOW()
                     WHERE id = %d";
    $sql  = sprintf($update_stmt, mysql_real_escape_string($type)
                                , substr(mysql_real_escape_string($uur), 0, 10)
                                , substr(mysql_real_escape_string($sporthal), 0, 40)
                                , substr(mysql_real_escape_string($_SESSION['usid']), 0, 10)
                                , mysql_real_escape_string($competitieploeg_id));
    $result = mysql_query($sql, $conn) or die("Invalid query: " . mysql_error());

    // Sla deze bewerking op in audit tabel
    log_action($_SESSION['usid'], 'UPDATE', $_POST['id'], stripslashes($sql), $conn);
  }

/*------------------------------------------------------------------------------------------------------
 | Verwijder de gegeven competitieploeg.
 | Vergeet niet om eerst tabel LEDEN_COMPETITIE, COMPETITIEMATCHEN en BESCHIKBAARHEDEN op te kuisen.
 -------------------------------------------------------------------------------------------------------*/

  function delete_competitieploeg($conn, $competitieploeg_id)
  {
    if (LOGGING)
    {
      $query = "SELECT *
                  FROM bad_competitieploegen
                 WHERE id = %d";
      $sql  = sprintf($query, mysql_real_escape_string($competitieploeg_id));
      $result = mysql_query($sql, $conn) or die("Invalid query: " . mysql_error());

      $row = mysql_fetch_object($result);
      $dml = "INSERT INTO bad_competitieploegen(id, seizoen, type, ploegnummer, afdeling, sporthal, uur, user_id, dt_wijz)
              VALUES (%d, '%s', '%s', '%s', '%s', %d, '%s', '%s', '%s', '%s')";
      $dml  = sprintf($dml, mysql_real_escape_string($row->id)
                          , mysql_real_escape_string($row->seizoen)
						  , mysql_real_escape_string($row->type)
                          , mysql_real_escape_string($row->ploegnummer)
                          , mysql_real_escape_string($row->afdeling)
                          , mysql_real_escape_string($row->sporthal)
                          , mysql_real_escape_string($row->uur)
                          , mysql_real_escape_string($row->user_id)
                          , mysql_real_escape_string($row->dt_wijz));

      // Sla deze bewerking op in audit tabel
      log_action($_SESSION['usid'], 'DELETE', $competitieploeg_id, stripslashes($dml), $conn);
    }

    $delete_stmt = "DELETE FROM bad_competitieploegen
                     WHERE id = %d";
    $sql  = sprintf($delete_stmt, mysql_real_escape_string($competitieploeg_id));
    $result = mysql_query($sql, $conn) or die("Invalid query: " . mysql_error());

    // Sla deze bewerking op in audit tabel
    log_action($_SESSION['usid'], 'DELETE', $competitieploeg_id, stripslashes($sql), $conn);

    mysql_free_result($result);
  }

?>