<?php
/**
 * images.php
 *
 * object     : Methods to do data manipulation on table IMAGES
 * author     : Freek Ceymeulen
 * created    : 29/05/2006
 **/

  define("MAXSIZE", 15360000); // max size of MEDIUMBLOB = 16Mb

/*------------------------------------------------------------------------------------------------------
 | Voeg een of meer nieuwe foto's toe
 -------------------------------------------------------------------------------------------------------*/

  function insert_images($conn, $ref_id, $category)
  {
    for ($i=0; $i<count($_FILES['image']['name']); $i++)
    {
      if (strlen($_FILES['image']['name'][$i]) > 0)
      {
        // Ga na of er een pasfoto geupload is
        if (is_uploaded_file($_FILES['image']['tmp_name'][$i]))
        {
          // Check the file is less than the maximum file size
          $size = $_FILES['image']['size'][$i];
          if($size < MAXSIZE)
          {
            $naam = $_FILES['image']['name'][$i];
            $array = explode(".", $naam);
            $nr    = count($array);
            $ext  = strtolower($array[$nr-1]);
            if ($ext=="jpg" || $ext=="jpeg" || $ext=="gif" || $ext=="png" || $ext=="bmp")
            {
              $data = addslashes(file_get_contents($_FILES['image']['tmp_name'][$i]));
              $mime = $_FILES['image']['type'][$i];
              list($width, $height, $type, $attr) = getimagesize($_FILES['image']['tmp_name'][$i]);

              $sql = "INSERT INTO images (ref_id, category, name, data, mime_type, image_type, size, width, height, attributes, usid_wijz)";
              $sql .= "VALUES ('".$ref_id."', '".substr($category, 0, 16)."', '".substr($naam, 0, 64)."', '".$data."', '".$mime."', '".$type."', ".$size.", ".$width.", ".$height.", '".$attr."', '".$_SESSION['usid']."')";
              $result = mysql_query($sql, $conn);
            }
            else
              return "Error uploading image: unsupported file extension: .".$ext;
          }
          else
            return "Error uploading image: image size (".$size.") is too big (max ".MAXSIZE.")";
        }
        else
        {
          $err_msg = array( 0 => "There is no error, the file uploaded with success. "
                          , 1 => "The uploaded file exceeds the upload_max_filesize directive in php.ini. "
                          , 2 => "The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form. "
                          , 3 => "The uploaded file was only partially uploaded. "
                          , 4 => "No file was uploaded. "
                          , 6 => "Missing a temporary folder. " );
          return "Error uploading image: ".$err_msg[$_FILES['image']['error'][$i]];
        }
      }
    }
    return "";
  }

/*------------------------------------------------------------------------------------------------------
 | Verwijder de gegeven image
 -------------------------------------------------------------------------------------------------------*/

  function delete_image($conn, $id)
  {
    $delete_stmt = "DELETE FROM images
                     WHERE id = %d";
    $sql  = sprintf($delete_stmt, mysql_real_escape_string($id));
    $result = mysql_query($sql, $conn) or die("Invalid query: " . mysql_error());

    // Sla deze bewerking op in audit tabel
    log_action($_SESSION['usid'], 'DELETE', $id, stripslashes($sql), $conn);
  }

?>