<?php
/**
 * bad_speeluren.php
 *
 * object     : Methods to do data manipulation on table BAD_SPEELUREN
 * author     : Freek Ceymeulen
 * created    : 24/05/2006
 *              02/11/2012 aantal_plaatsen, aantal_reserve en prijs toegevoegd
 **/

/*------------------------------------------------------------------------------------------------------
 | Maak een nieuw speeluur
 -------------------------------------------------------------------------------------------------------*/

  function insert_speeluur($conn, $plaats, $dag, $uur, $unit, $doelgroep, $type, $beschrijving, $aantal_plaatsen = 0, $aantal_reserve = 0, $prijs = 0)
  {
    $insert_stmt  = "INSERT INTO bad_speeluren (plaats, dag, uur, unit, doelgroep, type, beschrijving, aantal_plaatsen, aantal_reserve, prijs, user_id, dt_wijz)";
    $insert_stmt .= "     VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s', %d, %d, %d, '%s', NOW())";
    $sql  = sprintf($insert_stmt, mysql_real_escape_string($plaats)
                                , mysql_real_escape_string($dag)
                                , substr(mysql_real_escape_string($uur), 0, 10)
                                , mysql_real_escape_string($unit)
                                , mysql_real_escape_string($doelgroep)
                                , mysql_real_escape_string($type)
                                , mysql_real_escape_string($beschrijving)
								        , mysql_real_escape_string($aantal_plaatsen)
								        , mysql_real_escape_string($aantal_reserve)
								        , mysql_real_escape_string($prijs)
                                , substr(mysql_real_escape_string($_SESSION['usid']), 0, 10));
    $result = mysql_query($sql, $conn) or die("Invalid query: " . mysql_error());
    $new_id = mysql_insert_id();

    // Sla deze bewerking op in audit tabel
    log_action($_SESSION['usid'], 'INSERT', $new_id, stripslashes($sql), $conn);

    return $new_id;
  }

/*------------------------------------------------------------------------------------------------------
 | Wijzig het gegeven speeluur
 -------------------------------------------------------------------------------------------------------*/

  function update_speeluur($conn, $speeluur_id, $plaats, $dag, $uur, $unit, $doelgroep, $type, $beschrijving, $aantal_plaatsen, $aantal_reserve, $prijs)
  {
    $update_stmt = "UPDATE bad_speeluren
                       SET plaats = '%s'
                         , dag = '%s'
                         , uur = '%s'
                         , unit = '%s'
                         , doelgroep = '%s'
                         , type = '%s'
                         , beschrijving = '%s'
						 , aantal_plaatsen = '%s'
						 , aantal_reserve = '%s'
						 , prijs = '%s'
                         , user_id = '%s'
                         , dt_wijz = NOW()
                     WHERE id = %d";
     $sql  = sprintf($update_stmt, mysql_real_escape_string($plaats)
                                 , mysql_real_escape_string($dag)
                                 , substr(mysql_real_escape_string($uur), 0, 10)
                                 , mysql_real_escape_string($unit)
                                 , mysql_real_escape_string($doelgroep)
                                 , mysql_real_escape_string($type)
                                 , mysql_real_escape_string($beschrijving)
								 , mysql_real_escape_string($aantal_plaatsen)
								 , mysql_real_escape_string($aantal_reserve)
								 , mysql_real_escape_string($prijs)
                                 , substr(mysql_real_escape_string($_SESSION['usid']), 0, 10)
                                 , mysql_real_escape_string($speeluur_id));
     $result = mysql_query($sql, $conn) or die("Invalid query: " . mysql_error());

     // Sla deze bewerking op in audit tabel
     log_action($_SESSION['usid'], 'UPDATE', $speeluur_id, stripslashes($sql), $conn);
  }

/*------------------------------------------------------------------------------------------------------
 | Verwijder het gegeven speeluur.
 | Vergeet niet om eerst tabel LEDEN_UREN op te kuisen.
 -------------------------------------------------------------------------------------------------------*/

  function delete_speeluur($conn, $speeluur_id)
  {
    if (LOGGING)
    {
      // Get the info that is to be deleted for backup
      $query = "SELECT *
                FROM bad_speeluren
               WHERE id = %d";
      $sql  = sprintf($query, mysql_real_escape_string($speeluur_id));
      $result = mysql_query($sql, $conn) or die("Invalid query: " . mysql_error());

      $row = mysql_fetch_object($result);
      $dml = "INSERT INTO bad_speeluren(id, plaats, dag, uur, unit, doelgroep, type, user_id, dt_wijz)";
      $dml .= " VALUES (".$row->id.", '".$row->plaats."', '".$row->dag."', '".$row->uur."', '".$row->unit."', '".$row->doelgroep."', '".$row->type."', '".$row->user_id."', '".$row->dt_wijz."')";

      // Sla deze bewerking op in audit tabel
      log_action($_SESSION['usid'], 'DELETE', $speeluur_id, stripslashes($dml), $conn);

      mysql_free_result($result);
    }

    $delete_stmt = "DELETE FROM bad_speeluren WHERE id = %d";
    $sql  = sprintf($delete_stmt, mysql_real_escape_string($speeluur_id));
    $result = mysql_query($sql, $conn) or die("Invalid query: " . mysql_error());

    // Sla deze bewerking op in audit tabel
    log_action($_SESSION['usid'], 'DELETE', $speeluur_id, stripslashes($sql), $conn);
  }

?>