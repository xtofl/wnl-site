<?php
/**
 * reporting.php
 *
 * author     : Freek Ceymeulen
 * created    : 29/05/2006
 * parameters : $step
 $              $err_msg
 **/

  // Connect to the DB (must be done before calling sess.php)
  require_once "../functies/badm_db.inc.php";
  $badm_db = badm_conn_db();
  require_once('../functies/sess.php');

  if ($_SESSION['auth'] != true)
  {
    // niet ingelogd -> ga naar login pagina
    // geef deze pagina als parameter mee zodat na succesvolle login teruggekeerd kan worden naar deze pagina
    //session_write_close;
    mysql_close($badm_db);
    header("Location: http://www.badmintonsport.be/admin/login.php?ref=".basename($PHP_SELF));
    exit;
  }

/*
--------------------------------------------------------------------------------------------
|| CONSTANTS
--------------------------------------------------------------------------------------------
*/

  define("NUM_PREDICATES", 5);
  define("NUM_SORTS", 5);

  $GLOBALS['REPORTS'] = Array('REP1' => 'Overzicht van alle administratieve leden'
                             ,'REP2' => 'Overzicht van alle spelers per speeldag'
                             ,'REP3' => 'Overzicht van alle functies binnen de club'
                             ,'REP4' => 'Wie doet wat bij W&amp;L?'
                             ,'REP5' => 'Competitiekalender');

  $GLOBALS['COLUMNS'] = Array('REP1' => Array('naam' => 's.naam','telefoonnummer' => 's.tel','E-mail adres' => 's.email')
                             ,'REP2' => Array('naam' => 's.naam','geboortedatum' => 's.geb_dt','klassement' => 's.klassement','geslacht' => 's.geslacht','telefoonnummer' => 's.tel','E-mail adres' => 's.email','VBL lidnummer' => 's.lidnr','statuut' => 's.type_speler','sporthal' => 'su.plaats','dag van de week' => 'su.dag','uur' => 'su.uur','doelgroep' => 'su.doelgroep','speltype' => 'su.type','unit van de zaal' => 'su.unit')
                             ,'REP3' => Array('functienaam' => 'f.functie','funtieomschrijving' => 'f.omschrijving')
                             ,'REP4' => Array('naam' => 's.naam','telefoonnummer' => 's.tel','E-mail adres' => 's.email','functienaam' => 'f.functie','funtieomschrijving' => 'f.omschrijving')
                             ,'REP5' => Array('Seizoen' => 'p.seizoen','Datum' => 'm.datum','Uur' => 'm.uur','Ontmoeting' => 'm.wedstrijd','Afdeling' => 'p.afdeling','Sporthal' => 'p.sporthal','Reeks' => 'p.type','Resultaat' => 'm.score') );

  $GLOBALS['FROM'] = Array('REP1' => 'bad_spelers s'
                          ,'REP2' => 'bad_spelers s, leden_uren lu, bad_speeluren su'
                          ,'REP3' => 'clubfuncties f'
                          ,'REP4' => 'bad_spelers s, leden_functies lf, clubfuncties f'
                          ,'REP5' => 'competitiematchen m, bad_competitieploegen p');

  $GLOBALS['JOIN'] = Array('REP1' => ' AND eind_dt IS NULL'
                          ,'REP2' => ' AND lu.spelers_id = s.id AND lu.speeluren_id = su.id'
                          ,'REP3' => ''
                          ,'REP4' => ' AND lf.spelers_id = s.id AND lf.functies_id = f.id AND (lf.eind_dt IS NULL OR lf.eind_dt > NOW())'
                          ,'REP5' => ' AND m.competitieploegen_id = p.id');

/*
--------------------------------------------------------------------------------------------
|| FUNCTIONS
--------------------------------------------------------------------------------------------
*/

  require_once("../functies/general_functions.php");

  function print_hidden($step)
  {
    if (isset($_REQUEST['report_title']) && $step != 1)
    {
      echo "<input type=\"hidden\" name=\"report_title\" value=\"".$_REQUEST['report_title']."\">\n";
    }
    if (isset($_REQUEST['report_title']))
    {
      echo "<input type=\"hidden\" name=\"old_report_title\" value=\"".$_REQUEST['report_title']."\">\n";
    }
    echo "<input type=\"hidden\" name=\"available\" value=\"".$_REQUEST['available']."\">\n";
    echo "<input type=\"hidden\" name=\"selected\" value=\"".$_REQUEST['selected']."\">\n";
    if (isset($_REQUEST['filter']) && $step != 3)
    {
      for ($i=0; $i<NUM_PREDICATES; $i++)
      {
        echo "<input type=\"hidden\" name=\"filter[]\" value=\"".$_REQUEST['filter'][$i]."\">\n";
        echo "<input type=\"hidden\" name=\"operator[]\" value=\"".$_REQUEST['operator'][$i]."\">\n";
        echo "<input type=\"hidden\" name=\"predicate[]\" value=\"".$_REQUEST['predicate'][$i]."\">\n";
      }
    }
    if (isset($_REQUEST['order']) && $step != 4)
    {
      for ($i=0; $i<NUM_SORTS; $i++)
      {
        echo "<input type=\"hidden\" name=\"order[]\" value=\"".$_REQUEST['order'][$i]."\">\n";
        $direction = "direction".$i;
        echo "<input type=\"hidden\" name=\"direction".$i."\" value=\"".$_REQUEST[$direction]."\">\n";
      }
    }
    if (isset($_REQUEST['format']) && $step != 5)
    {
      echo "<input type=\"hidden\" name=\"format\" value=\"".$_REQUEST['format']."\">\n";
      echo "<input type=\"hidden\" name=\"separator\" value=\"".$_REQUEST['separator']."\">\n";
      echo "<input type=\"hidden\" name=\"orientation\" value=\"".$_REQUEST['orientation']."\">\n";
      echo "<input type=\"hidden\" name=\"fontsize\" value=\"".$_REQUEST['fontsize']."\">\n";
    }
  }

  function print_title($title)
  {
    echo"<tr><td class=\"td2\">".$title.":<br><br><hr></td></tr>\n";
  }

  function print_step1()
  {
    print_title("Kies het gewenste rapport");

    if (isset($_REQUEST['report_title']))
    {
      $rep = $_REQUEST['report_title'];
    }
    else
    {
      $rep = "REP1"; // default
    }
    foreach ($GLOBALS['REPORTS'] as $report => $title)
    {
      $checked = ($rep == $report) ? " checked" : "";
      echo"<tr><td class=\"td2\"><input type=\"radio\" name=\"report_title\" value=\"".$report."\"".$checked.">".$title."</td></tr>\n";
    }
    print_hidden(1);
  }

  function print_step2($rep)
  {
    print_title("Bepaal welke kolommen je wil zien, en in welke volgorde");
    echo "<tr><td><table width=\"100%\"><tr><td class=\"td2\" align=\"center\"><ul id=\"available\" class=\"sortable boxy\" style=\"height: ".(count($GLOBALS['COLUMNS'][$rep]) * 2)."em;\">\n";
    $selected = $_REQUEST['selected'];
    if (strlen($selected) == 0)  // Er werden nog geen kolommen geselecteerd
    {
      // Print alle kolommen
      foreach ($GLOBALS['COLUMNS'][$rep] as $naam => $column)
      {
        echo "<li>".$naam."</li>";
      }
      echo "</ul><td align=\"center\"><button title=\"Alles selecteren\" style=\"width: 30px;\" onClick=\"return select_all();\">&nbsp;&gt;&gt;&nbsp;</button></td><td class=\"td2\" align=\"center\"><ul id=\"selected\" class=\"sortable boxier\" style=\"height: ".(count($GLOBALS['COLUMNS'][$rep]) * 2)."em;\"></ul></td></tr></table></td></tr>\n";
    }
    else // er werden al kolommen geselecteerd in een vorige stap
    {
      $availableArr = split(",", $_REQUEST['available']);
      // Print de niet-geselecteerde kolommen
      foreach ($availableArr as $column)
      {
        if (strlen($column) > 0)
        {
          echo "<li>".$column."</li>";
        }
      }
      echo "</ul><td  align=\"center\"><button title=\"Alles selecteren\" style=\"width: 30px;\" onClick=\"return select_all();\">&nbsp;&gt;&gt;&nbsp;</button></td><td class=\"td2\" align=\"center\"><ul id=\"selected\" class=\"sortable boxier\" style=\"height: ".(count($GLOBALS['COLUMNS'][$rep]) * 2)."em;\">\n";
      $selectedArr = split(",", $selected);
      // Print de geselecteerde kolommen
      foreach ($selectedArr as $column)
      {
        echo "<li>".$column."</li>";
      }
      echo "</ul></td></tr></table></td></tr>\n";
    }
    echo "<tr><td class=\"td2\"><br>Sleep de gewenste kolommen met je muis naar het rechter vak en zet ze in de gewenste volgorde.<br>Laat je muis pas los wanneer je boven een vak bent.</td></tr>\n";
    print_hidden(2);
  }

  function print_step3()
  {
    print_title("Welke filters moeten er toegepast worden");
    $selectedArr = split(",", $_REQUEST['selected']);
    $rep = $_REQUEST['report_title'];
    for ($i=0; $i<NUM_PREDICATES; $i++)
    {
      $filter = $_REQUEST['filter'];
      $selectTag = "";
      foreach ($selectedArr as $naam)
      {
        $column = $GLOBALS['COLUMNS'][$rep][$naam];
        $selected = ($column == $filter[$i]) ? " SELECTED" : "";
        $selectTag .= "<option value=\"".$column."\"".$selected.">".$naam."</option>\n";
      }
      $selectTag .= "</select>\n";
      echo "<tr><td class=\"td2\"><select name=\"filter[]\"><option value=\"NOSELECTION\" ".(($filter[$i] == "NOSELECTION") ? " SELECTED" : "").">Kies ...</option>";
      echo $selectTag;
      $operator = $_REQUEST['operator'];
      echo "\n<select name=\"operator[]\"><option value=\"EQUALS\"".(($operator[$i] == "EQUALS") ? " SELECTED" : "").">=</option><option value=\"LIKE\"".(($operator[$i] == "LIKE") ? " SELECTED" : "").">LIKE</option><option value=\"NOTEQUALS\"".(($operator[$i] == "NOTEQUALS") ? " SELECTED" : "")."><></option><option value=\"BIGGER\"".(($operator[$i] == "BIGGER") ? " SELECTED" : "").">&gt;</option><option value=\"SMALLER\"".(($operator[$i] == "SMALLER") ? " SELECTED" : "").">&lt;</option></select>\n";
      echo "<input type=\"text\" name=\"predicate[]\" value=\"".$_REQUEST['predicate'][$i]."\"></td></tr>\n";
    }
    print_hidden(3);
  }

  function print_step4()
  {
    print_title("Hoe moeten de gegevens gesorteerd worden");
    echo "<tr><td><table>\n";
    $selectedArr = split(",", $_REQUEST['selected']);
    $rep = $_REQUEST['report_title'];
    for ($i=0; $i<NUM_SORTS; $i++)
    {
      $order = $_REQUEST['order'];
      $selectTag = "";
      foreach ($selectedArr as $naam)
      {
        $column = $GLOBALS['COLUMNS'][$rep][$naam];
        $selected = ($column == $order[$i]) ? " SELECTED" : "";
        $selectTag .= "<option value=\"".$column."\"".$selected.">".$naam."</option>\n";
      }
      $selectTag .= "</select>\n";
      echo "<tr><td class=\"td2\">".(($i == 0) ? "sorteer volgens&nbsp;" : "nadien volgens&nbsp;")."</td>\n";
      echo "<td class=\"td2\"><select name=\"order[]\">";
      if ($i > 0)
      {
        echo "<option value=\"NOSELECTION\"".(($order[$i] == "NOSELECTION") ? " SELECTED" : "").">Kies ...</option>";
      }
      echo $selectTag;
      $direction = "direction".$i;
      if (isset($_REQUEST[$direction]))
      {
        $direction = $_REQUEST[$direction];
      }
      else
      {
        $direction = "ASC"; // default
      }
      echo "\n<input type=\"radio\" name=\"direction".$i."\" value=\"ASC\"".(($direction == "ASC") ? " CHECKED" : "").">Oplopend\n<input type=\"radio\" name=\"direction".$i."\" value=\"DESC\"".(($direction == "DESC") ? " CHECKED" : "").">Aflopend</td></tr>\n";
    }
    echo "</table></td></tr>\n";
    print_hidden(4);
  }

  function print_step5()
  {
    print_title("In welk formaat wil je het rapport");
    echo "<tr><td><table>\n";
    echo "<tr><td class=\"td2\"><input type=\"radio\" name=\"format\" value=\"HTM\"".(($_REQUEST['format'] == "HTM") ? " CHECKED" : "")." onClick=\"show_hide();\">HTML<td>&nbsp;</tr>\n";
    echo "<tr><td class=\"td2\"><input id=\"format_xls\" type=\"radio\" name=\"format\" value=\"XLS\"".(($_REQUEST['format'] == "XLS") ? " CHECKED" : "")." onClick=\"show_hide();\">Excel (csv)</td>\n";
    echo "    <td class=\"td2\" style=\"padding-left:35px;\"><label for=\"separator\" id=\"separator_label\" style=\"display:".(($_REQUEST['format'] == "XLS") ? "inline" : "none").";\">&nbsp;Separator&nbsp;</label><select name=\"separator\" id=\"separator\" style=\"display:".(($_REQUEST['format'] == "XLS") ? "inline" : "none").";\"><option value=\"SEMICOL\"".(($_REQUEST['separator'] == "SEMICOL") ? " SELECTED" : "").">;</option><option value=\"COMMA\"".(($_REQUEST['separator'] == "COMMA") ? " SELECTED" : "").">,</option></select></td></tr>\n";
    echo "<tr><td class=\"td2\"><input type=\"radio\" name=\"format\" value=\"DOC\"".(($_REQUEST['format'] == "DOC") ? " CHECKED" : "")." onClick=\"show_hide();\">Word<td>&nbsp;</tr>\n";
    echo "<tr><td class=\"td2\"><input id=\"format_pdf\" type=\"radio\" name=\"format\" value=\"PDF\"".(($_REQUEST['format'] == "PDF") ? " CHECKED" : "")." onClick=\"show_hide();\">PDF</td>\n";
    echo "    <td class=\"td2\" style=\"padding-left:35px;\"><label for=\"orientation\" id=\"orientation_label\" style=\"display:".(($_REQUEST['format'] == "PDF") ? "inline" : "none").";\">&nbsp;Pagina OriŽntatie&nbsp;<select name=\"orientation\" id=\"orientation\" style=\"display:".(($_REQUEST['format'] == "PDF") ? "inline" : "none").";\"><option value=\"P\"".(($_REQUEST['orientation'] == "P") ? " SELECTED" : "").">Portrait (staand)</option><option value=\"L\"".(($_REQUEST['orientation'] == "L") ? " SELECTED" : "").">Landscape (liggend)</option></select>\n";
    echo "                                               <br><label for=\"fontsize\" id=\"fontsize_label\" style=\"display:".(($_REQUEST['format'] == "PDF") ? "inline" : "none").";\">&nbsp;Lettergrootte&nbsp;<input type=\"text\" size=\"2\" name=\"fontsize\" id=\"fontsize\" style=\"display:".(($_REQUEST['format'] == "PDF") ? "inline" : "none").";\" value=\"".(isset($_REQUEST['fontsize']) ? $_REQUEST['fontsize'] : 12)."\">&nbsp;points</td></tr>\n";
    echo "</table></td></tr>\n";
    echo "<tr><td class=\"td2\"><br>Bij Excel, Word en PDF: kies eerst voor opslaan en dan openen.</td></tr>\n";
    print_hidden(5);
  }

  function build_query()
  {
    // Build select clause
    $selectedArr = split(",", $_REQUEST['selected']);
    $select = "";
    foreach ($selectedArr as $naam)
    {
      $select .= ",".$GLOBALS['COLUMNS'][$_REQUEST['report_title']][$naam];
    }
    // Build where clause
    $operators = Array('EQUALS' => ' = ', 'LIKE' => ' LIKE ', 'NOTEQUALS' => ' <> ', 'BIGGER' => ' > ', 'SMALLER' => ' < ');
    $where = "1 = 1";
    for ($i=0; $i<count($_REQUEST['filter']); $i++)
    {
      if ($_REQUEST['filter'][$i] != "NOSELECTION")
      {
        $where .= " AND ".$_REQUEST['filter'][$i].$operators[$_REQUEST['operator'][$i]]."'".$_REQUEST['predicate'][$i]."'";
      }
    }
    // Build order by clause
    $orderby = "";
    for ($i=0; $i<count($_REQUEST['order']); $i++)
    {
      if ($_REQUEST['order'][$i] != "NOSELECTION")
      {
        $direction = "direction".$i;
        $orderby .= ",".$_REQUEST['order'][$i]." ".$_REQUEST[$direction];
      }
    }
    return "SELECT ".substr($select, 1)." FROM ".$GLOBALS['FROM'][$_REQUEST['report_title']]." WHERE ".$where.$GLOBALS['JOIN'][$_REQUEST['report_title']]." ORDER BY ".substr($orderby, 1);
  }

  function create_html_report($conn, $query)
  {
    echo "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\">\n";
    echo "<html>\n";
    echo "<head>\n";
    echo " <title>W&amp;L - ".$_REQUEST['report_title']."</title>\n";
    echo " <meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\">\n";
    echo "<style type=\"text/css\">\n";
    echo "<!--\n";
  echo "@media print {\n";
    echo ".noprint { display: none; }\n";
  echo "}\n";
    echo " table {\n";
    echo "    border: 1px solid black;\n";
    echo "}\n";
    echo " tr {\n";
    echo "    border-bottom: 1px solid black;\n";
    echo "    font-size: 9pt;\n";
    echo "}\n";
    echo " th {\n";
    echo "    background-color: #cccccc;\n";
    echo "}\n";
    echo "-->\n";
    echo "</style>\n";
    echo "</head>\n";
    echo "<body>\n";
    //echo "<div align=\"center\" class=\"noprint\"><a href=\"javascript:window.history.go(-5);\">Terug naar rapportering</a></div>\n";
    echo "<div align=\"center\" class=\"noprint\"><a href=\"http://www.badmintonsport.be/admin/reporting.php\">Terug naar rapportering</a></div>\n";
    echo "<table cellspacing=\"0\" cellpadding=\"2\">\n";
    // Print the table header
    $selectedArr = split(",", $_REQUEST['selected']);
    echo "<tr>\n";
    foreach ($selectedArr as $column_name)
    {
      echo "<th>".$column_name."</th>";
    }
    echo "</tr>\n";
    // Print the table data
    $result = mysql_query($query, $conn) or badm_mysql_die();
    $num_fields = mysql_num_fields($result);
    while ($row = mysql_fetch_row($result))
    {
      echo "<tr>\n";
      for ($i=0; $i<$num_fields; $i++)
      {
        echo "<td>".$row[$i]."</td>\n";
      }
      echo "</tr>\n";
    }

    echo "</table>\n";
    echo "</body>\n";
    echo "</html>\n";
  }

  function create_excel_report($conn, $query)
  {
    $separator = $_REQUEST['separator'];
    // Print de kolomhoofdingen
    $selectedArr = split(",", $_REQUEST['selected']);
    $header_row = "";
    foreach ($selectedArr as $column_name)
    {
      $header_row .= $separator.$column_name;
    }
    echo substr($header_row, 1)."\n";
    // Print de data
    $result = mysql_query($query, $conn) or badm_mysql_die();
    $num_fields = mysql_num_fields($result);
    while ($row = mysql_fetch_object($result))
    {
      $data_row = "";
      for ($i=0; $i<$num_fields; $i++)
      {
        $data_row .= $separator.$row[$i];
      }
      echo substr($data_row, 1)."\n";
    }
  }

  function create_pdf_report($conn, $query)
  {
    // Voeg fpdf class in
    require_once('../functies/pdf/fpdf.php');

    // breidt fpdf class uit
    class PDF extends FPDF
    {
      function FancyTable($header,$data)
      {
          //Kleuren, lijn dikte en vet lettertype
          $this->SetFillColor(255,0,0);
          $this->SetTextColor(255);
          $this->SetDrawColor(128,0,0);
          $this->SetLineWidth(.3);
          $this->SetFont('','B');
          //Koptekst
          $w=array(40,35,40,45);
          for($i=0;$i<count($header);$i++)
              $this->Cell($w[$i],7,$header[$i],1,0,'C',1);
          $this->Ln();
          //Herstel van kleuren en lettertype
          $this->SetFillColor(224,235,255);
          $this->SetTextColor(0);
          $this->SetFont('');
          //Gegevens
          $fill=0;
          foreach($data as $row)
          {
              $this->Cell($w[0],6,$row[0],'LR',0,'L',$fill);
              $this->Cell($w[1],6,$row[1],'LR',0,'L',$fill);
              $this->Cell($w[2],6,number_format($row[2]),'LR',0,'R',$fill);
              $this->Cell($w[3],6,number_format($row[3]),'LR',0,'R',$fill);
              $this->Ln();
              $fill=!$fill;
          }
          $this->Cell(array_sum($w),0,'','T');
      }
    }
    $pdf = new PDF($_REQUEST['orientation']);
    $pdf->AddPage();
    $pdf->SetFont('Arial', '', $_REQUEST['fontsize']);

    $result = mysql_query($query, $conn) or badm_mysql_die();
    $num_fields = mysql_num_fields($result);
    $data = Array();
    while ($row = mysql_fetch_object($result))
    {
      $data_row = Array();
      for ($i=0; $i<$num_fields; $i++)
      {
        $data_row[] = $row[$i];
      }
      $data[] = $data_row;
    }
    $selectedArr = split(",", $_REQUEST['selected']);
    $pdf->FancyTable($selectedArr, $data);
    $pdf->Output();
  }

  function create_report($conn)
  {
    $select_stmt = build_query();

    $rep = strtolower($_REQUEST['format']);
    $filename = str_replace(' ', '_', $GLOBALS['REPORTS'][$_REQUEST['report_title']]).".".$rep;
    switch ($rep)
    {
      case 'htm':
        create_html_report($conn, $select_stmt);
        break;
      case 'xls':
        header('Content-type: application/ms-excel');
        header('Content-Disposition: attachment; filename="'.$filename.'"');
        create_excel_report($conn, $select_stmt);
        break;
      case 'doc':
        header('Content-type: application/msword');
        header('Content-Disposition: attachment; filename="'.$filename.'"');
        create_html_report($conn, $select_stmt);
        break;
      case 'pdf':
        // We'll be outputting a PDF
        header('Content-type: application/pdf');
        // It will be called downloaded.pdf
        header('Content-Disposition: attachment; filename="'.$filename.'"');
//        create_pdf_report($badm_db, $select_stmt);
        break;
    }
    exit;
  }

  function show_step($step)
  {
    switch ($step)
     {
       case 1:
         print_step1();
         break;
       case 2:
         print_step2($_REQUEST['report_title']);
         break;
       case 3:
         print_step3();
         break;
       case 4:
         print_step4();
         break;
       case 5:
         print_step5();
         break;
       case 6:
         echo create_report($badm_db);
         break;
    }
  }

  function show_pagination($step)
  {
     switch ($step)
     {
       case 1:
         echo "<center><button type=\"button\" name=\"Next\" accesskey=\"N\" title=\"Ga naar stap 2\" onClick=\"return doSubmit(1,2);\">Verder >></button></center>\n";
         break;
       case 5:
         echo "<center><button type=\"button\" name=\"Previous\" accesskey=\"P\" title=\"Ga terug naar stap 4\" onClick=\"return doSubmit(5,4);\"><< Vorige</button>\n";
         echo "<button type=\"button\" name=\"Finish\" accesskey=\"F\" title=\"Maak het rapport\" onClick=\"return doSubmit(5,6);\">Finish</button></center>\n";
         break;
       default:
         echo "<center><button type=\"button\" name=\"Previous\" accesskey=\"P\" title=\"Ga terug naar stap ".($step-1)."\" onClick=\"return doSubmit(".$step.",".($step-1).");\"><< Vorige</button>\n";
         echo "<button type=\"button\" name=\"Next\" accesskey=\"N\" title=\"Ga naar stap ".($step+1)."\" onClick=\"return doSubmit(".$step.",".($step+1).");\">Verder >></button></center>\n";
     }
  }

/*
--------------------------------------------------------------------------------------------
|| BEGIN
--------------------------------------------------------------------------------------------
*/

  if (isset($_REQUEST['step']))
  {
    $step = $_REQUEST['step'];
  }
  else
  {
    $step = 1;
  }

  if (isset($_REQUEST['err_msg']))
  {
    $err_msg = $_REQUEST['err_msg'];
  }
  else
  {
    $err_msg = "";
  }

  if ($step == 6)
  {
    create_report($badm_db);
  }
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
 <title>W&amp;L Admin Module - Reporting</title>
 <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
 <meta http-equiv="imagetoolbar" content="no">
<link href="../css/admin.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="../css/lists.css" type="text/css">
<script language="JavaScript" type="text/javascript" src="../scripts/coordinates.js"></script>
<script language="JavaScript" type="text/javascript" src="../scripts/drag.js"></script>
<script language="JavaScript" type="text/javascript" src="../scripts/dragdrop.js"></script>
<script language="JavaScript" type="text/javascript">
  <!--
  function makeDragable()
  {
    var list = document.getElementById("selected");
    DragDrop.makeListContainer( list );
    list.onDragOver = function() { this.style["background"] = "#EEF"; };
    list.onDragOut = function() {this.style["background"] = "none"; };

    list = document.getElementById("available");
    DragDrop.makeListContainer( list );
    list.onDragOver = function() { this.style["border"] = "1px dashed #AAA"; };
    list.onDragOut = function() {this.style["border"] = "1px solid white"; };
  }

  function doSubmit(currStep, nextStep)
  {
    switch (currStep)
    {
      case 1:
        if (document.reporting.old_report_title)
        {
          for (i=0; i<document.reporting.report_title.length; i++)
          {
            if (document.reporting.report_title[i].checked)
            {
              var report_title = document.reporting.report_title[i].value;
            }
          }
          if (document.reporting.old_report_title.value != report_title)
          {
            document.reporting.selected.value = '';
            document.reporting.available.value = '';
          }
        }
        document.reporting.step.value = nextStep;
        document.reporting.submit();
        break;
      case 2:
        // geef de geselecteerde en niet-geselecteerde kolommen door als 2 comma separated lists
        var availableCsv = "";
        var avaList = document.getElementById("available");
        var avaArr = avaList.getElementsByTagName('li');
        for (var i=0; i<avaArr.length; i++)
        {
          availableCsv = availableCsv + "," + avaArr[i].innerHTML;
        }
        document.reporting.available.value = availableCsv.substring(1);
        var selectedCsv = "";
        var selList = document.getElementById("selected");
        var selArr = selList.getElementsByTagName('li');
        for (var i=0; i<selArr.length; i++)
        {
          selectedCsv = selectedCsv + "," + selArr[i].innerHTML;
        }
        document.reporting.selected.value = selectedCsv.substring(1);
        if (document.reporting.selected.value.length == 0 && nextStep != 1)
        {
          alert("Je moet op zijn minst 1 kolom kiezen");
          return false;
        }
        document.reporting.step.value = nextStep;
        document.reporting.submit();
        break;
      default:
        document.reporting.step.value = nextStep;
        document.reporting.submit();
        break;
    }
    return true;
  }

  function show_hide()
  {
    // Excel
    var radio = document.getElementById('format_xls');
    var separator = document.getElementById('separator');
    var label = document.getElementById('separator_label');
    if (radio.checked)
    {
      separator.style.display = 'inline';
      label.style.display = 'inline';
    }
    else
    {
      separator.style.display = 'none';
      label.style.display = 'none';
    }
    // PDF
    var radio = document.getElementById('format_pdf');
    var orientation = document.getElementById('orientation');
    var orientation_label = document.getElementById('orientation_label');
    var fontsize = document.getElementById('fontsize');
    var fontsize_label = document.getElementById('fontsize_label');
    if (radio.checked)
    {
      orientation.style.display = 'inline';
      orientation_label.style.display = 'inline';
      fontsize.style.display = 'inline';
      fontsize_label.style.display = 'inline';
      alert('PDF is nog niet in orde. sorry.');
    }
    else
    {
      orientation.style.display = 'none';
      orientation_label.style.display = 'none';
      fontsize.style.display = 'none';
      fontsize_label.style.display = 'none';
    }
  }

  function select_all()
  {
    var avaList = document.getElementById("available");
    var avaArr = avaList.getElementsByTagName('li');
    var selList = document.getElementById("selected");
    var avaLen = avaArr.length;
    // Voeg alle elementen uit de lijst Available toe aan lijst Selected
    for (var i=0; i<avaLen; i++)
    {
      listItem = document.createTextNode(avaArr[i].innerHTML);
      listObj = document.createElement("li");
      listObj.appendChild(listItem);
      selList.appendChild(listObj);
    }
    // Verwijder alle elementen uit de lijst Available
    for (var i=0; i<avaLen; i++)
    {
      // begin met het laatste element
       index = avaLen-i-1;
       avaList.removeChild(avaArr[index]);
    }
    // Zorg ervoor dat alle items opnieuw versleept kunnen worden
    makeDragable();
    return false;
  }
  //-->
</script>
</head>
<body bgcolor="#3A6EA5" link="#000000" vlink="#000000" topmargin="0" onLoad="makeDragable();">

<form name="reporting" action="reporting.php" method="POST">

<span style="font-size: 8pt; float: right">
<?php
  echo $_SESSION['username']."</span>";
  // Build current date and time string
  list($wday,$mday,$month,$year,$hour,$minutes) = split("( )",date("w j n Y H i",time()));
  $weekday = array('zondag', 'maandag', 'dinsdag', 'woensdag', 'donderdag', 'vrijdag', 'zaterdag');
  $months  = array('januari','februari','maart','april','mei','juni','juli','augustus','september','oktober','november','december');
  $newdate = "$weekday[$wday], $mday ".$months[$month-1]." $year $hour:$minutes";
?>

  <span style="font-size: 8pt"><?php echo $newdate; ?></span>

<table border="1" cellspacing="0" cellpadding="0" align="center" width="810">

  <tr bgcolor="#C6C3C6" valign="top"><td>
    <table width="100%" border="0" cellspacing="0" cellpadding="1" align="center">
      <tr bgcolor="#400080">
        <td height="20" class="title">&nbsp;W&amp;L - Rapportering - stap <?php echo $step; ?></td>
        <td height="20" align="right" bgcolor="#000084">
          <a href="../docs/W_L_admin_users_guide.doc" target="_blank"><img src="../poll/image/help.gif" width="16" height="14" border="0" alt="Gebruikershandleiding"></a><a href="login.php?action=logout"><img src="../poll/image/cross.gif" width="16" height="14" border="0" alt="Uitloggen"></a></td>
      </tr>
    </table>

    <table border="0" cellspacing="0" cellpadding="0">
      <tr valign="top">
        <td width="100">
<?php
  // Print the menu
  write_menu($_SERVER['PHP_SELF']);
?>
        </td>

        <td style="padding-left: 10px;">
          <table width="100%" border="0" cellspacing="0" cellpadding="10">
            <tr>
              <td>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
<?php
  if (strlen($err_msg) > 0)
  {
    echo "<tr><td colspan=\"7\"><font color=\"red\"><b>".$err_msg."</b></font></td></tr>";
  }
  show_step($step);
?>
                </table>

              </td>
            </tr>
            <tr>
              <td class="td2"><hr>
<?php
  show_pagination($step);
?>
            </tr>
          </table>
      </td></tr>

    </table>
  </td></tr>
</table>
<br>
<br>
<input type="hidden" name="step" value="">
</form>
</body>
</html>