<?php 

 /*
 * get_pasfoto.php
 * Fetching a foto out of the database
 * Author    : Freek Ceymeulen 
 * Date      : 16/11/2005
 * Parameter : id : spelers_id
 */

/*--------------------------------------------------------------------------------------------
 | SECURITY
 --------------------------------------------------------------------------------------------*/

  // Connect to the DB (must be done before calling sess.php)
  require_once "../functies/badm_db.inc.php";
  $badm_db = badm_conn_db();
  require_once('../functies/sess.php');

  if ($_SESSION['auth'] != true)
  {
    // niet ingelogd -> ga naar login pagina
    //session_write_close;
    mysql_close($badm_db);
    header("Location: http://www.badmintonsport.be/admin/login.php");
    exit;
  }

// Assert parameter
  if (isset($_GET['id']))
  {
    $spelers_id = $_GET['id'];
  }
  elseif (isset($_POST['id']))
  {
    $spelers_id = $_POST['id'];
  }
  else
  {
    $spelers_id = 0;
  }

  // Ga na of er een record bestaat voor het gegeven spelers ID
  $sql = "SELECT 1 FROM pasfotos WHERE spelers_id = %d";
  $query = sprintf($sql, $spelers_id);
  $result = mysql_query($query, $badm_db);
  if (mysql_num_rows($result) == 0)
  {
    $spelers_id = 0;
  }
  
  $sql = "SELECT data
               , naam
               , file_type
            FROM pasfotos
           WHERE spelers_id = %d";
         
  $query = sprintf($sql, $spelers_id);

  $result = mysql_query($query, $badm_db);

  $row = mysql_fetch_row($result); 
  $data = $row[0];
  $file = htmlspecialchars($row[1]);

  if (is_null($row[2]))
  {
    $mimetype = array( 'jpg'=>'image/jpeg'
                     , 'jpeg'=>'image/jpeg'
                     , 'gif'=>'image/gif'
                     , 'png'=>'image/x-png'
                     , 'bmp'=>'image/bmp'
                     , 'doc'=>'application/msword'
                     , 'eps'=>'application/postscript'
                     , 'htm'=>'text/html'
                     , 'html'=>'text/html'
                     , 'pdf'=>'application/pdf'
                     , 'txt'=>'text/plain'
                     , 'xls'=>'application/vnd.ms-excel' ); 

    $p = explode('.', $file); 
    $pc = count($p); 

    //send headers 
    if($pc > 1 && isset($mimetype[$p[$pc - 1]])) 
    { 
      header("Content-Type: " . $mimetype[$p[$pc - 1]] . "\n"); 
    } 
    else 
    { 
      header("Content-Type: application/octet-stream\n"); 
      header("Content-Disposition: attachment; filename=\"$file\"\n"); 
    }
  }
  else
  {
    header("Content-Type: " . $row[2] . "\n"); 
  }
  header("Content-Transfer-Encoding: binary\n"); 
  header("Content-length: " . strlen($data) . "\n"); 

  //send file contents 
  print($data);

?>

