<?php
/**
 * nieuwe_ploeg.php
 *
 * author    : Freek Ceymeulen
 * created   : 01/02/2005
 * variables :
 **/

  // Connect to the DB (must be done before calling sess.php)
  require_once "../functies/badm_db.inc.php";
  $badm_db = badm_conn_db();
  require_once('../functies/sess.php');

  if ($_SESSION['auth'] != true)
  {
    // niet ingelogd -> ga naar login pagina
    // geef deze pagina als parameter mee zodat na succesvolle login teruggekeerd kan worden naar deze pagina
    //session_write_close;
    mysql_close($badm_db);
    header("Location: http://www.badmintonsport.be/admin/login.php?ref=".basename($PHP_SELF));
    exit;
  }
/*
--------------------------------------------------------------------------------------------
|| FUNCTIONS
--------------------------------------------------------------------------------------------
*/
  require_once("../functies/general_functions.php");
/*
--------------------------------------------------------------------------------------------
|| BEGIN
--------------------------------------------------------------------------------------------
*/
  $ploegnr = "";
  $reeks = "";
  $afd = "";
  $kap = "";
  $hal = "Veltem";
  $uur = "";
  if (date("m") > 5)
  {
    $seizoen = date("Y")."-".(date("Y") + 1);
  }
  else
  {
    $seizoen = (date("Y") - 1)."-".date("Y");
  }
  $err_msg = NULL;
  if (isset($_GET['err_msg']))
  {
    $err_msg = $_GET['err_msg'];
  }
  if (isset($_POST['reeks']))
  {
    $reeks = $_POST['reeks'];
    $query = "SELECT ifnull(MAX(c.ploegnummer), 0) + 1 AS ploegnummer
                FROM bad_competitieploegen c
               WHERE c.seizoen = '%s'
           AND c.type = '%s'";
    $sql  = sprintf($query, mysql_real_escape_string($seizoen)
                        , mysql_real_escape_string($reeks));
    $result = mysql_query($sql, $badm_db) or badm_mysql_die();
    $row = mysql_fetch_object($result);
    $ploegnr = $row->ploegnummer;
    mysql_free_result($result);
  }
  elseif (isset($_GET['reeks']))
  {
    $reeks = $_GET['reeks'];
  }
  if (isset($_GET['afdeling']))
  {
    $afd = $_GET['afdeling'];
  }
  if (isset($_GET['kapitein']))
  {
    $kap = $_GET['kapitein'];
  }
  if (isset($_GET['sporthal']))
  {
    $hal = $_GET['sporthal'];
  }
  if (isset($_GET['uur']))
  {
    $uur = $_GET['uur'];
  }
?>
<html>
<head>
<title>W&amp;L Admin Module</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="http://www.badmintonsport.be/css/admin.css" rel="stylesheet" type="text/css">
<script language="JavaScript" src="../scripts/modal_dialog.js"></script>
<script language="JavaScript">
<!--
var retval = "";
// Calls a dialog window with LOV-values for this field
function showLov(obj1, obj2)
{
  if (navigator.appName == "Microsoft Internet Explorer")
  {
    retval = window.showModalDialog("http://www.badmintonsport.be/functies/build_dialog_window.php?p_lov_item="+obj1.name+"&p_search_string="+obj1.value.toUpperCase().replace('%', '***')+"&p_filter_min_length=0", window, "status:no; resizable:no; font-size:16px; dialogWidth:20em; dialogHeight: 20em");
    if (retval != null)
    {
      var desc_code_arr = retval.split("|");
      obj1.value = desc_code_arr[0];
      if (obj2 != null)
      {
        obj2.value = desc_code_arr[1];
      }
    }
  }
  else
  {
    //openDialog('lovNS.php', 300, 400, setPrefs);
    alert("This functionality only works with MS Internet Explorer.");
  }
}

function set_focus(p_nummer)
{
  if (p_nummer > 0)
  {
    document.forms[0].afdeling.focus();
  }
  else
  {
    document.forms[0].reeks.focus();
  }
}

function doSubmit(p_action)
{
  if (p_action == 'insert')
  {
    if (document.forms[0].ploeg.value.length == 0)
    {
      alert('Je bent vergeten een ploegnummer in te vullen!');
      document.forms[0].ploeg.focus();
      return false;
    }
    document.forms[0].action = "dml.php";
  }
  document.forms[0].submit();
  return true;
}
-->
</script>
</head>
<body bgcolor="#3A6EA5" link="#000000" vlink="#000000" topmargin="0" bottommargin="0" onload="set_focus(<?php echo $ploegnr; ?>)">

<span style="font-size: 8pt; float: right"><?php echo $_SESSION['username']; ?></span>
<?php
  // Build current date and time string
  list($wday,$mday,$month,$year,$hour,$minutes) = split("( )",date("w j n Y H i",time()));
  $weekday = array('zondag', 'maandag', 'dinsdag', 'woensdag', 'donderdag', 'vrijdag', 'zaterdag');
  $months  = array('januari','februari','maart','april','mei','juni','juli','augustus','september','oktober','november','december');
  $newdate = "$weekday[$wday], $mday ".$months[$month-1]." $year $hour:$minutes";
?>
<span style="font-size: 8pt"><?php echo $newdate; ?></span>

<table border="1" cellspacing="0" cellpadding="0" align="center" width="810">
  <tr bgcolor="#C6C3C6" valign="top"><td>
    <table width="100%" border="0" cellspacing="0" cellpadding="1" align="center">
      <tr bgcolor="#400080">
        <td height="20" class="title">&nbsp;W&amp;L - Competitieploeg toevoegen</td>
        <td height="20" align="right" bgcolor="#000084">
          <a href="http://www.badmintonsport.be/docs/W_L_admin_users_guide.doc" target="_blank"><img src="../poll/image/help.gif" width="16" height="14" border="0" alt="Gebruikershandleiding"></a><a href="login.php?action=logout"><img src="../poll/image/cross.gif" width="16" height="14" border="0" alt="Uitloggen"></a></td>
      </tr>
    </table>
    <table border="0" cellspacing="0" cellpadding="0">
      <tr valign="top">
        <td width="100">
<?php
  // Print the menu
  write_menu($_SERVER['PHP_SELF']);
?>
        </td>
        <td>
          <table width="100%" border="0" cellspacing="0" cellpadding="2">
<?php
  if (!is_null($err_msg))
  {
    echo '<tr><td colspan="2"><font color="red"><b>'.$err_msg.'</b></font></td></tr>';
  }
?>
           <form name="nieuwe_ploeg" action="<?php echo basename($PHP_SELF); ?>" method="post">
            <tr><hr>
              <td class="td2">Reeks :</td>
              <td><select name="reeks" class="input" onBlur="return doSubmit('get_ploeg_nr');">
<?php
  build_options('reeks', $reeks, $badm_db);
?>
                  </select></td>
            </tr>
            <tr>
              <td class="td2">Ploeg Nr :</td>
              <td><input type="text" name="ploeg" size="1" class="input" value="<?php echo $ploegnr; ?>"></td>
            </tr>
            <tr>
              <td class="td2">Afdeling :</td>
              <td><select name="afdeling" class="input">
<?php
  build_options('afdeling', $afd, $badm_db);
?>
                  </select></td>
            </tr>
            <tr>
              <td class="td2">Ploegkapitein:</td>
              <td class="td2"><?php
  if ( strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE'))
  {
?>
<input class="input" type="text" name="kapitein" value="<?=$kap?>" size="30" disabled>
<input type="hidden" name="kapitein_id">
<img src="../images/lov_button.jpg" alt="List Of Values" onClick="showLov(document.nieuwe_ploeg.kapitein, document.nieuwe_ploeg.kapitein_id);" style="CURSOR: pointer; CURSOR: hand">
<?php
  }
  else
  {
    echo "<select class=\"input\" name=\"kapitein\" onChange=\"document.nieuwe_ploeg.kapitein_id.value=document.nieuwe_ploeg.kapitein.value;\">\n<option value=\"\">--geen--</option>\n";
    $sql_stmt = "SELECT id, naam FROM bad_spelers WHERE eind_dt IS NULL ORDER BY 2";
    $rslt = mysql_query($sql_stmt, $badm_db) or badm_mysql_die();
    while ($row = mysql_fetch_object($rslt))
    {
      echo "<option value=\"".$row->id."\"";
      echo ">".$row->naam."</option>\n";
    }
    mysql_free_result($rslt);
    echo "</select>\n<input type=\"hidden\" name=\"kapitein_id\">\n";
  }
?>
              </td>
            </tr>
            <tr>
              <td class="td2">Sporthal:</td>
              <td><select name="sporthal" class="input">
<?php
  build_options('sporthal', $hal, $badm_db);
?>
                  </select></td>
            </tr>
            <tr>
              <td class="td2">Uur:</td>
              <td><input type="text" name="uur" size="5" class="input" value="<?php echo $uur; ?>"></td>
            </tr>
            <tr>
              <td class="td2" colspan="2" align="center"><hr>
                <input type="hidden" name="page" value="nieuwe_ploeg.php">
        <input type="hidden" name="seizoen" value="<?php echo $seizoen; ?>">
                <button type="button" name="add" accesskey="S" title="Voeg een nieuwe ploeg toe" onClick="return doSubmit('insert');"><span style="text-decoration: underline">S</span>ave</button>
              </td>
            </tr>
           </form>
          </table>
      </td></tr>
    </table>
  </td></tr>
</table>
<br>
<br>
</body>
</html>
<?php
  //mysql_close($badm_db);
?>