<?php
/**
 * print_etiketten.php
 *
 * object     : creates a PDF file with the layout specified in etiketten_param_sheet.php
 * author     : Freek Ceymeulen
 * created    : 13/10/2005
 * parameters : format       : (optional) Page format. Possible values are:
 *                             - A3
 *                             - A4 (default)
 *                             - A5
 *                             - A6
 *                             - letter
 *                             - legal
 *                             - executive
 *                             - custom
 *              pagewidth    : (optional) Page width (in mm)
 *              pageheight   : (optional) Page height (in mm)
 *              orientation  : (optional) Page orientation.  Possible values are:
 *                             - P(ortrait) (default)
 *                             - L(andscape)
 *              topmargin    : (optional) Distance (in mm) from top of page to first label. default 15mm
 *              leftmargin   : (optional) Distance (in mm) from left side of page to start of first label. default 7mm
 *              columns      : (optional) Number of labels in horizontal direction on one page. default 3
 *              rows         : (optional) Number of labels in vertical direction on one page. default 7
 *              horspace     : (optional) Horizontal space between 2 labels. default 2.5mm
 *              vertspace    : (optional) Vertical space between 2 labels. default 0mm
 *              padding      : (optional) Distance (in mm) between label edge and start of text. default 10mm
 *              family       : (optional) Font family. Possible values are:
 *                             - Arial (default)
 *                             - Helvetica
 *                             - Times
 *                             - Courier
 *                             - Symbol
 *                             - Zapfdingbats
 *              size         : (optional) Font size (in points). default 9pt
 *              style        : (optional) Font weight. Possible values:
 *                             - (leeg) (default)
 *                             - B(old)
 *                             - I(talic)
 *                             - BI
 *              labelcolor   : (optional) Label background color. Possible values:
 *                             - white (default)
 *                             - black
 *                             - yellow
 *              textcolor    : (optional) Text color. Possible values:
 *                             - white (default)
 *                             - black
 *                             - red
 *                             - green
 *                             - blue
 *                             - yellow
 *              align        : (optional) Text align. Possible values are:
 *                             - L(eft) (default)
 *                             - C(enter)
 *                             - R(ight)
 **/
/* // 15/10/2005 fc This causes problems in IE, the page is not rendered automatically, only after a refresh
  session_start();
  if ($_SESSION['auth'] != true)
  {
    // niet ingelogd -> ga naar login pagina
    // na succesvolle login terugkeren naar ledenpagina
    header("Location: http://www.badmintonsport.be/admin/login.php?ref=leden.php");
    exit;
  }
  */
  // Receive parameters
  // 1. Page format
  if (isset($_REQUEST["format"]))
  {
    $format = strtolower($_REQUEST["format"]);
    // Define page width and height (in mm)
    switch ($format)
    {
      case "a4":
        $page_width = 210;
        $page_height = 297;
        break;
      case "custom":
        if (isset($_REQUEST["pagewidth"]))
        {
          $page_width = $_REQUEST["pagewidth"];
        }
        else
        {
          $page_width = 210; // A4
        }
        if (isset($_REQUEST["pageheight"]))
        {
          $page_height = $_REQUEST["pageheight"];
        }
        else
        {
          $page_height = 297; // A4
        }
        break;
      case "a3":
        $page_width = 420;
        $page_height = 297;
        break;
      case "a5":
        $page_width = 148;
        $page_height = 210;
        break;
      case "a6":
        $page_width = 74;
        $page_height = 105;
        break;
      case "letter":
        $page_width = 215.9;
        $page_height = 279.4;
        break;
      case "legal":
        $page_width = 215.9;
        $page_height = 355.6;
        break;
      case "executive":
        $page_width = 184.2;
        $page_height = 26.67;
        break;
      default:  //A4
        $page_width = 210;
        $page_height = 297;
    }
  }
  else
  {
    // default A4
    $format = "a4";
    $page_width = 210;
    $page_height = 297;
  }

  // 2. Page orientation
  if (isset($_REQUEST["orientation"]))
  {
    $orientation = strtolower($_REQUEST["orientation"]);
    if ($orientation == "l" || $orientation == "landscape")
    {
      // switch page width and height
      $temp = $page_width;
      $page_width = $page_height;
      $page_height = $temp;
    }
  }
  else
  {
    $orientation = "p";
  }

  // Padding
  if (isset($_REQUEST["padding"]))
  {
    $padding = $_REQUEST["padding"];
  }
  else
  {
    $padding = 10;
  }

  // 3. Page margins
  if (isset($_REQUEST["topmargin"]))
  {
    $topmargin = $_REQUEST["topmargin"] + $padding;
  }
  else
  {
    $topmargin = 15 + $padding;
  }
  if (isset($_REQUEST["leftmargin"]))
  {
    $leftmargin = $_REQUEST["leftmargin"] + $padding;
  }
  else
  {
    $leftmargin = 7 + $padding;
  }
  if (isset($_REQUEST["bottommargin"]))
  {
    $bottommargin = $_REQUEST["bottommargin"] + $padding;
  }
  else
  {
    $bottommargin = $topmargin;
  }

  // 4. # columns
  if (isset($_REQUEST["columns"]))
  {
    $columns = $_REQUEST["columns"];
  }
  else
  {
    $columns = 3;
  }

  // 4. # rows
  if (isset($_REQUEST["rows"]))
  {
    $rows = $_REQUEST["rows"];
  }
  else
  {
    $rows = 7;
  }

  // 5. Distance between 2 labels
  if (isset($_REQUEST["horspace"]))
  {
    $horspace = $_REQUEST["horspace"];
  }
  else
  {
    $horspace = 2.5;
  }
  if (isset($_REQUEST["vertspace"]))
  {
    $vertspace = $_REQUEST["vertspace"];
  }
  else
  {
    $vertspace = 0;
  }

  // 6. Fonts
  if (isset($_REQUEST["family"]))
  {
    $family = $_REQUEST["family"];
  }
  else
  {
    $family = "Arial";
  }
  if (isset($_REQUEST["size"]))
  {
    $size = $_REQUEST["size"];
  }
  else
  {
    $size = 9;
  }
  if (isset($_REQUEST["style"]))
  {
    $style = $_REQUEST["style"];
  }
  else
  {
    $style = "";
  }

  // 7. Colors
  if (isset($_REQUEST["labelcolor"]))
  {
    $temp = $_REQUEST["labelcolor"];
  }
  else
  {
    $temp = "white";
  }
  switch ($temp)
  {
    case "white":
      $labelcolor = array(255, 255, 255);
      break;
    case "black":
      $labelcolor = array(0, 0, 0);
      break;
    case "yellow":
      $labelcolor = array(255, 255, 0);
      break;
    default:
      $labelcolor = array(255, 255, 255);
  }
  if (isset($_REQUEST["textcolor"]))
  {
    $temp = $_REQUEST["textcolor"];
  }
  else
  {
    $temp = "black";
  }
  switch ($temp)
  {
    case "white":
      $textcolor = array(255, 255, 255);
      break;
    case "black":
      $textcolor = array(0, 0, 0);
      break;
    case "red":
      $textcolor = array(255, 0, 0);
      break;
    case "green":
      $textcolor = array(0, 255, 0);
      break;
    case "blue":
      $textcolor = array(0, 0, 255);
      break;
    case "yellow":
      $textcolor = array(255, 255, 0);
      break;
    default:
      $textcolor = array(0, 0, 0);
  }

  // 8. Text
  if (isset($_REQUEST["align"]))
  {
    $align = $_REQUEST["align"];
  }
  else
  {
    $align = "L";
  }

  // 9. Label measures
  if (isset($_REQUEST["labelwidth"]))
  {
    $labelwidth = $_REQUEST["labelwidth"] - (2 * $padding);
  }
  else
  {
    //$labelwidth = (($page_width - ($leftmargin * 2) - ($horspace * ($columns - 1))) / $columns);
    $labelwidth = (($page_width - ($padding * 2) - ($horspace * ($columns - 1))) / $columns);
  }
  if (isset($_REQUEST["labelheight"]))
  {
    $labelheight = $_REQUEST["labelheight"] - (2 * $padding);
  }
  else
  {
    $labelheight = (($page_height - ($topmargin + $bottommargin)) / $rows);
  }

  // Voeg fpdf class in
  require_once('../functies/pdf/fpdf.php');

  // breidt fpdf class uit met enkele functies
  class PDF extends FPDF
  {
    //Current column
    var $col=0;
    //Ordinate of column start
    var $y0;
    var $leftmargin;
    var $labelwidth;
    var $horspace;
    var $columns;

    function PDF($orientation='P',$unit='mm',$format='A4',$leftmargin,$labelwidth,$horspace,$columns=1)
    {
      //Call parent constructor
      $this->FPDF($orientation,$unit,$format);
      //Initialization
      $this->leftmargin = $leftmargin;
      $this->labelwidth = $labelwidth;
      $this->horspace = $horspace;
      $this->columns = $columns;
    }

    function Header()
    {
      //Save ordinate
      $this->y0 = $this->GetY();
    }

    function SetCol($col)
    {
      //Set position at a given column
      $this->col = $col;
      $x = $this->leftmargin + ($col * ($this->labelwidth + $this->horspace));
      $this->SetLeftMargin($x);
      $this->SetX($x);
    }

    // Allow multicolumn layout
    function AcceptPageBreak()
    {
      //Method accepting or not automatic page break
      if($this->col < ($this->columns - 1))
      {
        //Go to next column
        $this->SetCol($this->col + 1);
        //Set ordinate to top
        $this->SetY($this->y0);
        //Keep on page
        return false;
      }
      else
      {
        //Go back to first column
        $this->SetCol(0);
        //Page break
        return true;
      }
    }
  }

  // Cre�er PDF-bestand
  $pdf = new PDF($orientation, 'mm', $format, $leftmargin, $labelwidth, $horspace, $columns);
  $pdf->SetCompression(False);
  $pdf->SetMargins($leftmargin, $topmargin);
  $pdf->AddPage();
  $pdf->SetFont($family, $style, $size);
  $pdf->SetTextColor($textcolor[0],$textcolor[1],$textcolor[2]);
  $pdf->SetFillColor($labelcolor[0], $labelcolor[1], $labelcolor[2]);

  // Connect to db
  require_once "../functies/badm_db.inc.php";
  $badm_db = badm_conn_db();

  // Get data
  $query = "SELECT naam
                 , adres
                 , postcode
                 , woonplaats
             FROM bad_spelers
            WHERE eind_dt IS NULL
              AND clubblad = 'J'
            ORDER BY SUBSTRING(naam, LOCATE(' ', naam) + 1, LENGTH(naam) - LOCATE(' ', naam))";
  //$sql  = sprintf($query, mysql_real_escape_string($orderBy));
  $sql = $query;
  $result = mysql_query($sql, $badm_db) or badm_mysql_die();

  while ($row = mysql_fetch_object($result))
  {
    $pdf->MultiCell($labelwidth, 4, $row->naam."\n".$row->adres."\n".$row->postcode." ".$row->woonplaats, 0, $align, 1);
    $pdf->ln($labelheight + $vertspace - (3 * 4));
  }

  mysql_free_result($result);
  //mysql_close($badm_db);
  
  $pdf->Output();
?>