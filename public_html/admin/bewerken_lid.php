<?php
/**
 * bewerken_lid.php
 *
 * author     : Freek Ceymeulen
 * created    : 01/02/2005
 *              02/11/2012 reserve toegevoegd
 *              23/11/2013 administratief_lid toegevoegd
 * parameters : err_msg
 **/

  // Connect to the DB (must be done before calling sess.php)
  require_once "../functies/badm_db.inc.php";
  $badm_db = badm_conn_db();
  require_once('../functies/sess.php');

  if ($_SESSION['auth'] != true)
  {
    // niet ingelogd -> ga naar login pagina
    // geef deze pagina als parameter mee zodat na succesvolle login teruggekeerd kan worden naar deze pagina
    //session_write_close;
    mysql_close($badm_db);
    header("Location: http://www.badmintonsport.be/admin/login.php?ref=".basename($PHP_SELF));
    exit;
  }
/*
--------------------------------------------------------------------------------------------
|| FUNCTIONS
--------------------------------------------------------------------------------------------
*/
  require_once("../functies/general_functions.php");

  function write_speeluur($id, $check, $uur, $type, $reserve)
  {
    echo '                    <td class="td2"><input type="checkbox" name="uur'.$id.'" class="input" style="border: none"'.$check.' onChange="set_update_value(\'SPEELUREN\');"></td>';
    echo '                    <td class="td2"><a href="bewerken_speeluur.php?id='.$id.'"'; echo ($reserve=='Y') ? ' style="color:orange"' : ''; echo '>'; echo ($check==' CHECKED') ? '<b>' : ''; echo $uur; echo ($check==' CHECKED') ? '</b>' : ''; echo '</a></td>';
    echo '                    <td class="td2">'.$type.'</td>';
  }

  function write_competitie($id, $afd, $type, $basis)
  {
    $checked = ($type == "TITULARIS") ? " CHECKED" : "";
    echo '                    <td class="td2"><a href="bewerken_ploeg.php?id='.$id.'">'.$afd.'</a></td>';
    echo '                    <td class="td2">&nbsp;&nbsp;basis</td>';
    echo '                    <td class="td2"><input type="checkbox" name="titularis'.$id.'" class="input" style="border: none"'.$checked.' onChange="set_update_value(\'COMPETITIE\');"></td>';
    $checked = ($type == "RESERVE") ? " CHECKED" : "";
    echo '                    <td class="td2">&nbsp;&nbsp;reserve</td>';
    echo '                    <td class="td2"><input type="checkbox" name="reserve'.$id.'" class="input" style="border: none"'.$checked.' onChange="set_update_value(\'COMPETITIE\');"></td>';
    $checked = ($basis == "Y") ? " CHECKED" : "";
    echo '                    <td class="td2" title="Basisopstelling">&nbsp;&nbsp;bo</td>';
    echo '                    <td class="td2"><input type="checkbox" name="basis'.$id.'" class="input" style="border: none"'.$checked.' onChange="set_update_value(\'COMPETITIE\');"></td>';
  }
/*
--------------------------------------------------------------------------------------------
|| BEGIN
--------------------------------------------------------------------------------------------
*/

  $spelers_id = $_REQUEST['id'];
  if (date("m") > 5)
  {
    $seizoen = date("Y")."-".(date("Y") + 1);
  }
  else
  {
  $seizoen = (date("Y") - 1)."-".date("Y");
  }

  if (isset($_REQUEST["err_msg"]))
  {
    $err_msg = $_REQUEST["err_msg"];
  }
  else
  {
    $err_msg = "";
  }
?>
<html>
<head>
<title>W&amp;L Admin Module</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../css/admin.css" rel="stylesheet" type="text/css">
<script language="JavaScript" src="../scripts/CalendarPopup.js"></script>
<script language="JavaScript">
  document.write(getCalendarStyles());
  var cal = new CalendarPopup("datediv");
  cal.showNavigationDropdowns();
</script>
<script>
<!--
function checkEmail(emailAddress)
{
   var foundAtSymbol = 0;
   var foundDot = 0;
   var md;

// Go through each character in the email address.
   for (var x=0; x<emailAddress.length - 1; x++)
   {
      md = emailAddress.substr(x, 1);

   // Is the character an @ symbol?
      if (md == '@')
         foundAtSymbol++;

   // Count how many dots there are after the @ symbol.
      if (md == '.' && foundAtSymbol == 1)
         foundDot++;
   }

// Is there only one @ symbol, and are there more than one dots?
   if (foundDot > 0 && foundAtSymbol == 1)
   {
      return true;
   }
   else
   {
      return false;
   }
}

// Geeft aan of een update op gegeven tabel dient te gebeuren
function set_update_value(p_table)
{
  if (p_table == 'SPELERS')
  {
    document.forms[0].update_spelers.value = 1;
  }
  else if (p_table == 'SPEELUREN')
  {
    document.forms[0].update_speeluren.value = 1;
  }
  else if (p_table == 'COMPETITIE')
  {
    document.forms[0].update_competitie.value = 1;
  }
}

function doSubmit(p_action)
{
  if (p_action == 'update')
  {
    // Controleer of naam is ingevuld
    if (document.forms[0].achternaam.value.length == 0)
    {
      alert('Je bent vergeten een achternaam in te vullen!');
      document.forms[0].achternaam.focus();
      return false;
    }
    if (document.forms[0].voornaam.value.length == 0)
    {
      alert('Je bent vergeten een voornaam in te vullen!');
      document.forms[0].voornaam.focus();
      return false;
    }
    if (document.forms[0].email.value.length > 0)
    {
      // Controleer of E-mail correct is
      if (!checkEmail(document.forms[0].email.value))
      {
        alert('Het E-mail adres is niet correct!');
        document.forms[0].email.focus();
        return false;
      }
    }
    document.forms[0].command.value = 'update';
    document.forms[0].action = 'dml.php';
  }
  else if (p_action == 'delete')
  {/*
    if (document.forms[0].totaal_uren.value > 0)
    {
      alert('WARNING: Je kan dit lid niet verwijderen zolang hij/zij toegekend is aan een speeldag.');
      return false;
    }
    else if (document.forms[0].totaal_comp.value > 0)
    {
      alert('WARNING: Je kan dit lid niet verwijderen zolang hij/zij toegekend is aan een competitieploeg.');
      return false;
    }
    else*/
    {
      if (confirm("Ben je zeker dat je deze speler wilt verwijderen?\nOm een speler op non-actief te zetten vul je de 'Gestopt op' datum in."))
      {
        document.forms[0].command.value = 'delete';
        document.forms[0].action = 'dml.php';
        alert('Deze functie is nog niet ge�mplementeerd');
        return false;
      }
      else
      {
        return false;
      }
    }
  }
  document.forms[0].submit();
  return true;
}
-->
</script>
</head>
<body bgcolor="#3A6EA5" link="#000000" vlink="#000000" topmargin="0" bottommargin="0">

<span style="font-size: 8pt; float: right"><?php echo $_SESSION['username']; ?></span>
<?php
  // Build current date and time string
  list($wday,$mday,$month,$year,$hour,$minutes) = split("( )",date("w j n Y H i",time()));
  $weekday = array('zondag', 'maandag', 'dinsdag', 'woensdag', 'donderdag', 'vrijdag', 'zaterdag');
  $months  = array('januari','februari','maart','april','mei','juni','juli','augustus','september','oktober','november','december');
  $newdate = "$weekday[$wday], $mday ".$months[$month-1]." $year $hour:$minutes";
?>
<span style="font-size: 8pt"><?php echo $newdate; ?></span>

<table width="810" border="1" cellspacing="0" cellpadding="0" align="center">
  <tr bgcolor="#C6C3C6" valign="top">
   <td>

    <table width="100%" border="0" cellspacing="0" cellpadding="1" align="center">
      <tr bgcolor="#400080">
        <td height="20" class="title">&nbsp;W&amp;L - Lid</td>
        <td height="20" align="right" bgcolor="#000084">
          <a href="../docs/W_L_admin_users_guide.doc" target="_blank"><img src="../poll/image/help.gif" width="16" height="14" border="0" alt="Gebruikershandleiding"></a><a href="login.php?action=logout"><img src="../poll/image/cross.gif" width="16" height="14" border="0" alt="Uitloggen"></a></td>
      </tr>
    </table>

    <table border="0" cellspacing="0" cellpadding="0">
      <tr valign="top">
        <td width="100">

<?php
  // Print the menu
  write_menu($_SERVER['PHP_SELF']);
?>

        </td>

        <td>
<?php
  if (!is_numeric($spelers_id))
  // Check to avoid people passing sql statements in the url
  {
    $err_msg = "Security violation, admin has been alerted.";
    mail("freek.ceymeulen@pandora.be", "Possible breakin attempt", "On page ".$_SERVER['PHP_SELF']." from IP ".$_SERVER['REMOTE_ADDR']." using method ".$_SERVER['REQUEST_METHOD']." on url ".$_SERVER['REQUEST_URI'], "From: webmaster@badmintonsport.be");
    //exit;
  }
  else
  {
    $query = "SELECT s.achternaam
                   , s.voornaam
                   , s.geslacht
                   , s.adres
                   , s.postcode
                   , s.woonplaats
                   , s.tel
                   , s.gsm
                   , s.email
                   , DATE_FORMAT(s.geb_dt, '%%d/%%m/%%Y') AS geb_dt
                   , s.lidnr
                   , s.klassement
                   , s.type_speler
                   , DATE_FORMAT(s.club_dt, '%%d/%%m/%%Y') AS club_dt
                   , DATE_FORMAT(s.eind_dt, '%%d/%%m/%%Y') AS eind_dt
                   , s.maat
                   , s.clubblad
                   , s.competitie
                   , s.lidgeld
                   , s.betaald
                   , s.remark
                   , s.administratief_lid
                FROM bad_spelers s
               WHERE s.id = %d";
    $sql  = sprintf($query, mysql_real_escape_string($spelers_id));
    $result = mysql_query($sql, $badm_db) or badm_mysql_die();
    if (mysql_num_rows($result) == 0)
    {
      $err_msg = "Er werden geen gegevens gevonden voor het lid met id ".$spelers_id;
    }
    // format results by row
    $speler = mysql_fetch_object($result);
    mysql_free_result($result);
  }
?>
         <form name="leden" method="post" action="<?php echo basename($PHP_SELF); ?>" enctype="multipart/form-data">

          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td colspan="3" width="100%">
                <table width="100%" border="0" cellspacing="0" cellpadding="2">
<?php
  if (strlen($err_msg) > 0)
  {
    echo "<tr><td colspan=\"7\"><font color=\"red\"><b>".$err_msg."</b></font></td></tr>";
  }
?>
                  <tr>
                    <td class="td2">Naam : </td>
                    <td><input type="text" name="achternaam" size="15" maxlength="30" class="input" value="<?php echo $speler->achternaam ; ?>" onChange="set_update_value('SPELERS');">
                        <input type="text" name="voornaam" size="10" maxlength="20" class="input" value="<?php echo $speler->voornaam ; ?>" onChange="set_update_value('SPELERS');"></td>
                    <td class="td2">Geboren : </td>
                    <td><input type="text" name="geb_dt" size="10" class="input" value="<?php echo $speler->geb_dt ; ?>" style="align: center" onChange="set_update_value('SPELERS');"></td>
                    <td class="td2">Geslacht : </td>
                    <td><select name="geslacht" class="input" onChange="set_update_value('SPELERS');">
<?php
  build_options('geslacht', $speler->geslacht, $badm_db);
?>
                        </select></td>
                    <td rowspan="6"><img src="get_pasfoto.php?id=<?=$spelers_id?>" alt="" width="90" height="130"></td>
                  </tr>
                  <tr>
                    <td class="td2">Adres : </td>
                    <td><input type="text" name="adres" size="30" class="input" value="<?php echo $speler->adres ; ?>" onChange="set_update_value('SPELERS');"></td>
                    <td class="td2">Tel : </td>
                    <td><input type="text" name="tel" size="10" class="input" value="<?php echo $speler->tel ; ?>" onChange="set_update_value('SPELERS');"></td>
                    <td class="td2">Postcode : </td>
                    <td><input type="text" name="postcode" size="4" maxlength="6" class="input" value="<?php echo $speler->postcode ; ?>" style="align: right" onChange="set_update_value('SPELERS');"></td>
                  </tr>
                  <tr>
                    <td class="td2">Gemeente : </td>
                    <td><input type="text" name="gemeente" size="30" maxlength="36" class="input" value="<?php echo $speler->woonplaats ; ?>" onChange="set_update_value('SPELERS');"></td>
                    <td class="td2">Gsm : </td>
                    <td><input type="text" name="gsm" size="12" class="input" value="<?php echo $speler->gsm ; ?>" onChange="set_update_value('SPELERS');"></td>
                    <td class="td2">T-shirt : </td>
                    <td><select name="maat" class="input" onChange="set_update_value('SPELERS');">
<?php
  build_options('maat', $speler->maat, $badm_db);
?>
                        </select></td>
                  </tr>
                  <tr>
                    <td class="td2">E-mail : </td>
                    <td><input type="text" name="email" size="30" maxlength="100" class="input" value="<?php echo $speler->email ; ?>" onChange="set_update_value('SPELERS');"></td>
                    <td class="td2">Lid vanaf : </td>
                    <td><input type="text" name="club_dt" size="10" class="input" value="<?php echo $speler->club_dt ; ?>" style="align: center" onChange="set_update_value('SPELERS');">
                        <a href="#" onClick="cal.select(document.leden.club_dt, 'anchor1','yyyy/MM/dd'); return false;" name="anchor1" id="anchor1">
                        <img src="../images/b_calendar.png" alt="kalender" border="none"></a></td>
                    <td class="td2">Klassement : </td>
                    <td><select name="klassement" class="input" onChange="set_update_value('SPELERS');">
<?php
  build_options('klassement', $speler->klassement, $badm_db);
?>
                        </select></td>
                  </tr>
                  <tr>
                    <td class="td2">Lidnr : </td>
                    <td><input type="text" name="lidnr" size="8" maxlength="8" class="input" value="<?php echo $speler->lidnr ; ?>" style="align: right" onChange="set_update_value('SPELERS');"></td>
                    <td class="td2">Gestopt op : </td>
                    <td><input type="text" name="eind_dt" size="10" class="input" value="<?php echo $speler->eind_dt ; ?>" style="align: center" onChange="set_update_value('SPELERS');">
                        <a href="#" onClick="cal.select(document.leden.eind_dt, 'anchor2','yyyy/MM/dd'); return false;" name="anchor2" id="anchor2">
                        <img src="../images/b_calendar.png" alt="kalender" border="none"></a></td>
                    <td class="td2">Type Speler : </td>
                    <td><select name="type" class="input" onChange="set_update_value('SPELERS');">
<?php
  build_options('statuut', $speler->type_speler, $badm_db);
?>
                        </select></td>
                  </tr>
                  <tr>
                    <td class="td2">Boekske</td>
                    <td class="td2"><select name="clubblad" class="input" onChange="set_update_value('SPELERS');">
<?php
  build_options('ja_nee', $speler->clubblad, $badm_db);
?>
                        </select>
                        &nbsp;Competitie? <select name="competitie" class="input" onChange="set_update_value('SPELERS');">
<?php
  build_options('competitiespeler', $speler->competitie, $badm_db);
?>
                        </select></td>
                    <td class="td2">Lidgeld : </td>
                    <td><input type="text" name="lidgeld" size="4" class="input" value="<?=$speler->lidgeld?>" style="align: right" onChange="set_update_value('SPELERS');">&nbsp;&euro;</td>
                    <td class="td2">Betaald : </td>
                    <td><input type="text" name="betaald" size="4" class="input" value="<?=$speler->betaald?>" style="align: right" onChange="set_update_value('SPELERS');">&nbsp;&euro;</td>
                  </tr>
                  <tr>
                    <td class="td2">Opmerking : </td>
                    <td colspan="4"><input type="text" name="opmerking" size="90" maxlength="150" class="input" value="<?php echo $speler->remark ; ?>" onChange="set_update_value('SPELERS');"></td>
                    <td colspan="2"><input type="file" class="input" name="pasfoto" size="10" onChange="set_update_value('SPELERS');"><input type="hidden" name="MAX_FILE_SIZE" value="16777216"></td>
                  </tr>
                </table>
              </td>
            </tr>
            <tr>
              <td width="40%"><hr></td>
              <td align="center">&nbsp;<span>Speeluren</span>&nbsp;</td>
              <td width="40%"><hr></td>
            </tr>

            <tr>
              <td colspan="3">

                <table  border="0" cellspacing="0" cellpadding="0" rules="groups">
                 <colgroup span="3">
                 <colgroup span="3">
                 <colgroup span="3">
<?php
  $query = "SELECT LOWER(u.dag) AS dag
                 , u.uur
                 , u.plaats
                 , u.type
                 , u.doelgroep
                 , u.id
                 , lu.spelers_id
                 , CASE u.dag WHEN 'maandag'   THEN 1
                              WHEN 'dinsdag'   THEN 2
                              WHEN 'woensdag'  THEN 3
                              WHEN 'donderdag' THEN 4
                              WHEN 'vrijdag'   THEN 5
                              WHEN 'zaterdag'  THEN 6
                              WHEN 'zondag'    THEN 7
                   END AS dagnummer
                 , lu.reserve
              FROM bad_speeluren u
              LEFT OUTER JOIN leden_uren lu ON u.id = lu.speeluren_id
                                            AND lu.spelers_id = %d
              ORDER BY 8, 2, 3";
  $sql  = sprintf($query, mysql_real_escape_string($spelers_id));
  $result = mysql_query($sql, $badm_db) or badm_mysql_die();

  $totaal = mysql_num_rows($result);
  $num_rows = ceil($totaal / 3);
  $ids = array();
  $checks = array();
  $uren = array();
  $types = array();
  $reserve = array();
  while ($row = mysql_fetch_object($result))
  {
    // Put the data in 4 arrays
    $ids[] = $row->id;
    $checks[] = $row->spelers_id;
    $uren[] = $row->dag."&nbsp;".$row->uur."&nbsp;".initcap($row->plaats);
    $types[] = "&nbsp;".$row->type."&nbsp;".$row->doelgroep."&nbsp;";
    $reserve[] = $row->reserve;
  }
  // Write speeluren
  for ($i=0; $i < $num_rows; $i++)
  {
    echo "                  <tr>\n";
    $checked = is_null($checks[$i]) ? "" : " CHECKED";
    // kolom 1
    write_speeluur($ids[$i], $checked, $uren[$i], $types[$i], $reserve[$i]);
    $index = $i + $num_rows;
    if (array_key_exists((int)$index, $ids))
    {
      $checked = is_null($checks[$index]) ? "" : " CHECKED";
      // kolom 2
      write_speeluur($ids[$index], $checked, $uren[$index], $types[$index], $reserve[$index]);
    }
    else
    {
      echo '<td class="td2">&nbsp;</td><td class="td2">&nbsp;</td><td class="td2">&nbsp;</td>';
    }
    $index = $i + (2 * $num_rows);
    if (array_key_exists((int)$index, $ids))
    {
      $checked = is_null($checks[$i + (2 * $num_rows)]) ? "" : " CHECKED";
      // kolom 3
      write_speeluur($ids[$index], $checked, $uren[$index], $types[$index], $reserve[$index]);
    }
    else
    {
      echo '<td class="td2">&nbsp;</td><td class="td2">&nbsp;</td><td class="td2">&nbsp;</td>';
    }
    echo "                  </tr>\n";
  }
  $checked = $speler->administratief_lid=="J" ? " CHECKED" : "";
  echo '                    <td class="td2"><input type="checkbox" name="administratief_lid" class="input" style="border: none"'.$checked.' onChange="set_update_value(\'SPELERS\');"></td>';
  echo '                    <td colspan="2" class="td2">'; echo ($checked==' CHECKED') ? '<b>' : ''; echo 'Administratief lid'; echo ($checked==' CHECKED') ? '</b>' : ''; echo '</td>';
?>
                </table>

              </td>
            </tr>

            <tr>
              <td width="40%"><hr></td>
              <td align="center">&nbsp;<span>Competitie</span>&nbsp;</td>
              <td width="40%"><hr></td>
            </tr>

            <tr>
              <td colspan="3" style="padding-left: 10px">

                <table  border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td colspan="7" align="center" class="td2">
<?php
  if ($speler->geslacht == 'M')
  {
    $reeks = "HEREN";
  }
  else
  {
    $reeks = "DAMES";
  }
  echo initcap($reeks);
?>
                    </td>
                    <td width="30">&nbsp;</td>
                    <td colspan="7" align="center" class="td2">Gemengd</td>
                  </tr>
<?php
  // Dames of heren ploegen
  $query1 = "SELECT c.id
                  , c.afdeling
                  , lc.type
                  , lc.basis
               FROM bad_competitieploegen c
               LEFT OUTER JOIN leden_competitie lc ON c.id = lc.competitieploegen_id
                AND lc.spelers_id = %d
              WHERE c.type = '%s'
          AND c.seizoen = '%s'
           ORDER BY ploegnummer";
  $sql1  = sprintf($query1, mysql_real_escape_string($spelers_id)
                          , mysql_real_escape_string($reeks)
              , mysql_real_escape_string($seizoen));
  $result1 = mysql_query($sql1, $badm_db) or badm_mysql_die();
  $totaal1 = mysql_num_rows($result1);
  // Gemengde ploegen
  $query2 = "SELECT c.id
                  , c.afdeling
                  , lc.type
                  , lc.basis
               FROM bad_competitieploegen c
               LEFT OUTER JOIN leden_competitie lc ON c.id = lc.competitieploegen_id
                AND lc.spelers_id = %d
              WHERE c.type = 'GEMENGD'
          AND c.seizoen = '%s'
           ORDER BY ploegnummer";
  $sql2  = sprintf($query2, mysql_real_escape_string($spelers_id)
                          , mysql_real_escape_string($seizoen));
  $result2 = mysql_query($sql2, $badm_db) or badm_mysql_die();
  $totaal2 = mysql_num_rows($result2);

  $max = max($totaal1, $totaal2);
  for ($i=1; $i <= $max; $i++)
  {
    echo "                  <tr>\n";
    if ($row = mysql_fetch_object($result1))
    {
      write_competitie($row->id, $row->afdeling, $row->type, $row->basis);
    }
    else
    {
      echo "<td colspan=\"7\">\n";
    }
    echo "                    <td>&nbsp;</td>\n";
    if ($mix = mysql_fetch_object($result2))
    {
      write_competitie($mix->id, $mix->afdeling, $mix->type, $mix->basis);
    }
    else
    {
      echo "<td colspan=\"7\">\n";
    }
    echo "                  <tr>\n";
  }
?>
                </table>
              </td>
            </tr>

            <tr>
              <td colspan="3" align="center"><hr>
               <!-- nagaan of een update moet gebeuren -->
               <input type="hidden" name="update_spelers" value="0">
               <input type="hidden" name="update_speeluren" value="0">
               <input type="hidden" name="update_competitie" value="0">
               <input type="hidden" name="totaal_uren" value="">
               <input type="hidden" name="totaal_comp" value="">

               <input type="hidden" name="spelers_id" value="<?php echo $spelers_id; ?>">
               <input type="hidden" name="page" value="bewerken_lid.php">
               <input type="hidden" name="command" value="query">
               <input type="hidden" name="seizoen" value="<?php echo $seizoen; ?>">
<?php
  if (isset($_SESSION['administrator']) || isset($_SESSION['competitieverantwoordelijke']) || isset($_SESSION['ledenbeheer']))
  {
?>
               <button type="button" name="save" accesskey="S" title="de gemaakte wijzigingen opslaan" onClick="return doSubmit('update');"><span style="text-decoration: underline">S</span>ave</button>
<?php
  }
  if (isset($_SESSION['administrator']))
  {
?>
               <button type="button" name="remove" accesskey="V" title="Verwijder deze speler" onClick="return doSubmit('delete');"><span style="text-decoration: underline">V</span>erwijder</button>
<?php
  }
?>
              </td>
            </tr>

           </form>

          </table>
      </td></tr>
    </table>
  </td></tr>
</table>

<!-- Division voor de kalender popup-->
<div id="datediv" style="position:absolute; visibility:hidden; background-color:white; layer-background-color:white;"></div>

</body>
</html>
<?php
  //mysql_close($badm_db);
?>