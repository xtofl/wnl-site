<?php
/**
 * instellingen.php
 *
 * author    : Freek Ceymeulen
 * created   : 01/02/2005
 * variables :
 **/

  // Connect to the DB (must be done before calling sess.php)
  require_once "../functies/badm_db.inc.php";
  $badm_db = badm_conn_db();
  require_once('../functies/sess.php');

  if ($_SESSION['auth'] != true)
  {
    // niet ingelogd -> ga naar login pagina
    // geef deze pagina als parameter mee zodat na succesvolle login teruggekeerd kan worden naar deze pagina
    //session_write_close;
    mysql_close($badm_db);
    header("Location: http://www.badmintonsport.be/admin/login.php?ref=".basename($PHP_SELF));
    exit;
  }
/*
--------------------------------------------------------------------------------------------
|| FUNCTIONS
--------------------------------------------------------------------------------------------
*/
  require_once("../functies/general_functions.php");
/*
--------------------------------------------------------------------------------------------
|| BEGIN
--------------------------------------------------------------------------------------------
*/
  $domain = NULL;
  if (isset($_GET['domain']))
  {
    $domain = $_GET['domain'];
  }
?>
<html>
<head>
<title>W&amp;L Admin Module</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="http://www.badmintonsport.be/css/admin.css" rel="stylesheet" type="text/css">
<script>
<!--
function doSubmit(p_action)
{
  if (p_action == 'insert')
  {
    if (document.forms[0].value.value != '' && document.forms[0].description.value != '')
    {
      //document.forms[0].command.value = 'insert';
      document.forms[0].action = 'dml.php';
    }
    else
    {
      alert('Vul zowel een waarde als een omschrijving in');
      document.forms[0].value.focus();
      return;
    }
  }
  else
  {
    document.forms[0].action = 'instellingen.php?domain='+p_action;
  }
  document.forms[0].submit();
}
-->
</script>
</head>
<body bgcolor="#3A6EA5" link="#000000" vlink="#000000" topmargin="0">

<span style="font-size: 8pt; float: right"><?php echo $_SESSION['username']; ?></span>
<?php
  list($wday,$mday,$month,$year,$hour,$minutes) = split("( )",date("w j n Y H i",time()));
  $weekday = array('zondag', 'maandag', 'dinsdag', 'woensdag', 'donderdag', 'vrijdag', 'zaterdag');
  $months  = array('januari','februari','maart','april','mei','juni','juli','augustus','september','oktober','november','december');
  $newdate = "$weekday[$wday], $mday ".$months[$month-1]." $year $hour:$minutes";
?>
<span style="font-size: 8pt"><?php echo $newdate; ?></span>

<table border="1" cellspacing="0" cellpadding="0" align="center" width="810">
  <tr bgcolor="#C6C3C6" valign="top"><td>
    <table width="100%" border="0" cellspacing="0" cellpadding="1" align="center">
      <tr bgcolor="#400080">
        <td height="20" class="title">&nbsp;W&amp;L - Configuratie</td>
        <td height="20" align="right" bgcolor="#000084">
          <a href="http://www.badmintonsport.be/docs/W_L_admin_users_guide.doc" target="_blank"><img src="../poll/image/help.gif" width="16" height="14" border="0" alt="Gebruikershandleiding"></a><a href="login.php?action=logout"><img src="../poll/image/cross.gif" width="16" height="14" border="0" alt="Uitloggen"></a></td>
      </tr>
    </table>
    <table border="0" cellspacing="0" cellpadding="0">
      <tr valign="top">
        <td width="100">
<?php
  // Print the menu
  write_menu($_SERVER['PHP_SELF']);
?>
        </td>
        <td>
          <form name="instellingen" action="instellingen.php" method="POST">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td>
                <table border="0" cellspacing="0" cellpadding="3">
                  <tr>
                    <td class="td2">Kies een domein:</td>
                    <td class="td2">
<?php
  $query1 = "SELECT DISTINCT domain
              FROM codes";
  $result = mysql_query($query1, $badm_db) or badm_mysql_die();

  $first_row = True;
  while ($row = mysql_fetch_object($result))
  {
    if (is_null($domain))
    {
      echo '<input type="radio" name="domein" value="'.$row->domain.'" CHECKED onClick="doSubmit(\''.$row->domain.'\');">'.$row->domain;
      $domain = $row->domain;
    }
    else
    {
      echo '<br/><input type="radio" name="domein" value="'.$row->domain.'" '; echo ($domain == $row->domain) ? "CHECKED" : ""; echo ' onClick="doSubmit(\''.$row->domain.'\');">'.$row->domain;
    }
  }
  mysql_free_result($result);
?>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
            <tr>
              <td><hr>
                <table border="0" cellspacing="0" cellpadding="3">
                  <tr>
                    <th>Waarde</th>
                    <th>Omschrijving</th>
                  </tr>
                <tbody>
<?php
  $query2 = "SELECT value
                  , description
               FROM codes
              WHERE domain = '%s'
           ORDER BY 1";
  $sql  = sprintf($query2, mysql_real_escape_string($domain));
  $result = mysql_query($sql, $badm_db) or badm_mysql_die();

  for ($i=0; $i<mysql_num_rows($result); $i++)
  {
    $row = mysql_fetch_assoc($result);
    echo '                  <tr class="'; echo ($i%2==0) ? "even" : "odd"; echo '">';
    echo '                    <td class="td2">'.$row['value'].'</td>';
    echo '                    <td class="td2">'.$row['description'].'</td>';
    echo '                  </tr>';
  }
  mysql_free_result($result);
?>
                  <tr>
                    <td class="td2"><input type="text" class="input" name="value" size="10"></td>
                    <td class="td2"><input type="text" class="input" name="description" size="15"></td>
                  </tr>
                </table>
              </td>
            </tr>
            <tr>
              <td class="td2" colspan="2" align="center"><hr>
                <input type="hidden" name="domain" value="<?php echo $domain; ?>">
                <input type="hidden" name="page" value="instellingen.php">
                <input type="hidden" name="command" value="insert">
                <button type="button" accesskey="S" title="Voeg een nieuwe code toe aan dit domein" onClick="doSubmit('insert');"><span style="text-decoration: underline">S</span>ave</button>
              </td>
            </tr>
          </table>
          </form>
        </td>
      </tr>
    </table>
  </td></tr>
</table>
<br>
<br>
</body>
</html>
<?php
  // Sla de uitgevoerde queries op in audit tabel
/*
  $audit = "INSERT INTO admin_audit (user_id, actie, id, inhoud)
            VALUES ('%s', 'QUERY', 0, '%s')";
  $sql  = sprintf($audit, mysql_real_escape_string($_SESSION['usid'])
                        , mysql_real_escape_string(addslashes($sql_stmt1)));
  $result = mysql_query($sql, $badm_db) or die("Invalid query: " . mysql_error());

  $audit = "INSERT INTO admin_audit (user_id, actie, id, inhoud)
            VALUES ('%s', 'QUERY', 0, '%s')";
  $sql  = sprintf($audit, mysql_real_escape_string($_SESSION['usid'])
                        , mysql_real_escape_string(addslashes($sql_stmt2)));
  $result = mysql_query($sql, $badm_db) or die("Invalid query: " . mysql_error());
*/
  //mysql_close($badm_db);
?>