<?php

 /*
 * display_image.php
 * Fetching a image out of the database
 * Author    : Freek Ceymeulen
 * Date      : 29/05/2006
 * Parameter : id : image_id
 */

/*--------------------------------------------------------------------------------------------
 | SECURITY
 --------------------------------------------------------------------------------------------*/

  // Connect to the DB (must be done before calling sess.php)
  require_once "../functies/badm_db.inc.php";
  $badm_db = badm_conn_db();
  require_once('../functies/sess.php');

  if ($_SESSION['auth'] != true)
  {
    // niet ingelogd -> ga naar login pagina
    //session_write_close;
    mysql_close($badm_db);
    header("Location: http://www.badmintonsport.be/admin/login.php");
    exit;
  }

// Assert parameter
  if (isset($_REQUEST['id']) && is_numeric($_REQUEST['id']))
  {
    $image_id = $_REQUEST['id'];

    $sql = "SELECT data
                 , name
                 , mime_type
              FROM images
             WHERE id = %d";

    $query = sprintf($sql, $image_id);

    $result = mysql_query($query, $badm_db);

    $row = mysql_fetch_row($result);
    $data = $row[0];
    $file = htmlspecialchars($row[1]);

    if (is_null($row[2]))
    {
      $mimetype = array( 'jpg'=>'image/jpeg'
                       , 'jpeg'=>'image/jpeg'
                       , 'gif'=>'image/gif'
                       , 'png'=>'image/x-png'
                       , 'png'=>'image/png'
                       , 'bmp'=>'image/bmp'
                       , 'jpg'=>'image/pjpeg'
                       , 'bmp'=>'image/x-MS-bmp' );

      $p = explode('.', $file);
      $pc = count($p);

      //send headers
      if ($pc > 1 && isset($mimetype[$p[$pc - 1]]))
      {
        header("Content-Type: " . $mimetype[$p[$pc - 1]] . "\n");
      }
      else
      {
        header("Content-Type: application/octet-stream\n");
        header("Content-Disposition: attachment; filename=\"$file\"\n");
      }
    }
    else
    {
      header("Content-Type: " . $row[2] . "\n");
    }
    header("Content-Transfer-Encoding: binary\n");
    header("Content-length: " . strlen($data) . "\n");

    //send file contents
    print($data);

    mysql_free_result($result);
    // Close DB connection
  }
?>

