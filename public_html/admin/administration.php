<?php

/**
 * administration.php
 *
 * author     : Freek Ceymeulen
 * created    : 01/02/2007
 * parameters : start_date   date since when admin module usage should be displayed
 *              submit       [Requery|Delete]
 *              msg_id       message to be deleted
 *              del_cascade  if this variable is available then also message replies should be deleted
 *              player       name of player for which to show user credentials
 **/

  // Connect to the DB (must be done before calling sess.php)
  require_once "../functies/badm_db.inc.php";
  $badm_db = badm_conn_db();
  require_once('../functies/sess.php');

  if (!isset($_SESSION['auth']))
  {
    // niet ingelogd -> ga naar login pagina
    // geef deze pagina als parameter mee zodat na succesvolle login teruggekeerd kan worden naar deze pagina
    //session_write_close;
    mysql_close($badm_db);
    header("Location: http://www.badmintonsport.be/admin/login.php?ref=".basename($PHP_SELF));
    exit;
  }

  // Alleen webmasters mogen deze pagina gebruiken
  if (!isset($_SESSION['administrator']))
  {
    mysql_close($badm_db);
    header("Location: http://www.badmintonsport.be/admin/login");
    exit;
  }

  define("ERROR_IMG", "<img src=\"../images/error.gif\" height=\"16\" width=\"16\" alt=\"Error\">");
  define("INFO_IMG" , "<img src=\"../images/info.gif\" height=\"16\" width=\"16\" alt=\"Info\">");

  require_once("../functies/general_functions.php");

  // Display the error for the given field, if any
  function fieldError($fieldName, $errors)
  {
    if (isset($errors[$fieldName]))
    {
      echo ERROR_IMG." <font color=\"red\">".$errors[$fieldName]."</font><br>";
    }
  }

  // Register an error array - just in case!
  if (!session_is_registered("errors"))
  {
    session_register("errors");
  }
  // Clear any errors that might have been found previously
  $errors = array();
  // Initialize message
  $msg = "";
  // Initialize date that defines since when admin module usage data will be retrieved
  if (isset($_REQUEST["start_date"]))
  {
    $start_date = $_REQUEST["start_date"];
  }
  else
  {
    // Default today
    $start_date = date("d/m/Y");
  }
  // Handle a submit
  if (isset($_REQUEST["submit"]))
  {
    switch ($_REQUEST["submit"])
    {
      case "Requery":
        $action = "Requery";
        break;
      case "Delete":
        $action = "Delete";
        if (isset($_REQUEST["msg_id"]))
        {
          $msg_id = $_REQUEST["msg_id"];
          // Validate msg_id
          if (empty($msg_id))
          {
            $errors["msg_id"] = "You have not entered a Message Id.";
          }
          elseif (!is_numeric($msg_id))
          {
            $errors["msg_id"] = "Invalid Message Id ".$msg_id;
          }
          else
          {
            if (isset($_REQUEST["del_cascade"]))
            {
              // Delete all replies to this message first
              $delete_statement = "DELETE FROM gb_replies"
                                 ." WHERE bericht_id = %d";
              $sql = sprintf($delete_statement, mysql_real_escape_string($msg_id));
              mysql_query($sql) or die('Delete failed: ' . mysql_error());
              // Give feedback about what happened
              if (mysql_affected_rows() == 1)
              {
                $msg = INFO_IMG." 1 reply deleted.<br>";
              }
              else
              {
                $msg = INFO_IMG." ".mysql_affected_rows()." replies deleted.<br>";
              }
            }
            // Now delete the message itself
            $delete_statement = "DELETE FROM gb_berichten"
                               ." WHERE id = %d";
            $sql  = sprintf($delete_statement, mysql_real_escape_string($msg_id));
            mysql_query($sql) or die('Delete failed: ' . mysql_error());
            // Give feedback about what happened
            if (mysql_affected_rows() == 1)
            {
              $msg .= INFO_IMG." Message ".$msg_id." deleted.";
              $msg_id = "";
            }
            else
            {
              $msg .= ERROR_IMG." Message ".$msg_id." could NOT be deleted.";
              $msg_id = "";
            }
          }
        }
        break;
      case "Show":
        $action = "Show";
        if (isset($_REQUEST["player"]))
        {
          $player = $_REQUEST["player"];
          // Validate player
          if ( strlen($player) < 2 || strlen($player) > 61)
          {
            $errors["player"] = "Name of player must be longer than 2 and smaller than 61 characters";
          }
        }
        else
        {
          $errors["player"] = "Enter a player.";
        }
        break;
    } // end switch
  } // end submit
  else
  {
    $action = "";
    $msg_id = "";
    $player = "";
  }
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html lang="en">

<head>
 <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
 <title>W&amp;L - Management</title>
 <link rel="stylesheet" type="text/css" href="">
 <style>
div.title {
    background-color: #ccc;
    border-color: black;
    border-style: solid;
    border-width: 1px;
    margin-left: 10px;
    padding: 5px;
    width: 95%;
}
div.content {
    margin-left   : 20px;
    padding-top   : 20px;
    padding-bottom: 20px;
}
td {
    padding-left : 6px;
    padding-right: 6px;
}
 </style>
</head>

<body>

<form name="form" action="<?php echo $PHP_SELF; ?>" method="POST">

<div style="float:right;">
 You are logged in as <?php echo $_SESSION['username']; ?><br>
 <font size="-1">If you are not <?php echo $_SESSION['username']; ?>, click <a href="http://www.badmintonsport.be/admin/login.php?action=logout" target="" title="logout">here</a></font>
</div>
W&amp;L administration page
<br>
<br>
<div style="padding-left: 30px; background-color: #FAF8CC;"><?php echo $msg; ?></div>
<br>

<div class="title">Delete message from messageboard</div>

<div class="content">
<?php
  // Print errors (if any)
  fieldError("msg_id", $errors);
?>
 <label for="msg_id">Message Id</label>
 <input type="text" name="msg_id" value="<?php echo $msg_id; ?>" size="10" maxlength="10">
 <input type="submit" name="submit" value="Delete" onClick="return (document.form.msg_id.value.length>0 && confirm('Delete message '+document.form.msg_id.value+'?'));">
 <input type="checkbox" name="del_cascade">
 <label for="del_cascade">Delete replies</label>
</div>

<div class="title">Get login information</div>

<div class="content">
<?php
  fieldError("player", $errors);
?>
 <label for="player">Player (first name + last name)</label>
 <input type="text" name="player" value="<?php echo $player; ?>" size="30" maxlength="61">
 <input type="submit" name="submit" value="Show" onClick="return (document.form.player.value.length()>0);">
</div>

<?php
  if ($action == "Show" && count($errors) == 0)
  {
?>
<div class="content">
<table cellspacing="0" border="1">
 <tr>
  <th>Player</th>
  <th>Username</th>
  <th>Password</th>
  <th>Level</th>
 </tr>
<?php
    // Get the credentials for the requested user
    $query = "SELECT naam"
            ."     , usid"
            ."     , paswoord"
            ."     , level"
            ."  FROM users"
            ." WHERE UPPER(naam) LIKE '%%%s%%'"
            ." ORDER BY naam";
    $sql  = sprintf($query, mysql_real_escape_string(strtoupper($player)));
    $result = mysql_query($sql) or die('Query failed: ' . mysql_error());
    if (mysql_num_rows($result) == 0)
    {
      echo "<tr><td colspan=\"4\">".INFO_IMG." No data found for player ".mysql_real_escape_string($player)."</td></tr>\n";
    }
    else
    {
      while ($line = mysql_fetch_array($result, MYSQL_ASSOC))
      {
        echo " <tr>\n";
        foreach ($line as $col_value)
        {
          echo "  <td>$col_value</td>\n";
        }
        echo " </tr>\n";
      }
    }
    mysql_free_result($result);
?>
</table>
</div>
<?php
  }
?>

<div class="title">Who is using the admin module right now?</div>

<div class="content">
<table cellspacing="0" border="1">
 <tr>
  <th>Username</th>
  <th>Page</th>
  <th>Start Date</th>
  <th>Expire Date</th>
  <th>Session Variables</th>
 </tr>
<?php
  // Get the people that are currently using the admin module
  $query = "SELECT username"
          ."     , page"
          ."     , DATE_FORMAT(FROM_UNIXTIME(ses_start),'%d/%m/%Y %T') AS ses_start"
          ."     , DATE_FORMAT(FROM_UNIXTIME(ses_expire),'%d/%m/%Y %T') AS ses_expire"
          ."     , ses_value"
          ."  FROM sessions"
          ." ORDER BY ses_start DESC";
  //$sql  = sprintf($query, mysql_real_escape_string($));
  $result = mysql_query($query) or die('Query failed: ' . mysql_error());
  while ($line = mysql_fetch_array($result, MYSQL_ASSOC))
  {
    echo " <tr>\n";
    foreach ($line as $col_value)
    {
      echo "  <td>".nvl(str_replace(";", "<br>", $col_value), "&nbsp;")."</td>\n";
    }
    echo " </tr>\n";
  }
  mysql_free_result($result);
?>
</table>
</div>

<div class="title">
 <span style="float:right;">
  since <input type="text" name="start_date" value="<?php echo $start_date; ?>" size="10" maxlength="10">
  <input type="submit" name="submit" value="Requery">
 </span>
 Admin Module Usage history
</div>

<div class="content">
<table cellspacing="0" border="1">
 <tr>
  <th>Username</th>
  <th>Start Date</th>
  <th>End Date</th>
  <th>IP Address</th>
  <th>Browser</th>
 </tr>
<?php
  // Convert date to YYYY/MM/DD format
  $start_date = get_valid_date($start_date);
  // Get the admin module usage data since given day until now
  $query = "SELECT user"
          ."     , DATE_FORMAT(start_time,'%%d/%%m/%%Y %%T') AS start_time"
          ."     , DATE_FORMAT(stop_time,'%%d/%%m/%%Y %%T') AS stop_time"
          ."     , ip"
          ."     , browser"
          ."  FROM admin_logging"
          ." WHERE start_time > '%s'"
          ." ORDER BY start_time DESC";
  $sql  = sprintf($query, mysql_real_escape_string($start_date));
  $result = mysql_query($sql) or die('Query failed: ' . mysql_error());
  while ($line = mysql_fetch_array($result, MYSQL_ASSOC))
  {
    echo " <tr>\n";
    foreach ($line as $col_value)
    {
      echo "  <td><font size=\"-1\">".nvl($col_value, "&nbsp;")."</font></td>\n";
    }
    echo " </tr>\n";
  }
  mysql_free_result($result);
?>
</table>
</div>

</form>
</body>
</html>