<?php
/**
 * print_etiketten.php
 * 
 * object     : creates a html file with the layout specified in etiketten_param_sheet.php
 * author     : Freek Ceymeulen
 * created    : 13/10/2005
 * parameters : format       : (optional) Page format. Possible values are:
 *                             - A3
 *                             - A4 (default)
 *                             - A5
 *                             - A6
 *                             - letter
 *                             - legal
 *                             - executive
 *                             - custom
 *              pagewidth    : (optional) Page width (in mm)
 *              pageheight   : (optional) Page height (in mm)
 *              orientation  : (optional) Page orientation.  Possible values are:
 *                             - P(ortrait) (default)
 *                             - L(andscape)
 *              topmargin    : (optional) Distance (in mm) from top of page to first label. default 15mm
 *              leftmargin   : (optional) Distance (in mm) from left side of page to start of first label. default 7mm
 *              bottommargin : (optional) Distance (in mm) from bottom of last label to bottom of page; defaults to topmargin
 *              columns      : (optional) Number of labels in horizontal direction on one page. default 3
 *              rows         : (optional) Number of labels in vertical direction on one page. default 7
 *              horspace     : (optional) Horizontal space between 2 labels. default 2.5mm
 *              vertspace    : (optional) Vertical space between 2 labels. default 0mm
 *              padding      : (optional) Distance between label edge and start of text (in mm)
 *              family       : (optional) Font family. default Arial
 *              size         : (optional) Font size (in points). default 9pt
 *              weight       : (optional) Font weight. default normal
 *              labelcolor   : (optional) Label background color. default white
 *              textcolor    : (optional) Text color. default black
 *              align        : (optional) Text align. default left
 *              transform    : (optional) Text transform.  Possible values are:
 *                             - none (default)
 *                             - capitalize
 *                             - uppercase
 *                             - lowercase
 **/
/*
  session_start();
  if ($_SESSION['auth'] != true)
  {
    // niet ingelogd -> ga naar login pagina
    // na succesvolle login terugkeren naar ledenpagina
    session_write_close;
    header("Location: http://www.badmintonsport.be/admin/login.php?ref=leden.php");
    exit;
  }
*/
  // Receive parameters
  // 1. Page format
  if (isset($_REQUEST["format"]))
  {
    $format = strtolower($_REQUEST["format"]);
    // Define page width and height (in mm)
    switch ($format)
    {
      case "a4":
        $page_width = 210;
        $page_height = 297;
        break;
      case "custom":
        if (isset($_REQUEST["pagewidth"]))
        {
          $page_width = $_REQUEST["pagewidth"];
        }
        else
        {
          $page_width = 210; // A4
        }
        if (isset($_REQUEST["pageheight"]))
        {
          $page_height = $_REQUEST["pageheight"];
        }
        else
        {
          $page_height = 297; // A4
        }
        break;
      case "a3":
        $page_width = 420;
        $page_height = 297;
        break;
      case "a5":
        $page_width = 148;
        $page_height = 210;
        break;
      case "a6":
        $page_width = 74;
        $page_height = 105;
        break;
      case "letter":
        $page_width = 215.9;
        $page_height = 279.4;
        break;
      case "legal":
        $page_width = 215.9;
        $page_height = 355.6;
        break;
      case "executive":
        $page_width = 184.2;
        $page_height = 26.67;
        break;
      default:  //A4
        $page_width = 210;
        $page_height = 297;
    }
  }
  else
  {
    // default A4
    $format = "a4";
    $page_width = 210;
    $page_height = 297;
  }
  
  // 2. Page orientation
  if (isset($_REQUEST["orientation"]))
  {
    $orientation = strtolower($_REQUEST["orientation"]);
    if ($orientation == "l" || $orientation == "landscape")
    {
      // switch page width and height
      $temp = $page_width;
      $page_width = $page_height;
      $page_height = $temp;
    }
  }
  
  // 3. Page margins
  if (isset($_REQUEST["topmargin"]))
  {
    $topmargin = $_REQUEST["topmargin"];
  }
  else
  {
    $topmargin = 15;
  }
  if (isset($_REQUEST["leftmargin"]))
  {
    $leftmargin = $_REQUEST["leftmargin"];
  }
  else
  {
    $leftmargin = 7;
  }
  if (isset($_REQUEST["bottommargin"]))
  {
    $bottommargin = $_REQUEST["bottommargin"];
  }
  else
  {
    $bottommargin = $topmargin;
  }
  
  // 4. # columns
  if (isset($_REQUEST["columns"]))
  {
    $columns = $_REQUEST["columns"];
  }
  else
  {
    $columns = 3;
  }
  
  // 4. # rows
  if (isset($_REQUEST["rows"]))
  {
    $rows = $_REQUEST["rows"];
  }
  else
  {
    $rows = 7;
  }
  
  // 5. Distance between 2 labels
  if (isset($_REQUEST["horspace"]))
  {
    $horspace = $_REQUEST["horspace"];
  }
  else
  {
    $horspace = 2.5;
  }
  if (isset($_REQUEST["vertspace"]))
  {
    $vertspace = $_REQUEST["vertspace"];
  }
  else
  {
    $vertspace = 0;
  }
  
  // 6. Fonts
  if (isset($_REQUEST["family"]))
  {
    $family = $_REQUEST["family"];
  }
  else
  {
    $family = "Arial";
  }
  if (isset($_REQUEST["size"]))
  {
    $size = $_REQUEST["size"];
  }
  else
  {
    $size = 9;
  }
  if (isset($_REQUEST["weight"]))
  {
    $weight = $_REQUEST["weight"];
  }
  else
  {
    $weight = "normal";
  }
  
  // 7. Colors
  if (isset($_REQUEST["labelcolor"]))
  {
    $labelcolor = $_REQUEST["labelcolor"];
  }
  else
  {
    $labelcolor = "white";
  }
  if (isset($_REQUEST["textcolor"]))
  {
    $textcolor = $_REQUEST["textcolor"];
  }
  else
  {
    $textcolor = "black";
  }
  
  // 8. Text
  if (isset($_REQUEST["align"]))
  {
    $align = $_REQUEST["align"];
  }
  else
  {
    $align = "left";
  }
  if (isset($_REQUEST["transform"]))
  {
    $transform = $_REQUEST["transform"];
  }
  else
  {
    $transform = "none";
  }
  
  // 9. Label measures
  if (isset($_REQUEST["padding"]))
  {
    $padding = $_REQUEST["padding"];
  }
  else
  {
    $padding = 4;
  }
  if (isset($_REQUEST["labelwidth"]))
  {
    $labelwidth = $_REQUEST["labelwidth"] - (2 * $padding);
  }
  else
  {
    $labelwidth = (($page_width - ($leftmargin * 2) - ($horspace * ($columns - 1))) / $columns) - (2 * $padding);
  }
  if (isset($_REQUEST["labelheight"]))
  {
    $labelheight = $_REQUEST["labelheight"] - (2 * $padding);
  }
  else
  {
    $labelheight = (($page_height - ($topmargin + $bottommargin)) / $rows) - (2 * $padding);
  }
  
?>

<html>
<head>
<title>Print etiketten</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<style type="text/css">
<!--
div {
    background-color : <?=$labelcolor?>;
    color : <?=$textcolor?>;
    font-family : <?=$family?>;
    font-size : <?=$size?>pt;
    font-weight : <?=$weight?>;
    height : <?=round($labelheight, 2)?>mm;
    max-width : <?=round($labelwidth, 2)?>mm;
    max-height : <?=round($labelheight, 2)?>mm;
    padding : <?=$padding?>mm;
    position : absolute;
    text-align : <?=$align?>;
    text-transform : <?=$transform?>;
    width : <?=round($labelwidth, 2)?>mm;
}
-->
</style>
</head>

<body>

<?php
  // Connect to db
  require_once "../functies/badm_db.inc.php";
  $badm_db = badm_conn_db();
  
  // Get data
  $query = "SELECT naam
                 , adres
                 , postcode
                 , woonplaats
             FROM bad_spelers
            WHERE eind_dt IS NULL
              AND clubblad = 'J'
            ORDER BY SUBSTRING(naam, LOCATE(' ', naam) + 1, LENGTH(naam) - LOCATE(' ', naam))";
  //$sql  = sprintf($query, mysql_real_escape_string($orderBy));
  $sql = $query;
  $result = mysql_query($sql, $badm_db) or badm_mysql_die();
  
  $i = 0;
  while ($row = mysql_fetch_object($result))
  {
    $left = $leftmargin + (($i % $columns) * ($labelwidth + $horspace));
    $top = $topmargin + (($labelheight + $vertspace) * floor($i / $columns)) + (($topmargin + $bottommargin) * floor($i / ($columns * $rows)));
    echo "<div style=\"left: ".round($left, 2)."mm; top: ".round($top, 2)."mm;\">\n";
    echo $row->naam."<br>\n";
    echo $row->adres."<br>\n";
    echo $row->postcode." ".$row->woonplaats."\n";
    echo "</div>\n";
    $i++;
  }
    
  mysql_free_result($result);
  //mysql_close($badm_db);
?>

</body>
</html>