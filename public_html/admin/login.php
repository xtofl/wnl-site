<?php
/**
 * login.php
 *
 * object     : Login screen for the admin module
 * author     : Freek Ceymeulen
 * created    : 01/02/2005
 *              08/09/2013 Login voor trainers toegevoegd
 * parameters : ref : page that called login.php
 *              action : login/logout
 **/

  // Connect to the DB (must be done before calling sess.php)
  require_once "../functies/badm_db.inc.php";
  $badm_db = badm_conn_db();
  require_once('../functies/sess.php');

/*
--------------------------------------------------------------------------------------------
|| FUNCTIONS
--------------------------------------------------------------------------------------------
*/
  require("../functies/general_functions.php");

  function log_start_time ($usernaam, $sessie_id, $conn)
  {
    $insert_stmt = "INSERT INTO admin_logging (user, session_id, ip, browser, url, referer, start_time)
                    VALUES ('%s', '%s', '%s', '%s', '%s', '%s', NOW())";
    $sql  = sprintf($insert_stmt, mysql_real_escape_string($usernaam)
                                , mysql_real_escape_string($sessie_id)
                                , mysql_real_escape_string($_SERVER['REMOTE_ADDR'])
                                , mysql_real_escape_string($_SERVER['HTTP_USER_AGENT'])
                                , mysql_real_escape_string($_SERVER['REQUEST_URI'])
                                , mysql_real_escape_string($_SERVER['HTTP_REFERER']));
    $result = mysql_query($sql, $conn) or badm_mysql_die();
  }

  function log_end_time ($sessie_id, $conn)
  {
    $update_stmt = "UPDATE admin_logging
                       SET stop_time = NOW()
                     WHERE session_id = '%s'";
    $sql  = sprintf($update_stmt, mysql_real_escape_string($sessie_id));
    $result = mysql_query($sql, $conn) or badm_mysql_die();
  }
/*
--------------------------------------------------------------------------------------------
|| BEGIN
--------------------------------------------------------------------------------------------
*/
if (isset($_REQUEST['ref']))
{
   $ref = $_REQUEST['ref'];
}
  if (!isset($ref) || $ref == 'bewerken_lid.php' || $ref == 'bewerken_ploeg.php' || $ref == 'bewerken_speeluur.php') // deze aanroepende pagina's vragen om een id en kunnen daarom niet rechtstreeks aangeroepen worden
  {
    $ref = 'leden.php';
  }

$message = 'Vul een geldige gebruikersnaam en wachtwoord in';
//$message .= '<font color="red">! Onderhoud bezig...</font> - Freek';

if (isset($_REQUEST['action']))
{
if ($_REQUEST['action'] == 'logout')
{
  log_end_time($_SESSION['id'], $badm_db);
  session_unset();
  session_destroy();
}
elseif ($_REQUEST['action'] == 'login')
{
  $usernaam = $_POST["usid"];
  $query = "SELECT s.naam
                 , f.functie
                 , f.omschrijving
                 , lf.ref_id
              FROM bad_spelers s
        INNER JOIN leden_functies lf       ON s.id = lf.spelers_id
        INNER JOIN clubfuncties f          ON f.id = lf.functies_id
   LEFT OUTER JOIN bad_competitieploegen c ON c.id = lf.ref_id
   LEFT OUTER JOIN bad_speeluren u         ON u.id = lf.ref_id
        INNER JOIN users                   ON users.naam = s.naam
           AND ( lf.eind_dt IS NULL OR lf.eind_dt > NOW() )
               AND users.usid = '%s'
               AND users.paswoord = '%s'";
  $sql  = sprintf($query, mysql_real_escape_string($usernaam)
                        , mysql_real_escape_string($_POST["password"]));
  $result = mysql_query($sql, $badm_db) or badm_mysql_die();
  $numrows = mysql_num_rows($result);

  if ($numrows == 0)
  {
    $message = 'Verkeerde gebruikersnaam of wachtwoord';
  }
  else
  {
    setcookie ("usid", $usernaam, time()+7948800); //3 maanden
    for ($i=0; $i < $numrows; $i++)
    {
      $row = mysql_fetch_assoc($result);
      switch ($row['functie'])
      {
        case 'WEBMASTER':
        case 'SECRETARIS':
          $_SESSION['administrator'] = true;
          break;
        case 'COMPETITIE':
          $_SESSION['competitieverantwoordelijke'] = true;
          break;
        case 'TORNOOI':
          $_SESSION['tornooiverantwoordelijke'] = true;
          break;
        case 'LEDENBEHEER':
          $_SESSION['ledenbeheer'] = true;
          break;
        case 'KAPITEIN':
          $_SESSION['ploegkapitein'] = true;
          if (isset($_SESSION['ploeg_id']))
          {
            $_SESSION['ploeg_id'] = $_SESSION['ploeg_id'].'#'.$row['ref_id'];
          }
          else
          {
            $_SESSION['ploeg_id'] = $row['ref_id'];
          }
          break;
        case 'DAG':
          $_SESSION['dagverantwoordelijke'] = true;
          if (isset($_SESSION['ploeg_id']))
          {
            $_SESSION['speeluur_id'] = $_SESSION['speeluur_id'].'#'.$row['ref_id'];
          }
          else
          {
            $_SESSION['speeluur_id'] = $row['ref_id'];
          }
          break;
        case 'TRAINER':
          $_SESSION['trainer'] = true;
          if (isset($_SESSION['dag_id']))
          {
            $_SESSION['dag_id'] = $_SESSION['dag_id'].'#'.$row['ref_id'];
          }
          else
          {
            $_SESSION['dag_id'] = $row['ref_id'];
          }
          break;
        case 'RECREANT':
          $_SESSION['bestuurslid'] = true;
          break;
        case 'VOORZITTER':
          $_SESSION['voorzitter'] = true;
          $_SESSION['bestuurslid'] = true;
          break;
        default:
          $_SESSION['bestuurslid'] = true;
      }
      $naam = $row['naam'];
    }
    $_SESSION['username'] = $naam;
    $_SESSION['usid'] = $usernaam;

    mysql_free_result($result);

    srand((double)microtime()*1000000);
    $session_id = md5 (uniqid (rand()));
    $_SESSION['id'] = $session_id;
    $_SESSION['auth'] = true;

    log_start_time($usernaam, $session_id, $badm_db);

    if (!isset($_SESSION['administrator']) && !isset($_SESSION['competitieverantwoordelijke']) && !isset($_SESSION['bestuurslid']))
    {
      if (isset($_SESSION['ploegkapitein']) && $ref == "leden.php")
      {
        $ref = "competitie.php";
      }
      elseif (isset($_SESSION['dagverantwoordelijke']) && $ref == "leden.php")
      {
        $ref = "speeluren.php";
      }
      elseif (isset($_SESSION['trainer']) && $ref == "leden.php")
      {
        $ref = "speeluren.php";
      }
    }
    session_write_close;
//    mysql_close($badm_db);
    header("Location: ".$ref);
    exit;
  }
}
}
?>

<html>
<head>
<title>W&amp;L Login</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="http://www.badmintonsport.be/css/admin.css" rel="stylesheet" type="text/css">
<script language="JavaScript" type="text/javascript">
<!--
function set_focus() {
  document.pass.usid.focus();
}
<?php
  if(preg_match('/MSIE/i', $_SERVER['HTTP_USER_AGENT']))
  {
?>
// Create the popup object. For predictable results, always create a global popup object.
var oPopup = window.createPopup();
var screenHeight = window.screen.availHeight;
function openPopup(obj)
{
  // The following HTML that populates the popup object.
  oPopup.document.body.innerHTML = obj.innerHTML;
  // The popup object exposes the document object and its properties.
  var popupBody = oPopup.document.body;
  popupBody.style.backgroundColor = "lightyellow";
  popupBody.style.border = "solid black 1px";
  popupBody.style.fontSize = "13px";
  popupBody.style.margin = "0.2cm";
  // The following popup object is used only to detect what height the
  // displayed popup object should be using the scrollHeight property.
  // This is important because the size of the popup object varies
  // depending on the length of the definition text. This first
  // popup object is not seen by the user.
  oPopup.show(0, 0, 500, 0);
  var realHeight = popupBody.scrollHeight;
  //var realWidth  = popupBody.scrollWidth;
  // Hides the dimension detector popup object.
  oPopup.hide();
  // Shows the actual popup object with correct height.
  // Parameters of the show method are in the following order: x-coordinate,
  // y-coordinate, width, height, and the element to which the x,y
  // coordinates are relative. Note that this popup object is displayed
  // relative to the body of the document.
  oPopup.show(-400, 280, 500, realHeight, event.srcElement);
}
<?php
  }
?>
// -->
</script>
</head>
<body bgcolor="#3A6EA5" onload="set_focus()">
<br>
<br>
<?php
  //echo "aantal rijen: ".$numrows;
  //echo $naam;
  //echo "<br>sessie: ".$_SESSION['naam'];
?>
<br>
<br>
<table border="1" cellspacing="0" cellpadding="0" align="center" width="450">
  <tr bgcolor="#C6C3C6">
    <td>
      <table width="450" border="0" cellspacing="0" cellpadding="1" align="center">
        <tr bgcolor="#400080">
          <td height="20" class="title">&nbsp;W&amp;L</td>
          <td height="20" class="td1" align="right" bgcolor="#000084">
            <?php if (preg_match('/MSIE/i', $_SERVER['HTTP_USER_AGENT'])) { echo '<img src="../poll/image/help.gif" width="16" height="14" alt="Help" onclick="openPopup(oHelp);">'; } ?>
            <img src="../poll/image/cross.gif" width="16" height="14" alt="Afsluiten" onClick="window.close();"> </td>
        </tr>
        <tr align="center">
          <td colspan="2">
            <form method="post" name="pass" action="login.php?ref=<?php echo $ref; ?>">
              <table border="0" cellspacing="0" cellpadding="1" width="100%">
                <tr>
                  <td rowspan="3" valign="top" align="right" width="12%">
                    <img src="../poll/image/key.gif" width="35" height="40">
                  </td>
                  <td colspan="2" class="td1" height="42"><?php echo $message; ?></td>
                </tr>
                <tr>
                  <td class="td1" width="18%">Gebruikersnaam</td>
                  <td width="70%">
                    <input type="text" name="usid" size="25" class="input1">
                  </td>
                </tr>
                <tr>
                  <td class="td1" height="35" width="18%">Wachtwoord</td>
                  <td height="35" width="70%">
                    <input type="password" name="password" size="25" class="input1">
                    <input type="hidden" name="action" id="action" value="login">
                  </td>
                </tr>
              </table>
              <table width="100%" border="0" cellspacing="0" cellpadding="1">
                <tr>
                  <td align="right" width="47%">
                    <input type="submit" value="   OK   " class="button1">
                  </td>
                  <td align="right" width="1%">&nbsp;</td>
                  <td width="52%">
                    <input type="reset" value="Reset" class="button1">
                  </td>
                </tr>
              </table>
            </form>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<?php
  if (preg_match('/MSIE/i', $_SERVER['HTTP_USER_AGENT']))
  {
?>
<div id="oHelp" style="display:none;">
  <!-- Content of the popup comes here -->
Dit is het inlogscherm voor de admin module van badmintonvereniging W&amp;L.<br>
Alleen bestuursleden, ploegkapiteins, dagverantwoordelijken en trainers van W&amp;L bv kunnen hier inloggen.<br>
Zij kunnen hiervoor hun logingegevens van de W&amp;L website gebruiken.<br>
Deze gegevens kunnen opgevraagd worden bij de <a href="mailto:webmaster@badmintonsport.be">webmaster</a>.<br>
Afhankelijk van je functie binnen de club zal je meer of minder mogelijkheden hebben binnen de module.<br>
Bij eventuele problemen en/of vragen, gelieve de <a href="mailto:webmaster@badmintonsport.be">webmaster</a> te contacteren.<br>
Gelieve steeds uit te loggen via het kruisje rechts boven in de applicatiebalk wanneer je de module wil verlaten.
</div>
<?php
  }
  else
  {
?>
<div id="oHelp" style="text-align:center;">
<p style="color:white">Dit is het inlogscherm voor de admin module van badmintonvereniging W&amp;L.</p>
<p style="color:white">Alleen bestuursleden, ploegkapiteins, dagverantwoordelijken en trainers van W&amp;L bv kunnen hier inloggen.<br>
Zij kunnen hiervoor hun logingegevens van de W&amp;L website gebruiken.<br>
Deze gegevens kunnen opgevraagd worden bij de <a href="mailto:webmaster@badmintonsport.be">webmaster</a>.<br>
Afhankelijk van je functie binnen de club zal je meer of minder mogelijkheden hebben binnen de module.<br>
De handleiding kan je raadplegen door na login op het vraagteken rechts bovenaan te klikken.<br>
Bij eventuele problemen en/of vragen, gelieve de <a href="mailto:webmaster@badmintonsport.be">webmaster</a> te contacteren.<br>
Gelieve steeds uit te loggen via het kruisje rechts boven in de applicatiebalk wanneer je de module wil verlaten.</p>
</div>
<?php
  }
?>
<!-- preloading images -->
<img class="hidden" src="../poll/image/index.gif" alt="">
<img class="hidden" src="../poll/image/new.gif" alt="">
<img class="hidden" src="../poll/image/settings.gif" alt="">
<img class="hidden" src="../images/b_calendar.png" alt="">
<img class="hidden" src="../images/check.png" alt="">
<br>
<br>
<br>
<?php
  //mysql_close($badm_db);
?>
</body>
</html>
