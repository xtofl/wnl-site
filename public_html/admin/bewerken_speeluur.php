<?php
/**
 * bewerken_speeluur.php
 *
 * author    : Freek Ceymeulen
 * created   : 01/02/2005
 *             02/11/2012 aantal_plaatsen, aantal_reserve en prijs toegevoegd
 * variables : id : speeluren_id
 **/

  // Connect to the DB (must be done before calling sess.php)
  require_once "../functies/badm_db.inc.php";
  $badm_db = badm_conn_db();
  require_once('../functies/sess.php');

  if ($_SESSION['auth'] != true)
  {
    // niet ingelogd -> ga naar login pagina
    // geef deze pagina als parameter mee zodat na succesvolle login teruggekeerd kan worden naar deze pagina
  //session_write_close;
  mysql_close($badm_db);
    header("Location: http://www.badmintonsport.be/admin/login.php?ref=".basename($PHP_SELF));
    exit;
  }
/*
--------------------------------------------------------------------------------------------
|| FUNCTIONS
--------------------------------------------------------------------------------------------
*/
  require_once("../functies/general_functions.php");

  function validate_domain($domain, $conn)
  {
    $lv_return = "";
    $query = "SELECT UPPER(value) AS value
                   , description
                FROM codes
               WHERE domain = '%s'";
    $sql  = sprintf($query, mysql_real_escape_string($domain));
    $rslt = mysql_query($sql, $conn) or badm_mysql_die();
    while ($row = mysql_fetch_object($rslt))
    {
      $lv_return .= " && document.speeluren.".$domain.".value.toUpperCase() != '".$row->value."'";
    }
    mysql_free_result($rslt);
    return substr($lv_return, 4);
  }
/*
--------------------------------------------------------------------------------------------
|| BEGIN
--------------------------------------------------------------------------------------------
*/
  $speeluren_id = $_GET['id'];
  $err_msg = '';
  $orderBy = '2, 6, 4';
  if (isset($_GET['order']))
  {
    $order = $_GET['order'];
    if ($order == 'naam')
    {
      $orderBy = '2, 6, 4';
    }
    elseif ($order == 'klassement')
    {
      $orderBy = '4, 6';
    }
    elseif ($order == 'lidnr')
    {
      $orderBy = '5';
    }
    elseif ($order == 'leeftijd')
    {
      $orderBy = '6, 8, 2';
    }
    elseif ($order == 'type')
    {
      $orderBy = '7, 2';
    }
    elseif ($order == 'geslacht')
    {
      $orderBy = '8, 6, 2';
    }
    elseif ($order == 'inschrijving')
    {
      $orderBy = 's.inschrijving_dt';
    }
  }
?>
<html>
<head>
<title>W&amp;L Admin Module</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="http://www.badmintonsport.be/css/admin.css" rel="stylesheet" type="text/css">
<style type="text/css">
<!--
@media print {
img, input, button, #menu, #doc {
   visibility : hidden;
}
}
-->
</style>
<script language="JavaScript" src="../scripts/modal_dialog.js"></script>
<script type="text/javascript" language="JavaScript" src="http://www.badmintonsport.be/scripts/createXmlHttpRequest.js"></script>
<script language="JavaScript">
<!--
var retval = "";
// Calls a dialog window with LOV-values for this field
function showLov(obj1, obj2)
{
  if (navigator.appName == "Microsoft Internet Explorer")
  {
    retval = window.showModalDialog("http://www.badmintonsport.be/functies/build_dialog_window.php?p_lov_item="+obj1.name+"&p_search_string="+obj1.value.toUpperCase().replace('%', '***')+"&p_filter_min_length=0", window, "status:no; resizable:no; font-size:16px; dialogWidth:20em; dialogHeight: 20em");
    if (retval != null)
    {
      var desc_code_arr = retval.split("|");
      obj1.value = desc_code_arr[0];
      if (obj2 != null)
      {
        obj2.value = desc_code_arr[1];
      }
    }
  }
  else
  {
    //openDialog('lovNS.php', 300, 400, setPrefs);
    alert("This functionality only works with MS Internet Explorer.\nContact the webmaster.");
  }
}

function doSubmit(p_action)
{
  if (p_action == 'update')
  {
    if (document.speeluren.sporthal.value.length == 0)
    {
      alert('Je bent vergeten een sporthal in te vullen!');
      document.speeluren.sporthal.focus();
      return;
    }
    else
    {
      if (<?php echo validate_domain('sporthal', $badm_db); ?>)
      {
        alert('De ingevulde sporthal is niet correct.  Geldige waarden zijn: Veltem/Kessel-Lo/Heverlee.');
        document.speeluren.sporthal.focus();
        return;
      }
    }
    if (document.speeluren.dag.value.length == 0)
    {
      alert('Je bent vergeten een dag in te vullen!');
      document.speeluren.dag.focus();
      return;
    }
    else
    {
      if (<?php echo validate_domain('dag', $badm_db); ?>)
      {
        alert('De ingevulde dag is niet correct.');
        document.speeluren.dag.focus();
        return;
      }
    }
    if (document.speeluren.uur.value.length == 0)
    {
      alert('Je bent vergeten een uur in te vullen!');
      document.speeluren.uur.focus();
      return;
    }
    document.speeluren.command.value = 'update';
    document.speeluren.action = 'dml.php';
  }
  else if (p_action == 'delete')
  {
    if (document.speeluren.totaal.value > 0)
    {
      alert('WARNING: Je kan dit speeluur niet verwijderen zolang er spelers aan zijn toegekend.');
      return;
    }
    else
    {
      if (confirm("Ben je zeker dat je dit speeluur wilt verwijderen?"))
      {
        document.speeluren.command.value = 'delete';
        document.speeluren.action = 'dml.php';
      }
      else
      {
        return;
      }
    }
  }
  else if (p_action == 'send_mail')
  {
    document.speeluren.action = 'zend_email.php';
  }
  document.speeluren.submit();
}

function deleteVerantwoordelijke(id)
{
  document.speeluren.delete_verantwoordelijke.value = id;
  doSubmit('update');
}

function deleteTrainer(id)
{
  document.speeluren.delete_trainer.value = id;
  doSubmit('update');
}
// Verandert de gegeven speler van reserve naar basis of omgekeerd voor dit speeluur
function set_reserve($spelers_id, $reserve)
{
  if ($reserve)
  {
    requestData('ajax_set_reserve.php?speeluren_id=<?php echo $speeluren_id; ?>&spelers_id='+$spelers_id+'&reserve=Y', 'set_reserve', 'spelers_id='+$spelers_id);
  }
  else
  {
    requestData('ajax_set_reserve.php?speeluren_id=<?php echo $speeluren_id; ?>&spelers_id='+$spelers_id+'&reserve=N', 'set_reserve', 'spelers_id='+$spelers_id);
  }
}
function processData($action)
{
  if ($action == "set_reserve")
  {
    //alert("Reserve updated.");
    ;
  }
}
-->
</script>
</head>
<body bgcolor="#3A6EA5" link="#000000" vlink="#000000" topmargin="0">

<span style="font-size: 8pt; float: right"><?php echo $_SESSION['username']; ?></span>
<?php
  // Build current date and time string
  list($wday,$mday,$month,$year,$hour,$minutes) = split("( )",date("w j n Y H i",time()));
  $weekday = array('zondag', 'maandag', 'dinsdag', 'woensdag', 'donderdag', 'vrijdag', 'zaterdag');
  $months  = array('januari','februari','maart','april','mei','juni','juli','augustus','september','oktober','november','december');
  $newdate = "$weekday[$wday], $mday ".$months[$month-1]." $year $hour:$minutes";
?>
<span style="font-size: 8pt"><?php echo $newdate; ?></span>

<table border="1" cellspacing="0" cellpadding="0" align="center" width="810">
 <tr bgcolor="#C6C3C6" valign="top">
  <td>
   <table width="100%" border="0" cellspacing="0" cellpadding="1" align="center">
    <tr bgcolor="#400080">
     <td height="20" class="title">&nbsp;W&amp;L - Speeluur</td>
     <td height="20" align="right" bgcolor="#000084">
      <a href="http://www.badmintonsport.be/docs/W_L_admin_users_guide.doc" target="_blank"><img src="../poll/image/help.gif" width="16" height="14" border="0" alt="Gebruikershandleiding"></a><a href="login.php?action=logout"><img src="../poll/image/cross.gif" width="16" height="14" border="0" alt="Uitloggen"></a></td>
    </tr>
   </table>
   <table border="0" cellspacing="0" cellpadding="0">
    <tr valign="top">
     <td width="100">
<?php
  // Print the menu
  write_menu($_SERVER['PHP_SELF']);
?>
     </td>
     <td>
      <form name="speeluren" action="<?=basename($PHP_SELF)?>" method="POST">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
<?php
  if (strlen($err_msg) > 0)
  {
    echo "<tr><td><font color=\"red\"><b>".$err_msg."</b></font></td></tr>";
  }
?>
       <tr>
        <td align="center">
         <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
           <td>
            <table border="0" cellspacing="0" cellpadding="2">
<?php
  if (!is_numeric($speeluren_id))
  // Check to avoid people passing sql statements in the url
  {
    $err_msg = "Security violation, admin has been alerted.";
    mail("webmaster@badmintonsport.be", "Possible breakin attempt", "User ".$_SESSION['username']." on page ".$_SERVER['PHP_SELF']." from IP ".$_SERVER['REMOTE_ADDR']." using method ".$_SERVER['REQUEST_METHOD']." on url ".$_SERVER['REQUEST_URI'], "From: webmaster@badmintonsport.be");
    //exit;
  }
  else
  {
    $query  = "SELECT u.id
                    , u.plaats
                    , LOWER(u.dag) AS dag
                    , u.uur
                    , u.unit
                    , u.type
                    , u.doelgroep
                    , u.beschrijving
                    , u.aantal_plaatsen
                    , u.aantal_reserve
                    , u.prijs
                 FROM bad_speeluren u
                WHERE u.id = %d";
    $sql  = sprintf($query, mysql_real_escape_string($speeluren_id));
    $result = mysql_query($sql, $badm_db) or badm_mysql_die();
    if (mysql_num_rows($result) == 0)
    {
      $err_msg = "Er werden geen gegevens gevonden voor het speeluur met id ".$speeluren_id;
    }
    // format results by row
    $uur = mysql_fetch_object($result);
    mysql_free_result($result);
  }
?>
                 <tr>
                  <td class="td2">Uur:</td>
                  <td><input class="input" type="text" name="uur" value="<?=$uur->uur?>" size="6" maxlength="10"></td>
                  <td class="td2">Dag:</td>
                  <td><select class="input" name="dag">
<?php
  build_options('dag', $uur->dag, $badm_db);
?>
                  </select></td>
                 </tr>
                 <tr>
                  <td class="td2">Unit:</td>
                  <td><select class="input" name="unit">
<?php
  build_options('unit', $uur->unit, $badm_db);
?>
                  </select></td>
                  <td class="td2">Sporthal:</td>
                  <td><select class="input" name="sporthal">
<?php
  build_options('sporthal', $uur->plaats, $badm_db);
?>
                  </select></td>
                 </tr>
                 <tr>
                  <td class="td2">Doelgroep:</td>
                  <td><select class="input" name="doelgroep">
<?php
  build_options('statuut', $uur->doelgroep, $badm_db);
?>
                  </select></td>
                  <td class="td2">Type:</td>
                  <td><select class="input" name="type">
<?php
  build_options('SPELTYPE', $uur->type, $badm_db);
?>
                  </select></td>
                 </tr>
                 <tr>
                  <td class="td2">Aantal plaatsen:</td>
                  <td><input class="input" type="text" name="aantal_plaatsen" value="<?=$uur->aantal_plaatsen?>" size="2" maxlength="2"></td>
                  <td class="td2">Aantal reserve:</td>
                  <td><input class="input" type="text" name="aantal_reserve" value="<?=$uur->aantal_reserve?>" size="2" maxlength="2"></td>
                 </tr>
                 <tr>
                  <td class="td2">Prijs:</td>
                  <td><input class="input" type="text" name="prijs" value="<?=$uur->prijs?>" size="3" maxlength="3"></td>
                  <td class="td2">&nbsp;</td>
                  <td>&nbsp;</td>
                 </tr>
                 <tr>
                  <td class="td2">Beschrijving:</td>
                  <td colspan="3"><input class="input" type="text" name="beschrijving" value="<?=$uur->beschrijving?>" size="50" maxlength="255"></td>
                 </tr>
                </table>
               </td>
               <td valign="top">
                <table border="0" cellspacing="0" cellpadding="2">
                 <tr>
                  <td class="td2" valign="top">Verantwoordelijke(n):</td>
                  <td>
<?php
    $sql_stmt = "SELECT s.id
                      , s.naam
                   FROM leden_functies lf
                      , bad_spelers s
                  WHERE lf.spelers_id = s.id
                    AND lf.functies_id = 25
                    AND lf.ref_id = %d";
    $sql  = sprintf($sql_stmt, mysql_real_escape_string($speeluren_id));
    $result = mysql_query($sql, $badm_db) or badm_mysql_die();
    while ($row = mysql_fetch_object($result))
    {
      echo "                    <input class=\"input\" type=\"text\" name=\"dagverantwoordelijken\" value=\"".$row->naam."\" size=\"25\" disabled>\n";
      echo "                    <img src=\"../images/red_cross.gif\" alt=\"verwijder deze verantwoordelijke\" style=\"cursor: hand\" onClick=\"deleteVerantwoordelijke(".$row->id.");\"><br/>\n";
    }
    mysql_free_result($result);
  if ( strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE'))
  {
?>
                    <input class="input" type="text" name="verantwoordelijke" value="" size="25" disabled>
                    <input type="hidden" name="verantwoordelijke_id">
                    <img src="../images/lov_button.jpg" alt="List Of Values" onClick="showLov(document.speeluren.verantwoordelijke, document.speeluren.verantwoordelijke_id);" style="CURSOR: pointer; CURSOR: hand">
<?php
  }
  else
  {
    echo "                    <select class=\"input\" name=\"verantwoordelijke\" onChange=\"document.speeluren.verantwoordelijke_id.value=document.speeluren.verantwoordelijke.value;\">\n";
    echo "                     <option value=\"\">--geen--</option>\n";
    $sql_stmt = "SELECT id, naam FROM bad_spelers WHERE eind_dt IS NULL ORDER BY 2";
    $rslt = mysql_query($sql_stmt, $badm_db) or badm_mysql_die();
    while ($row = mysql_fetch_object($rslt))
    {
      echo "                     <option value=\"".$row->id."\">".$row->naam."</option>\n";
    }
    mysql_free_result($rslt);
    echo "                    </select>\n";
    echo "                    <input type=\"hidden\" name=\"verantwoordelijke_id\">\n";
  }
?>
                   </td>
                  </tr>
                  <tr>
                   <td class="td2" valign="top">Trainer(s):</td>
                   <td>
<?php
    $sql_stmt = "SELECT s.id
                      , s.naam
                   FROM leden_functies lf
                      , bad_spelers s
                  WHERE lf.spelers_id = s.id
                    AND lf.functies_id = 26
                    AND lf.ref_id = %d";
    $sql  = sprintf($sql_stmt, mysql_real_escape_string($speeluren_id));
    $result = mysql_query($sql, $badm_db) or badm_mysql_die();
    while ($row = mysql_fetch_object($result))
    {
      echo "                    <input class=\"input\" type=\"text\" name=\"trainers\" value=\"".$row->naam."\" size=\"25\" disabled>\n";
      echo "                    <img src=\"../images/red_cross.gif\" alt=\"verwijder deze trainer\" style=\"cursor: hand\" onClick=\"deleteTrainer(".$row->id.");\"><br/>\n";
    }
    mysql_free_result($result);

  if ( strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE'))
  {
?>
                    <input class="input" type="text" name="trainer" value="" size="25" disabled>
                    <input type="hidden" name="trainer_id">
                    <img src="../images/lov_button.jpg" alt="List Of Values" onClick="showLov(document.speeluren.trainer, document.speeluren.trainer_id);" style="CURSOR: pointer; CURSOR: hand">
<?php
  }
  else
  {
    echo "                    <select class=\"input\" name=\"trainer\" onChange=\"document.speeluren.trainer_id.value=document.speeluren.trainer.value;\">\n<option value=\"\">--geen--</option>\n";
    $sql_stmt = "SELECT id, naam FROM bad_spelers WHERE eind_dt IS NULL ORDER BY 2";
    $rslt = mysql_query($sql_stmt, $badm_db) or badm_mysql_die();
    while ($row = mysql_fetch_object($rslt))
    {
      echo "                    <option value=\"".$row->id."\">".$row->naam."</option>\n";
    }
    mysql_free_result($rslt);
    echo "                    </select>\n<input type=\"hidden\" name=\"trainer_id\">\n";
  }
?>
                   </td>
                  </tr>
                 </table>
                </td>
               </tr>
              </table>
             </td>
            </tr>
            <tr>
             <td><hr>
              <table border="1" cellspacing="0" cellpadding="3" frame="below" rules="groups">
               <tr>
                <th>&nbsp;</th>
                <th><a href="<?=basename($PHP_SELF)?>?id=<?=$speeluren_id?>&order=naam" title="Sorteer">Naam</a></th>
                <th><a href="<?=basename($PHP_SELF)?>?id=<?=$speeluren_id?>&order=klassement" title="Sorteer">Klas</a></th>
                <th><a href="<?=basename($PHP_SELF)?>?id=<?=$speeluren_id?>&order=lidnr" title="Sorteer">Lidnr</a></th>
                <th><a href="<?=basename($PHP_SELF)?>?id=<?=$speeluren_id?>&order=leeftijd" title="Sorteer">Lft</a></th>
                <th><a href="<?=basename($PHP_SELF)?>?id=<?=$speeluren_id?>&order=type" title="Sorteer">Type</a></th>
                <th><a href="<?=basename($PHP_SELF)?>?id=<?=$speeluren_id?>&order=geslacht" title="Sorteer">Gesl</a></th>
                <th>Betaald</th>
                <th>Reserve</th>
                <th><a href="<?=basename($PHP_SELF)?>?id=<?=$speeluren_id?>&order=inschrijving_dt" title="Sorteer">Datum Inschr.</a></th>
               </tr>
<?php
  $query  = "SELECT l.spelers_id
                  , s.achternaam
                  , s.voornaam
                  , s.klassement
                  , s.lidnr
                  , YEAR(CURRENT_DATE) - YEAR(s.geb_dt) - (RIGHT(CURRENT_DATE, 5) < RIGHT(s.geb_dt, 5)) AS leeftijd
                  , s.type_speler
                  , s.geslacht
                  , s.email
                  , s.betaald
                  , l.reserve
                  , DATE_FORMAT(s.inschrijving_dt, '%%d/%%m/%%Y') AS inschrijving_dt
               FROM leden_uren l
                  , bad_spelers s
              WHERE l.spelers_id = s.id
                AND ( s.eind_dt IS NULL OR s.eind_dt > CURRENT_DATE )
                AND l.speeluren_id = %d
           ORDER BY %d";
  $sql  = sprintf($query, mysql_real_escape_string($speeluren_id)
                        , mysql_real_escape_string($orderBy));
  $result = mysql_query($sql, $badm_db) or badm_mysql_die();
  $mailing_list = '';
  $totaal = mysql_num_rows($result);
  for ($i=0; $i < $totaal; $i++)
  {
    $row = mysql_fetch_assoc($result);
    echo '                  <tr class="'; echo ($i%2==0) ? "even" : "odd"; echo '">';
    echo '                    <td class="td2" align="right">'.($i + 1).'</td>';
    echo '                    <td class="td2"'; echo ($row['reserve']=='Y') ? ' style="color:orange"' : ''; echo '><a href="bewerken_lid.php?id='.$row['spelers_id'].'">'.$row['achternaam'].' '.$row['voornaam'].'</a></td>';
    echo '                    <td class="td2" align="center">'.$row['klassement'].'</td>';
    echo '                    <td class="td2" align="right">'.$row['lidnr'].'</td>';
    echo '                    <td class="td2" align="right">'.$row['leeftijd'].'</td>';
    echo '                    <td class="td2" align="center">'.$row['type_speler'].'</td>';
    echo '                    <td class="td2" align="center">'.$row['geslacht'].'</td>';
    echo '                    <td class="td2" align="right">'.$row['betaald'].'</td>';
    echo '                    <td class="td2" align="center"><input type="checkbox" name="reserve'.$row['spelers_id'].'" class="input" style="border: none"'; echo ($row['reserve']=='Y') ? ' CHECKED' : ''; echo ' onChange="javascript:set_reserve('.$row['spelers_id'].', this.checked);"></td>';
    echo '                    <td class="td2" align="right">'.$row['inschrijving_dt'].'</td>';
    // build list with E-mail adresses
    if (strlen($row['email']) > 0)
    {
      $mailing_list .= ','.$row['voornaam'].' '.$row['achternaam'].'<'.$row['email'].'>';
    }
  }
  mysql_free_result($result);
?>
         </table>
        </td>
       </tr>

       <tr>
        <td class="td2"><hr>
         <span style="float: right">
         <input type="hidden" name="id" value="<?=$speeluren_id?>">
         <input type="hidden" name="page" value="bewerken_speeluur.php">
         <input type="hidden" name="command" value="query">
         <input type="hidden" name="aan" value="<?php echo substr($mailing_list, 1); ?>">
         <input type="hidden" name="delete_verantwoordelijke" value="0">
         <input type="hidden" name="delete_trainer" value="0">
<?php
  $authorization = false;
  // De dagverantwoordelijke voor dit uur mag de gegevens bewerken
  if (isset($_SESSION['dagverantwoordelijke']))
  {
    $uren_ids = explode("#", $_SESSION['speeluur_id']);
    foreach ($uren_ids as $id)
    {
      if ($id == $speeluren_id)
      {
        $authorization = true;
      }
    }
  }
  if (isset($_SESSION['administrator']) || isset($_SESSION['voorzitter']) || $authorization)
  {
?>
         <button type="button" name="save" accesskey="S" title="de gemaakte wijzigingen opslaan" onClick="doSubmit('update');"><span style="text-decoration: underline">S</span>la Op</button>
<?php
    if (isset($_SESSION['administrator']) || isset($_SESSION['voorzitter']))
    {
?>
         <button type="button" name="delete" accesskey="V" title="Verwijder dit speeluur" onClick="doSubmit('delete');"><span style="text-decoration: underline">V</span>erwijder</button>
<?php
    }
  }
?>
         <button type="button" name="email" accesskey="E" title="Verstuur een e-mail naar al deze spelers" onClick="doSubmit('send_mail');">Verstuur <span style="text-decoration: underline">E</span>mail</button>
         <!-- to check if spelers exist for this speeluur -->
         <input type="hidden" name="totaal" value="<?php echo $totaal; ?>">
         </span>
         Totaal: <?php echo $totaal; ?> spelers</td>
       </tr>
       </form>
      </table>
     </td>
    </tr>
   </table>
  </td>
 </tr>
</table>
<br>
<br>
<?php
  //mysql_close($badm_db);
?>
</body>
</html>