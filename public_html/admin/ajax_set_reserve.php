<?php
/**
 * ajax_set_reserve.php
 *
 * object     : Methods to return data that are requested asynchronous
 * author     : Freek Ceymeulen
 * created    : 24/11/2012
 * parameters : $speeluren_id : 
 *              $spelers_id   : 
 *              $reserve      :
 **/

 /*
 || Functions
 */

  function set_reserve($speeluren_id, $spelers_id, $reserve)
  {
    global $debug;
    $debug .= "<br>set_reserve(".$speeluren_id.",".$spelers_id.",".$reserve.")";
    require_once "../functies/badm_db.inc.php";
    $badm_db = badm_conn_db();

    $update_stmt = "UPDATE leden_uren
                       SET reserve = '%s'
                         , user_id = '%s'
                         , dt_wijz = NOW()
                     WHERE spelers_id = %d
                       AND speeluren_id = %d";
    $sql  = sprintf($update_stmt, mysql_real_escape_string($reserve)
                                , substr(mysql_real_escape_string($_SESSION['usid']), 0, 10)
                                , mysql_real_escape_string($spelers_id)
                                , mysql_real_escape_string($speeluren_id));
    $result = mysql_query($sql, $badm_db) or die("Invalid query: " . mysql_error());
    //echo $debug;
  }

 /*
 || Begin
 */

  $debug = "";
  if (isset($_REQUEST["speeluren_id"]))
  {
    $speeluren_id = $_REQUEST["speeluren_id"];
    if (!empty($speeluren_id))
    {
      set_reserve($speeluren_id, $_REQUEST["spelers_id"], $_REQUEST["reserve"]);
    }
  }
  //mail("freek.ceymeulen@pandora.be", "Debug ajax_set_reserve.php", $debug, "From: webmaster@badmintonsport.be");
?>