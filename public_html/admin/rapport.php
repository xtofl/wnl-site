<?php
  // Connect to database
  require_once "/home/badmin/public_html/functies/badm_db.inc.php";
  $badm_db = badm_conn_db();

  function page_views ($conn)
  {
    $query = "
SELECT page AS pagina
     , COUNT(*) AS aantal
     , ROUND(AVG(generation_time), 3) AS duration
  FROM website_usage
 GROUP BY page
 ORDER BY 2 DESC";
    //$sql  = sprintf($query, mysql_real_escape_string($_REQUEST["id"]));
    $sql = $query;
    $result = mysql_query($sql, $conn) or badm_mysql_die();
    $numFields = mysql_num_fields($result);
    echo "<table class=\"sortable\">";
    // Print the table header
    echo "<tr>";
    echo "<th>N�</th>";
    for ($i=0; $i<$numFields;$i++)
    {
      echo "<th>".ucwords(mysql_field_name($result, $i))."</th>";
    }
    echo "</tr>\n";
    // Print the table rows
    $counter = 0;
    while ($row = mysql_fetch_object($result))
    {
      $counter++;
      echo "<tr>";
      echo "<td align=\"right\">".$counter."</td>";
      echo "<td>".$row->pagina."</td>";
      echo "<td align=\"right\">".$row->aantal."</td>";
      echo "<td align=\"right\">".$row->duration."</td>";
      echo "</tr>\n";
    }
    echo "</table>";
  }

  function visitors ($conn)
  {
    $query = "
SELECT visitor AS bezoeker
     , COUNT(*) AS aantal
     , ROUND(AVG(generation_time), 3) AS duration
  FROM `website_usage`
 GROUP BY visitor
 ORDER BY 2 DESC";
    //$sql  = sprintf($query, mysql_real_escape_string($_REQUEST["id"]));
    $sql = $query;
    $result = mysql_query($sql, $conn) or badm_mysql_die();
    $numFields = mysql_num_fields($result);
    echo "<table class=\"sortable\">";
    // Print the table header
    echo "<tr>";
    echo "<th>N�</th>";
    for ($i=0; $i<$numFields;$i++)
    {
      echo "<th>".ucwords(mysql_field_name($result, $i))."</th>";
    }
    echo "</tr>\n";
    // Print the table rows
    $counter = 0;
    while ($row = mysql_fetch_object($result))
    {
      $counter++;
      echo "<tr>";
      echo "<td align=\"right\">".$counter."</td>";
      echo "<td><a href=\"rapport.php?report=page_views_by_visitor&visitor=".$row->bezoeker."\">".$row->bezoeker."</a></td>";
      echo "<td align=\"right\">".$row->aantal."</td>";
      echo "<td align=\"right\">".$row->duration."</td>";
      echo "</tr>\n";
    }
    echo "</table>";
  }

  function referers ($conn)
  {
    $query = "
SELECT referer AS referer
     , COUNT(*) AS aantal
  FROM website_usage
 WHERE referer LIKE '%s%%'
 GROUP BY referer
 ORDER BY 2 DESC";
    $sql  = sprintf($query, mysql_real_escape_string($_REQUEST["referer"]));
    $result = mysql_query($sql, $conn) or badm_mysql_die();
    $numFields = mysql_num_fields($result);
    echo "<table class=\"sortable\">";
    // Print the table header
    echo "<tr>";
    echo "<th>N�</th>";
    for ($i=0; $i<$numFields;$i++)
    {
      echo "<th>".ucwords(mysql_field_name($result, $i))."</th>";
    }
    echo "</tr>\n";
    // Print the table rows
    $counter = 0;
    while ($row = mysql_fetch_object($result))
    {
      $counter++;
      echo "<tr>";
      echo "<td align=\"right\">".$counter."</td>";
      echo "<td>".$row->referer."</td>";
      echo "<td align=\"right\">".$row->aantal."</td>";
      echo "</tr>\n";
    }
    echo "</table>";
  }

  function spelersinfo ($conn)
  {
    $query = "
SELECT s.naam AS speler
     , COUNT(*) AS aantal
  FROM website_usage wu
     , bad_spelers s
 WHERE wu.query_string = s.id
   AND wu.page = 'spelersinfo.php'
 GROUP BY s.naam
 ORDER BY 2 DESC";
    //$sql  = sprintf($query, mysql_real_escape_string($_REQUEST["visitor"]));
    $sql = $query;
    $result = mysql_query($sql, $conn) or badm_mysql_die();
    $numFields = mysql_num_fields($result);
    echo "<table class=\"sortable\">";
    // Print the table header
    echo "<tr>";
    echo "<th>N�</th>";
    for ($i=0; $i<$numFields;$i++)
    {
      echo "<th>".ucwords(mysql_field_name($result, $i))."</th>";
    }
    echo "</tr>\n";
    // Print the table rows
    $counter = 0;
    while ($row = mysql_fetch_object($result))
    {
      $counter++;
      echo "<tr>";
      echo "<td align=\"right\">".$counter."</td>";
      echo "<td>".$row->speler."</td>";
      echo "<td align=\"right\">".$row->aantal."</td>";
      echo "</tr>\n";
    }
    echo "</table>";
  }

  function spelersinfo2 ($conn)
  {
    $query = "
SELECT s.naam AS speler
     , wu.visitor AS bezoeker
     , COUNT(*)   AS aantal
  FROM website_usage wu
     , bad_spelers s
 WHERE wu.query_string = s.id
   AND wu.page = 'spelersinfo.php'
   AND wu.visitor <> 'anoniem'
   AND wu.visitor LIKE '%s%%'
 GROUP BY s.naam
        , wu.visitor
 ORDER BY 3 DESC";
    $sql  = sprintf($query, mysql_real_escape_string($_REQUEST["visitor"]));
    $result = mysql_query($sql, $conn) or badm_mysql_die();
    $numFields = mysql_num_fields($result);
    echo "<table class=\"sortable\">";
    // Print the table header
    echo "<tr>";
    echo "<th>N�</th>";
    for ($i=0; $i<$numFields;$i++)
    {
      echo "<th>".ucwords(mysql_field_name($result, $i))."</th>";
    }
    echo "</tr>\n";
    // Print the table rows
    $counter = 0;
    while ($row = mysql_fetch_object($result))
    {
      $counter++;
      echo "<tr>";
      echo "<td align=\"right\">".$counter."</td>";
      echo "<td>".$row->speler."</td>";
      echo "<td>".$row->bezoeker."</td>";
      echo "<td align=\"right\">".$row->aantal."</td>";
      echo "</tr>\n";
    }
    echo "</table>";
  }

  function page_views_by_visitor ($conn, $visitor)
  {
    $query = "
SELECT page AS pagina
     , COUNT(*) AS aantal
     , ROUND(AVG(generation_time), 3) AS duration
  FROM website_usage
 WHERE visitor = '$visitor'
 GROUP BY page
 ORDER BY 2 DESC";
    //$sql  = sprintf($query, mysql_real_escape_string($_REQUEST["visitor"]));
    $sql = $query;
    $result = mysql_query($sql, $conn) or badm_mysql_die();
    $numFields = mysql_num_fields($result);
    echo "<table class=\"sortable\">";
    // Print the table header
    echo "<tr>";
    echo "<th>N�</th>";
    for ($i=0; $i<$numFields;$i++)
    {
      echo "<th>".ucwords(mysql_field_name($result, $i))."</th>";
    }
    echo "</tr>\n";
    // Print the table rows
    $counter = 0;
    while ($row = mysql_fetch_object($result))
    {
      $counter++;
      echo "<tr>";
      echo "<td align=\"right\">".$counter."</td>";
      echo "<td>".$row->pagina."</td>";
      echo "<td align=\"right\">".$row->aantal."</td>";
      echo "<td align=\"right\">".$row->duration."</td>";
      echo "</tr>\n";
    }
    echo "</table>";
  }

  function print_report ($conn, $report)
  {
    echo "&nbsp;|&nbsp;<a href=\"http://www.badmintonsport.be/admin/rapport.php?report=page_views\">Page Views</a>\n";
    echo "&nbsp;|&nbsp;<a href=\"http://www.badmintonsport.be/admin/rapport.php?report=visitors\">Visitors</a>\n";
    echo "&nbsp;|&nbsp;<a href=\"http://www.badmintonsport.be/admin/rapport.php?report=referers\">Referers</a>\n";
    echo "&nbsp;|&nbsp;<a href=\"http://www.badmintonsport.be/admin/rapport.php?report=spelersinfo\">Spelersinfo</a>\n";
    echo "&nbsp;|&nbsp;<a href=\"http://www.badmintonsport.be/admin/rapport.php?report=spelersinfo2\">Spelersinfo 2</a><br><br>\n";
    switch (strtolower($report))
    {
      case "page_views":
        page_views($conn);
        break;
      case "visitors":
        visitors($conn);
        break;
      case "referers":
        referers($conn);
        break;
      case "spelersinfo":
        spelersinfo($conn);
        break;
      case "spelersinfo2":
        spelersinfo2($conn);
        break;
      case "page_views_by_visitor":
        page_views_by_visitor($conn, mysql_real_escape_string($_REQUEST["visitor"]));
        break;
    }
  }
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="nl">
 <head>
  <meta http-equiv="Content-Type"      content="text/html; charset=iso-8859-1">
  <meta http-equiv="Pragma"            content="no-cache">
  <meta http-equiv="Expires"           content="now">
  <title>W&amp;L badmintonvereniging vzw - Website statistieken</title>
  <script type="text/javascript" language="JavaScript" src="http://www.badmintonsport.be/scripts/sorttable.js"></script>
  <script type="text/javascript" language="JavaScript">
  <!--
  //-->
  </script>
  <style type="text/css">
  <!--
  table {
    border : 1px solid black;
  }
  th {
    background-color : #ccc;
  }
  td {
    margin-right : 3px;
    margin-left : 3px;
  }
  -->
  </style>
 </head>
 <body>
<?php
  print_report($badm_db, $_REQUEST["report"]);
?>
 </body>
</html>