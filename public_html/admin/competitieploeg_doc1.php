<?php
/**
 * competitieploeg_doc.php
 * 
 * object    : creates a Word file and forces a download of this file
 * author    : Freek Ceymeulen
 * created   : 06/06/2005
 * parameters : id : competitieploegen_id
 **/
  ini_set("session.save_handler", "files");
  session_start();
  if ($_SESSION['auth'] != true)
  {
    // niet ingelogd -> ga naar login pagina
    // geef deze pagina als parameter mee zodat na succesvolle login teruggekeerd kan worden naar deze pagina
    header("Location: http://www.badmintonsport.be/admin/login.php?ref=".basename($PHP_SELF));
    exit;
  }
/*
--------------------------------------------------------------------------------------------
|| FUNCTIONS
--------------------------------------------------------------------------------------------
*/
  require("../functies/general_functions.php");
/*
--------------------------------------------------------------------------------------------
|| BEGIN
--------------------------------------------------------------------------------------------
*/
  
  require_once "../badm_db.inc.php";
  $badm_db = badm_conn_db();

  $query = "SELECT c.type
                 , c.afdeling
                 , c.sporthal
                 , s.naam
                 , s.adres
                 , s.postcode
                 , s.woonplaats
                 , s.tel
                 , s.gsm
                 , s.email
              FROM bad_competitieploegen c
              LEFT OUTER JOIN bad_spelers s
              ON c.kapitein = s.id
             WHERE c.id = %d";
  $sql = sprintf($query, mysql_real_escape_string($_GET['id']));
  $result = mysql_query($sql, $badm_db) or badm_mysql_die();
  
  if (mysql_num_rows($result) == 0)
  {
    echo "No data found";
  }
  else
  {
    $ploeg = mysql_fetch_object($result);
    mysql_free_result($result);

    $type = initcap($ploeg->type);
    if (date("n") > 5)
    {
      $cur_year = date("Y");
    }
    else
    {
      $cur_year = date("Y") - 1;
    }
    $next_year = $cur_year + 1;
    
    header("Content-Description: File Transfer");
    //header("Content-Length: " . $filesize);
    header("Content-Disposition: attachment"); 
    header("Content-Type: application/msword"); 
    header("Content-Type: application/force-download"); 
    header("Content-Type: application/download"); 
    header("Content-Transfer-Encoding: binary");
    header("Cache-Control: no-cache, must-revalidate, post-check=0, pre-check=0");
    header("Pragma:no-cache"); 
    header("Expires:0");
    
    echo '<html xmlns:o="urn:schemas-microsoft-com:office:office"';
    echo 'xmlns:w="urn:schemas-microsoft-com:office:word"';
    echo 'xmlns="http://www.w3.org/TR/REC-html40">';

    echo '<head>';
    echo '<meta http-equiv=Content-Type content="text/html; charset=windows-1252">';
    echo '<meta name=ProgId content=Word.Document>';
    echo '<meta name=Generator content="Microsoft Word 10">';
    echo '<meta name=Originator content="Microsoft Word 10">';
    echo '<link rel=File-List href="'.$type.'competitie%20'.date("Y").'_files/filelist.xml">';
    echo '<link rel=Edit-Time-Data href="'.$type.'competitie%20'.date("Y").'_files/editdata.mso">';
    echo '<title>'.$type.'competitie  -  '.$cur_year.' / '.$next_year.'  -  '.strtolower($ploeg->afdeling).'</title>';
    echo '<!--[if gte mso 9]><xml>';
    echo ' <o:DocumentProperties>';
    echo '  <o:Author>Ceymeulen Freek</o:Author>';
    echo '  <o:Template>Normal</o:Template>';
    echo '  <o:LastAuthor>Ceymeulen Freek</o:LastAuthor>';
    echo '  <o:Revision>2</o:Revision>';
    echo '  <o:TotalTime>67</o:TotalTime>';
    echo '  <o:LastPrinted>2004-10-30T16:49:00Z</o:LastPrinted>';
    echo '  <o:Created>2005-06-02T11:44:00Z</o:Created>';
    echo '  <o:LastSaved>2005-06-02T11:44:00Z</o:LastSaved>';
    echo '  <o:Pages>1</o:Pages>';
    echo '  <o:Words>150</o:Words>';
    echo '  <o:Characters>856</o:Characters>';
    echo '  <o:Company>W&L bv</o:Company>';
    echo '  <o:Lines>7</o:Lines>';
    echo '  <o:Paragraphs>2</o:Paragraphs>';
    echo '  <o:CharactersWithSpaces>1004</o:CharactersWithSpaces>';
    echo '  <o:Version>10.6626</o:Version>';
    echo ' </o:DocumentProperties>';
    echo '</xml><![endif]--><!--[if gte mso 9]><xml>';
    echo ' <w:WordDocument>';
    echo '  <w:GrammarState>Clean</w:GrammarState>';
    echo '  <w:HyphenationZone>21</w:HyphenationZone>';
    echo '  <w:DisplayHorizontalDrawingGridEvery>0</w:DisplayHorizontalDrawingGridEvery>';
    echo '  <w:DisplayVerticalDrawingGridEvery>0</w:DisplayVerticalDrawingGridEvery>';
    echo '  <w:UseMarginsForDrawingGridOrigin/>';
    echo '  <w:Compatibility>';
    echo '   <w:FootnoteLayoutLikeWW8/>';
    echo '   <w:ShapeLayoutLikeWW8/>';
    echo '   <w:AlignTablesRowByRow/>';
    echo '   <w:ForgetLastTabAlignment/>';
    echo '   <w:LayoutRawTableWidth/>';
    echo '   <w:LayoutTableRowsApart/>';
    echo '   <w:UseWord97LineBreakingRules/>';
    echo '  </w:Compatibility>';
    echo '  <w:BrowserLevel>MicrosoftInternetExplorer4</w:BrowserLevel>';
    echo ' </w:WordDocument>';
    echo '</xml><![endif]-->';
    echo '<style>';
    echo '<!--';
    echo ' /* Style Definitions */';
    echo ' p.MsoNormal, li.MsoNormal, div.MsoNormal';
    echo '  {mso-style-parent:"";';
    echo '  margin:0cm;';
    echo '  margin-bottom:.0001pt;';
    echo '  mso-pagination:widow-orphan;';
    echo '  font-size:10.0pt;';
    echo '  font-family:"Times New Roman";';
    echo '  mso-fareast-font-family:"Times New Roman";';
    echo '  mso-ansi-language:NL;}';
    echo 'h1';
    echo '  {mso-style-next:Normal;';
    echo '  margin:0cm;';
    echo '  margin-bottom:.0001pt;';
    echo '  mso-pagination:widow-orphan;';
    echo '  page-break-after:avoid;';
    echo '  mso-outline-level:1;';
    echo '  font-size:12.0pt;';
    echo '  mso-bidi-font-size:10.0pt;';
    echo '  font-family:"Times New Roman";';
    echo '  mso-font-kerning:0pt;';
    echo '  mso-ansi-language:NL-BE;';
    echo '  mso-bidi-font-weight:normal;}';
    echo 'h2';
    echo '  {mso-style-next:Normal;';
    echo '  margin:0cm;';
    echo '  margin-bottom:.0001pt;';
    echo '  mso-pagination:widow-orphan;';
    echo '  page-break-after:avoid;';
    echo '  mso-outline-level:2;';
    echo '  font-size:10.0pt;';
    echo '  font-family:"Times New Roman";';
    echo '  mso-ansi-language:NL-BE;';
    echo '  font-weight:normal;';
    echo '  font-style:italic;';
    echo '  mso-bidi-font-style:normal;}';
    echo 'span.GramE';
    echo '  {mso-style-name:"";';
    echo '  mso-gram-e:yes;}';
    echo '@page Section1';
    echo '  {size:595.3pt 841.9pt;';
    echo '  margin:70.85pt 70.85pt 70.85pt 70.85pt;';
    echo '  mso-header-margin:36.0pt;';
    echo '  mso-footer-margin:36.0pt;';
    echo '  mso-paper-source:0;}';
    echo 'div.Section1';
    echo '  {page:Section1;}';
    echo ' /* List Definitions */';
    echo ' @list l0';
    echo '  {mso-list-id:1829907314;';
    echo '  mso-list-type:simple;';
    echo '  mso-list-template-ids:68354063;}';
    echo '@list l0:level1';
    echo '  {mso-level-tab-stop:18.0pt;';
    echo '  mso-level-number-position:left;';
    echo '  margin-left:18.0pt;';
    echo '  text-indent:-18.0pt;}';
    echo 'ol';
    echo '  {margin-bottom:0cm;}';
    echo 'ul';
    echo '  {margin-bottom:0cm;}';
    echo '-->';
    echo '</style>';
    echo '<!--[if gte mso 10]>';
    echo '<style>';
    echo ' /* Style Definitions */';
    echo ' table.MsoNormalTable';
    echo '  {mso-style-name:"Table Normal";';
    echo '  mso-tstyle-rowband-size:0;';
    echo '  mso-tstyle-colband-size:0;';
    echo '  mso-style-noshow:yes;';
    echo '  mso-style-parent:"";';
    echo '  mso-padding-alt:0cm 5.4pt 0cm 5.4pt;';
    echo '  mso-para-margin:0cm;';
    echo '  mso-para-margin-bottom:.0001pt;';
    echo '  mso-pagination:widow-orphan;';
    echo '  font-size:10.0pt;';
    echo '  font-family:"Times New Roman";}';
    echo '</style>';
    echo '<![endif]-->';
    echo '</head>';
    echo '';
    echo '<body lang=EN-US style="tab-interval:35.4pt">';
    echo '';
    echo '<div class=Section1>';
    echo '';
    echo '<div style="border:none;border-bottom:solid windowtext 1.0pt;mso-border-bottom-alt:solid windowtext .5pt;padding:0cm 0cm 1.0pt 0cm">';
    echo '';
    echo '<p class=MsoNormal align=center style="text-align:center;border:none;';
    echo 'mso-border-bottom-alt:solid windowtext .5pt;padding:0cm;mso-padding-alt:0cm 0cm 1.0pt 0cm"><b';
    echo 'style="mso-bidi-font-weight:normal"><span lang=NL-BE style="font-size:18.0pt;';
    echo 'mso-bidi-font-size:10.0pt;mso-ansi-language:NL-BE">';
    echo $type.'competitie';
    echo '<span style="mso-spacerun:yes">  </span>-<span style="mso-spacerun:yes">  </span>';
    echo $cur_year;
    echo '/ <span class=GramE>'.$next_year.'<span style="mso-spacerun:yes">  </span></span>-<span';
    echo 'style="mso-spacerun:yes">  </span>'.strtolower($ploeg->afdeling).'<o:p></o:p></span></b></p>';
    echo '';
    echo '</div>';
    echo '';
    echo '<p class=MsoNormal><span lang=NL-BE style="mso-ansi-language:NL-BE"><o:p>&nbsp;</o:p></span></p>';
    echo '';
    echo '<h1 style="margin-left:92.15pt;text-indent:-92.15pt;tab-stops:3.0cm"><span';
    echo 'lang=NL-BE>Basisspelers <span style="mso-tab-count:1">      </span>: </span><span';
    echo 'lang=NL-BE style="font-weight:normal">';
    $query = "SELECT s.naam
                FROM bad_spelers s
                   , leden_competitie lc
               WHERE s.id = lc.spelers_id
                 AND lc.type = 'TITULARIS'
                 AND lc.competitieploegen_id = %d";
    $sql = sprintf($query, mysql_real_escape_string($_GET['id']));
    $result = mysql_query($sql, $badm_db) or badm_mysql_die();
    $basisspelers = '';
    while ($row = mysql_fetch_object($result))
    {
      $basisspelers = $basisspelers.', '.$row->naam;
    }
    mysql_free_result($result);
    echo substr($basisspelers, 2);
    echo '<o:p></o:p></span></h1>';
    echo '';
    echo '<p class=MsoNormal style="margin-left:92.15pt;text-indent:-92.15pt;tab-stops:3.0cm"><b style="mso-bidi-font-weight:normal">';
    echo '<span lang=NL style="font-size:12.0pt;mso-bidi-font-size:10.0pt">Reservespelers ';
    echo '<span style="mso-tab-count:1"> </span>: </span></b><span lang=NL style="font-size:12.0pt;mso-bidi-font-size:10.0pt">';
    $query = "SELECT s.naam
                FROM bad_spelers s
                   , leden_competitie lc
               WHERE s.id = lc.spelers_id
                 AND lc.type = 'RESERVE'
                 AND lc.competitieploegen_id = %d";
    $sql = sprintf($query, mysql_real_escape_string($_GET['id']));
    $result = mysql_query($sql, $badm_db) or badm_mysql_die();
    $reservespelers = '';
    while ($row = mysql_fetch_object($result))
    {
      $reservespelers = $reservespelers.', '.$row->naam;
    }
    mysql_free_result($result);
    echo substr($reservespelers, 2);
    echo '<o:p></o:p></span></p>';
    echo '';
    echo '<p class=MsoNormal><span lang=NL style="font-size:12.0pt;mso-bidi-font-size:10.0pt">';
    echo '<span style="mso-tab-count:2">                        </span><span';
    echo 'style="mso-spacerun:yes">   </span><o:p></o:p></span></p>';
    echo '';
    echo '<p class=MsoNormal><span lang=NL><o:p>&nbsp;</o:p></span></p>';
    echo '';
    echo '<h1 style="margin-bottom:6.0pt"><span lang=NL-BE>Ploegkapitein</span></h1>';
    echo '';
    echo '<p class=MsoNormal style="tab-stops:70.9pt 3.0cm"><span lang=NL-BE';
    echo 'style="font-size:12.0pt;mso-bidi-font-size:10.0pt;mso-ansi-language:NL-BE">Naam<span';
    echo 'style="mso-tab-count:1">               </span><span class=GramE>:<span';
    echo 'style="mso-tab-count:1">    </span><i style="mso-bidi-font-style:normal">';
    echo $ploeg->naam;
    echo '<o:p></o:p></i></span></p>';
    echo '';
    echo '<p class=MsoNormal style="tab-stops:70.9pt 3.0cm"><span lang=NL-BE';
    echo 'style="font-size:12.0pt;mso-bidi-font-size:10.0pt;mso-ansi-language:NL-BE">Adres<span';
    echo 'style="mso-tab-count:1">               </span><span class=GramE>:<span';
    echo 'style="mso-tab-count:1">    </span><i style="mso-bidi-font-style:normal">';
    echo $ploeg->adres;
    echo '<o:p></o:p></i></span></p>';
    echo '';
    echo '<p class=MsoNormal style="tab-stops:70.9pt 3.0cm"><span lang=NL-BE';
    echo 'style="font-size:12.0pt;mso-bidi-font-size:10.0pt;mso-ansi-language:NL-BE"><span';
    echo 'style="mso-tab-count:2">                            </span><i style="mso-bidi-font-style:normal">';
    echo $ploeg->postcode.' '.$ploeg->woonplaats;
    echo '<o:p></o:p></i></span></p>';
    echo '';
    echo '<p class=MsoNormal style="tab-stops:70.9pt 3.0cm"><span lang=NL-BE';
    echo 'style="font-size:12.0pt;mso-bidi-font-size:10.0pt;mso-ansi-language:NL-BE">Telefoon<span';
    echo 'style="mso-tab-count:1">           </span><span class=GramE>:<span';
    echo 'style="mso-tab-count:1">    </span></span><i style="mso-bidi-font-style:normal">';
    echo $ploeg->tel;
    echo '<o:p></o:p></i></span></p>';
    echo '<p class=MsoNormal style="tab-stops:70.9pt 3.0cm"><span lang=NL-BE';
    echo 'style="font-size:12.0pt;mso-bidi-font-size:10.0pt;mso-ansi-language:NL-BE">GSM<span';
    echo 'style="mso-tab-count:1">               </span><span class=GramE>:<span';
    echo 'style="mso-tab-count:1">    </span></span><i style="mso-bidi-font-style:normal">';
    echo $ploeg->gsm;
    echo '<o:p></o:p></i></span></p>';
    echo '<p class=MsoNormal style="tab-stops:70.9pt 3.0cm"><span lang=NL-BE';
    echo 'style="font-size:12.0pt;mso-bidi-font-size:10.0pt;mso-ansi-language:NL-BE">E-mail<span';
    echo 'style="mso-tab-count:1">              </span>:<span style="mso-tab-count:1">    </span><span';
    echo 'class=GramE><i style="mso-bidi-font-style:normal">';
    echo $ploeg->email;
    echo '<o:p></o:p></i></span></p>';
    echo '';
    echo '<p class=MsoNormal><span lang=NL-BE style="font-size:12.0pt;mso-bidi-font-size:10.0pt;mso-ansi-language:NL-BE">';
    echo '<o:p>&nbsp;</o:p></span></p>';
    echo '';
    echo '<p class=MsoNormal><span lang=NL-BE style="font-size:12.0pt;mso-bidi-font-size:10.0pt;mso-ansi-language:NL-BE">';
    echo '<o:p>&nbsp;</o:p></span></p>';
    echo '';
    echo '<h1 style="margin-bottom:6.0pt"><span lang=NL-BE>Wedstrijden<o:p></o:p></span></h1>';
    echo '';
    echo '<div style="border:none;border-bottom:solid windowtext 1.0pt;mso-border-bottom-alt:';
    echo 'solid windowtext .5pt;padding:0cm 0cm 1.0pt 0cm">';
    echo '';
    echo '<p class=MsoNormal style="tab-stops:106.35pt 177.2pt 354.4pt;border:none;';
    echo 'mso-border-bottom-alt:solid windowtext .5pt;padding:0cm;mso-padding-alt:0cm 0cm 1.0pt 0cm">';
    echo '<b style="mso-bidi-font-weight:normal"><span lang=NL-BE style="font-size:12.0pt;';
    echo 'mso-bidi-font-size:10.0pt;mso-ansi-language:NL-BE">Datum<span style="mso-tab-count:';
    echo '1">                        </span>Uur<span style="mso-tab-count:1">                 </span>Wedstrijd<span';
    echo 'style="mso-tab-count:1">                                           </span>Spelen';
    echo '( ja / neen )<o:p></o:p></span></b></p>';
    echo '';
    echo '</div>';
    echo '';
    $query = "SELECT CASE DAYOFWEEK(m.datum)
                     WHEN 1 THEN 'Zon'
                     WHEN 2 THEN 'Ma'
                     WHEN 3 THEN 'Di'
                     WHEN 4 THEN 'Wo'
                     WHEN 5 THEN 'Do'
                     WHEN 6 THEN 'Vr'
                     WHEN 7 THEN 'Zat'
                     END AS dag
                   , DATE_FORMAT(m.datum, '%%d/%%m/%%Y') AS datum
                   , m.uur
                   , m.wedstrijd
                FROM competitiematchen m
                   , bad_competitieploegen c
               WHERE c.id = m.competitieploegen_id
                 AND c.id = %d
               ORDER BY 2, 3";
    $sql = sprintf($query, mysql_real_escape_string($_GET['id']));
    $result = mysql_query($sql, $badm_db) or badm_mysql_die();
    while ($row = mysql_fetch_object($result))
    {
      echo '<p class=MsoNormal style="tab-stops:right 134.7pt left 177.2pt 354.4pt"><span';
      echo 'lang=NL style="font-size:12.0pt;mso-bidi-font-size:10.0pt;color:black;';
      echo 'mso-fareast-language:NL;layout-grid-mode:line">'.$row->dag.' '.$row->datum.'<span';
      echo 'style="mso-tab-count:1">                </span>'.$row->uur.'<span style="mso-tab-count:';
      echo '1">              </span>'.$row->wedstrijd.'<span style="mso-tab-count:1">                                  </span><o:p></o:p></span></p>';
      echo '';
    }
    mysql_free_result($result);
    echo '<p class=MsoNormal><span lang=NL style="font-size:12.0pt;mso-bidi-font-size:';
    echo '10.0pt;color:black;mso-fareast-language:NL;layout-grid-mode:line"><o:p>&nbsp;</o:p></span></p>';
    echo '';
    echo '<p class=MsoNormal><span lang=NL style="font-size:12.0pt;mso-bidi-font-size:';
    echo '10.0pt;color:black;mso-fareast-language:NL;layout-grid-mode:line"><o:p>&nbsp;</o:p></span></p>';
    echo '';
    echo '<h2><span lang=NL-BE>Invullen of je de wedstrijd al dan niet kunt spelen</span></h2>';
    echo '';
    echo '<h2><span lang=NL style="mso-ansi-language:NL">De thuiswedstrijden zijn in ';
    echo initcap($ploeg->sporthal);
    echo ' !</span>!<o:p></o:p></span></h2>';
    echo '';
    echo '<p class=MsoNormal><span lang=NL style="font-size:12.0pt;mso-bidi-font-size:';
    echo '10.0pt;color:black;mso-fareast-language:NL;layout-grid-mode:line"><o:p>&nbsp;</o:p></span></p>';
    echo '';
    echo '<p class=MsoNormal><span lang=NL-BE style="font-size:12.0pt;mso-bidi-font-size:';
    echo '10.0pt;mso-ansi-language:NL-BE"><o:p>&nbsp;</o:p></span></p>';
    echo '';
    echo '</div>';
    echo '';
    echo '</body>';
    echo '';
    echo '</html>';
  }
?>