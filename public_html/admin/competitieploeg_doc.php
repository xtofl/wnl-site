<?php
/**
 * competitieploeg_doc.php
 * 
 * object    : creates a Word file and forces a download of this file
 * author    : Freek Ceymeulen
 * created   : 06/06/2005
 * parameters : id : competitieploegen_id
 **/

  // Connect to the DB (must be done before calling sess.php)
  require_once "../functies/badm_db.inc.php";
  $badm_db = badm_conn_db();
  require_once('../functies/sess.php');

  if ($_SESSION['auth'] != true)
  {
    // niet ingelogd -> ga naar login pagina
    //session_write_close;
    mysql_close($badm_db);
    header("Location: http://www.badmintonsport.be/admin/login.php");
    exit;
  }
/*
--------------------------------------------------------------------------------------------
|| FUNCTIONS
--------------------------------------------------------------------------------------------
*/
  require_once("../functies/general_functions.php");

function ForceFileDownload($file)
{ 
  $filesize = @filesize($file);
  header("Content-Description: File Transfer");
  header("Content-Length: " . $filesize);
  header("Content-Disposition: attachment; filename=".basename($file)); 
  header("Content-Type: application/octet-stream");
  header("Content-Type: application/msword");
  header("Content-Type: application/force-download"); 
  header("Content-Type: application/download"); 
  header("Content-Transfer-Encoding: binary");
  header("Cache-Control: no-cache, must-revalidate, post-check=0, pre-check=0");
  header("Pragma: no-cache"); 
  header("Expires: 0");
  // Set the number of seconds the script is allowed to run
  @set_time_limit(600); 
  readfile($file); 
}
/*
--------------------------------------------------------------------------------------------
|| BEGIN
--------------------------------------------------------------------------------------------
*/
  // Selecteer gegevens van ploeg en ploegkapitein
  $query = "SELECT c.type
                 , c.afdeling
                 , c.sporthal
                 , s.naam
                 , s.adres
                 , s.postcode
                 , s.woonplaats
                 , s.tel
                 , s.gsm
                 , s.email
              FROM bad_competitieploegen c
              LEFT OUTER JOIN leden_functies lf ON c.id = lf.ref_id
              LEFT OUTER JOIN bad_spelers s ON lf.spelers_id = s.id
             WHERE lf.functies_id = 13
               AND c.id = %d";
  $sql = sprintf($query, mysql_real_escape_string($_GET['id']));
  $result = mysql_query($sql, $badm_db) or badm_mysql_die();
  
  if (mysql_num_rows($result) == 0)
  {
    echo "No data found";
  }
  else
  {
    $ploeg = mysql_fetch_object($result);
    mysql_free_result($result);

    $type = initcap($ploeg->type);
    if (date("n") > 5)
    {
      $cur_year = date("Y");
    }
    else
    {
      $cur_year = date("Y") - 1;
    }
    $next_year = $cur_year + 1;
    
    $file = $type."competitie_".$cur_year.".doc";
    //$file = "competieploeg.doc";
    if (!$file_handle = fopen($file, "w"))
    {
      echo "<br>Kan bestand ($file) niet openen/aanmaken";
    }
    $data = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">';
    if (!fwrite($file_handle, $data))
    {
      echo "<br/>Kan niet schrijven naar bestand ($file)";
    }
    
    fwrite($file_handle, '<html>');
    fwrite($file_handle, '<head>');
    fwrite($file_handle, '<title>'.$type.'competitie  -  '.$cur_year.' / '.$next_year.'  -  '.strtolower($ploeg->afdeling).'</title>');
    fwrite($file_handle, '<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">');
    fwrite($file_handle, '<style type="text/css"><!--');
    fwrite($file_handle, 'body {    font-family : "Times New Roman", Times, serif;    font-size : 12pt;}');
    fwrite($file_handle, 'caption {    font-weight : bold;    text-align : left;}');
    fwrite($file_handle, 'div.documentTitle {    /*border-bottom : solid 1pt;*/    font-size : 18pt;
    font-weight : bold;    text-align : center;}');
    fwrite($file_handle, 'table {    padding-right : 15px;}');
    fwrite($file_handle, '--></style>');
    fwrite($file_handle, '</head>');
    fwrite($file_handle, '<body>');
    fwrite($file_handle, '<div class="documentTitle"><center>');
    fwrite($file_handle, '<font face="Times New Roman, Times, serif" size="5"><b>');
    fwrite($file_handle, $type.'competitie'.' - '.$cur_year.' / '.$next_year.' - '.strtolower($ploeg->afdeling));
    fwrite($file_handle, '</b></font><hr></center></div>');

    $query = "SELECT s.naam
                FROM bad_spelers s
                   , leden_competitie lc
               WHERE s.id = lc.spelers_id
                 AND lc.type = 'TITULARIS'
                 AND lc.competitieploegen_id = %d";
    $sql = sprintf($query, mysql_real_escape_string($_GET['id']));
    $result = mysql_query($sql, $badm_db) or badm_mysql_die();
    $basisspelers = '';
    while ($row = mysql_fetch_object($result))
    {
      $basisspelers = $basisspelers.', '.$row->naam;
    }
    mysql_free_result($result);

    fwrite($file_handle, '<p>');
    fwrite($file_handle, '<table border="0" cellspacing="0" cellpadding="0">');
    fwrite($file_handle, '  <tr>');
    fwrite($file_handle, '    <td width="130"><b>Basisspelers :</b></td>');
    fwrite($file_handle, '    <td>'.substr($basisspelers, 2).'</td>');
    fwrite($file_handle, '  </tr>');

    $query = "SELECT s.naam
                FROM bad_spelers s
                   , leden_competitie lc
               WHERE s.id = lc.spelers_id
                 AND lc.type = 'RESERVE'
                 AND lc.competitieploegen_id = %d";
    $sql = sprintf($query, mysql_real_escape_string($_GET['id']));
    $result = mysql_query($sql, $badm_db) or badm_mysql_die();
    $reservespelers = '';
    while ($row = mysql_fetch_object($result))
    {
      $reservespelers = $reservespelers.', '.$row->naam;
    }
    mysql_free_result($result);

    fwrite($file_handle, '  <tr>');
    fwrite($file_handle, '    <td><b>Reservespelers :</b></td>');
    fwrite($file_handle, '    <td>'.substr($reservespelers, 2).'</td>');
    fwrite($file_handle, '  </tr>');
    fwrite($file_handle, '</table>');
    fwrite($file_handle, '</p><br/><p>');
    fwrite($file_handle, '<table border="0" cellspacing="0" cellpadding="0">');
    fwrite($file_handle, '<caption><b>Ploegkapitein</b></caption>');
    fwrite($file_handle, '  <tr>    <td>Naam</td>    <td>:</td>');
    fwrite($file_handle, '    <td><i>'.$ploeg->naam.'</i></td>');
    fwrite($file_handle, '  </tr>  <tr>    <td>Adres</td>    <td>:</td>');
    fwrite($file_handle, '    <td><i>'.$ploeg->adres.'</i></td>');
    fwrite($file_handle, '  </tr>  <tr>    <td>&nbsp;</td>    <td>&nbsp;</td>');
    fwrite($file_handle, '    <td><i>'.$ploeg->postcode.' '.$ploeg->woonplaats.'</i></td>');
    fwrite($file_handle, '  </tr>  <tr>    <td>Telefoon </td>    <td>:</td>');
    fwrite($file_handle, '    <td><i>'.$ploeg->tel.'</i></td>');
    fwrite($file_handle, '  </tr>  <tr>    <td>GSM</td>    <td>:</td>');
    fwrite($file_handle, '    <td><i>'.$ploeg->gsm.'</i></td>');
    fwrite($file_handle, '  </tr>  <tr>    <td>E-mail</td>    <td>:</td>');
    fwrite($file_handle, '    <td><i>'.$ploeg->email.'</i></td>');
    fwrite($file_handle, '  </tr></table></p><br/><p>');

    fwrite($file_handle, '<table border="0" cellspacing="0" cellpadding="0">');
    fwrite($file_handle, '<caption><b>Wedstrijden</b></caption>');
    fwrite($file_handle, '  <tr>');
    fwrite($file_handle, '    <th>Datum</th>');
    fwrite($file_handle, '    <th>Uur</th>');
    fwrite($file_handle, '    <th>Wedstrijd</th>');
    fwrite($file_handle, '    <th>Spelen ( ja / neen )</th>');
    fwrite($file_handle, '  </tr>');

    $query = "SELECT CASE DAYOFWEEK(m.datum)
                     WHEN 1 THEN 'Zon'
                     WHEN 2 THEN 'Ma'
                     WHEN 3 THEN 'Di'
                     WHEN 4 THEN 'Wo'
                     WHEN 5 THEN 'Do'
                     WHEN 6 THEN 'Vr'
                     WHEN 7 THEN 'Zat'
                     END AS dag
                   , DATE_FORMAT(m.datum, '%%d/%%m/%%Y') AS datum
                   , m.uur
                   , m.wedstrijd
                FROM competitiematchen m
                   , bad_competitieploegen c
               WHERE c.id = m.competitieploegen_id
                 AND c.id = %d
               ORDER BY m.datum, m.uur";
    $sql = sprintf($query, mysql_real_escape_string($_GET['id']));
    $result = mysql_query($sql, $badm_db) or badm_mysql_die();
    while ($row = mysql_fetch_object($result))
    {
      fwrite($file_handle, '  <tr>');
      fwrite($file_handle, '    <td>'.$row->dag.' '.$row->datum.'</td>');
      fwrite($file_handle, '    <td>'.$row->uur.'</td>');
      fwrite($file_handle, '    <td>'.$row->wedstrijd.'</td>');
      fwrite($file_handle, '    <td>&nbsp;</td>');
      fwrite($file_handle, '  </tr>');
    }
    mysql_free_result($result);

    fwrite($file_handle, '</table></p><br/><p><font size="2"><i>');
    fwrite($file_handle, 'Invullen of je de wedstrijd al dan niet kunt spelen</p><p>');
    fwrite($file_handle, 'De thuiswedstrijden zijn in '.initcap($ploeg->sporthal).' !!</i></font></p>');
    fwrite($file_handle, '</body>');
    fwrite($file_handle, '</html>');

    fclose($file_handle);
    ForceFileDownload($file);
  }
  //mysql_close($badm_db);
?>