<?php
/**
 * speeluren.php
 *
 * object     : Show all records from table bad_speeluren
 * author     : Freek Ceymeulen
 * created    : 01/02/2005
 * parameters : err_msg : error message
                order : column to order the data by
 **/

  // Connect to the DB (must be done before calling sess.php)
  require_once "../functies/badm_db.inc.php";
  $badm_db = badm_conn_db();
  require_once('../functies/sess.php');

  if ($_SESSION['auth'] != true)
  {
    // niet ingelogd -> ga naar login pagina
    // geef deze pagina als parameter mee zodat na succesvolle login teruggekeerd kan worden naar deze pagina
    //session_write_close;
    mysql_close($badm_db);
    header("Location: http://www.badmintonsport.be/admin/login.php?ref=".basename($PHP_SELF));
    exit;
  }
/*
--------------------------------------------------------------------------------------------
|| FUNCTIONS
--------------------------------------------------------------------------------------------
*/
  require_once("../functies/general_functions.php");
/*
--------------------------------------------------------------------------------------------
|| BEGIN
--------------------------------------------------------------------------------------------
*/
  // Initialize
  $err_msg = NULL;
  if (isset($_GET['err_msg']))
  {
    $err_msg = $_GET['err_msg'];
  }
  $orderBy = '2, 8, 4, 5';
  if (isset($_GET['order']))
  {
    $order = $_GET['order'];
    if ($order == 'plaats')
    {
      $orderBy = '2, 8, 4';
    }
    elseif ($order == 'dag')
    {
      $orderBy = '8, 4';
    }
    elseif ($order == 'uur')
    {
      $orderBy = '4, 2, 5';
    }
    elseif ($order == 'unit')
    {
      $orderBy = '5, 2, 4';
    }
    elseif ($order == 'doelgroep')
    {
      $orderBy = '6, 3, 4';
    }
    elseif ($order == 'type')
    {
      $orderBy = '7, 3, 4';
    }
    elseif ($order == 'aantal')
    {
      $orderBy = '9 DESC';
    }
    elseif ($order == 'vrij')
    {
      $orderBy = '9';
    }
    elseif ($order == 'reserve')
    {
      $orderBy = '13';
    }
  }
?>
<html>
<head>
<title>W&amp;L Admin Module</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="http://www.badmintonsport.be/css/admin.css" rel="stylesheet" type="text/css">
<style type="text/css">
<!--
@media print {
img, input, button, #menu, #doc {
   visibility : hidden;
}
}
-->
</style>
</head>
<body bgcolor="#3A6EA5" link="#000000" vlink="#000000" topmargin="0">

<span style="font-size: 8pt; float: right"><?php echo $_SESSION['username']; ?></span>
<?php
  list($wday,$mday,$month,$year,$hour,$minutes) = split("( )",date("w j n Y H i",time()));
  $weekday = array('zondag', 'maandag', 'dinsdag', 'woensdag', 'donderdag', 'vrijdag', 'zaterdag');
  $months  = array('januari','februari','maart','april','mei','juni','juli','augustus','september','oktober','november','december');
  $newdate = "$weekday[$wday], $mday ".$months[$month-1]." $year $hour:$minutes";
?>
<span style="font-size: 8pt"><?php echo $newdate; ?></span>

<table border="1" cellspacing="0" cellpadding="0" align="center" width="810">
  <tr bgcolor="#C6C3C6" valign="top"><td>
    <table width="100%" border="0" cellspacing="0" cellpadding="1" align="center">
      <tr bgcolor="#400080">
        <td height="20" class="title">&nbsp;W&amp;L - Speeluren</td>
        <td height="20" align="right" bgcolor="#000084">
          <a href="http://www.badmintonsport.be/docs/W_L_admin_users_guide.doc" target="_blank"><img src="../poll/image/help.gif" width="16" height="14" border="0" alt="Gebruikershandleiding"></a><a href="login.php?action=logout"><img src="../poll/image/cross.gif" width="16" height="14" border="0" alt="Uitloggen"></a></td>
      </tr>
    </table>
    <table border="0" cellspacing="0" cellpadding="0">
      <tr valign="top">
        <td width="100">
<?php
  // Print the menu
  write_menu($_SERVER['PHP_SELF']);
?>
        </td>
        <td>
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
<?php
  if (!is_null($err_msg))
  {
    echo '<tr><td><font color="red"><b>'.$err_msg.'</b></font></td></tr>';
  }
?>
            <tr>
              <td><hr>
                <table border="1" cellspacing="0" cellpadding="3" frame="below" rules="groups">
                  <tr>
                    <th><a href="<?php echo basename($PHP_SELF); ?>?order=plaats" title="Sorteer">Plaats</a></th>
                    <th><a href="<?php echo basename($PHP_SELF); ?>?order=dag" title="Sorteer">Dag</a></th>
                    <th><a href="<?php echo basename($PHP_SELF); ?>?order=uur" title="Sorteer">Uur</a></th>
                    <th><a href="<?php echo basename($PHP_SELF); ?>?order=unit" title="Sorteer">Unit</a></th>
                    <th><a href="<?php echo basename($PHP_SELF); ?>?order=doelgroep" title="Sorteer">Doelgr</a></th>
                    <th><a href="<?php echo basename($PHP_SELF); ?>?order=type" title="Sorteer">Type</a></th>
                    <th><a href="<?php echo basename($PHP_SELF); ?>?order=aantal" title="Sorteer">Aantal</a></th>
                    <th><a href="<?php echo basename($PHP_SELF); ?>?order=vrij" title="Sorteer">Vrij</a></th>
                    <th><a href="<?php echo basename($PHP_SELF); ?>?order=reserve" title="Sorteer">Reserve</a></th>
                  </tr>
<?php
  $query  = "SELECT u.id
                  , u.plaats AS plaats
                  , LOWER(u.dag) AS dag
                  , u.uur
                  , u.unit
                  , u.doelgroep
                  , u.type
                  , CASE u.dag WHEN 'maandag'   THEN 1
                               WHEN 'dinsdag'   THEN 2
                               WHEN 'woensdag'  THEN 3
                               WHEN 'donderdag' THEN 4
                               WHEN 'vrijdag'   THEN 5
                               WHEN 'zaterdag'  THEN 6
                               WHEN 'zondag'    THEN 7
                    END AS dagnummer
                  , COUNT(lu.spelers_id) AS aantal
                  , u.aantal_plaatsen
                  , u.aantal_reserve
                  , u.prijs
                  , COUNT(CASE WHEN lu.reserve = 'Y' THEN 1 END) AS reserve
               FROM bad_speeluren u LEFT OUTER JOIN leden_uren lu ON u.id = lu.speeluren_id
                                    LEFT OUTER JOIN bad_spelers s ON lu.spelers_id = s.id
              WHERE ( s.eind_dt IS NULL OR s.eind_dt > NOW() )
           GROUP BY u.id
                  , u.plaats
                  , LOWER(u.dag)
                  , u.uur
                  , u.unit
                  , u.doelgroep
                  , u.type
                  , CASE u.dag WHEN 'maandag'   THEN 1
                               WHEN 'dinsdag'   THEN 2
                               WHEN 'woensdag'  THEN 3
                               WHEN 'donderdag' THEN 4
                               WHEN 'vrijdag'   THEN 5
                               WHEN 'zaterdag'  THEN 6
                               WHEN 'zondag'    THEN 7
                    END
                  , u.aantal_plaatsen
                  , u.aantal_reserve
                  , u.prijs
           ORDER BY %s";
  $sql  = sprintf($query, mysql_real_escape_string($orderBy));
  $result = mysql_query($sql, $badm_db) or badm_mysql_die();

  $totaal = mysql_num_rows($result);
  $tot_vrij = 0;
  for ($i=0; $i < $totaal; $i++)
  {
    $row = mysql_fetch_assoc($result);
    echo '                  <tr class="'; echo ($i%2==0) ? "even" : "odd"; echo '">';
    echo '                    <td class="td2">'.initcap($row['plaats']).'</td>';
    echo '                    <td class="td2"><a href="bewerken_speeluur.php?id='.$row['id'].'">'.$row['dag'].'</a></td>';
    echo '                    <td class="td2" align="right"><a href="bewerken_speeluur.php?id='.$row['id'].'">'.$row['uur'].'</a></td>';
    echo '                    <td class="td2" align="center">'.nvl($row['unit'], '&nbsp;').'</td>';
    echo '                    <td class="td2" align="center">'.$row['doelgroep'].'</td>';
    echo '                    <td class="td2">'.$row['type'].'</td>';
    echo '                    <td class="td2" align="right">'.$row['aantal'].'</td>';
    $vrij = nvl($row['aantal_plaatsen'], 16) - $row['aantal'];
    if ($vrij < 0)
    {
      $vrij = 0;
    }
    $tot_vrij = $tot_vrij + $vrij;
    if ($vrij > 0)
    {
      $vrij = '<b>'.$vrij.'</b>';
    }
    echo '                    <td class="td2" align="right">'.$vrij.'</td>';
    echo '                    <td class="td2" align="right">'.$row['reserve'].'</td>';
    echo '                  </tr>';
  }
  mysql_free_result($result);

  // Stel de mailing list samen van alle dagverantwoordelijken
  $query  = "SELECT DISTINCT s.naam
                  , s.email
               FROM bad_spelers s
                  , leden_functies lf
              WHERE s.id = lf.spelers_id
                AND lf.functies_id = 25
                AND s.email IS NOT NULL";
  $result = mysql_query($query, $badm_db) or badm_mysql_die();

  $mailing_list = '';
  while ($row = mysql_fetch_object($result))
  {
    // build list with E-mail adresses
    if (strlen($row->email) > 0)
    {
      $mailing_list .= ','.$row->naam.'<'.$row->email.'>';
    }
  }

  mysql_free_result($result);
?>
                </table>
              </td>
            </tr>
            <form name="speeluren" action="zend_email.php" method="POST">
            <tr>
              <td class="td2"><hr>
                <span style="float: right">
                <input type="hidden" name="aan" value="<?php echo substr($mailing_list, 1); ?>">
                <input type="submit" name="email" value="Verstuur Email" class="button" title="Verstuur een e-mail naar alle dagverantwoordelijken" accesskey="E">
                </span>
                Totaal: <b><?php echo $totaal; ?></b> speeluren, totaal vrije plaatsen: <b><?php echo $tot_vrij; ?></b></td>
            </tr>
            </form>
          </table>
          <div id="doc">
          <h4>&nbsp;&nbsp;&nbsp;&nbsp;Documenten</h4><p>
          <a href="http://www.badmintonsport.be/docs/Inlichtingenblad.doc" target="_blank">&nbsp;&nbsp;Aansluitingsformulier W&amp;L bv</a><br/>
          <a href="http://www.badmintonsport.be/docs/aansluitingsformuliervbl.xls" target="_blank">&nbsp;&nbsp;Aansluitingsformulier VBL</a><br/>
<!--          <a href="http://www.badmintonsport.be/docs/Inlichtingenblad2007.doc" target="_blank">&nbsp;&nbsp;Wijziging speeluren W&amp;L bv</a><br/>-->
          <a href="http://www.badmintonsport.be/docs/prijslijst.doc" target="_blank">&nbsp;&nbsp;Prijslijst W&amp;L bv</a><br>
      &nbsp;&nbsp;<a href="http://www.badmintonsport.be/docs/verzekeringspapieren.doc" target="_blank">Verzekering</a>
          </p>
          </div>
      </td></tr>
    </table>
  </td></tr>
</table>
<br>
<br>
<?php
  //mysql_close($badm_db);
?>
</body>
</html>