<?php

/**
 * addHomepageContent.php
 *
 * author     : Freek Ceymeulen
 * created    : 24/08/2008
 * parameters : id (optional) homepage_content_id
 *              submit       [Requery|Delete]
 **/

  // Connect to the DB (must be done before calling sess.php)
  require_once "../functies/badm_db.inc.php";
  $badm_db = badm_conn_db();
  require_once('../functies/sess.php');

  if (!isset($_SESSION['auth']))
  {
    // niet ingelogd -> ga naar login pagina
    // geef deze pagina als parameter mee zodat na succesvolle login teruggekeerd kan worden naar deze pagina
    //session_write_close;
    mysql_close($badm_db);
    header("Location: http://www.badmintonsport.be/admin/login.php?ref=".$_SERVER['REQUEST_URI']);
    exit;
  }

  // Alleen webmasters en bestuursleden mogen deze pagina gebruiken
  if (!isset($_SESSION['bestuurslid']) && !isset($_SESSION['administrator']))
  {
    mysql_close($badm_db);
    header("Location: http://www.badmintonsport.be/admin/login.php");
    exit;
  }

  define("ERROR_IMG", "<img src=\"../images/error.gif\" height=\"16\" width=\"16\" alt=\"Error\">");
  define("INFO_IMG" , "<img src=\"../images/info.gif\" height=\"16\" width=\"16\" alt=\"Info\">");

  require_once("../functies/general_functions.php");
  include_once("../fckeditor/fckeditor.php");


  // Display the error for the given field, if any
  function fieldError($fieldName, $errors)
  {
    if (isset($errors[$fieldName]))
    {
      echo ERROR_IMG." <font color=\"red\">".$errors[$fieldName]."</font><br>";
    }
  }

/***************************************
 * notify_about_new_topic
 **************************************/
 function notify_about_new_topic($user, $title)
 {
   // Connect
   require_once "../functies/badm_db.inc.php";
   $badm_db = badm_conn_db();
   // Check that the item is not yet added to the RSS feed
   /*$query = "SELECT id FROM rss WHERE title = '%s'";
   $sql  = sprintf($query, mysql_real_escape_string($title) );
   $result = mysql_query($sql, $badm_db) or badm_mysql_die();
   $rss = mysql_fetch_object($result);
   if (mysql_num_rows($result) == 0)
   {*/
     $stmt = "INSERT INTO rss ( title, link, description )
            VALUES ( '%s', '%s', '%s' )";
   $sql  = sprintf($stmt, mysql_real_escape_string($title)
                        , mysql_real_escape_string("http://www.badmintonsport.be")
              , mysql_real_escape_string("New topic added by ".$user) );
   $result = mysql_query($sql, $badm_db) or badm_mysql_die();
   //}
 }

  // Register an error array - just in case!
  if (!session_is_registered("errors"))
  {
    session_register("errors");
  }
  // Clear any errors that might have been found previously
  $errors = array();

  // Initialize message
  $msg = "";
  //
  $id = null;
  if (isset($_REQUEST["id"]))
  {
    $id = $_REQUEST["id"];
    if (!empty($id) && !is_numeric($id))
    {
      mail("freek.ceymeulen@pandora.be", "Possible break-in attempt", $_SESSION['username']." on page addHomepageContent.php from IP ".$_SERVER['REMOTE_ADDR']." using method ".$_SERVER['REQUEST_METHOD']." on url ".$PHP_SELF."?".urlencode($_SERVER["QUERY_STRING"])." coming from ".$_SERVER["HTTP_REFERER"].": HomePage_Content_id \"".$id."\" is not numeric.", "From: webmaster@badmintonsport.be");
      exit;
    }
    $action = "Wijzig dit homepage topic | <a href=\"addHomepageContent.php\" target=\"_self\">Een nieuw topic maken</a>";
  }
  else
  {
    $action = "Voeg een nieuw topic toe aan de homepage";
  }
  // Handle a submit
  if (isset($_REQUEST["submit"]))
  {
    $title = $_REQUEST["title"];
  $message = stripslashes($_REQUEST["FCKeditor1"]);
    $from_date = $_REQUEST["from_date"];
    $thru_date = $_REQUEST["thru_date"];
    // Validate title
    if (empty($title))
    {
      $errors["validation"] = "You have not entered a Title.";
    }
    else
    {
      // Validate message
      if (empty($message))
      {
        $errors["validation"] = "You have not entered a Message.";
      }
      else
      {
        // Validate from_date
        if (empty($from_date))
        {
          $errors["validation"] = "You have not entered a Publication Date.";
        }
        elseif (!empty($err_msg))
        {
          $errors["validation"] = $err_msg;
        }
        else
        {
          // Validate thru_date
          if (empty($thru_date))
          {
            $errors["validation"] = "You have not entered an Expiration Date.";
          }
          elseif (!empty($err_msg))
          {
            $errors["validation"] = $err_msg;
          }
          else
          {
        $from_date = get_valid_date($from_date);
        $thru_date = get_valid_date($thru_date);
            if (empty($id)) // insert
            {
              //
              $insert_statement = "INSERT INTO homepage_content ( title, message, from_date, thru_date, dt_crea, usid_crea )"
                                 ."VALUES ( '%s', '%s', '%s', '%s', NOW(), '%s' )";
              $sql = sprintf($insert_statement, mysql_real_escape_string($title)
                                              , mysql_real_escape_string($message)
                                              , mysql_real_escape_string($from_date)
                                              , mysql_real_escape_string($thru_date)
                                              , mysql_real_escape_string($_SESSION['username']));
              mysql_query($sql) or die('Insert failed: ' . mysql_error());
              // Give feedback about what happened
              $msg = ERROR_IMG."Insert failed.";
              if (mysql_affected_rows() == 1)
              {
                $msg = INFO_IMG." Message created.<br>";
        notify_about_new_topic($_SESSION['username'], $title);
              }
            }
            else // update
            {
              //
              $update_statement = "UPDATE homepage_content"
                                 ."   SET title     = '%s'"
                                 ."     , message   = '%s'"
                                 ."     , from_date = '%s'"
                                 ."     , thru_date = '%s'"
                                 ."     , dt_wijz   = NOW()"
                                 ."     , usid_wijz = '%s'"
                                 ." WHERE id = %d";
              $sql = sprintf($update_statement, mysql_real_escape_string($title)
                                              , mysql_real_escape_string($message)
                                              , mysql_real_escape_string($from_date)
                                              , mysql_real_escape_string($thru_date)
                                              , mysql_real_escape_string($_SESSION['username'])
                                              , mysql_real_escape_string($id));
              mysql_query($sql) or die('Update failed: ' . mysql_error());
              // Give feedback about what happened
              $msg = ERROR_IMG."Update failed.";
              if (mysql_affected_rows() == 1)
              {
                $msg = INFO_IMG." Message updated.<br>";
              }
            }
          }
        }
      }
    }
  } // end submit
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html lang="en">

<head>
 <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
 <title>W&amp;L - Add HomePage Content</title>
 <style type="text/css">
 <!--
  div.title {
      background-color: #ccc;
      border-color: black;
      border-style: solid;
      border-width: 1px;
      margin-left: 10px;
      padding: 5px;
      width: 95%;
  }
  div.content {
      margin-left   : 20px;
      padding-top   : 20px;
      padding-bottom: 20px;
  }
  td {
      padding-left : 6px;
      padding-right: 6px;
  }
  -->
 </style>
 <script language="javascript" type="text/javascript">
 <!--
  function validate_input ()
  {
    var l_return = true;
    l_info_img = '<img src="../images/info.gif" height="16" width="16" alt="Info">&nbsp;';
    if (document.form.title.value == "")
    {
      document.getElementById('message').innerHtml = l_info_img + 'Gelieve een titel in te vullen';
    alert('Gelieve een titel in te vullen');
      l_return = false;
    }
/*    if (document.form.FCKeditor1___Frame.innerHtml == "")
    {
      document.getElementById('message').innerHtml = l_info_img + 'Gelieve een boodschap in te vullen';
    alert('Gelieve een boodschap in te vullen');
      l_return = false;
    }*/
    if (document.form.from_date.value == "")
    {
      document.getElementById('message').innerHtml = l_info_img + 'Gelieve een publicatiedatum in te vullen';
    alert('Gelieve een publicatiedatum in te vullen');
      l_return = false;
    }
    if (document.form.thru_date.value == "")
    {
      document.getElementById('message').innerHtml = l_info_img + 'Gelieve een vervaldatum in te vullen';
    alert('Gelieve een vervaldatum in te vullen');
      l_return = false;
    }
    return l_return;
  }
  -->
 </script>
</head>

<body>

<form name="form" action="<?php echo $PHP_SELF; ?>" method="POST" onSubmit="return validate_input()">

<div style="float:right;">
 You are logged in as <?php echo $_SESSION['username']; ?><br>
 <font size="-1">If you are not <?php echo $_SESSION['username']; ?>, click <a href="http://www.badmintonsport.be/admin/login.php?action=logout&ref=<?php echo basename($PHP_SELF); ?>" target="" title="logout">here</a></font>
</div>
W&amp;L homepage content management page
<br>
<br>
<div id="message" style="padding-left: 30px; background-color: #FAF8CC;"><?php echo $msg; ?></div>
<p>Return to the <a href="http://www.badmintonsport.be">homepage</a></p>
<br>
<?php
  if (empty($msg))
  {
?>
<div class="title"><?php echo $action; ?></div>

<div class="content">
<?php
  fieldError("validation", $errors);
?>
<?php
  if (!empty($id))
  {
    // Get the data
    $query = "SELECT title"
            ."     , message"
            ."     , DATE_FORMAT(from_date, '%%d/%%m/%%Y') AS from_date"
            ."     , DATE_FORMAT(thru_date, '%%d/%%m/%%Y') AS thru_date"
            ."  FROM homepage_content"
            ." WHERE id = '%d'";
    $sql  = sprintf($query, mysql_real_escape_string($id));
    $result = mysql_query($sql) or die('Query failed: ' . mysql_error());
    $title     = mysql_result($result, 0, "title");
    $message   = mysql_result($result, 0, "message");
    $from_date = mysql_result($result, 0, "from_date");
    $thru_date = mysql_result($result, 0, "thru_date");
  }
  if (is_null($from_date))
  {
    $from_date = date("d/m/Y"); // set by default on today
  }
?>
 <label for="title">Titel</label>
 <input type="text" name="title" value="<?php echo $title; ?>" size="75" maxlength="128">
 <br>
 <br>
<?php
  $oFCKeditor = new FCKeditor('FCKeditor1');
  $oFCKeditor->BasePath = '/fckeditor/';
  $oFCKeditor->ToolbarSet = 'Ceybie'; //Basic,Default
  //$oFCKeditor->Config["CustomConfigurationsPath"] = '/myconfig.js';
  $oFCKeditor->Height = 300;
  $oFCKeditor->Config["BaseHref"] = 'http://www.badmintonsport.be/';
  $oFCKeditor->Config["DefaultLinkTarget"] = '_blank';
  $oFCKeditor->Config["EnterMode"] = 'br'; //p, div
  $oFCKeditor->Config["DefaultLanguage"] = 'nl';
  $oFCKeditor->Config["FlashBrowser"] = false;
  $oFCKeditor->Config["LinkUploadAllowedExtensions"] = ".(avi|doc|docx|gif|jpeg|jpg|mov|mp3|mpeg|mpg|pdf|png|pps|ppt|swf|txt|wma|wmv|xls|xml|zip)$";
  $oFCKeditor->Config["ImageUploadAllowedExtensions"] = ".(jpg|gif|jpeg|png|bmp)$";
  $oFCKeditor->Config["FlashUpload"] = false;
  $oFCKeditor->Value = $message;
  $oFCKeditor->Create() ;
?>
 <br>
 <br>
 <label for="from_date">Publicatiedatum <span style="font-size:small">(DD/MM/YYYY)</span></label>
 <input type="text" name="from_date" value="<?php echo $from_date; ?>" size="9" maxlength="10">
 <label for="thru_date">Vervaldatum <span style="font-size:small">(DD/MM/YYYY)</span></label>
 <input type="text" name="thru_date" value="<?php echo $thru_date; ?>" size="9" maxlength="10">
 <input type="hidden" name="id" value="<?php echo $id; ?>">
 <br>
 <br>
 <input type="submit" name="submit" value="Opslaan">
</div>
<?php
  }
?>

</form>
</body>
</html>
