<?php
/**
 * zend_email.php
 * 
 * author    : Freek Ceymeulen
 * created   : 01/02/2005
 * variables : 
 **/

  // Connect to the DB (must be done before calling sess.php)
  require_once "../functies/badm_db.inc.php";
  $badm_db = badm_conn_db();
  require_once('../functies/sess.php');

  if ($_SESSION['auth'] != true)
  {
    // niet ingelogd -> ga naar login pagina
    // geef deze pagina als parameter mee zodat na succesvolle login teruggekeerd kan worden naar deze pagina
    //session_write_close;
    mysql_close($badm_db);
    header("Location: http://www.badmintonsport.be/admin/login.php?ref=".basename($PHP_SELF));
    exit;
  }
/*
--------------------------------------------------------------------------------------------
|| FUNCTIONS
--------------------------------------------------------------------------------------------
*/
  require_once("../functies/general_functions.php");
  
  function init ($var)
  {
    if (isset($_POST[$var]))
    {
      return $_POST[$var];
    }
    elseif (isset($_GET[$var]))
    {
      return $_GET[$var];
    }
    else return '';
  }
/*
--------------------------------------------------------------------------------------------
|| BEGIN
--------------------------------------------------------------------------------------------
*/
  // Get e-mail adres of current user
  $query = "SELECT email
              FROM bad_spelers
             WHERE naam = '%s'";
  $sql  = sprintf($query, mysql_real_escape_string($_SESSION['username']));
  $result = mysql_query($sql, $badm_db) or badm_mysql_die();
  
  // Initialize variables
  $from = $_SESSION['username'].'<'.mysql_result($result, 0, "email").'>';
  $to = init('aan');
  $cc = init('cc');
  $bcc = init('bcc');
  $subject = init('onderwerp');
  $url = init('url');
  $tekst = init('tekst');
  
  $admin = False;
  $enabled = ' DISABLED';
  if (isset($_SESSION['administrator']) && $_SESSION['administrator'] == True)
  {
    $admin = True;
    $enabled = '';
  }

  if ($_SERVER['REQUEST_METHOD'] == 'POST' && $_POST['command'] == 'send')
  {
    $content_type = "text/html; charset=iso-8859-1";

    $from    = stripslashes($from);
    $to      = stripslashes($to);
    $cc      = stripslashes($cc);
    $bcc     = stripslashes($bcc);
    // Zet <br /> voor elke newline
    $message = nl2br($tekst);
    
    if (strlen($url) > 0)
    {
      // send the specified webpage instead of text
      $message = file_get_contents($url);
    }
    
    $attachment = $_FILES['bijlage']['name'];
    if (strlen($attachment) > 0)
    {
      // generate a random string to be used as the boundary marker
      $mime_boundary = "==Multipart_Boundary_x".md5(mt_rand())."x";
      
      // store the file information to variables for easier access
      $tmp_name = $_FILES['bijlage']['tmp_name'];
      $type     = $_FILES['bijlage']['type'];
      $size     = $_FILES['bijlage']['size'];
      
      // if the upload succeeded, the file will exist
      if (file_exists($tmp_name))
      {
        // check to make sure that it is an uploaded file and not a system file
        if (is_uploaded_file($tmp_name))
        {
          // open the file for a binary read
          $file = fopen($tmp_name, 'rb');
          // read the file content into a variable
          $data = fread($file, filesize($tmp_name));
          // close the file
          fclose($file);
          // now we encode it and split it into acceptable length lines
          $data = chunk_split(base64_encode($data));
          // set content type
          $content_type = "multipart/mixed;";
        }
      }
    }

    // now we'll build the message headers
    $headers = "From: $from\r\n";
    $headers .= "Cc: $cc\r\n";
    $headers .= "Bcc: $bcc\r\n";
    $headers .= "MIME-Version: 1.0\r\n";
    $headers .= "Content-Type: $content_type\r\n";
    
    if ($content_type == "multipart/mixed;")
    {
      $headers .= " boundary=\"{$mime_boundary}\"";
      //$message .= "\n\n";
      $message = "--{$mime_boundary}\n";
      $message .= "Content-Type: text/html; charset=\"iso-8859-1\"\n";
      $message .= "Content-Transfer-Encoding: 7bit\n\n";
      $message .= $tekst."\n\n";
      
      // now we'll insert a boundary to indicate we're starting the attachment
      // we have to specify the content type, file name, and disposition as
      // an attachment, then add the file content and set another boundary to
      // indicate that the end of the file has been reached
      
      $message .= "--{$mime_boundary}\n";
      $message .= "Content-Type: {$type};\n";
      $message .= " name=\"{$attachment}\"\n";
         //$message .= "Content-Disposition: attachment;\n";
         //$message .= " filename=\"{$fileatt_name}\"\n";
      $message .= "Content-Transfer-Encoding: base64\n\n";
      $message .= $data . "\n\n";
      $message .= "--{$mime_boundary}--\n";
    }
    
    if (mail($to, $subject, $message, $headers))
    {
      header("Location: http://www.badmintonsport.be/admin/verzonden.php");
      exit;
    }
    else
    {
      $errormsg = '<font color="red"><b>Het verzenden is mislukt!</b></font><br/>';
    }
  }
?>
<html>
<head>
<title>W&amp;L Admin Module</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="ROBOTS" content="NOINDEX">
<link href="http://www.badmintonsport.be/css/admin.css" rel="stylesheet" type="text/css">
</head>
<body bgcolor="#3A6EA5" link="#000000" vlink="#000000" topmargin="0">

<span style="font-size: 8pt; float: right"><?php echo $_SESSION['username']; ?></span>
<?php
  // Build current date and time string
  list($wday,$mday,$month,$year,$hour,$minutes) = split("( )",date("w j n Y H i",time()));
  $weekday = array('zondag', 'maandag', 'dinsdag', 'woensdag', 'donderdag', 'vrijdag', 'zaterdag');
  $months  = array('januari','februari','maart','april','mei','juni','juli','augustus','september','oktober','november','december');
  $newdate = "$weekday[$wday], $mday ".$months[$month-1]." $year $hour:$minutes";
?>
<span style="font-size: 8pt"><?php echo $newdate; ?></span>

<table border="1" cellspacing="0" cellpadding="0" align="center" width="810">
  <tr bgcolor="#C6C3C6" valign="top"><td>
    <table width="100%" border="0" cellspacing="0" cellpadding="1" align="center">
      <tr bgcolor="#400080">
        <td height="20" class="title">&nbsp;W&amp;L - E-mail</td>
        <td height="20" align="right" bgcolor="#000084">
          <a href="http://www.badmintonsport.be/docs/W_L_admin_users_guide.doc" target="_blank"><img src="../poll/image/help.gif" width="16" height="14" border="0" alt="Gebruikershandleiding"></a><a href="login.php?action=logout"><img src="../poll/image/cross.gif" width="16" height="14" border="0" alt="Uitloggen"></a></td>
      </tr>
    </table>
    <table border="0" cellspacing="0" cellpadding="0">
      <tr valign="top">
        <td width="100">
<?php
  // Print the menu
  write_menu($_SERVER['PHP_SELF']);
?>
        </td>
        <td>
          <form name="zend_mail" action="<?php echo basename($PHP_SELF); ?>" method="POST" enctype="multipart/form-data">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr><hr>
              <td class="td2">&nbsp;Van : </td>
              <td><?php echo $errormsg; ?>
                  <input class="input" type="text" name="van" style="width: 600px"<?php echo $enabled; ?> value="<?php echo $from; ?>"></td>
            </tr>
            <tr>
              <td class="td2">&nbsp;Aan : </td>
              <td><input class="input" type="text" name="aan" style="width: 600px" value="<?php echo $to; ?>"></td>
            </tr>
            <tr>
              <td class="td2">&nbsp;CC : </td>
              <td><input class="input" type="text" name="cc" style="width: 600px" value="<?php echo $cc; ?>"></td>
            </tr>
            <tr>
              <td class="td2">&nbsp;BCC : </td>
              <td><input class="input" type="text" name="bcc" style="width: 600px" value="<?php echo $bcc; ?>"></td>
            </tr>
            <tr>
              <td class="td2">&nbsp;Bijlage : </td>
              <td><input class="input" type="file" name="bijlage" style="width: 600px" value="<?php echo $attachment; ?>"></td>
            </tr>
            <tr>
              <td class="td2">&nbsp;Onderwerp : </td>
              <td><input class="input" type="text" name="onderwerp" style="width: 600px" value="<?php echo $subject; ?>"></td>
            </tr>
<?php
  if ($admin == True)
  {
?>
            <tr>
              <td class="td2">&nbsp;Url : </td>
              <td><input class="input" type="text" name="url" style="width: 600px" value="<?php echo $url; ?>"></td>
            </tr>
<?php
  }
?>
            <tr>
              <td class="td2">&nbsp;Tekst : </td>
              <td><textarea class="text" name="tekst" rows="10" cols="50"><?php echo $tekst; ?></textarea></td>
            </tr>
            <tr>
              <td class="td2" colspan="2" align="center"><hr>
                <input type="hidden" name="command" value="send">
                <input type="submit" name="email" value="   Zend   " class="button" title="Verstuur bericht">
              </td>
            </tr>
            </form>
          </table>
      </td></tr>
    </table>
  </td></tr>
</table>
<br>
<br>
<?php
  //mysql_close($badm_db);
?>
</body>
</html>