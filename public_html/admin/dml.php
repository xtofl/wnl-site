<?php
/**
 * dml.php
 *
 * object     : This page handles all data manipulation for the admin page tables
 * author     : Freek Ceymeulen
 * created    : 10/04/2005
 *              02/11/2012 aantal_plaatsen, aantal_reserve en prijs toegevoegd
 *              23/11/2013 jeugd = -19 ipv -18
 *                         statuut jeugd valt weg
 *                         basisbedrag = 98 voor CT, 78 voor R
 *                         geen vroegboekkorting meer noch boete
 * parameters : page : the page that called dml.php
 *              command : the statement that is to be conducted
 **/

  // Connect to the DB (must be done before calling sess.php)
  require_once "../functies/badm_db.inc.php";
  $badm_db = badm_conn_db();
  require_once('../functies/sess.php');

/*--------------------------------------------------------------------------------------------
 | CONSTANTS
 --------------------------------------------------------------------------------------------*/

  define("BASE_URL", "http://www.badmintonsport.be/admin/");
  define("LOGGING", False);            // Zet deze variabele op true om logging te enabelen
  define("DAGVERANTWOORDELIJKE", 25);  // id van dagverantwoordelijke in tabel CLUBFUNCTIES
  define("PLOEGKAPITEIN", 13);         // id van ploegkapitein in tabel CLUBFUNCTIES
  define("TRAINER", 26);               // id van trainer in tabel CLUBFUNCTIES

/*--------------------------------------------------------------------------------------------
 | SECURITY
 --------------------------------------------------------------------------------------------*/

  if ($_SESSION['auth'] != true)
  {
    // niet ingelogd -> ga naar login pagina
    mail("freek.ceymeulen@pandora.be", "Not authenticated", "On page ".$_SERVER['PHP_SELF']." from IP ".$_SERVER['REMOTE_ADDR']." using method ".$_SERVER['REQUEST_METHOD']." on url ".$_SERVER['REQUEST_URI'].": Sessie variable 'auth' is not true.", "From: webmaster@badmintonsport.be");
    //session_write_close();
    mysql_close($badm_db);
    header("Location: ".BASE_URL."login.php");
    exit();
  }

/*--------------------------------------------------------------------------------------------
 | FUNCTIONS
 --------------------------------------------------------------------------------------------*/

  require_once("../functies/general_functions.php");

  function redirect($conn, $url)
  {
    mysql_close($conn);
    //session_write_close();
    header("Location: ".$url);
    exit();
  }


  function log_action($user_wijz, $actie, $id, $stmt, $conn)
  {
    if (LOGGING)
    {
      // Sla de bewerking op in audit tabel
      $audit = "INSERT INTO admin_audit (user_id, actie, id, inhoud)
                VALUES ('%s','%s',%d,'%s')";
      $sql  = sprintf($audit, mysql_real_escape_string($user_wijz)
                            , mysql_real_escape_string($actie)
                            , mysql_real_escape_string($id)
                            , mysql_real_escape_string(addslashes($stmt)));
      $result = mysql_query($sql, $conn) or die("Invalid query: " . mysql_error());
    }
  }


  function quote_smart($value)
  {
    // Stripslashes
    if (get_magic_quotes_gpc())
    {
       $value = stripslashes($value);
    }
    // Quote if not integer
    if (!is_numeric($value))
    {
        $value = "'" . mysql_real_escape_string($value) . "'";
    }
    return $value;
  }


  function bewerken_speeluren($conn, $spelers_id)
  {
    // Verwijder alle records uit tabel LEDEN_UREN voor deze speler
    require_once "dml/leden_uren.php";
    delete_leden_uren($conn, $spelers_id);

    // Voeg deze speler toe in tabel leden_uren voor alle aangevinkte uren
    $sql = "SELECT id FROM bad_speeluren";
    $result = mysql_query($sql, $conn) or die("Invalid query: " . mysql_error());

    while ($row = mysql_fetch_object($result))
    {
      // Alleen checkboxen die aangevinkt zijn worden gepost
      $uur = "uur".$row->id;
      if (isset($_REQUEST[$uur]))
      {
        insert_leden_uren($conn, $spelers_id, $row->id);
      }
    }
    mysql_free_result($result);
  }


  function bewerken_spelers($conn, $spelers_id)
  {
    $err_msg = "";

    // Wijzig spelersinfo
    if (!isset($_REQUEST['achternaam']))
    {
      $err_msg = "achternaam";
    }
    else if (!isset($_REQUEST['voornaam']))
    {
      $err_msg = "voornaam";
    }
    else if (!isset($_REQUEST['geb_dt']))
    {
      $err_msg = "geb_dt";
    }
    else if (!isset($_REQUEST['klassement']))
    {
      $err_msg = "klassement";
    }
    else if (!isset($_REQUEST['geslacht']))
    {
      $err_msg = "geslacht";
    }
    else if (!isset($_REQUEST['lidnr']))
    {
      $err_msg = "lidnr";
    }
    else if (!isset($_REQUEST['type']))
    {
      $err_msg = "type";
    }
    else if (!isset($_REQUEST['adres']))
    {
      $err_msg = "adres";
    }
    else if (!isset($_REQUEST['postcode']))
    {
      $err_msg = "postcode";
    }
    else if (!isset($_REQUEST['gemeente']))
    {
      $err_msg = "gemeente";
    }
    else if (!isset($_REQUEST['email']))
    {
      $err_msg = "email";
    }
    else if (!isset($_REQUEST['tel']))
    {
      $err_msg = "tel";
    }
    else if (!isset($_REQUEST['gsm']))
    {
      $err_msg = "gsm";
    }
    else if (!isset($_REQUEST['club_dt']))
    {
      $err_msg = "club_dt";
    }
    else if (!isset($_REQUEST['eind_dt']))
    {
      $err_msg = "eind_dt";
    }
    else if (!isset($_REQUEST['maat']))
    {
      $err_msg = "maat";
    }
    else if (!isset($_REQUEST['competitie']))
    {
      $err_msg = "competitie";
    }
    else if (!isset($_REQUEST['betaald']))
    {
      $err_msg = "betaald";
    }
    else if (!isset($_REQUEST['lidgeld']))
    {
      $err_msg = "lidgeld";
    }
    else if (!isset($_REQUEST['clubblad']))
    {
      $err_msg = "clubblad";
    }
    else if (!isset($_REQUEST['opmerking']))
    {
      $err_msg = "opmerking";
    }
    if ($err_msg != '')
    {
      mail("freek.ceymeulen@pandora.be", "Application error", "Error : Parameter ".$err_msg." not set in bewerken_spelers.<br>User ".$_SESSION['usid']." on page ".$_SERVER['PHP_SELF']." from IP ".$_SERVER['REMOTE_ADDR']." using method ".$_SERVER['REQUEST_METHOD']." on url ".$_SERVER['REQUEST_URI'], "From: webmaster@badmintonsport.be");
    }
    if (isset($_REQUEST["administratief_lid"]))
    {
      $administratief_lid = "J";
    }
    else
    {
      $administratief_lid = "N";
    }
    require_once "dml/bad_spelers.php";
    $err_msg .= update_speler($conn, $spelers_id, $_REQUEST['achternaam'], $_REQUEST['voornaam'], $_REQUEST['geb_dt'], $_REQUEST['klassement'], $_REQUEST['geslacht'], $_REQUEST['lidnr'], $_REQUEST['type'], $_REQUEST['adres'], $_REQUEST['postcode'], $_REQUEST['gemeente'], $_REQUEST['email'], $_REQUEST['tel'], $_REQUEST['gsm'], $_REQUEST['club_dt'], $_REQUEST['eind_dt'], $_REQUEST['maat'], $_REQUEST['competitie'], $_REQUEST['betaald'], $_REQUEST['lidgeld'], $_REQUEST['clubblad'], $_REQUEST['opmerking'], $administratief_lid);

    if ($_FILES['pasfoto']['size'] != 0)
    {
      // Pasfoto toevoegen of wijzigen
      require_once "dml/pasfotos.php";
      $err_msg .= insert_pasfoto($conn, $spelers_id);
    }
    return $err_msg;
  }


  function bewerken_competitie($conn, $spelers_id)
  {
    // Verwijder alle records uit LEDEN_COMPETITIE voor de gegeven speler
    require_once "dml/leden_competitie.php";
    delete_leden_competitie($conn, $spelers_id, "spelers_id");

    // Voeg deze speler toe in tabel leden_competitie voor alle aangevinkte ploegen
    if ($_REQUEST['geslacht'] == 'M')
    {
      $reeks = "HEREN";
    }
    else
    {
      $reeks = "DAMES";
    }
    $query = "SELECT id, type FROM bad_competitieploegen WHERE type = 'GEMENGD' OR type = '%s'";
    $sql  = sprintf($query, mysql_real_escape_string($reeks));
    $result = mysql_query($sql, $conn) or die("Invalid query: " . mysql_error());

    $err_msg = "";

    while ($row = mysql_fetch_object($result))
    {
      // Alleen checkboxen die aangevinkt zijn worden gepost
      $titularis = "titularis".$row->id;
      $reserve = "reserve".$row->id;
      $basis = "basis".$row->id;

      $do_insert = 0;
      if (isset($_REQUEST[$titularis]))
      {
        $type = "'TITULARIS'";
        $do_insert = 1;
      }
      else if (isset($_REQUEST[$reserve]))
      {
        $type = "'RESERVE'";
        $do_insert = 1;
      }
      else
      {
        $type = "NULL";
      }
      if (isset($_REQUEST[$basis]))
      {
        $basis = "Y";
        $do_insert = 1;
      }
      else
      {
        $basis = "N";
      }
      if ($do_insert == 1)
      {
        $err_msg .= insert_leden_competitie($conn, $spelers_id, $row->id, $type, $basis, $row->type, $_REQUEST['seizoen']);
      }
    }
    mysql_free_result($result);
    return ($err_msg == "") ? "Info competitie gewijzigd. " : $err_msg;
  }


  function bewerken_wedstrijden($conn, $ploeg_id)
  {
    require_once "dml/competitiematchen.php";

    // Query het id van alle matchen van deze ploeg
    $query = "SELECT id FROM competitiematchen WHERE competitieploegen_id = %d";
    $sql  = sprintf($query, mysql_real_escape_string($ploeg_id));
    $result = mysql_query($sql, $conn) or die("Invalid query: " . mysql_error());

    while ($row = mysql_fetch_object($result))
    {
      // Build the field names
      $datum = "datum".$row->id;
      $uur = "uur".$row->id;
      $ontmoeting = "ontmoeting".$row->id;
      $score = "score".$row->id;

      if (strlen($_REQUEST[$datum]) == 0 && strlen($_REQUEST[$uur]) == 0 && strlen($_REQUEST[$ontmoeting]) == 0 && strlen($_REQUEST[$score]) == 0)
      {
        // Verwijder de beschikbaarheden voor deze match
        require_once "dml/beschikbaarheden.php";
        delete_beschikbaarheid($conn, $row->id);
        // Verwijder de ontmoeting
        delete_competitiematch($conn, $row->id);
      }
      else
      {
        // Wijzig de ontmoeting
        update_competitiematch($conn, $row->id, $_REQUEST[$datum], $_REQUEST[$uur], $_REQUEST[$ontmoeting], $_REQUEST[$score]);
      }
    }
  }


  function bewerken_beschikbaarheden($conn, $ploeg_id)
  {
    // 1. Delete all old availabilities for this team
    require_once "dml/beschikbaarheden.php";
    delete_beschikbaarheden($conn, $ploeg_id);

    // 2. Insert all new availabilities for this team
    $query = "SELECT spelers_id
                FROM leden_competitie
               WHERE competitieploegen_id = %d";
    $sql  = sprintf($query, mysql_real_escape_string($ploeg_id));
    $result = mysql_query($sql, $conn) or die("Invalid query: " . mysql_error());
    $spelers_ids = array();
    // Put all players of this team in an array
    while($row = mysql_fetch_row($result))
    {
      $spelers_ids[] = $row[0];
    }
    mysql_free_result($result);
    $query = "SELECT id
                FROM competitiematchen
               WHERE competitieploegen_id = %d";
    $sql  = sprintf($query, mysql_real_escape_string($ploeg_id));
    $result1 = mysql_query($sql, $conn) or die("Invalid query: " . mysql_error());

    while ($match = mysql_fetch_object($result1))
    // Loop over every match
    {
      for ($i = 0; $i < count($spelers_ids); $i++)
      // Loop over every player
      {
        $fieldname = 'b'.$match->id.'_'.$spelers_ids[$i];
        if (strlen($_REQUEST[$fieldname]) > 0)
        {
          insert_beschikbaarheden($conn, $match->id, $spelers_ids[$i], $_REQUEST[$fieldname]);
        }
      }
    }
    mysql_free_result($result1);
  }


  function bewerken_competitieploeg($conn, $ploeg_id, $ploegkapitein_id)
  {
    if (strlen($ploegkapitein_id) > 0)
    {
      // Ga na of er al een ploegkapitein bestond voor deze ploeg
      require_once "dml/leden_functies.php";
      if (functie_exists($conn, PLOEGKAPITEIN, $ploeg_id))
      {
        update_leden_functies($conn, $ploegkapitein_id, PLOEGKAPITEIN, $ploeg_id);
      }
      else
      {
        insert_leden_functies($conn, $ploegkapitein_id, PLOEGKAPITEIN, $ploeg_id);
      }
    }

    // Wijzig de competitieploeg
    require_once "dml/bad_competitieploegen.php";
    update_competitieploeg($conn, $ploeg_id, $_REQUEST['reeks'], $_REQUEST['sporthal'], $_REQUEST['uur']);
  }


  function bereken_lidgeld($conn, $spelers_id)
  {
    $query = "SELECT a.id
     , a.leeftijd_op_1_jan
     , a.statuut
     , a.inschrijving_dt
     , a.administratief_lid
     /*, CASE WHEN a.leeftijd_op_1_jan < 19 THEN 105 ELSE CASE WHEN a.statuut = 'R' THEN 91 ELSE 117 END END AS basisbedrag*/
     , CASE WHEN a.statuut = 'R' THEN 78 ELSE 98 END AS basisbedrag
     /*, CASE WHEN IFNULL(a.inschrijving_dt, STR_TO_DATE('01,01,2999', '%%d,%%m,%%Y')) < STR_TO_DATE(CONCAT('21,12,', YEAR( CURRENT_DATE ) - CASE MONTH( current_date ) WHEN 11 THEN 0 WHEN 12 THEN 0 ELSE 1 END),'%%d,%%m,%%Y') THEN 30 ELSE 0 END AS vroegboekkorting*/
     /*, CASE WHEN IFNULL(a.inschrijving_dt, STR_TO_DATE('01,01,2999', '%%d,%%m,%%Y')) < STR_TO_DATE(CONCAT('21,12,', YEAR(IFNULL(a.inschrijving_dt, STR_TO_DATE('01,01,1900', '%%d,%%m,%%Y'))) - CASE MONTH(IFNULL(a.inschrijving_dt, STR_TO_DATE('01,01,1900', '%%d,%%m,%%Y'))) WHEN 11 THEN 0 WHEN 12 THEN 0 ELSE 1 END),'%%d,%%m,%%Y') THEN 30 ELSE 0 END AS vroegboekkorting*/
     , CAST(0 AS UNSIGNED) AS vroegboekkorting
     /*, CASE WHEN MONTH(IFNULL(a.inschrijving_dt, STR_TO_DATE('01,07,2999', '%%d,%%m,%%Y'))) IN (11, 12, 1) THEN 0 ELSE -30 END AS boete*/
     , CAST(0 AS UNSIGNED) AS boete
     , CASE WHEN MONTH(IFNULL(a.inschrijving_dt, STR_TO_DATE('01,01,2999', '%%d,%%m,%%Y'))) IN (12, 1) THEN 12/12
            WHEN MONTH(IFNULL(a.inschrijving_dt, STR_TO_DATE('01,01,2999', '%%d,%%m,%%Y'))) IN (3, 4) THEN 10/12
            WHEN MONTH(IFNULL(a.inschrijving_dt, STR_TO_DATE('01,01,2999', '%%d,%%m,%%Y'))) IN (5, 6, 7, 8) THEN 8/12
            WHEN MONTH(IFNULL(a.inschrijving_dt, STR_TO_DATE('01,01,2999', '%%d,%%m,%%Y'))) IN (9, 10) THEN 4/12
            WHEN MONTH(IFNULL(a.inschrijving_dt, STR_TO_DATE('01,01,2999', '%%d,%%m,%%Y'))) IN (11) THEN 2/12
       END AS discount_pct
     , a.speelurenbedrag
  FROM ( SELECT s.id
              , s.type_speler AS statuut
              , s.inschrijving_dt
              , s.administratief_lid
              , YEAR( CURRENT_DATE ) + CASE MONTH( current_date ) WHEN 11 THEN 1 WHEN 12 THEN 1 ELSE 0 END - YEAR( geb_dt ) - (  '01-01' < RIGHT( geb_dt, 5 ) ) AS leeftijd_op_1_jan
              , SUM(u.prijs) AS speelurenbedrag
           FROM bad_spelers s
           LEFT OUTER JOIN leden_uren lu ON lu.spelers_id = s.id
           LEFT OUTER JOIN bad_speeluren u ON lu.speeluren_id = u.id
          WHERE s.id = %d
          GROUP by s.id
                 , s.type_speler
                 , s.inschrijving_dt
                 , s.administratief_lid
                 , YEAR( CURRENT_DATE ) + CASE MONTH( current_date ) WHEN 11 THEN 1 WHEN 12 THEN 1 ELSE 0 END - YEAR( geb_dt ) - (  '01-01' < RIGHT( geb_dt, 5 ) ) ) a";
    $sql  = sprintf($query, mysql_real_escape_string($spelers_id, $conn) );
    $result = mysql_query($sql, $conn) or badm_mysql_die();
    $row = mysql_fetch_object($result);
    if ($row->administratief_lid == "J")
    {
      $lidgeld = 40;
    }
    else
    {
      $lidgeld = ($row->basisbedrag + $row->speelurenbedrag - $row->vroegboekkorting + $row->boete) * $row->discount_pct;
    }
    mysql_free_result($result);

    $update_stmt = "UPDATE bad_spelers
                       SET lidgeld = %d
                     WHERE id = %d";
    $sql  = sprintf($update_stmt, $lidgeld
                                , $spelers_id);
    $result = mysql_query($sql, $conn) or die("Invalid query: " . mysql_error());
  }


  function bewerken_lid($conn, $spelers_id)
  {
    $err_msg = "";
    if (!isset($_REQUEST['update_speeluren']))
    {
      mail("freek.ceymeulen@pandora.be", "Application error", "Error : Parameter update_speeluren not set in bewerken_lid.<br>User ".$_SESSION['usid']." on page ".$_SERVER['PHP_SELF']." from IP ".$_SERVER['REMOTE_ADDR']." using method ".$_SERVER['REQUEST_METHOD']." on url ".$_SERVER['REQUEST_URI'], "From: webmaster@badmintonsport.be");
    }
    else if ($_REQUEST['update_speeluren'] == 1)
    {
      bewerken_speeluren($conn, $spelers_id);
      bereken_lidgeld($conn, $spelers_id);
      $err_msg .= "Speeluren gewijzigd ";
    }
    if (!isset($_REQUEST['update_spelers']))
    {
      mail("freek.ceymeulen@pandora.be", "Application error", "Error : Parameter update_spelers not set in bewerken_lid.<br>User ".$_SESSION['usid']." on page ".$_SERVER['PHP_SELF']." from IP ".$_SERVER['REMOTE_ADDR']." using method ".$_SERVER['REQUEST_METHOD']." on url ".$_SERVER['REQUEST_URI'], "From: webmaster@badmintonsport.be");
    }
    else if ($_REQUEST['update_spelers'] == 1)
    {
      $err_msg .= bewerken_spelers($conn, $spelers_id);
      bereken_lidgeld($conn, $spelers_id);
    }
    if (!isset($_REQUEST['update_competitie']))
    {
      mail("freek.ceymeulen@pandora.be", "Application error", "Error : Parameter update_competitie not set in bewerken_lid.<br>User ".$_SESSION['usid']." on page ".$_SERVER['PHP_SELF']." from IP ".$_SERVER['REMOTE_ADDR']." using method ".$_SERVER['REQUEST_METHOD']." on url ".$_SERVER['REQUEST_URI'], "From: webmaster@badmintonsport.be");
    }
    else if ($_REQUEST['update_competitie'] == 1)
    {
      $err_msg .= bewerken_competitie($conn, $spelers_id);
    }
    redirect($conn, BASE_URL."bewerken_lid.php?id=".$spelers_id."&err_msg=".urlencode($err_msg));
  }


  function bewerken_ploeg($conn, $ploeg_id)
  {
    if ($_REQUEST['update_wedstrijden'] == 1)
    {
      bewerken_wedstrijden($conn, $ploeg_id);
    }
    if (strlen($_REQUEST['ontmoeting0']) > 0)
    {
      require_once "dml/competitiematchen.php";
      insert_competitiematch($conn, $ploeg_id, $_REQUEST['datum0'], $_REQUEST['uur0'], $_REQUEST['ontmoeting0'], $_REQUEST['score0']);
    }
    if ($_REQUEST['update_beschikbaarheden'] == 1)
    {
      bewerken_beschikbaarheden($conn, $ploeg_id);
    }
    if ($_REQUEST['update_ploeg'] == 1)
    {
      bewerken_competitieploeg($conn, $ploeg_id, $_REQUEST['kapitein_id']);
    }
    redirect($conn, BASE_URL."bewerken_ploeg.php?id=".$ploeg_id."&tab=".$_REQUEST['tabblad']);
  }


  function verwijderen_ploeg($conn, $ploeg_id)
  {
    // Verwijder alle beschikbaarheden voor de competitiematchen van de gegeven competitieploeg
    require_once "dml/beschikbaarheden.php";
    delete_beschikbaarheden($conn, $ploeg_id);

    // Verwijder alle competitiematchen van de gegeven competitieploeg
    require_once "dml/competitiematchen.php";
    delete_competitiematchen($conn, $ploeg_id);

    // Verwijder alle records uit LEDEN_COMPETITIE van de gegeven competitieploeg
    require_once "dml/leden_competitie.php";
    delete_leden_competitie($conn, $ploeg_id, "competitieploegen_id");

    // Verwijder de ploegkapitein

    // Verwijder de competitieploeg
    require_once "dml/bad_competitieploegen.php";
    delete_competitieploeg($conn, $ploeg_id);

    redirect($conn, BASE_URL."competitie.php");
  }


  function bewerken_speeluur($conn)
  {
    $err_msg = "";

    if (strlen($_REQUEST['verantwoordelijke_id']) > 0)
    {
      // Dagverantwoordelijke toevoegen
      require_once "dml/leden_functies.php";
      insert_leden_functies($conn, $_REQUEST['verantwoordelijke_id'], DAGVERANTWOORDELIJKE, $_REQUEST['id']);
      $err_msg = "Dagverantwoordelijke toegevoegd.";
    }

    if (strlen($_REQUEST['trainer_id']) > 0)
    {
      // Trainer toevoegen
      require_once "dml/leden_functies.php";
      insert_leden_functies($conn, $_REQUEST['trainer_id'], TRAINER, $_REQUEST['id']);
      $err_msg = "Trainer toegevoegd.";
    }

    // Speeluur wijzigen
    require_once "dml/bad_speeluren.php";
    update_speeluur($conn, $_REQUEST['id'], $_REQUEST['sporthal'], $_REQUEST['dag'], $_REQUEST['uur'], $_REQUEST['unit'], $_REQUEST['doelgroep'], $_REQUEST['type'], $_REQUEST['beschrijving'], $_REQUEST['aantal_plaatsen'], $_REQUEST['aantal_reserve'], $_REQUEST['prijs']);
    $err_msg .= " Speeluur gewijzigd. ";

    if ($_REQUEST['delete_verantwoordelijke'] > 0)
    {
      // Dagverantwoordelijke verwijderen
      require_once "dml/leden_functies.php";
      delete_leden_functies($conn, $_REQUEST['delete_verantwoordelijke'], DAGVERANTWOORDELIJKE, $_REQUEST['id']);
      $err_msg .= "Dagverantwoordelijke verwijderd.";
    }

    if ($_REQUEST['delete_trainer'] > 0)
    {
      // Trainer verwijderen
      require_once "dml/leden_functies.php";
      delete_leden_functies($conn, $_REQUEST['delete_trainer'], TRAINER, $_REQUEST['id']);
      $err_msg .= "Trainer verwijderd";
    }
    redirect($conn, BASE_URL."speeluren.php?err_msg=".urlencode($err_msg));
  }


  function verwijderen_speeluur($conn)
  {
    // Verwijder alle records uit tabel LEDEN_UREN voor dit speeluur
    require_once "dml/leden_uren.php";
    delete_uren_leden($conn, $_REQUEST['id']);

    // Verwijder speeluur
    require_once "dml/bad_speeluren.php";
    delete_speeluur($conn, $_REQUEST['id']);

    if (strlen($_REQUEST['verantwoordelijke_id']) > 0)
    {
      // Verwijder dagverantwoordelijke
      require_once "dml/leden_functies.php";
      delete_leden_functies($conn, $_REQUEST['verantwoordelijke_id'], DAGVERANTWOORDELIJKE, $_REQUEST['id']);
    }
    redirect($conn, BASE_URL."speeluren.php?err_msg=".urlencode("Speeluur verwijderd"));
  }


  function nieuw_lid($conn)
  {
    global $new_id;
    // Lid toevoegen
    require_once "dml/bad_spelers.php";
    $err_msg = insert_speler($conn, $_REQUEST['naam'], $_REQUEST['voornaam'], $_REQUEST['geboren'], $_REQUEST['klassement'], $_REQUEST['geslacht'], $_REQUEST['lidnr'], $_REQUEST['type'], $_REQUEST['straat'], $_REQUEST['nr'], $_REQUEST['bus'], $_REQUEST['postcode'], $_REQUEST['gemeente'], $_REQUEST['email'], $_REQUEST['tel'], $_REQUEST['gsm'], $_REQUEST['club_dt'], $_REQUEST['eind_dt'], $_REQUEST['maat'], $_REQUEST['competitie'], $_REQUEST['betaald'], $_REQUEST['opmerking']);

    if ($err_msg != "")
    {
      redirect($conn, BASE_URL."nieuw_lid.php?voornaam=".$_REQUEST['voornaam']."&naam=".$_REQUEST['naam']."&geslacht=".$_REQUEST['geslacht']."&straat=".$_REQUEST['straat']."&nr=".$_REQUEST['nr']."&bus=".$_REQUEST['bus']."&postcode=".$_REQUEST['postcode']."&gemeente=".$_REQUEST['gemeente']."&geboren=".$_REQUEST['geboren']."&maat=".$_REQUEST['maat']."&email=".$_REQUEST['email']."&tel=".$_REQUEST['tel']."&gsm=".$_REQUEST['gsm']."&lidnr=".$_REQUEST['lidnr']."&klassement=".$_REQUEST['klassement']."&type_speler=".$_REQUEST['type_speler']."&club_dt=".$_REQUEST['club_dt']."&eind_dt=".$_REQUEST['eind_dt']."&betaald=".$_REQUEST['betaald']."&competitie=".$_REQUEST['competitie']."&opmerking=".$_REQUEST['opmerking']."&err_msg=".$err_msg);
    }
    redirect($conn, BASE_URL."bewerken_lid.php?id=".$new_id);
  }


  function nieuwe_ploeg($conn)
  {
    global $new_id;
    // Competitieploeg toevoegen, ploegkapitein toevoegen
    require_once "dml/bad_competitieploegen.php";
    $err_msg = insert_competitieploeg($conn, $_REQUEST['seizoen'], $_REQUEST['reeks'], $_REQUEST['ploeg'], $_REQUEST['afdeling'], $_REQUEST['sporthal'], $_REQUEST['uur']);

    if ($err_msg != "")
    {
      redirect($conn, BASE_URL."nieuwe_ploeg.php?reeks=".$_REQUEST['reeks']."&ploegnr=".$_REQUEST['ploeg']."&afdeling=".$_REQUEST['afdeling']."&kapitein=".$_REQUEST['kapitein']."&sporthal=".$_REQUEST['sporthal']."&uur=".$_REQUEST['uur']."&err_msg=".$err_msg);
    }

    if (strlen($_REQUEST['kapitein_id']) > 0)
    {
      // Ploegkapitein toevoegen in leden_functies
      require_once "dml/leden_functies.php";
      insert_leden_functies($conn, $_REQUEST['kapitein_id'], PLOEGKAPITEIN, $new_id);
    }


    redirect($conn, BASE_URL."competitie.php");
  }


  function nieuw_uur($conn)
  {
    // Uur toevoegen, geeft id van nieuw record terug
    require_once "dml/bad_speeluren.php";
    $uur_id = insert_speeluur($conn, $_REQUEST['plaats'], $_REQUEST['dag'], $_REQUEST['uur'], $_REQUEST['unit'], $_REQUEST['doelgroep'], $_REQUEST['type'], $_REQUEST['beschrijving']);

    if (strlen($_POST['verantwoordelijke']) > 0)
    {
      // Dagverantwoordelijke toevoegen
      require_once "dml/leden_functies.php";
      insert_leden_functies($conn, $_REQUEST['verantwoordelijke_id'], DAGVERANTWOORDELIJKE, $uur_id);
    }

    if (strlen($_POST['trainer']) > 0)
    {
      // Trainer toevoegen
      require_once "dml/leden_functies.php";
      insert_leden_functies($conn, $_REQUEST['trainer_id'], TRAINER, $uur_id);
    }

    redirect($conn, BASE_URL."speeluren.php?err_msg=".urlencode("Speeluur toegevoegd"));
  }


  function desactiveer_leden($conn)
  {
    // Zoek de leden die niet betaald hebben
    $sql = "SELECT id, naam FROM bad_spelers WHERE ( eind_dt IS NULL OR eind_dt > NOW() ) AND betaald IS NULL";
    $result = mysql_query($sql, $conn) or die("Invalid query: " . mysql_error());
    $ids = "";
    $namen = "";
    while ($row = mysql_fetch_object($result))
    {
      $ids = $ids.", ".$row->id;
      $namen = $namen.", ".$row->naam;
    }
    $update_stmt = "UPDATE bad_spelers SET eind_dt = NULL WHERE id IN (".substr($ids, 2).")";

    // Sla deze bewerking op in audit tabel
    log_action($_SESSION['usid'], 'UPDATE', 0, $update_stmt, $conn);

    // Zet de leden die niet betaald hebben op non-actief
    $update_stmt = "UPDATE bad_spelers
                       SET eind_dt = NOW()
                     WHERE ( betaald IS NULL OR betaald = 0 )
                       AND ( eind_dt IS NULL OR eind_dt > NOW() )";
    $result = mysql_query($update_stmt, $conn) or die("Invalid query: " . mysql_error());

    $err_msg = "Volgende spelers werden op non-actief geplaatst: ".substr($namen, 2);

    redirect($conn, BASE_URL."leden.php?err_msg=".urlencode($err_msg));
  }



  function nieuw_stock_item($conn)
  {
    global $new_id;
    require_once "dml/stock_items.php";
    $err_msg = insert_stock_item($conn, $_REQUEST['name'], $_REQUEST['location'], $_REQUEST['quantity'], $_REQUEST['price'], $_REQUEST['description']);

    if ($err_msg == "")
    {
      // check if an image was submitted
      if (isset($_FILES['image']))
      {
        require_once "dml/images.php";
        $err_msg = insert_images($conn, $new_id, "STOCK");
      }
    }

    if ($err_msg == "")
    {
      redirect($conn, BASE_URL."stock_item.php?id=".$new_id."&err_msg=Item toegevoegd.");
    }
    else
    {
      redirect($conn, BASE_URL."stock_item.php?name=".$_REQUEST['name']."&location=".$_REQUEST['location']."&quantity=".$_REQUEST['quantity']."&price=".$_REQUEST['price']."&description=".$_REQUEST['description']."&err_msg=".$err_msg);
    }
  }


  function bewerken_stock_item($conn, $item_id)
  {
    require_once "dml/stock_items.php";
    $err_msg = update_stock_item($conn, $item_id, $_REQUEST['name'], $_REQUEST['location'], $_REQUEST['quantity'], $_REQUEST['price'], $_REQUEST['description']);

    if ($err_msg == "")
    {
      // check if an image was submitted
      if (isset($_FILES['image']))
      {
        require_once "dml/images.php";
        $err_msg = insert_images($conn, $item_id, "STOCK");
      }
    }

    if ($err_msg == "")
    {
      redirect($conn, BASE_URL."stock.php?err_msg=Item gewijzigd.");
    }
    else
    {
      redirect($conn, BASE_URL."stock_item.php?id=".$_REQUEST['id']."&err_msg=".$err_msg);
    }
  }


  function verwijderen_stock_item($conn, $item_id)
  {
    // Eerst alle foto's verwijderen
    $query = "SELECT id FROM images WHERE ref_id = %d";
    $sql  = sprintf($query, mysql_real_escape_string($item_id));
    $result = mysql_query($sql, $conn) or die("Invalid query: " . mysql_error());
    while ($row = mysql_fetch_object($result))
    {
      require_once "dml/images.php";
      delete_image($conn, $row->id);
    }
    // Dan het item zelf verwijderen
    require_once "dml/stock_items.php";
    delete_stock_item($conn, $item_id);
    redirect($conn, BASE_URL."stock.php?err_msg=Item verwijderd.");
  }


  function verwijderen_foto($conn, $image_id)
  {
    require_once "dml/images.php";
    delete_image($conn, $image_id);
    redirect($conn, BASE_URL."stock_item.php?id=".$_REQUEST['id']."&err_msg=Foto verwijderd.");
  }


  function nieuwe_instelling($conn)
  {
    require_once "dml/codes.php";
    insert_code($conn, $_REQUEST['domain'],$_REQUEST['value'],$_REQUEST['description']);
    redirect($conn, BASE_URL."instellingen.php?domain=".$_REQUEST['domain']);
  }

/*--------------------------------------------------------------------------------------------
 | BEGIN
 --------------------------------------------------------------------------------------------*/
  if (isset($_POST['page']))
  {
    $page = $_POST['page'];
//echo "page = ".$page;
  }
  else
  {
    echo '<html><body>Security violation, admin has been alerted.</body></html>';
    mail("freek.ceymeulen@pandora.be", "Possible breakin attempt", "Error : page not set<br>User ".$_SESSION['usid']." on page ".$_SERVER['PHP_SELF']." from IP ".$_SERVER['REMOTE_ADDR']." using method ".$_SERVER['REQUEST_METHOD']." on url ".$_SERVER['REQUEST_URI'], "From: webmaster@badmintonsport.be");
    mysql_close($badm_db);
    exit;
  }

  switch (strtoupper($page))
  {
    case 'BEWERKEN_LID.PHP':
       switch ($_POST['command'])
       {
         case 'update':
            if (!isset($_POST['spelers_id']))
            {
              mail("freek.ceymeulen@pandora.be", "Application error", "Error : Spelers Id is not set in bewerken_lid<br>User ".$_SESSION['usid']." on page ".$_SERVER['PHP_SELF']." from IP ".$_SERVER['REMOTE_ADDR']." using method ".$_SERVER['REQUEST_METHOD']." on url ".$_SERVER['REQUEST_URI'], "From: webmaster@badmintonsport.be");
            }
            bewerken_lid($badm_db, $_POST['spelers_id']);
            break;
         default:
            mail("freek.ceymeulen@pandora.be", "Application error", "Error : No command ".$_POST['command']." recognized in bewerken_lid<br>User ".$_SESSION['usid']." on page ".$_SERVER['PHP_SELF']." from IP ".$_SERVER['REMOTE_ADDR']." using method ".$_SERVER['REQUEST_METHOD']." on url ".$_SERVER['REQUEST_URI'], "From: webmaster@badmintonsport.be");
       }
       break;
    case 'BEWERKEN_PLOEG.PHP':
       switch ($_POST['command'])
       {
         case 'update':
            bewerken_ploeg($badm_db, $_POST['id']);
            break;
         case 'delete':
            verwijderen_ploeg($badm_db, $_POST['id']);
            break;
         default:
            mail("freek.ceymeulen@pandora.be", "Application error", "Error : No command ".$_POST['command']." recognized in bewerken_ploeg<br>User ".$_SESSION['usid']." on page ".$_SERVER['PHP_SELF']." from IP ".$_SERVER['REMOTE_ADDR']." using method ".$_SERVER['REQUEST_METHOD']." on url ".$_SERVER['REQUEST_URI'], "From: webmaster@badmintonsport.be");
       }
       break;
    case 'BEWERKEN_SPEELUUR.PHP':
       switch ($_POST['command'])
       {
         case 'update':
            bewerken_speeluur($badm_db);
            break;
         case 'delete':
            verwijderen_speeluur($badm_db);
            break;
         default:
            mail("freek.ceymeulen@pandora.be", "Application error", "Error : No command ".$_POST['command']." recognized in bewerken_speeluur<br>User ".$_SESSION['usid']." on page ".$_SERVER['PHP_SELF']." from IP ".$_SERVER['REMOTE_ADDR']." using method ".$_SERVER['REQUEST_METHOD']." on url ".$_SERVER['REQUEST_URI'], "From: webmaster@badmintonsport.be");
       }
       break;
    case 'NIEUW_LID.PHP':
       nieuw_lid($badm_db);
       break;
    case 'NIEUWE_PLOEG.PHP':
       nieuwe_ploeg($badm_db);
       break;
    case 'NIEUW_UUR.PHP':
       nieuw_uur($badm_db);
       break;
    case 'LEDEN.PHP':
       switch ($_POST['command'])
       {
         case 'disactivate':
            desactiveer_leden($badm_db);
            break;
         default:
            mail("freek.ceymeulen@pandora.be", "Application error", "Error : No command ".$_POST['command']." recognized in leden<br>User ".$_SESSION['usid']." on page ".$_SERVER['PHP_SELF']." from IP ".$_SERVER['REMOTE_ADDR']." using method ".$_SERVER['REQUEST_METHOD']." on url ".$_SERVER['REQUEST_URI'], "From: webmaster@badmintonsport.be");
       }
       break;
    case 'STOCK_ITEM.PHP':
       switch ($_POST['command'])
       {
         case 'update':
            bewerken_stock_item($badm_db, $_POST['id']);
            break;
         case 'insert':
            nieuw_stock_item($badm_db);
            break;
         case 'delete':
            verwijderen_stock_item($badm_db, $_POST['id']);
            break;
         case 'delete_photo':
            verwijderen_foto($badm_db, $_POST['photo_id']);
            break;
         default:
            mail("freek.ceymeulen@pandora.be", "Application error", "Error : No command ".$_POST['command']." recognized in stock_item<br>User ".$_SESSION['usid']." on page ".$_SERVER['PHP_SELF']." from IP ".$_SERVER['REMOTE_ADDR']." using method ".$_SERVER['REQUEST_METHOD']." on url ".$_SERVER['REQUEST_URI'], "From: webmaster@badmintonsport.be");
       }
       break;
    case 'INSTELLINGEN.PHP':
       nieuwe_instelling($badm_db);
       break;
    default:
       mail("freek.ceymeulen@pandora.be", "Application error", "Error : Page ".$page." not recognized in dml.php<br>User ".$_SESSION['usid']." on page ".$_SERVER['PHP_SELF']." from IP ".$_SERVER['REMOTE_ADDR']." using method ".$_SERVER['REQUEST_METHOD']." on url ".$_SERVER['REQUEST_URI'], "From: webmaster@badmintonsport.be");
       echo "Page not set.  Please inform the administrator.";
  }
  //mysql_close($badm_db);
?>