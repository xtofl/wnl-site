<?php
/**
 * leden_excel.css
 * 
 * object    : creates an Excel file and forces a download of this file
 * author    : Freek Ceymeulen
 * created   : 01/05/2005
 **/

  // Connect to the DB (must be done before calling sess.php)
  require_once "../functies/badm_db.inc.php";
  $badm_db = badm_conn_db();
  require_once('../functies/sess.php');

  if ($_SESSION['auth'] != true)
  {
    // niet ingelogd -> ga naar login pagina
    //session_write_close;
    mysql_close($badm_db);
    header("Location: http://www.badmintonsport.be/admin/login.php");
    exit;
  }
/*
--------------------------------------------------------------------------------------------
|| FUNCTIONS
--------------------------------------------------------------------------------------------
*/
function ForceFileDownload($file)
{ 
  $filesize = @filesize($file);
  header("Content-Description: File Transfer");
  header("Content-Length: " . $filesize);
  header("Content-Disposition: attachment; filename=".basename($file)); 
  header("Content-Type: application/octet-stream"); 
  header("Content-Type: application/force-download"); 
  header("Content-Type: application/download"); 
  header("Content-Transfer-Encoding: binary");
  header("Cache-Control: no-cache, must-revalidate, post-check=0, pre-check=0");
  header("Pragma:no-cache"); 
  header("Expires:0");
  // Set the number of seconds the script is allowed to run
  @set_time_limit(600); 
  readfile($file); 
}
/*
--------------------------------------------------------------------------------------------
|| BEGIN
--------------------------------------------------------------------------------------------
*/
  $sep = "\t";
  
  if (isset($_POST['ids']))
  {
    $query = "SELECT SUBSTRING(s.naam, LOCATE(' ', s.naam) + 1, LENGTH(s.naam) - LOCATE(' ', s.naam))  AS Naam
                   , SUBSTRING(s.naam, 1, LOCATE(' ', s.naam))  AS Voornaam
                   , DATE_FORMAT(s.geb_dt, '%%d/%%m/%%Y') AS Geboortedatum
                   , (TO_DAYS(NOW()) - TO_DAYS(s.geb_dt)) / 365 AS Leeftijd
                   , s.geslacht
                   , s.klassement
                   , s.type_speler
                   , s.lidnr
                   , s.adres
                   , s.postcode
                   , s.woonplaats
                   , s.tel
                   , s.gsm
                   , s.email
                   , s.geb_plaats
                   , s.lengte
                   , s.gewicht
                   , s.beroep
                   , s.handig
                   , s.start_dt
                   , s.club_dt
                   , s.eind_dt
                   , s.racket
                   , s.website
                   , s.maat
                   , s.clubblad
                   , c.naam AS Club
                   , s.remark
               FROM bad_spelers s
               LEFT OUTER JOIN  bad_clubs c ON s.clubs_id = c.id
              WHERE s.id IN (%s)
              ORDER BY 1, 2";
    $sql  = sprintf($query, stripslashes($_POST['ids']));
    $result = mysql_query($sql, $badm_db) or badm_mysql_die();
    if (mysql_num_rows($result) == 0)
    {
      echo "No data found";
    }
    else
    {
      $file = "leden.xls";    
      if (!$file_handle = fopen($file, "w"))
      {
        echo "<br>Kan bestand ($file) niet openen/aanmaken";
      }
      $data = "Naam".$sep."Voornaam".$sep."Geboortedatum".$sep."Leeftijd".$sep."Geslacht".$sep."Klassement".$sep.
              "Type Speler".$sep."Lidnummer".$sep."Adres".$sep."Postcode".$sep."Woonplaats".$sep."Tel".$sep.
              "Gsm".$sep."E-mail".$sep."Geboorteplaats".$sep."Lengte".$sep."Gewicht".$sep."Beroep".$sep."Handig".$sep.
              "Start Datum".$sep."Club Datum".$sep."Stop Datum".$sep."Racket".$sep."Website".$sep."Maat T-shirt".$sep.
              "Boekske".$sep."Club".$sep."Opmerking\n";
      if (!fwrite($file_handle, $data))
      {
        echo "<br/>Kan niet schrijven naar bestand ($file)";
      }
      
      while ($row = mysql_fetch_array($result, MYSQL_NUM))
      {
        $data = "";
        for ($i=0; $i < count($row); $i++)
        {
          if ($i == count($row) - 1)
          {
            $data .= $row[$i]."\n";
          }
          else
          {
            $data .= $row[$i].$sep;
          }
        }
        if (!fwrite($file_handle, $data))
        {
          echo "<br/>Kan niet schrijven naar bestand ($file)";
        }
      }
      //echo "<br>Export met succes gemaakt naar $file";    
      fclose($file_handle);
   }
   ForceFileDownload($file);
 }
  //mysql_close($badm_db);
?>