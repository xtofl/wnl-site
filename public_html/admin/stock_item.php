<?php
/**
 * stock_item.php
 *
 * author     : Freek Ceymeulen
 * created    : 29/05/2006
 * parameters : (optional) id      : only when updating, inserting otherwise
 *              (optional) err_msg
 *              (optional) name        : when returning with error from insert
 *              (optional) location    : when returning with error from insert
 *              (optional) quantity    : when returning with error from insert
 *              (optional) price       : when returning with error from insert
 *              (optional) description : when returning with error from insert
 **/

  // Connect to the DB (must be done before calling sess.php)
  require_once "../functies/badm_db.inc.php";
  $badm_db = badm_conn_db();
  require_once('../functies/sess.php');

  if ($_SESSION['auth'] != true)
  {
    // niet ingelogd -> ga naar login pagina
    // geef deze pagina als parameter mee zodat na succesvolle login teruggekeerd kan worden naar deze pagina
    //session_write_close;
    mysql_close($badm_db);
    header("Location: http://www.badmintonsport.be/admin/login.php?ref=".basename($PHP_SELF));
    exit;
  }

  if (isset($_REQUEST['id']))
  {
    $id = $_REQUEST['id'];
    if (!is_numeric($id))
    // Check to avoid people passing sql statements in the url
    {
      echo "Security violation, admin has been alerted.";
      mail("freek.ceymeulen@pandora.be", "Possible breakin attempt<br>User ".$_SESSION['usid']." On page ".$_SERVER['PHP_SELF']." from IP ".$_SERVER['REMOTE_ADDR']." using method ".$_SERVER['REQUEST_METHOD']." on url ".$_SERVER['REQUEST_URI'], "From: webmaster@badmintonsport.be");
      mysql_close($badm_db);
      exit;
    }
  }
  else
  {
    $id = null;
    $name        = $_REQUEST['name'];
    $location    = $_REQUEST['location'];
    $quantity    = $_REQUEST['quantity'];
    $price       = $_REQUEST['price'];
    $description = $_REQUEST['description'];
  }

  if (isset($_REQUEST['err_msg']))
  {
    $err_msg = $_REQUEST['err_msg'];
  }
  else
  {
    $err_msg = "";
  }

/*
--------------------------------------------------------------------------------------------
|| CONSTANTS
--------------------------------------------------------------------------------------------
*/

  define("NUM_IMAGES", 4);  // aantal foto's per item

/*
--------------------------------------------------------------------------------------------
|| FUNCTIONS
--------------------------------------------------------------------------------------------
*/

  require_once("../functies/general_functions.php");

/*
--------------------------------------------------------------------------------------------
|| BEGIN
--------------------------------------------------------------------------------------------
*/
?>

<html>
<head>
<title>W&amp;L Admin Module - Stock Item</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../css/admin.css" rel="stylesheet" type="text/css">
<script language="JavaScript" type="text/JavaScript">
<!--
function deletePhoto(p_id)
{
  document.forms[0].command.value = 'delete_photo';
  document.forms[0].photo_id.value = p_id;
  document.forms[0].action = 'dmlNEW.php';
  document.forms[0].submit();
}


function doSubmit(p_action)
{
  if (p_action == 'save')
  {
    // Controleer of naam is ingevuld
    if (document.forms[0].name.value.length == 0)
    {
      alert('Je bent vergeten een naam in te vullen!');
      document.forms[0].name.focus();
      return false;
    }
    document.forms[0].command.value = '<?php echo (is_null($id)) ? "insert" : "update"; ?>';
    document.forms[0].action = 'dmlNEW.php';
  }
  else if (p_action == 'cancel')
  {
    window.location.href='stock.php';
    return false;
  }
  else if (p_action == 'delete')
  {
    if (confirm('Ben je zeker dat je dit item wil verwijderen?'))
    {
      document.forms[0].command.value = 'delete';
      document.forms[0].action = 'dmlNEW.php';
    }
    else
    {
      return false;
    }
  }
  document.forms[0].submit();
  return true;
}
// -->
</script>
</head>
<body bgcolor="#3A6EA5" link="#000000" vlink="#000000" topmargin="0">

<span style="font-size: 8pt; float: right"><?php echo $_SESSION['username']; ?></span>
<?php
  // Build current date and time string
  list($wday,$mday,$month,$year,$hour,$minutes) = split("( )",date("w j n Y H i",time()));
  $weekday = array('zondag', 'maandag', 'dinsdag', 'woensdag', 'donderdag', 'vrijdag', 'zaterdag');
  $months  = array('januari','februari','maart','april','mei','juni','juli','augustus','september','oktober','november','december');
  $newdate = "$weekday[$wday], $mday ".$months[$month-1]." $year $hour:$minutes";
?>
<span style="font-size: 8pt"><?php echo $newdate; ?></span>

<table border="1" cellspacing="0" cellpadding="0" align="center" width="810">

  <tr bgcolor="#C6C3C6" valign="top"><td>
    <table width="100%" border="0" cellspacing="0" cellpadding="1" align="center">
      <tr bgcolor="#400080">
        <td height="20" class="title">&nbsp;W&amp;L - Stock Item</td>
        <td height="20" align="right" bgcolor="#000084">
          <a href="http://www.badmintonsport.be/docs/W_L_admin_users_guide.doc" target="_blank"><img src="../poll/image/help.gif" width="16" height="14" border="0" alt="Gebruikershandleiding"></a><a href="login.php?action=logout"><img src="../poll/image/cross.gif" width="16" height="14" border="0" alt="Uitloggen"></a></td>
      </tr>
    </table>

    <table border="0" cellspacing="0" cellpadding="0">
      <tr valign="top">
        <td width="100">
<?php
  // Print the menu
  write_menu($_SERVER['PHP_SELF']);
?>
        </td>

        <td style="padding-left: 10px;">
<?php
  if (strlen($err_msg) > 0)
  {
    echo "<font color=\"red\"><b>".$err_msg."</b></font>";
  }
?>
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <form name="stock_items" action="dmlNEW.php" enctype="multipart/form-data" method="POST">
            <tr>
              <td style="background-color:black;">
                <table border="0" cellspacing="0" cellpadding="5">
<?php
  $idarr = array(); // Array om de id's van de foto's bij te houden
  if (!is_null($id))
  {
    // Selecteer de foto's van dit item
    $query1 = "SELECT i.id
                    , i.name
                 FROM images i
              WHERE i.category = \"STOCK\"
                AND i.ref_id = %d
           ORDER BY dt_wijz";
    $sql  = sprintf($query1, mysql_real_escape_string($id));
    $result = mysql_query($sql, $badm_db) or badm_mysql_die();
    // Toon de foto's
    while ($row = mysql_fetch_object($result))
    {
      echo "<tr>\n";
      echo "<td align=\"center\"><a href=\"display_image.php?id=".$row->id."\" target=\"_blank\"><img src=\"display_image.php?id=".$row->id."\" height=\"100\" alt=\"".$row->name."\"></a></td>\n";
      echo "</tr>\n";
      // Vul een array met de image id's, nodig om ze te kunnen verwijderen later
      $idarr[] = $row->id;
    }
  }
  else
  {
    echo "<tr><td>&nbsp;</td></tr>\n";
  }
?>
                </table>

              </td>
              <td style="padding-left: 10px; padding-top:10px; vertical-align:top;">
                <table border="0" cellspacing="0" cellpadding="3">
<?php
  if (!is_null($id))
  {
    $query1 = "SELECT si.id
                    , si.name
                    , si.location
                    , si.quantity
                    , si.price
                    , si.description
                 FROM stock_items si
              WHERE id = %d
           ORDER BY dt_wijz";
    $sql  = sprintf($query1, mysql_real_escape_string($id));
    $result = mysql_query($sql, $badm_db) or badm_mysql_die();
    $row = mysql_fetch_object($result);
    $name        = $row->name;
    $location    = $row->location;
    $quantity    = $row->quantity;
    $price       = $row->price;
    $description = $row->description;
  }
?>
                  <tr>
                    <td>Item</td><td><input type="text" name="name" size="34" maxlength="64" value="<?php echo $name; ?>"></td>
                  </tr>
                  <tr>
                    <td>Lokatie</td><td><input type="text" name="location" size="34" maxlength="64" value="<?php echo $location; ?>"></td>
                  </tr>
                  <tr>
                    <td>Hoeveelheid</td><td><input type="text" name="quantity" size="6" maxlength="10" value="<?php echo $quantity; ?>"></td>
                  </tr>
                  <tr>
                    <td>Aankoopprijs</td><td><input type="text" name="price" size="6" maxlength="10" value="<?php echo $price; ?>"> �</td>
                  </tr>
                  <tr>
                    <td>Beschrijving</td><td><textarea name="description" cols="26" rows="4" maxlength="256"><?php echo $description; ?></textarea><input type="hidden" name="MAX_FILE_SIZE" value="15000000"></td>
                  </tr>
<?php
  for ($i=0; $i < count($idarr); $i++)
  {
    // Toon link om foto te verwijderen
    echo "<tr>\n";
    echo "<td><a href=\"javascript:deletePhoto(".$idarr[$i].");\">Verwijder foto ".($i+1)."</a></td>\n";
    echo "</tr>\n";
  }
  for ($i=count($idarr); $i < NUM_IMAGES; $i++)
  {
    // Toon browse file item om foto toe te voegen
    echo "<tr>\n";
    echo "<td>Foto ".($i+1)."</td><td><input type=\"file\" name=\"image[]\"></td>\n";
    echo "</tr>\n";
  }
?>
                </table>

              </td>
            </tr>
            <tr>
              <td colspan="2" class="td2"><hr>
                <span style="float: right">
                  <input type="hidden" name="id" value="<?php echo $id; ?>">
                  <input type="hidden" name="photo_id" value="">
                  <input type="hidden" name="page" value="stock_item.php">
                  <input type="hidden" name="command" value="query">
                  <button type="button" name="delete" accesskey="V" title="Verwijder dit item" onClick="return doSubmit('delete');">Item <span style="text-decoration: underline">V</span>erwijderen</button>
                  <button type="button" name="cancel" accesskey="C" title="Afsluiten zonder opslaan" onClick="return doSubmit('cancel');"><span style="text-decoration: underline">C</span>ancel</button>
                  <button type="button" name="save" accesskey="S" title="Wijzigingen Opslaan" onClick="return doSubmit('save');"><span style="text-decoration: underline">S</span>ave</button>
                </span>
              </td>
            </tr>
            </form>
          </table>
      </td></tr>

    </table>
  </td></tr>
</table>
<br>
<br>
<?php
  //mysql_close($badm_db);
?>
</body>
</html>