<?php
  require_once "../functies/website_usage.php";
  
   if ($_GET['n'] == 'y')
   {
      if (isset($nieuw))
         $array = explode("/", $_COOKIE["nieuw"]);
      $array[] = $_GET['id'];
      $nieuw = implode("/", $array);
      setcookie ("nieuw", $nieuw, time()+172800);  //2 dagen
   }
   $bericht_id = 0;
   $bericht_id = $_GET['id'];
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
  <title>W&amp;L badmintonvereniging vzw - Web Forum</title>
  <link href="http://www.badmintonsport.be/css/messageboard.css" rel="stylesheet" type="text/css">
</head>
<body>
  <div align="center" style="width: 95%; margin-left:auto; margin-right:auto;">
    <table border="0">
      <tr>
        <td valign="top">
        </td>
        <td>&nbsp;&nbsp;</td>
        <td valign="top" align="center">
          <div class="title">W&amp;L Web Forum</div>
          <div class="text" style="padding-top: 10px;"></div>
          <br />
          <br />
          <a href="http://www.badmintonsport.be" target="_self">Terug naar de W&amp;L website</a>
        </td>
      </tr>
    </table>
    <br />

<!-- NAVIGATIE KNOPPEN -->

    <table width="100%" border="0" cellpadding="0" cellspacing="5">
      <tr>

<?php

   require_once "../functies/badm_db.inc.php";
   $badm_db = badm_conn_db();

// create SQL statement om aantal blz te bepalen

   $query = "SELECT count(*) as aantal
               FROM gb_replies
              WHERE bericht_id = %d";

   $sql = sprintf($query, mysql_real_escape_string($bericht_id));
   $result = mysql_query ($sql,$badm_db) or badm_mysql_die();

// format results by row

      $topics = mysql_fetch_object($result);
      $total  = ceil($topics->aantal / 50);

?>

        <td>

<?php

     echo     "<div class=\"pagerbutton\" onmouseover=\"this.className='pagerbutton pagerbutton_over'\" onmouseout=\"this.className='pagerbutton'\" onclick=\"location.href='nieuw_bericht.php?m=u&id=".$bericht_id."'\">";

?>

           <img align="absmiddle" src="images/postmessage.gif" border="0">&nbsp;&nbsp;Beantwoorden
          </div>
        </td>
        <td>
          <div class="pagerbutton" onmouseover="this.className='pagerbutton pagerbutton_over'" onmouseout="this.className='pagerbutton'" onclick="location.href='messageboard.php?p=1&m=n&id=0'">
            <img align="absmiddle" src="images/home.gif" border="0">&nbsp;&nbsp;Forum
          </div>
        </td>
        <td>
        </td>
      </tr>
    </table>

<!-- Einde navigatieknoppen -->

    <table>
      <tr>
        <td colspan="3" align="left">

<?php

   $update = "UPDATE gb_berichten
                 SET bekeken = bekeken + 1
                   , datum   = datum
               WHERE id = %d";

   $sql = sprintf($update, mysql_real_escape_string($bericht_id));
   $result = mysql_query($sql, $badm_db) or die("Invalid query: " . mysql_error());

?>

          <span class="text">Pagina 1 van

<?php

   if ($topics->aantal != 0)
      echo $total;
   else
      echo "1";

?>

         (Totaal Aantal Berichten:

<?php

   echo $topics->aantal + 1;

?>

         )</span>
        </td>
      </tr>
    </table>
    <br />

<!-- ZOEK FORM -->
<!--
    <div style="text-align:center;">
      <form action="show.php" method="post" style="display:inline;">
        <input type="hidden" name="usernum"   value="307150424" />Search this Forum:
        <input type="text"   name="searchfor" value="" />         &nbsp;
        <input type="submit"                  value="Search" />
      </form>
    </div>
    <br />

-->

    <table width="100%" cellpadding="0" cellspacing="0" border="0" bgcolor="#dddddd">
      <tr>
        <td>
          <table width="100%" border="0" cellpadding="10" cellspacing="1">
            <tr class="tableb">
              <td width="220" style="font-weight: bold;">Auteur</td>
              <td style="font-weight: bold;">Bericht &nbsp; &nbsp;</td>
            </tr>

<?php

// create SQL statement voor hoofdbericht

   $query = "SELECT naam
                  , email
                  , onderwerp
                  , tekst
                  , DATE_FORMAT(datum, '%%d-%%m-%%Y - %%T') AS datum
                  , icoon
               FROM gb_berichten
              WHERE id = %d";

   $sql = sprintf($query, mysql_real_escape_string($bericht_id));
   $result = mysql_query($sql,$badm_db) or badm_mysql_die();

// format results by row

   $topic = mysql_fetch_object($result);

   echo "<tr class=\"tablea\"><td valign=\"top\" align=\"left\"><b>";
   echo stripslashes($topic->naam);
   echo "</b><br /><br /><div style=\"margin-top: 2px;\">";
   echo "<span style=\"cursor: pointer;\" onclick=\"javascript: document.getElementById('info191895').style.display = (document.getElementById('info191895').style.display == 'block') ? 'none':'block';\">";
   echo "<img border=\"0\" src=\"images/mail.gif\" alt=\"E-mail\"></span>";
//   echo "<span style=\"cursor: pointer;\" onclick=\"javascript: document.getElementById('info191895').style.display = (document.getElementById('info191895').style.display == 'block') ? 'none':'block';\">";
//   echo "<img align=\"absmiddle\" border=\"0\" src=\"images/im_3.gif\">";
//   echo "</span>";
//   echo "<span style=\"cursor: pointer;\" onclick=\"javascript: document.getElementById('info191895').style.display = (document.getElementById('info191895').style.display == 'block') ? 'none':'block';\">";
//   echo "<img border=\"0\" src=\"images/link.gif\" alt=\"\">";
//   echo "</span>";
   echo "</div><br><br>";
   echo $topic->datum;
   echo "</td><td valign=\"top\" align=\"left\"><b>";
   echo stripslashes($topic->onderwerp);
   echo "</b><div style=\"width:100%; border-bottom: 1px solid #dddddd; margin-bottom: 10px;\"></div>";
   echo stripslashes($topic->tekst);
   echo "<div id=\"info191895\" style=\"margin-top: 15px; display: none;\"><b>E-mail:</b>";
   if (strlen($topic->email) > 0)
   {
      // Ik bouw het e-mail adres op de client via Javascript, op die manier kunnen robots het niet herkennen en vermijden we spam.
      echo "<script type=\"text/javascript\" language=\"JavaScript\">\n";
      echo "<!--\n";
      echo " naam = '".htmlspecialchars(substr($topic->email, 0, strpos($topic->email, "@")))."';\n";
      echo " akrol = '@';\n";
      echo " domein = '".htmlspecialchars(substr($topic->email, strpos($topic->email, "@") + 1))."';\n";
      echo " document.write('<a href=\"mailto:'+naam+akrol+domein+'\">'+naam+akrol+domein+'</a>');\n";
      echo "-->\n";
      echo "</script>\n";
      echo "<noscript>\n";
      echo "Email adres afgeschermd mbv Javascript.<br>Laat Javascript toe voor contact.\n";
      echo "</noscript>\n";
   }
   echo "<br><br></div></td></tr>\n";

//---------------------------------------------------REPLIES--------------------------------------------------------------------------------

   $query2 = "SELECT id
                   , naam
                   , email
                   , onderwerp
                   , tekst
                   , icoon
                   , DATE_FORMAT(datum, '%%d-%%m-%%Y - %%T') AS datum
                FROM gb_replies
               WHERE bericht_id = %d";

   $sql2 = sprintf($query2, mysql_real_escape_string($bericht_id));
   $result2 = mysql_query ($sql2, $badm_db) or badm_mysql_die();

// format results by row

   while ($reply = mysql_fetch_object($result2))
   {
     echo "<tr class=\"tablea\"><td valign=\"top\" align=\"left\"><b>";
     echo stripslashes($reply->naam);
     echo "</b><br /><br /><div style=\"margin-top: 2px;\">";
     echo "<span style=\"cursor: pointer;\" onclick=\"javascript: document.getElementById('info".$reply->id."').style.display = (document.getElementById('info".$reply->id."').style.display == 'block') ? 'none':'block';\">";
     echo "<img border=\"0\" src=\"images/mail.gif\" alt=\"E-mail\"></span>";
//   echo "<span style=\"cursor: pointer;\" onclick=\"javascript: document.getElementById('info191895').style.display = (document.getElementById('info191895').style.display == 'block') ? 'none':'block';\">";
//   echo "<img align=\"absmiddle\" border=\"0\" src=\"images/im_3.gif\">";
//   echo "</span>";
//   echo "<span style=\"cursor: pointer;\" onclick=\"javascript: document.getElementById('info191895').style.display = (document.getElementById('info191895').style.display == 'block') ? 'none':'block';\">";
//   echo "<img border=\"0\" src=\"images/link.gif\" alt=\"\">";
//   echo "</span>";
     echo "</div><br><br>";
     echo $reply->datum;
     echo "</td><td valign=\"top\" align=\"left\">";
     if ($reply->icoon != null)
     {
       echo "<img align=\"absmiddle\" src=\"images/";
       echo $reply->icoon;
       echo ".gif\">";
     }
     echo "<b>";
     echo stripslashes($reply->onderwerp);
     echo "</b><div style=\"width:100%; border-bottom: 1px solid #dddddd; margin-bottom: 10px;\"></div>";
     echo stripslashes($reply->tekst);
     echo "<div id=\"info".$reply->id."\" style=\"margin-top: 15px; display: none;\"><b>E-mail:</b>";
     if (strlen($reply->email) > 0)
     {
        // Ik bouw het e-mail adres op de client via Javascript, op die manier kunnen robots het niet herkennen en vermijden we spam.
        echo "<script type=\"text/javascript\" language=\"JavaScript\">\n";
        echo "<!--\n";
        echo " naam = '".htmlspecialchars(substr($reply->email, 0, strpos($reply->email, "@")))."';\n";
        echo " akrol = '@';\n";
        echo " domein = '".htmlspecialchars(substr($reply->email, strpos($reply->email, "@") + 1))."';\n";
        echo " document.write('<a href=\"mailto:'+naam+akrol+domein+'\">'+naam+akrol+domein+'</a>');\n";
        echo "-->\n";
        echo "</script>\n";
        echo "<noscript>\n";
        echo "Email adres afgeschermd mbv Javascript.<br>Laat Javascript toe voor contact.\n";
        echo "</noscript>\n";
     }
     echo "<br><br></div></td></tr>\n";
   }
?>

            </table>
          </td>
        </tr>
      </table>
      <br>
      <br>
    </div>
  </div>
</body>
</html>
<?php
  log_website_usage($start_time, $bericht_id, $badm_db);
  mysql_close($badm_db);
?>