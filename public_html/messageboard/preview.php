<?php
  require_once "../functies/website_usage.php";

  $subject = $_POST["onderwerp"];
  if (strlen(trim($subject)) == 0)
  {
    $subject = "<font color=\"red\">No subject!</font>";
  }

  // message icons verwerken
    if ($_POST["nofunimage"] == 1)
    {
      $smiley = "";
    }
    else
    {
      switch ($_POST["smiley"])
      {
         case 1:
           $smiley = "set11";
           break;
         case 2:
           $smiley = "set12";
           break;
         case 3:
           $smiley = "set13";
           break;
         case 4:
           $smiley = "set14";
           break;
         case 5:
           $smiley = "set15";
           break;
         case 6:
           $smiley = "set16";
           break;
         case 7:
           $smiley = "set17";
           break;
         case 8:
           $smiley = "set18";
           break;
         case 9:
           $smiley = "set19";
           break;
         case 10:
           $smiley = "set110";
           break;
      }
      $smiley = "<img src=\"images/$smiley.gif\" align=\"middle\">";
    }
  
   $message = nl2br($_POST["tekst"]);

   // bericht verwerken

   $message = str_replace("[o:)]", "<img src=\"images/30_angel.gif\" border=\"0\" alt=\"\">", $message);
   $message = str_replace("[:)s]", "<img src=\"images/28_spin.gif\" border=\"0\" alt=\"\">", $message);
   $message = str_replace("[:))]", "<img src=\"images/27_laughing.gif\" border=\"0\" alt=\"\">", $message);
   $message = str_replace("[:)]" , "<img src=\"images/30_angel.gif\" border=\"0\" alt=\"\">", $message);
   $message = str_replace("[:-)]", "<img src=\"images/12_smile.gif\" border=\"0\" alt=\"\">", $message);
   $message = str_replace("[:D]" , "<img src=\"images/1_grin.gif\" border=\"0\" alt=\"\">", $message);
   $message = str_replace("[:-D]", "<img src=\"images/13_grin.gif\" border=\"0\" alt=\"\">", $message);
   $message = str_replace("[:|]" , "<img src=\"images/8_grim.gif\" border=\"0\" alt=\"\">", $message);
   $message = str_replace("[:-|]", "<img src=\"images/19_indifferent.gif\" border=\"0\" alt=\"\">", $message);
   $message = str_replace("[8)]" , "<img src=\"images/9_cool.gif\" border=\"0\" alt=\"\">", $message);
   $message = str_replace("[8-)]", "<img src=\"images/25_coolguy.gif\" border=\"0\" alt=\"\">", $message);
   $message = str_replace("[:-?]", "<img src=\"images/11_confused.gif\" border=\"0\" alt=\"\">", $message);
   $message = str_replace("[Bouncy]", "<img src=\"images/bouncy.gif\" border=\"0\" alt=\"\">", $message);
   $message = str_replace("[Headbang]", "<img src=\"images/headbang.gif\" border=\"0\" alt=\"\">", $message);
   $message = str_replace("[Eeek]", "<img src=\"images/22_yikes.gif\" border=\"0\" alt=\"\">", $message);
   $message = str_replace("[Crying]", "<img src=\"images/26_crying.gif\" border=\"0\" alt=\"\">", $message);
   $message = str_replace("[Irked]", "<img src=\"images/21_irked.gif\" border=\"0\" alt=\"\">", $message);
   $message = str_replace("[Oh]", "<img src=\"images/3_oh.gif\" border=\"0\" alt=\"\">", $message);
   $message = str_replace("[Sing]", "<img src=\"images/23_sing.gif\" border=\"0\" alt=\"\">", $message);
   $message = str_replace("[Party]", "<img src=\"images/party.gif\" border=\"0\" alt=\"\">", $message);
   $message = str_replace("[Angry]", "<img src=\"images/16_angry.gif\" border=\"0\" alt=\"\">", $message);
   $message = str_replace("[Propeller]", "<img src=\"images/propeller.gif\" border=\"0\" alt=\"\">", $message);
   $message = str_replace("[Kissing]", "<img src=\"images/kissing.gif\" border=\"0\" alt=\"\">", $message);
   $message = str_replace("[Badminton]", "<img src=\"images/badminton.gif\" border=\"0\" alt=\"\">", $message);
   $message = str_replace("[Shocked]", "<img src=\"images/24_shocked.gif\" border=\"0\" alt=\"\">", $message);
   $message = str_replace("[Wink]", "<img src=\"images/17_wink.gif\" border=\"0\" alt=\"\">", $message);
   $message = str_replace("[Bouncer]", "<img src=\"images/bouncer.gif\" border=\"0\" alt=\"\">", $message);
   $message = str_replace("[Denmark]", "<img src=\"images/denmark.gif\" border=\"0\" alt=\"\">", $message);
   $message = str_replace("  ", "&nbsp;", $message);
   $message = str_replace("<?", "&lt;?", $message);
   $message = str_replace("?>", "?&gt;", $message);
  
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
  <title>W&amp;L badmintonvereniging vzw - Web Forum - Preview</title>
  <link href="http://www.badmintonsport.be/css/messageboard.css" rel="stylesheet" type="text/css">
</head>

<body>
<table width="100%" border="0" cellpadding="10" cellspacing="1">
  <tr class="tableb">
    <td style="font-weight: bold;" width="220"> Auteur</td>
    <td style="font-weight: bold;">Bericht&nbsp; &nbsp;</td>
  </tr>
  <tr class="tablea">
    <td align="left" valign="top">
      <b><?=$_POST["naam"]?></b><br><br>
      <div style="margin-top: 2px;">
        <span style="cursor: pointer;" onclick="javascript: document.getElementById('info191895').style.display = (document.getElementById('info191895').style.display == 'block') ? 'none':'block';">
          <img src="images/mail.gif" alt="<?=$_POST["email"]?>" border="0">
        </span>
      </div><br><br>
      <?=date("m-d-Y - H:i")?>
    </td>
    <td align="left" valign="top"><?=$smiley?><b><?=$subject?></b>
      <div style="border-bottom: 1px solid rgb(221, 221, 221); width: 100%; margin-bottom: 10px;"></div><?=$message?>
      <div id="info191895" style="margin-top: 15px; display: none;">
        <b>E-mail:</b>
        <a href="mailto:<?=$_POST["email"]?>"><?=$_POST["email"]?></a><br><br>
      </div>
    </td>
  </tr>
</table>
<br>
<br>
<br>
<br>
<center><a href="javascript:window.close();">Sluit dit venster</a></center>
</body>
</html>
<?php
  require_once "../functies/badm_db.inc.php";
  $badm_db = badm_conn_db();
  log_website_usage($start_time, $_POST["naam"], $badm_db);
  mysql_close($badm_db);
?>