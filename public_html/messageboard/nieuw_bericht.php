<?php
  require_once "../functies/website_usage.php";
  require_once "../functies/badm_db.inc.php";
  $badm_db = badm_conn_db();
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="nl">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
  <title>W&amp;L badmintonvereniging vzw - Web Forum</title>
<link href="http://www.badmintonsport.be/css/messageboard.css" rel="stylesheet" type="text/css">
</head>
<script language="JavaScript" type="text/javascript">
<!--
 // dit zorgt ervoor dat wanneer je vanuit de messageboard pagina op de "back"-toets klikt om terug te keren naar deze pagina
 // dat dat niet lukt.  Je blijft op de messageboard pagina.
 javascript:window.history.forward(1);

function trim(str)
{
  // Trim leading/trailing whitespace off string
  return str.replace(/^\s+|\s+$/g, '');
}

function validate_form()
{
  if (trim(document.add_form.tekst.value) == "")
  {
    document.add_form.tekst.focus();
    alert("Je hebt geen bericht ingevuld");
  return false;
  }
  return true;
}
//-->
</script>
<body>
  <div align="center" style="width: 95%; margin-left:auto; margin-right:auto;">
    <table border="0">
      <tr>
        <td valign="top">
        </td>
        <td>&nbsp;&nbsp;</td>
        <td valign="top" align="center">
        <div class="title">W&amp;L Web Forum</div>
        <div class="text" style="padding-top: 10px;"></div>
        <br />
        <br />
        <a href="messageboard.php?p=1&m=n&id=0">Terug naar het Forum</a>&nbsp; &middot; &nbsp;
        <a href="http://www.badmintonsport.be" target="_self">Terug naar de W&amp;L website</a>
      </td>
    </tr>
  </table>
  <br />
<!-- FORM -->
  <form name="add_form" style="display: inline;" action="http://www.badmintonsport.be/messageboard/messageboard.php
<?php
   if ($_GET['m'] == i) //nieuw bericht
   {
      echo "?p=1&m=i&id=".$_GET['id'];
   }
   else // reply
   {
      echo "?p=1&m=u&id=".$_GET['id'];

   // create SQL statement
      $query = "SELECT onderwerp
                  FROM gb_berichten
                 WHERE id = %d";
      $sql = sprintf($query, mysql_real_escape_string($_GET['id']));
      $result = mysql_query ($sql, $badm_db) or badm_mysql_die();
   // format results by row
      $bericht = mysql_fetch_object($result);
   }
?>
" method="post" enctype="application/x-www-form-urlencoded" target="mywin">
    <table width="100%" cellpadding="0" cellspacing="0" style="border: 1px solid #dddddd; ">
      <tr class="tableb">
        <td colspan="2" style="padding: 10px;">
          <b>Verstuur een nieuw bericht</b>
        </td>
      </tr>
      <tr>
        <td class="tablea">
          <table border="0" cellpadding="6" cellspacing="0">
            <tr>
              <td align="right" style="padding: 3px; padding-top: 15px;">Naam:</td>
              <td align="left" style="padding: 3px; padding-top: 15px;">
                <span class="text">
                  <input type="text" name="naam" value="
<?php
   if (isset($naam))
      echo $_COOKIE["naam"];
?>
" maxlength="100" style="width: 450px; " />
                </span>
              </td>
            </tr>
            <tr>
              <td align="right" style="padding: 3px;">E-mail:</td>
              <td style="padding: 3px;" align="left">
                <span class="text">
                  <input type="text" name="email" value="
<?php
   if (isset($email))
      echo $_COOKIE["email"];
?>
" maxlength="100" style="width: 450px; " />
                </span>
              </td>
            </tr>
            <tr>
              <td  align="right" style="padding: 3px;">Onderwerp:</td>
              <td align="left" style="padding: 3px;">
                <span class="text">
                  <input type="text" name="onderwerp" value="
<?php
   if ($_GET['m'] == u) // reply
   {
      echo " RE: ".$bericht->onderwerp;
   }
?>
" maxlength="100" style="width: 450px; " />
                </span>
              </td>
            </tr>
<?php
   if ($_GET['m'] == u) /*de query wordt enkel uitgevoerd bij een reply*/
   {
     mysql_free_result($result);
   }
?>
            <tr>
<!-- MESSAGE ICONS -->

              <td align="right">Bericht Icoontje:</td>
              <td align="left">
                <span class="text">
                  <input type="radio" value="1" name="smiley" checked>
                    <img src="images/set11.gif">
                  <input type="radio" value="2" name="smiley" >
                    <img src="images/set12.gif">
                  <input type="radio" value="3" name="smiley" >
                    <img src="images/set13.gif">
                  <input type="radio" value="4" name="smiley" >
                    <img src="images/set14.gif">
                  <input type="radio" value="5" name="smiley" >
                    <img src="images/set15.gif">
                  <input type="radio" value="6" name="smiley" >
                    <img src="images/set16.gif">
                  <input type="radio" value="7" name="smiley" >
                    <img src="images/set17.gif">
                  <input type="radio" value="8" name="smiley" >
                    <img src="images/set18.gif">
                  <input type="radio" value="9" name="smiley" >
                    <img src="images/set19.gif">
                  <input type="radio" value="10" name="smiley" >
                    <img src="images/set110.gif">
                </span>
              </td>
            </tr>
            <tr>
              <td align="right" valign="top">Bericht:</td>
              <td align="left">
                <table cellpadding="1" cellspacing="0" border="0">
                  <tr>
                    <td>
                      <textarea id="text_content" name="tekst" style="width: 450px; height: 180px "></textarea>
                    </td>
                    <td>
                      <div style="border: 1px solid #000000; width: 3px; height: 180px; background-color: #ff0000;">
                        <div style="height: 180px">
                          <div id="contentbar_pbar" style="width: 3px; height: 180px; background-color: #f5f5f5; border-bottom: 1px solid #000000;">
<!--                            <img src="broken.gif" width="1" height="1" style="display: none"> -->
                          </div>
                        </div>
                      </div>
                    </td>
                  </tr>
<!-- SMILEYS IN TEKST -->
                </table>
                      <nobr>
                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                        <tr>
                            <td align="left">
                <a href="javascript: void(0);" onclick="document.add_form.tekst.value=document.add_form.tekst.value+'[o:)]'; return false;"><img src="images/30_angel.gif" border="0" alt=""></a>
                <a href="javascript: void(0);" onclick="document.add_form.tekst.value=document.add_form.tekst.value+'[:)s]'; return false;"><img src="images/28_spin.gif" border="0" alt=""></a>
                <a href="javascript: void(0);" onclick="document.add_form.tekst.value=document.add_form.tekst.value+'[:))]'; return false;"><img src="images/27_laughing.gif" border="0" alt=""></a>
                <a href="javascript: void(0);" onclick="document.add_form.tekst.value=document.add_form.tekst.value+'[:)]'; return false;"><img src="images/2_smile.gif" border="0" alt=""></a>
                <a href="javascript: void(0);" onclick="document.add_form.tekst.value=document.add_form.tekst.value+'[:-)]'; return false;"><img src="images/12_smile.gif" border="0" alt=""></a>
                <a href="javascript: void(0);" onclick="document.add_form.tekst.value=document.add_form.tekst.value+'[:D]'; return false;"><img src="images/1_grin.gif" border="0" alt=""></a>
<!--                <a href="javascript: void(0);" onclick="document.add_form.tekst.value=document.add_form.tekst.value+'[:-D]'; return false;"><img src="images/13_grin.gif" border="0" alt=""></a>-->
                <a href="javascript: void(0);" onclick="document.add_form.tekst.value=document.add_form.tekst.value+'[:|]'; return false;"><img src="images/8_grim.gif" border="0" alt=""></a>
                <a href="javascript: void(0);" onclick="document.add_form.tekst.value=document.add_form.tekst.value+'[:-|]'; return false;"><img src="images/19_indifferent.gif" border="0" alt=""></a>
                <a href="javascript: void(0);" onclick="document.add_form.tekst.value=document.add_form.tekst.value+'[8)]'; return false;"><img src="images/9_cool.gif" border="0" alt=""></a>
                <a href="javascript: void(0);" onclick="document.add_form.tekst.value=document.add_form.tekst.value+'[8-)]'; return false;"><img src="images/25_coolguy.gif" border="0" alt=""></a>
                <a href="javascript: void(0);" onclick="document.add_form.tekst.value=document.add_form.tekst.value+'[:-?]'; return false;"><img src="images/11_confused.gif" border="0" alt="">
<!-- <a href="javascript: void(0);" onclick="document.add_form.tekst.value=document.add_form.tekst.value+'[Bouncy]'; return false;"> -->
<!-- <img src="images/bouncy.gif" border="0" alt=""></a>  -->
                <a href="javascript: void(0);" onclick="document.add_form.tekst.value=document.add_form.tekst.value+'[Headbang]'; return false;"><img src="images/headbang.gif" border="0" alt=""></a>
<!-- <a href="javascript: void(0);" onclick="document.add_form.tekst.value=document.add_form.tekst.value+'[Eeek]'; return false;"> -->
<!-- <img src="images/22_yikes.gif" border="0" alt=""></a> -->
                <a href="javascript: void(0);" onclick="document.add_form.tekst.value=document.add_form.tekst.value+'[Crying]'; return false;"><img src="images/26_crying.gif" border="0" alt=""></a>
                <a href="javascript: void(0);" onclick="document.add_form.tekst.value=document.add_form.tekst.value+'[Irked]'; return false;"><img src="images/21_irked.gif" border="0" alt=""></a>
                <a href="javascript: void(0);" onclick="document.add_form.tekst.value=document.add_form.tekst.value+'[Oh]'; return false;"><img src="images/3_oh.gif" border="0" alt=""></a>
                <a href="javascript: void(0);" onclick="document.add_form.tekst.value=document.add_form.tekst.value+'[Sing]'; return false;"><img src="images/23_sing.gif" border="0" alt=""></a>
                <a href="javascript: void(0);" onclick="document.add_form.tekst.value=document.add_form.tekst.value+'[Party]'; return false;"><img src="images/party.gif" border="0" alt=""></a>
                <a href="javascript: void(0);" onclick="document.add_form.tekst.value=document.add_form.tekst.value+'[Angry]'; return false;"><img src="images/16_angry.gif" border="0" alt=""></a>
<!-- <a href="javascript: void(0);" onclick="document.add_form.tekst.value=document.add_form.tekst.value+'[Propeller]'; return false;"> -->
<!-- <img src="images/propeller.gif" border="0" alt=""> -->
                <a href="javascript: void(0);" onclick="document.add_form.tekst.value=document.add_form.tekst.value+'[Kissing]'; return false;"><img src="images/kissing.gif" border="0" alt=""></a>
                <a href="javascript: void(0);" onclick="document.add_form.tekst.value=document.add_form.tekst.value+'[Badminton]'; return false;"><img src="images/badminton.gif" border="0" alt=""></a>
                <a href="javascript: void(0);" onclick="document.add_form.tekst.value=document.add_form.tekst.value+'[Shocked]'; return false;"><img src="images/24_shocked.gif" border="0" alt=""></a>
<!--                <a href="javascript: void(0);" onclick="document.add_form.tekst.value=document.add_form.tekst.value+'[Wink]'; return false;"><img src="images/17_wink.gif" border="0" alt=""></a>-->
<!-- <a href="javascript: void(0);" onclick="document.add_form.tekst.value=document.add_form.tekst.value+'[Bouncer]'; return false;"> -->
<!-- <img src="images/bouncer.gif" border="0" alt=""></a> -->
                <a href="javascript: void(0);" onclick="document.add_form.tekst.value=document.add_form.tekst.value+'[Denmark]'; return false;"><img src="images/denmark.gif" border="0" alt=""></a>
                <a href="javascript: void(0);" onclick="document.add_form.tekst.value=document.add_form.tekst.value+'[worshippy]'; return false;"><img src="images/worshippy.gif" border="0" alt=""></a>
                <a href="javascript: void(0);" onclick="document.add_form.tekst.value=document.add_form.tekst.value+'[wip]'; return false;"><img src="images/wip.gif" border="0" alt=""></a>
                <a href="javascript: void(0);" onclick="document.add_form.tekst.value=document.add_form.tekst.value+'[cheer]'; return false;"><img src="images/cheer.gif" border="0" alt=""></a>
                            </td>
                            <td align="right">
<!--                              <a class="text" style="font-size: 11px;" href="javascript: --><!-- PopupWindow('http://pub4.bravenet.com/smilies/index.php?elem=add_form.tekst','570','470');">More Smilies >></a> -->
                            </td>
                          </tr>
                        </table>
                      </nobr>
                    </td>
                  </tr>
                  <tr>
                    <td align="right" valign="top" style="padding-top: 8px;">Opties:</td>
                    <td  align="left" style="padding-top: 8px;">
<!--                      <div> -->
<!--                        <input type="checkbox" style="margin: 0px; " name="emailnotify" value="1"> -->
<!-- Notify me by email when somebody replies to this message -->
<!--                      </div> -->
                      <div>
                        <input type="checkbox" name="nofunimage" style="margin: 0px;" value="1"> Geen bericht icoontje
                      </div>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
          </table>
          <br />
          <br />
          <input type="button" name="cancel" value="&lt;&lt; Cancel " onclick="location.href='messageboard.php?p=1&m=n';" class="formbutton" style="width: 140px;" />&nbsp; &nbsp;
          <input type="submit" name="preview" value=" Preview Message " class="formbutton" style="width: 160px;" onclick="win=window.open('','myWin','height=400,left=150, toolbars=0,top=200,width=650'); this.form.target='myWin';this.form.action='http://www.badmintonsport.be/messageboard/preview.php';">&nbsp; &nbsp; &nbsp; &nbsp;
          <input type="submit" name="post" value=" Post Message &gt;&gt; "  class="formbutton" style="width: 140px;" onclick="return validate_form(); this.form.target='_self';this.form.action='http://www.badmintonsport.be/messageboard/messageboard.php?p=1&m=<?php echo $_GET["m"]; ?>&id=<?php echo $_GET["id"]; ?>';">
      </form>
      <br />
      <br />
    </div>
  </div>
</body>
</html>
<?php
  log_website_usage($start_time, null, $badm_db);
  mysql_close($badm_db);
?>