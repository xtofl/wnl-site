<?php
  require_once "../functies/website_usage.php";

  if (($_GET['m'] == 'i') OR ($_GET['m'] == 'u'))
  {
    //zorgt ervoor dat de velden "naam" en "e-mail" reeds ingevuld zijn wanneer je een bericht wil
    //posten
    setcookie ("naam", $_POST["naam"], time()+7948800); //3 maanden
    setcookie ("email", $_POST["email"], time()+7948800);
  }
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
  <title>W&amp;L badmintonvereniging vzw - Web Forum</title>
  <link href="http://www.badmintonsport.be/css/badminton.css" rel="stylesheet" type="text/css">
  <link href="http://www.badmintonsport.be/css/messageboard.css" rel="stylesheet" type="text/css">
  <link rel="alternate" title="W&amp;L Messageboard" href="http://www.badmintonsport.be/rss/gb.rss" type="application/rss+xml">
</head>

<body>

 <table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
   <td width="100%" style="background-color: #006600;"><img src="http://www.badmintonsport.be/images/pt_transp.gif" width="1px" height="4px" alt="" border="0"></a></td>
  </tr>
  <tr>
   <td align="center" valign="top" bgcolor="#CCCCCC">
    <table width="100%" border="0" cellspacing="0" cellpadding="3">
     <tr align="center" valign="middle">
      <td rowspan="2" align="left">
     <a href="http://www.badmintonsport.be" title="terug naar de W&amp;L website"><img src="http://www.badmintonsport.be/images/logow&amp;l-100.gif" width="100" height="80" alt="logo" border="0"></a>
    </td>
      <td>
     <h4>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;W&amp;L Web Forum</h4>
  </td>
      <td align="right"><a href="http://www.veltem-motors.be" target="_blank"><img src="http://www.badmintonsport.be/images/Veltem_Motors_banner.png" alt="sponsor" height="100" width="320" border="0"></a></td>
     </tr>
    </table>
 </td>
  </tr>
  <tr>
   <td bgcolor="#336600"><img src="http://www.badmintonsport.be/images/pt_transp.gif" width="1px" height="4px" alt=""></td>
  </tr>
 </table>

<?php

   // Connect to DB
   require_once "../functies/badm_db.inc.php";
   $badm_db = badm_conn_db();

if (($_GET['m'] == 'i') OR ($_GET['m'] == 'u')) //mode = Insert of Update
{
  // Message Body verwerken

  if (strlen(trim($_POST["tekst"])) == 0)
  {
    $err_msg = "Lege berichten worden niet bewaard.";
  }
  else
  {

    // Onderwerp verwerken

    if (strlen(trim($_POST["onderwerp"])) == 0)
    {
      $onderwerp = "Geen onderwerp";
    }
    else
    {
      $onderwerp = $_POST["onderwerp"];
    }

    // message icons verwerken

     if ($_POST["nofunimage"] == 1)
     {
        $smiley = null;
     }
     else
     {
        switch ($_POST["smiley"])
        {
           case 1:
             $smiley = "set11";
             break;
           case 2:
             $smiley = "set12";
             break;
           case 3:
             $smiley = "set13";
             break;
           case 4:
             $smiley = "set14";
             break;
           case 5:
             $smiley = "set15";
             break;
           case 6:
             $smiley = "set16";
             break;
           case 7:
             $smiley = "set17";
             break;
           case 8:
             $smiley = "set18";
             break;
           case 9:
             $smiley = "set19";
             break;
           case 10:
             $smiley = "set110";
             break;
        }
     }

     // bericht verwerken

     $message = nl2br($_POST["tekst"]);
     $message = str_replace("[o:)]", "<img src=\"images/30_angel.gif\" border=\"0\" alt=\"\">", $message);
     $message = str_replace("[:)s]", "<img src=\"images/28_spin.gif\" border=\"0\" alt=\"\">", $message);
     $message = str_replace("[:))]", "<img src=\"images/27_laughing.gif\" border=\"0\" alt=\"\">", $message);
     $message = str_replace("[:)]" , "<img src=\"images/30_angel.gif\" border=\"0\" alt=\"\">", $message);
     $message = str_replace("[:-)]", "<img src=\"images/12_smile.gif\" border=\"0\" alt=\"\">", $message);
     $message = str_replace("[:D]" , "<img src=\"images/1_grin.gif\" border=\"0\" alt=\"\">", $message);
  //   $message = str_replace("[:-D]", "<img src=\"images/13_grin.gif\" border=\"0\" alt=\"\">", $message);
     $message = str_replace("[:|]" , "<img src=\"images/8_grim.gif\" border=\"0\" alt=\"\">", $message);
     $message = str_replace("[:-|]", "<img src=\"images/19_indifferent.gif\" border=\"0\" alt=\"\">", $message);
     $message = str_replace("[8)]" , "<img src=\"images/9_cool.gif\" border=\"0\" alt=\"\">", $message);
     $message = str_replace("[8-)]", "<img src=\"images/25_coolguy.gif\" border=\"0\" alt=\"\">", $message);
     $message = str_replace("[:-?]", "<img src=\"images/11_confused.gif\" border=\"0\" alt=\"\">", $message);
     $message = str_replace("[Bouncy]", "<img src=\"images/bouncy.gif\" border=\"0\" alt=\"\">", $message);
     $message = str_replace("[Headbang]", "<img src=\"images/headbang.gif\" border=\"0\" alt=\"\">", $message);
     $message = str_replace("[Eeek]", "<img src=\"images/22_yikes.gif\" border=\"0\" alt=\"\">", $message);
     $message = str_replace("[Crying]", "<img src=\"images/26_crying.gif\" border=\"0\" alt=\"\">", $message);
     $message = str_replace("[Irked]", "<img src=\"images/21_irked.gif\" border=\"0\" alt=\"\">", $message);
     $message = str_replace("[Oh]", "<img src=\"images/3_oh.gif\" border=\"0\" alt=\"\">", $message);
     $message = str_replace("[Sing]", "<img src=\"images/23_sing.gif\" border=\"0\" alt=\"\">", $message);
     $message = str_replace("[Party]", "<img src=\"images/party.gif\" border=\"0\" alt=\"\">", $message);
     $message = str_replace("[Angry]", "<img src=\"images/16_angry.gif\" border=\"0\" alt=\"\">", $message);
     $message = str_replace("[Propeller]", "<img src=\"images/propeller.gif\" border=\"0\" alt=\"\">", $message);
     $message = str_replace("[Kissing]", "<img src=\"images/kissing.gif\" border=\"0\" alt=\"\">", $message);
     $message = str_replace("[Badminton]", "<img src=\"images/badminton.gif\" border=\"0\" alt=\"\">", $message);
     $message = str_replace("[Shocked]", "<img src=\"images/24_shocked.gif\" border=\"0\" alt=\"\">", $message);
  //   $message = str_replace("[Wink]", "<img src=\"images/17_wink.gif\" border=\"0\" alt=\"\">", $message);
     $message = str_replace("[Bouncer]", "<img src=\"images/bouncer.gif\" border=\"0\" alt=\"\">", $message);
     $message = str_replace("[Denmark]", "<img src=\"images/denmark.gif\" border=\"0\" alt=\"\">", $message);
     $message = str_replace("[worshippy]", "<img src=\"images/worshippy.gif\" border=\"0\" alt=\"\">", $message);
     $message = str_replace("[wip]", "<img src=\"images/wip.gif\" border=\"0\" alt=\"\">", $message);
     $message = str_replace("[cheer]", "<img src=\"images/cheer.gif\" border=\"0\" alt=\"\">", $message);
     $message = str_replace("  ", "&nbsp;", $message);
     $message = str_replace("<?", "&lt;?", $message);
     $message = str_replace("?>", "?&gt;", $message);
  }
}

if ($_GET['m'] == 'i' && is_null($err_msg)) /*----------------- nieuw bericht wegschrijven in tabel -----------------------------*/
{
   //bericht wegschrijven in de tabel

   $insert = "INSERT INTO gb_berichten
           ( naam
           , email
           , onderwerp
           , tekst
           , icoon
           , bekeken
           , ip )
           VALUES ('%s', '%s', '%s', '%s', '%s', '0', '".$_SERVER['REMOTE_ADDR']."' )";

   $sql = sprintf($insert, substr(htmlspecialchars($_POST["naam"]), 0, 100)
                         , substr(htmlspecialchars($_POST["email"]), 0, 100)
                         , substr($onderwerp, 0, 100)
                         , $message
                         , $smiley );
   $result = mysql_query($sql, $badm_db) or die("Invalid query: " . mysql_error());

}

else if ($_GET['m'] == 'u' && is_null($err_msg)) /*----------------- nieuwe reply wegschrijven in tabel ------------------------*/

{

   $insert = "INSERT INTO gb_replies
           ( naam
           , email
           , onderwerp
           , tekst
           , icoon
           , bericht_id
           , ip )
           VALUES ('%s', '%s', '%s', '%s', '%s', %d, '".$_SERVER['REMOTE_ADDR']."' ) ";

   $sql = sprintf($insert, substr(htmlspecialchars($_POST["naam"]), 0, 100)
                         , substr(htmlspecialchars($_POST["email"]), 0, 100)
                         , substr($onderwerp, 0, 100)
                         , $message
                         , $smiley
                         , mysql_real_escape_string($_REQUEST["id"]) );
   $result = mysql_query($sql, $badm_db) or die("Invalid query: " . mysql_error());

}

//------------------------------------------------------------------------------------------------------------------------------------------

// create SQL statement om aantal blz te bepalen

   if ($_GET['m'] == 'z')
   {
      $search_string = mysql_real_escape_string($_POST["searchfor"]);
      $sql = "SELECT count(*) as aantal
                FROM gb_berichten
               WHERE tekst LIKE '%".$search_string."%' OR onderwerp LIKE '%".$search_string."%'";
   }
   else
   {
     $sql = "SELECT count(*) as aantal
               FROM gb_berichten";
   }
   $result = mysql_query ($sql, $badm_db) or badm_mysql_die();

  // format results by row
  $topics = mysql_fetch_object($result);
  $total  = ceil($topics->aantal / 50);

?>

  <div align="center" style="width: 95%; margin-left:auto; margin-right:auto;">
<?php
  if (!is_null($err_msg))
  {
    // Print foutboodschap in rood
    echo "<table border=\"0\"><tr>\n";
    echo "        <td valign=\"top\">\n";
    echo "            <span style=\"color:#FF0000; font-weight:bold\">".$err_msg."</span>\n";
    echo "          </a>\n";
    echo "        </td>\n";
    echo "       </tr></table>\n";
  }
?>
    <br/>

<!-- navigatie knoppen -->

    <table width="100%" border="0" cellpadding="0" cellspacing="5">
      <tr>
        <td>

<?php

   if ($_GET['p'] == 1)
   {
      echo "<div class=\"pagerbutton pagerbutton_disabled\">\n";
      echo "<img align=\"absmiddle\" src=\"images/pagefirst.gif\" border=\"0\">&nbsp;&nbsp;Eerste pagina";
      echo "</div></td>\n";
      echo "<td><div class=\"pagerbutton pagerbutton_disabled\">\n";
      echo "<img align=\"absmiddle\" src=\"images/pageback.gif\" border=\"0\">&nbsp;&nbsp;Vorige pagina";
      echo "</div>\n";
   }
   else
   {
      echo "<a href=\"http://www.badmintonsport.be/messageboard/messageboard.php?p=1&m=n\" target=\"_self\" style=\"text-decoration:none\">";
      echo "<img align=\"absmiddle\" src=\"images/first.gif\" border=\"0\">\n";
      echo "</a></td>\n";
      echo "<td><a href=\"http://www.badmintonsport.be/messageboard/messageboard.php?p=";
      echo $_GET['p'] - 1;
      echo "&m=n\" target=\"_self\" style=\"text-decoration:none\">\n";
      echo "<img align=\"absmiddle\" src=\"images/prev.gif\" border=\"0\">\n";
      echo "</a>";
   }

?>

        </td>
        <td>
          <a href="http://www.badmintonsport.be/messageboard/nieuw_bericht.php?m=i" target="_self" style="text-decoration:none">
           <img align="absmiddle" src="images/post.gif" border="0">
          </a>
        </td>
        <td>

<?php

   if ($_GET['p'] == $total OR $total == 1)
   {
      echo "<div class=\"pagerbutton pagerbutton_disabled\">Volgende pagina&nbsp;&nbsp;\n";
      echo "<img align=\"absmiddle\" src=\"images/pageahead.gif\" border=\"0\"></div>";
      echo "</td><td><div class=\"pagerbutton pagerbutton_disabled\">Laatste pagina&nbsp;&nbsp;\n";
      echo "<img align=\"absmiddle\" src=\"images/pagelast.gif\" border=\"0\"></div>";
   }
   else
   {
      echo "<a href=\"http://www.badmintonsport.be/messageboard/messageboard.php?p=";
      echo $_GET['p'] + 1;
      echo "&m=n\" target=\"_self\" style=\"text-decoration:none\">\n";
      echo "<img align=\"absmiddle\" src=\"images/next.gif\" border=\"0\">\n";
      echo "</a></td>\n";
      echo "<td><a href=\"http://www.badmintonsport.be/messageboard/messageboard.php?p=";
      echo $total;
      echo "&m=n\" target=\"_self\" style=\"text-decoration:none\">\n";
      echo "<img align=\"absmiddle\" src=\"images/last.gif\" border=\"0\">\n";
      echo "</a>";
   }

?>

        </td>
      </tr>
    </table>

<!-- Einde navigatieknoppen -->

    <table>
      <tr class="kleinetekst">
        <td width="50%">

<?php

   $footer = "Pagina ".$_GET['p']." van ".$total." (Totaal Aantal Topics: ".$topics->aantal." )";
   echo $footer;
   mysql_free_result($result);

?>

        </td>
        <td width="50%" align="right">
          <form action="http://www.badmintonsport.be/messageboard/messageboard.php?p=1&m=z" method="post" style="display:inline;">Doorzoek dit Forum:&nbsp;<input type="text" name="searchfor" value="">&nbsp;<input type="submit" value="Zoek"></form>
        </td>
      </tr>
    </table>
    <br>
    <table width="100%" cellpadding="0" cellspacing="0" border="0" bgcolor="#dddddd">
      <tr>
        <td>
          <table width="100%" border="0" cellpadding="10" cellspacing="1">
            <tr class="tableb">
              <th>            Topic     </th>
              <th>            Auteur    </th>
              <th width="45"> Bekeken   </th>
              <th width="45"> Antwoorden </th>
              <th>            Laatste Bericht </th>
            </tr>
<?php

   if ($_GET['m'] == 'z') //------------------------------------------zoeken------------------------------------------------------------------------
   {

// create SQL statement

   $sql = "SELECT id
                , naam
                , onderwerp
                , DATE_FORMAT(datum, '%d-%m-%Y - %T') AS timestamp
                , icoon
                , bekeken
                , TO_DAYS(now()) - TO_DAYS(datum) AS ouderdom
           FROM gb_berichten
           WHERE tekst LIKE '%".$search_string."%' OR onderwerp LIKE '%".$search_string
        ."%' ORDER BY datum DESC";
   $result = mysql_query ($sql,$badm_db) or badm_mysql_die();

// format results by row

   while ($topic = mysql_fetch_object($result))
   {
      $new = True;
      if (isset($nieuw))
      {
         $array = explode("/", $_COOKIE["nieuw"]);
         if (in_array($topic->id, $array))
            $new = False;
      }

      $sql2 = "SELECT COUNT(*) AS aantal
                    , DATE_FORMAT(MAX(datum), '%d-%m-%Y - %T') AS datum
                 FROM gb_replies
                WHERE bericht_id = $topic->id
                GROUP BY bericht_id";

      $result2 = mysql_query($sql2,$badm_db) or badm_mysql_die();
      $reply = mysql_fetch_object($result2);

      echo "<tr class=\"tablea\">";
      echo "<td align=\"left\">";

      if ($topic->icoon != '')
      {
         echo "<img align=\"absmiddle\" src=\"images/";
         echo $topic->icoon;
         echo ".gif\">";
      }
      echo "<span class=\"subject\">";
      if ($new)
         echo "<a href=\"bericht.php?n=y&id=";
      else
         echo "<a href=\"bericht.php?n=n&id=";
      echo $topic->id;
      echo "\">";
      echo stripslashes($topic->onderwerp);
      echo "</a>";
      echo "</span>&nbsp;";
      if ($reply->ouderdom == 0)
         $replyouderdom = 3;
      if (min($topic->ouderdom, $replyouderdom) < 2 AND $new)
      {
         echo "<span style=\"border:1px solid #000080; font:bold 10px tahoma; color:#000080; padding-left: 2px;padding-right: 1px;\">&laquo;&nbsp;";
         echo "<span style=\"font-size:9px;\">NIEUW</span>";
      }
      echo "</span>";
      echo "</td>";
      echo "<td align=\"left\">";
      echo $topic->naam;
      echo "</td>";
      echo "<td align=\"center\">";
      echo $topic->bekeken;
      echo "</td>";
      echo "<td align=\"center\">";
      if ($reply->aantal != '')
      {
         echo $reply->aantal;
      }
      else
      {
         echo " 0 ";
      }
      echo "</td>";
      echo "<td align=\"right\" nowrap>";
      if ($reply->datum != '')
      {
         echo $reply->datum;
      }
      else
      {
         echo $topic->timestamp;
      }
      echo "</td>";
      echo "</tr>";
      mysql_free_result($result2);
   }
   mysql_free_result($result);
   }

   else //-------------------------------------alle berichten weergeven----------------------------------------------------------------------
   {

// create SQL statement

   $max = $_GET['p'] * 50;
   $min = $max - 49;
   $where = "WHERE id BETWEEN ".$min." AND ".$max;
   $sql = "SELECT COUNT(*) AS rang
                , b1.id
                , b1.naam
                , b1.email
                , b1.onderwerp
                , DATE_FORMAT(b1.datum, '%d-%m-%Y - %T') AS timestamp
                , b1.icoon
                , b1.bekeken
                , TO_DAYS(now()) - TO_DAYS(b1.datum) AS ouderdom
           FROM gb_berichten AS b1
              , gb_berichten AS b2
           WHERE b1.datum <= b2.datum
           GROUP  BY b1.id
           HAVING rang BETWEEN ".$min." AND ".$max
        ." ORDER  BY rang";

   $result = mysql_query ($sql,$badm_db) or badm_mysql_die();

// format results by row

   while ($topic = mysql_fetch_object($result))
   {
      $new = True;
      if (isset($nieuw))
      {
         $array = explode("/", $_COOKIE["nieuw"]);
         if (in_array($topic->id, $array))
            $new = False;
      }

      $sql2 = "SELECT COUNT(*) as aantal
                    , DATE_FORMAT(MAX(datum), '%d-%m-%Y - %T') AS datum
                    , TO_DAYS(now()) - TO_DAYS(MAX(datum)) AS ouderdom
               FROM gb_replies
               WHERE bericht_id = $topic->id
               GROUP BY bericht_id";
      $result2 = mysql_query ($sql2,$badm_db) or badm_mysql_die();
      $reply = mysql_fetch_object($result2);
      echo "<tr class=\"tablea\">";
      echo "<td align=\"left\">";
      if ($topic->icoon != '')
      {
         echo "<img align=\"absmiddle\" src=\"images/";
         echo $topic->icoon;
         echo ".gif\">";
      }
      echo "<span class=\"subject\">";
      if ($topic->ouderdom < 2 AND $new)
         echo "<a href=\"bericht.php?p=1&n=y&id=";
      else
         echo "<a href=\"bericht.php?p=1&n=n&id=";
      echo $topic->id;
      echo "\">";
      echo stripslashes($topic->onderwerp);
      echo "</a>";
      echo "</span>&nbsp;";
      $replyouderdom = $reply->ouderdom;
      if ($reply->ouderdom == 0)
         $replyouderdom = 3;
      if (min($topic->ouderdom, $replyouderdom) < 2 AND $new)
      {
         echo "<span style=\"border:1px solid #000080; font:bold 10px tahoma; color:#000080; padding-left: 2px;padding-right: 1px;\">&laquo;&nbsp;";
         echo "<span style=\"font-size:9px;\">NEW</span>";
      }
      echo "</span>";
      echo "</td>";
      echo "<td align=\"left\">";
      echo $topic->naam;
      echo "</td>";
      echo "<td align=\"center\">";
      echo $topic->bekeken;
      echo "</td>";
      echo "<td align=\"center\">";
      if ($reply->aantal != '')
      {
         echo $reply->aantal;
      }
      else
      {
         echo " 0 ";
      }
      echo "</td>";
      echo "<td align=\"right\" nowrap>";
      if ($reply->datum != '')
      {
         echo $reply->datum;
      }
      else
      {
         echo $topic->timestamp;
      }
      echo "</td>";
      echo "</tr>";
      mysql_free_result($result2);
   }
   mysql_free_result($result);
   }
?>
          </table>
        </td>
      </tr>
    </table>
    <br />
    <br />

<!-- ---------------------------------------------------------NAVIGATIE KNOPPEN------------------------------------------------------------ -->

    <table width="100%" border="0" cellpadding="0" cellspacing="5">
      <tr>
        <td>

<?php

   if ($_GET['p'] == 1)
   {
      echo "<div class=\"pagerbutton pagerbutton_disabled\">\n";
      echo "<img align=\"absmiddle\" src=\"images/pagefirst.gif\" border=\"0\">&nbsp;&nbsp;Eerste pagina";
      echo "</div></td>\n";
      echo "<td><div class=\"pagerbutton pagerbutton_disabled\">\n";
      echo "<img align=\"absmiddle\" src=\"images/pageback.gif\" border=\"0\">&nbsp;&nbsp;Vorige pagina";
      echo "</div>\n";
   }
   else
   {
      echo "<a href=\"http://www.badmintonsport.be/messageboard/messageboard.php?p=1&m=n\" target=\"_self\" style=\"text-decoration:none\">";
      echo "<img align=\"absmiddle\" src=\"images/first.gif\" border=\"0\">";
      echo "</a></td>\n";
      echo "<td><a href=\"http://www.badmintonsport.be/messageboard/messageboard.php?p=";
      echo $_GET['p'] - 1;
      echo "&m=n\" target=\"_self\" style=\"text-decoration:none\">\n";
      echo "<img align=\"absmiddle\" src=\"images/prev.gif\" border=\"0\">";
      echo "</a>\n";
   }

?>

        </td>
        <td>
          <a href="http://www.badmintonsport.be/messageboard/nieuw_bericht.php?m=i" target="_self" style="text-decoration:none">
           <img align="absmiddle" src="images/post.gif" border="0">
          </a>
        </td>
        <td>

<?php

   if ($_GET['p'] == $total OR $total == 1)
   {
      echo "<div class=\"pagerbutton pagerbutton_disabled\">Volgende pagina&nbsp;&nbsp;";
      echo "<img align=\"absmiddle\" src=\"images/pageahead.gif\" border=\"0\"></div>\n";
      echo "</td><td><div class=\"pagerbutton pagerbutton_disabled\">Laatste pagina&nbsp;&nbsp;";
      echo "<img align=\"absmiddle\" src=\"images/pagelast.gif\" border=\"0\"></div>\n";
   }
   else
   {
      echo "<a href=\"http://www.badmintonsport.be/messageboard/messageboard.php?p=";
      echo $_GET['p'] + 1;
      echo "&m=n\" target=\"_self\" style=\"text-decoration:none\">\n";
      echo "<img align=\"absmiddle\" src=\"images/next.gif\" border=\"0\">";
      echo "</a></td>\n";
      echo "<td><a href=\"http://www.badmintonsport.be/messageboard/messageboard.php?p=";
      echo $total;
      echo "&m=n\" target=\"_self\" style=\"text-decoration:none\">\n";
      echo "<img align=\"absmiddle\" src=\"images/last.gif\" border=\"0\">";
      echo "</a>\n";
   }

?>

        </td>
      </tr>
    </table>
    <table>
      <tr>
        <td colspan="3" align="left">
          <span class="text">

<?php
   echo $footer;
?>

          </span><br><br>
        </td>
      </tr>
    </table>
  </div>
</div>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td width="100%" height="50" valign="top" bgcolor="#FFFFFF">
    <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#CCCCCC">
        <tr>
          <td align="center" bordercolor="#CCCCCC" bgcolor="#006600"><img src="http://www.badmintonsport.be/images/spacer.gif" width="1" height="4" alt=""></td>
        </tr>
        <tr>
          <td width="100%" height="42" align="center" valign="middle" bordercolor="#CCCCCC" bgcolor="#CCCCCC" class="gmenu">
        Last change: 26/9/2007 &nbsp;
            E-mail: <a href="mailto:webmaster@badmintonsport.be">webmaster@badmintonsport.be</a></td>
        </tr>
        <tr>
          <td align="center" bordercolor="#CCCCCC" bgcolor="#006600" class="gmenu"><img src="http://www.badmintonsport.be/images/spacer.gif" width="1" height="4" alt=""></td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</body>
</html>
<?php
  log_website_usage($start_time, null, $badm_db);
  mysql_close($badm_db);
?>