/*
   Selecteer de username, het paswoord en het E-mail adres van alle spelers.
*/

SELECT u.usid
     , u.paswoord
     , u.naam
     , s.email
  FROM users       AS u
     , bad_spelers AS s
 WHERE u.naam = s.naam
   AND s.email IS NOT NULL
LIMIT 0 , 350 