/*
   Combineer de gegevens van de ingeschrevenen voor een tornooi uit de inschr_spel tabel met andere gegevens uit de 
   bad_spelers tabel, zoals leeftijd en e-mail.
*/

SELECT SUBSTRING(b.naam, 1, LOCATE(" ", b.naam))  AS Voornaam
     , SUBSTRING(b.naam, LOCATE(" ", b.naam) + 1, LENGTH(b.naam) - LOCATE(" ", b.naam))  AS Naam
     , DATE_FORMAT(b.geb_dt, '%d-%m-%Y')  AS Geboortedatum
     , (TO_DAYS('2004-04-24') - TO_DAYS(b.geb_dt)) / 365  AS Leeftijd
     , b.Geslacht
     , b.Klassement
     , b.Lidnr
     , CASE i.enkel WHEN "1" THEN "JA" ELSE "NEE" END  AS Enkel
     , CASE i.dubbel WHEN "1" THEN "JA" ELSE "NEE" END  AS Dubbel
     , i.dub_partner  AS Partner
     , b.email  AS "E-mail"
  FROM bad_spelers b
     , inschr_spel i
 WHERE b.lidnr = i.lidnr
   AND i.inschr_torn_id = 150
 ORDER BY 2, 1;