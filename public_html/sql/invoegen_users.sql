/*
   Script om de users tabel mee te vullen op basis van de gegevens uit de spelers tabel.
   Werkt alleen voor spelers voor wie het lidnummer ingevuld is.
   Maak eerst de users tabel leeg voor je dit script uitvoert, zoniet gaan alle spelers er twee keer in zitten, en dan
   werkt het inloggen niet meer.
*/

INSERT INTO users
  ( usid
  , paswoord
  , level
  , naam
  , usid_wijz)
SELECT UPPER(                      --vorm het userid: 3 eerste letters achternaam + 3 eerste letters voornaam
          CONCAT(
             SUBSTRING( 
                REPLACE(
                   SUBSTRING( naam
                            , LOCATE( " ", naam) + 1
                            , LENGTH(naam) - LOCATE( " ", naam))
                 , " ", "")
              , 1, 3)
           , REPLACE(
                REPLACE(
                   SUBSTRING( naam, 1, 3)
                 , "�", "e")
              , "�", "e")))  AS usid
     , CONCAT(                             --vorm het paswoord: een letter + 5 cijfers
          CASE mod( length( naam ) , 26 )  --letter op basis van lengte van de volledige naam
          WHEN  1 THEN "A"
          WHEN  2 THEN "B"
          WHEN  3 THEN "C"
          WHEN  4 THEN "D"
          WHEN  5 THEN "E"
          WHEN  6 THEN "F"
          WHEN  7 THEN "G"
          WHEN  8 THEN "H"
          WHEN  9 THEN "I"
          WHEN 10 THEN "J"
          WHEN 11 THEN "K"
          WHEN 12 THEN "L"
          WHEN 13 THEN "M"
          WHEN 14 THEN "N"
          WHEN 15 THEN "O"
          WHEN 16 THEN "P"
          WHEN 17 THEN "Q"
          WHEN 18 THEN "R"
          WHEN 19 THEN "S"
          WHEN 20 THEN "T"
          WHEN 21 THEN "U"
          WHEN 22 THEN "V"
          WHEN 23 THEN "W"
          WHEN 24 THEN "X"
          WHEN 25 THEN "Y"
          ELSE "Z" END
     , (97531 - lidnr) + (SUBSTRING(lidnr, 1, 2) * 11)) AS paswoord    --cijfers op basis van lidnummer
     , 1         --level
     , naam
     , 'CEYFRE'  --usid_wijz
  FROM bad_spelers
 WHERE lidnr IS NOT NULL
