/*
   Scheiden van voornaam en familienaam met deze Select.
   Je kan dit kopie�ren en plakken in een andere Select.
*/

SELECT SUBSTRING(b.naam, 1, LOCATE(" ", b.naam))  AS Voornaam
     , SUBSTRING(b.naam, LOCATE(" ", b.naam) + 1, LENGTH(b.naam) - LOCATE(" ", b.naam))  AS Naam
  FROM bad_spelers b
 ORDER BY 2, 1;