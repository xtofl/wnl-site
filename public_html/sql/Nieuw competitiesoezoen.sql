/**
 * 12/06/2007 Freek Ceymeulen
 *
 * Na afloop van het huidige seizoen (eind mei), dient de W&L admin module klaargemaakt voor het volgende seizoen.
 * Daarom maken we voor het nieuwe seizoen een copie van de competitieploegen van het afgelopen seizoen
 * Dat is wat het volgende statement doet.
 *
 * Vanaf 1 juli kan je de nieuwe ploegen gebruiken, d.w.z. er spelers aan toekennen
 *
 **/

INSERT INTO bad_competitieploegen ( seizoen, type, ploegnummer, afdeling, sporthal, uur, thumb_url, image_url, user_id, dt_wijz )
SELECT CONCAT(DATE_FORMAT(NOW(), '%Y'), '-', DATE_FORMAT(NOW(), '%Y')+1) AS seizoen
     , type
     , ploegnummer
     , afdeling
     , sporthal
     , uur
     , NULL
     , NULL
     , 'CEYFRE'
     , NOW()
  FROM bad_competitieploegen
 WHERE seizoen = CONCAT(DATE_FORMAT(NOW(), '%Y')-1, '-', DATE_FORMAT(NOW(), '%Y'))
/

--
-- De functie van ploegkapitein afsluiten voor het afgelopen seizoen
--

UPDATE leden_functies
SET eind_dt = NOW()
WHERE functies_id = ( SELECT id FROM clubfuncties WHERE functie = 'KAPITEIN' )
AND ref_id IN ( SELECT id FROM bad_competitieploegen WHERE seizoen = CONCAT(DATE_FORMAT(NOW(), '%Y')-1, '-', DATE_FORMAT(NOW(), '%Y')) )
/



/* -- statisch statement
INSERT INTO bad_competitieploegen ( seizoen, type, ploegnummer, afdeling, sporthal, uur, thumb_url, image_url, user_id, dt_wijz )
SELECT '2007-2008'
     , type
     , ploegnummer
     , afdeling
     , sporthal
     , uur
     , NULL
     , NULL
     , 'CEYFRE'
     , NOW()
  FROM bad_competitieploegen
 WHERE seizoen = '2006-2007'
*/