/*
   Selecteer alle gegevens uit de users tabel op basis van de spelers tabel.
   Om deze gegevens voor 1 bepaalde speler te selecteren voeg je na de FROM-clause
   deze WHERE-clause toe: WHERE naam = 'voornaam achternaam'
*/

SELECT naam
     , UPPER(
         CONCAT(
           SUBSTRING(
             REPLACE(
               SUBSTRING(naam
                        ,LOCATE(" ", naam) + 1
                        ,LENGTH(naam) - LOCATE(" ", naam))
               , " "
               , "")
             , 1
             , 3)
           , REPLACE(
               REPLACE(
                 SUBSTRING(naam, 1, 3)
               , "�"
               , "e")
             , "�"
             , "e")))  AS usid
      , CONCAT(
          CASE MOD(LENGTH(naam), 26)
          WHEN 1 THEN "A"
          WHEN 2 THEN "B"
          WHEN 3 THEN "C"
          WHEN 4 THEN "D"
          WHEN 5 THEN "E"
          WHEN 6 THEN "F"
          WHEN 7 THEN "G"
          WHEN 8 THEN "H"
          WHEN 9 THEN "I"
          WHEN 10 THEN "J"
          WHEN 11 THEN "K"
          WHEN 12 THEN "L"
          WHEN 13 THEN "M"
          WHEN 14 THEN "N"
          WHEN 15 THEN "O"
          WHEN 16 THEN "P"
          WHEN 17 THEN "Q"
          WHEN 18 THEN "R"
          WHEN 19 THEN "S"
          WHEN 20 THEN "T"
          WHEN 21 THEN "U"
          WHEN 22 THEN "V"
          WHEN 23 THEN "W"
          WHEN 24 THEN "X"
          WHEN 25 THEN "Y"
          ELSE "Z" END
     , (97531 - lidnr) + (SUBSTRING(lidnr, 1, 2) * 11))  AS paswoord
     , lidnr
  FROM bad_spelers
 ORDER BY naam;