/* Geef een overzicht van alle spelers die een functie uitoefenen bij W&L
of Geef een overzicht van alle functies bij W&L en wie ze uitoefent */

SELECT s.naam
     , f.omschrijving
     , lf.eind_dt
     , lf.ref_id
  FROM bad_spelers    s
     , clubfuncties   f
     , leden_functies lf
 WHERE s.id = lf.spelers_id
   AND f.id = lf.functies_id
 ORDER BY 1, 2