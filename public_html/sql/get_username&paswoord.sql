/*
   Selecteer de username en het paswoord van een speler.
   Vervang "tom" door de naam van de te zoeken speler (of een deel van de naam).
*/

SELECT naam
     , paswoord
     , usid
  FROM users
 WHERE naam LIKE '%tom%'