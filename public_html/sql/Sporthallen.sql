/*
|| Toon een lijst met clubs en hun sporthal(len)
*/

SELECT c.naam AS Club
     , sc.rang
     , s.naam AS Sporthal
     , s.gemeente
     , s.afstand
     , s.tijd
  FROM bad_clubs c
     , sporthallen_clubs sc
     , sporthallen s
 WHERE c.id = sc.clubs_id
   AND sc.sporthallen_id = s.id
 ORDER BY 1, 2