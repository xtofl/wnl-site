<?php

function speeluren($badm_db)
{
    $query = "SELECT LOWER(u.dag) AS dag
                 , u.uur
                 , u.plaats
                 , u.type
                 , u.doelgroep
                 , u.id
                 , u.beschrijving
                 , u.aantal_plaatsen
                 , u.aantal_reserve AS aantal_reserve_plaatsen
                 , u.prijs
                 , CASE u.dag WHEN 'maandag'   THEN 1
                              WHEN 'dinsdag'   THEN 2
                              WHEN 'woensdag'  THEN 3
                              WHEN 'donderdag' THEN 4
                              WHEN 'vrijdag'   THEN 5
                              WHEN 'zaterdag'  THEN 6
                              WHEN 'zondag'    THEN 7
                   END AS dagnummer
                 , SUM(CASE reserve WHEN 'N' THEN 1 ELSE 0 END) AS aantal_spelers
                 , SUM(CASE reserve WHEN 'Y' THEN 1 ELSE 0 END) AS aantal_reserven
              FROM bad_speeluren u
              LEFT OUTER JOIN leden_uren lu ON u.id = lu.speeluren_id
             WHERE plaats = 'VELTEM'
             GROUP BY LOWER(u.dag)
                    , u.uur
                    , u.plaats
                    , u.type
                    , u.doelgroep
                    , u.id
                    , u.beschrijving
                    , u.aantal_plaatsen
                    , u.aantal_reserve
                    , u.prijs
                    , CASE u.dag WHEN 'maandag'   THEN 1
                                 WHEN 'dinsdag'   THEN 2
                                 WHEN 'woensdag'  THEN 3
                                 WHEN 'donderdag' THEN 4
                                 WHEN 'vrijdag'   THEN 5
                                 WHEN 'zaterdag'  THEN 6
                                 WHEN 'zondag'    THEN 7
                      END
             ORDER BY 11, 2, 3, 5";
    $resultaat = mysql_query($query, $badm_db) or badm_mysql_die();

    $totaal = mysql_num_rows($resultaat);
    $rijen = array();
    while ($rij = mysql_fetch_object($resultaat)) {
        $rijen[] = $rij;
    }
    return $rijen;
}