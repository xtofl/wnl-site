<?php
function get_user_agent()
{
  if ( strpos($_SERVER['HTTP_USER_AGENT'], 'Gecko') )
  {
    if ( strpos($_SERVER['HTTP_USER_AGENT'], 'Netscape') )
    {
      //$browser = 'Netscape (Gecko/Netscape)';
      $browser = 'Netscape';
    }
    else if ( strpos($_SERVER['HTTP_USER_AGENT'], 'Firefox') )
    {
      //$browser = 'Mozilla Firefox (Gecko/Firefox)';
      $browser = 'Mozilla Firefox';
    }
    else
    {
      //$browser = 'Mozilla (Gecko/Mozilla)';
      $browser = 'Mozilla';
    }
  }
  else if ( strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE') )
  {
    if ( strpos($_SERVER['HTTP_USER_AGENT'], 'Opera') )
    {
      //$browser = 'Opera (MSIE/Opera/Compatible)';
      $browser = 'Opera';
    }
    else
    {
      //$browser = 'Internet Explorer (MSIE/Compatible)';
      $browser = 'Internet Explorer';
    }
  }
  else
  {
    $browser = 'Others browsers';
  }
  return $browser;
}
?>