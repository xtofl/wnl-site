<?php

/**
 * Purpose    : Maak een afbeelding met de gegeven tekst vertikaal uitgelijnd
 * Date       : 29/09/2007 Freek Ceymeulen
 * Parameters : tekst : de tekst
 *              size  : grootte van tekst [1-5] (2 voor gewoon, 3 voor bold)
 * Remark     : wordt gebruikt in bewerken_ploeg.php voor de tabelhoofdingen
 **/

if (isset($_REQUEST["tekst"]))
{
  $tekst = $_REQUEST["tekst"];
}

if (isset($_REQUEST["size"]))
{
  $size = $_REQUEST["size"];
}
else
{
  $size = 2;
}

// create a 20*170 image
$im = imagecreate(20, 170);

// define background and black text
$bg = imagecolorallocate($im, 153, 153, 204); // #9999CC
$textcolor = imagecolorallocate($im, 0, 0, 0);

// write the string at the top left
imagestringup($im, $size, 5, 168, $tekst, $textcolor);

// output the image
header("Content-type: image/png");
imagepng($im);
?> 