<?php
/**
 * send_inschr_torn.php
 *
 * object     : Methods to send the tournament subscriptions
 * author     : Freek Ceymeulen
 * created    : 20/06/2007
 **/

 require_once("/home/badmin/public_html/functies/general_functions.php");

/*------------------------------------------------------------------------------------------------------
 | Bouw de inschrijvingen
 -------------------------------------------------------------------------------------------------------*/

  function get_msg_table_rows($result, $doelgroep = "", $zelfde_disc_mag = "n", $which = "all")
  {
    global $cc;
    global $bcc;
    $cc = "";
    $bcc = "";
    $return = "";
    while ($inschrijving = mysql_fetch_object($result))
    {
	  $email = $inschrijving->email;
      // Zet de speler in CC of BCC als hij/zij dat heeft aangegeven in zijn/haar spelersprofiel
      if (($inschrijving->zend_inschrijving == "CC" || $which == "one") && !empty($email))
      {
         $cc .= ",".$inschrijving->naam." <".$email.">";
      }
      elseif ($inschrijving->zend_inschrijving == "BCC" && !empty($email))
      {
        $bcc .= ",".$email;
      }
      $club = "";
      $clubD = "";
      $clubG = "";
	  $partner = "";
      $return .= "<tr valign=\"middle\">\n";
      $return .= "<td>".$inschrijving->naam."</td>\n";
      $return .= "<td align=\"center\">".$inschrijving->klassement."</td>\n";
      $return .= "<td align=\"right\">".$inschrijving->lidnr."</td>\n";
      $return .= "<td align=\"center\">".$inschrijving->geslacht."</td>\n";
      if ($doelgroep == "J" || $doelgroep == "V")
      {
        $return .= "<td align=\"center\">".$inschrijving->geb_dt."</td>\n";
        $return .= "<td align=\"right\">".$inschrijving->leeftijd."</td>\n"; // huidige leeftijd, niet op datum van tornooi
      }
      if ($zelfde_disc_mag == "j")
      {
        if ($inschrijving->enkel == "X")
        {
          $return .= "<td>".$inschrijving->discipline1."</td>\n";
          $return .= "<td>".$inschrijving->reeksE."</td>\n";
		  $partner = $inschrijving->partner;
          if (!empty($partner) && strtolower($partner) != 'x')
          {
            if ($inschrijving->clubE != "W&L bv" && $inschrijving->clubE != "W&L")
            {
              $club = " (".$inschrijving->clubE.",".$inschrijving->lidnrE.",".$inschrijving->klasE.")";
            }
          }
          $return .= "<td>".$partner.$club."</td>\n";
        }
        else
        {
          $return .= "<td>&nbsp;</td>\n";
          $return .= "<td>&nbsp;</td>\n";
          $return .= "<td>&nbsp;</td>\n";
        }
        if ($inschrijving->dubbel == "X")
        {
          $return .= "<td>".$inschrijving->discipline2."</td>\n";
          $return .= "<td>".$inschrijving->reeksD."</td>\n";
          if (strtolower($inschrijving->dub_partner) != 'x')
          {
            if ($inschrijving->clubD != "W&L bv" && $inschrijving->clubD != "W&L")
            {
              $clubD = " (".$inschrijving->clubD.",".$inschrijving->lidnrD.",".$inschrijving->klasD.")";
            }
          }
          $return .= "<td>".$inschrijving->dub_partner.$clubD."</td>\n";
        }
        else
        {
          $return .= "<td>&nbsp;</td>\n";
          $return .= "<td>&nbsp;</td>\n";
          $return .= "<td>&nbsp;</td>\n";
        }
        if ($inschrijving->gemengd == "X")
        {
          $return .= "<td>".$inschrijving->discipline3."</td>\n";
          $return .= "<td>".$inschrijving->reeksG."</td>\n";
          if (strtolower($inschrijving->mix_partner) != 'x')
          {
            if ($inschrijving->clubG != "W&L bv" && $inschrijving->clubG != "W&L")
            {
              $clubG = " (".$inschrijving->clubG.",".$inschrijving->lidnrG.",".$inschrijving->klasG.")";
            }
          }
          $return .= "<td>".$inschrijving->mix_partner.$clubG."</td>\n";
        }
        else
        {
          $return .= "<td>&nbsp;</td>\n";
          $return .= "<td>&nbsp;</td>\n";
          $return .= "<td>&nbsp;</td>\n";
        }
      }
      else // $zelfde_disc_mag = "n"
      {
        $return .= "<td>".$inschrijving->enkel."</td>\n";
        if ($inschrijving->enkel == "X")
        {
          $return .= "<td align=\"center\">".$inschrijving->reeksE."</td>\n";
        }
        else
        {
          $return .= "<td>&nbsp;</td>\n";
        }
        $return .= "<td>".$inschrijving->dubbel."</td>\n";
        if ($inschrijving->dubbel == "X")
        {
          if (strtolower($inschrijving->dub_partner) != 'x')
          {
            if ($inschrijving->clubD != "W&L bv" && $inschrijving->clubD != "W&L")
            {
              $clubD = " (".$inschrijving->clubD.",".$inschrijving->lidnrD.",".$inschrijving->klasD.")";
            }
          }
          $return .= "<td align=\"center\">".$inschrijving->reeksD."</td>\n";
        }
        else
        {
          $return .= "<td>&nbsp;</td>\n";
          $clubD = "";
        }
        $return .= "<td>".$inschrijving->gemengd."</td>\n";
        if ($inschrijving->gemengd == "X")
        {
          if (strtolower($inschrijving->mix_partner) != 'x')
          {
            if ($inschrijving->clubG != "W&L bv" && $inschrijving->clubG != "W&L")
            {
              $clubG = " (".$inschrijving->clubG.",".$inschrijving->lidnrG.",".$inschrijving->klasG.")";
            }
          }
          $return .= "<td align=\"center\">".$inschrijving->reeksG."</td>\n";
        }
        else
        {
          $return .= "<td>&nbsp;</td>\n";
          $clubG = "";
        }
        $return .= "<td>".$inschrijving->dub_partner.$clubD."</td>\n";
        $return .= "<td>".$inschrijving->mix_partner.$clubG."</td>\n";
      }
      $return .= "</tr>\n";
    }
    return $return;
  } // get_msg_table_rows

/*------------------------------------------------------------------------------------------------------
 | Bepaal de tabelhoofdingen
 -------------------------------------------------------------------------------------------------------*/

  function get_msg_table_headers($taal = "N", $doelgroep = "", $zelfde_disc_mag = "n")
  {
    // Label vertalingen
    $naam         = array("N" => "Naam"          , "F"=> "Nom");
    $klassement   = array("N" => "Klas."         , "F"=> "Class.");
    $lidnr        = array("N" => "Lidnr."        , "F"=> "N� Aff.");
    $geslacht     = array("N" => "Gesl."         , "F"=> "Sexe");
    $geb_dt       = array("N" => "Geboren"       , "F"=> "N�");
    $leeftijd     = array("N" => "Lft."          , "F"=> "�ge");
    $enkel        = array("N" => "E"             , "F"=> "S");
    $dubbel       = array("N" => "D"             , "F"=> "D");
    $gemengd      = array("N" => "G"             , "F"=> "Mx");
    $reeksE       = array("N" => "Reeks E"       , "F"=> "Cat. S");
    $reeksD       = array("N" => "Reeks D"       , "F"=> "Cat. D");
    $reeksG       = array("N" => "Reeks G"       , "F"=> "Cat. Mx");
    $partner      = array("N" => "Partner"       , "F"=> "Partenaire");
    $dub_partner  = array("N" => "Dubbel Partner", "F"=> "Partenaire Double");
    $mix_partner  = array("N" => "Mixed Partner" , "F"=> "Partenaire D.Mx");
    $discipline1  = array("N" => "Disc. 1"       , "F"=> "Disc. 1");
    $discipline2  = array("N" => "Disc. 2"       , "F"=> "Disc. 2");
    $discipline3  = array("N" => "Disc. 3"       , "F"=> "Disc. 3");
    // Vul een array met alle header labels
    $return = array($naam[$taal], $klassement[$taal], $lidnr[$taal], $geslacht[$taal]);
    if ($doelgroep == "J" || $doelgroep == "V")
    {
      $return[] = $geb_dt[$taal];
      $return[] = $leeftijd[$taal];
    }
    if ($zelfde_disc_mag == "j")
    {
      $return[] = $discipline1[$taal];
      $return[] = $reeksE[$taal];
      $return[] = $partner[$taal];
      $return[] = $discipline2[$taal];
      $return[] = $reeksD[$taal];
      $return[] = $dub_partner[$taal];
      $return[] = $discipline3[$taal];
      $return[] = $reeksG[$taal];
      $return[] = $mix_partner[$taal];
    }
    else
    {
      $return[] = $enkel[$taal];
      $return[] = $reeksE[$taal];
      $return[] = $dubbel[$taal];
      $return[] = $reeksD[$taal];
      $return[] = $gemengd[$taal];
      $return[] = $reeksG[$taal];
      $return[] = $dub_partner[$taal];
      $return[] = $mix_partner[$taal];
    }
    return $return;
  } // get_msg_table_headers

/*------------------------------------------------------------------------------------------------------
 | Stel de hoofding samen
 -------------------------------------------------------------------------------------------------------*/

  function get_msg_table_header($taal, $doelgroep, $zelfde_disc_mag)
  {
    $return = "";
    $hdr_labels = get_msg_table_headers($taal, $doelgroep, $zelfde_disc_mag);
    foreach($hdr_labels as $value)
    {
      $return .= "<th bgcolor=\"#cccccc\">".$value."</th>\n";
    }
    return $return;
  }

/*------------------------------------------------------------------------------------------------------
 | Bepaal het bericht
 -------------------------------------------------------------------------------------------------------*/

  function get_message($result, $tornooi_id, $tornooi, $dt_inschr, $doelgroep, $zelfde_disc_mag, $naam, $tv_naam, $tv_tel, $which = "all", $taal = "N")
  {
    global $cc;
    global $bcc;
    $return = "";
    $aanspreking = array("N" => "Beste ".$naam.",", "F" => "Madame, Monsieur,");
    $paraf1Def = array("N" => "Hier zijn de definitieve inschrijvingen van W&L bv voor jullie tornooi"
                     , "F" => "Voici les inscriptions d�finitives de W&L bv pour votre tournoi");
    $paraf1Temp = array("N" => "Hier zijn de voorlopige inschrijvingen van <a href=\"http://www.badmintonsport.be\">W&L</a> bv voor jullie tornooi.<br>Wij sturen deze inschrijvingen nu reeds door om te voorkomen dat het tornooi volzet is.<br>Onze definitieve inschrijvingen volgen op %s<br>Elke maandag worden nieuwe en gewijzigde inschrijvingen verstuurd.<br>U vindt de volledige lijst op <a href=\"http://www.badmintonsport.be/tornooien/inschrijvingen.php?id=%s\">http://www.badmintonsport.be/tornooien/inschrijvingen.php?id=%s</a>."
                      , "F" => "Ici vous trouverez les inscriptions provisoires de <a href=\"http://www.badmintonsport.be\">W&L</a> bv pour votre tournoi.<br>Nous envoyons ces inscriptions maintenant pour ne pas risquer que le tournoi soit complet.<br>Vous recevrez nos inscriptions d�finitives le %s<br>Chaque lundi les inscriptions nouvelles et chang�es sont envoy�es.<br>Vous pouvez consulter la liste compl�te sur <a href=\"http://www.badmintonsport.be/tornooien/inschrijvingen.php?id=%s\">http://www.badmintonsport.be/tornooien/inschrijvingen.php?id=%s</a>.");
    $paraf1One = array("N" => "Mijn inschrijving voor jullie tornooi:"
                     , "F" => "Voici mon inscription pour votre tournoi:");
    $conf = array("N" => "Zou u ons willen bevestigen dat u deze mail goed ontvangen hebt? &nbsp;&nbsp;Dank u.<br>"
                 ,"F" => "Pourriez-vous me confirmer la bonne r�ception de cet e-mail? &nbsp;&nbsp;Merci.<br>");
    $groet = array("N" => "Met vriendelijke groeten,", "F" => "Nos sinc�res salutations,");
    $sender = array("N" => "%s<br>tornooiverantwoordelijke W&L bv<br>tornooien@badmintonsport.be<br>%s"
                   ,"F" => "%s<br>responsable des tournois pour W&L bv<br>tornooien@badmintonsport.be<br>%s");
    $message = "
<html>
 <head>
  <title>W&amp;L bv inschrijvingen voor %s</title>
 </head>
 <body>
  <!--
  Als u deze tekst te zien krijgt is uw email programma
  niet correct ingesteld om dit HTML-bericht te tonen.
  Breng ons a.u.b. hiervan op de hoogte!
  If you're seeing this message, your email program
  is not properly reading this HTML formatted message.
  Please inform us about this problem!
  -->
  <p>%s</p>
  <p>%s</p>
  <table width=\"99%%\" border=\"1\" cellspacing=\"0\" cellpadding=\"4\">
   <tr>
    %s
   </tr>
   %s
  </table>
  <p>%s</p>
  <p>%s</p>
  <p>%s</p>
 </body>
</html>";
    if ($which == "all")
    {
      $paraf1 = $paraf1Def[$taal];
    }
    elseif ($which == "new")
    {
      $paraf1 = sprintf( $paraf1Temp[$taal], $dt_inschr, $tornooi_id, $tornooi_id );
    }
    elseif ($which == "one")
    {
      $paraf1 = $paraf1One[$taal];
    }
    else
    {
      mail("freek.ceymeulen@pandora.be", "Possible break-in attempt", "On page ".$_SERVER['PHP_SELF']." from IP ".$_SERVER['REMOTE_ADDR']." using method ".$_SERVER['REQUEST_METHOD']." on url ".$_SERVER['REQUEST_URI'].": which \"".$which."\" is not defined.", "From: webmaster@badmintonsport.be");
    }
    $return = sprintf( $message
                     , $tornooi
                     , $aanspreking[$taal]
                     , $paraf1
                     , get_msg_table_header($taal, $doelgroep, $zelfde_disc_mag)
                     , get_msg_table_rows($result, $doelgroep, $zelfde_disc_mag, $which)
                     , $conf[$taal]
                     , $groet[$taal]
                     , sprintf( $sender[$taal], $tv_naam, $tv_tel ) );
    return $return;
  } // get_message

/*------------------------------------------------------------------------------------------------------
 | Bepaal het onderwerp
 -------------------------------------------------------------------------------------------------------*/

  function get_subject($tornooi, $which = "all", $taal = "N")
  {
    $return = "";
    $subject["N"] = "%s inschrijvingen van W&L bv voor %s";
    $subject["F"] = "Inscriptions %s de W&L bv pour %s";
    $def["N"] = "Definitieve";
    $def["F"] = "d�finitives";
    $temp["N"] = "Voorlopige";
    $temp["F"] = "provisoires";
    $one["N"] = "Inschrijving voor %s";
    $one["F"] = "Inscription pour %s";
    switch ($which)
    {
      case "all":
        $return = sprintf($subject[$taal], $def[$taal], $tornooi);
        break;
      case "new":
        $return = sprintf($subject[$taal], $temp[$taal], $tornooi);
        break;
      case "one":
        $return = sprintf($one[$taal], $tornooi);
        break;
      default:
        mail("freek.ceymeulen@pandora.be", "Possible break-in attempt", "On page ".$_SERVER['PHP_SELF']." from IP ".$_SERVER['REMOTE_ADDR']." using method ".$_SERVER['REQUEST_METHOD']." on url ".$_SERVER['REQUEST_URI'].": which \"".$which."\" is not defined.", "From: webmaster@badmintonsport.be");
        exit;
    }
    return $return;
  }

/*------------------------------------------------------------------------------------------------------
 | Haal de inschrijvingen op
 -------------------------------------------------------------------------------------------------------*/

  function get_subscriptions ($conn, $tornooi_id, $doelgroep = "", $which = "all", $inschr_spel_id = "")
  {
    global $debug;
    $return = false;
    // Define ORDER BY clause
    if ($doelgroep == "J" || $doelgroep == "V")
    {
      $ob = ", leeftijd";
    }
    // Define WHERE clause
    switch ($which)
    {
      case "all":
        $where = "";
        break;
      case "new":
/*        $where = " AND i.dt_wijz >= DATE_SUB(CURDATE(), INTERVAL 7 DAY)
                   AND i.dt_wijz < CURDATE()";*/
          $where = " AND (i.dt_verzonden IS NULL OR DATE(i.dt_wijz) > i.dt_verzonden)";
        break;
      case "one":
        $where = " AND i.id = ".$inschr_spel_id;
        break;
      default:
        mail("freek.ceymeulen@pandora.be", "Possible break-in attempt", "On page ".$_SERVER['PHP_SELF']." from IP ".$_SERVER['REMOTE_ADDR']." using method ".$_SERVER['REQUEST_METHOD']." on url ".$_SERVER['REQUEST_URI'].": which \"".$which."\" is not defined.", "From: webmaster@badmintonsport.be");
        exit;
    }
    // Haal de gegevens op
    $sql = "SELECT i.id
                 , i.naam
                 , i.lidnr
                 , i.klassement
                 , IFNULL(s.geslacht, '&nbsp;') AS geslacht
                 , DATE_FORMAT(i.geb_dt, '%d-%m-%Y') AS geb_dt
                 , (YEAR(CURDATE()) - YEAR(i.geb_dt)) - (RIGHT(CURDATE(), 5) < RIGHT(i.geb_dt, 5)) AS leeftijd
                 , IF (i.enkel = '0', '&nbsp;', 'X') AS enkel
                 , IF (i.dubbel = '0', '&nbsp;', 'X') AS dubbel
                 , IF (i.gemengd = '0', '&nbsp;', 'X') AS gemengd
                 , i.discipline1
                 , i.discipline2
                 , i.discipline3
                 , i.reeksE
                 , i.reeksD
                 , i.reeksG
                 , IF (i.partner = '', '&nbsp;', i.partner) AS partner
                 , IF (i.dub_partner = '', '&nbsp;', i.dub_partner) AS dub_partner
                 , IF (i.mix_partner = '', '&nbsp;', i.mix_partner) AS mix_partner
                 , i.klasE
                 , i.klasD
                 , i.klasG
                 , i.clubE
                 , i.clubD
                 , i.clubG
                 , i.lidnrE
                 , i.lidnrD
                 , i.lidnrG
                 , s.email
                 , s.zend_inschrijving
                 , UCASE(i.usid_wijz) AS usid_wijz
              FROM inschr_spel i LEFT OUTER JOIN bad_spelers s ON (i.naam  = s.naam AND i.lidnr = s.lidnr)
             WHERE i.inschr_torn_id = ".$tornooi_id.$where
          ." ORDER BY i.klassement".$ob.", i.naam";
    $debug .= "<br>get_subscriptions: ".$sql;
    $return = mysql_query ($sql, $conn) or badm_mysql_die();
    return $return;
  }

/*------------------------------------------------------------------------------------------------------
 | Verstuur de lijst met inschrijvingen naar de organisatie
 | Returns TRUE when inscriptions have been send
 |         "No data found" when there are no inscriptions for the given tournament
 |         FALSE otherwise
 -------------------------------------------------------------------------------------------------------*/

  function send_tournament ($conn, $tornooi_id, $tornooi, $dt_inschr, $doelgroep, $zelfde_disc_mag, $naam, $email, $taal = "N", $which = "all", $inschr_spel_id = "")
  {
    global $debug;
    global $cc;
    global $bcc;
    $return = false;
    // Haal de inschrijvingen op
    $result = get_subscriptions($conn, $tornooi_id, $doelgroep, $which, $inschr_spel_id );
    if (mysql_num_rows($result) != 0) //er zijn inschrijvingen voor dit tornooi
    {
      // Haal de gegevens van de tornooiverantwoordelijke op.
      $tv = mysql_query("SELECT s.naam, s.adres, s.postcode, s.woonplaats, IF (s.tel IS NULL, s.gsm, tel) AS tel, s.gsm, s.email FROM bad_spelers s, clubfuncties f, leden_functies lf WHERE s.id = lf.spelers_id AND f.id = lf.functies_id AND f.functie = 'TORNOOI' AND ( lf.eind_dt IS NULL OR lf.eind_dt > NOW() )", $conn);
      $tv_naam  = mysql_result($tv, 0, "naam");
      $tv_tel   = mysql_result($tv, 0, "tel");
      $tv_email = mysql_result($tv, 0, "email");
      // Recipients
      $to = $email;
      // Subject
      $subject = get_subject($tornooi, $which, $taal);
      // Message
      $message = get_message($result, $tornooi_id, $tornooi, $dt_inschr, $doelgroep, $zelfde_disc_mag, $naam, $tv_naam, $tv_tel, $which, $taal);
      // Headers
      $headers  = "MIME-Version: 1.0\r\n";
      $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
      $headers .= "From: W&L bv <tornooien@badmintonsport.be>\r\n";
      $headers .= "Cc: W&L bv <tornooien@badmintonsport.be>".$cc."\r\n";
      $headers .= "Bcc: freek.ceymeulen@pandora.be, ".$tv_email.$bcc."\r\n";
      // Send
      if (mail($to, $subject, $message, $headers))
      {
        $return = true;
      }
    }
    else //er zijn GEEN inschrijvingen voor dit tornooi
    {
      $return = "No data found";
    }
    //mail("freek.ceymeulen@pandora.be", "Debug Send_Tournament", $debug, "From: webmaster@badmintonsport.be");
    return $return;
  }

/*------------------------------------------------------------------------------------------------------
 | Verstuur de lijst met nieuwe en/of gewijzigde inschrijvingen naar de organisatie
 | voor elk tornooi dat nog niet werd afgesloten.
 -------------------------------------------------------------------------------------------------------*/

  function send_new_and_changed_subscriptions ($conn)
  {
    global $debug;
    $return = false;
    // Zoek alle tornooien die nog niet afgesloten zijn
    // en waarvoor er nieuwe en/of gewijzigde inschrijvingen zijn
    // die nog niet werden verzonden
    $sql = "SELECT DISTINCT t.id
                 , t.tornooi
                 , t.organisatie
                 , t.doelgroep
                 , DATE_FORMAT(t.dt_inschr, '%d-%m-%Y') AS dt_inschr
                 , t.zelfde_disc_mag
                 , t.email
                 , t.naam
                 , t.taal
                 , t.verzonden
                 , t.usid_wijz
              FROM inschr_torn t
                 , inschr_spel s
             WHERE s.inschr_torn_id = t.id
               AND (s.dt_verzonden IS NULL OR DATE(s.dt_wijz) > s.dt_verzonden)
               AND t.verzonden = '0'";
    $debug .= "<br>send_new_and_changed_subscriptions: ".$sql;
    $result = mysql_query ($sql, $conn) or badm_mysql_die();
    if (mysql_num_rows($result) == 0)
    {
      $return = "No data found";
    }
    else
    {
      while ($tornooi = mysql_fetch_object($result))
      {
        $return = send_tournament($conn, $tornooi->id, $tornooi->tornooi, $tornooi->dt_inschr, $tornooi->doelgroep, $tornooi->zelfde_disc_mag, $tornooi->naam, $tornooi->email, $tornooi->taal, "new");
        if ($return == false)
        {
          mail("freek.ceymeulen@pandora.be", "Problem in Send_Inschr_Torn.php->Send_New_And_Changed_Subscriptions(".$tornooi->id.")", $debug, "From: webmaster@badmintonsport.be");
        }
        else
        {
          // Verhinder dat een volgende bezoeker deze mailing nog eens triggert
          $update = "UPDATE inschr_spel
                        SET dt_verzonden = NOW()
                      WHERE inschr_torn_id = ".$tornooi->id."
                        AND (dt_verzonden IS NULL OR dt_wijz > dt_verzonden)";
          $result2 = mysql_query($update, $conn) or die("Invalid query: " . mysql_error());
        }
      }
    }
    return $return;
  }

/*------------------------------------------------------------------------------------------------------
 | Verstuur de lijst met ingeschreven naar de organisatie
 | voor elk tornooi waarvan de inschrijvingsperiode vandaag verstrijkt
 | en dat nog niet werd afgesloten.
 -------------------------------------------------------------------------------------------------------*/

  function send_all_subscriptions ($conn)
  {
    global $debug;
    $return = false;
    // Zoek alle tornooien waarvan de inschrijvingsperiode vandaag verstrijkt
    // en die nog niet afgesloten zijn
    $sql = "SELECT id
                 , tornooi
                 , organisatie
                 , doelgroep
                 , DATE_FORMAT(dt_inschr, '%d-%m-%Y') AS dt_inschr
                 , zelfde_disc_mag
                 , email
                 , naam
                 , taal
                 , verzonden
                 , usid_wijz
            FROM inschr_torn
           WHERE DATE_FORMAT(dt_inschr, '%d-%m-%Y') = DATE_FORMAT(NOW(), '%d-%m-%Y')
             AND verzonden = '0'";
    $debug .= "<br>send_all_subscriptions: ".$sql;
    $result = mysql_query ($sql, $conn) or badm_mysql_die();
    if (mysql_num_rows($result) == 0)
    {
      $return = "No data found";
    }
    else
    {
      while ($tornooi = mysql_fetch_object($result))
      {
        $return = send_tournament($conn, $tornooi->id, $tornooi->tornooi, $tornooi->dt_inschr, $tornooi->doelgroep, $tornooi->zelfde_disc_mag, $tornooi->naam, $tornooi->email, $tornooi->taal, "all");
        if ($return == false)
        {
          mail("freek.ceymeulen@pandora.be", "Problem in Send_Inschr_Torn.php->Send_All_Subscriptions(".$tornooi->id.")", $debug, "From: webmaster@badmintonsport.be");
        }
        else
        {
          $update = "UPDATE inschr_torn SET verzonden = '1' WHERE id = ".$tornooi->id;
          $result2 = mysql_query($update, $conn) or die("Invalid query: " . mysql_error());
        }
      }
    }
    return $return;
  }

?>