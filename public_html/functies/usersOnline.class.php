<?php
class usersOnline
{
	var $timeout = 600;
	var $count = 0;
	
	function usersOnline ()
	{
		$this->timestamp = time();
		$this->ip = $this->ipCheck();
		$this->new_user();
		$this->delete_user();
		$this->count_users();
	}
	
	function ipCheck()
	{
	/*
	Deze functie gaat na of een bezoeker van achter een proxy server komt. Waarom?
	Als je site veel gebruikers heeft kan het zijn dat er meerdere bezoekers via dezelfde 
	proxy server komen (bv. AOL). In dat geval zou het script hen als 1 bezoeker herkennen.
	Deze functie probeert het echte IP-adres te achterhalen.
	De getenv() functie werkt niet wanneer PHP als ISAPI module is ingesteld.
	*/
		if (getenv('HTTP_CLIENT_IP'))
		{
			$ip = getenv('HTTP_CLIENT_IP');
		}
		elseif (getenv('HTTP_X_FORWARDED_FOR'))
		{
			$ip = getenv('HTTP_X_FORWARDED_FOR');
		}
		elseif (getenv('HTTP_X_FORWARDED'))
		{
			$ip = getenv('HTTP_X_FORWARDED');
		}
		elseif (getenv('HTTP_FORWARDED_FOR'))
		{
			$ip = getenv('HTTP_FORWARDED_FOR');
		}
		elseif (getenv('HTTP_FORWARDED'))
		{
			$ip = getenv('HTTP_FORWARDED');
		}
		else
		{
			$ip = $_SERVER['REMOTE_ADDR'];
		}
		return $ip;
	}
	
	function new_user()
	{
		$insert = mysql_query ("INSERT INTO useronline(datum, ip) VALUES ('$this->timestamp', '$this->ip')");
	}
	
	function delete_user()
	{
		$delete = mysql_query ("DELETE FROM useronline WHERE datum < ($this->timestamp - $this->timeout)");
	}
	
	function count_users()
	{
		$count = mysql_num_rows ( mysql_query("SELECT DISTINCT ip FROM useronline"));
		return $count;
	}

}
?>
