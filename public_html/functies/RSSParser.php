<?php

/*
 * see: http://www.sitepoint.com/article/php-xml-parsing-rss-1-0
 */

class RssParser {

	var $insideitem = false;
	var $tag = "";
	var $title = "";
	var $description = "";
	var $link = "";
	var $pubdate = "";

	// This function will be called by the XML parser whenever an opening tag is encountered.
	function startElement($parser, $tagName, $attrs)
	{
		if ($this->insideitem)
		{
			$this->tag = $tagName;
		}
		elseif ($tagName == "ITEM")
		{
			$this->insideitem = true;
		}
	}

	// This function will be called by the XML parser whenever an closing tag is encountered.
	function endElement($parser, $tagName)
	{
		if ($tagName == "ITEM")
		{
			echo "<div class=\"item\">\n";
			printf("<div class=\"title\"><span class=\"pubdate\">%s</span>",
			        trim($this->pubdate));
			printf("<span class=\"url\"><a href=\"%s\">%s</a></span></div>",
				trim($this->link), htmlspecialchars(trim($this->title)));
			printf("<div class=\"description\">%s</div>", htmlspecialchars(trim($this->description)));
			echo "</div>\n";
			$this->title = "";
			$this->description = "";
			$this->link = "";
			$this->pubdate = "";
			$this->insideitem = false;
		}
	}

	// This function handles the character data that appears between tags
	function characterData($parser, $data)
	{
		if ($this->insideitem)
		{
		switch ($this->tag)
		{
			case "TITLE":
			$this->title .= $data;
			break;
			case "DESCRIPTION":
			$this->description .= $data;
			break;
			case "LINK":
			$this->link .= $data;
			break;
			case "PUBDATE":
			$this->pubdate .= $data;
			break;

		}
		}
	}
}
?>