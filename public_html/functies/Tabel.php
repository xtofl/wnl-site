<?php

/*Functie voor het schrijven van gegevens in een tabel*/

function makeTable ($gegevens           //array met values voor elke cel, van links naar rechts en van boven naar onder
                   ,$columns            //aantal kolommen
                   ,$align = "left"     //array met voor elke kolom de gewenste uitlijning
                   ,$header = False     // a header line included in the data
                   ,$colwidth = ""      //array met voor elke kolom de gewenste breedte
                   ,$width = ""         //tabelbreedte
                   ,$bgcolor = ""       //achtergrondkleur tabel
                   ,$border = "0"       //dikte van de boorden
                   ,$cellspacing = "0"  //ruimte tussen 2 cellen
                   ,$cellpadding = "3"  //ruimte tussen celwand en celinhoud
				   ,$class = "kleinetekst" )       //style klasse uit css
{
  echo "<table";
  echo " width='".$width."'";
  echo " bgcolor='".$bgcolor."'";
  echo " border='".$border."'";
  echo " cellspacing='".$cellspacing."'";
  echo " cellpadding='".$cellpadding."'";

  if (count($align) == 1)
    echo " align='".$align[0]."'";
  echo ">";
  $i = 0;  //teller voor de gegevens
  $numrows = count($gegevens)/$columns;  //aantal rijen
  // header
  if ($header)
  {
    echo "<tr>\n";
    for ($col = 0; $col < $columns; $col++)
    {
       echo "<th>".$gegevens[$i]."</th>\n";
       $i++;
    }
    echo "</tr>\n";
    $numrows = $numrows-1;  //aantal rijen
  }
  for ($row = 1; $row <= ($numrows); $row++)
  {
    echo "<tr>\n";
    for ($col = 0; $col < $columns; $col++)
    {
      echo "<td class=\"".$class."\"";
      if (count($align) == $columns)
        echo " align='".$align[$col]."'";
      if (count($colwidth) == $columns)
        echo " width='".$colwidth[$col]."'";
      echo ">";
      echo $gegevens[$i];
      echo "</td>\n";
      $i++;
    }
    echo "</tr>\n";
  }
  echo "</table>";
} /*einde functie makeTable*/
?>

