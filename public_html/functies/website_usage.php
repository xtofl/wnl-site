<?php
/**
 * website_usage.php
 *
 * object     : Log info about website usage
 * author     : Freek Ceymeulen
 * created    : 03/06/2007
 **/
 
  /**
   * Simple function to replicate PHP 5 behaviour
   */
  function microtime_float()
  {
    list($usec, $sec) = explode(" ", microtime());
    return ((float)$usec + (float)$sec);
  }

  $start_time = microtime_float(); // This will happen at the time of include

  /**
   * Purpose    : Write the info in the website_usage table
   *              This function should be called at the end of page generation.
   * Parameters : $start        : start time of page generation
   *              $query_string : extra info to identify a page when the page is dynamic
   *              $conn         : database connection
   */
  function log_website_usage($start, $query_string, $conn)
  {
    // Try to identify the visitor
    if (isset($_COOKIE["usid"]))
    {
      $visitor = $_COOKIE["usid"];
    }
    elseif (isset($_COOKIE["naam"]))
    {
      $visitor = $_COOKIE["naam"];
    }
    else
    {
      $visitor = "anoniem";
    }
	// Compute the generation time
    $end_time = microtime_float();
    $elapsed = $end_time - $start;
    // Store the info
    $insert_stmt = "INSERT INTO website_usage (visitor, page, timestamp, session_id, ip, browser, referer, generation_time, query_string)
                    VALUES ('%s', '%s', NOW(), '%s', '%s', '%s', '%s', '%s', '%s')";
    $sql  = sprintf($insert_stmt, substr(mysql_real_escape_string($visitor, $conn), 0, 30)
                                , substr(mysql_real_escape_string(basename($_SERVER['PHP_SELF']), $conn), 0, 64)
                                , $_SESSION['id']
                                , mysql_real_escape_string($_SERVER['REMOTE_ADDR'], $conn)
                                , mysql_real_escape_string($_SERVER['HTTP_USER_AGENT'], $conn)
                                , mysql_real_escape_string($_SERVER['HTTP_REFERER'], $conn)
                                , $elapsed
                                , substr(mysql_real_escape_string($query_string, $conn), 0, 100));
    $result = mysql_query($sql, $conn) or badm_mysql_die();
  }
?>