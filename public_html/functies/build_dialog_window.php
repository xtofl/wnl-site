<?php
function initcap( $in )
{
  return (strtoupper(substr($in, 0, 1)).strtolower(substr($in, 1)));
}
function nvl ($p_in, $p_ifnull)
{
  if (is_null($p_in))
  {
    return $p_ifnull;
  }
  else
  {
    return $p_in;
  }
}
function init ($var)
{
  if (isset($_POST[$var]))
  {
    return $_POST[$var];
  }
  elseif (isset($_GET[$var]))
  {
    return $_GET[$var];
  }
  else return null;
}
/*
--  Procedure : BUILD_DIALOG_WINDOW
--     Object : Build a dialog window that functions as a List Of Values
--       Date : 26/08/2004 fc  Creation
--   Modified : 18/06/2005 fc  created PHP function of PL/SQL procedure
-- Parameters : $lov_item      : the name of the item for which an LOV should be showed
--              $search_string : a query parameter to narrow the values queried
--                                if you specify 'QUERY_ALL' then a query will be executed on 
--                                calling the LOV and all the data will be queried.
*/
$lov_item = init('p_lov_item');
$search_string = init('p_search_string');
$filter_min_length = init('p_filter_min_length');

  echo "<html>\n";
  echo "<head>\n";
  //echo "<title>".initcap(str_replace(str_replace($lov_item, 'p_', ''), '_', ' '))." List\n";
  echo "<title>".initcap($lov_item)." List\n";
  echo "</title>\n";
  echo "<style type=\"text/css\">\n";
  echo "<!--\n";
//  echo " input {\n";
//  echo "     text-transform : uppercase;\n";
//  echo " }\n";
  echo " button {\n";
  echo "     width : 65px;\n";
  echo " }\n";
  echo "-->\n";
  echo "</style>\n";
  echo "<script type=\"text/javascript\" language=\"JavaScript\">\n";
  echo "<!--\n";
  echo "// variable used to get the value of the item selected in the iFrame upon closing the lov\n";
  echo "var preEl;\n";
  echo "function closeMe()\n";
  echo "{\n";
  echo "  window.close();\n";
  echo "}\n";
  echo "function handleCancel()\n";
  echo "{\n";
  echo "  closeMe();\n";
  echo "  return false;\n";
  echo "}\n";
  echo "function handleOK()\n";
  echo "{\n";
  echo "  // Check if a value is selected\n";
  echo "  if (typeof(preEl) != 'undefined')\n";
  echo "  {\n";
  echo "    // Return this value to the calling window\n";
  echo "    window.returnValue = preEl.value+'|'+preEl.name;\n";
  echo "    // Close the dialog window\n";
  echo "    window.close();\n";
  echo "  }\n";
  echo "  else\n";
  echo "  {\n";
  echo "    alert(\"Choose a value first.\");\n";
  echo "  }\n";
  echo "  return false;\n";
  echo "}\n";
  echo "function doSearch()\n";
  echo "{\n";
  echo "  if (document.lov.find.value.replace(/_/g, \"\").replace(/%/g, \"\").length < ".nvl($filter_min_length, 0).")\n";
  echo "  {\n";
  echo "    alert(\"You have to enter at least ".nvl($filter_min_length, 0)." non-wildcard characters in the filter.\");\n";
  echo "    return false;\n";
  echo "  }\n";
  echo "  input.data.p_search_string.value = document.lov.find.value.toUpperCase();\n";
  echo "  input.data.p_do_query.value = 1;\n";
  echo "  input.data.submit();\n";
  echo "  return false;\n";
  echo "}\n";
  echo "-->\n";
  echo "</script>\n";
  echo "</head>\n";
  echo "<body bgcolor=\"#BBBBBB\" onLoad=\"if (opener) opener.blockEvents();\" onUnload=\"if (opener) opener.unblockEvents();\">\n";
  echo "<form name=\"lov\">\n";
  echo " <table width=\"100%\" height=\"100%\">\n";
  echo "  <tr>\n";
  echo "   <td align=\"center\" height=\"40\">Find &nbsp; <input type=\"text\" name=\"find\" ";
  if (strlen($search_string) > 0)
  {
    $pos = strpos($search_string, '***');
    if (!$pos === false)
    {
      echo "value=\"".str_replace($search_string, '***', '%')."\"";
    }
    else
    {
      echo "value=\"".$search_string."\"";
    }
  }
  else
  {
    echo "value=\"%\"";
  }
  echo " onKeyPress=\"if (event.keyCode==13) {return doSearch();}\"></td>\n";
  echo "  </tr>\n";
  echo "  <tr>\n";
  echo "   <td valign=\"top\">\n";
//  echo '    <IFRAME name="input" frameborder="1" scrolling="auto" src="http://www.badmintonsport.be/functies/populate_iframe.php?p_lov_item='.$lov_item.'&p_search_string='.$search_string.'&p_do_query=0&p_value_length=100%&p_description_length=250px&p_filter_min_length=0&p_filter_before_display=false" vspace="0" width="100%" height="100%" marginwidth="1">';
  echo "    <iframe name=\"input\" frameborder=\"1\" scrolling=\"auto\" src=\"http://www.badmintonsport.be/functies/populate_iframe.php?p_lov_item=".$lov_item."&p_search_string=".$search_string."\" vspace=\"0\" width=\"100%\" height=\"100%\" marginwidth=\"1\">\n";
  echo "     Your browser does not support inline frames.  Call your system administrator.\n";
  echo "    </iframe></br>\n";
  echo "   </td>\n";
  echo "  </tr>\n";
  echo "  <tr>\n";
  echo "   <td align=\"center\" height=\"40\">\n";
  echo "    <button accesskey=\"F\" onClick=\"return doSearch();\"><span style=\"text-decoration: underline\">F</span>ind</button>\n";
  echo "    <button accesskey=\"O\" onClick=\"handleOK();\"><span style=\"text-decoration: underline\">O</span>K</button>\n";
  echo "    <button accesskey=\"C\" onClick=\"handleCancel();\"><span style=\"text-decoration: underline\">C</span>ancel</button>\n";
  echo "   </td>\n";
  echo "  </tr>\n";
  echo " </table>\n";
  echo "</form>\n";
  echo "</body>\n";
  echo "</html>\n";
?>