<?php
/**
 * general_functions.php
 *
 * object    : general functions : - Build_Options
 *                                 - Initcap
 *                                 - Nvl
 *                                 - Get_Valid_Date
 *                                 - Write_Menu
 * author    : Freek Ceymeulen
 * created   : 01/05/2005
 **/

  /*
  --------------------------------------------------------------------------------------------
  || Writes an option element for every value of the given domain
  --------------------------------------------------------------------------------------------
  */
  function build_options($domain, $selected, $conn)
  {
    $sql_stmt = sprintf("SELECT value, description FROM codes WHERE domain = '%s' ORDER BY description"
                       , mysql_real_escape_string($domain, $conn));
    $rslt = mysql_query($sql_stmt, $conn) or badm_mysql_die();
    while ($row = mysql_fetch_object($rslt))
    {
      echo "<option value=\"".$row->value."\"";
      echo (strtoupper($row->value) == strtoupper($selected)) ? " selected" : "";
      echo ">".$row->description."</option>\n";
    }
    mysql_free_result($rslt);
  }

  /*
  --------------------------------------------------------------------------------------------
  || Returns the given string with every first character capitalized
  --------------------------------------------------------------------------------------------
  */
  function initcap( $in )
  {
    return (strtoupper(substr($in, 0, 1)).strtolower(substr($in, 1)));
  }

  /*
  --------------------------------------------------------------------------------------------
  || Returns p_ifnull when p_in is null
  --------------------------------------------------------------------------------------------
  */
  function nvl ($p_in, $p_ifnull)
  {
    if (is_null($p_in) || $p_in === '')
    {
      return $p_ifnull;
    }
    else
    {
      return $p_in;
    }
  }

  /*
  --------------------------------------------------------------------------------------------
  || Returns the given date in format YYYY-MM-DD
  --------------------------------------------------------------------------------------------
  */
  function get_valid_date($datum)
  {
    $lv_return = $datum;
    // convert date from DD-MM-YYYY to YYYY-MM-DD if necessary
    if (ereg ("([0-9]{1,2})-([0-9]{1,2})-([0-9]{4})", $datum, $regs))
    {
      $lv_return = "$regs[3]-$regs[2]-$regs[1]";
    }
    elseif (ereg ("([0-9]{1,2})/([0-9]{1,2})/([0-9]{4})", $datum, $regs))
    {
      $lv_return = "$regs[3]-$regs[2]-$regs[1]";
    }
    else
    {
      $err_msg = "Invalid date format";
    }
    return $lv_return;
  }

  /*
  --------------------------------------------------------------------------------------------
  || Writes the admin menu
  --------------------------------------------------------------------------------------------
  */
  function write_menu($p_current)
  {
    echo '          <table id="menu" width="100%" border="0" cellspacing="0" cellpadding="0" style="border-right: 1px solid">';
  // LEDENOVERZICHT
    if (isset($_SESSION['administrator']) || isset($_SESSION['competitieverantwoordelijke']) || isset($_SESSION['bestuurslid']))
    {
      echo '            <tr>';
      if ($p_current == 'leden.php')
      {
        echo '              <td align="center" class="td2"><img src="../poll/image/index.gif" width="32" height="32" border="0" alt=""><br/>Leden</td>';
      }
      else
      {
        echo '              <td align="center" class="td2"><a href="leden.php" class="menu" accesskey="L"><img src="../poll/image/index.gif" width="32" height="32" border="0" alt=""><br/><span style="text-decoration: underline">L</span>eden</a></td>';
      }
      echo '            </tr>';
    }
  // NIEUW LID
    if (isset($_SESSION['administrator']))
    {
      echo '            <tr>';
      if ($p_current == 'nieuw_lid.php')
      {
        echo '              <td align="center" class="td2"><img src="../poll/image/new.gif" width="32" height="32" border="0" alt=""><br/>Nieuw Lid</td>';
      }
      else
      {
        echo '              <td align="center" class="td2"><a href="nieuw_lid.php" class="menu"><img src="../poll/image/new.gif" width="32" height="32" border="0" alt=""><br/>Nieuw Lid</a></td>';
      }
      echo '            </tr>';
    }
  // COMPETITIEPLOEGENOVERZICHT
    if (isset($_SESSION['administrator']) || isset($_SESSION['competitieverantwoordelijke']) || isset($_SESSION['ploegkapitein']) || isset($_SESSION['bestuurslid']))
    {
      echo '            <tr>';
      if ($p_current == 'competitie.php')
      {
        echo '              <td align="center" class="td2"><img src="../poll/image/index.gif" width="32" height="32" border="0" alt=""><br/>Competitie</td>';
      }
      else
      {
        echo '              <td align="center" class="td2"><a href="competitie.php" class="menu" accesskey="C"><img src="../poll/image/index.gif" width="32" height="32" border="0" alt=""><br/><span style="text-decoration: underline">C</span>ompetitie</a></td>';
      }
      echo '            </tr>';
    }
  // NIEUWE COMPETITIEPLOEG
    if (isset($_SESSION['administrator']) || isset($_SESSION['competitieverantwoordelijke']))
    {
      echo '            <tr>';
      if ($p_current == 'nieuwe_ploeg.php')
      {
        echo '              <td align="center" class="td2"><img src="../poll/image/new.gif" width="32" height="32" border="0" alt=""><br/>Nieuwe Ploeg</td>';
      }
      else
      {
        echo '              <td align="center" class="td2"><a href="nieuwe_ploeg.php" class="menu"><img src="../poll/image/new.gif" width="32" height="32" border="0" alt=""><br/>Nieuwe Ploeg</a></td>';
      }
      echo '            </tr>';
    }
  // SPEELUREN
    echo '            <tr>';
    if ($p_current == 'speeluren.php')
    {
      echo '              <td align="center" class="td2"><img src="../poll/image/index.gif" width="32" height="32" border="0" alt=""><br/>Speeluren</td>';
    }
    else
    {
      echo '              <td align="center" class="td2"><a href="speeluren.php" class="menu" accesskey="U"><img src="../poll/image/index.gif" width="32" height="32" border="0" alt=""><br/>Speel<span style="text-decoration: underline">u</span>ren</a></td>';
    }
    echo '            </tr>';
  // NIEUW SPEELUUR
    if (isset($_SESSION['administrator']) || isset($_SESSION['voorzitter']))
    {
      echo '            <tr>';
      if ($p_current == 'nieuw_uur.php')
      {
        echo '              <td align="center" class="td2"><img src="../poll/image/new.gif" width="32" height="32" border="0" alt=""><br/>Nieuw Speeluur</td>';
      }
      else
      {
        echo '              <td align="center" class="td2"><a href="nieuw_uur.php" class="menu"><img src="../poll/image/new.gif" width="32" height="32" border="0" alt=""><br/>Nieuw Speeluur</a></td>';
      }
      echo '            </tr>';
    }
    echo '            <tr>';
  // INSCHRIJVINGEN
    if (isset($_SESSION['administrator']) || isset($_SESSION['bestuurslid']))
    {
      if ($p_current == 'inschrijvingen.php')
      {
        echo '              <td align="center" class="td2"><img src="../poll/image/index.gif" width="32" height="32" border="0" alt=""><br/>Inschrijvingen</td>';
      }
      else
      {
        echo '              <td align="center" class="td2"><a href="inschrijvingen.php" class="menu" accesskey="I"><img src="../poll/image/index.gif" width="32" height="32" border="0" alt=""><br/><span style="text-decoration: underline">I</span>nschrijvingen</a></td>';
      }
      echo '            </tr>';
    }
  // STOCK
    if (isset($_SESSION['administrator']) || isset($_SESSION['bestuurslid']))
    {
      echo '            <tr>';
      if ($p_current == 'stock.php')
      {
        echo '              <td align="center" class="td2"><img src="../poll/image/index.gif" width="32" height="32" border="0" alt=""><br/>Stock</td>';
      }
      else
      {
        echo '              <td align="center" class="td2"><a href="stock.php" class="menu"><img src="../poll/image/index.gif" width="32" height="32" border="0" alt=""><br/>Stock</a></td>';
      }
      echo '            </tr>';
    }
    echo '            <tr>';
  // RAPPORTERING
    if (isset($_SESSION['administrator']) || isset($_SESSION['bestuurslid']))
    {
      echo '            <tr>';
      if ($p_current == 'reporting.php')
      {
        echo '              <td align="center" class="td2"><img src="../poll/image/index.gif" width="32" height="32" border="0" alt=""><br/>Rapportering</td>';
      }
      else
      {
        echo '              <td align="center" class="td2"><a href="reporting.php" class="menu"><img src="../poll/image/index.gif" width="32" height="32" border="0" alt=""><br/>Rapportering</a></td>';
      }
      echo '            </tr>';
    }
    echo '            <tr>';
  // INSTELLINGEN
    if (isset($_SESSION['administrator']))
    {
      if ($p_current == 'instellingen.php')
      {
        echo '              <td align="center" class="td2"><img src="../poll/image/settings.gif" width="32" height="32" border="0" alt=""><br/>Configuratie</td>';
      }
      else
      {
        echo '              <td align="center" class="td2"><a href="instellingen.php" class="menu"><img src="../poll/image/settings.gif" width="32" height="32" border="0" alt=""><br/>Configuratie</a></td>';
      }
      echo '            </tr>';
    }
    echo '            <tr>';
    echo '          </table>';
  }
?>