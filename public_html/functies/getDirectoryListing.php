<?php
function getDirectoryListing($dirname, $sortorder = "a", $show_subdirs = 0, $show_subdirfiles = 0, $exts = "", $ext_save = 1)
{
// This function will return an array with filenames based on the criteria you can set in the variables
// @sortorder : a for ascending (the standard) or d for descending (you can use the "r" for reverse as well, works the same)
// @show_subdirs : 0 for NO, 1 for YES - meaning it will show the names of subdirectories if there are any
// Logically subdirnames will not be checked for the required extentions
// @show_subdirfiles : 0 for NO, 1 for YES - meaning it will show files from the subdirs
// Files from subdirs will be prefixed with the subdir name and checked for the required extentions.
// @exts can be either a string or an array, if not passed to the function, then the default will be a check for common image files
// If exts is set to "all" then all extentions are allowed
// @ext_save : 1 for YES, 0 for NO - meaning it will filter out system files or not (such as .htaccess)

   if (!$exts || empty($exts) || $exts == "") {
       $exts = array("jpg", "gif", "jpeg", "png");
   }
   if ($handle = opendir($dirname)) {
       $filelist = array();
       while (false !== ($file = readdir($handle))) {

           // Filter out higher directory references
           if ($file != "." && $file != "..") {
               // Only look at directories or files, filter out symbolic links
               if ( filetype ($dirname."/".$file) != "link") {
                   // If it's a file, check against valid extentions and add to the list
                   if ( filetype ($dirname."/".$file) == "file" ) {
                       if (checkFileExtention($file, $exts, $ext_save)) {
                                       $filelist[] = $file;
                       }
                   }
                   // If it's a directory and either subdirs should be listed or files from subdirs add relevant names to the list
                   else if ( filetype ($dirname."/".$file) == "dir" && ($show_subdirs == 1 || $show_subdirfiles == 1)) {
                       if ($show_subdirs == 1) {
                           $filelist[] = $file;
                       }
                       if ($show_subdirfiles == 1) {
                           $subdirname = $file;
                           $subdirfilelist = getDirectoryListing($dirname."/".$subdirname."/", $sortorder, $show_subdirs, $show_subdirfiles, $exts, $ext_save);
                           for ($i = 0 ; $i < count($subdirfilelist) ; $i++) {
                               $subdirfilelist[$i] = $subdirname."/".$subdirfilelist[$i];
                           }
                           $filelist = array_merge($filelist, $subdirfilelist);
                       }

                   }

               }
           }
       }
       closedir($handle); 

       // Sort the results
       if (count($filelist) > 1) {
           natcasesort($filelist);
           if ($sortorder == "d" || $sortorder == "r" ) {
               $filelist = array_reverse($filelist, TRUE);
           }
       }
       return $filelist;
   }
   else {
       return false;
   }
}

function checkFileExtention($filename, $exts, $ext_save = 1) {
   $passed = FALSE;
   if ($ext_save == 1) {
       if (preg_match("/^\./", $filename)) {
           return $passed;
       }
   }
   if ($exts == "all") {
                   $passed = TRUE;
       return $passed;
   }
   if (is_string($exts)) {
       if (eregi("\.". $exts ."$", $filename)) {
                       $passed = TRUE;
           return $passed;
       }
   } else if (is_array($exts)) {
       foreach ($exts as $theExt) {
           if (eregi("\.". $theExt ."$", $filename)) {
               $passed = TRUE;
               return $passed;
           }
       }
   }
   return $passed;
}
?>