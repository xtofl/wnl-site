<?php

/**
 *  A class to handle sessions by using a mySQL database for session related data storage providing better
 *  security then the default session handler used by PHP.
 *
 *  To prevent session hijacking, don't forget to use the {@link regenerate_id} method whenever you do a
 *  privilege change in your application
 *
 * 30/11/2014 FC mysql_real_escape_string replaced with mysql_escape_string to avoid "Warning: mysql_real_escape_string() [function.mysql-real-escape-string]: Access denied for user 'root'@'localhost' (using password: NO) in /home/badmin/public_html/functies/sess.php on line 232"
 *                                                                                   "Warning: mysql_real_escape_string() [function.mysql-real-escape-string]: A link to the server could not be established in /home/badmin/public_html/functies/sess.php on line 232"
 *
 **/

//error_reporting(E_ALL);

/* Create new object of class */
$session = new dbsession();

class dbsession
{
    /**
     *  Constructor of class
     *
     *  Initializes the class and starts a new session
     *
     *  There is no need to call start_session() after instantiating this class
     *
     *  @param  integer     $gc_maxlifetime     (optional) the number of seconds after which data will be seen as 'garbage' and
     *                                          cleaned up on the next run of the gc (garbage collection) routine
     *
     *                                          Default is specified in php.ini file
     *
     *  @param  integer     $gc_probability     (optional) used in conjunction with gc_divisor, is used to manage probability that
     *                                          the gc routine is started. the probability is expressed by the formula
     *
     *                                          probability = $gc_probability / $gc_divisor
     *
     *                                          So if $gc_probability is 1 and $gc_divisor is 100 means that there is
     *                                          a 1% chance the the gc routine will be called on each request
     *
     *                                          Default is specified in php.ini file
     *
     *  @param  integer     $gc_divisor         (optional) used in conjunction with gc_probability, is used to manage probability
     *                                          that the gc routine is started. the probability is expressed by the formula
     *
     *                                          probability = $gc_probability / $gc_divisor
     *
     *                                          So if $gc_probability is 1 and $gc_divisor is 100 means that there is
     *                                          a 1% chance the the gc routine will be called on each request
     *
     *                                          Default is specified in php.ini file
     *
     *  @return void
     */
    function dbsession($gc_maxlifetime = "", $gc_probability = "", $gc_divisor = "")
    {

        // if $gc_maxlifetime is specified and is an integer number
        if ($gc_maxlifetime != "" && is_integer($gc_maxlifetime))
        {
            // set the new value
            @ini_set('session.gc_maxlifetime', $gc_maxlifetime);
        }

        // if $gc_probability is specified and is an integer number
        if ($gc_probability != "" && is_integer($gc_probability))
        {
            // set the new value
            @ini_set('session.gc_probability', $gc_probability);
        }

        // if $gc_divisor is specified and is an integer number
        if ($gc_divisor != "" && is_integer($gc_divisor))
        {
            // set the new value
            @ini_set('session.gc_divisor', $gc_divisor);
        }

        // get session lifetime
        $this->sessionLifetime = ini_get("session.gc_maxlifetime");

    // get the requested page
    $this->page = $_SERVER['PHP_SELF'];

    // get the user
    if (isset($_REQUEST["usid"]))
    {
        $this->username = $_REQUEST["usid"];
    }
    elseif (isset($_SESSION["username"]))
    {
        $this->username = $_SESSION["username"];
    }

        // register the new handler
        session_set_save_handler(
            array(&$this, '_open'),
            array(&$this, '_close'),
            array(&$this, '_read'),
            array(&$this, '_write'),
            array(&$this, '_destroy'),
            array(&$this, '_gc')
        );
        register_shutdown_function('session_write_close');

        // start the session
        session_start();
        header("Cache-control: private"); // to overcome/fix a bug in IE 6.x

    }

    /**
     *  Deletes all data related to the session
     *
     *  @since 1.0.1
     *
     *  @return void
     */
    function stop()
    {
        $this->regenerate_id();
        session_unset();
        session_destroy();
    }

    /**
     *  Regenerates the session id.
     *
     *  <b>Call this method whenever you do a privilege change!</b>
     *
     *  @return void
     */
    function regenerate_id()
    {

        // saves the old session's id
        $oldSessionID = session_id();

        // regenerates the id
        // this function will create a new session, with a new id and containing the data from the old session
        // but will not delete the old session
        session_regenerate_id();

        // because the session_regenerate_id() function does not delete the old session,
        // we have to delete it manually
        $this->_destroy($oldSessionID);

    }

    /**
     *  Get the number of online users
     *
     *  @return integer     number of users currently online
     */
    function get_users_online()
    {

        // call the garbage collector
        $this->_gc($this->sessionLifetime);

        // counts the rows from the database
        $result = @mysql_fetch_assoc(@mysql_query("SELECT COUNT(ses_id) as count FROM sessions"));

        // return the number of found rows
        return $result["count"];

    }

    /**
     *  Custom open() function
     *
     *  @access private
     */
    function _open($save_path, $session_name)
    {
        return TRUE;
    }

    /**
     *  Custom close() function
     *
     *  @access private
     */
    function _close()
    {
        // call the garbage collector
        $this->_gc($this->sessionLifetime);
        return TRUE;
    }

    /**
     *  Custom read() function
     *
     *  @access private
     */
    function _read($ses_id)
    {
        if (get_magic_quotes_gpc())
        {
            $ses_id = stripslashes($ses_id);
        }
        // Quote if not integer
        if (!is_numeric($ses_id))
        {
            $ses_id = mysql_escape_string($ses_id);
        }
        $session_sql = "SELECT * FROM sessions"
                     . " WHERE ses_id = '$ses_id'";
        $session_res = @mysql_query($session_sql);

        // if anything was found
        if (is_resource($session_res) && @mysql_num_rows($session_res) > 0)
        {
            // return found data
            $fields = @mysql_fetch_assoc($session_res);
            // don't bother with the unserialization - PHP handles this automatically
            return $fields["ses_value"];
        }

        // if there was an error return an empty string - this HAS to be an empty string
        return "";
    }

    /**
     *  Custom write() function
     *
     *  @access private
     */
    function _write($ses_id, $data)
    {
        // update the existing session's data
        // and set new expiry time
        $session_sql = "UPDATE sessions"
                     . "   SET ses_expire = '".mysql_escape_string(time() + $this->sessionLifetime)
                     . "'    , ses_value  = '".addslashes($data)
           . "'    , page = '".$this->page."'"
           . " WHERE ses_id='".mysql_escape_string($ses_id)."'";
        $session_res = @mysql_query ($session_sql);
        if (!$session_res)
        {
            return FALSE;
        }
        // if anything happened
        if (mysql_affected_rows())
        {
            return TRUE;
        }

        $session_sql = "INSERT INTO sessions"
                     . " (ses_id, ses_expire, ses_start, ses_value, username, page)"
                     . " VALUES ('".mysql_escape_string($ses_id)."', '".mysql_escape_string(time() + $this->sessionLifetime)
                     . "', '" . time() . "', '".addslashes($data)."', '".$this->username."', '".$this->page."')";
        $session_res = @mysql_query ($session_sql);
        if (!$session_res) {
            return FALSE;
        }         else {
            return TRUE;
        }
    }

    /**
     *  Custom destroy() function
     *
     *  @access private
     */
    function _destroy($ses_id)
    {
        $session_sql = "DELETE FROM sessions"
                     . " WHERE ses_id = '".mysql_escape_string($ses_id)."'";
        $session_res = @mysql_query ($session_sql);

        // if anything happened
        if (@mysql_affected_rows())
        {
            return true;
        }

        // if something went wrong, return false
        return false;
    }

    /**
     *  Custom gc() function (garbage collector, deletes old sessions)
     *
     *  @access private
     */
    function _gc($maxlifetime)
    {
//        $ses_life = strtotime("-15 minutes");

        $session_sql = "DELETE FROM sessions"
                     . " WHERE ses_expire < '".mysql_escape_string(time() - $maxlifetime)."'";
        $session_res = @mysql_query ($session_sql);

        if (!$session_res)
        {
            return FALSE;
        }
        else
        {
            return TRUE;
        }
    }
}
?>