<?php
function initcap( $in )
{
  return (strtoupper(substr($in, 0, 1)).strtolower(substr($in, 1)));
}
function nvl ($p_in, $p_ifnull)
{
  if (is_null($p_in))
  {
    return $p_ifnull;
  }
  else
  {
    return $p_in;
  }
}
function init ($var)
{
  if (isset($_POST[$var]))
  {
    return $_POST[$var];
  }
  elseif (isset($_GET[$var]))
  {
    return $_GET[$var];
  }
  else return null;
}
/*
--  Procedure : WRITE_DESCRIPTION
--     Object : Writes a description out in the iFrame
--       Date : 09/06/2005  fc  Creation
-- Parameters : $description : the description
--              $value       : the value (if any)
*/
function write_description( $description
                          , $value = '' )
{
  echo "    <input type=\"text\" name=\"".$value."\" class=\"value\" value=\"".$description."\" onFocus=\"highlight(this);\" onMouseOver=\"this.style.cursor='pointer'\"><br/>\n";
}
/*
--  Procedure : GET_VALUES
--     Object : Queries the LOV values and writes them
--       Date : 07/06/2005  fc  Creation
-- Parameters : $lov_item      : the name of the item for which LOV-data should be queried
--              $search_string : a query parameter to narrow the values queried
*/
function get_values( $lov_item
                   , $search_string )
{
  // Connect to db
  require_once "badm_db.inc.php";
  $badm_db = badm_conn_db();

  switch (strtoupper($lov_item))
  {
  case '$DESTINATION_CODE' :
/*    FOR rec IN ( SELECT destination_code
                      , INITCAP(description) AS description
                   FROM destination
                  WHERE destination_code LIKE $search_string
                  ORDER BY 2 ) LOOP
      write_value_and_desc( rec.destination_code, rec.description );
    END LOOP;*/
    break;
  case 'SPELER' :
    // Build select statement
    $query  = "SELECT naam
                    , id
                 FROM bad_spelers
                WHERE eind_dt IS NULL
                  AND naam LIKE '%s'
                ORDER BY naam";
  case 'KAPITEIN' :
    // Build select statement
    $query  = "SELECT naam
                    , id
                 FROM bad_spelers
                WHERE eind_dt IS NULL
                  AND naam LIKE '%s'
                ORDER BY naam";

  case 'VERANTWOORDELIJKE' :
    // Build select statement
    $query  = "SELECT naam
                    , id
                 FROM bad_spelers
                WHERE eind_dt IS NULL
                  AND naam LIKE '%s'
                ORDER BY naam";
  case 'TRAINER' :
    // Build select statement
    $query  = "SELECT naam
                    , id
                 FROM bad_spelers
                WHERE eind_dt IS NULL
                  AND naam LIKE '%s'
                ORDER BY naam";
    $sql  = sprintf($query, mysql_real_escape_string(strtoupper($search_string)));
    $result = mysql_query($sql, $badm_db) or badm_mysql_die();
    while ($row = mysql_fetch_row($result))
    {
      write_description( $row[0], $row[1] );
    }
    break;
  }
}
/*
--
--  Procedure : POPULATE_IFRAME
--     Object : Populates an iFrame in the dialog window that shows the LOV-values
--       Date : 26/08/2004  fc  Creation
-- Parameters : $lov_item              : the name of the item for which LOV-data should be queried
--              $search_string         : a query parameter to narrow the values queried
--              $value_length          : the width of the value column on the LOV
--              $description_length    : the width of the description column on the LOV
--              $do_query              : indicates the user pressed the find button {0|1}
--              $filter_before_display : not autoquery {true|false}
*/
$lov_item = init('p_lov_item');
$search_string = init('p_search_string');
$do_query = nvl(init('p_do_query'), 0);
$value_length = nvl(init('p_value_length'), '100%');
$description_length = nvl(init('p_description_length'), '250px');
$filter_min_length = nvl(init('p_filter_min_length'), 0);
$filter_before_display = nvl(init('p_filter_before_display'), false);

  $pos = strpos($search_string, '***');
  if (!$pos === false)
  {
    $lv_search_string = str_replace($search_string, '***', '%');
  }
  else
  {
    $lv_search_string = $search_string;
  }
  echo "<html>\n";
  echo "<head>\n";
  echo "<style type=\"text/css\">\n";
  echo "<!--\n";
  echo " input {\n";
  echo "     border : none;\n";
  echo " }\n";
  echo "  input.value {\n";
  echo "      width : ".$value_length.";\n";
  echo "  }\n";
  echo "    input.descr {\n";
  echo "      width : ".$description_length.";\n";
  echo "  }\n";
  echo " td.label {\n";
  echo "     border-bottom-style : solid;\n";
  echo "     border-bottom-width : thin;\n";
  echo "     border-bottom-color : black;\n";
  echo " }\n";
  echo "-->\n";
  echo "</style>\n";
  echo "<script>\n";
  echo "// original background color\n";
  echo "var orgBColor;\n";
  echo "// original text color\n";
  echo "var orgTColor;\n";
  echo "// original font weight\n";
  echo "var orgFWeight = 'normal';\n";
  echo "// Highlight an element when clicked on it\n";
  echo "// and restore the previously highlighted item to its original settings\n";
  echo "function highlight(element)\n";
  echo "{\n";
  echo "  // preEl is a variable defined in the parent window, so we call it by specifying 'top' */\n";
  echo "  if(typeof(parent.preEl)!= 'undefined') // some element is already highlighted\n";
  echo "  {\n";
  echo "    // restore this element to its original settings\n";
  echo "    parent.preEl.style.backgroundColor = orgBColor;\n";
  echo "    parent.preEl.style.color = orgTColor;\n";
  echo "    parent.preEl.style.fontWeight = orgFWeight;\n";
  echo "  }\n";
  echo "  // store settings of new element\n";
  echo "  orgBColor = element.style.backgroundColor;\n";
  echo "  orgTColor = element.style.color;\n";
  echo "  orgFWeight = element.style.fontWeight;\n";
  echo "  // highlight new element\n";
  echo "  element.style.backgroundColor = 'blue';\n";
  echo "  element.style.color = 'white';\n";
  echo "  element.style.fontWeight = 'bold';\n";
  echo "  // set previous element to new element\n";
  echo "  parent.preEl = element;\n";
  echo "}\n";
  echo "  function handleEvent(key_code)\n";
  echo "  {\n";
  echo "    if (key_code == 13) // enter\n";
  echo "    {\n";
  echo "      parent.handleOK();\n";
  echo "    }\n";
  echo "    else if (key_code == 40) // down\n";
  echo "    {\n";
  echo "      event.keyCode = 9; // tab\n";
  echo "    }\n";
  echo "    else if (key_code == 38) // up\n";
  echo "    {\n";
  echo "      var obj = new ActiveXObject('Wscript.shell');\n";
  // In order for this to work you should enable native ActiveX in Tools>Internet Options>Security>Custom Level
  // otherwise you get a javascript error "automation server can't create object"
  echo "      obj.SendKeys(\"+{TAB}\");\n";
  echo "    }\n";
  echo "  }\n";
  echo "</script>\n";
  echo "</head>\n";
  echo "<body bgcolor=\"#BBBBBB\" onKeyDown=\"handleEvent(event.keyCode);\">\n";
  /*
  || This form is submitted when the 'Find' button in the parent window is clicked.
  || On submission a select statement is conducted that will get all the values that correspond to the string
  || entered in the Find field in the parent window, and these values will be displayed here in the iFrame
  */
  echo "<form name=\"data\" action=\"".$_SERVER['PHP_SELF']."\" method=\"post\">\n";
  /*
  || These hidden fields are used to save the parameter values received from the parent window
  || and to transmit these values to the populate_iframe procedure on submit.
  */
  echo " <input type=\"hidden\" name=\"p_lov_item\" value=\"".$lov_item."\">\n";
  echo " <input type=\"hidden\" name=\"p_search_string\" value=\"\">\n";
  echo " <input type=\"hidden\" name=\"p_do_query\" value=\"1\">\n";
  echo " <table width=\"100%\">\n";
  echo "  <tr>\n";
  echo "   <td align=\"center\" class=\"label\" valign=\"top\">\n";
  /* Print the label of the iFrame */
  //echo initcap(str_replace(str_replace($lov_item, 'p_', ''), '_', ' '));
  echo initcap($lov_item);
  echo "   </td>\n";
  echo "  </tr>\n";
  echo "  <tr>\n";
  echo "   <td align=\"center\">\n";
  /* Display the requested values. */
  if ($lv_search_string == NULL)
  {
    $lv_search_string = '%';
  }
  if (nvl(strlen(str_replace(str_replace($lv_search_string, '_', ''), '%', '')), 0) >= $filter_min_length)
  {
    if ($do_query <> 0)
    {
      get_values( $lov_item, $lv_search_string );
    }
    elseif (!$filter_before_display)
    {
      get_values( $lov_item, $lv_search_string );
    }
  }
  echo "   </td>\n";
  echo "  </tr>\n";
  echo " </table>\n";
  echo "</form>\n";
  echo "</body>\n";
  echo "</html>\n";
?>