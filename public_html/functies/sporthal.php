<?php
//
// Toon adres en link naar plannetje op map24
//

function display_sporthal($club_naam, $conn, $rang = 1)
{
  $select_stmt = "
SELECT sh.naam
     , sh.adres
     , sh.postcode
     , sh.gemeente
     , sh.tel
     , sh.afstand
     , sh.tijd
  FROM sporthallen sh
     , sporthallen_clubs sc
     , bad_clubs bc
 WHERE sh.id = sc.sporthallen_id
   AND sc.clubs_id = bc.id
   AND bc.naam = '%s'
   AND sc.rang = %d";

  $query = sprintf($select_stmt, mysql_real_escape_string($club_naam)
                                     , mysql_real_escape_string($rang));

  $rslt = mysql_query($query, $conn) or badm_mysql_die();

  if (mysql_num_rows($rslt) > 0)
  {
  
    $row = mysql_fetch_row($rslt);

    echo "<p>".$row[0]."<br>\n";
    echo $row[1]."<br>\n";
    echo $row[2]." ".$row[3]."<br>\n";
    echo "tel: ".$row[4]."<br>\n";
    echo "<span title=\"afstand en tijd vanaf de sporthal in Veltem\">afstand: ".$row[5]." km (".$row[6].")</span><br>\n";
    echo "[<a href=\"http://link2.map24.com/?lid=1bf222f9&maptype=JAVA&width0=1500&street0=".$row[1]."&zip0=".$row[2]."&city0=".$row[3]."&state0=&country0=BE&logo_url=&name0=".$row[0]."&description0=\" target=\"_blank\">plan</a>]</p>\n";

  }
}
?>