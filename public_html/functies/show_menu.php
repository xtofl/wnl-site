<?php
function show_menu()
{
   /* Build the relative path to the root directory of the website */
   $rootdir = "";
   for ($i=1; $i<substr_count(dirname($_SERVER['PHP_SELF']), "/"); $i++)
   {
     $rootdir = $rootdir."../";
   }

  require_once $rootdir."functies/badm_db.inc.php";
  $badm_db = badm_conn_db();

  echo '<script type="text/javascript" language="JavaScript1.2">'."\n";
  echo '<!--'."\n";
  echo 'stm_bm(["W&L menu",400,"","'.$rootdir.'scripts/blank.gif",0,"","",0,0,250,0,1000,1,0,0]);'."\n";
  echo 'stm_bp("p0",[1,4,0,0,1,4,0,7,100,"progid:DXImageTransform.Microsoft.Fade(overlap=.5,enabled=0,Duration=0.30)",-2,"progid:DXImageTransform.Microsoft.Pixelate(MaxSquare=15,enabled=0,Duration=0.50)",-2,50,0,0,"#669999","#006633","",3,0,0,"#ffffff"]);'."\n";
  echo 'stm_ai("p0i0",[0,"Home","","",-1,-1,0,"'.$rootdir.'index.php","_self","Startpagina","","","",0,0,0,"","",0,0,0,0,1,"#fffff7",0,"#b5bed6",0,"","",3,3,0,0,"#fffff7","#000000","#000000","#000000","10pt Verdana","10pt Verdana",0,0]);'."\n";
  echo 'stm_aix("p0i1","p0i0",[0,"Algemeen","","",-1,-1,0,"","_self","informatie over de club","","","",0,0,0,"'.$rootdir.'scripts/arrow_r.gif","'.$rootdir.'scripts/arrow_r.gif",7,7]);'."\n";
  echo 'stm_bpx("p1","p0",[1,2,0,0,1,4,0]);'."\n";
  echo 'stm_aix("p1i0","p0i0",[0,"Bestuur","","",-1,-1,0,"'.$rootdir.'algemeen/bestuur.php","_self","Bestuursleden","","","",0,0,0,"","",0,0,0,1]);'."\n";
  echo 'stm_aix("p1i1","p0i0",[0,"Medewerkers","","",-1,-1,0,"'.$rootdir.'algemeen/medewerkers.php","_self","Medewerkers","","","",0,0,0,"","",0,0,0,1]);'."\n";
  echo 'stm_aix("p1i2","p0i0",[0,"Speeluren","","",-1,-1,0,"'.$rootdir.'algemeen/speeluren.php","_self","Speeluren","","","",0,0,0,"","",0,0,0,1]);'."\n";
  echo 'stm_aix("p1i3","p0i1",[0,"Geschiedenis","","",-1,-1,0,"","_self","Geschiedenis","","","",0,0,0,"","",0,0,0,1]);'."\n";
  echo 'stm_bpx("p2","p0",[1,2,0,0,1,4,0,0]);'."\n";
  echo 'stm_aix("p2i0","p1i0",[0,"Clubkampioenen","","",-1,-1,0,"'.$rootdir.'algemeen/Geschiedenis/clubkampioenen.php","_self","Winnaars Clubkampioenschap"]);'."\n";
  echo 'stm_aix("p2i1","p1i0",[0,"Nationale Kampioenen","","",-1,-1,0,"'.$rootdir.'algemeen/Geschiedenis/nationalekampioenen.php","_self","Nationale Kampioenen W&L"]);'."\n";
  echo 'stm_ep();'."\n";
  echo 'stm_aix("p1i4","p0i0",[0,"Sponsors","","",-1,-1,0,"'.$rootdir.'algemeen/sponsors.php","_self","Sponsors","","","",0,0,0,"","",0,0,0,1]);'."\n";
  echo 'stm_aix("p1i5","p0i0",[0,"Artikels","","",-1,-1,0,"'.$rootdir.'algemeen/artikels.php","_self","Artikels over de club","","","",0,0,0,"","",0,0,0,1]);'."\n";
  echo 'stm_ep();'."\n";
  echo 'stm_aix("p0i2","p0i0",[0,"Competitie","","",-1,-1,0,"'.$rootdir.'competitie.php","_self","Competitie: ploegen en uitslagen"]);'."\n";
  echo 'stm_aix("p0i3","p0i1",[0,"Tornooien","","",-1,-1,0,"http://www.badmintonsport.be/tornooien/tornooioverzicht.php","_self","Tornooien"]);'."\n";
  echo 'stm_bpx("p1","p0",[1,2,0,0,1,4,0]);'."\n";
  echo 'stm_aix("p1i0","p0i1",[0,"Inschrijvingen","","",-1,-1,0,"","_self","Schrijf je hier in voor tornooien"]);'."\n";
  echo 'stm_bpx("p1","p0",[1,2,0,0,1,4,0,0]);'."\n";
/*************************************************************************************************
 * Maak een link naar elk tornooi waarvoor je kan inschrijven
 ************************************************************************************************/
// create SQL statement 
   $sql = "SELECT id
                , organisatie
                , doelgroep
                , DATE_FORMAT(start_dt, '%d/%m') AS start_dt
                , verzonden
           FROM inschr_torn
		   WHERE DATE_ADD(dt_inschr, INTERVAL 21 DAY ) > NOW()
		   ORDER BY dt_inschr DESC";
   $result = mysql_query ($sql, $badm_db) or badm_mysql_die();
// format results by row
   $teller = 0;
   while ($badm = mysql_fetch_object($result))
   {
      echo 'stm_aix("p2i';
      echo $teller;
      echo '","p1i0",[1,"';
      echo $badm->organisatie;
      if ($badm->doelgroep == 'J' || $badm->doelgroep == 'V')
      {
        echo $badm->doelgroep == 'J' ? ' (Jeugd)' : ' (Veteranen)';
        if ($badm->verzonden == 0) //inschrijvingsperiode nog niet afgelopen
        {
          echo ' <B>'.$badm->start_dt.'</B>';
        }
        else
        {
          echo '  <IMG src='.$rootdir.'images/closed.png>';
        }
        echo '","","",-1,-1,0,"'.$rootdir.'tornooien/inschrijvingen_jeugd.php?id=';
      }
      else
      {
        if ($badm->verzonden == 0) //inschrijvingsperiode nog niet afgelopen
        {
          echo ' <B>'.$badm->start_dt.'</B>';
        }
        else
        {
          echo '  <IMG src='.$rootdir.'images/closed.png>';
        }
        echo '","","",-1,-1,0,"'.$rootdir.'tornooien/inschrijvingen.php?id=';
      }
      echo $badm->id;
      echo '","_self","Inschrijving ';
      echo $badm->organisatie;
      echo '"]);'."\n";
      $teller = $teller + 1;
   }
   echo 'stm_ep();'."\n";
   echo 'stm_aix("p1i1","p0i0",[0,"Aanvangsuren","","",-1,-1,0,"'.$rootdir.'tornooien/aanvangsuren.php","_self","Aanvangsuren tornooien"]);'."\n";
   echo 'stm_aix("p1i2","p0i0",[0,"Kalender","","",-1,-1,0,"'.$rootdir.'docs/tornooikalender.pdf","_blank","Tornooikalender"]);'."\n";
   echo 'stm_aix("p1i3","p0i1",[0,"Resultaten","","",-1,-1,0,"","_self","Tornooiresultaten W&L spelers"]);'."\n";
   echo 'stm_bpx("p2","p0",[1,2,0,0,1,4,0,0]);'."\n";
/*************************************************************************************************
 * Maak een link voor elk seizoen waarvoor er tornooiresultaten zijn
 ************************************************************************************************/
// create SQL statement 
   $sql = "SELECT  DISTINCT 
           IF(DATE_FORMAT(dt_van, '%m') < 8, DATE_FORMAT(dt_van, '%Y') - 1, DATE_FORMAT(dt_van, '%Y')) AS start
          ,IF(DATE_FORMAT(dt_van, '%m') < 8, DATE_FORMAT(dt_van, '%Y'), DATE_FORMAT(dt_van, '%Y') + 1) AS einde
           FROM bad_tornooien
           ORDER  BY start DESC ";
   $result = mysql_query ($sql, $badm_db) or badm_mysql_die();
// format results by row
   $teller = 0;
   while ($badm = mysql_fetch_object($result))
   {
      echo 'stm_aix("p2i';
      echo $teller;
      echo '","p1i0",[0,"Seizoen ';
      echo $badm->start.'-'.$badm->einde;
      echo '","","",-1,-1,0,"'.$rootdir.'tornooien/resultaten.php?s=';
      echo $badm->einde;
      echo '","_self","Seizoen ';
      echo $badm->start.'-'.$badm->einde;
      echo '"]);'."\n";
      $teller = $teller + 1;
   }
   mysql_free_result($result);
   mysql_close($badm_db);
   echo 'stm_ep();'."\n";
   echo 'stm_ep();'."\n";
   echo 'stm_aix("p0i4","p0i0",[0,"Spelers","","",-1,-1,0,"'.$rootdir.'spelers.php","_self","Spelersprofielen en statistieken"]);'."\n";
   echo 'stm_aix("p0i5","p0i1",[0,"Jeugd","","",-1,-1,0,"","_self","Informatie over en voor de jeugdspelers"]);'."\n";
   echo 'stm_bpx("p1","p0",[1,2,0,0,1,4,0]);'."\n";
   echo 'stm_aix("p1i0","p0i0",[0,"Algemeen","","",-1,-1,0,"'.$rootdir.'jeugd/jeugd.php","_self","Jeugd trainers"]);'."\n";
   echo 'stm_aix("p1i1","p0i0",[0,"Teams","","",-1,-1,0,"'.$rootdir.'jeugd/jeugdteams.php","_self","Jeugdteams"]);'."\n";
   echo 'stm_aix("p1i2","p0i0",[0,"Resultaten","","",-1,-1,0,"'.$rootdir.'jeugd/resultaten_jeugd.php?s=2005","_self","Resultaten jeugdtornooien W&L spelers"]);'."\n";
   echo 'stm_aix("p1i3","p0i0",[0,"Jeugdtornooi","","",-1,-1,0,"'.$rootdir.'jeugd/jeugdtornooi.php","_self","Jeugdtornooi W&L"]);'."\n";
   echo 'stm_ep();'."\n";
   echo 'stm_aix("p0i6","p0i0",[0,"Recreanten","","",-1,-1,0,"'.$rootdir.'recreant.php","_self","Recreanten"]);'."\n";
   echo 'stm_aix("p0i7","p0i1",[0,"Foto\'s","","",-1,-1,0,"http://users.pandora.be/ikkegg","_blank","badminton foto\'s"]);'."\n";
/*************************************************************************************************
 * Create a link to every photo directory that exists
 ************************************************************************************************/
/* Get the root dirctory of the website */
  //$rootdir = $_SERVER['DOCUMENT_ROOT'];
/* directory where all the photo's are located in subdirectories */
  $dir = $rootdir."pictures";
/* Get all the subdirectories of the pictures directory */
  include $rootdir."functies/getDirectoryListing.php";
  $subdirs = getDirectoryListing($dir, "a", 1, 0, "none");
/* Number of subdirs in directory */
  $subdir_cnt = count($subdirs);
/* Display a link to every subdirectory */
  for ($i = 0; $i < $subdir_cnt; $i++)
  {
    if ($i == 0) // first subdirectory
    {
       echo 'stm_bpx("p1","p0",[1,2,0,0,1,4,0]);'."\n";
    }
    echo 'stm_aix("p0i'.$i.'","p0i0",[0,"'.$subdirs[$i].'","","",-1,-1,0,"'.$rootdir.'pictures/showThumbnails.php?dir='.$subdirs[$i].'","_self","'.$subdirs[$i].'"]);'."\n";
    if ($i == $subdir_cnt - 1) // last subdirectory
    {
      echo 'stm_ep();'."\n";
    }
  }
  echo 'stm_ai("p0i8",[6,4,"#006633","",0,0,0]);'."\n";
  echo 'stm_aix("p0i9","p0i0",[0,"Links","","",-1,-1,0,"'.$rootdir.'links.php","_self","Links"]);'."\n";
  echo 'stm_aix("p0i10","p0i0",[0,"Message Board","","",-1,-1,0,"'.$rootdir.'messageboard/messageboard.php?p=1&m=n&id=0","_blank","Message Board"]);'."\n";
  echo 'stm_aix("p0i11","p0i0",[0,"Laatste Nieuws","","",-1,-1,0,"'.$rootdir.'laatste_nieuws.php","_self","De laatste wijzigingen aan de W&L website"]);'."\n";
  echo 'stm_ep();'."\n";
  echo 'stm_em();'."\n";
  echo '//-->'."\n";
  echo '</script>'."\n";
}

?>