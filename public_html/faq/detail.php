<?php

  /**
   * page       : detail.php
   * purpose    : Display the FAQ topics of 1) the given category or
   *                                        2) responding to the given search string
   * date       : 20/12/2005 Freek Ceymeulen
   * parameters : id            : categorie_id
   *              search_string : space separated search terms
   *              Only one of the two parameters can be present and one of them must be present
   **/


  function highlight ($text, $terms)
  {
     require_once ('../functies/str_highlight.php');
     return str_highlight($text, $terms, null, "<span style=\"background-color: yellow;\">\\0</span>");
  }
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html><!-- InstanceBegin template="/Templates/templatesub1php.dwt.php" codeOutsideHTMLIsLocked="false" -->
<?php
    // Connect to database
    require_once "/home/badmin/public_html/functies/badm_db.inc.php";
    $badm_db = badm_conn_db();
?>
<head>
<META HTTP-EQUIV="Content-Type"      CONTENT="text/html; charset=iso-8859-1">
<META NAME="robots"                  CONTENT="index,follow">
<META NAME="keywords"                CONTENT="badminton,batminton,w&l,badmintonvereniging,club,badmintonclub,sport,Belgium,Veltem,Herent,Leuven,Winksele,Belgi�" />
<META NAME="description"             CONTENT="W&L bv : De grootste en leukste badmintonclub van Brabant" />
<META NAME="abstract"                CONTENT="badminton, club">
<META NAME="Area"                    CONTENT="General">
<META NAME="Author"                  CONTENT="W&L bv">
<META NAME="Copyright"               CONTENT="(c) 2003-2004 W&L bv">
<META NAME="Creator"                 CONTENT="W&L bv">
<META NAME="Generator"               CONTENT="W&L bv">
<META NAME="Owner"                   CONTENT="W&L bv">
<META NAME="Publisher"               CONTENT="W&L bv">
<META NAME="Rating"                  CONTENT="General">
<META NAME="revisit-after"           CONTENT="7 Days">
<META NAME="revisit"                 CONTENT="7 Days">
<META NAME="document-classification" CONTENT="Internet">
<META NAME="document-type"           CONTENT="Public">
<META NAME="document-rating"         CONTENT="Safe for Kids">
<META NAME="document-distribution"   CONTENT="Global">
<META HTTP-EQUIV="Content-Type"      CONTENT="text/html; charset=iso-8859-1" />
<META HTTP-EQUIV="Audience"          CONTENT="General">
<META HTTP-EQUIV="content-language"  CONTENT="NL">
<META HTTP-EQUIV="Pragma"            CONTENT="no-cache">
<META HTTP-EQUIV="Expires"           CONTENT="now">
<!-- InstanceBeginEditable name="doctitle" -->
<title>W&amp;L badmintonvereniging - Frequently Asked Questions</title>
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="head" -->


<style type="text/css">
<!--
dt {
    background-color : #CCCCCC;
    color : #0000FF;
    font-family : Verdana, Arial, Helvetica, sans-serif;
    font-size : small;
    font-weight : bold;
    margin : 5px;
    padding : 10px;
}
dd {
    font-family : Verdana, Arial, Helvetica, sans-serif;
    font-size : small;
}
-->
</style>

<script language="javascript" type="text/javascript">
<!--
function show_all()
{
  var tables = document.getElementsByTagName('dd');
  for(var I=0;I<tables.length;I++)
  {
    if (tables[I].getAttribute('id') != '')
      tables[I].style.display = '';
  }
}
function hide_all()
{
  var tables = document.getElementsByTagName('dd');
  for(var I=0;I<tables.length;I++)
  {
    if (tables[I].getAttribute('id') != '')
      tables[I].style.display = 'none';
  }
}
function showhide()
{
  for(var I=0;I<arguments.length;I++)
  {
    var obj=document.getElementById(arguments[I]);
    if (obj.style.display == '')
      obj.style.display = 'none';
    else
      obj.style.display = '';
  }
}
var mode = 'hide';
function showhide_all(obj)
{
  if (mode == 'hide')
  {
    show_all();
    mode = 'show';
    obj.value = 'Toon alleen de vragen';
  }
  else
  {
    hide_all();
    mode = 'hide';
    obj.value = 'Toon alle antwoorden';
  }
}
function showLink(element)
{
  element.style.cursor='pointer';
  element.style.textDecoration='underline';
}
function hideLink(element)
{
  element.style.textDecoration='none';
}
-->
</script>
<!-- InstanceEndEditable -->
<link href="../css/badminton.css" rel="stylesheet" type="text/css">
<script type="text/javascript" language="JavaScript" src="http://www.badmintonsport.be/scripts/stm31.js"></script>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" class="badminton">
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <!--DWLayoutTable-->
 <tr>
  <td height="95" colspan="2" valign="top">
   <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <!--DWLayoutTable-->
    <tr>          
     <td width="100%" height="95">
      <table width="100%" border="0" cellpadding="0" cellspacing="0" bordercolor="#CCCCCC" bgcolor="#FFFFFF">
              <!--DWLayoutTable-->
       <tr> 
        <td width="100%" align="center" valign="middle" bgcolor="#006600"><img src="http://www.badmintonsport.be/images/spacer.gif" width="200" height="4"></td>
       </tr>
       <tr> 
        <td align="center" valign="middle" bgcolor="#CCCCCC"><img src="http://www.badmintonsport.be/images/pt_transp.gif" width="500" height="5"></td>
       </tr>
       <tr> 
        <td align="center" valign="top" bgcolor="#CCCCCC">
         <table width="100%" border="0" cellspacing="0" cellpadding="3">
          <tr align="center" valign="middle"> 
           <td rowspan="2" align="left"><img src="http://www.badmintonsport.be/images/logow&l-100.gif" width="100" height="80"></td>
           <td>&nbsp;</td>
           <td><h4>W&amp;L Badmintonvereniging vzw, Mechelsesteenweg 
                         387, 3020 Herent</h4></td>
          </tr>
          <tr align="center" valign="middle"> 
           <td>&nbsp;</td>
           <td><img src="http://www.badmintonsport.be/images/chinas.gif" width="400" height="60"></td>
          </tr>
         </table>
        </td>
       </tr>
       <tr> 
        <td align="center" valign="middle" bgcolor="#CCCCCC"><img src="http://www.badmintonsport.be/images/pt_transp.gif" width="500" height="5"></td>
       </tr>
       <tr> 
        <td align="center" valign="middle" bgcolor="#336600"><img src="http://www.badmintonsport.be/images/spacer.gif" width="200" height="4"></td>
       </tr>
      </table>
     </td>
    </tr>
   </table>
  </td>
 </tr>
 <tr>
  <td width="150" height="213" valign="top">
<script type="text/javascript" language="JavaScript1.2">
<!--
stm_bm(["W&L menu",400,"","http://www.badmintonsport.be/scripts/blank.gif",0,"","",0,0,250,0,1000,1,0,0]);
stm_bp("p0",[1,4,0,0,1,4,0,7,100,"progid:DXImageTransform.Microsoft.Fade(overlap=.5,enabled=0,Duration=0.30)",-2,"progid:DXImageTransform.Microsoft.Pixelate(MaxSquare=15,enabled=0,Duration=0.50)",-2,50,0,0,"#669999","#006633","",3,0,0,"#ffffff"]);
stm_ai("p0i0",[0,"Home","","",-1,-1,0,"http://www.badmintonsport.be/index.php","_self","Startpagina","","","",0,0,0,"","",0,0,0,0,1,"#fffff7",0,"#b5bed6",0,"","",3,3,0,0,"#fffff7","#000000","#000000","#000000","10pt Verdana","10pt Verdana",0,0]);
stm_aix("p0i1","p0i0",[0,"Algemeen","","",-1,-1,0,"","_self","informatie over de club","","","",0,0,0,"http://www.badmintonsport.be/scripts/arrow_r.gif","http://www.badmintonsport.be/scripts/arrow_r.gif",7,7]);
stm_bpx("p1","p0",[1,2,0,0,1,4,0]);
stm_aix("p1i0","p0i0",[0,"Bestuur","","",-1,-1,0,"http://www.badmintonsport.be/algemeen/bestuur.php","_self","Bestuursleden","","","",0,0,0,"","",0,0,0,1]);
stm_bpx("p1","p0",[1,2,0,0,1,4,0,0]);
stm_aix("p2i0","p1i0",[0,"Mededelingen","","",-1,-1,0,"http://www.badmintonsport.be/algemeen/Bestuur/bestuursmededelingen.php","_self","Bestuursmededelingen"]);
stm_ep();
stm_aix("p1i1","p0i0",[0,"Medewerkers","","",-1,-1,0,"http://www.badmintonsport.be/algemeen/medewerkers.php","_self","Medewerkers","","","",0,0,0,"","",0,0,0,1]);
stm_aix("p1i2","p0i0",[0,"Speeluren","","",-1,-1,0,"http://www.badmintonsport.be/algemeen/speeluren.php","_self","Speeluren","","","",0,0,0,"","",0,0,0,1]);
stm_aix("p1i3","p0i1",[0,"Geschiedenis","","",-1,-1,0,"","_self","Geschiedenis","","","",0,0,0,"","",0,0,0,1]);
stm_bpx("p2","p0",[1,2,0,0,1,4,0,0]);
stm_aix("p2i0","p1i0",[0,"Clubkampioenen","","",-1,-1,0,"http://www.badmintonsport.be/algemeen/Geschiedenis/clubkampioenen.php","_self","Winnaars Clubkampioenschap"]);
stm_aix("p2i1","p1i0",[0,"Nationale Kampioenen","","",-1,-1,0,"http://www.badmintonsport.be/algemeen/Geschiedenis/nationalekampioenen.php","_self","Nationale Kampioenen W&L"]);
stm_ep();
stm_aix("p1i4","p0i0",[0,"Sponsors","","",-1,-1,0,"http://www.badmintonsport.be/algemeen/sponsors.php","_self","Sponsors","","","",0,0,0,"","",0,0,0,1]);
stm_aix("p1i5","p0i0",[0,"Artikels","","",-1,-1,0,"http://www.badmintonsport.be/algemeen/artikels.php","_self","Artikels over de club","","","",0,0,0,"","",0,0,0,1]);
stm_ep();
stm_aix("p0i2","p0i0",[0,"Competitie","","",-1,-1,0,"http://www.badmintonsport.be/competitie.php","_self","Competitie: ploegen en uitslagen"]);
stm_aix("p0i3","p0i1",[0,"Tornooien","","",-1,-1,0,"","Tornooien"]);
stm_bpx("p1","p0",[1,2,0,0,1,4,0]);
stm_aix("p1i0","p0i1",[0,"Inschrijvingen","","",-1,-1,0,"_self","Schrijf je hier in voor tornooien"]);
stm_bpx("p1","p0",[1,2,0,0,1,4,0,0]);
//-->
</script>
<?php
// create SQL statement 
   $sql = "SELECT id
                , organisatie
                , doelgroep
                , DATE_FORMAT(start_dt, '%d/%m') AS start_dt
                , verzonden
           FROM inschr_torn
		   WHERE DATE_ADD(dt_inschr, INTERVAL 21 DAY ) > NOW()
		   ORDER BY dt_inschr DESC";
   $result = mysql_query ($sql, $badm_db) or badm_mysql_die();
// format results by row
   $teller = 0;
   echo "<script type=\"text/javascript\" language=\"JavaScript1.2\">";
   while ($badm = mysql_fetch_object($result))
   {
      echo "stm_aix(\"p2i";
      echo $teller;
      echo "\",\"p1i0\",[1,\"";
      echo $badm->organisatie;
      if ($badm->doelgroep == 'J' || $badm->doelgroep == 'V')
      {
        echo $badm->doelgroep == 'J' ? " (Jeugd)" : " (Veteranen)";
        if ($badm->verzonden == 0) //inschrijvingsperiode nog niet afgelopen
        {
          echo " <B>".$badm->start_dt."</B>";
        }
        else
        {
          echo "  <IMG src=http://www.badmintonsport.be/images/closed.png>";
        }
        echo "\",\"\",\"\",-1,-1,0,\"http://www.badmintonsport.be/tornooien/inschrijvingen_jeugd.php?id=";
      }
      else
      {
        if ($badm->verzonden == 0) //inschrijvingsperiode nog niet afgelopen
        {
          echo " <B>".$badm->start_dt."</B>";
        }
        else
        {
          echo "  <IMG src=http://www.badmintonsport.be/images/closed.png>";
        }
        echo "\",\"\",\"\",-1,-1,0,\"http://www.badmintonsport.be/tornooien/inschrijvingen.php?id=";
      }
      echo $badm->id;
      echo "\",\"_self\",\"Inschrijving ";
      echo $badm->organisatie;
      echo "\"]);";
      $teller = $teller + 1;
   }
   echo"</script>";
?>
<script type="text/javascript" language="JavaScript1.2">
<!--
stm_ep();
stm_aix("p1i1","p0i0",[0,"Aanvangsuren","","",-1,-1,0,"http://www.badmintonsport.be/tornooien/aanvangsuren.php","_self","Aanvangsuren tornooien"]);
stm_aix("p1i2","p0i0",[0,"Kalender","","",-1,-1,0,"http://www.badmintonsport.be/docs/tornooikalender20052006.pdf","_blank","Tornooikalender seizoen 2005 - 2006"]);
stm_aix("p1i3","p0i1",[0,"Resultaten","","",-1,-1,0,"","_self","Tornooiresultaten W&L spelers"]);
stm_bpx("p2","p0",[1,2,0,0,1,4,0,0]);
//--></script>
<?php
// create SQL statement
    $sql = "SELECT  DISTINCT IF(DATE_FORMAT(dt_van, '%m') < 8
, DATE_FORMAT(dt_van, '%Y') - 1
, DATE_FORMAT(dt_van, '%Y')) AS start
, IF(DATE_FORMAT(dt_van, '%m') < 8
, DATE_FORMAT(dt_van, '%Y')
, DATE_FORMAT(dt_van, '%Y') + 1) AS einde
           FROM bad_tornooien
           ORDER  BY start DESC ";
   $result = mysql_query ($sql, $badm_db) or badm_mysql_die();
// format results by row
   $teller = 0;
   echo"<script type=\"text/javascript\" language=\"JavaScript\">";
   while ($badm = mysql_fetch_object($result))
   {
      echo "stm_aix(\"p2i".$teller."\",\"p1i0\",[0,\"Seizoen ".$badm->start."-".$badm->einde."\",\"\",\"\",-1,-1,0,\"http://www.badmintonsport.be/tornooien/resultaten.php?s=".$badm->einde."\",\"_self\",\"Seizoen ".$badm->start."-".$badm->einde."\"]);";
      $teller = $teller + 1;
   }
   echo"</script>";
   //mysql_free_result($result);
   mysql_close($badm_db);
?>
<script type="text/javascript" language="JavaScript">
<!--stm_ep();
stm_ep();
stm_ep();
stm_aix("p0i4","p0i0",[0,"Spelers","","",-1,-1,0,"http://www.badmintonsport.be/spelers.php","_self","Spelersprofielen en statistieken"]);
stm_aix("p0i5","p0i1",[0,"Jeugd","","",-1,-1,0,"","_self","Informatie over en voor de jeugdspelers"]);
stm_bpx("p1","p0",[1,2,0,0,1,4,0]);
stm_aix("p1i0","p0i0",[0,"Algemeen","","",-1,-1,0,"http://www.badmintonsport.be/jeugd/jeugd.php","_self","Jeugd trainers"]);
stm_aix("p1i1","p0i0",[0,"Teams","","",-1,-1,0,"http://www.badmintonsport.be/jeugd/jeugdteams.php","_self","Jeugdteams"]);
stm_aix("p1i2","p0i0",[0,"J-team","","",-1,-1,0,"http://www.badmintonsport.be/jeugd/J-team.php","_self","J-team"]);
stm_aix("p1i3","p0i0",[0,"Resultaten","","",-1,-1,0,"http://www.badmintonsport.be/jeugd/resultaten_jeugd.php?s=2006","_self","Resultaten jeugtornooien W&L spelers"]);
stm_aix("p1i4","p0i0",[0,"Jeugdtornooi","","",-1,-1,0,"http://www.badmintonsport.be/jeugd/jeugdtornooi.php","_self","Jeugdtornooi W&L"]);
stm_ep();
stm_aix("p0i6","p0i0",[0,"Recreanten","","",-1,-1,0,"http://www.badmintonsport.be/recreant.php","_self","Recreanten"]);
stm_aix("p0i7","p0i1",[0,"Foto's","","",-1,-1,0,"http://users.pandora.be/ikkegg","_blank","badminton foto's"]);
//-->
</script>
<?php
/*************************************************************************************************
 * Create a link to every photo directory that exists
 ************************************************************************************************/
/* Get the root dirctory of the website */
  $rootdir = $_SERVER['DOCUMENT_ROOT'];
/* directory where all the photo's are located in subdirectories */
  $dir = $rootdir."/pictures";
/* Get all the subdirectories of the pictures directory */
  include $rootdir."/functies/getDirectoryListing.php";
  $subdirs = getDirectoryListing($dir, "a", 1, 0, "none");
/* Number of subdirs in directory */
  $subdir_cnt = count($subdirs);
/* Display a link to every subdirectory */
  for ($i = 0; $i < $subdir_cnt; $i++)
  {
    echo"<script type=\"text/javascript\" language=\"JavaScript\">";
    if ($i == 0) // first subdirectory
    {
       echo "stm_bpx(\"p1\",\"p0\",[1,2,0,0,1,4,0]);\n";
    }
		echo "stm_aix(\"p0i".$i."\",\"p0i0\",[0,\"".$subdirs[$i]."\",\"\",\"\",-1,-1,0,\"http://www.badmintonsport.be/pictures/showThumbnails.php?dir=".$subdirs[$i]."\",\"_self\",\"".$subdirs[$i]."\"]);\n";
    if ($i == $subdir_cnt - 1) // last subdirectory
    {
      echo "stm_ep();\n";
    }
    echo"</script>";
  }
?>
<script type="text/javascript" language="JavaScript">
<!--
stm_ai("p0i8",[6,4,"#006633","",0,0,0]);
stm_aix("p0i9","p0i0",[0,"Links","","",-1,-1,0,"http://www.badmintonsport.be/links.php","_self","Links"]);
stm_aix("p0i10","p0i0",[0,"Message Board","","",-1,-1,0,"http://www.badmintonsport.be/messageboard/messageboard.php?p=1&m=n&id=0","_blank","Message Board"]);
stm_aix("p0i11","p0i0",[0,"Laatste Nieuws","","",-1,-1,0,"http://www.badmintonsport.be/laatste_nieuws.php","_self","De laatste wijzigingen aan de W&L website"]);
stm_ep();
stm_em();
//--></script>
      &nbsp;
 <!-- InstanceBeginEditable name="counter" -->
<!-- InstanceEndEditable -->
       <!-- InstanceBeginEditable name="wiens verjaardag is het" -->
<!-- InstanceEndEditable -->
  </td>
  <td width="100%" valign="top">
<!-- InstanceBeginEditable name="general" -->

<center>
<?php
   // Connect to DB
   require_once "../functies/badm_db.inc.php";
   $badm_db = badm_conn_db();
   
   // Handle parameters
   $categorie_id = '';
   if (isset($_GET['id']))
   {
	   $categorie_id = $_GET['id'];
	   // Bepaal de naam van de categorie
	   $query = "SELECT title FROM faq_categories WHERE id = %d";
	   $sql = sprintf($query, mysql_real_escape_string($categorie_id));
	   $result = mysql_query($sql, $badm_db) or badm_mysql_die();
	   if (mysql_num_rows($result) == 0)
           {
                   $category = "Onbekende categorie";
           }
           $row = mysql_fetch_object($result);
           $category = $row->title;
           mysql_free_result($result);
	   // Bouw de WHERE-clause
	   $where = " WHERE categorie_id = ".mysql_real_escape_string($categorie_id);
   }
   $search_string = '';
   if (isset($_POST['search_string']))
   {
       $category = "Gevonden resultaten";
	   $search_string = $_POST['search_string'];
	   // Eerst gaan we de search string opsplitsen in quoted strings en gewone strings
	   $search_string = trim($search_string);
	   $quote_open = FALSE;
	   $start = 0;
	   while (($pos = strpos($search_string, '"', $start)) !== FALSE)
	   {
  	   	if ($quote_open)
  	   	{
    	   		$quote_open = FALSE;
    	   		$quoted_string[] = trim(substr($search_string, $start, $pos - $start - 1));
  	   	}
  	   	else
  	   	{
    	   		$quote_open = TRUE;
    	   		$string[] = trim(substr($search_string, $start, $pos - $start - 1));
  	   	}
  	   	$start = $pos + 1;
	   }
	   if ($start == 0)
	   {
  	   	// Geen quote gevonden
  	   	$string[] = $search_string;
	   }
	   else if ($start != strlen($search_string) - 1)
	   {
  	   	// Laatste stuk
  	   	$string[] = trim(substr($search_string, $start));
	   }
	   
	   // Bouw de WHERE-clause
	   $where = "";
	   // De quoted strings
	   for ($i=0; $i < count($quoted_string); $i++)
	   {
		   $where .= " AND (titel LIKE '%".mysql_real_escape_string($quoted_string[$i])."%'";
		   $where .= " OR tekst LIKE '%".mysql_real_escape_string($quoted_string[$i])."%')";
	   }
	   $strings = array();
	   // De non quoted strings
	   for ($i=0; $i < count($string); $i++)
	   {
		   // Splits elke string in afzonderlijke woorden
		   $temp = explode(" ", $string[$i]);
		   $strings = array_merge($strings, $temp);
	   }
	   for ($i=0; $i < count($strings); $i++)
	   {
		   if (substr($strings[$i], 0, 1) == "-")
		   {
			   // Negatie
			   $where .= " AND NOT (titel LIKE '%".mysql_real_escape_string(substr($strings[$i], 1))."%'";
		           $where .= " OR tekst LIKE '%".mysql_real_escape_string(substr($strings[$i], 1))."%')";
	           }
	           else
	           {
		           $where .= " AND (titel LIKE '%".mysql_real_escape_string($strings[$i])."%'";
		           $where .= " OR tekst LIKE '%".mysql_real_escape_string($strings[$i])."%')";
	           }
	   }
	   $where = " WHERE".substr($where, 4);
	   $zoektermen = array_merge($strings, $quoted_string);

       // Sla de zoekterm(en) op in audit tabel
       $audit = "INSERT INTO admin_audit (user_id, actie, id, inhoud)
                 VALUES ('PUBLIC', 'QUERY', 0, '%s')";
       $sql  = sprintf($audit, "Zoektermen voor FAQ: ".mysql_real_escape_string($search_string));
       $result = mysql_query($sql, $badm_db) or die("Invalid query: " . mysql_error());
   }
?>
<span style="float: right;"><a href="overzicht.php">Terug naar overzicht</a>&nbsp;</span><h1>FAQ : <?=$category; ?></h1>
<input type="button" style="background-color: white; border-style: none; color: blue; text-decoration: underline;" onClick="showhide_all(this);"  onMouseOver="showLink(this);" onMouseOut="hideLink(this);" value="Toon alle antwoorden">
</center>

<dl>
<?php
   // Zoek de gevraagde topics
   $sql = "SELECT id
                , titel
                , tekst
             FROM faq_topics".$where;

   $result = mysql_query($sql, $badm_db) or badm_mysql_die();
   while($row = mysql_fetch_assoc($result))
   {
     if ($categorie_id == '')
     {
       // Markeer de zoektermen
       echo " <dt onClick=\"showhide('".$row['id']."');\" onMouseOver=\"showLink(this);\" onMouseOut=\"hideLink(this);\">".highlight($row['titel'], $zoektermen)."</dt>\n";
       echo " <dd id=\"".$row['id']."\">".highlight($row['tekst'], $zoektermen)."</dt>\n";
     }
     else
     {
       echo " <dt onClick=\"showhide('".$row['id']."');\" onMouseOver=\"showLink(this);\" onMouseOut=\"hideLink(this);\">".highlight($row['titel'], $zoektermen)."</dt>\n";
       echo " <dd id=\"".$row['id']."\">".highlight($row['tekst'], $zoektermen)."</dt>\n";
     }
   }
   if (mysql_num_rows($result) == 0)
   {
     echo "<dt>Er zijn geen resultaten die aan je zoekopdracht voldoen.</dt>";
   }
   mysql_free_result($result);
?>
</dl>

<!-- InstanceEndEditable -->
  </td>
 </tr>
 <tr>
  <td height="0"></td>
  <td></td>
 </tr>
 <tr>
  <td height="1"><img src="http://www.badmintonsport.be/images/spacer.gif" alt="" width="150" height="1"></td>
  <td></td>
 </tr>
</table>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <!--DWLayoutTable-->
 <tr>
  <td width="100%" height="50" valign="top" bgcolor="#FFFFFF">
   <table width="100%" border="0" cellpadding="0" cellspacing="0" bordercolor="#CCCCCC" bgcolor="#CCCCCC">
       <!--DWLayoutTable-->
    <tr>
     <td align="center" valign="middle" bordercolor="#CCCCCC" bgcolor="#006600">
         <img src="http://www.badmintonsport.be/images/spacer.gif" width="200" height="4"></td>
    </tr>
    <tr>
     <td width="100%" align="center" valign="middle" bordercolor="#CCCCCC" bgcolor="#CCCCCC" class="gmenu">Last change:
 <!-- InstanceBeginEditable name="datum" -->
 <!-- InstanceEndEditable -->
             E-mail: <a href="mailto:webmaster@badmintonsport.be">webmaster@badmintonsport.be</a></td>
    </tr>
    <tr>
     <td align="center" valign="middle" bordercolor="#CCCCCC" bgcolor="#006600" class="gmenu">
          <img src="http://www.badmintonsport.be/images/spacer.gif" width="200" height="4"></td>
    </tr>
   </table>
  </td>
 </tr>
</table>
</body>
<!-- InstanceEnd --></html>