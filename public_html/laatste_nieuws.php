<?php
  require_once "functies/website_usage.php";
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html lang="nl"><!-- InstanceBegin template="/Templates/templaterootphp.dwt.php" codeOutsideHTMLIsLocked="false" -->
<?php
   require_once "functies/badm_db.inc.php";
   $badm_db = badm_conn_db();
?>
<head>
<META NAME="robots"                  CONTENT="index,follow">
<META NAME="keywords"                CONTENT="badminton,batminton,w&amp;l,badmintonvereniging,club,badmintonclub,sport,Belgium,Veltem,Herent,Leuven,Winksele,Belgi�">
<META NAME="description"             CONTENT="W&amp;L bv : De grootste en leukste badmintonclub van Brabant">
<META NAME="abstract"                CONTENT="badminton, club">
<META NAME="Area"                    CONTENT="General">
<META NAME="Author"                  CONTENT="W&amp;L bv">
<META NAME="Copyright"               CONTENT="(c) 2003-2004 W&amp;L bv">
<META NAME="Creator"                 CONTENT="W&amp;L bv">
<META NAME="Generator"               CONTENT="W&amp;L bv">
<META NAME="Owner"                   CONTENT="W&amp;L bv">
<META NAME="Publisher"               CONTENT="W&amp;L bv">
<META NAME="Rating"                  CONTENT="General">
<META NAME="revisit-after"           CONTENT="7 Days">
<META NAME="revisit"                 CONTENT="7 Days">
<META NAME="document-classification" CONTENT="Internet">
<META NAME="document-type"           CONTENT="Public">
<META NAME="document-rating"         CONTENT="Safe for Kids">
<META NAME="document-distribution"   CONTENT="Global">
<META HTTP-EQUIV="Content-Type"      CONTENT="text/html; charset=iso-8859-1">
<META HTTP-EQUIV="Audience"          CONTENT="General">
<META HTTP-EQUIV="content-language"  CONTENT="NL">
<META HTTP-EQUIV="Pragma"            CONTENT="no-cache">
<META HTTP-EQUIV="Expires"           CONTENT="now">
<!-- InstanceBeginEditable name="doctitle" -->
<title>W&amp;L bv Laatste Nieuws</title>
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="head" -->


<style type="text/css">
<!--
a {
    color : #800000;
    text-decoration : none
}
a:hover {
    color : #800000;
    text-decoration : underline;
}
a:visited {
    color : #006600;
}
div.item {
    border-style : solid;
    border-color : #006600;
    border-width : 1px;
    margin : 10px;
    padding : 10px;
}
div.title {
    background-color : #EFEFEF;
    padding-bottom : 10px;
}
div.description {
    background-color : #EFEFEF;
    font-family : Verdana, Arial, Helvetica, sans-serif;
    font-size : small;
}
span.pubdate {
    color : black;
    font-size : x-small;
    float : right;
}
span.url {
    font-family : Georgia, "Times New Roman", Times, serif;
    font-weight : bold;
}
-->
</style>
<!-- InstanceEndEditable -->
<link href="http://www.badmintonsport.be/css/badminton.css" rel="stylesheet" type="text/css">
<script type="text/javascript" language="JavaScript1.2" src="http://www.badmintonsport.be/scripts/stm31.js"></script>
<script type="text/javascript" language="JavaScript" src="http://www.badmintonsport.be/scripts/dw_rotator.js"></script>
<script type="text/javascript" language="JavaScript">
<!--
function initRotator()
{
  var rotator1 = new dw_Rotator("sponsors_banner", 4000, "http://www.badmintonsport.be/images/", "_blank");
  rotator1.addImages("Veltem_Motors_banner.png", "banner_verhoeven.jpg");
  //rotator1.addImages("Veltem_Motors_banner.png");
  rotator1.addActions("http://www.veltem-motors.be", "http://www.isolatieverhoeven.be");
  //rotator1.addActions("http://www.veltem-motors.be");
  dw_Rotator.start();
}
function doLoad()
{
  // initialize the menu
  st_onload();
  // initialize the banner rotator
  initRotator();
}
//-->
</script>
</head>
<body class="badminton" onload="doLoad();">
<table width="100%" border="0" cellpadding="0" cellspacing="0">
 <tr>
  <td colspan="2">
   <table width="100%" border="0" cellpadding="0" cellspacing="0">
    <!--DWLayoutTable-->
    <tr> 
     <td width="100%" style="background-color: #006600;"><img src="http://www.badmintonsport.be/images/pt_transp.gif" width="1px" height="4px" alt=""></td>
    </tr>
    <tr> 
     <td align="center" valign="top" bgcolor="#CCCCCC">
      <table width="100%" border="0" cellspacing="0" cellpadding="3">
       <tr align="center" valign="middle"> 
        <td rowspan="2" align="left">
	     <img src="http://www.badmintonsport.be/images/logow&amp;l-100.gif" width="100" height="80" alt="logo">
	    </td>
        <td>
	     <h4>W&amp;L Badmintonvereniging vzw<br>
	         Mechelsesteenweg 387<br>
             3020 Herent</h4>
		</td>
        <td align="right"><a href="javascript:void();" onclick="return dw_Rotator.doClick(0)"  onmouseover="dw_Rotator.pause(0)" onmouseout="dw_Rotator.resume(0)" onfocus="this.blur()"><img id="sponsors_banner" name="sponsors_banner" src="images/Veltem_Motors_banner.png" alt="sponsors" height="100" width="320" border="0"></a></td>
       </tr>
      </table>
	 </td>
    </tr>
    <tr> 
     <td bgcolor="#336600"><img src="http://www.badmintonsport.be/images/pt_transp.gif" width="1px" height="4px" alt=""></td>
    </tr>
   </table>
  </td>
 </tr>
 <tr> 
    <td width="150" valign="top">
<!-- InstanceBeginEditable name="Floating Menu" -->
<!-- InstanceEndEditable --> 
      <script type="text/javascript" language="JavaScript1.2">
<!--
stm_bm(["W&L menu",400,"","http://www.badmintonsport.be/scripts/blank.gif",0,"","",0,0,250,0,1000,1,0,0]);
stm_bp("p0",[1,4,0,0,1,4,0,7,100,"progid:DXImageTransform.Microsoft.Fade(overlap=.5,enabled=0,Duration=0.30)",-2,"progid:DXImageTransform.Microsoft.Pixelate(MaxSquare=15,enabled=0,Duration=0.50)",-2,50,0,0,"#669999","#006633","",3,0,0,"#ffffff"]);
stm_ai("p0i0",[0,"Home","","",-1,-1,0,"index.php","_self","Startpagina","","","",0,0,0,"","",0,0,0,0,1,"#fffff7",0,"#b5bed6",0,"","",3,3,0,0,"#fffff7","#000000","#000000","#000000","10pt Verdana","10pt Verdana",0,0]);
stm_aix("p0i1","p0i0",[0,"Algemeen","","",-1,-1,0,"","_self","informatie over de club","","","",0,0,0,"http://www.badmintonsport.be/scripts/arrow_r.gif","http://www.badmintonsport.be/scripts/arrow_r.gif",7,7]);
stm_bpx("p1","p0",[1,2,0,0,1,4,0]);
stm_aix("p1i0","p0i0",[0,"Bestuur","","",-1,-1,0,"/algemeen/bestuur.php","_self","Bestuursleden","","","",0,0,0,"","",0,0,0,1]);
stm_bpx("p1","p0",[1,2,0,0,1,4,0,0]);
stm_aix("p2i0","p1i0",[0,"Mededelingen","","",-1,-1,0,"/algemeen/Bestuur/bestuursmededelingen.php","_self","Bestuursmededelingen"]);
stm_ep();
stm_aix("p1i1","p0i0",[0,"Medewerkers","","",-1,-1,0,"/algemeen/medewerkers.php","_self","Medewerkers","","","",0,0,0,"","",0,0,0,1]);
stm_aix("p1i2","p0i0",[0,"Speeluren","","",-1,-1,0,"/algemeen/speeluren.php","_self","Speeluren","","","",0,0,0,"","",0,0,0,1]);
stm_aix("p1i3","p0i1",[0,"Geschiedenis","","",-1,-1,0,"","_self","Geschiedenis","","","",0,0,0,"","",0,0,0,1]);
stm_bpx("p2","p0",[1,2,0,0,1,4,0,0]);
stm_aix("p2i0","p1i0",[0,"Clubkampioenen","","",-1,-1,0,"/algemeen/Geschiedenis/clubkampioenen.php","_self","Winnaars Clubkampioenschap"]);
stm_aix("p2i1","p1i0",[0,"Nationale Kampioenen","","",-1,-1,0,"/algemeen/Geschiedenis/nationalekampioenen.php","_self","Nationale Kampioenen W&L"]);
stm_ep();
stm_aix("p1i4","p0i0",[0,"Sponsors","","",-1,-1,0,"/algemeen/sponsors.php","_self","Sponsors","","","",0,0,0,"","",0,0,0,1]);
stm_aix("p1i5","p0i0",[0,"Artikels","","",-1,-1,0,"/algemeen/artikels.php","_self","Artikels","","","",0,0,0,"","",0,0,0,1]);
stm_ep();
stm_aix("p0i2","p0i0",[0,"Competitie","","",-1,-1,0,"/competitie.php","_self","Competitie: ploegen en uitslagen"]);
stm_aix("p0i3","p0i1",[0,"Tornooien","","",-1,-1,0,"/tornooien/tornooioverzicht.php","_self","Tornooien"]);
stm_bpx("p1","p0",[1,2,0,0,1,4,0]);
stm_aix("p1i0","p0i1",[0,"Inschrijvingen","","",-1,-1,0,"","_self","Inschrijvingen","Schrijf je hier in voor tornooien"]);
stm_bpx("p1","p0",[1,2,0,0,1,4,0,0]);
//-->
</script>
<?php
// create SQL statement 
   $sql = "SELECT id
                , organisatie
                , doelgroep
                , DATE_FORMAT(start_dt, '%d/%m') AS start_dt
                , verzonden
           FROM inschr_torn
		   WHERE DATE_ADD(dt_inschr, INTERVAL 21 DAY ) > NOW()
		   ORDER BY dt_inschr DESC";
   $result = mysql_query ($sql, $badm_db) or badm_mysql_die();
// format results by row
   $teller = 0;
   echo"<script type=\"text/javascript\" language=\"JavaScript1.2\">";
   while ($badm = mysql_fetch_object($result))
   {
      echo "stm_aix(\"p2i";
      echo $teller;
      echo "\",\"p1i0\",[1,\"";
      echo $badm->organisatie;
      if ($badm->doelgroep == 'J' || $badm->doelgroep == 'V')
      {
        echo $badm->doelgroep == 'J' ? " (Jeugd)" : " (Veteranen)";
        if ($badm->verzonden == 0) //inschrijvingsperiode nog niet afgelopen
        {
          echo " <B>".$badm->start_dt."</B>";
        }
        else
        {
          echo "  <IMG src=/images/closed.png>";
        }
      }
      else
      {
       if ($badm->verzonden == 0) //inschrijvingsperiode nog niet afgelopen
        {
          echo " <B>".$badm->start_dt."</B>";
        }
        else
        {
          echo "  <IMG src=/images/closed.png>";
        }
      }
      echo "\",\"\",\"\",-1,-1,0,\"/tornooien/inschrijvingen.php?id=";
      echo $badm->id;
      echo "\",\"_self\",\"Inschrijving ";
      echo $badm->organisatie;
      echo "\"]);\n";
      $teller = $teller + 1;
   }
   echo"</script>\n";
?>
<script type="text/javascript" language="JavaScript1.2">
<!--
stm_ep();
stm_aix("p1i1","p0i0",[0,"Aanvangsuren","","",-1,-1,0,"/tornooien/aanvangsuren.php","_self","Aanvangsuren tornooien"]);
stm_aix("p1i2","p0i0",[0,"Kalender","","",-1,-1,0,"http://www.badmintonsport.be/docs/tornooikalender.pdf","_blank","Tornooikalender"]);
stm_aix("p1i3","p0i1",[0,"Resultaten","","",-1,-1,0,"","_self","Tornooiresultaten W&L spelers"]);
stm_bpx("p2","p0",[1,2,0,0,1,4,0,0]);
//-->
</script>
<?php
// create SQL statement   
 $sql = "SELECT  DISTINCT 
IF(DATE_FORMAT(dt_van, '%m') < 8, DATE_FORMAT(dt_van, '%Y') - 1, DATE_FORMAT(dt_van, '%Y')) AS start
,IF(DATE_FORMAT(dt_van, '%m') < 8, DATE_FORMAT(dt_van, '%Y'), DATE_FORMAT(dt_van, '%Y') + 1) AS einde
           FROM bad_tornooien
           ORDER  BY start DESC ";
   $result = mysql_query ($sql, $badm_db) or badm_mysql_die();
// format results by row
   $teller = 0;
   echo"<script type=\"text/javascript\" language=\"JavaScript1.2\">";
   while ($badm = mysql_fetch_object($result))
   {
       echo "stm_aix(\"p2i";
      echo $teller;
      echo "\",\"p1i0\",[0,\"Seizoen ";
      echo $badm->start.'-'.$badm->einde;
      echo "\",\"\",\"\",-1,-1,0,\"/tornooien/resultaten.php?s=";
      echo $badm->einde;
      echo "&view_id=22\",\"_self\",\"Seizoen ";
      echo $badm->start.'-'.$badm->einde;
      echo "\"]);\n";
      $teller = $teller + 1;
   }
   echo"</script>\n";
   //mysql_free_result($result);
   mysql_close($badm_db);
?>
<script type="text/javascript" language="JavaScript1.2">
<!--
stm_ep();
stm_ep();
stm_aix("p0i4","p0i0",[0,"Spelers","","",-1,-1,0,"/spelers.php","_self","Spelersprofielen"]);
stm_aix("p0i5","p0i1",[0,"Jeugd","","",-1,-1,0,"","_self","Informatie over en voor de jeugdspelers"]);
stm_bpx("p1","p0",[1,2,0,0,1,4,0]);
stm_aix("p1i0","p0i0",[0,"Algemeen","","",-1,-1,0,"/jeugd/jeugd.php","_self","Jeugd trainers"]);
stm_aix("p1i1","p0i0",[0,"Teams","","",-1,-1,0,"/jeugd/jeugdteams.php","_self","Jeugdteams"]);
stm_aix("p1i2","p0i0",[0,"J-team","","",-1,-1,0,"/jeugd/J-team.php","_self","J-team"]);
stm_aix("p1i3","p0i0",[0,"Resultaten","","",-1,-1,0,"/jeugd/resultaten_jeugd.php?s=2008","_self","Resultaten jeugdtornooien W&L spelers"]);
stm_aix("p1i4","p0i0",[0,"Jeugdtornooi","","",-1,-1,0,"/jeugd/jeugdtornooi.php","_self","Jeugdtornooi W&L"]);
stm_ep();
stm_aix("p0i6","p0i0",[0,"Recreanten","","",-1,-1,0,"/recreant.php","_self","Recreanten"]);
stm_aix("p0i7","p0i1",[0,"Foto's","","",-1,-1,0,"http://users.pandora.be/ikkegg","_blank","badminton foto's"]);
//-->
</script>
<?php
/*************************************************************************************************
 * Create a link to every photo directory that exists
 ************************************************************************************************/
/* Get the root dirctory of the website */
  $rootdir = $_SERVER['DOCUMENT_ROOT'];
/* directory where all the photo's are located in subdirectories */
  $dir = $rootdir."/pictures";
/* Get all the subdirectories of the pictures directory */
  include $rootdir."/functies/getDirectoryListing.php";
  $subdirs = getDirectoryListing($dir, "a", 1, 0, "none");
/* Number of subdirs in directory */
  $subdir_cnt = count($subdirs);
/* Display a link to every subdirectory */
  $i = 0;
  foreach ($subdirs as $subdir)
  {
    echo "<script type=\"text/javascript\" language=\"JavaScript1.2\">\n";
    if ($i == 0) // first subdirectory
    {
       echo "stm_bpx(\"p1\",\"p0\",[1,2,0,0,1,4,0]);\n";
    }
	echo "stm_aix(\"p0i".$i."\",\"p0i0\",[0,\"".$subdir."\",\"\",\"\",-1,-1,0,\"/pictures/showThumbnails.php?dir=".$subdir."\",\"_self\",\"".$subdir."\"]);\n";
    if ($i == $subdir_cnt - 1) // last subdirectory
    {
      echo "stm_ep();\n";
    }
    echo"</script>\n";
	$i++;
  }
?>
      <script type="text/javascript" language="JavaScript1.2">
<!--
stm_ai("p0i8",[6,4,"#006633","",0,0,0]);
stm_aix("p0i9","p0i0",[0,"Links","","",-1,-1,0,"/links.php","_self","Links"]);
stm_aix("p0i10","p0i0",[0,"Message board","","",-1,-1,0,"/messageboard/messageboard.php?p=1&m=n&id=0","_blank","Message Board"]);
stm_aix("p0i11","p0i0",[0,"Laatste Nieuws","","",-1,-1,0,"/laatste_nieuws.php","_self","De laatste wijzigingen aan de W&L website"]);
stm_aix("p0i12","p0i0",[0,"Team Veltem Motors","","",-1,-1,0,"http://www.everyoneweb.com/teamveltemmotors","_self","De fansite van Team Veltem Motors"]);
stm_ep();
stm_em();
//-->
</script>
<!-- InstanceBeginEditable name="Floating Menu end" -->
<!-- InstanceEndEditable --> 
      &nbsp;&nbsp; <!-- InstanceBeginEditable name="counter" --><!-- InstanceEndEditable --> 
      <!-- InstanceBeginEditable name="Poll" --><!-- InstanceEndEditable --> <!-- InstanceBeginEditable name="wiens verjaardag is het" --><!-- InstanceEndEditable --> 
    </td>
    <td width="100%" valign="top">
	  <!-- InstanceBeginEditable name="general" -->

<p>Deze pagina is een visuele voorstelling van onze algemene <a href="http://www.badmintonsport.be/rss/main.rss" target="_blank">RSS feed</a>.  Hier kan je de 15 laatste wijzigingen aan de W&amp;L website raadplegen.</p>

<?php
  require_once('functies/RSSParser.php');
 // Create an XML parser
  $xml_parser = xml_parser_create();
 // Instantiate 
  $rss_parser = new RssParser();
  xml_set_object($xml_parser,&$rss_parser);
 // Set the functions to handle opening and closing tags
  xml_set_element_handler($xml_parser, "startElement", "endElement");
 // Set the function to handle blocks of character data
  xml_set_character_data_handler($xml_parser, "characterData");
 // Open the XML file for reading
  $fp = fopen("http://www.badmintonsport.be/rss/main.rss","r")
    or die("Error reading RSS data.");
 // Read the XML file 4KB at a time
  while ($data = fread($fp, 4096))
  {
    // Parse each 4KB chunk with the XML parser created above
    xml_parse($xml_parser, $data, feof($fp))
        or die(sprintf("XML error: %s at line %d",  
            xml_error_string(xml_get_error_code($xml_parser)),  
            xml_get_current_line_number($xml_parser)));
  }
 // Close the XML file
  fclose($fp);
 // Free up memory used by the XML parser
  xml_parser_free($xml_parser);
?>

<!-- InstanceEndEditable -->
	</td>
  </tr>
  <tr> 
    <td height="0"></td>
    <td></td>
  </tr>
  <tr> 
    <td height="1"><img src="http://www.badmintonsport.be/images/spacer.gif" alt="" width="150" height="1"></td>
    <td></td>
  </tr>
</table>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <!--DWLayoutTable-->
  <tr> 
    <td width="100%" height="50" valign="top" bgcolor="#FFFFFF">
	  <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#CCCCCC">
        <!--DWLayoutTable-->
        <tr> 
          <td align="center" bordercolor="#CCCCCC" bgcolor="#006600"><img src="http://www.badmintonsport.be/images/spacer.gif" width="1" height="4" alt=""></td>
        </tr>
        <tr> 
          <td width="100%" align="center" valign="middle" bordercolor="#CCCCCC" bgcolor="#CCCCCC" class="gmenu">
		    Last change: <!-- InstanceBeginEditable name="datum" --> <!-- InstanceEndEditable --> 
            E-mail: <a href="mailto:webmaster@badmintonsport.be">webmaster@badmintonsport.be</a></td>
        </tr>
        <tr>
          <td align="center" bordercolor="#CCCCCC" bgcolor="#006600" class="gmenu"><img src="http://www.badmintonsport.be/images/spacer.gif" width="1" height="4" alt=""></td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</body>
<!-- InstanceEnd --></html>
<?php
  require_once "functies/badm_db.inc.php";
  $badm_db = badm_conn_db();
  log_website_usage($start_time, null, $badm_db);
  mysql_close($badm_db);
?>