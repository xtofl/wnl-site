<html>
<head>
  <title>Maak backup</title>
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>

<body bgcolor="#CCCCFF">
<?php
   require "../badm_db.inc.php";
   $badm_db = badm_conn_db();
if(!isset($_POST["inschr_torn"])
and !isset($_POST["inschr_spel"])
and !isset($_POST["gb_berichten"])
and !isset($_POST["gb_replies"])
and !isset($_POST["bad_spelers"])
and !isset($_POST["bad_tornooien"])
and !isset($_POST["users"])
and !isset($_POST["clickcount"]))
{
  echo "<p>Duidt aan welke tabel(len) je wil archiveren:</p>";
  echo "<table>\n";
  echo "<FORM NAME=\"backup\" ACTION=\"MaakBackup.php\" METHOD=\"post\">\n";
  echo "  <tr><th>Tabel</th><th>Laatste wijziging</th><th>Aantal rijen</th></tr>\n";
/*schrijf hier de naam van de tabellen die gebackupped kunnen worden*/
   $array = array("inschr_torn"
                 ,"inschr_spel"
		         ,"gb_berichten"
			     ,"gb_replies"
                 ,"bad_spelers"
                 ,"bad_tornooien"
                 ,"users"
				 ,"clickcount");
   foreach ($array as $value)
   {
      $sql = "SELECT DATE_FORMAT(MAX(dt_wijz), '%d-%m-%Y') AS dt_wijz
              ,      COUNT(*) AS aantal
		      FROM ".$value;
      $result = mysql_query($sql, $badm_db) or badm_mysql_die();
      $data = mysql_fetch_object($result);
      echo "<tr>\n";
      echo "  <td><INPUT TYPE=\"checkbox\" NAME=\"".$value."\" VALUE=\"1\">".$value."</td>\n";
      echo "  <td align=\"center\">".$data->dt_wijz."</td>\n";
      echo "  <td align=\"right\">".$data->aantal."</td>\n";
      echo "</tr>";
   } //end foreach
   echo "<tr>\n";
   echo "  <td colspan=3 align=\"center\"><br><INPUT TYPE=\"submit\" value=\"Maak backup\"></td>\n";
   echo "</tr>\n";
   echo "</FORM>\n";
   echo "</table>\n";
} //end if
else
{
   if ($_POST["inschr_spel"] == 1)
   {
   /*Maak backup tabel inschr_spel*/
   $sql = "SELECT id
           ,      naam
		   ,      lidnr
           ,      klassement
           ,      enkel
           ,      dubbel
           ,      gemengd
           ,      dub_partner
           ,      mix_partner
           ,      inschr_torn_id
           ,      usid_wijz
           ,      dt_wijz
           FROM inschr_spel";
   $result = mysql_query ($sql, $badm_db) or badm_mysql_die();
   $data = "";
   while ($spel = mysql_fetch_object($result))
   {
      $data .= "INSERT INTO inschr_spel VALUES (".$spel->id.", '";
      $data .= $spel->naam."', ";
      $data .= $spel->lidnr.", '";
      $data .= $spel->klassement."', '";
      $data .= $spel->enkel."', '";
      $data .= $spel->dubbel."', '";
      $data .= $spel->gemengd."', '";
      $data .= $spel->dub_partner."', '";
      $data .= $spel->mix_partner."', ";
      $data .= $spel->inschr_torn_id.", '";
      $data .= $spel->usid_wijz."', ";
      $data .= $spel->dt_wijz.");\n";
   } //end while
   $file = "inschr_spel.txt";    
   if (!$file_handle = fopen($file,"w"))
   { echo "<br>Kan bestand ($file) niet openen/aanmaken"; }   
   if (!fwrite($file_handle, $data))
   { echo "<br>Kan niet schrijven naar bestand ($file)"; }   
   echo "<br>Backup met succes gemaakt naar $file";    
   fclose($file_handle);
   } //end if
   if ($_POST["inschr_torn"] == 1)
   {
   /*Maak backup tabel inschr_torn*/
   $sql = "SELECT id
           ,      datum
           ,      organisatie
           ,      type
           ,      reeks_dis
           ,      info
           ,      dt_inschr
           ,      email
           ,      naam
           ,      taal
           ,      verzonden
           ,      usid_wijz
           ,      dt_wijz
           FROM inschr_torn";
   $result = mysql_query ($sql, $badm_db) or badm_mysql_die();
   $data = "";
   while ($spel = mysql_fetch_object($result))
   {
      $data .= "INSERT INTO inschr_torn VALUES (".$spel->id.", '";
      $data .= $spel->datum."', '";
      $data .= $spel->organisatie."', '";
      $data .= $spel->type."', '";
      $data .= $spel->reeks_dis."', '";
      $data .= $spel->info."', '";
      $data .= $spel->dt_inschr."', '";
      $data .= $spel->email."', '";
      $data .= $spel->naam."', '";
      $data .= $spel->taal."', '";
      $data .= $spel->verzonden."', '";
      $data .= $spel->usid_wijz."', ";
      $data .= $spel->dt_wijz.");\n";
   } //end while
   $file = "inschr_torn.txt";    
   if (!$file_handle = fopen($file,"w"))
   { echo "<br>Kan bestand ($file) niet openen/aanmaken"; }   
   if (!fwrite($file_handle, $data))
   { echo "<br>Kan niet schrijven naar bestand ($file)"; }   
   echo "<br>Backup met succes gemaakt naar $file";    
   fclose($file_handle);
   } //end if
   if ($_POST["gb_berichten"] == 1)
   {
   /*Maak backup tabel gb_berichten*/
   $sql = "SELECT id
           ,      naam
		   ,      email
           ,      onderwerp
           ,      tekst
           ,      datum
           ,      icoon
           ,      bekeken
           ,      dt_wijz
           FROM gb_berichten";
   $result = mysql_query ($sql, $badm_db) or badm_mysql_die();
   $data = "";
   while ($bericht = mysql_fetch_object($result))
   {
      $data .= "INSERT INTO gb_berichten VALUES (".$bericht->id.", '";
      $data .= $bericht->naam."', '";
      $data .= $bericht->email."', '";
      $data .= str_replace("'", " ", $bericht->onderwerp)."', '";
      $data .= str_replace("'", " ", $bericht->tekst)."', ";
      $data .= $bericht->datum.", '";
      $data .= $bericht->icoon."', ";
      $data .= $bericht->bekeken.", ";
      $data .= $bericht->dt_wijz.");\n";
   } //end while
   $file = "gb_berichten.txt";    
   if (!$file_handle = fopen($file,"w"))
   { echo "<br>Kan bestand ($file) niet openen/aanmaken"; }   
   if (!fwrite($file_handle, $data))
   { echo "<br>Kan niet schrijven naar bestand ($file)"; }   
   echo "<br>Backup met succes gemaakt naar $file";    
   fclose($file_handle);
   } //end if
   if ($_POST["gb_replies"] == 1)
   {
   /*Maak backup tabel gb_replies*/
   $sql = "SELECT id
           ,      naam
		   ,      email
           ,      onderwerp
           ,      tekst
           ,      icoon
           ,      datum
           ,      bericht_id
           ,      dt_wijz
           FROM gb_replies";
   $result = mysql_query ($sql, $badm_db) or badm_mysql_die();
   $data = "";
   while ($reply = mysql_fetch_object($result))
   {
      $data .= "INSERT INTO gb_replies VALUES (".$reply->id.", '";
      $data .= $reply->naam."', '";
      $data .= $reply->email."', '";
      $data .= str_replace("'", " ", $reply->onderwerp)."', '";
      $data .= str_replace("'", " ", $reply->tekst)."', '";
      $data .= $reply->icoon."', ";
      $data .= $reply->datum.", ";
      $data .= $reply->bericht_id.", ";
      $data .= $reply->dt_wijz.");\n";
   } //end while
   $file = "gb_replies.txt";    
   if (!$file_handle = fopen($file,"w"))
   { echo "<br>Kan bestand ($file) niet openen/aanmaken"; }   
   if (!fwrite($file_handle, $data))
   { echo "<br>Kan niet schrijven naar bestand ($file)"; }   
   echo "<br>Backup met succes gemaakt naar $file";    
   fclose($file_handle);
   } //end if
   if ($_POST["bad_spelers"] == 1)
   {
   /*Maak backup tabel bad_spelers*/
   $sql = "SELECT id
           ,      naam
		   ,      geb_dt
           ,      geslacht
           ,      klassement
           ,      geb_plaats
           ,      adres
           ,      postcode
		   ,      woonplaats
		   ,      tel
		   ,      gsm
		   ,      lengte
		   ,      gewicht
		   ,      beroep
		   ,      handig
		   ,      start_dt
		   ,      club_dt
		   ,      racket
		   ,      lidnr
		   ,      email
		   ,      usid_wijz
           ,      dt_wijz
           FROM bad_spelers";
   $result = mysql_query ($sql, $badm_db) or badm_mysql_die();
   $data = "";
   while ($speler = mysql_fetch_object($result))
   {
      $data .= "INSERT INTO bad_spelers VALUES (".$speler->id.", '";
      $data .= $speler->naam."', '";
      $data .= $speler->geb_dt."', '";
      $data .= $speler->geslacht."', '";
      $data .= $speler->klassement."', '";
      $data .= $speler->geb_plaats."', '";
      $data .= $speler->adres."', '";
      $data .= $speler->postcode."', '";
      $data .= $speler->woonplaats."', '";
      $data .= $speler->tel."', '";
      $data .= $speler->gsm."', '";
      $data .= $speler->lengte."', ";
      $data .= $speler->gewicht.", '";
      $data .= $speler->beroep."', '";
      $data .= $speler->handig."', '";
      $data .= $speler->start_dt."', '";
      $data .= $speler->club_dt."', '";
      $data .= $speler->racket."', ";
      $data .= $speler->lidnr.", '";
      $data .= $speler->email."', '";
      $data .= $speler->usid_wijz."', ";
      $data .= $speler->dt_wijz.");\n";
   } //end while
   $file = "bad_spelers.txt";    
   if (!$file_handle = fopen($file,"w"))
   { echo "<br>Kan bestand ($file) niet openen/aanmaken"; }   
   if (!fwrite($file_handle, $data))
   { echo "<br>Kan niet schrijven naar bestand ($file)"; }   
   echo "<br>Backup met succes gemaakt naar $file";    
   fclose($file_handle);
   } //end if
   if ($_POST["bad_tornooien"] == 1)
   {
   /*Maak backup tabel bad_tornooien*/
   $sql = "SELECT id
           ,      dt_van
		   ,      dt_tot
           ,      plaats
           ,      reeks
           ,      klassement
           ,      partner
           ,      spelers_id
		   ,      usid_wijz
           ,      dt_wijz
           FROM bad_tornooien";
   $result = mysql_query ($sql, $badm_db) or badm_mysql_die();
   $data = "";
   while ($tornooi = mysql_fetch_object($result))
   {
      $data .= "INSERT INTO bad_tornooien VALUES (".$tornooi->id.", '";
      $data .= $tornooi->dt_van."', '";
      $data .= $tornooi->dt_tot."', '";
      $data .= $tornooi->plaats."', '";
      $data .= $tornooi->reeks."', '";
      $data .= $tornooi->klassement."', '";
      $data .= $tornooi->partner."', ";
      $data .= $tornooi->spelers_id.", '";
      $data .= $tornooi->usid_wijz."', ";
      $data .= $tornooi->dt_wijz.");\n";
   } //end while
   $file = "bad_tornooien.txt";    
   if (!$file_handle = fopen($file,"w"))
   { echo "<br>Kan bestand ($file) niet openen/aanmaken"; }   
   if (!fwrite($file_handle, $data))
   { echo "<br>Kan niet schrijven naar bestand ($file)"; }   
   echo "<br>Backup met succes gemaakt naar $file";    
   fclose($file_handle);
   } //end if
   if ($_POST["users"] == 1)
   {
   /*Maak backup tabel users*/
   $sql = "SELECT id
           ,      usid
		   ,      paswoord
           ,      level
           ,      naam
		   ,      usid_wijz
           ,      dt_wijz
           FROM users";
   $result = mysql_query ($sql, $badm_db) or badm_mysql_die();
   $data = "";
   while ($tornooi = mysql_fetch_object($result))
   {
      $data .= "INSERT INTO users VALUES (".$tornooi->id.", '";
      $data .= $tornooi->usid."', '";
      $data .= $tornooi->paswoord."', ";
      $data .= $tornooi->level.", '";
      $data .= $tornooi->naam."', '";
      $data .= $tornooi->usid_wijz."', ";
      $data .= $tornooi->dt_wijz.");\n";
   } //end while
   $file = "users.txt";    
   if (!$file_handle = fopen($file,"w"))
   { echo "<br>Kan bestand ($file) niet openen/aanmaken"; }   
   if (!fwrite($file_handle, $data))
   { echo "<br>Kan niet schrijven naar bestand ($file)"; }   
   echo "<br>Backup met succes gemaakt naar $file";    
   fclose($file_handle);
   } //end if
   if ($_POST["clickcount"] == 1)
   {
   /*Maak backup tabel clickcount*/
   $sql = "SELECT id
           ,      pagina
		   ,      clicks
           FROM clickcount";
   $result = mysql_query ($sql, $badm_db) or badm_mysql_die();
   $data = "";
   while ($clicks = mysql_fetch_object($result))
   {
      $data .= "INSERT INTO clickcount VALUES (".$clicks->id.", '";
      $data .= $clicks->pagina."', '";
      $data .= $clicks->clicks."');\n";
   } //end while
   $file = "clickcount.txt";    
   if (!$file_handle = fopen($file,"w"))
   { echo "<br>Kan bestand ($file) niet openen/aanmaken"; }   
   if (!fwrite($file_handle, $data))
   { echo "<br>Kan niet schrijven naar bestand ($file)"; }   
   echo "<br>Backup met succes gemaakt naar $file";    
   fclose($file_handle);
   } //end if
   mysql_close($badm_db);
   echo "<br><br>Dit scherm sluit automatisch na 5 seconden";
   echo "<script language=\"JavaScript\">";
   echo "window.setTimeout(\"window.close()\",5000);";
   echo "</script>";
} //end else
?> 
</body>
</html>
